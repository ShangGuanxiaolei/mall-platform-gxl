package com.xquark.service.wechat.impl;


import com.xquark.dal.mapper.*;

import com.xquark.dal.model.ShopWechatSetting;
import com.xquark.dal.model.User;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.wechat.ShopWechatSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("shopWechatSettingService")
public class ShopWechatSettingServiceImpl extends BaseServiceImpl implements
    ShopWechatSettingService {

  @Autowired
  private ShopWechatSettingMapper shopWechatSettingMapper;


  @Override
  public int create(ShopWechatSetting shopWechatSetting) {
    return shopWechatSettingMapper.insert(shopWechatSetting);
  }

  @Override
  public int update(ShopWechatSetting shopWechatSetting) {
    return shopWechatSettingMapper.updateByPrimaryKey(shopWechatSetting);
  }

  @Override
  public int insert(ShopWechatSetting shopWechatSetting) {
    return shopWechatSettingMapper.insert(shopWechatSetting);
  }

  @Override
  public int insertOrder(ShopWechatSetting shopWechatSetting) {
    return shopWechatSettingMapper.insert(shopWechatSetting);
  }

  @Override
  public ShopWechatSetting load(String id) {
    return shopWechatSettingMapper.selectByPrimaryKey(id);
  }

  @Override
  public ShopWechatSetting findByUser(String userId) {
    return shopWechatSettingMapper.selectByUserId(userId);
  }

  @Override
  public ShopWechatSetting mine() {
    return shopWechatSettingMapper.selectByUserId(getCurrentUser().getId());
  }

  @Override
  public int delete(String id) {
    return shopWechatSettingMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int undelete(String id) {
    return shopWechatSettingMapper.unDeleteByPrimaryKey(id);
  }
}
