package com.xquark.service.banner.impl;

import com.xquark.dal.mapper.IndexBannerMapper;
import com.xquark.dal.model.IndexBanner;
import com.xquark.service.banner.IndexBannerService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: IndexBannerServiceImpl
 * @Description: TODO
 * @Author: Kwonghom
 * @CreateDate: 2019/4/2 13:54
 * @Version: 1.0
 **/
@Service
public class IndexBannerServiceImpl implements IndexBannerService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IndexBannerMapper indexBannerMapper;

    @Override
    public int insertIndexBanner(IndexBanner indexBanner) {
        Integer lastSequence = indexBannerMapper.selectLastSequence();
        if (null == lastSequence) {
            indexBanner.setSequence(1);
        } else {
            indexBanner.setSequence(++lastSequence);
        }
        indexBanner.setStatus(indexBanner.getStatus() == null ? true : false);
        return indexBannerMapper.insertIndexBanner(indexBanner);
    }

    @Override
    public List<IndexBanner> searchIndexBanner(Map<String, Object> map, Pageable pageable) {
        if (null == map.get("dropStatus") || map.get("dropStatus").equals("")) {
            map.put("dropStatus", 1);
        }
        List<IndexBanner> indexBanners = indexBannerMapper.searchIndexBanner(map, pageable);
        if (CollectionUtils.isNotEmpty(indexBanners)) {
            for (IndexBanner indexBanner : indexBanners) {
                calcBannerDropStatus(indexBanner);
            }
        }
        return indexBanners;
    }

    @Override
    public long countIndexBannerBySearch(Map<String, Object> map) {
        if (null == map.get("dropStatus") || map.get("dropStatus").equals("")) {
            map.put("dropStatus", 1);
        }
        return indexBannerMapper.countIndexBannerBySearch(map);
    }

    @Override
    public IndexBanner selectByPrimaryKey(String id) {
        IndexBanner indexBanner = indexBannerMapper.selectByPrimaryKey(id);
        calcBannerDropStatus(indexBanner);
        return indexBanner;
    }

    @Override
    public boolean changeBannerSequence(Map<String, Object> map, Pageable pageable) {
        try {
            long start = System.currentTimeMillis();
            String id = (String) map.get("id");
            String direction = (String) map.get("direction");
            String toChangeId = null;
            List<IndexBanner> indexBanners = indexBannerMapper.selectIndexBanners(pageable);
            if (CollectionUtils.isNotEmpty(indexBanners)) {
                for (int i = 0; i < indexBanners.size(); i++) {
                    if ( (direction.equals("up") && indexBanners.get(0).getId().equals(id))
                            || (direction.equals("down") && indexBanners.get(indexBanners.size() - 1).getId().equals(id)) ) {
                        logger.error("无法调整顺序, 索引越界");
                        break;
                    } else if (direction.equals("up") && indexBanners.get(i).getId().equals(id)) { // 向上
                        toChangeId = indexBanners.get(i - 1).getId();
                        break;
                    } else if (direction.equals("down") && indexBanners.get(i).getId().equals(id)) { // 向下
                        toChangeId = indexBanners.get(i + 1).getId();
                        break;
                    }
                }
            }
            if (StringUtils.isNotEmpty(toChangeId)) {
                IndexBanner curIndexBanner = indexBannerMapper.selectByPrimaryKey(id);
                IndexBanner toChangeIndexBanner = indexBannerMapper.selectByPrimaryKey(toChangeId);
                int tempSequence = -1; // 处理唯一约束, 临时更新的顺序
                int curSequence = curIndexBanner.getSequence();
                int toChangeSequence = toChangeIndexBanner.getSequence();
                curIndexBanner.setSequence(toChangeSequence);
                toChangeIndexBanner.setSequence(tempSequence);
                indexBannerMapper.updateSequenceByPrimaryKey(toChangeIndexBanner);
                indexBannerMapper.updateSequenceByPrimaryKey(curIndexBanner);
                // 处理唯一字段, 更新第3次结果
                toChangeIndexBanner.setSequence(curSequence);
                indexBannerMapper.updateSequenceByPrimaryKey(toChangeIndexBanner);
                long end = System.currentTimeMillis();
                logger.info("本次调整 banner 顺序执行了--> {} ms", end - start);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("调整 banner 发生了异常, 异常信息: {}", e.getMessage());
            return false;
        }
    }

    @Override
    public List<IndexBanner> getIndexBanners(Pageable pageable) {
        List<IndexBanner> indexBanners = indexBannerMapper.selectIndexBanners(pageable);
        if (CollectionUtils.isNotEmpty(indexBanners)) {
            for (IndexBanner indexBanner : indexBanners) {
                calcBannerDropStatus(indexBanner);
            }
        }
        return indexBanners;
    }

    @Override
    public int updateByPrimaryKey(IndexBanner indexBanner) {
        if (indexBanner.getStatus() == null) {
            indexBanner.setStatus(true);
        }
        return indexBannerMapper.updateByPrimaryKey(indexBanner);
    }

    @Override
    public boolean selectExists(String id) {
        return indexBannerMapper.selectExists(id);
    }

    // 根据开始,结束时间计算投放状态
    private void calcBannerDropStatus(IndexBanner indexBanner) {
        final Date curDate = new Date();
        if (indexBanner != null) {
            if (indexBanner.getStartAt().after(curDate)) {
                indexBanner.setDropStatus(3);
            } else if (indexBanner.getStartAt().before(curDate) && indexBanner.getEndAt().after(curDate)) {
                indexBanner.setDropStatus(2);
            } else {
                indexBanner.setDropStatus(4);
            }
        }
    }

    @Override
    public List<IndexBanner> getAppIndexBanners() {
        List<IndexBanner> indexBanners = indexBannerMapper.selectAppIndexBanners();
        if (CollectionUtils.isNotEmpty(indexBanners)) {
            for (IndexBanner indexBanner : indexBanners) {
                indexBanner.setDropStatus(2);
            }
        }
        return indexBanners;
    }

    @Override
    public int deletedByPrimaryKey(String id) {
        return indexBannerMapper.deletedByPrimaryKey(id);
    }

    @Override
    public long countIndexBannerList() {
        return indexBannerMapper.countIndexBannerList();
    }
}
