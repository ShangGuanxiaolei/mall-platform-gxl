package com.xquark.service.banner;

import com.xquark.dal.model.IndexBanner;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: IndexBannerService
 * @Description: 首页优化 banner
 * @Author: Kwonghom
 * @CreateDate: 2019/4/2 13:53
 * @Version: 1.0
 **/
public interface IndexBannerService {

    /**
     * 新增首页 banner
     * @author Kwonghom
     * @date 2019/4/2 15:18
     * @param [indexBanner]
     * @return int
     */
    int insertIndexBanner(IndexBanner indexBanner);

    /**
     * 根据筛选条件, 筛选 banner
     * @param map
     * @param pageable
     * @return
     */
    List<IndexBanner> searchIndexBanner(Map<String, Object> map, Pageable pageable);

    /**
     * 统计筛选后的 banner 总数
     * @author Kwonghom
     * @date 2019/4/2 16:08
     * @param [map]
     * @return long
     */
    long countIndexBannerBySearch(Map<String, Object> map);

    /**
     * 是否存在
     * @author Kwonghom
     * @date 2019/4/2 17:34
     * @param [id]
     * @return boolean
     */
    boolean selectExists(String id);

    /**
     * 更新首页 banner
     * @author Kwonghom
     * @date 2019/4/2 18:03
     * @param [indexBanner]
     * @return int
     */
    int updateByPrimaryKey(IndexBanner indexBanner);

    /**
     * 查询指定 banner
     * @author Kwonghom
     * @date 2019/4/2 18:19
     * @param [id]
     * @return com.xquark.dal.model.IndexBanner
     */
    IndexBanner selectByPrimaryKey(String id);

    boolean changeBannerSequence(Map<String, Object> map, Pageable pageable);

    List<IndexBanner> getIndexBanners(Pageable pageable);

    List<IndexBanner> getAppIndexBanners();

    int deletedByPrimaryKey(String id);

    long countIndexBannerList();
}
