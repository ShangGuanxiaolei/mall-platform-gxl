package com.xquark.service.bank.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

import com.xquark.dal.model.*;
import com.xquark.service.shopTree.ShopTreeService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xquark.dal.mapper.WithdrawApplyMapper;
import com.xquark.dal.status.WithdrawApplyStatus;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.type.PayRequestBizType;
import com.xquark.dal.type.PayRequestPayType;
import com.xquark.dal.vo.SubAccountVO;
import com.xquark.dal.vo.WithdrawAdmin;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.account.AccountService;
import com.xquark.service.account.SubAccountService;
import com.xquark.service.alipay.UserAlipayService;
import com.xquark.service.bank.UserBankService;
import com.xquark.service.bank.WithdrawApplyService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pay.PayRequestApiService;
import com.xquark.service.user.UserService;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;

@Service("withdrawApplyService")
public class WithdrawApplyServiceImpl extends BaseServiceImpl implements WithdrawApplyService {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private WithdrawApplyMapper withdrawApplyMapper;

  @Autowired
  private UserService userService;

  @Autowired
  private AccountService accountService;

  @Autowired
  private SubAccountService subAccountService;

  @Autowired
  private UserBankService userBankService;

  @Autowired
  private UserAlipayService userAlipayService;

  @Autowired
  private PayRequestApiService payRequestApiService;

  @Autowired
  private AccountApiService accountApiService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Override
  public String generateApplyNo() {
    return UniqueNoUtils.next(UniqueNoType.WITHDRAW);
  }

  @Override
  public WithdrawApply load(String id) {
    return withdrawApplyMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<WithdrawApply> listWithdrawApply(String userId, WithdrawApplyStatus status,
      Pageable page) {
    return withdrawApplyMapper.listWithdrawApply(userId, status, page);
  }

  @Override
  public List<WithdrawAdmin> listWithdrawApply(Map<String, Object> params, Pageable page) {
    return withdrawApplyMapper.listWithdrawApplyByAdmin(params, page);
  }

  @Override
  public Map<String, Object> countWithdrawApply(Map<String, Object> params) {
    return withdrawApplyMapper.countWithdrawApplyByAdmin(params);
  }

  /**
   * 按状态获取累计金额
   */
  @Override
  public BigDecimal totalWithdrawApplyByStatus(String userId, WithdrawApplyStatus status) {
    return withdrawApplyMapper.totalWithdrawApplyByStatus(userId, status);
  }

  @Override
  public int delete(String id) {
    return withdrawApplyMapper.updateForArchive(id);
  }

  @Override
  public int undelete(String id) {
    return withdrawApplyMapper.updateForUnArchive(id);
  }

  @Override
  @Transactional
  public int update(WithdrawApply apply) {
    if (apply.getStatus() != WithdrawApplyStatus.NEW) {
      return 0;
    }
    return withdrawApplyMapper.updateByPrimaryKeySelective(apply);
  }

  @Override
  @Transactional
  public int insert(WithdrawApply withdraw) {
    String payNo = payRequestApiService.generatePayNo();
    SubAccount fromAccount = accountApiService
        .findSubAccountByUserId(withdraw.getUserId(), AccountType.AVAILABLE);
    SubAccount toAccount = accountApiService
        .findSubAccountByUserId(withdraw.getUserId(), AccountType.WITHDRAW);
    PayRequest request = new PayRequest(payNo, withdraw.getApplyNo(), PayRequestBizType.WITHDRAW,
        PayRequestPayType.WITHDRAW, withdraw.getApplyMoney(), fromAccount.getId(),
        toAccount.getId(), null);
    int flag1 = payRequestApiService.payRequest(request) ? 1 : 0;
    int flag2 = 0;
    if (flag1 == 1) {
      flag2 = withdrawApplyMapper.insert(withdraw);
    }

    if (flag1 == 0 || flag2 == 0) {
      log.error("withdraw create error userId=[" + withdraw.getUserId() + "] flag1=[" + flag1
          + "]  flag2=[" + flag1 + "]");
    }

    return flag1 & flag2;
  }

  @Override
  @Transactional
  public int insertOrder(WithdrawApply withdraw) {
    String payNo = payRequestApiService.generatePayNo();
    SubAccount fromAccount = accountApiService
            .findSubAccountByUserId(withdraw.getUserId(), AccountType.AVAILABLE);
    SubAccount toAccount = accountApiService
            .findSubAccountByUserId(withdraw.getUserId(), AccountType.WITHDRAW);
    PayRequest request = new PayRequest(payNo, withdraw.getApplyNo(), PayRequestBizType.WITHDRAW,
            PayRequestPayType.WITHDRAW, withdraw.getApplyMoney(), fromAccount.getId(),
            toAccount.getId(), null);
    int flag1 = payRequestApiService.payRequest(request) ? 1 : 0;
    int flag2 = 0;
    if (flag1 == 1) {
      flag2 = withdrawApplyMapper.insert(withdraw);
    }

    if (flag1 == 0 || flag2 == 0) {
      log.error("withdraw create error userId=[" + withdraw.getUserId() + "] flag1=[" + flag1
              + "]  flag2=[" + flag1 + "]");
    }

    return flag1 & flag2;
  }

  @Override
  @Transactional
  public void batchPay(String ids, String confirmMoneys) {
    String[] idTmp = ids.split(",");
    String[] confirmMoneyTmp = confirmMoneys.split(",");
    for (int i = 0; i < idTmp.length; i++) {
      WithdrawApply apply = load(idTmp[i]);
      apply.setConfirmMoney(NumberUtils.createBigDecimal(confirmMoneyTmp[i]));
      pay(apply);
    }
  }

  @Override
  @Transactional
  public void pay(WithdrawApply withdraw) {

    if (withdraw.getStatus() == WithdrawApplyStatus.NEW) {
      if (withdraw.getApplyMoney().compareTo(withdraw.getConfirmMoney()) < 0) {
        throw new BizException(GlobalErrorCode.UNKNOWN,
            withdraw.getAccountName() + " 核准金额=" + withdraw.getConfirmMoney() + " 不能大于申请金额："
                + withdraw.getApplyMoney());
      }
      //TODO 先转入kkkd的提现账户
      String payNo = payRequestApiService.generatePayNo();
      SubAccount fromAccount = accountApiService
          .findSubAccountByUserId(withdraw.getUserId(), AccountType.WITHDRAW);
      SubAccount toAccount = accountApiService
          .findSubAccountByUserId(userService.loadKkkdUserId(), AccountType.WITHDRAW);
      PayRequest request = new PayRequest(payNo, withdraw.getApplyNo(), PayRequestBizType.WITHDRAW,
          PayRequestPayType.WITHDRAW, withdraw.getConfirmMoney(), fromAccount.getId(),
          toAccount.getId(), null);

      int flag1 = payRequestApiService.payRequest(request) ? 1 : 0;
      int flag2 = 0;
      if (flag1 == 1) {
        flag2 = withdrawApplyMapper.pay(withdraw);
      }

      if (flag1 == 0 || flag2 == 0) {
        log.error("withdraw pay error userId=[" + withdraw.getUserId() + "] flag1=[" + flag1
            + "]  flag2=[" + flag1 + "]");
        throw new BizException(GlobalErrorCode.UNKNOWN,
            withdraw.getAccountName() + " 金额：" + withdraw.getApplyMoney() + "失败");
      }
    }
    //return flag1 & flag2;
  }

  @Override
  @Transactional
  public Long autoWithdrawByTask() {
    List<SubAccountVO> accounts = subAccountService.listByCanWithdraw();
    WithdrawApply withdrawApply = null;
    UserBank userbank = null;
    UserAlipay userAlipay = null;
    for (SubAccountVO account : accounts) {
      if (account.getBalance().compareTo(new BigDecimal("0.01")) < 0) {
        continue;
      }
      withdrawApply = new WithdrawApply();
      withdrawApply.setType(account.getWithdrawType());
      if (account.getWithdrawType() == 1) {
        //提现方式为银行卡
        List<UserBank> list = userBankService.listByUserId(account.getUserId());
        if (list.size() == 0) {
          log.warn("userId [" + account.getUserId() + "] 未绑定银行卡！");
          continue;
        }
        userbank = list.get(0);
        withdrawApply.setAccountName(userbank.getAccountName());
        withdrawApply.setAccountNumber(userbank.getAccountNumber());
        withdrawApply.setBankId(userbank.getId());
        withdrawApply.setOpeningBank(userbank.getOpeningBank());
      } else if (account.getWithdrawType() == 2) {
        //提现方式为支付宝
        userAlipay = userAlipayService.loadByUserId(account.getUserId());
        if (userAlipay == null) {
          log.warn("userId [" + account.getUserId() + "] 未绑定支付宝！");
          continue;
        }
        withdrawApply.setAccountName(userAlipay.getName());
        withdrawApply.setAccountNumber(userAlipay.getAccount());
        withdrawApply.setBankId(userAlipay.getId());
        withdrawApply.setOpeningBank("支付宝");
      } else if (account.getWithdrawType() == 3) {
        //提现方式为微信
        User user = userService.load(account.getUserId());
        if (!StringUtils.isNoneBlank(user.getWeixinCode())) {
          log.warn("userId [" + account.getUserId() + "] 未绑定微信！");
          continue;
        }

        ShopTree rootShop = shopTreeService.selectRootShopByShopId(user.getShopId());
        withdrawApply.setAccountName(user.getName());
        withdrawApply.setAccountNumber(rootShop.getRootShopId());
        withdrawApply.setBankId(user.getId());
        withdrawApply.setOpeningBank("微信");
      } else {
        log.error(
            "无法识别的提现类型：SubAccountVO(userId=" + account.getUserId() + " ,withdrawType=" + account
                .getWithdrawType() + ")");
        continue;
      }
      withdrawApply.setApplyNo(generateApplyNo());
      withdrawApply.setApplyMoney(account.getBalance().setScale(2, RoundingMode.DOWN));
      withdrawApply.setUserId(account.getUserId());
      withdrawApply.setStatus(WithdrawApplyStatus.NEW);
      insert(withdrawApply);
    }

    return (long) accounts.size();
  }

  public int withdrawSuccessByNo(String applyNo, BigDecimal confirmFee) {
    return withdrawApplyMapper.withdrawSuccessByNo(applyNo, confirmFee);
  }

  @Override
  public WithdrawApply loadByApplyNo(String applyNo) {
    return withdrawApplyMapper.loadByApplyNo(applyNo);
  }

  @Override
  public boolean cancel(String withdrawId, String opRemark) {
    return withdrawApplyMapper.cancel(withdrawId, opRemark) == 1;
  }

  @Override
  public int withdrawPendingByApplyNo(String applyNo) {
    return withdrawApplyMapper.withdrawPendingByApplyNo(applyNo);
  }

  @Override
  public int withdrawFailedByApplyNo(String applyNo) {
    return withdrawApplyMapper.withdrawFailedByApplyNo(applyNo);
  }

  @Override
  public Long SynchronousWithdrawByTask() {
    int ret = 0;
    try {
      //同步银行账号
      ret = withdrawApplyMapper.synchronousWithdrawByBank();
      //同步支付宝账号
      ret += withdrawApplyMapper.synchronousWithdrawByAliPay();
    } catch (Exception ex) {
      log.error("同步提现账号失败：" + ex);
    }
    return (long) ret;
  }

  @Override
  public List<WithdrawAdmin> listWithdrawApply4WxExport(Map<String, Object> params) {
    return withdrawApplyMapper.listWithdrawApply4WxExport(params);
  }

  @Override
  public int countWithdrawApply(String userId) {
    return withdrawApplyMapper.countWithdrawApply(userId);
  }

}
