package com.xquark.service.bank.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.UserBankMapper;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserBank;
import com.xquark.service.bank.UserBankService;
import com.xquark.service.common.BaseServiceImpl;

@Service("userBankService")
public class UserBankServiceImpl extends BaseServiceImpl implements UserBankService {

  @Autowired
  private UserBankMapper userBankMapper;

  @Override
  public int delete(String id) {
    UserBank bank = load(id);
    if (!bank.getUserId().equals(getCurrentUser().getId())) {
      return 0;
    }
    return userBankMapper.updateForArchive(id);
  }

  @Override
  public int undelete(String id) {
    return userBankMapper.updateForUnArchive(id);
  }

  @Override
  public int insert(UserBank bank) {
    if (!bank.getUserId().equals(getCurrentUser().getId())) {
      return 0;
    }
    List<UserBank> banks = mine();
    if (banks.size() > 0) {
      return 0;
    }

    return userBankMapper.insert(bank);
  }

  @Override
  public int insertOrder(UserBank userBank) {
    if (!userBank.getUserId().equals(getCurrentUser().getId())) {
      return 0;
    }
    List<UserBank> banks = mine();
    if (banks.size() > 0) {
      return 0;
    }

    return userBankMapper.insert(userBank);
  }

  @Override
  public int update(UserBank bank) {
    if (!bank.getUserId().equals(getCurrentUser().getId())) {
      return 0;
    }

    return userBankMapper.updateByPrimaryKeySelective(bank);
  }

  @Override
  public UserBank load(String id) {
    return userBankMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<UserBank> mine() {
    return listByUserId(getCurrentUser().getId());
  }

  @Override
  public List<UserBank> listByUserId(String userId) {
    return userBankMapper.listByUserId(userId);
  }

}
