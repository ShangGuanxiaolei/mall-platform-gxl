package com.xquark.service.bank;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.WithdrawApply;
import com.xquark.dal.status.WithdrawApplyStatus;
import com.xquark.dal.vo.WithdrawAdmin;
import com.xquark.service.ArchivableEntityService;

public interface WithdrawApplyService extends ArchivableEntityService<WithdrawApply> {

  /**
   * 获取唯一申请编号
   */
  public String generateApplyNo();

  /**
   * 主要用于后台管理获取列表 目前没做权限判断
   */
  List<WithdrawApply> listWithdrawApply(String userId, WithdrawApplyStatus status, Pageable page);

  /**
   * 根据状态获得累计提现记录
   */
  BigDecimal totalWithdrawApplyByStatus(String userId, WithdrawApplyStatus status);

  /**
   * 更新
   */
  int update(WithdrawApply apply);


  /**
   * 自动发起提现申请，由task调用
   */
  Long autoWithdrawByTask();

  List<WithdrawAdmin> listWithdrawApply(Map<String, Object> params, Pageable page);

  Map<String, Object> countWithdrawApply(Map<String, Object> params);

  int withdrawSuccessByNo(String applyNo, BigDecimal confirmFee);

  int withdrawPendingByApplyNo(String applyNo);

  int withdrawFailedByApplyNo(String applyNo);

  WithdrawApply loadByApplyNo(String applyNo);

  boolean cancel(String withdrawId, String opRemark);

  void batchPay(String ids, String confirmMoneys);

  void pay(WithdrawApply withdraw);

  Long SynchronousWithdrawByTask();

  List<WithdrawAdmin> listWithdrawApply4WxExport(Map<String, Object> params);

  /**
   * 获取某个用户的提现记录条数
   */
  int countWithdrawApply(String userId);

}
