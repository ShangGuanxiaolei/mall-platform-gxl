package com.xquark.service.bank.impl;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.mapper.CustomerBankAuditMapper;
import com.xquark.dal.mapper.CustomerBankMapper;
import com.xquark.dal.mapper.CustomerProfileAuditMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.type.IdSequenceType;
import com.xquark.service.sequence.IdSequenceService;
import com.xquark.service.bank.CustomerBankService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.user.UserService;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jqx on 18/8/15.
 */
@Service
@Transactional
public class CustomerBankServiceImpl implements CustomerBankService {

    @Autowired
    private UserService userService;

    @Autowired
    private CustomerProfileService customerProfileService;

    @Autowired
    private CustomerProfileAuditMapper customerProfileAuditMapper;

    @Autowired
    private CustomerBankMapper customerBankMapper;

    @Autowired
    private IdSequenceService idSequenceService;

    @Autowired
    private CustomerBankAuditMapper customerBankAuditMapper;

    public CustomerBank parseBankImage(String imgStr) {
        String host = "http://bankcard.aliapi.hanvon.com";
        String path = "/rt/ws/v1/ocr/bankcard/cropped/recg";
        String method = "POST";
        String appcode = "a75491470a3c40cc83345a733f4d2e3d";
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "APPCODE " + appcode);
        headers.put("Content-Type", "application/json; charset=UTF-8");
        headers.put("Content-Type", "application/octet-stream");
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("code", "e954b425-6380-4946-83ae-9cc44f7d8331");
        String bodys = "{\"uid\":\"118.12.0.12\",\"image\":\"" + imgStr + "\"}";

        CustomerBank customerBank = new CustomerBank();
        try {
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            String result = EntityUtils.toString(response.getEntity());

            JSONObject jsonObject = JSONObject.parseObject(result);
            if(jsonObject != null && "0".equals(jsonObject.getString("code"))){
                JSONObject bankCardObj = jsonObject.getJSONObject("bankCard");
                customerBank.setBankFullName(bankCardObj.getString("bankname"));
                customerBank.setBankAccount(bankCardObj.getString("cardno"));
                customerBank.setCardName(bankCardObj.getString("cardname"));
                String cardType = bankCardObj.getString("cardtype");
                customerBank.setCardType(cardType);
                if("信用卡".equals(cardType) || "贷记卡".equals(cardType)){
                    customerBank.setSupported(false);
                }else{
                    customerBank.setSupported(true);
                }
            }else{
                throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "银行卡解析错误");
            }
        } catch (Exception e) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "银行卡解析异常",e);
        }
        return customerBank;
    }

  private Boolean authBankInfo(CustomerBank customerBank) {
        String host = "http://lundroid.market.alicloudapi.com";
        String path = "/lianzhuo/verifi";
        String method = "GET";
        String appcode = "a75491470a3c40cc83345a733f4d2e3d";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("acct_name", customerBank.getAccountName());
        querys.put("acct_pan", customerBank.getBankAccount());
        querys.put("cert_id", customerBank.getTinCode());

        Map<String,Object> map = new HashMap<String,Object>();

    Boolean bool = true;

        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            String result = EntityUtils.toString(response.getEntity());

            JSONObject jsonObject = JSONObject.parseObject(result);
            if(jsonObject != null){
                JSONObject respObject = jsonObject.getJSONObject("resp");
                if(respObject.getIntValue("code") == 0){
                  bool = false;
                }
            }
        } catch (Exception e) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "银行卡信息验证异常",e);
        }

    return bool;
  }

  private String getBankId(String bankFullName) {
    if ("中国银行".equals(bankFullName)) {
      return "104100000004";
    } else if ("中国建设银行".equals(bankFullName)) {
      return "105100000017";
    } else if ("中国农业银行".equals(bankFullName)) {
      return "103100000026";
    } else if ("中国工商银行".equals(bankFullName)) {
      return "102100099996";
    } else if ("招商银行".equals(bankFullName)) {
      return "308584000013";
    }

    return null;
    }

    public void addCustomerBankInfo(CustomerBank customerBank,User user) {
        if (authBankInfo(customerBank)) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "请确认银行卡信息与身份信息是否匹配");
        }

        //更新xquark_user表身份证字段
        userService.updateTinCode(user.getCpId(),customerBank.getTinCode());

        //customerbank表添加记录
        customerBank.setCpId(user.getCpId());
        Long bankId = idSequenceService.generateIdByType(IdSequenceType.BANK);
        customerBank.setBankId(bankId);
        customerBank.setCreateBy(user.getName());
        customerBank.setBankNumber(getBankId(customerBank.getBankFullName()));
        customerBank.setAccountName(customerBank.getAccountName());
        customerBank.setTincode(customerBank.getTinCode());
        customerBankMapper.addCustomerBankInfo(customerBank);

        //customerbankaudit表添加记录
        CustomerBankAudit customerBankAudit = new CustomerBankAudit();
        customerBankAudit.setAuditType((byte) 1);
        customerBankAudit.setCpId(user.getCpId());
        customerBankAudit.setBankAccount(customerBank.getBankAccount());
        customerBankAuditMapper.insert(customerBankAudit);
    }

    public List<CustomerBank> queryCustomerBankListByCpId(Long cpId) {
        List<CustomerBank> list = customerBankMapper.queryCustomerBankListByCpId(cpId);
        return list;
    }
}