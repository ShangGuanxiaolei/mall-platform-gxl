package com.xquark.service.bank;

import java.util.List;

import com.xquark.dal.model.UserBank;
import com.xquark.service.ArchivableEntityService;

public interface UserBankService extends ArchivableEntityService<UserBank> {

  List<UserBank> mine();

  List<UserBank> listByUserId(String userId);

  int update(UserBank bank);
}
