package com.xquark.service.bank;

import com.xquark.dal.model.CustomerBank;
import com.xquark.dal.model.User;

import java.util.List;

/**
 * Created by jqx on 18/8/15.
 */
public interface CustomerBankService {

    CustomerBank parseBankImage(String imgStr);

    void addCustomerBankInfo(CustomerBank customerBank,User user);

    List<CustomerBank> queryCustomerBankListByCpId(Long cpId);
}
