package com.xquark.service.user.vo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/21
 * Time:21:22
 * Des:用户扫码数据
 */
public class UserScan {
    private String redirectType;
    private String redirectUrl;
    private String redirectName;
    private String redirectApp;

    public String getRedirectType() {
        return redirectType;
    }

    public void setRedirectType(String redirectType) {
        this.redirectType = redirectType;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getRedirectName() {
        return redirectName;
    }

    public void setRedirectName(String redirectName) {
        this.redirectName = redirectName;
    }

    public String getRedirectApp() {
        return redirectApp;
    }

    public void setRedirectApp(String redirectApp) {
        this.redirectApp = redirectApp;
    }
}