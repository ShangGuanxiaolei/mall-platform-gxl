package com.xquark.service.user.vo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/4/21
 * Time:14:29
 * Des:用户日志
 */
public class UserLogInfo {
    private int type; //类型
    private String logInfo; //日志信息
    private Long fromCpId; //分享人
    private Long cpId; //用户cpId

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getLogInfo() {
        return logInfo;
    }

    public void setLogInfo(String logInfo) {
        this.logInfo = logInfo;
    }

    public Long getFromCpId() {
        return fromCpId;
    }

    public void setFromCpId(Long fromCpId) {
        this.fromCpId = fromCpId;
    }

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }
}