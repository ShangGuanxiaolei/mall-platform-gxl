package com.xquark.service.user.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.ImmutableMap;
import com.xquark.aop.anno.NotNull;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.status.AgentStatus;
import com.xquark.dal.status.ShopStatus;
import com.xquark.dal.status.TwitterStatus;
import com.xquark.dal.status.UserType;
import com.xquark.dal.type.IdSequenceType;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.type.PlatformType;
import com.xquark.dal.util.CommonConst;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.dal.vo.SellerInfoVO;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.dal.vo.UserEmployeeVO;
import com.xquark.dal.vo.UserInfoVO;
import com.xquark.helper.Transformer;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CareerLevelService;
import com.xquark.service.sequence.IdSequenceService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.user.ThirdUserConverter;
import com.xquark.service.user.UserService;
import com.xquark.service.user.vo.WhiteListInfo;
import com.xquark.service.weakLink.WeakLinkService;
import com.xquark.utils.EmojiFilter;
import com.xquark.utils.ResourceResolver;
import com.xquark.utils.StringUtil;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;
import com.xquark.utils.http.PoolingHttpClients;
import com.xquark.utils.unionpay.HttpClient;
import com.xquark.wechat.oauth.ThirdUserBean;
import com.xquark.wechat.oauth.protocol.WechatUserBean;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.*;

import static org.parboiled.common.Preconditions.checkArgument;
import static org.parboiled.common.Preconditions.checkNotNull;

//@Service("userService")
//service已在applicationContext-service.xml中定义
public class UserServiceImpl extends BaseServiceImpl implements UserService {
  //白名单魔法值
  private static final Long MAGIC_NUMBER = 9999999L;
  @Autowired
  private UserMapper userMapper;

  @Autowired
  private PasswordEncoder pwdEncoder;

  @Autowired
  private ShopMapper shopMapper;

  //第三方支付的User
  private LinkedHashMap<PaymentMode, String> thirdPartyUsers;

  @Autowired
  private UserAgentMapper userAgentMapper;

  @Autowired
  private UserTwitterMapper userTwitterMapper;

  @Autowired
  private UserPartnerMapper userPartnerMapper;

  @Autowired
  private UserTeamMapper userTeamMapper;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private InsideEmployeeMapper insideEmployeeMapper;

  @Autowired
  private InsideUserEmployeeMapper userEmployeeMapper;

  @Autowired
  private CustomerProfileMapper customerProfileMapper;

  @Autowired
  private CustomerProfileAuditMapper customerProfileAuditMapper;

  @Autowired
  private CustomerWechartUnionMapper customerWechartUnionMapper;

  @Autowired
  private CareerLevelService careerLevelService;

  @Autowired
  private IdSequenceService idSequenceService;

  @Autowired
  private WeakLinkService weakLinkService;

  @Autowired
  private PromotionWhitelistMapper promotionWhitelistMapper;

  //自己平台的账号ID，目前用于分佣
  @Value("${user.id.kkkd}")
  private String userIdKkkd;

  @Value("${balance.center.url}")
  private String host;

  private static final String DTSPATH="/v1/api/saveCustomer";

  @Value("${shequ.api.host}")
  private String shequHost;

  private static final String SQPATH="/cuser/createUser";

  @Autowired
  private MemberInfoMapper memberInfoMapper;

  @Override
  public String loadKkkdUserId() {
    return userIdKkkd;
  }

  @Override
  public User loadUserByUsername(String username) throws UsernameNotFoundException {
    return userMapper.loadByLoginname(username);
  }

  @Override
//  @Transactional
  public User registerAnonymous(String cid, String partner) {
    // 用户的验证
    if (!cid.startsWith(UniqueNoType.CID.name())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "非法注册用户");
    }
    User u = userMapper.loadByLoginname(cid);
    if (u != null) {
      return u;
    }
    User user = new User();
    user.setLoginname(cid);
    user.setPartner(partner);
    this.insert(user);
    return user;
  }

  @Override
  public User checkUserInfo(String u, String p) {
    User u2 = userMapper.loadByLoginname(u);
    if (u2 == null) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "用户" + u + "不存在");
    }

    if (!pwdEncoder.matches(p, u2.getPassword())) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "密码错误");
    }

    return u2;
  }

  @Override
//  @Transactional
  public User register(String phone, String password) {
    // 用户的验证应该放到这里来，否则有安全问题
    User u = userMapper.loadByLoginname(phone);
    if (u != null) {
      //if(pwdEncoder.matches(password, u.getPassword())) {
      //    return u;
      //}
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "用户[" + phone + "]已经注册，请直接登录或者使用其他的用户名注册");
    }

    User phoneUser = userMapper.loadAnonymousByPhone(phone);
    if (phoneUser != null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "用户[" + phone + "]已经注册，请直接登录或者使用其他的用户名注册");
    }

    User user = new User();
    user.setLoginname(phone);
    user.setPhone(phone);
    // 游客和默认头像
    user.setName(CommonConst.defaultName);
    user.setAvatar(CommonConst.defaultAavtar);

    // 用户注册默认密码为123456
    if (StringUtils.isEmpty(password)) {
      password = "123456";
    }
    // 用户注册默认extUserId为phone
    user.setExtUserId(phone);
    user.setPassword(pwdEncoder.encode(password));
    this.insert(user);
    return user;
  }

  @Override
  public boolean isPwdSet() {
    User user = userMapper.selectByPrimaryKey(getCurrentUser().getId());
    return user != null && user.getPassword() != null;
  }

  @Override
  public boolean changePwd(String oldPwd, String newPwd) {
    User sessionUser = (User) getCurrentUser();
    User u = load(getCurrentUser().getId());

    if (org.apache.commons.lang3.StringUtils.isNotBlank(oldPwd) && !pwdEncoder
        .matches(oldPwd, u.getPassword())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "原密码不正确");
    }

    if (!StringUtils.hasLength(oldPwd) && !StringUtils.hasLength(u.getPassword()) || oldPwd != null
        && u.getPassword() != null && pwdEncoder.matches(oldPwd, u.getPassword())) {
      newPwd = pwdEncoder.encode(newPwd);
      boolean changed = userMapper.changePwd(u.getId(), newPwd) > 0;
      if (changed) {
        u.setPassword(newPwd);
      }
      // 更新session中的密码
      sessionUser.setPassword(newPwd);
      return changed;
    }
    return false;
  }

  @Override
  public boolean modifyYundou(String uId, Long newAmount) {
    User user = this.load(uId);
    Long oldYundou = user.getYundou();
    return userMapper.updateYundou(uId, newAmount + oldYundou) > 0;
  }

  @Override
  public Long getCurrentYundou(String userId) {
    return userMapper.selectCurrentYundou(userId);
  }

  @Override
  public boolean updateFanCardNo(String uId, String fanCardNo) {
    return userMapper.updateFanCardNo(uId, fanCardNo) > 0;
  }

  @Override
  public boolean updateUserInfo(User user) {
    if (!getCurrentUser().getId().equals(user.getId())) {
      return false;
    }
    return userMapper.updateByPrimaryKeySelective(user) > 0;
  }

  @Override
  public boolean updateSCrmUserInfoByUnionId(User user) {
    checkNotNull(user, "用户信息不能为空");
    return userMapper.updateSCrmUserInfoByUnionId(user) > 0;
  }

  @Override
  public boolean syncWechatUserInfo(User user) {
    return userMapper.updateByPrimaryKeySelectiveNoMatterArchived(user) > 0;
  }

  public User loadByWechatParam(String loginName, String weixinCode, Boolean archive) {
    return userMapper.loadByWechatParam(loginName, weixinCode, archive);
  }

  @Override
  public User load(String id) {
    return userMapper.selectByPrimaryKey(id);
  }

  @Override
  public UserEmployeeVO loadUserEmployeeVO(String id) {
    return userMapper.selectEmployeeVOByPrimaryKey(id);
  }

  @Override
  public boolean isRegistered(String loginname) {
    return userMapper.countRegistered(loginname) > 0;
  }

  @Override
  public User loadByMoreParam(String loginname, String weixinCode, String weiboCode, String cid) {

    List<User> userList = userMapper.loadByMoreParam(loginname, weixinCode, weiboCode, cid);
    if (CollectionUtils.isNotEmpty(userList)) {
      if (userList.size() > 1) {
        for (User user : userList) {
          if (!user.getLoginname().startsWith("CID")) {
            return user;
          }
        }
      }
      return userList.get(0);
    }
    return null;
  }

  @Override
  @Transactional
  public int insert(User record, String upline, String channel,GetCpid cpidClass) {
    Long cpId = idSequenceService.generateIdByType(IdSequenceType.MEMBER);
    String customerNumber = String.format("%010d", cpId);

    cpidClass.setCpid(cpId);

    record.setCpId(cpId);
    record.setCustomerNumber(customerNumber);

    int id = userMapper.insert(record);
    addCode(record.getId());

    if (!StringUtils.isEmpty(record.getUnionId())) {
      CustomerProfile customerProfile = new CustomerProfile();
      customerProfile.setCpId(record.getCpId());
      customerProfile.setTinCode(record.getIdCardNum());
      customerProfile.setSource((byte) 3);
      customerProfile.setPhoneNo1(record.getPhone());
      customerProfile.setCustomerNumber(customerNumber);
      customerProfile.setNameCN(record.getName());

      //如果不是推荐进来的默认上级是公司
      if (StringUtil.isNotNull(upline)) {
          customerProfile.setUplineCpId(Long.valueOf(upline));
      }else {
          customerProfile.setUplineCpId(2000002L);
      }
      cpidClass.setUpline(customerProfile.getUplineCpId());
      customerProfileMapper.add(customerProfile);

      CustomerProfileAudit customerProfileAudit  = Transformer.fromBean(customerProfile, CustomerProfileAudit.class);
      customerProfileAudit.setAuditType((byte) 1);
      customerProfileAudit.setAuditDate(new Date(System.currentTimeMillis()));
      customerProfileAudit.setCpId(record.getCpId());
      customerProfileAudit.setAuditUser("E");

      customerProfileAuditMapper.insert(customerProfileAudit);

      CustomerWechatUnion customerWechatUnion = new CustomerWechatUnion();
      customerWechatUnion.setCpId(record.getCpId());
      customerWechatUnion.setUnionId(record.getUnionId());
      customerWechatUnion.setNickName(record.getName());
      customerWechatUnion.setHeadImgurl(record.getAvatar());

      customerWechartUnionMapper.insert(customerWechatUnion);

      CustomerCareerLevel careerLevel = new CustomerCareerLevel();
      careerLevel.setCpId(cpId);
      careerLevelService.save(careerLevel, PlatformType.E);

      //白人入网绑定弱关系
      WeakLink weakLink = new WeakLink();
      weakLink.setCpid(cpId);
      weakLink.setShareCpid(customerProfile.getUplineCpId());
      weakLink.setStatus(1);
      if (StringUtil.isNotNull(channel)) {
        weakLink.setChannel(channel);
      }else {
        weakLink.setChannel("SELF_MOTION");
      }
      weakLinkService.bound(weakLink);

    }

    return id;
  }

  private boolean callAnChangAddCustomerRelationship(Long cpId,Long upline) {
    if (Objects.isNull(cpId)) {
      log.info("方法入参空");
      return false;
    }
    //调用安畅接口，记录用户关系变更
    String path = "/v1/api/addCustomerRelationship";
    log.info("白人入网绑定弱关系进入调用安畅关系绑定接口：" + cpId + ",upline=" + upline);
    String value = memberInfoMapper.selectSystemConfig();
    if (value == "1" || value.equals("1")) {
        Map<String, Object> mapTag = new HashMap<>();

        HttpResponse response = null; //接口返回
        String entity = null;//接口返回值
        mapTag.put("method", "callAnChangAddCustomerRelationship");
        mapTag.put("cpId", cpId);

        //url
        String repUrl =
                new StringBuffer().append(host).append(path)
                        .toString();
        ImmutableMap<String,Long> reParam = ImmutableMap.of("cpid", cpId
                , "upline", upline, "type", 2L, "source", 3L);
        mapTag.put("请求参数", reParam);
        mapTag.put("请求URL", repUrl);
        log.info("白人入网绑定弱关系调用安畅接口接口参数：" + mapTag);

        try {
          response = PoolingHttpClients.postJsonResponse(repUrl, reParam);
        } catch (Exception e) {
          log.error("白人入网绑定弱关系调用安畅接口调用失败" + mapTag + e.getMessage());
          return false;
        }

        if (response == null) {
          log.error("白人入网绑定弱关系调用安畅接口调用失败,返回null" + mapTag);
          return false;
        }

        try {
          entity = EntityUtils.toString(response.getEntity(), "utf-8");
        } catch (IOException e) {
          log.error("白人入网绑定弱关系调用安畅接口解析返回体数据io异常" + e.getMessage());
          return false;
        }

        JSONObject json = JSON.parseObject(entity);
        mapTag.put("返回状态码", response.getStatusLine());
        mapTag.put("返回entity", json);

        int res_code = json.getIntValue("res_code");
        if(res_code == 10){
        log.info("白人入网绑定弱关系调用安畅接口成功" + mapTag);
        return true;
        }
      log.info("白人入网绑定弱关系调用安畅接口失败" + mapTag);
      return false;
    }else{
      log.info("白人入网绑定弱关系调用安畅接口未开启！");
    }
  return false;
  }



  /**
   * dts新建用户调用安畅接口
   * @param cpId
   * @param upline
   * @return
   */
  private boolean addUserInfoToCenter(Long cpId,Long upline){
    if(Objects.isNull(cpId)){
      log.info("方法入参空");
      return false;
    }
    log.info("创建用户请求安畅接口cpId{},upline{}",cpId,upline);
    Map<String,Object> mapTag = new HashMap<>();

    HttpResponse response = null; //接口返回
    String entity = null;//接口返回值
    mapTag.put("method", "addUserInfoToCenter");
    mapTag.put("cpId", cpId);

    //url
    String repUrl =
            new StringBuffer().append(host).append(DTSPATH)
                    .toString();
    ImmutableMap<String, Long> reParam = ImmutableMap.of("cpid",cpId
            ,"upline_cpid",upline);
    mapTag.put("请求参数", reParam);
    mapTag.put("请求URL", repUrl);
    log.info("请求安畅创建用户接口参数："+mapTag);

    try {
      response = PoolingHttpClients.postJsonResponse(repUrl, reParam);
    }catch (Exception e){
      log.error("模拟DTS新建用户接口调用失败"+mapTag + e.getMessage());
      return false;
    }

    if(response == null){
      log.error("模拟DTS新建用户接口调用失败,返回null"+mapTag);
      return false;
    }

    try {
      entity = EntityUtils.toString(response.getEntity(), "utf-8");
    } catch (IOException e) {
      log.error("模拟DTS新建用户接口解析返回体数据io异常"+e.getMessage());
      return false;
    }

    JSONObject json = JSON.parseObject(entity);
    mapTag.put("返回状态码", response.getStatusLine());
    mapTag.put("返回entity", json);
    log.info("安畅接口调用成功：cpid为：" + cpId+",upline_cpid为："+upline+",返回结果为：" + mapTag);
    return true;
  }

  @Override
  public int insert(User record) {
    GetCpid cpidClass=new GetCpid();
    int insert=insert(record, null,null,cpidClass);
    addUserInfoToCenter(cpidClass.getCpid(),2000002L);
    callAnChangAddCustomerRelationship(cpidClass.getCpid(),2000002L);
    return insert;
  }

  @Override
  public int insertOrder(User user) {
    GetCpid cpidClass=new GetCpid();
    int insert=insert(user, null,null,cpidClass);
    addUserInfoToCenter(cpidClass.getCpid(),2000002L);
    //白人入網通知安暢綁定弱關係
    callAnChangAddCustomerRelationship(cpidClass.getCpid(),2000002L);

    return insert;
  }

  @Override
  public int delete(String id) {
    return userMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int undelete(String id) {
    return userMapper.undeleteByPrimaryKey(id);
  }

  @Override
  public boolean emptyUserPassword(String mobile) {
    return userMapper.emptyUserPasswordByLoginname(mobile);
  }

  @Override
  public boolean emptyUserPassword(String mobile, String code) {
    // validate mobile and code
    User user = userMapper.loadAnonymousByPhone(mobile);
    if (user == null) {
      return false;
    }
    String userId = user.getId();
    return userMapper.emptyUserPassword(userId);
  }

  @Override
  public User loadByLoginname(String loginname) {
    return userMapper.loadByLoginname(loginname);
  }

  @Override
  public LinkedHashMap<PaymentMode, String> getThirdPartyUsers() {
    return thirdPartyUsers;
  }

  public void setThirdPartyUsers(LinkedHashMap<PaymentMode, String> thirdPartyUsers) {
    this.thirdPartyUsers = thirdPartyUsers;
  }

  @Override
  public int updateNameAndIdCardNumByPrimaryKey(String id, String name, String idCardNum) {
    return userMapper.updateNameAndIdCardNumByPrimaryKey(id, name, idCardNum);
  }

  @Override
  public User registerExtUser(String partner, String extUserId, String userName, String avatar) {
    String loginname = extUserId + "@" + partner;
    User u = userMapper.loadByLoginname(loginname);
    if (u != null) {
      return u;
    }
    User user = new User();
    user.setLoginname(loginname);
    user.setName(userName);
    user.setAvatar(avatar);
    user.setPartner(partner);
    user.setExtUserId(extUserId);
    this.insert(user);
    return user;
  }

  @Override
  public int saveWithDrawType(String id, int type) {
    return userMapper.saveWithDrawType(id, type);
  }

  @Override
  public User loadByAdmin(String id) {
    return userMapper.selectByPKAndAdmin(id);
  }

  @Override
  public List<User> getFeeSplitAcct() {
    return userMapper.loadByRoles(2L);
  }

  @Override
  public User loadExtUser(String domain, String extUid) {
    User user = userMapper.selectByDomainAndExtUid(domain, extUid);
    if (user == null) {
      String loginname = extUid + "@" + domain;
      User u = userMapper.loadByLoginname(loginname);
      if (u != null) {
        return u;
      }
      User newUser = new User();

      newUser.setLoginname(loginname);
      newUser.setName("name@" + domain);
      newUser.setAvatar("avatar@" + domain);
      newUser.setPartner(domain);
      newUser.setExtUserId(extUid);
      try { //并发时
        this.insert(newUser);
      } catch (Exception e) {
        newUser = userMapper.selectByDomainAndExtUid(domain, extUid);
      }
      return newUser;
    }
    return user;
  }

  @Override
  public User loadExtUserByUid(String extUid) {
    User user = userMapper.selectByExtUid(extUid);
    return user;
  }

  @Override
  public List<User> listNoCodeUsers() {
    return userMapper.listNoCodeUsers();
  }

  @Override
  public void addCode(String... ids) {
    if (ids == null || ArrayUtils.isEmpty(ids)) {
      return;
    }

    for (String p : ids) {
      userMapper.addCode(p);
    }
  }

  @Override
  public SellerInfoVO getSellerInfoVO(String id) {
    User user = load(id);
    Shop shop = shopMapper.selectByUserId(user.getId());
    SellerInfoVO vo = new SellerInfoVO();
    BeanUtils.copyProperties(user, vo);
    if (shop != null) {
      if (shop.getName() != null) {
        vo.setUserShopName(shop.getName());
      }
      if (shop.getServicePhone() != null && !shop.getServicePhone().equals("")) {
        vo.setServicePhone(shop.getServicePhone());
      } else {
        vo.setServicePhone(user.getPhone());
      }
    }
    return vo;
  }

  @Override
  public User loadAnonymousByPhone(String phone) {
    return userMapper.loadAnonymousByPhone(phone);
  }

  /**
   * 根据用户名更新部分用户信息
   */
  @Override
  public int updateByCidSelective(User user) {
    checkNotNull(user);
    checkArgument(!StringUtils.isEmpty(user.getLoginname()), "用户名不能为空");
    return userMapper.updateByCidSelective(user);
  }

  private void updateCustomerProfile(User user) {
    CustomerProfile customerProfile = new CustomerProfile();
    customerProfile.setCpId(user.getCpId());
    customerProfile.setTinCode(user.getIdCardNum());
    customerProfile.setPhoneNo1(user.getPhone());
    customerProfile.setUpdatedBy(getCurrentUser().getName());
    customerProfile.setNameCN(user.getName());

    customerProfileMapper.merge(customerProfile);

    CustomerProfileAudit customerProfileAudit = new CustomerProfileAudit();
    customerProfileAudit.setAuditType((byte) 2);
    customerProfileAudit.setCpId(user.getCpId());

    customerProfileAuditMapper.insert(customerProfileAudit);

    CustomerWechatUnion customerWechatUnion = new CustomerWechatUnion();
    customerWechatUnion.setCpId(user.getCpId());
    customerWechatUnion.setUnionId(user.getUnionId());
    customerWechatUnion.setNickName(user.getName());
    customerWechatUnion.setHeadImgurl(user.getAvatar());

    customerWechartUnionMapper.update(customerWechatUnion);
  }

  /**
   * 根据unionId更新部分用户信息
   */
  @Override
  @Transactional
  public int updateByUnionIdSelective(User user) {
    checkNotNull(user);
    checkArgument(!StringUtils.isEmpty(user.getUnionId()), "unionId不能为空");

//    updateCustomerProfile(user);

    return userMapper.updateByUnionIdSelective(user);
  }

  @Override
  @Transactional
  public int updateByCpIdSelective(User user) {
    checkNotNull(user);
    checkArgument(user.getCpId() != null, "cpId不能为空");

//    updateCustomerProfile(user);

    int ret = userMapper.updateByCpIdSelective(user);
    customerProfileMapper.updatePhone(user.getCpId(), user.getPhone());
    return ret;
  }

  @Override
  @Transactional
  public int updateAnonymousPhone(String cid, String phone) {
    User user = userMapper.loadByLoginname(cid);
    user.setPhone(phone);
//    updateCustomerProfile(user);

    return userMapper.updateAnonymousPhone(cid, phone);
  }

  @Override
  public User createAnonymous(String partner, String phone, String cid) {
    User user = new User();
    user.setPartner(partner);
    user.setPhone(phone);
    user.setLoginname(cid);
    this.insert(user);
    return user;
  }

  @Override
  public <T extends ThirdUserConverter, U extends ThirdUserBean>
  User createUserFromThirdUserIfNotExists(@NotNull T converter,
      @NotNull U thirdUser, String upline, String channel) {
    String unionId = thirdUser.getUnionId();
    // TODO 中台用户unionId不唯一, 如何进行小程序登录
    User dbUser = this.loadByUnionId(unionId);
    // 如果该用户没有头像说明是scrm系统创建的用户, 此时获取微信信息更新头像及昵称
    if (dbUser != null) {
      boolean needUpdateUser = StringUtils.isEmpty(dbUser.getAvatar()) ||
          StringUtils.isEmpty(dbUser.getLoginname());
      // 没有头像或者没有appId的用户更新微信用户信息
      if (needUpdateUser && (thirdUser instanceof WechatUserBean)) {
        updateWechatUserInfo(dbUser, (WechatUserBean) thirdUser);
      }
      return dbUser;
    }
    User userFromThirdUser = createUserFromThirdUser(converter, thirdUser, upline, channel);

    //创建用户请求安畅接口
    addUserInfoToCenter(userFromThirdUser.getCpId(),org.apache.commons.lang3.StringUtils.isNotBlank(upline)?Long.valueOf(upline):2000002L);
    callAnChangAddCustomerRelationship(userFromThirdUser.getCpId(), org.apache.commons.lang3.StringUtils.isNotBlank(upline)?Long.valueOf(upline):2000002L);

    //新用户注册，调用社区接口，新增用户到社区用户表
    this.addCommunityUser(userFromThirdUser);
    return userFromThirdUser;
  }

  @Transactional(rollbackFor = Exception.class)
  public void updateWechatUserInfo(User dbUser, WechatUserBean wechatUser) {
    dbUser.setAvatar(wechatUser.getAvatar());
    String nickname = EmojiFilter.filterEmoji(wechatUser.getNickName());
    // 防止微信昵称全部是emoji符号，导致返回的昵称为空，则默认赋值普通用户
    if (nickname == null || "".equals(nickname)) {
      nickname = "普通用户";
    }
    dbUser.setName(nickname);
    dbUser.setLoginname(wechatUser.getOpenId());
    dbUser.setPassword(pwdEncoder.encode("123456"));
    dbUser.setCode(dbUser.getId());
    this.createShopIfNotExists(dbUser);
    this.updateSCrmUserInfoByUnionId(dbUser);
  }

  @Override
  @Transactional
  public <T extends ThirdUserConverter<U>, U extends ThirdUserBean>
  User createUserFromThirdUser(@NotNull T converter,
      @NotNull U thirdUser, String upline, String channel) {

    String unionId = thirdUser.getUnionId();
    // TODO 中台用户unionId不唯一
    User user = this.loadByUnionId(unionId);
    if (user != null) {
      return user;
    }
    user = converter.fromThirdUser(thirdUser);
    GetCpid cpidClass=new GetCpid();
    // 创建用户时生成cpId
    // this.insert(user);
    insert(user, upline, channel,cpidClass);

    // app微信登陆，创建用户的同时也要创建对应的店铺
    createShopIfNotExists(user);

    //如果不是推荐进来的默认上级是公司
    if (StringUtil.isNull(upline)) {
      upline="2000002";
    }



//        if (notifySCrm) {
//        	sCrmHttpUtils.postSyncUserAsync(user.getPhone(), user.getUnionId());
//        }
    return user;
  }

  @Override
  public Shop createShopIfNotExists(User user) {
    Shop shop = shopMapper.selectByUserId(user.getId());
    if (shop != null) {
      if (StringUtils.isEmpty(user.getShopId())) {
        user.setShopId(shop.getId());
        userMapper.updateByPrimaryKeySelective(user);
      }
      return shop;
    }
    shop = new Shop();
    shop.setName(user.getName() + "的小店");
    shop.setWechat(user.getName());
    shop.setOwnerId(user.getId());
    shop.setImg(user.getAvatar());
    this.createShop(user.getId(), shop);

    user.setShopId(shop.getId());
    //在APP中的卖家,将当前的访问店铺自动设置为卖家自身的shopid
    user.setCurrentSellerShopId(shop.getId());
    // 将shopId再更新到用户中
    userMapper.updateByPrimaryKeySelective(user);
    return shop;
  }

  @Override
  public boolean updateByBosUserInfo(User user) {
    return userMapper.updateByPrimaryKeySelective(user) > 0;
  }

  @Override
  public List<User> listUsersByRootShopId(String rootShopId, String name, String phone,
      Pageable pageable) {
    return userMapper.listUsersByRootShopId(rootShopId, name, phone, pageable);
  }

  @Override
  public Long countUsersByRootShopId(String rootShopId, String name, String phone) {
    return userMapper.countUsersByRootShopId(rootShopId, name, phone);
  }

  /**
   * 获取当前用户的用户类型
   */
  @Override
  public UserType getUserType(String userId) {
    UserType type = UserType.DEFAULT;
    UserAgentVO userAgentVO = userAgentMapper.selectByUserId(userId);
    //如果在userAgent中存在则为b2b用户
    if (userAgentVO != null) {
      AgentStatus agentStatus = userAgentVO.getStatus();
      if (agentStatus == AgentStatus.ACTIVE) {
        type = UserType.B2B;
      } else if (agentStatus == AgentStatus.APPLYING) {
        type = UserType.B2B_APPLYING;
      } else if (agentStatus == AgentStatus.FROZEN) {
        type = UserType.B2B_FROZEN;
      }
    }
    // 如果在UserTwitter中存在则为b2c用户
    else {
      List<UserTwitter> userTwitters = userTwitterMapper.selectByUserId(userId);
      if (userTwitters != null) {
        for (UserTwitter userTwitter : userTwitters) {
          TwitterStatus twitterStatus = userTwitter.getStatus();
          if (twitterStatus == TwitterStatus.ACTIVE) {
            type = UserType.B2C;
          } else if (twitterStatus == TwitterStatus.APPLYING) {
            type = UserType.B2C_APPLYING;
          }
        }
      }
    }
    return type;
  }

  /**
   * 获取当前用户的信息，包含用户，推客，合伙人，战队信息
   */
  @Override
  public UserInfoVO getUserInfo(String userId) {
    UserInfoVO info = new UserInfoVO();
    User userInfo = userMapper.selectByPrimaryKey(userId);
    info.setUser(userInfo);
    List<UserTwitter> userTwitters = userTwitterMapper.selectByUserId(userId);
    if (userTwitters != null) {
      for (UserTwitter userTwitter : userTwitters) {
        info.setTwitter(userTwitter);
      }
    }
    UserPartner partner = userPartnerMapper.selectUserPartnersByUserId(userId);
    info.setPartner(partner);
    UserTeam userTeam = userTeamMapper.selectByUserId(userId);
    info.setTeam(userTeam);
    return info;
  }

  /**
   * 保存微信二维码名片
   */
  @Override
  public boolean updateWxQRcodeCard(String id, String qrcode) {
    return userMapper.updateWxQRcodeCard(id, qrcode) == 1;
  }

  /**
   * 获取微信二维码名片
   */
  @Override
  public String getWxQRcodeCard(String id) {
    String qrcode = userMapper.getWxQRcodeCard(id);
    if (StringUtils.isEmpty(qrcode)) {
      return "";
    } else {
      ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
          .getBean("resourceFacade");
      return resourceFacade.resolveUrl(qrcode);
    }
  }

  // FIXME wangxinhua
  // 该方法是从shopService中复制过来的
  // 因为在这里引入shopService会导致循环依赖
  // 解决Spring循环依赖的问题, 使用shopService创建shop
  protected Shop createShop(String userId, Shop shop) {
    //Shop chkShop = findByName(shop.getName());
    /**if (chkShop != null) {
     throw new BizException(GlobalErrorCode.UNKNOWN,
     "商铺已经存在 shopName=[" + shop.getName() + "]");
     }**/

    shop.setOwnerId(userId);
    Shop existShop = shopMapper.selectByUserId(userId);
    if (existShop == null) {
      shop.setStatus(ShopStatus.ACTIVE);
      shop.setDanbao(true);
      // 店铺默认头像
      if (org.apache.commons.lang3.StringUtils.isEmpty(shop.getImg())) {
        shop.setImg("http://images.handeson.com/Fnr5rtXEssim1NVarB-sCoa24ZJf");
      }
      int id = shopMapper.insert(shop);
      shopMapper.addCode(shop.getId());

      User userUpdate = new User();
      userUpdate.setId(userId);
      userUpdate.setShopId(shop.getId());
      this.updateByBosUserInfo(userUpdate);
      //user.setShopId(shop.getId());

      // 暂时写死rootshopid modify by chh 2016-12-02
      String rootShopId = "aj6wd1mm";
      // 增加shop的同时，也要维护shoptree对应关系
      ShopTree shopTree = new ShopTree();
      shopTree.setRootShopId(rootShopId);
      shopTree.setAncestorShopId(rootShopId);
      shopTree.setDescendantShopId(shop.getId());
      shopTreeService.insertShopTree(shopTree);

    } else {
      //user.setShopId(existShop.getId());
      shop = existShop;
    }
    return shop;
  }

  /**
   * 将匹配入参id的cid2的数据都清空
   */
  @Override
  public int updateCid2Null(String id) {
    return userMapper.updateCid2Null(id);
  }

  /**
   * 将匹配入参id的cid3的数据都清空
   */
  @Override
  public int updateCid3Null(String id) {
    return userMapper.updateCid3Null(id);
  }

  @Override
  public User loadByUnionId(String unionId) {
    return userMapper.loadByUnionId(unionId);
  }

  @Override
  public User loadByCpId(Long cpId) {
    return userMapper.selectByCpId(cpId);
  }

  /**
   * 更新用户积分
   */
  @Override
  public int updateYundouAdd(String userId, String yundou) {
    return userMapper.updateYundouAdd(userId, yundou);
  }

  @Override
  public boolean isInsideEmployee(Long emplyeeId, String name) {
    return insideEmployeeMapper.isInsideEmployee(emplyeeId, name);
  }

  @Override
  public boolean isRegistedEmployee(String userId) {
    return userEmployeeMapper.selectIsUserExists(userId);
  }

  @Override
  public boolean saveUserEmployee(InsideUserEmployee userEmployee) {
    return userEmployeeMapper.insert(userEmployee) > 0;
  }

  @Override
  public boolean removeEmployee(Long employeeId) {
    return userEmployeeMapper.deleteByEmployeeId(employeeId) > 0;
  }

  @Override
  @Transactional
  public boolean updateEmployeeId(String userId, Long newId) {
    InsideUserEmployee userEmployee = userEmployeeMapper.selectByUserId(userId);
    if (userEmployee == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该用户没有注册为内部员工");
    }
    Long oldId = userEmployee.getEmployeeId();
    insideEmployeeMapper.updateEmployeeId(oldId, newId);
    insideEmployeeMapper.updateEmployeeIdQinyuan(oldId, newId);
    return userEmployeeMapper.updateByUserIdSelective(new InsideUserEmployee(userId, newId)) > 0;
  }

  @Override
  public boolean selectUserExistsByUserId(String userId) {
    return userMapper.selectUserExistsByUserId(userId);
  }

  @Override
  public boolean selectUserExistsByUnionId(String unionId) {
    return userMapper.selectUserExistsByUnionId(unionId);
  }

  @Override
  public boolean selectUserExistsByCpId(Long cpId) {
    return userMapper.selectUserExistsByCpId(cpId);
  }

  @Override
  public boolean hasEmployeeId(Long id) {
    return userEmployeeMapper.selectIsEmployeeIdInUser(id);
  }

  @Override
  public Long selectCpIdByUserId(String userId) {
    return userMapper.selectCpIdByUserId(userId);
  }

  @Override
  public User selectUserIdByCpId(String cpId) {
    return userMapper.selectUserIdByCpId(cpId);
  }

  @Override
  public User selectUserByUserId(String userId){
    return userMapper.selectUserByUserId(userId);
  }

  /**
   * 各个模块白名单校验入口
   * 如需开启一个新的模块白名单要在WhiteListInfo类中加一个字段作为标记
   * @param cpId
   * @return
   */
  @Override
  public WhiteListInfo checkIsWhite(Long cpId) {
    WhiteListInfo whiteListInfo = new WhiteListInfo();
    //邀请码
    boolean isShowInviteCode = this.checkModulIsWhiteInfo(cpId,"showInviteCode");
    if(isShowInviteCode){
      whiteListInfo.setShowInviteCode(1);
    }
    //在线客服
    boolean isShowOnlineService = this.checkModulIsWhiteInfo(cpId,"showOnlineService");
    if(isShowOnlineService){
      whiteListInfo.setShowOnlineService(1);
    }
    return whiteListInfo;
  }

  /**
   * 依据不同模块校验是否在各个模块的白名单中
   * @param cpId
   * @param type
   * @return
   */
  private boolean  checkModulIsWhiteInfo(Long cpId, String type) {
    if(cpId == null || org.apache.commons.lang3.StringUtils.isBlank(type)){
      return false;
    }
    //是否开启白名单
      boolean isOpen = this.promotionWhitelistMapper.checkMettingIsOpen(type, MAGIC_NUMBER);
    //开启
    if(isOpen){
        //是否在白名单中
        boolean isWhiteExist = this.promotionWhitelistMapper
                .checkIsWhiteExist(type, cpId);

        if(isWhiteExist)
            return true;

    }else {
        //关闭
        return true;
    }
    return false;
  }

  @Override
  public boolean updateRegisterPointFlag(Long cpId, int flag) {
    return this.userMapper.updateRegisterPointFlag(cpId, flag);
  }

  @Override
  public String selectIdType(String cpId) {
    return userMapper.selectIdType(cpId);
  }

  @Override
  public String selectViViType(String cpId) {
    return userMapper.selectViViType(cpId);
  }

  @Override
  public String selectIdTypeByPhone(String phone) {
    return userMapper.selectIdTypeByPhone(phone);
  }

  @Override
  public String selectIdTypeByUnionId(String unionId) {
    return userMapper.selectIdTypeByUnionId(unionId);
  }

    @Override
    public String selectIdTypeByExtUid(String extUid) {
        return userMapper.selectIdTypeByExtUid(extUid);
    }


  @Override
  public int selectExpOfficerByCpId(Long cpId) {
    return userMapper.selectExpOfficerByCpId(cpId);
  }


  @Override
  public boolean selectIsFreeLogisticUsed(Long cpId) {
    return userMapper.selectIsFreeLogisticUsed(cpId);
  }

  @Override
  public boolean updateFreeLogistic(Long cpId, Boolean status) {
    User user = new User();
    user.setCpId(cpId);
    user.setFreeLogisticUsed(status);
    return userMapper.updateByCpIdSelective(user) > 0;
  }

  @Override
  public List<User> queryUserByPhone(String mobile) {
    return userMapper.queryUserByPhone(mobile);
  }

  @Override
  public void updateTinCode(Long cpId, String tinCode) {
    userMapper.updateTinCode(cpId,tinCode);
  }

  @Override
  public void updateHeadImageByUnionId(String headImage, String unionId){
    userMapper.updateHeadImageByUnionId(headImage,unionId);
    userMapper.updateWechatHeadImageByUnionId(headImage,unionId);
  }

  @Override
  public void updateNickNameByUnionId(String unionId,String nickName){

    userMapper.updateWechatNickNameByUnionId(unionId,nickName);
  }

  /**
   * 将新用户的信息保存到社区用户表
   *
   */
  private boolean addCommunityUser(User user){
    String url = new StringBuffer().append(shequHost).append(SQPATH).toString();
    ImmutableMap<String, String> reParam = ImmutableMap.of("cpId",String.valueOf(user.getCpId())
            ,"img",user.getAvatar(),"nickName",user.getName());
    Map<String,Object> mapTag = new HashMap<>();

    HttpResponse response = null; //接口返回
    String entity = null;//接口返回值
    mapTag.put("method", "addCommunityUser");
    mapTag.put("cpId", user.getCpId());
    mapTag.put("请求参数", reParam);
    mapTag.put("请求URL", url);
    log.info("请求社区创建用户接口参数："+ mapTag);

    try {
      response = PoolingHttpClients.postJsonResponse(url, reParam);
    }catch (Exception e){
      log.error("社区新建用户接口调用失败"+mapTag + e.getMessage());
      return false;
    }

    if(response == null){
      log.error("社区新建用户接口调用失败,返回null"+mapTag);
      return false;
    }

    try {
      entity = EntityUtils.toString(response.getEntity(), "utf-8");
    } catch (IOException e) {
      log.error("社区新建用户接口解析返回体数据io异常"+e.getMessage());
      return false;
    }

    JSONObject json = JSON.parseObject(entity);
    mapTag.put("返回状态码", response.getStatusLine());
    mapTag.put("返回entity", json);
    log.info("社区接口调用成功：cpId为：" + user.getCpId()+",img为："+user.getAvatar()+",nickName为:" +user.getName() +
            ",返回结果为：" + mapTag);
    return true;
  }

}
