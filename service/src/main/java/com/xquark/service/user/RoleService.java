package com.xquark.service.user;

import com.xquark.dal.model.Role;
import java.util.List;
import org.springframework.data.domain.Pageable;

/**
 * Created by chh on 16-12-13. 用户对应角色Service
 */
public interface RoleService {

  Role selectByPrimaryKey(String id);

  int insert(Role role);

  int modify(Role role);

  int delete(String id);

  /**
   * 列表查询页面
   */
  List<Role> list(Pageable page, String keyword);

  List<Role> listAll();

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(String keyword);

  /**
   * 根据编码和归属获取对应角色信息
   */
  Role selectByCodeAndBelong(String code, String belong);

  /**
   * 查询某个归属内的所有角色信息
   */
  List<Role> listByBelong(String belong);

}
