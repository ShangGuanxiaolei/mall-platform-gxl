package com.xquark.service.user;

import com.xquark.service.user.vo.UserLogInfo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/4/21
 * Time:15:21
 * Des:用户日志记录
 */
public interface UserLogService {
    boolean insertUserLog(UserLogInfo userLogInfo);
}