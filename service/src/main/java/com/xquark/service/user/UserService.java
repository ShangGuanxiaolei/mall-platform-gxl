package com.xquark.service.user;

import com.xquark.aop.anno.NotNull;
import com.xquark.dal.model.InsideUserEmployee;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.dal.status.UserType;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.vo.SellerInfoVO;
import com.xquark.dal.vo.UserEmployeeVO;
import com.xquark.dal.vo.UserInfoVO;
import com.xquark.service.ArchivableEntityService;
import com.xquark.service.user.vo.WhiteListInfo;
import com.xquark.wechat.oauth.ThirdUserBean;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author jamesp
 */
@Validated
public interface UserService extends UserDetailsService, ArchivableEntityService<User> {

  User registerAnonymous(String cid, String partner);

  /**
   * 手机号码注册，若已存在，直接返回该对象
   */
  User register(String loginname, String password);

  User registerExtUser(String partner, String extUserId, String userName, String avatar);

  /**
   * 修改密码
   *
   * @param oldPwd can be null
   * @param newPwd not null
   * @return changed?
   */
  boolean changePwd(String oldPwd, String newPwd);

  boolean modifyYundou(String uId, Long newAmount);

  /**
   * 查询用户当前积分数量
   *
   * @param userId 用户id
   * @return 用户当前积分
   */
  Long getCurrentYundou(String userId);

  boolean updateFanCardNo(String uId, String fanCardNo);

  /**
   * @return 当前用户是否已设置密码
   */
  boolean isPwdSet();

  /**
   * 更新用户姓名，身份证号码
   */
  boolean updateUserInfo(User user);

  boolean updateSCrmUserInfoByUnionId(User user);

  /**
   * 同步用户信息
   */
  boolean syncWechatUserInfo(User user);


  User loadByWechatParam(String loginName, String weixinCode, Boolean archive);


  UserEmployeeVO loadUserEmployeeVO(String id);

  /**
   * 用户是否已注册
   */
  boolean isRegistered(String loginname);

  /**
   * 通过登录用户名加载用户信息
   */
  User loadByLoginname(String loginname);

  /**
   * 通过多数据加载用户信息
   */
  User loadByMoreParam(String loginname, String weixinCode, String weiboCode, String cid);

  /**
   * 用户是否已注册
   */
  User loadExtUser(String domain, String extUid);

  /**
   * 根据汇购网用户id返回对应代理用户
   */
  User loadExtUserByUid(String extUid);

  int insert(User record, String upline,String channel,GetCpid cpidClass);

  boolean emptyUserPassword(String mobile);

  boolean emptyUserPassword(String mobile, String code);

  String loadKkkdUserId();

  LinkedHashMap<PaymentMode, String> getThirdPartyUsers();

  int updateNameAndIdCardNumByPrimaryKey(String id, String name, String idCardNum);

  int saveWithDrawType(String id, int type);

  User loadByAdmin(String id);

  List<User> getFeeSplitAcct();

  User checkUserInfo(String u, String p);

  List<User> listNoCodeUsers();

  void addCode(String... ids);

  SellerInfoVO getSellerInfoVO(String id);

  User loadAnonymousByPhone(String phone);

  int updateByCidSelective(User user);

  int updateByUnionIdSelective(User user);

  int updateByCpIdSelective(User user);

  int updateAnonymousPhone(String cid, String phone);

  User createAnonymous(String partner, String phone, String cid);

  /**
   * 如果本地用户不存在则根据第三方用户转换并保存
   *
   * @param <T> 转换器泛型
   * @param <U> 用户泛型
   * @param converter 将第三方用户转换为本地用户
   * @param thirdUser 第三方用户
   * @param upline
   * @return 创建的本地用户 或已存在的用户
   */
  <T extends ThirdUserConverter, U extends ThirdUserBean>
  User createUserFromThirdUserIfNotExists(@NotNull T converter,
      @NotNull U thirdUser, String upline, String channel);

  /**
   * 根据第三方用户及创建类创建本地类
   *
   * @param <T> 转换器泛型
   * @param <U> 用户泛型
   * @param converter 将第三方用户转换为本地用户
   * @param thirdUser 第三方用户
   * @param upline
   * @return 创建的本地用户
   */
  @Transactional
  <T extends ThirdUserConverter<U>, U extends ThirdUserBean> User createUserFromThirdUser(
      @NotNull T converter,
      @NotNull U thirdUser, String upline,String channel);

  @Transactional
  Shop createShopIfNotExists(User user);

  /**
   * bos开店更新用户姓名，身份证号码
   */
  boolean updateByBosUserInfo(User user);

  List<User> listUsersByRootShopId(String rootShopId, String name, String phone, Pageable pageable);

  Long countUsersByRootShopId(String rootShopId, String name, String phone);

  /**
   * 获取当前用户的用户类型
   */
  UserType getUserType(String userId);

  /**
   * 上传微信二维码名片
   */
  boolean updateWxQRcodeCard(String id, String qrcode);

  /**
   * 获取微信二维码名片
   */
  String getWxQRcodeCard(String id);

  /**
   * 将匹配入参id的cid2的数据都清空
   */
  int updateCid2Null(String id);

  /**
   * 将匹配入参id的cid3的数据都清空
   */
  int updateCid3Null(String id);

  /**
   * 根据unionid返回对应用户信息
   */
  /**
   * @deprecated 根据unionid返回对应用户信息 中台用户unionId不唯一
   */
  @Deprecated
  User loadByUnionId(String unionId);

  /**
   * 获取当前用户的信息，包含用户，推客，合伙人，战队信息
   */
  UserInfoVO getUserInfo(String userId);

  User loadByCpId(Long cpId);

  /**
   * 更新用户积分
   */
  int updateYundouAdd(String userId, String yundou);

  /**
   * 查询是否是内部员工
   *
   * @param employeeId 员工编号
   * @param name 员工姓名
   * @return 查询结果
   */
  boolean isInsideEmployee(Long employeeId, String name);

  /**
   * 查询该用户是否已经注册为内部员工
   *
   * @param userId 用户id
   * @return 查询结果
   */
  boolean isRegistedEmployee(String userId);

  /**
   * 查询某个工号是否已被注册
   */
  boolean hasEmployeeId(Long id);

  /**
   * 保存用户员工对应关系
   *
   * @param userEmployee 用户员工关系对象
   * @return 保存结果
   */
  boolean saveUserEmployee(InsideUserEmployee userEmployee);

  /**
   * 删除内部员工
   *
   * @param employeeId 员工编号
   * @return 删除结果
   */
  boolean removeEmployee(Long employeeId);

  /**
   * 更新员工号
   */
  boolean updateEmployeeId(String userId, Long newId);

  /**
   * 根据用户id查询用户是否存在
   */
  boolean selectUserExistsByUserId(String userId);

  /**
   * 根据unionId查询用户是否存在
   */
  boolean selectUserExistsByUnionId(String unionId);

  boolean selectUserExistsByCpId(Long cpId);

  /**
   * 根据用户id查询cpId
   *
   * @param userId 用户id
   * @return cpId
   */
  Long selectCpIdByUserId(String userId);

  boolean selectIsFreeLogisticUsed(Long cpId);

  boolean updateFreeLogistic(Long cpId, Boolean status);

  List<User> queryUserByPhone(String mobile);

  void updateTinCode(Long cpId, String tinCode);

  User selectUserIdByCpId(String cpId);

  User selectUserByUserId(String userId);

  /**
   * 更新新注册用户德分标记
   * @param cpId
   * @param flag
   * @return
   */
  boolean updateRegisterPointFlag(Long cpId, int flag);
  String selectIdType(String cpId);

  String selectViViType(String cpId);

  String selectIdTypeByPhone(String phone);

  String selectIdTypeByUnionId(String unionId);

  String selectIdTypeByExtUid(String extUid);

  int selectExpOfficerByCpId(Long cpId);

  void updateHeadImageByUnionId(String image,String unionId);

  void updateNickNameByUnionId(String unionId,String nickName);




  /**
   * 是否在白名单中
   * @param cpId
   * @return
   */
  WhiteListInfo checkIsWhite(Long cpId);


  public  class GetCpid{
    private Long cpid;
    private Long upline;

    public Long getUpline() {
      return upline;
    }

    public void setUpline(Long upline) {
      this.upline = upline;
    }

    public Long getCpid() {
      return cpid;
    }

    public void setCpid(Long cpid) {
      this.cpid = cpid;
    }
  }
}