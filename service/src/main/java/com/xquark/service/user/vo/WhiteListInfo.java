package com.xquark.service.user.vo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/25
 * Time:15:46
 * Des:白名单模块信息
 */
public class WhiteListInfo {
    private int showInviteCode; //是否显示邀请码 0：不显示；1：显示
    private int showOnlineService; //是否显示在线客服入口 0：不显示；1：显示

    public int getShowInviteCode() {
        return showInviteCode;
    }

    public void setShowInviteCode(int showInviteCode) {
        this.showInviteCode = showInviteCode;
    }

    public int getShowOnlineService() {
        return showOnlineService;
    }

    public void setShowOnlineService(int showOnlineService) {
        this.showOnlineService = showOnlineService;
    }
}