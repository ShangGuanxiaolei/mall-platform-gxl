package com.xquark.service.user;

import com.xquark.dal.model.Role;
import com.xquark.dal.model.RolePrice;
import com.xquark.dal.vo.RolePriceVO;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by chh on 16-12-13. 用户角色对应商品价格Service
 */
public interface RolePriceService {

  RolePriceVO selectByPrimaryKey(String id);

  int insert(RolePrice role);

  int modify(RolePrice role);

  int delete(String id);

  /**
   * 列表查询页面
   */
  List<RolePriceVO> list(Pageable page, String keyword);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(String keyword);

  RolePriceVO selectByProductAndRole(String productId, String roleId);

}
