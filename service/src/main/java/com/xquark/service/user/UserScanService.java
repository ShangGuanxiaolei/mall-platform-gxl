package com.xquark.service.user;

import com.xquark.service.user.vo.ShopInfo;
import com.xquark.service.user.vo.UserScan;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/19
 * Time:20:52
 * Des:用户扫码相关
 */
public interface UserScanService {
    /**
     * 赋能新人扫码重定向页面
     * @param cpId
     * @param type
     * @return
     */
    String scanRedirectPage(Long cpId,Long fromCpId, String type,String spreaderId);

    /**
     * 赋能商家支付扫码重定向页面
     * @param cpId
     * @param type
     * @param spreaderId
     * @return
     */
    String shopPayScanRedirectPage(Long cpId,Long fromCpId, String type,String spreaderId);

    /**
     * 商家码
     * @param cpId
     * @param type
     * @param spreaderId 推广码
     * @return
     */
    String selfBusinessScanCode(Long cpId,Long fromCpId,String type,String spreaderId);

    /**
     * 个人码
     * @param cpId
     * @param type
     * @return
     */
    String selfCustomerScanCode(Long cpId, Long fromCpId, String type);

    /**
     * 弱关系升级成为次强关系，注意：要知道用户当前小于次强关系才可以调用
     * @param cpId
     * @param fromCpId
     * @return
     */
    boolean weekToSubStrong(Long cpId, Long fromCpId);

    /**
     * 用户扫码返回
     * @param cpId
     * @param fromCpId
     * @param type
     * @return
     */
    UserScan userScan(Long cpId, Long fromCpId, String type,String spreaderId);

    /**
     * 调用赋能商家系统的接口
     * @param cpId
     * @param fromCpId
     * @param spreaderId
     * @return
     */
    boolean weekToSubStrong4Business(Long cpId, Long fromCpId, String spreaderId);
    /**
     *赋能扫码绑定次强关系
     * @param cpId
     * @param upLine
     * @param spreaderId：
     *赋能专区生成的推荐人唯一号，扫描商家推广码时会携带；推广人编号-1代表推广人是商家自身
     * @return
     */
    public boolean boundRelationShip4Empower(Long cpId, Long upLine,String spreaderId);

    /**
     * 查询商家信息
     * @param cpId
     */
    ShopInfo queryShopInfoByCenter(Long cpId);

    /**
     * 是否是有效的商家
     * @param cpId
     * @return
     */
    boolean checkIsEffectShop(Long cpId);
}