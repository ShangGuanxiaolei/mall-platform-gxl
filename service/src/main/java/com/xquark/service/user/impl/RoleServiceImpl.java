package com.xquark.service.user.impl;

import com.xquark.dal.mapper.RoleMapper;
import com.xquark.dal.model.Role;
import com.xquark.service.user.RoleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by chh on 16-12-13.
 */
@Service("roleServiceImpl")
public class RoleServiceImpl implements RoleService {

  @Autowired
  private RoleMapper roleMapper;

  @Override
  public Role selectByPrimaryKey(String id) {
    return roleMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(Role role) {
    return roleMapper.insert(role);
  }

  @Override
  public int modify(Role role) {
    return roleMapper.modify(role);
  }

  @Override
  public int delete(String id) {
    return roleMapper.delete(id);
  }

  /**
   * 列表查询页面
   */
  @Override
  public List<Role> list(Pageable page, String keyword) {
    return roleMapper.list(page, keyword);
  }

  @Override
  public List<Role> listAll() {
    return roleMapper.listAll();
  }

  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long selectCnt(String keyword) {
    return roleMapper.selectCnt(keyword);
  }

  /**
   * 根据编码和归属获取对应角色信息
   */
  @Override
  public Role selectByCodeAndBelong(String code, String belong) {
    return roleMapper.selectByCodeAndBelong(code, belong);
  }

  /**
   * 查询某个归属内的所有角色信息
   */
  @Override
  public List<Role> listByBelong(String belong) {
    return roleMapper.listByBelong(belong);
  }

}
