package com.xquark.service.user.vo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/19
 * Time:20:59
 * Des:赋能新人扫码重定向页面
 */
public enum ScanRedirectPageType {
    HW_HOME, //汉薇主页
    HW_FRESH_MAN,//汉薇新人页
    HW_SHOP_DETAIL,//商家新人详情页
    HW_PAY//支付页面
}
