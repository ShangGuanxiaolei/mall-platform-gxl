package com.xquark.service.user;

import com.xquark.dal.model.User;
import com.xquark.wechat.oauth.ThirdUserBean;

/**
 * Created by wangxinhua on 18-2-11. DESC: 对接第三方用户接口
 */
public interface ThirdUserConverter<T extends ThirdUserBean> {

  /**
   * 将第三方用户转换为XquarkUser
   *
   * @param thirdUserBean 第三方用户Bean
   * @return {@link User}, 若不存在则创建
   */
  public User fromThirdUser(T thirdUserBean);

}
