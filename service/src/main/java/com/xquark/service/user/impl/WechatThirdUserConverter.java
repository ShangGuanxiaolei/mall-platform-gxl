package com.xquark.service.user.impl;

import com.xquark.dal.model.User;
import com.xquark.service.user.ThirdUserConverter;
import com.xquark.utils.EmojiFilter;
import com.xquark.wechat.oauth.protocol.WechatUserBean;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by wangxinhua on 18-2-11. DESC: 针对微信的第三方用户对接接口
 */
@Component
public class WechatThirdUserConverter implements ThirdUserConverter<WechatUserBean> {

  private PasswordEncoder pwdEncoder;

  @Autowired
  public void setPwdEncoder(PasswordEncoder pwdEncoder) {
    this.pwdEncoder = pwdEncoder;
  }

  @Override
  public User fromThirdUser(WechatUserBean wechatUser) {
    User user = new User();
    String nickname = EmojiFilter.filterEmoji(wechatUser.getNickName());
    // 防止微信昵称全部是emoji符号，导致返回的昵称为空，则默认赋值普通用户
    if (nickname == null || "".equals(nickname)) {
      nickname = "普通用户";
    }
    String appId = wechatUser.getAppId();
    user.setName(nickname);
    user.setLoginname(wechatUser.getOpenId());
    user.setPassword(pwdEncoder.encode(wechatUser.getOpenId()));
    user.setAvatar(wechatUser.getAvatar());
    user.setWeixinCode(appId);
    user.setWithdrawType(3); //微信提现
    // 设置用户unionid，这样其他开放平台下的应用（如小程序），该微信号均能登陆
    if (StringUtils.isNotEmpty(wechatUser.getUnionId())) {
      user.setUnionId(wechatUser.getUnionId());
    }
    return user;
  }
}
