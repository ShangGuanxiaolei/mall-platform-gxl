package com.xquark.service.user.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/20
 * Time:9:35
 * Des:扫码类型
 */
public class ScanCodeType {
    public static  final String SHOP_CODE = "shopCode"; //商家码
    public static  final String SELF_CODE = "selfCode"; //个人码
    public static  final String PAY_CODE = "payCode"; //付款码
    public static List<String> SCAN_CODE_TYPE_LIST = new ArrayList<>();
    static{
        SCAN_CODE_TYPE_LIST.add("shopCode");
        //SCAN_CODE_TYPE_LIST.add("selfCode");
        SCAN_CODE_TYPE_LIST.add("payCode");
    }
}