package com.xquark.service.user.impl;

import com.xquark.dal.mapper.UserLogMapper;
import com.xquark.dal.model.UserLogVo;
import com.xquark.service.user.UserLogService;
import com.xquark.service.user.vo.UserLogInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/4/21
 * Time:15:22
 * Des:用户日志
 */
@Service
public class UserLogServiceImpl implements UserLogService {
    @Autowired
    private UserLogMapper userLogMapper;
    @Override
    public boolean insertUserLog(UserLogInfo u) {
        if(Objects.isNull(u))
            return false;

        UserLogVo userLogVo = new UserLogVo();
        userLogVo.setCpId(u.getCpId());
        userLogVo.setFromCpId(u.getFromCpId());
        userLogVo.setLogInfo(u.getLogInfo());
        userLogVo.setType(u.getType());

        return this.userLogMapper.insert(userLogVo);
    }
}