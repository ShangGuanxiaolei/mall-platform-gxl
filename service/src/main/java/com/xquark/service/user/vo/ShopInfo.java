package com.xquark.service.user.vo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/6/26
 * Time:11:32
 * Des:
 */
public class ShopInfo {

    private int shopId;
    private String shopName;
    private String logoUrl;
    private int isOpen;
    private String allowPayment;

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public int getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(int isOpen) {
        this.isOpen = isOpen;
    }

    public String getAllowPayment() {
        return allowPayment;
    }

    public void setAllowPayment(String allowPayment) {
        this.allowPayment = allowPayment;
    }

    @Override
    public String toString() {
        return "ShopInfo{" +
                "shopId=" + shopId +
                ", shopName='" + shopName + '\'' +
                ", logoUrl='" + logoUrl + '\'' +
                ", isOpen=" + isOpen +
                ", allowPayment='" + allowPayment + '\'' +
                '}';
    }
}