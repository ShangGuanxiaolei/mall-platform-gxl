package com.xquark.service.user.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.ImmutableMap;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.mapper.CustomerModifyLinkDetailMapper;
import com.xquark.dal.mapper.WeakLinkMapper;
import com.xquark.dal.model.ConsumerToConsumer;
import com.xquark.dal.model.ConsumerToCustomer;
import com.xquark.dal.model.CustomerModifyLinkDetail;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.user.UserScanService;
import com.xquark.service.user.vo.ScanCodeType;
import com.xquark.service.user.vo.ScanRedirectPageType;
import com.xquark.service.user.vo.ShopInfo;
import com.xquark.service.user.vo.UserScan;
import com.xquark.utils.http.PoolingHttpClients;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/19
 * Time:20:53
 * Des:用户扫码相关
 */
@Service
public class UserScanServiceImpl implements UserScanService {
    private Logger log = LoggerFactory.getLogger(getClass());
    private static final Long COMPANY_CPID = 2000002L;
    @Value("${empower.host.url}")
    private String host;
    private final String WEEK_TO_SUBSTRONG_URL= "/v1/ee/binding_ss_relationship";
    private final String QUERY_SHOP_URL = "/v1/ee/query_shop"; // 查询商家信息
    @Autowired
    private CustomerProfileService customerProfileService;
    @Autowired
    private WeakLinkMapper weakLinkMapper;

    @Autowired
    private CustomerModifyLinkDetailMapper customerModifyLinkDetailMapper;

    /**
     * 赋能新人扫码重定向页面
     * @param cpId
     * @return
     */
    @Override
    public String scanRedirectPage(Long cpId, Long fromCpId,String type,String spreaderId) {
        log.info(new StringBuffer()
        .append("赋能新人扫码入参")
        .append("cpId是")
        .append(cpId)
        .append("{}fromCpId是:")
        .append(fromCpId)
        .append("{}type是:")
        .append(type)
        .append("{}spId是:")
        .append(spreaderId)
        .toString());
        //校验入参
        if(this.validateScanParam(cpId,fromCpId, type)){
            return ScanRedirectPageType.HW_HOME.name();
        }

        //自己扫自己
        if(this.validateEqCpIdAndFromCpId(cpId, fromCpId)){
            if(ScanCodeType.SELF_CODE.equalsIgnoreCase(type)){
                return ScanRedirectPageType.HW_FRESH_MAN.name();
            }
            if(ScanCodeType.SHOP_CODE.equalsIgnoreCase(type)){
                return ScanRedirectPageType.HW_SHOP_DETAIL.name();
            }
        }

        //个人码
        if(ScanCodeType.SELF_CODE.equalsIgnoreCase(type)){
            return this.selfCustomerScanCode(cpId, fromCpId, type);
        }
        //商家码
        if(ScanCodeType.SHOP_CODE.equalsIgnoreCase(type)){
            return this.selfBusinessScanCode(cpId, fromCpId, type,spreaderId);
        }
        //付款码
        if(ScanCodeType.PAY_CODE.equalsIgnoreCase(type)){
            return this.shopPayScanRedirectPage(cpId, fromCpId, type,spreaderId);
        }
        //默认到主页
        return ScanRedirectPageType.HW_HOME.name();
    }

    private boolean validateEqCpIdAndFromCpId(Long cpId, Long fromCpId) {

        if(cpId == fromCpId || cpId.equals(fromCpId)){
            //导到个人主页
            return true;
        }
        return false;
    }

    /**
     * 赋能商家支付扫码重定向页面
     * @param cpId
     * @param type
     * @param spreaderId
     * @return
     */
    @Override
    public String shopPayScanRedirectPage(Long cpId, Long fromCpId,String type,String spreaderId) {
        //校验入参
        if(this.validateScanParam(cpId,fromCpId, type)){
            return ScanRedirectPageType.HW_HOME.name();
        }
        //强关系
        boolean strong = customerProfileService.isStrong(cpId);
        if (strong){
            this.delRedisScanRedirectByCpId(cpId);
            return ScanRedirectPageType.HW_PAY.name();
        }
        //次强关系
        boolean subStrong = customerProfileService.isSubStrong(cpId);
        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        if(subStrong){
            String flag = redisUtils.get("scanRedirect:" + cpId);
            if(StringUtils.isNotBlank(flag)){
                redisUtils.del("scanRedirect:" + cpId);
                return ScanRedirectPageType.HW_FRESH_MAN.name();
            }
            return ScanRedirectPageType.HW_PAY.name();
        }
        //弱关系白人
        boolean isNew = customerProfileService.isNew(cpId.toString());
        if(isNew){
            this.delRedisScanRedirectByCpId(cpId);
            //绑定次强关系
            this.weekToSubStrong(cpId, fromCpId);
            String flag = redisUtils.get("scanPay4SubStrong:" + cpId);
            if(StringUtils.isNotBlank(flag)){
                log.info(new StringBuffer()
                        .append("扫付款码，身份是纯白人扫码，cpId是：")
                        .append(cpId)
                        .append("开始调用绑定次强关系接口").toString());
                boolean b = this.weekToSubStrong4Business(cpId, fromCpId, spreaderId);
                if(b){
                    log.info("有调用赋能接口返回，是付款码"+ cpId);
                }
            }
            return ScanRedirectPageType.HW_FRESH_MAN.name();
        }
        return ScanRedirectPageType.HW_HOME.name();
    }

    /**
     * 商家码
     * @param cpId
     * @param type
     * @return
     */
    @Override
    public String selfBusinessScanCode(Long cpId, Long fromCpId,String type, String spreaderId) {
        //校验入参
        if(this.validateScanParam(cpId, fromCpId ,type)){
            return ScanRedirectPageType.HW_HOME.name();
        }
        //强关系
        boolean strong = customerProfileService.isStrong(cpId);
        if (strong){
            this.delRedisScanRedirectByCpId(cpId);
            return ScanRedirectPageType.HW_SHOP_DETAIL.name();
        }
        //次强关系
        boolean subStrong = customerProfileService.isSubStrong(cpId);
        if(subStrong){
            RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
            String flag = redisUtils.get("scanRedirect:" + cpId);
            if(StringUtils.isNotBlank(flag)){
                redisUtils.del("scanRedirect:" + cpId);
                return ScanRedirectPageType.HW_FRESH_MAN.name();
            }
            return ScanRedirectPageType.HW_SHOP_DETAIL.name();
        }
        //弱关系白人
        boolean isNew = customerProfileService.isNew(cpId.toString());
        if(isNew){
            this.delRedisScanRedirectByCpId(cpId);

            log.info("开始执行汉薇升级次强关系功能"+ cpId);
            //升级成为次强关系
            this.weekToSubStrong(cpId, fromCpId);

            log.info("汉薇升级次强关系完成，开始调用安畅通知接口"+ cpId);
            //同时调用安畅接口升级他们的维护的次强关系
            boolean b = this.weekToSubStrong4Business(cpId, fromCpId,spreaderId);
            if(b){
                log.info("有调用赋能接口返回"+ cpId);
            }
            return ScanRedirectPageType.HW_FRESH_MAN.name();
        }
        return ScanRedirectPageType.HW_HOME.name();
    }

    /**
     * 个人码
     * @param cpId
     * @param type
     * @return
     */
    @Override
    public String selfCustomerScanCode(Long cpId, Long fromCpId,String type) {
        //删除商家码标记
        this.delRedisScanRedirectByCpId(cpId);
        //校验入参
        if(this.validateScanParam(cpId,fromCpId, type)){
            return ScanRedirectPageType.HW_HOME.name();
        }
        //强关系
        boolean strong = customerProfileService.isStrong(cpId);
        if (strong){
            return ScanRedirectPageType.HW_HOME.name();
        }
        //次强关系
        boolean subStrong = customerProfileService.isSubStrong(cpId);
        if(subStrong){
            return ScanRedirectPageType.HW_HOME.name();
        }
        //弱关系白人
        boolean isNew = customerProfileService.isNew(cpId.toString());
        if(isNew){
            return ScanRedirectPageType.HW_FRESH_MAN.name();
        }
        return ScanRedirectPageType.HW_HOME.name();
    }

    /**
     * 弱关系升级成为次强关系，注意：要知道用户当前小于次强关系才可以调用
     * @param cpId
     * @param fromCpId
     * @return
     */
    @Override
    public boolean weekToSubStrong(Long cpId, Long fromCpId) {
        if (cpId == null || fromCpId == null) {
            return false;
        }
        //防止自己绑定自己
        if(cpId.equals(fromCpId)){
            fromCpId = customerProfileService.getUpline(cpId, fromCpId);
        }
        boolean hasUpline = customerProfileService.hasUplineCpId(cpId);
        Long boundCpId = customerProfileService.getUplineWeekIgnoreIdentity(cpId);
        // 身份跟白人表都没有绑定过弱关系 且自己没有强关系
        if (boundCpId == null && !hasUpline) {

            //原有弱关系绑定变更为次强关系，记录身份变更轨迹
            Long weakShareCpId = weakLinkMapper.selectShareCpId(cpId);
            CustomerModifyLinkDetail customerModifyLinkDetail = new CustomerModifyLinkDetail();
            customerModifyLinkDetail.setCpid(cpId);
            //设置原上级cpid，如果为空，则取公司
            customerModifyLinkDetail.setFromCpid(weakShareCpId == null ? COMPANY_CPID : weakShareCpId);
            customerModifyLinkDetail.setLinkDirection("WEAK_TO_SUBSTRONG");//弱关系->次强关系

            // 如果分享人跟当前下单人都是白人, 则保存到白人弱关系表
            if (fromCpId != null
                    && !customerProfileService.hasUplineCpId(fromCpId)
                    && !customerProfileService.hasUplineCpId(cpId)) {
                // 分享人白人上面的第一个有身份的人
                Long firstIdentityCpId = customerProfileService.getFirstIdentityUpline(fromCpId);
                customerProfileService.saveConsumerToConsumer(
                        new ConsumerToConsumer(cpId, fromCpId, firstIdentityCpId));

                customerModifyLinkDetail.setToCpid(fromCpId);//设置当前分享人的cpid
                customerModifyLinkDetailMapper.insertSelective(customerModifyLinkDetail);
                log.info("弱关系->次强关系(分享人也是白人)，关系变更轨迹记录成功cpId" + cpId);
                return true;
            }

            //保存有身份的弱关系
            ConsumerToCustomer consumerToCustomer = new ConsumerToCustomer(cpId, fromCpId);
            customerProfileService.saveConsumerToCustomer(consumerToCustomer);

            //设置当前分享人的cpid,如果为空，取原上级
            if (Objects.isNull(weakShareCpId)) {
                customerModifyLinkDetail.setToCpid(fromCpId == null ? fromCpId : fromCpId);
            } else {
                customerModifyLinkDetail.setToCpid(fromCpId == null ? weakShareCpId : fromCpId);
            }
            customerModifyLinkDetailMapper.insertSelective(customerModifyLinkDetail);
            log.info("弱关系->次强关系(分享人为有身份的人)，关系变更轨迹记录成功cpId" + cpId);
            return true;
        }
        return false;
    }

    /**
     * 用户扫码返回
     * @param cpId
     * @param fromCpId
     * @param type
     * @return
     */
    @Override
    public UserScan userScan(Long cpId, Long fromCpId, String type,String spreaderId) {
        UserScan userScan = new UserScan();
        String page = this.scanRedirectPage(cpId, fromCpId, type,spreaderId);
        userScan = this.converUserScan(page,userScan);
        return userScan;
    }

    /**
     * 调用赋能商家系统的接口
     * @param cpId
     * @param fromCpId
     * @param spreaderId
     * @return
     */
    @Override
    public boolean weekToSubStrong4Business(Long cpId, Long fromCpId, String spreaderId) {
        log.info("请求赋能系统接口升级次强关系");

        HttpResponse response = null;
        String entity = null;//接口返回值
        if(StringUtils.isBlank(spreaderId))
            spreaderId = "-1";
        ImmutableMap<String, String> reParam = ImmutableMap.of(
                "cpid", cpId.toString(), "upline_cpid", fromCpId.toString(), "spreader_id", spreaderId);
        log.info(new StringBuffer().append("请求赋能系统接口升级次强关系的参数:")
        .append(reParam).toString());
        try {
            log.info(new StringBuffer()
                    .append("请求赋能系统接口的URL：")
                    .append(host)
                    .append(WEEK_TO_SUBSTRONG_URL).toString());

            response = PoolingHttpClients.postJsonResponse(new StringBuffer().
                    append(host)
                    .append(WEEK_TO_SUBSTRONG_URL).toString(), reParam);
        }catch (Exception e){
            log.error("请求赋能系统接口升级次强关系出现异常", e.getMessage());
            return false;
        }
        if(response == null)
            log.error("请求赋能系统接口升级次强关系返回null");
        else {
            try {
                entity = EntityUtils.toString(response.getEntity(), "utf-8");
                JSONObject json = JSON.parseObject(entity);
                String bindingStatus = json.getString("binding_status");

                if(StringUtils.isBlank(bindingStatus))
                    log.error("获取binding_status为空");

                if("1".equals(bindingStatus))
                    log.info("请求赋能系统接口升级次强关系成功");
                else {
                    log.info("请求赋能系统接口升级次强关系失败");
                }
                log.info(new StringBuffer()
                        .append("返回的binding_status是：")
                        .append(bindingStatus).toString());

                String binding_msg = json.getString("binding_msg");
                log.info(new StringBuffer()
                .append("返回的binding_msg是：")
                .append(binding_msg).toString());
            } catch (IOException e) {
                log.error("io异常", e.getMessage());
                return false;
            }
            log.info(new StringBuffer().append("请求赋能系统接口升级次强关系的返回数据状态")
                    .append(response.getStatusLine())
                    .append("返回Entity")
                    .append(entity).toString());
        }
        return true;
    }


    @Override
    public boolean boundRelationShip4Empower(Long cpId, Long upLine, String spreaderId) {
        if(Objects.isNull(cpId) || Objects.isNull(upLine)){
            log.error("赋能扫码请求结算中心绑定次强关系入参为空");
            return false;
        }
        Map<String,Object> mapTag = new HashMap<>();
        mapTag.put("cpId", cpId);

        HttpResponse response = null;
        String entity = null;//接口返回值
        if(StringUtils.isBlank(spreaderId))
            spreaderId = "-1";
        log.info("赋能扫码请求结算中心绑定次强关系入参cpId{" + cpId + "}upLine{" + upLine + "}spreaderId{" + spreaderId + "}");

        ImmutableMap<String, String> reParam = ImmutableMap.of(
                "cpid", cpId.toString(), "upline_cpid", upLine.toString(), "spreader_id", spreaderId);

        try {
            log.info("赋能扫码请求结算中心绑定次强关系请求接口开始执行" + reParam + mapTag);
            response = PoolingHttpClients.postJsonResponse(new StringBuffer().
                    append(host).append(WEEK_TO_SUBSTRONG_URL).toString(), reParam);
        }catch (Exception e){
            log.error("赋能扫码请求结算中心绑定次强关系出现异常" + JSONObject.toJSON(e.getMessage()) + mapTag);
            return false;
        }

        if(response == null) {
            log.error("赋能扫码请求结算中心绑定次强关系返回null" + mapTag);
            return false;
        }

        try {
            entity = EntityUtils.toString(response.getEntity(), "GBK");
            JSONObject json = JSONObject.parseObject(entity);
            log.info("赋能扫码请求结算中心绑定次强返回" + JSONObject.toJSON(json) + mapTag);
            String bindingStatus = json.getString("binding_status");

            if("1".equals(bindingStatus))
                log.info("赋能扫码请求结算中心绑定次强关系成功" + mapTag);
            else {
                log.error("赋能扫码请求结算中心绑定次强关系失败" + mapTag);
            }
        } catch (IOException e) {
            log.error("赋能扫码请求结算中心绑定次强关io异常" + JSONObject.toJSON(e.getMessage()) + mapTag);
            return false;
        }

        return true;
    }

    @Override
    public ShopInfo queryShopInfoByCenter(Long cpId) {
        if(Objects.isNull(cpId)) return null;
        log.info("查询商家信息入参" + cpId);
        HttpResponse response = null;
        ImmutableMap<String, String> reParam = ImmutableMap.of(
                "cpid", cpId.toString());
        try {
            response = PoolingHttpClients.postJsonResponse(new StringBuffer().
                append(host).append(QUERY_SHOP_URL).toString(), reParam);
            if(response == null){
                log.info("查询商家信息返回null" + cpId);
                return null;
            }
            String entity = EntityUtils.toString(response.getEntity(), "GBK");
            JSONObject json = JSONObject.parseObject(entity);
            log.info("查询商家信息返回entity" + json + cpId);

            ShopInfo shopInfo = new ShopInfo();
            // 赋能商家店铺号，若未注册返回-1
            int shop_id = json.getIntValue("shop_id");
            String shop_name = json.getString("shop_name");
            String logo_url = json.getString("logo_url");
            // 商家是否上架，1上架，0未上架，若未注册返回-1
            int is_open = json.getIntValue("is_open");
            // 是否开通买单功能，1开通，0未开通，若未注册返回-1
            String allow_payment = json.getString("allow_payment");
            shopInfo.setShopId(shop_id);
            shopInfo.setShopName(shop_name);
            shopInfo.setLogoUrl(logo_url);
            shopInfo.setIsOpen(is_open);
            shopInfo.setAllowPayment(allow_payment);
            return shopInfo;
        }catch (Exception e){
            log.info("查询商家信息出现异常" + JSONObject.toJSON(e.getMessage()) + cpId);
        }
        return null;
    }

    @Override
    public boolean checkIsEffectShop(Long cpId) {
        if(Objects.isNull(cpId)) return false;

        ShopInfo shopInfo = this.queryShopInfoByCenter(cpId);

        if(Objects.isNull(shopInfo)) return false;

        // 赋能商家店铺号，若未注册返回-1
        int shopId = shopInfo.getShopId();
        // 商家是否上架，1上架，0未上架，若未注册返回-1
        int isOpen = shopInfo.getIsOpen();

        // 是商家
        if(shopId != -1 && isOpen == 1) return true;

        return false;
    }

    private UserScan converUserScan(String page, UserScan userScan) {
        userScan.setRedirectType(page);
        userScan.setRedirectApp("wechat");
        if(ScanRedirectPageType.HW_HOME.name().equals(page)){
            userScan.setRedirectName("汉薇主页");
        }
        if(ScanRedirectPageType.HW_SHOP_DETAIL.name().equals(page)){
            userScan.setRedirectName("商家新人详情页");
        }
        if(ScanRedirectPageType.HW_FRESH_MAN.name().equals(page)){
            userScan.setRedirectName("汉薇新人页");
        }
        if(ScanRedirectPageType.HW_PAY.name().equals(page)){
            userScan.setRedirectName("支付码扫码成功页面");
        }
        return userScan;
    }

    /**
     * 校验参数
     * @param cpId
     * @param type
     * @return
     */
    private boolean validateScanParam(Long cpId, Long fromCpId,String type){
        if(cpId == null || fromCpId== null || StringUtils.isBlank(type)){
            //导到汉薇主页
            return true;
        }
        return false;
    }

    /**
     * 清除商家码新注册用户导入到含联名卡的新人页标记
     * @param cpId
     * @return
     */
    private boolean delRedisScanRedirectByCpId(Long cpId){
        if(Objects.isNull(cpId))
            return false;
        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        redisUtils.del("scanRedirect:" + cpId);
        return true;
    }
}