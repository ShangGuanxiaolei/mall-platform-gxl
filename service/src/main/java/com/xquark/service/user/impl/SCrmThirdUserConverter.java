package com.xquark.service.user.impl;

import com.xquark.dal.model.User;
import com.xquark.helper.Transformer;
import com.xquark.service.user.ThirdUserConverter;
import com.xquark.wechat.oauth.SCrmUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 18-2-11. DESC: 针对SCrm系统的用户对接相关实现
 */
@Component
public class SCrmThirdUserConverter implements ThirdUserConverter<SCrmUser> {

  private PasswordEncoder pwdEncoder;

  @Autowired
  public void setPwdEncoder(PasswordEncoder pwdEncoder) {
    this.pwdEncoder = pwdEncoder;
  }

  @Override
  public User fromThirdUser(SCrmUser thirdUserBean) {
    User baseUser = Transformer.fromBean(thirdUserBean, User.class);
    baseUser.setPassword(pwdEncoder.encode(thirdUserBean.getUnionId()));
    baseUser.setName(thirdUserBean.getPhone());
    baseUser.setLoginname(thirdUserBean.getPhone());
    baseUser.setAvatar("");
    return baseUser;
  }

}
