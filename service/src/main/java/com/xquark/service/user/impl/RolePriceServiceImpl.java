package com.xquark.service.user.impl;

import com.xquark.dal.mapper.RoleMapper;
import com.xquark.dal.mapper.RolePriceMapper;
import com.xquark.dal.model.Role;
import com.xquark.dal.model.RolePrice;
import com.xquark.dal.vo.RolePriceVO;
import com.xquark.service.user.RolePriceService;
import com.xquark.service.user.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by chh on 16-12-13.
 */
@Service("rolePriceServiceImpl")
public class RolePriceServiceImpl implements RolePriceService {

  @Autowired
  private RolePriceMapper rolePriceMapper;

  @Override
  public RolePriceVO selectByPrimaryKey(String id) {
    return rolePriceMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(RolePrice rolePrice) {
    return rolePriceMapper.insert(rolePrice);
  }

  @Override
  public int modify(RolePrice rolePrice) {
    return rolePriceMapper.modify(rolePrice);
  }

  @Override
  public int delete(String id) {
    return rolePriceMapper.delete(id);
  }

  /**
   * 列表查询页面
   */
  @Override
  public List<RolePriceVO> list(Pageable page, String keyword) {
    return rolePriceMapper.list(page, keyword);
  }

  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long selectCnt(String keyword) {
    return rolePriceMapper.selectCnt(keyword);
  }

  @Override
  public RolePriceVO selectByProductAndRole(String productId, String roleId) {
    return rolePriceMapper.selectByProductAndRole(productId, roleId);
  }

}
