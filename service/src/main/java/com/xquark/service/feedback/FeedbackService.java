package com.xquark.service.feedback;

import com.xquark.dal.model.Feedback;
import com.xquark.dal.vo.FeedbackVO;
import com.xquark.service.BaseEntityService;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

public interface FeedbackService extends BaseEntityService<Feedback> {

  Feedback load(String id);

  int insert(Feedback feedback);

  boolean saveWithTags(Feedback feedback, List<String> tags);

  int updateForClosed(String id, String replay);

  List<FeedbackVO> listFeedbacksByAdmin(Map<String, Object> params, Pageable page);

  Long countFeedbacksByAdmin(Map<String, Object> params);

  int insertWithoutUser(Feedback feedback);

  int delete(String id);

  Feedback selectByContact(String contact);

}
