package com.xquark.service.feedback.impl;

import com.xquark.aop.anno.IdEncode;
import com.xquark.aop.anno.NotNull;
import com.xquark.dal.IUser;
import com.xquark.dal.mapper.FeedbackMapper;
import com.xquark.dal.model.Feedback;
import com.xquark.dal.model.Tags;
import com.xquark.dal.status.FeedbackStatus;
import com.xquark.dal.type.TagsCategory;
import com.xquark.dal.vo.FeedbackVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.feedback.FeedbackService;
import com.xquark.service.tags.TagsService;
import com.xquark.service.tags.impl.FeedBackTagsService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("feedbackService")
public class FeedbackServiceImpl extends BaseServiceImpl implements FeedbackService {

  @Autowired
  private FeedbackMapper feedbackMapper;

  @Autowired
  private FeedBackTagsService feedBackTagsService;

  @Autowired
  private TagsService tagsService;

  @Override
  public Feedback load(String id) {
    return feedbackMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(@IdEncode Feedback feedback) {
    feedback.setStatus(FeedbackStatus.NEW);
    IUser user = this.getCurrentUser();
    if (user != null) {
      feedback.setUserId(user.getId());
      feedback.setName(user.getName());
    }
    return feedbackMapper.insert(feedback);
  }

  @Override
  public int insertOrder(Feedback feedback) {
    feedback.setStatus(FeedbackStatus.NEW);
    IUser user = this.getCurrentUser();
    if (user != null) {
      feedback.setUserId(user.getId());
      feedback.setName(user.getName());
    }
    return feedbackMapper.insert(feedback);
  }

  @Override
  @Transactional
  public boolean saveWithTags(@NotNull Feedback feedback, @NotNull List<String> tags) {
    boolean saveResult = this.insert(feedback) > 0;
    String id = feedback.getId();
    List<Tags> tagsToSave = tagsService.listTags(tags, TagsCategory.HELPER);
    return saveResult && feedBackTagsService
        .saveWithRelation(id, tagsToSave.toArray(new Tags[tagsToSave.size()]));
  }

  @Override
  public int insertWithoutUser(Feedback feedback) {
    feedback.setStatus(FeedbackStatus.NEW);
    return feedbackMapper.insert(feedback);
  }

  @Override
  public int updateForClosed(String id, String replay) {
    return feedbackMapper.updateForClosed(id, replay, FeedbackStatus.CLOSED);
  }

  @Override
  public List<FeedbackVO> listFeedbacksByAdmin(Map<String, Object> params, Pageable page) {
    return feedbackMapper.listFeedbacksByAdmin(params, page);
  }

  @Override
  public Long countFeedbacksByAdmin(Map<String, Object> params) {
    return feedbackMapper.countFeedbacksByAdmin(params);
  }

  @Override
  public int delete(String id) {
    return feedbackMapper.delete(id);
  }

  @Override
  public Feedback selectByContact(String contact) {
    return feedbackMapper.selectByContact(contact);
  }

}
