package com.xquark.service.sequence.exception;

/**
 * @author Jack Zhu
 * @since 2018/12/29
 */
public class CannotTakeIdException extends Exception {
    public CannotTakeIdException(String message) {
        super(message);
    }

    public CannotTakeIdException(String message, Throwable cause) {
        super(message, cause);
    }
}
