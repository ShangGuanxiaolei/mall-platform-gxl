package com.xquark.service.sequence.impl;

import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableMap;
import com.xquark.dal.SequenceId;
import com.xquark.dal.mapper.CustomerluckylistMapper;
import com.xquark.dal.mapper.SequenceGeneratorMapper;
import com.xquark.dal.type.IdSequenceType;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.sequence.IdSequenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.xquark.dal.type.IdSequenceType.*;

/**
 * @author wangxinhua.
 * @date 2018/12/10 基于单点的redis的全局id生产器
 */
@Service("redisSequenceService")
public class RedisSequenceService implements IdSequenceService {

  private Logger log = LoggerFactory.getLogger(getClass());

  private static final String KEY_SUFFIX = "_COUNTER";

  /** cpId生成器 */
  private static final String MEMBER_COUNTER_KEY = MEMBER + KEY_SUFFIX;

  /** 地址id生成器 */
  private static final String ADDRESS_COUNTER_KEY = ADDRESS + KEY_SUFFIX;

  /** 银行编号生成器 */
  private static final String BANK_COUNTER_KEY = BANK + KEY_SUFFIX;

  /** 自增初始值 */
  private static AtomicLong orderAscId ;

  /**
   * @Author chp
   * @Description //订单编号生成器
   * @Date
   * @Param
   * @return
   **/
  private static final String ORDER_COUNTRY_KEY=ORDER+KEY_SUFFIX;

  private final ImmutableMap<IdSequenceType, ? extends Supplier<Long>> generatorMap;

  private final RedisTemplate<String, Long> idTemplate;

  private final SequenceGeneratorMapper sequenceGeneratorMapper;

  @Autowired
  public RedisSequenceService(
      RedisTemplate<String, Long> idTemplate,
      CustomerluckylistMapper customerluckylistMapper,
      SequenceGeneratorMapper sequenceGeneratorMapper) {
    this.idTemplate = idTemplate;
    this.sequenceGeneratorMapper = sequenceGeneratorMapper;
    generatorMap =
        ImmutableMap.of(
            MEMBER,
                new MemberRedisIdSupplier(
                    new RedisAtomicLong(MEMBER_COUNTER_KEY, idTemplate), customerluckylistMapper),
            ADDRESS, new RedisSegmentIDService(new RedisAtomicLong(ADDRESS_COUNTER_KEY, idTemplate)),
            BANK, new RedisSegmentIDService(new RedisAtomicLong(BANK_COUNTER_KEY, idTemplate)),
            ORDER, new RedisSegmentIDService(new RedisAtomicLong(ORDER_COUNTRY_KEY, idTemplate))
                );
  }

  @Override
  public Long generateIdByType(IdSequenceType type) {
    checkNotNull(type, "type can not be null");
    Supplier<Long> idGenerator = generatorMap.get(type);
    if (idGenerator == null) {
      throw new IllegalArgumentException("redis counter with type: " + type + " not exist");
    }
    return idGenerator.get();
  }


  @Override
  public Long generateIdByType2(IdSequenceType type) {
    checkNotNull(type, "type can not be null");
    Supplier<Long> idGenerator = generatorMap.get(type);
    if (idGenerator == null) {
      throw new IllegalArgumentException("redis counter with type: " + type + " not exist");
    }
      Long aLong = idGenerator.get();
    synchronized (this){
        if(aLong>(orderAscId.longValue())){
            switch (type) {
                case ORDER:
                    //当生成的值自增超过1000,进行数据库的更新,在此基础上自增1000
                    int i  = sequenceGeneratorMapper.orderUpdate();
                    if(1!=i){throw  new BizException(GlobalErrorCode.UNKNOWN,"订单主键增加失败");
                    }
                  orderAscId.addAndGet(1000);
                  break;
                default:
            }
        }
    }

    return aLong;
  }






  @PostConstruct
  public void init() {
    for (IdSequenceType type : IdSequenceType.values()) {
      setRedis(type);
    }
  }

  private void setRedis(IdSequenceType type) {
    String key = type + KEY_SUFFIX;
    Long redisId = idTemplate.opsForValue().get(key);
//      idTemplate.opsForValue().set(key,null);
    Long dbId;
    switch (type) {
      case MEMBER:
        dbId = sequenceGeneratorMapper.getMemberId();
        break;
      case BANK:
        dbId = sequenceGeneratorMapper.getBankId();
        break;
      case ADDRESS:
        dbId = sequenceGeneratorMapper.getAddressId();
        break;
      case ORDER:
        dbId = sequenceGeneratorMapper.getOrderId();
        orderAscId=new AtomicLong(dbId);
        break;
      default:
        dbId = null;
        // do nothing
    }
    if (dbId == null) {
      throw new IllegalArgumentException("类型 " + type + "不支持");
    }
    long safeNumber = 100L;
    // 同步数据库值 || 只在初次上线会生效
    // TODO 后续移除初始化代码
    if (redisId == null || redisId < dbId) {
      if(!type.equals(ORDER)){
        idTemplate.opsForValue().set(key, dbId + safeNumber);
      }
//      和数据库中id做对比,数据库中id始终比redis多1000
      if(type.equals(ORDER)){
        if(redisId == null){
          idTemplate.opsForValue().set(key, dbId);
          int i = sequenceGeneratorMapper.orderUpdate();
          if(1!=i){
            throw  new BizException(GlobalErrorCode.UNKNOWN,"订单主键增加失败");

          }
          orderAscId=new AtomicLong(dbId+1000);
        }


      }

    }
    //如果redis当前值大于数据库，则更改数据库
      if(type.equals(ORDER)){
          if(redisId>dbId){

            sequenceGeneratorMapper.orderIdUpdate(redisId);
            //  数据库扩容1000
            int i = sequenceGeneratorMapper.orderUpdate();
            if(1!=i){
              throw  new BizException(GlobalErrorCode.UNKNOWN,"订单主键增加失败");

            }
            orderAscId=new AtomicLong(redisId+1000);
          }


      }



  }
}
