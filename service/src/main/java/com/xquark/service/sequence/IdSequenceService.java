package com.xquark.service.sequence;

import com.xquark.dal.type.IdSequenceType;

/**
 * @author wangxinhua.
 * @date 2018/12/10
 */
public interface IdSequenceService {

  /**
   * 根据类型生成唯一id
   *
   * @param type {@code MEMBER | ADDRESS | BANK}
   * @return 全局唯一id
   */
  Long generateIdByType(IdSequenceType type);


  Long generateIdByType2(IdSequenceType type);
}
