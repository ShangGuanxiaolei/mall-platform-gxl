package com.xquark.service.sequence.impl;

import com.xquark.dal.mapper.CustomerluckylistMapper;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;

/**
 * @author wangxinhua.
 * @date 2018/12/10
 * 会员id编号生成器
 */
public class MemberRedisIdSupplier extends RedisSegmentIDService {

  private final CustomerluckylistMapper customerluckylistMapper;

  MemberRedisIdSupplier(
      RedisAtomicLong redisAtomicLong,
      CustomerluckylistMapper customerluckylistMapper) {
    super(redisAtomicLong);
    this.customerluckylistMapper = customerluckylistMapper;
  }

  @Override
  public Long get() {
    Long next = super.get();
    boolean isCpIdExists = customerluckylistMapper.selectCpIdExists(next);
    if (isCpIdExists) {
      // 吉祥号则调过当前的继续取下一个
      return get();
    }
    return next;
  }

}
