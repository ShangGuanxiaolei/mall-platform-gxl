package com.xquark.service.sequence.impl;

import com.google.common.base.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;

/**
 * @author JackZhu
 * @date 2018/12/29
 */
public class RedisSegmentIDService implements Supplier<Long> {

    /**
     * 自动原子服务
     */
    private final RedisAtomicLong atomicLong;

    public static final long STEP_SIZE = 10;

    private ThreadLocal<IdLocal> idThreadLocal = new ThreadLocal<>();

    public RedisSegmentIDService(RedisAtomicLong atomicLong) {
        this.atomicLong = atomicLong;
    }

    @Override
    public Long get() {

        long ret;
         IdLocal currentIdLocal = idThreadLocal.get();
        if (currentIdLocal == null) {
            idThreadLocal.set(new IdLocal(0L, atomicLong.addAndGet(STEP_SIZE)));
            currentIdLocal = idThreadLocal.get();
        }
        ret = currentIdLocal.getNextId();
        if (ret == -1) {
            IdLocal newIdLocal = new IdLocal(0L, atomicLong.addAndGet(STEP_SIZE));
            idThreadLocal.set(newIdLocal);
            ret = newIdLocal.getNextId();
        }
        return ret;
    }



}
