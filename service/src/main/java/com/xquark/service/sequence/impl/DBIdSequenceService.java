package com.xquark.service.sequence.impl;

import com.xquark.dal.SequenceId;
import com.xquark.dal.mapper.CustomerluckylistMapper;
import com.xquark.dal.mapper.SequenceGeneratorMapper;
import com.xquark.dal.type.IdSequenceType;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.sequence.IdSequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * created by
 *
 * @author wangxinhua at 18-5-31 下午1:10
 * 基于数据库的Id自增序列实现
 * @deprecated 使用 {@link RedisSequenceService}
 */
public class DBIdSequenceService implements IdSequenceService {

  private SequenceGeneratorMapper sequenceGeneratorMapper;

  private final CustomerluckylistMapper customerluckylistMapper;

  @Autowired
  public DBIdSequenceService(
      SequenceGeneratorMapper sequenceGeneratorMapper,
      CustomerluckylistMapper customerluckylistMapper) {
    this.sequenceGeneratorMapper = sequenceGeneratorMapper;
    this.customerluckylistMapper = customerluckylistMapper;
  }

  /**
   * 根据类型生成全局id
   * @param type sequence type
   * @return id
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public Long generateIdByType(IdSequenceType type) {
    SequenceId sequence = new SequenceId('a');
    switch (type) {
      case MEMBER:
        sequenceGeneratorMapper.replaceMemberInfo(sequence);
        Long id = sequence.getId();
        // 跳过吉祥号
        boolean isCpidExists = customerluckylistMapper.selectCpIdExists(id);
        if (isCpidExists) {
          return generateIdByType(IdSequenceType.MEMBER);
        }
        break;
      case BANK:
        sequenceGeneratorMapper.replaceBankInfo(sequence);
        break;
      case ADDRESS:
        sequenceGeneratorMapper.replaceAddressInfo(sequence);
        break;
      case ORDER:
        sequenceGeneratorMapper.replaceOrderInfo(sequence);
      default:
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "unexcepted sequence type");
    }
    return sequence.getId();
  }

  @Override
  public Long generateIdByType2(IdSequenceType type) {
    return null;
  }

}
