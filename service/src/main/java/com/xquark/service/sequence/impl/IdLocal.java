package com.xquark.service.sequence.impl;

import static com.xquark.service.sequence.impl.RedisSegmentIDService.STEP_SIZE;

/**
 * @author Jack Zhu
 * @since 2018/12/30
 */
public class IdLocal {
    private Long step ;
    private Long id;

    public IdLocal(Long step, Long id) {
        this.step = step;
        this.id = id;
    }

    public Long getNextId() {
        if (this.step == STEP_SIZE) {
            return -1L;

        }
        return (this.id + this.step++) - STEP_SIZE;
    }

}
