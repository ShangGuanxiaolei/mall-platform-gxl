package com.xquark.service.sequence.impl;

import com.google.common.base.Supplier;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;

/**
 * @author wangxinhua.
 * @date 2018/12/10
 */
public class RedisIdSupplier implements Supplier<Long> {

  private final RedisAtomicLong redisAtomicLong;

  RedisIdSupplier(RedisAtomicLong redisAtomicLong) {
    this.redisAtomicLong = redisAtomicLong;
  }

  @Override
  public Long get() {
    return redisAtomicLong.incrementAndGet();
  }

}
