//package com.xquark.service.moduleFuncation;
//
//import com.xquark.dal.model.ModuleFunction;
//import com.xquark.dal.vo.ModuleFunctionVO;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.domain.Sort;
//
//import java.util.List;
//
///**
// * 作者: wangxh
// * 创建日期: 17-4-7
// * 简介:
// */
//public interface ModuleFunctionService {
//
//    Boolean save(ModuleFunction moduleFunction);
//
//    Boolean deleteById(String id);
//
//    Boolean deleteByModuleId(String moduleId);
//
//    Boolean update(ModuleFunction moduleFunction);
//
//    ModuleFunction load(String id);
//
//    List<ModuleFunction> loadAll(String order, Pageable pageable, Sort.Direction direction);
//
//    List<ModuleFunction> loadByModuleId(String moduleId, String order, Pageable pageable, Sort.Direction direction);
//
//    List<ModuleFunction> loadByModuleName(String moduleName, String order, Pageable pageable, Sort.Direction direction);
//
//    List<ModuleFunctionVO> listModuleFunctionVO(String order, Pageable pageable, Sort.Direction direction);
//
//    List<ModuleFunctionVO> listModuleFunctionVO();
//
//    List<ModuleFunctionVO> listModuleFunctionVO(String moduleId, String order, Pageable pageable, Sort.Direction direction);
//
//    Integer totalCount();
//
//    Integer totalCount(String moduleId);
//}
