//package com.xquark.service.moduleFuncation.impl;
//
//import com.xquark.dal.mapper.MerchantRoleMapper;
//import com.xquark.dal.mapper.ModuleFunctionMapper;
//import com.xquark.dal.mapper.ModuleMapper;
//import com.xquark.dal.mapper.RoleFunctionMapper;
//import com.xquark.dal.model.ModuleFunction;
//import com.xquark.dal.vo.ModuleFunctionVO;
//import com.xquark.service.error.BizException;
//import com.xquark.service.error.GlobalErrorCode;
//import com.xquark.service.moduleFuncation.ModuleFunctionService;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.domain.Sort;
//import org.springframework.stereotype.Service;
//
//import java.util.Date;
//import java.util.List;
//
///**
// * 作者: wangxh
// * 创建日期: 17-4-7
// * 简介:
// */
//@Service
//public class ModuleFunctionServiceImpl implements ModuleFunctionService {
//
//    @Autowired
//    private ModuleFunctionMapper moduleFunctionMapper;
//
//    @Autowired
//    private ModuleMapper moduleMapper;
//
//    @Autowired
//    private RoleFunctionMapper roleFunctionMapper;
//
//    @Autowired
//    private MerchantRoleMapper merchantRoleMapper;
//
//    @Override
//    public Boolean save(ModuleFunction moduleFunction) {
//        Boolean isExists = checkExists(moduleFunction.getFunctionId(), moduleFunction.getModuleId());
//        if (isExists) throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "要添加的按钮已存在");
//        if (moduleFunction.getCreatedAt() == null) moduleFunction.setCreatedAt(new Date());
//        if (moduleFunction.getUpdatedAt() == null) moduleFunction.setUpdatedAt(new Date());
//        return moduleFunctionMapper.insert(moduleFunction) > 0;
//    }
//
//    //TODO wangxh 迁移到按钮表的service
//    @Override
//    public Boolean deleteById(String id) {
//        ModuleFunction moduleFunction = moduleFunctionMapper.selectOne(id);
//        if (moduleFunction == null) throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "要删除的按钮不存在");
//        boolean moduleResult = moduleFunctionMapper.deleteById(id) > 0;
//        boolean roleResult = roleFunctionMapper.deleteByFunctionId(moduleFunction.getFunctionId()) > 0;
//        return moduleResult && roleResult;
//    }
//
//    @Override
//    public Boolean deleteByModuleId(String moduleId) {
//        return moduleFunctionMapper.deleteByModuleId(moduleId) > 0;
//    }
//
//    @Override
//    public Boolean update(ModuleFunction moduleFunction) {
//        Boolean isExists = checkExists(moduleFunction.getFunctionId(), moduleFunction.getModuleId());
//        if (!isExists) throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "要修改的按钮不存在");
//        return moduleFunctionMapper.updateById(moduleFunction) > 0;
//    }
//
//    @Override
//    public ModuleFunction load(String id) {
//        return moduleFunctionMapper.selectOne(id);
//    }
//
//    @Override
//    public List<ModuleFunction> loadAll(String order, Pageable pageable, Sort.Direction direction) {
//        return moduleFunctionMapper.list(order, pageable, direction);
//    }
//
//    @Override
//    public List<ModuleFunction> loadByModuleId(String moduleId, String order, Pageable pageable, Sort.Direction direction) {
//        return moduleFunctionMapper.listByModuleId(moduleId, order, pageable, direction);
//    }
//
//    @Override
//    public List<ModuleFunction> loadByModuleName(String moduleName, String order, Pageable pageable, Sort.Direction direction) {
//        return moduleFunctionMapper.listByModuleName(moduleName, order, pageable, direction);
//    }
//
//    @Override
//    public List<ModuleFunctionVO> listModuleFunctionVO(String order, Pageable pageable, Sort.Direction direction) {
//        return moduleFunctionMapper.listModuleFunctionVO(null, order, pageable, direction);
//    }
//
//    @Override
//    public List<ModuleFunctionVO> listModuleFunctionVO(String moduleId, String order, Pageable pageable,
//                                                                 Sort.Direction direction) {
//        return moduleFunctionMapper.listModuleFunctionVO(moduleId, order, pageable, direction);
//    }
//
//    @Override
//    public List<ModuleFunctionVO> listModuleFunctionVO() {
//        return moduleFunctionMapper.listModuleFunctionVO(null, null, null, null);
//    }
//
//    @Override
//    public Integer totalCount() {
//        return moduleFunctionMapper.selectCount(null);
//    }
//
//    @Override
//    public Integer totalCount(String moduleId) {
//        return moduleFunctionMapper.selectCount(moduleId);
//    }
//
//    private Boolean checkExists(String functionId, String moduleId) {
//        if (StringUtils.isBlank(functionId)) throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "按钮ID不能为空");
//        if (StringUtils.isBlank(moduleId)) throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "菜单ID不能为空");
//        return moduleFunctionMapper.checkExists(functionId, moduleId);
//    }
//}
