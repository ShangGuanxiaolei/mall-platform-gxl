package com.xquark.service.PolyProductService;

import com.xquark.dal.model.GoodInfo;
import java.util.List;
import org.neo4j.cypher.internal.compiler.v2_1.ast.In;

/**
 * @auther liuwei
 * @date 2018/6/13 22:01
 */
public interface PolyProductService {

  Integer getPolyCountTotal();

  List<GoodInfo> getPloyProductList(Integer PageIndex, Integer PageSize);

  void savePloyProductQuantity(Integer quantity,Integer id);

  void savePloyProductSkuQuantity(Integer quantity,String skuId);

  boolean checkProductId(Integer id);

  Integer getPloyProductQuantity(Integer id);

}
