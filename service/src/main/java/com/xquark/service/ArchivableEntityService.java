package com.xquark.service;

import com.xquark.dal.Archivable;

public interface ArchivableEntityService<E extends Archivable> extends BaseEntityService<E> {

  int delete(String id);

  int undelete(String id);
}
