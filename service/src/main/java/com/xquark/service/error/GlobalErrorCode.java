package com.xquark.service.error;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.HashMap;
import java.util.Map;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum GlobalErrorCode {
  //
  SUCESS(200, "Success"),

  // 访问权限
  UNAUTHORIZED(401, "Unauthorized"),
  AUTH_UNKNOWN(401000, "登陆校验未知错误"),
  MOBILE_PHONE_VERIFY_CODE_ERROR(401001, "验证码错误"),
  MOBILE_PHONE_USER_NOT_EXIST(401002, "该手机号尚未注册"),
  CP_USER_NOT_EXISTS(401006, "获取用户信息失败"),
  CP_TOKEN_EXPIRED_OR_NOT_EXIST(401005, "微信授权信息已失效, 请重新登录"),
  BAD_CREDENTIALS(401003, "未设置密码"),
  PASSWORD_VERIFY_ERROR(401004, "密码错误"),
  WECHAT_OAUTH_CODE_NOT_VAILD(401050, "微信第三方登陆失败, 请重试"),
  WECHAT_MINI_PROGRAM_LOGIN_ERROR(401080, "微信小程序登陆失败, 请重试"),

  SCRM_USER_SYNC_FAIL(501000, "用户同步失败"),
  SCRM_ADDRESS_ILLEGAL(502000, "地址不正确"),
  SCRM_ADDRESS_SYNC_FAIL(502001, "地址同步失败"),
  SCRM_PRODUCT_NOT_EXISTS(502002, "商品编码不存在"),

  COUPON_OUT_OF_STOCK(600000, "优惠券已被领完"),
  COUPON_OUT_OF_TIME(600001, "优惠券已过期"),
  COUPON_ACQUIRED(600002, "您已领取此优惠券请勿重复领取"),

  NOT_INSIDE_EMPLOYEE_ERROR(700001, "内部员工号不正确"),
  INSIDE_EMPLOYEE_REGISTERED(700002, "已注册为内部员工"),

  POINT_NOT_SUPPORT(8000000, "积分形式不支持"),
  DUPLICATE_PAYMENT(8000001, "请勿重复支付!"),
  REPEAT_APPLY_REFUND(40011, "重复提交退款申请"),
  NEED_APPLY_REFUND(40012, "未申请退款"),
  REPEAT_APPLY_REFUND_PROOF(40013, "退款申请不能重复提交"),
  NEED_PASS_REFUND_CHECK(40014, "退款申请未通过"),
  REPEAT_APPLY_CHANGE(40015, "重复提交换货申请"),
  REPEAT_APPLY_REISSUE(40016, "重复提交补货申请"),
  APPLY_CHANGE_FAIL(40017, "换货申请失败"),

  PIECE_PAY_FREE_ERROR(8000002, "您来晚啦, 免单名额已被抢走~"),
  POINT_NOT_ENOUGH(8000002, "积分不足"),

  COMMISSION_NOT_ENOUGH(8000003, "德分不足"),
  NEW_MAN_ERROR(8000002, "只有下过单的用户才能发红包哦"),
  POINT_RECORD_NOT_FOUNT(8000004, "找不到积分记录"),

  COMMISSION_RECORD_NOT_FOUNT(8000005, "找不到德分记录"),

  //
  NOT_FOUND(404, "Resource not found"),
  //
  INTERNAL_ERROR(500, "Server internal error"),

  // 第三方应用调用openapi签名错误
  SIGN_ERROR(21001, "签名错误"),

  // 第三方应用调用openapi获取用户失败
  USER_NOT_EXIST(21002, "获取用户失败"),

  //
  INVALID_ARGUMENT(11001, "Invalid argument"),
  // 错误的参数，原参数已修改， 页面需重新刷新
  INVALID_ARGUMENT_2(11002, "Invalid argument"),
  //
  THIRDPLANT_BUZERROR(700, "Business error"),
  FILE_SIZE_TOO_LARGE(401051, "文件大小超过限制"),
  FILE_UPLOAD_ERROR(401051, "文件上传失败"),

  WITH_DRAW_NOT_ENOUGH(8000008, "可提现积分不足"),

  WITH_DRAW_NOT_ENOUGH_PLATFORM(8000009, "当前平台可提现积分不足"),

  WITH_DRAW_LIMIT_NOT_ENOUGH(80000010, "账户可用积分不足，无法提现"),

  PRODUCT_INVENTORY(900001, "商品库存不足"),

  ORDER_DEST_CAN_NOT_BE_NULL(900001, "订单的是否可以退款字段不应该为空"),

  DUPLICATED_PIECE(1000001, "请勿重复参团"),

  PIECE_ERROR(1000002, "拼团异常"),

  PIECE_ERROR_OVERTIME(1000003, "拼团活动已结束"),

  MEETING_INVITE(409, "邀请码无效或失效。"),

  PIECE_IDENTITY_NOT_MATCH(501, "拼团身份不匹配"),

  REPEAT_SUBMIT(900002, "未选择换货商品"),

  REPEAT2_SUBMIT(900003, "未选择补货商品"),

  PROMOTION_LIMIT(9000004, "超过最大购买限制"),

  ERROR_PARAM(900005, "错误的参数"),
  NOT_DIVIDE_POINT(900006, "等额红包没有被整除,错误!"),
  NOT_DIVIDE_RANDOM_POINT(900007, "发放人数超过最大德分红包"),

  PACKAGE_GIFT_NOT_EXSIT(9000005, "红包不存在"),

  PACKAGE_GIFT_OVER_DUE(9000006, "红包已经过期"),

  PACKAGE_GIFT_BROUGHT_OUT(9000007, "红包已经领完"),

  PACKET_TYPE_ERROR(9000008, "红包类型错误"),

  PACKET_REPEAT_ERROR(9000009, "红包已经领过"),

  PACKET_AUTH(9000010, "红包领取权限校验失败"),

  BARGAIN_BIZ_ERR(7000001, "砍价业务异常"),

  PACKET_RAIN_COMMISSION_NOT_ENOUGH(9000011, "啊偶~不足10德分,攒德分去咯"),
  PACKET_RAIN_LOTTERY_LIMIT(9000012, "今日抽奖次数已用完,攒德分明日再战!"),

  FRESHMAN_LOTTERY_LIMIT(9000013, "抽奖次数已用完,不能在进行抽奖!"),

  // -------------------------- 预售相关错误码 ------------------------------
  RESERVE_LIMIT(10100, "商品预约总数已达到上限"),
  RESERVE_USER_LIMIT(10101, "用户的预约数已达到上限"),
  RESERVE_PRODUCT_NOT_EXISTS(10102, "预售活动商品不存在"),
  RESERVE_PAY_TIME_ERROR(10103, "支付时间无效"),
  RESERVE_PROMOTION_AMOUNT_LIMIT(10104, "手慢了一点点，好运下次降临"),
  RESERVE_PRE_ORDER_NO_NOT_EXISTS(10105, "预约单号不存在"),
  RESERVE_ORDER_TIME_ERROR(10106, "当前时间为非预约时间"),
  RESERVE_PRE_ORDER_NO_PAID(10107, "该预购单已支付"),
  RESERVE_PRE_ORDER_NO_CANCELLED(10108, "该预购单已失效"),
  RESERVE_PRE_ORDER_NO_USED(10109, "该预购单已使用"),

  // --------------------------订单导入错误码 ------------------------------
  ORDER_IMPORT_TYPR_ERROR(90001,"TYPE_ERROR"),
  ORDER_IMPORT_INSERT_ERROR(90002,"INSERT_ERROR"),
  ORDER_IMPORT_CREATE_ERROR(90003,"CREATE_ORDER_ERROR"),
  ORDER_IMPORT_SKUCODE_ERROR(90004,"ORDER_IMPORT_SKUCODE_ERROR"),
  ORDER_IMPORT_ORDER_NULL(90005,"ORDER_IMPORT_ORDER_NULL"),
  ORDER_IMPORT_CHECK_ERROR(90006,"ORDER_IMPORT_CHECK_ERROR"),
  ORDER_IMPORT_NO_AMOUNT(90007,"ORDER_IMPORT_NO_AMOUNT"),
  UNKNOWN(-1, "Unknown error");

  private static final Map<Integer, GlobalErrorCode> values =
      new HashMap<Integer, GlobalErrorCode>();

  static {
    for (GlobalErrorCode ec : GlobalErrorCode.values()) {
      values.put(ec.errorCode, ec);
    }
  }

  private int errorCode;
  private String error;

  private GlobalErrorCode(int errorCode, String error) {
    this.errorCode = errorCode;
    this.error = error;
  }

  public static GlobalErrorCode valueOf(int code) {
    GlobalErrorCode ec = values.get(code);
    if (ec != null) {
      return ec;
    }
    return UNKNOWN;
  }

  public int getErrorCode() {
    return errorCode;
  }

  public String getError() {
    return error;
  }

  public String render() {
    return errorCode + ":" + error;
  }
}
