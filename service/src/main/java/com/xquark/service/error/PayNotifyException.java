package com.xquark.service.error;

/**
 * @Description 支付回调异常
 * 
 * @author default
 *
 */
public class PayNotifyException extends RuntimeException {

	private static final long serialVersionUID = 1L;
  
	/**
	 * 
	 */
	public static final String SUCCESSMSG = "SUCCESS";
  	
  	public static final String FAILMSG = "FAIL";
  	
  	private String msg;
  	
	private int code;
  	
  	private ThirdRefundData data;
  	
  	private String payType;
  	
  	public PayNotifyException(int code,String msg,ThirdRefundData data,String payType){
  		super(msg);
		this.data = data;
		this.msg = msg;
		this.code = code;
		this.payType = payType;
	}
  	
  	public PayNotifyException(int code,String msg,ThirdRefundData data){
  		this(code,msg,data,"");
	}
  	
	public PayNotifyException(String msg,ThirdRefundData data){
		this(200,msg,data);
	}
	
	public PayNotifyException(int code,ThirdRefundData data){
		this(code,SUCCESSMSG,data);
	}

	public ThirdRefundData getData() {
		return data;
	}

	public void setData(ThirdRefundData data) {
		this.data = data;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}
	
 
}
