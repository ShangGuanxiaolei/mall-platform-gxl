package com.xquark.service.error;

public class XqProductBuyException extends BizException {

  private static final long serialVersionUID = -7821286782953993591L;

  public XqProductBuyException(GlobalErrorCode ec, String message) {
    super(ec, message);
  }

}
