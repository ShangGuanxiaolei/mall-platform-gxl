package com.xquark.service.error;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author default
 * @Description 请求退款信息
 *
 */
public class ThirdRefundData {
	
	@NotBlank(message = "来源系统不能为空！")
	@JSONField(name = "from_system")
    private String fromSystem;

    @NotBlank(message = "支付渠道不能为空！")
    @JSONField(name = "pay_channel")
    private String payChannel;
    
    @NotBlank(message = "支付方式不能为空！")
    @JSONField(name = "pay_type")
    private String payType;
    
    @NotBlank(message = "业务订单号不能为空！")
    private String ono;

    @NotBlank(message = "业务方退款单号不能为空！")
    @JSONField(name = "refund_no")
    private String refundNo;

    @NotBlank(message = "支付平台订单号不能为空！")
    @JSONField(name = "trade_no")
    private String tradeNo;
    //单位 分
    @NotNull(message = "退款金额不能为空！")
    @DecimalMin(value = "1", message = "退款金额不能小于1分！")
    private Integer amount;
    //单位 分
    @NotNull(message = "支付总金额不能为空！")
    @DecimalMin(value = "1", message = "支付总金额不能小于1分！")
    @JSONField(name = "total_amount")
    private Integer totalAmount;

    @NotBlank(message = "业务方用户ID不能为空！")
    private String cpid;

    @NotBlank(message = "回调地址不能为空！")
    @JSONField(name = "callback_url")
    private String callbackUrl;

    private String ip;

	public String getFromSystem() {
		return fromSystem;
	}

	public void setFromSystem(String fromSystem) {
		this.fromSystem = fromSystem;
	}

	public String getPayChannel() {
		return payChannel;
	}

	public void setPayChannel(String payChannel) {
		this.payChannel = payChannel;
	}

	public String getOno() {
		return ono;
	}

	public void setOno(String ono) {
		this.ono = ono;
	}

	public String getRefundNo() {
		return refundNo;
	}

	public void setRefundNo(String refundNo) {
		this.refundNo = refundNo;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getCpid() {
		return cpid;
	}

	public void setCpid(String cpid) {
		this.cpid = cpid;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public Integer getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}
	
}
