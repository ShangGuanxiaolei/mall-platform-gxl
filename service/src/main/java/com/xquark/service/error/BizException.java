package com.xquark.service.error;

import org.apache.commons.lang3.StringUtils;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * 业务逻辑异常。抛向前端，方便不同的客户端，不同的处理方式
 *
 * @author jamesp
 */
public class BizException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private final GlobalErrorCode errorCode;

  public BizException(GlobalErrorCode ec, String message, Throwable cause) {
    super(message, cause);
    errorCode = ec;
  }

  public BizException(GlobalErrorCode ec) {
    this(ec, ec.getError());
  }

  public BizException(GlobalErrorCode ec, String message) {
    this(ec, message, null);
  }

  public BizException(GlobalErrorCode ec, Throwable cause) {
    this(ec, null, cause);
  }

  public static Supplier<BizException> ofSupplier(GlobalErrorCode ec) {
    return () -> new BizException(ec, ec.getError());
  }

  public static Supplier<BizException> ofSupplier(GlobalErrorCode ec, String msg) {
    return () -> new BizException(ec, msg);
  }

  public static Function<Throwable, BizException> ofThrowable() {
    return e ->
        new BizException(
            GlobalErrorCode.INTERNAL_ERROR,
            StringUtils.defaultIfBlank(e.getMessage(), "服务器错误, 请稍后再试"),
            e);
  }

  public GlobalErrorCode getErrorCode() {
    return errorCode;
  }
}
