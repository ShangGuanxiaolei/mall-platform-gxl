package com.xquark.service.error;

public class NonCheckBizException extends BizException {
  public NonCheckBizException(GlobalErrorCode ec, String message, Throwable cause) {
    super(ec, message, cause);
  }

  public NonCheckBizException(GlobalErrorCode ec) {
    super(ec);
  }

  public NonCheckBizException(GlobalErrorCode ec, String message) {
    super(ec, message);
  }

  public NonCheckBizException(GlobalErrorCode ec, Throwable cause) {
    super(ec, cause);
  }
}
