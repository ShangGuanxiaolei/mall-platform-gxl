package com.xquark.service.whitelist.impl;

import com.xquark.dal.mapper.PromotionWhitelistMapper;
import com.xquark.service.whitelist.WhitelistService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author Damon S.
 */
@Service
public class WhitelistServiceImpl implements WhitelistService {

    private static final Long CPID_BOTTOM = 1000000L;

    private static final Long WHITELIST_FLAG_NUM = 9999999L;

    @Autowired
    private PromotionWhitelistMapper promotionWhitelistMapper;


    @Override
    public boolean checkValidation(String pCode, Long cpId) {
        if (StringUtils.isBlank(pCode)) {
            throw new RuntimeException("活动pCode不能为空");
        }
        if (Objects.isNull(cpId) || cpId < CPID_BOTTOM) {
            throw new RuntimeException("cpId不合法");
        }
        // 先判断 9999999 是否生效（即白名单是否生效），再判断是否存在该CPID
        return promotionWhitelistMapper.checkWhitelistValidation(pCode, WHITELIST_FLAG_NUM) > 0
                && promotionWhitelistMapper.checkWhitelistValidation(pCode, cpId) > 0;
    }
}
