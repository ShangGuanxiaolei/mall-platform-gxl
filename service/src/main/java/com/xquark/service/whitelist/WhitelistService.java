package com.xquark.service.whitelist;

public interface WhitelistService {

    /**
     * 检查是否在白名单中
     * @param pCode
     * @param cpId
     * @return
     */
    boolean checkValidation(String pCode, Long cpId);
}
