package com.xquark.service.supplier;

import com.xquark.dal.model.Supplier;
import com.xquark.dal.model.SupplierWareHouse;
import com.xquark.service.base.TemplateBaseService;
import java.util.List;

/**
 * User: huangjie Date: 2018/6/26. Time: 下午5:55
 */
public interface SupplierService extends TemplateBaseService<Supplier> {

  /**
   * 添加
   */
  boolean insert(Supplier obj, List<SupplierWareHouse> list);

  /**
   * 更新
   */
  boolean update(Supplier obj, List<SupplierWareHouse> list);


}