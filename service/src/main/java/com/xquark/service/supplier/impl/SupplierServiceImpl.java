package com.xquark.service.supplier.impl;

import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.mapper.SupplierMapper;
import com.xquark.dal.mapper.SupplierWarehouseMapper;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.Supplier;
import com.xquark.dal.model.SupplierWareHouse;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.vo.SupplierVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.supplier.SupplierService;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotNull;

import io.netty.handler.codec.spdy.SpdyHttpResponseStreamIdHandler;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

/**
 * User: huangjie Date: 2018/6/26. Time: 下午5:56 供应商服务
 */
@Service
@Transactional
@Validated
public class SupplierServiceImpl extends BaseServiceImpl implements SupplierService {

  @Autowired
  private SupplierMapper baseMapper;

  @Autowired
  private SupplierWarehouseMapper supplierWarehouseMapper;

  @Autowired
  private SkuMapper skuMapper;
  /**
   * 根据id查询
   */
  @Override
  public SupplierVO getByPrimaryKey(@NotBlank String id) {
    return baseMapper.getByPrimaryKey(id);
  }

  /**
   * 插入数据
   */
  @Override
  public boolean insert(@NotNull Supplier obj, @NotNull List<SupplierWareHouse> list) {
    boolean ret;
    ret = baseMapper.insert(obj) > 0;
    for (SupplierWareHouse sw : list) {
      sw.setSupplierId(obj.getId());
    }
    if (CollectionUtils.isNotEmpty(list)) {
      supplierWarehouseMapper.batchInsert(list);
    }
    return ret;
  }

  /**
   * 修改数据
   */
  @Override
  public boolean update(@NotNull Supplier obj, @NotNull List<SupplierWareHouse> list) {
    boolean ret;
    List<Supplier> suppliers = baseMapper.selectAll();
    List<Sku> skus = null;
    for (Supplier supplier : suppliers) {
      if (supplier.getId().equals(Long.toString(IdTypeHandler.decode(obj.getId())))) {
        skus = skuMapper.listByResourceCode(supplier.getCode());
      }
    }
    ret = baseMapper.update(obj) > 0;
      for (SupplierWareHouse sw : list) {
        sw.setSupplierId(obj.getId());
      }
      supplierWarehouseMapper.deleteBySupplierId(obj.getId());
      if (CollectionUtils.isNotEmpty(list)) {
        supplierWarehouseMapper.batchInsert(list);
      }

      if (CollectionUtils.isNotEmpty(skus)) {
          Sku sku = skus.get(0);
          skuMapper.updateSkuCodeResources(obj.getCode(), sku.getSkuCodeResources());
      }

      return ret;
    }

  @Override
  public boolean delete(@NotBlank String id) {
    return baseMapper.delete(id) > 0;
  }

  /**
   * 批量删除
   *
   * @param ids id集合
   */
  @Override
  public boolean batchDelete(@NotEmpty List<String> ids) {
    final boolean ret = baseMapper.batchDelete(ids) > 0;
    return ret;
  }


  /**
   * 分页查询
   */
  @Override
  public List<? extends Supplier> list(Map<String, Object> params) {
    return baseMapper.list(params);
  }

  @Override
  public Boolean insert(Supplier record) {
    return null;
  }

  @Override
  public Boolean update(Supplier record) {
    return null;
  }


  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long count(Map<String, Object> params) {
    return baseMapper.count(params);
  }


}
