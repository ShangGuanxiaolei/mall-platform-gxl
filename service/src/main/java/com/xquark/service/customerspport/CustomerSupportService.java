package com.xquark.service.customerspport;

import java.util.List;

import com.xquark.dal.model.CustomerService;
import com.xquark.service.ArchivableEntityService;

public interface CustomerSupportService extends ArchivableEntityService<CustomerService> {

  CustomerService getSupport();

  List<CustomerService> listAll();

  List<CustomerService> getSupportByName(String userId);

  List<CustomerService> getSupportByPhone(String phone);

  List<CustomerService> getSupportByScope(String scope);

  Boolean addService(CustomerService e);
}
