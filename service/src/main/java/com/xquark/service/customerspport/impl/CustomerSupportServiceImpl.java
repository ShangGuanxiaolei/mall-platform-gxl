package com.xquark.service.customerspport.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.CustomerServiceMapper;
import com.xquark.dal.model.CustomerService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.customerspport.CustomerSupportService;

@Service("CustomerSupportService")
public class CustomerSupportServiceImpl extends BaseServiceImpl implements CustomerSupportService {

  @Autowired
  private CustomerServiceMapper customerServiceMapper;

  @Override
  public int delete(String id) {
    return customerServiceMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int undelete(String id) {
    return customerServiceMapper.undeleteByPrimaryKey(id);
  }

  @Override
  public Boolean addService(CustomerService e) {
    return customerServiceMapper.insert(e) == 1 ? true : false;
  }

  @Override
  public CustomerService load(String id) {
    return customerServiceMapper.load(id);
  }

  @Override
  public CustomerService getSupport() {
    List<CustomerService> aList = customerServiceMapper.loadAll();
    if (aList != null && aList.size() != 0) {
      return aList.get((int) (aList.size() * Math.random()));
    } else {
      return null;
    }
  }

  @Override
  public List<CustomerService> getSupportByName(String name) {
    return customerServiceMapper.selectByName(name);
  }

  @Override
  public List<CustomerService> getSupportByPhone(String phone) {
    return customerServiceMapper.selectByPhone(phone);
  }

  @Override
  public List<CustomerService> getSupportByScope(String scope) {
    return customerServiceMapper.getSupportByScope("%" + scope + "%");
  }

  @Override
  public int insert(CustomerService e) {
    return customerServiceMapper.insert(e);
  }

  @Override
  public int insertOrder(CustomerService customerService) {
    return customerServiceMapper.insert(customerService);
  }

  @Override
  public List<CustomerService> listAll() {
    return customerServiceMapper.loadAll();
  }

}
