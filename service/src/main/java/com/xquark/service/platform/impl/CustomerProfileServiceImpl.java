package com.xquark.service.platform.impl;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.constrant.GradeCodeConstrants;
import com.hds.xquark.dal.model.CustomerWithdrawal;
import com.hds.xquark.dal.type.TotalAuditType;
import com.hds.xquark.dal.type.Trancd;
import com.hds.xquark.service.point.PointCommService;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.dal.type.*;
import com.xquark.dal.vo.*;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CareerLevelService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.platform.pojo.UpLineBO;
import com.xquark.service.pricing.base.PgPromotionInfo;
import com.xquark.service.pricing.base.PromotionInfo;
import com.xquark.service.push.type.Platform;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.parboiled.common.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.xquark.dal.consts.MsgCodeConstrant.LIGHTEN;
import static com.xquark.dal.type.CareerLevelType.PW;
import static com.xquark.dal.type.CareerLevelType.RC;

/**
 * created by 中台customerProfile
 *
 * @author wangxinhua at 18-6-6 下午11:36
 */
@Service
public class CustomerProfileServiceImpl extends BaseServiceImpl implements CustomerProfileService {

  private final CustomerMessageMapper customerMessageMapper;

  private final CareerLevelService careerLevelService;

  private final OrderHeaderMapper orderHeaderMapper;

  private final OrderMapper orderMapper;

  private final CustomerProfileMapper customerProfileMapper;

  private final ConsumerToConsumerMapper consumerToConsumerMapper;

  private final CustomerWechartUnionMapper wechartUnionMapper;

  private final ConsumerToCustomerMapper consumerToCustomerMapper;

  @Autowired private PromotionGoodsMapper promotionGoodsMapper;

  private final PointCommService pointCommService;

  @Autowired private RecordPromotionPgTranInfoMapper recordPromotionPgTranInfoMapper;

  @Autowired private WeakLinkMapper weakLinkMapper;

  @Autowired
  private CustomerProfileHDSMapper customerProfileHDSMapper;

  @Autowired
  private CustomerProfileVvlifeMapper customerProfileVvlifeMapper;

  @Autowired
  private UserMapper userMapper;

  private static final Logger LOGGER = LoggerFactory.getLogger(CustomerProfileServiceImpl.class);

  private static final Long COMPANY_CPID = 2000002L;

  @Autowired
  public CustomerProfileServiceImpl(
          CustomerMessageMapper customerMessageMapper,
          CareerLevelService careerLevelService,
          OrderHeaderMapper orderHeaderMapper,
          OrderMapper orderMapper,
          CustomerProfileMapper customerProfileMapper,
          ConsumerToConsumerMapper consumerToConsumerMapper,
          CustomerWechartUnionMapper wechartUnionMapper,
          ConsumerToCustomerMapper consumerToCustomerMapper,
          PointContextInitialize pointContextInitialize) {
    this.customerMessageMapper = customerMessageMapper;
    this.careerLevelService = careerLevelService;
    this.orderHeaderMapper = orderHeaderMapper;
    this.orderMapper = orderMapper;
    this.customerProfileMapper = customerProfileMapper;
    this.consumerToConsumerMapper = consumerToConsumerMapper;
    this.wechartUnionMapper = wechartUnionMapper;
    this.consumerToCustomerMapper = consumerToCustomerMapper;
    this.pointCommService = pointContextInitialize.getPointService();
  }

  /** 查询用户是否点亮 */
  @Override
  public boolean isLighten(Long cpId) {
    CustomerCareerLevel careerLevel = careerLevelService.load(cpId);
    if (careerLevel == null) {
      return false;
    }
    return careerLevel.getIsLightening();
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public LightenResult lightenWrapped(Long cpId, PlatformType source) {
    CustomerCareerLevel careerLevel = careerLevelService.load(cpId);

    if (careerLevel == null) {
      LOGGER.error(GlobalErrorCode.INVALID_ARGUMENT.getError() + "：cpId没有对应的用户");
      return new LightenResult(GlobalErrorCode.INVALID_ARGUMENT, "cpId没有对应的用户");
    }
    boolean isLighten = careerLevel.getIsLightening();
    if (isLighten) {
      LOGGER.error(GlobalErrorCode.INVALID_ARGUMENT.getError() + "：该用户已经点亮, 请不要重复操作");
      return new LightenResult(GlobalErrorCode.INVALID_ARGUMENT, "该用户已经点亮, 请不要重复操作");
    }
    String hdsType = careerLevel.getHdsType();
    String viviType = careerLevel.getViviLifeType();
    String sp = "SP";
    // 两个平台都是sp才满足点亮条件
    if (!(StringUtils.equals(hdsType, sp) && StringUtils.equals(viviType, sp))) {
      LOGGER.error(GlobalErrorCode.INVALID_ARGUMENT.getError() + "：用户未满足点亮条件");
      return new LightenResult(GlobalErrorCode.INVALID_ARGUMENT, "用户未满足点亮条件");
    }
    careerLevel.setIsLightening(true);
    careerLevel.setLightenDate(new Date());
    boolean ret = careerLevelService.update(careerLevel, source);

    // FIXME 消息统一放置
    String msg = "恭喜您成为社交新零售全国代理!";
    CustomerMessage messageHds = new CustomerMessage(cpId, LIGHTEN, msg, PlatformType.H.getCode());
    CustomerMessage messageVivi = new CustomerMessage(cpId, LIGHTEN, msg, PlatformType.V.getCode());

    // 插入两条消息提醒用户身份点亮
    customerMessageMapper.insertMultiple(ImmutableList.of(messageHds, messageVivi));
    return new LightenResult(ret);
  }


  /**
   * 点亮逻辑
   *
   * @return 是否点亮成功
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public boolean lighten(Long cpId, PlatformType source) {
    CustomerCareerLevel careerLevel = careerLevelService.load(cpId);
    if (careerLevel == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "cpId没有对应的用户");
    }
    boolean isLighten = careerLevel.getIsLightening();
    if (isLighten) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该用户已经点亮, 请不要重复操作");
    }
    String hdsType = careerLevel.getHdsType();
    String viviType = careerLevel.getViviLifeType();
    String sp = "SP";
    // 两个平台都是sp才满足点亮条件
    if (!(StringUtils.equals(hdsType, sp) && StringUtils.equals(viviType, sp))) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "用户未满足点亮条件");
    }
    careerLevel.setIsLightening(true);
    careerLevel.setLightenDate(new Date());
    boolean ret = careerLevelService.update(careerLevel, source);

    // FIXME 消息统一放置
    String msg = "恭喜您成为社交新零售全国代理!";
    CustomerMessage messageHds = new CustomerMessage(cpId, LIGHTEN, msg, PlatformType.H.getCode());
    CustomerMessage messageVivi = new CustomerMessage(cpId, LIGHTEN, msg, PlatformType.V.getCode());

    // 插入两条消息提醒用户身份点亮
    customerMessageMapper.insertMultiple(ImmutableList.of(messageHds, messageVivi));
    return ret;
  }

  @Override
  public List<CustomerMessage> listUnReadMessages(
          Long cpId, String msgCode, Integer source, Integer limit) {
    return customerMessageMapper.listUnRead(cpId, msgCode, source, limit);
  }

  @Override
  public List<CustomerMessage> readUnReadMessages(
          Long cpId, String msgCode, Integer source, Integer limit) {
    List<CustomerMessage> ret = listUnReadMessages(cpId, msgCode, source, limit);
    if (CollectionUtils.isEmpty(ret)) {
      return Collections.emptyList();
    }
    // 更新为已读
    updateReadStatusMultiple(ret);
    return ret;
  }

  /**
   * 查询业绩 暂时只有零售业绩
   */
  @Override
  public Map<CareerTotalType, String> listCareerTotal(Long cpId, String month) {
    Map<CareerTotalType, String> ret = new HashMap<>();

    boolean isShopkeeper = careerLevelService.isShopkeeper(cpId);
    boolean isVip = careerLevelService.isVip(cpId);
    BigDecimal retailTotal;
    if (isShopkeeper) { // 店主或代理(个人业绩+小组业绩)
      ret.put(CareerTotalType.PPV_HVMALL, BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
      retailTotal = orderHeaderMapper.selectPPVPerformanceByHvmall(cpId, Integer.valueOf(month));
      if (retailTotal != null) {
        ret.put(CareerTotalType.PPV_HVMALL, retailTotal.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
      }

      ret.put(CareerTotalType.PPV_ALL, BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
      retailTotal = orderHeaderMapper.selectPPVPerformanceByAll(cpId, Integer.valueOf(month));
      if (retailTotal != null) {
        ret.put(CareerTotalType.PPV_ALL, retailTotal.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
      }

      ret.put(CareerTotalType.PV_HVMALL, BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
      retailTotal = orderHeaderMapper.selectPVPerformanceByHvmall(cpId, Integer.valueOf(month));
      if (retailTotal != null) {
        ret.put(CareerTotalType.PV_HVMALL, retailTotal.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
      }

      ret.put(CareerTotalType.PV_ALL, BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
      retailTotal = orderHeaderMapper.selectPVPerformanceByAll(cpId, Integer.valueOf(month));
      if (retailTotal != null) {
        ret.put(CareerTotalType.PV_ALL, retailTotal.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
      }

    } else if (isVip) { // vip 只有个人业绩
      ret.put(CareerTotalType.PPV_HVMALL, BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
      retailTotal = orderHeaderMapper.selectPPVPerformanceByHvmall(cpId, Integer.valueOf(month));
      if (retailTotal != null) {
        ret.put(CareerTotalType.PPV_HVMALL, retailTotal.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
      }

      ret.put(CareerTotalType.PPV_ALL, BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
      retailTotal = orderHeaderMapper.selectPPVPerformanceByAll(cpId, Integer.valueOf(month));
      if (retailTotal != null) {
        ret.put(CareerTotalType.PPV_ALL, retailTotal.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
      }
    } else {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该用户身份不符合，业绩查询失败");
    }

    return ret;
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public boolean updateReadStatusMultiple(List<CustomerMessage> messages) {
    if (CollectionUtils.isEmpty(messages)) {
      return false;
    }
    for (CustomerMessage message : messages) {
      message.setIsRead(true);
    }
    return customerMessageMapper.updateReadStatusMultiple(messages) > 0;
  }

  /**
   * 1. 拿cpId先查看customercareerlevel是否存在该人，不存在，说明是白人 2. 检查hds_type是否有值，如果有值，说明是三网人员 3.
   * 如果hds_type无值，接着判断vivilife_type是否有值，有值，如果是DS or SP 那说明是三网的人员 4. 其他都为白人
   */
  @Override
  public boolean hasIdentity(Long cpId) {
    CustomerCareerLevel careerLevel = careerLevelService.load(cpId);
    if (careerLevel == null) {
      return false;
    }
    String hdsType = careerLevel.getHdsType();
    if (StringUtils.isNotBlank(hdsType)) {
      return StringUtils.equalsIgnoreCase(hdsType, "DS")
              || StringUtils.equalsIgnoreCase(hdsType, "SP");
    }
    String viviLifeType = careerLevel.getViviLifeType();
    return StringUtils.equalsIgnoreCase(viviLifeType, "DS")
            || StringUtils.equalsIgnoreCase(viviLifeType, "SP");
  }

  /**
   * 根据上下级判断是否有关系, 强关系跟弱关系都算
   */
  @Override
  public boolean hasIdentityConnection(Long cpId) {
    return hasUplineCpId(cpId) || getBoundUpline(cpId) != null;
  }

  /**
   * 根据cpId判断是不是白人, 包括纯白人跟弱白人
   */
  @Override
  public boolean isConnectionRC(Long cpId) {
    if (hasUplineCpId(cpId)) {
      return false;
    }
    return getBoundUpline(cpId) == null;
  }

  @Override
  public List<BaseCustomerVO> listCustomerByUnionId(String unionId) {
    return customerProfileMapper.listCustomerByUnionId(unionId);
  }

  @Override
  public List<WxVO> listCusbyUni(String unionId) {
    return customerProfileMapper.listCusByUnionId(unionId);
  }

  @Override
  public boolean isWechatUnionExists(String unionId) {
    return customerProfileMapper.selectWechatUnionExists(unionId);
  }

  @Override
  public String loadAvatarByCpIdAndUnionId(Long cpId, String unionId) {
    return wechartUnionMapper.selectAvatarByUnionIdAndCpId(cpId, unionId);
  }

  @Override
  public Boolean updatePhone(Long cpId, String phone) {
    return customerProfileMapper.updatePhone(cpId, phone);
  }

  @Override
  public Long getUpline(Long currCpId, Long fromCpId) {

    // 如果自己点击自己的链接进来upline置为空
    if ((!Objects.isNull(fromCpId)) && (fromCpId.equals(currCpId))) {
      fromCpId = null;
    }

    // 被分享人有强关系,通过强关系查找
    if (hasUplineCpId(currCpId)) {
      CustomerProfile customerProfile = customerProfileMapper.selectByPrimaryKey(currCpId);
      return Optional.ofNullable(customerProfile.getUplineCpId()).orElse(COMPANY_CPID);
    }

    // 被分享人有次强关系, 分享人有身份
    Long boundCpId = getBoundUpline(currCpId);
    if (boundCpId != null && !boundCpId.equals(currCpId)) {
      return boundCpId;
    }
    // 被分享人有次强关系, 分享人没有身份
    Long boundRC = getBoundRC(currCpId);
    if (boundRC != null) {
      //防止自己绑定自己
      if(boundRC.equals(currCpId)){
        return this.getUpline4RC(currCpId);
      }
      return boundRC;
    }
    // 被分享人有弱关系
    Long shareCpId = getShareCpId(currCpId);
    if (fromCpId == null && shareCpId != null) {
      return shareCpId;//分享人为空，传入弱关系上级
    }
    //以上都不满足
    return fromCpId == null ? COMPANY_CPID : fromCpId;
  }

  private Long getShareCpId(Long currCpId) {
    return weakLinkMapper.selectShareCpId(currCpId);
  }

  /**
   * 是否有弱关系及以上的身份
   *
   * @param currCpId
   * @return
   */
  @Override
  public boolean getIdentify(Long currCpId) {
    if (hasUplineCpId(currCpId)) {
      return true;
    }
    if (getBoundUpline(currCpId) != null) {
      return true;
    }
    if (getBoundRC(currCpId) != null) {
      return true;
    }
    return false;
  }

  @Override
  /** 找到当前人在弱关系表中第一个有身份的上级 如果找不到就返回总部id 2000002 */
  public Long getFirstIdentityUpline(Long currCpId) {
    if (currCpId == null) {
      return null;
    }
//    // 遇到有身份的就返回
//    if (hasUplineCpId(currCpId)) {
//      return currCpId;
//    }
//    // 否则找弱关系(身份跟白人关系), 如果没有就返回公司
//    Long next = getUplineWeekIgnoreIdentity(currCpId);
//    if (next == null) {
//      return COMPANY_CPID;
//    }
    UpLineBO upLineBO = new UpLineBO();
    upLineBO.setCpId(currCpId);
    upLineBO.setUpLineCpId(currCpId);
    upLineBO.setCpIdList(new ArrayList<>());

    // 还有弱关系, 继续递归查找，并设置最大迭代次数100
    return getFirstIdentityUpline(upLineBO,100);
  }

  /**
   *
   * @param upLineBO
   * @param maxDeep 最大深度
   * @return
   */
  private Long getFirstIdentityUpline(UpLineBO upLineBO, int maxDeep) {
    Long currCpId = upLineBO.getUpLineCpId();
    if (currCpId == null) {
      return null;
    }

    upLineBO.getCpIdList().add(currCpId);

    // 遇到有身份的就返回
    if (hasUplineCpId(currCpId)) {
      return currCpId;
    }

    // 否则找弱关系(身份跟白人关系), 如果没有就返回公司
    Long next = getUplineWeekIgnoreIdentity(currCpId);
    if (next == null) {
      return COMPANY_CPID;
    }

    // 出现互相绑定，防止迭代死循环
    if(upLineBO.getCpIdList().contains(next)){
      log.info("cpId是" + upLineBO.getCpId() + "的上级出现互绑定，其中一个互绑上级是" + next);
      CustomerProfile c = this.queryCustomerProfileByCpId(upLineBO.getCpId());

      if(Objects.isNull(c) || Objects.isNull(c.getUplineCpId()))
        return COMPANY_CPID;
      return c.getUplineCpId();
    }

    // 防止出现意外一直迭代
    if(maxDeep < upLineBO.getCurrentDeep()){
      log.info("cpId是" + upLineBO.getCpId() + "查询上级迭代次数超过" + maxDeep + "次");
      CustomerProfile c = this.queryCustomerProfileByCpId(upLineBO.getCpId());

      if(Objects.isNull(c) || Objects.isNull(c.getUplineCpId()))
        return COMPANY_CPID;
      return c.getUplineCpId();
    }

    upLineBO.setUpLineCpId(next);

    // 默认值
    if(maxDeep < 1)
      maxDeep = 50;

    upLineBO.setCurrentDeep(upLineBO.getCurrentDeep() + 1);
    // 还有弱关系, 继续递归查找
    return getFirstIdentityUpline(upLineBO, maxDeep);
  }
  /**
   * 根据不同的活动, 获取上级的方式不同
   */
  @Override
  public Long getUpline(Long currCpId, Long fromCpId, PromotionInfo promotionInfo) {
    if (promotionInfo == null) {
      return getUpline(currCpId, fromCpId);
    }
    PromotionType promotionType = promotionInfo.getPromotionType();
    switch (promotionType) {
      // 拼团订单走默认逻辑
      //      case PIECE_GROUP:
      //        return getUplinePiece(currCpId, fromCpId, promotionInfo);
      case VIP_RIGHT:
        return getUplineStrong(currCpId, fromCpId);
      default:
        return getUpline(currCpId, fromCpId);
    }
  }

  @Override
  public Long getUplineStrong(Long currCpId, Long fromCpId) {

    // 如果自己点击自己的链接进来upline置为空
    if ((!Objects.isNull(fromCpId)) && (fromCpId.equals(currCpId))) {
      fromCpId = null;
    }

    // 当前用户是否有身份
    if (hasUplineCpId(currCpId)) {
      CustomerProfile customerProfile = customerProfileMapper.selectByPrimaryKey(currCpId);
      return Optional.ofNullable(customerProfile.getUplineCpId()).orElse(COMPANY_CPID);
    }

    //没有分享人，用户自己去商城下单
    if (fromCpId == null) {
      Long boundCpId = getBoundUpline(currCpId);
      if (boundCpId != null && !boundCpId.equals(currCpId)) {
        return boundCpId;
      }

      Long boundRC = getBoundRC(currCpId);
      if (boundRC != null) {
        //防止自己绑定自己
        if(boundRC.equals(currCpId)){
          return this.getUpline4RC(currCpId);
        }
        return boundRC;
      }

      Long shareCpId = getShareCpId(currCpId);
      if(shareCpId != null && !shareCpId.equals(currCpId)) {
        return shareCpId;
      }
      //以上都不满足
      return COMPANY_CPID;
    }

    // 分享人是有身份的人
    if (hasUplineCpId(fromCpId)) {
      return fromCpId;
    }

    return fromCpId == null ? COMPANY_CPID : fromCpId;
  }

  /**
   * 从consumerToConsumer表中获取上级（不包含自己）
   * @param currCpId
   * @return
   */
  private Long getUpline4RC(Long currCpId) {
    Long customerCpId = this.consumerToConsumerMapper.selectCustomerCpId(currCpId);
    if(currCpId.equals(customerCpId)){
      //若还是自己绑定自己，则找若关系表中的分享人，没有分享人则默认公司
      Long shareCpId = this.weakLinkMapper.selectShareCpId(currCpId);
      return shareCpId == null ? COMPANY_CPID : shareCpId;
    }
    return customerCpId;
  }

  private Long getUplinePiece(Long currCpId, Long fromCpId, PromotionInfo promotionInfo) {
    if (promotionInfo instanceof PgPromotionInfo) {
      PgPromotionInfo pgInfo = (PgPromotionInfo) promotionInfo;
      // 如果是参团分享, 则分享人id为
      String tranCode = pgInfo.getTranCode();
      PGTranInfoVo tranInfoVo = recordPromotionPgTranInfoMapper.selectTranInfoVOByCode(tranCode);
      if (tranInfoVo != null) {
        CustomerProfile customerProfile = customerProfileMapper.selectByPrimaryKey(currCpId);
        // 如果有上级，且为强关系fromCpId置为null
        if (customerProfile.getUplineCpId() != null) {
          fromCpId = null;
        } else {
          Long customerCpId = consumerToCustomerMapper.selectCustomerIdByConsumeId(currCpId);
          // 如果是弱关系,收益给上级
          if (customerCpId != null) {
            fromCpId = customerCpId;
          } else {
            // 以上都不满足给拼团主
            fromCpId = Long.valueOf(tranInfoVo.getGroupHeadId());
          }
        }
        return fromCpId;
      }
    }
    return getUpline(currCpId, fromCpId);
  }

  @Override
  public Long getUplineX(Long currCpId, Long fromCpId) {
    // 当前用户是否有upline, 即有身份
    if (!hasUplineCpId(currCpId)) {
      return COMPANY_CPID;
    }
    // 已绑定的cpId
    Long boundCpId = getBoundUpline(currCpId);
    if (boundCpId != null) {
      return boundCpId;
    }
    // 分享人跟被分享人都是白人, 不记录关系
    return COMPANY_CPID;
  }

  @Override
  public Long getUplineWeak(Long currCpId) {
    // 如果是强关联返回
    if (hasUplineCpId(currCpId)) {
      return null;
    }

    // 获取弱关联信息
    return getBoundUpline(currCpId);
  }

  @Override
  public Long getFromConsumerCpId(Long currCpId) {
    return consumerToConsumerMapper.selectUplineConsumerCpId(currCpId);
  }

  /** 查询当前用户是否为纯白人（未下过订单）fix */
  @Override
  public boolean isNew(String cpid) {
    return !hasIdentity(Long.valueOf(cpid)) && promotionGoodsMapper.selectIsNew(cpid) == 0;
  }

  /**
   * 判断是否只下了一次单或者没下过单
   */
  @Override
  public boolean isFirstOrder(String cpId) {
    return promotionGoodsMapper.selectIsNew(cpId) <= 1;
  }

  /**
   * 查询绑定的upline
   */
  @Override
  public Long getBoundUpline(Long currCpId) {
    return consumerToCustomerMapper.selectCustomerIdByConsumeId(currCpId);
  }

  /**
   * 查询白人关系表
   */
  @Override
  public Long getBoundRC(Long currCpId) {
    return consumerToConsumerMapper.selectUplineConsumerCpId(currCpId);
  }

  /**
   * 判断用户是否是纯白人,根据关系网
   *
   * @param currCpId currCpId
   * @return true  or false
   */
  @Override
  public boolean isPureWhite(Long currCpId) {
    if (getShareCpId(currCpId) != null) {//弱关系有值
      if (getUplineWeekIgnoreIdentity(currCpId) != null) {//次强关系有值，不是纯白人
        return false;
      } else {
        return hasIdentity(currCpId);//次强关系无值，判断是否有身份，没身份为纯白人
      }
    } else {//弱关系无值
      if (getUplineWeekIgnoreIdentity(currCpId) == null) {//次强关系无值
        return hasIdentity(currCpId);//判断是否有身份，没身份为纯白人
      } else {
        return false;//次强关系有值，不是纯白人
      }
    }
  }

  /**
   * 判断用户是否是VIP
   *
   * @param cpId cpId
   * @return true  or false
   */
  @Override
  public boolean isVip(Long cpId) {
    CustomerCareerLevel careerLevel = careerLevelService.load(cpId);
    if (careerLevel == null) {
      return false;
    }
    String hdsType = careerLevel.getHdsType();
    String viviLifeType = careerLevel.getViviLifeType();
    if (StringUtils.isNotBlank(hdsType)) {
      if (StringUtils.equalsIgnoreCase(hdsType, "DS")) {
        return true;
      }
    }
    if (StringUtils.isNotBlank(viviLifeType)) {
      return StringUtils.equalsIgnoreCase(viviLifeType, "DS");
    } else {
      return false;
    }
  }

  /**
   * 判断用户是否是店主
   *
   * @param cpId cpId
   * @return true  or false
   */
  @Override
  public boolean isSp(Long cpId) {
    CustomerCareerLevel careerLevel = careerLevelService.load(cpId);
    if (careerLevel == null) {
      return false;
    }
    String hdsType = careerLevel.getHdsType();
    String viviLifeType = careerLevel.getViviLifeType();
    if (StringUtils.isNotBlank(hdsType)) {
      if (StringUtils.equalsIgnoreCase(hdsType, "SP")) {
        return true;
      }
    }
    if (StringUtils.isNotBlank(viviLifeType)) {
      return StringUtils.equalsIgnoreCase(viviLifeType, "SP");
    } else {
      return false;
    }
  }

  @Override
  public boolean isProxy(Long cpId) {
    boolean b = false;
    RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
    String json = redisUtils.get("login_return_" + cpId);
    if (json != null) {

      JSONObject jsonObject = JSONObject.parseObject(json);
      Boolean isProxy = jsonObject.getBoolean("isProxy");
      if(isProxy != null)
        b = isProxy;
    }
    return b;
  }

  @Override
  public boolean isMember(Long cpId) {
    boolean b = false;
    RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
    String json = redisUtils.get("login_return_" + cpId);
    if (json != null) {
      JSONObject jsonObject = JSONObject.parseObject(json);
      Boolean isMember = jsonObject.getBoolean("isMember");
      if(isMember != null)
        b = isMember;
    }
    return b;
  }

  /**
   * 查询次强关系上级
   *
   * @param currCpId currCpId
   * @return Long
   */
  @Override
  public Long getUplineWeekIgnoreIdentity(Long currCpId) {
    Long ret = getBoundUpline(currCpId);
    if (ret == null) {
      ret = getBoundRC(currCpId);
    }
    return ret;
  }

  @Override
  public boolean isProfileExists(Long cpId) {
    return customerProfileMapper.selectExists(cpId);
  }

  /**
   * 查询是否有身份, 关联过upline
   */
  @Override
  public boolean hasUplineCpId(Long currCpId) {
    return hasIdentity(currCpId);
  }

  @Override
  public boolean saveConsumerToCustomer(ConsumerToCustomer consumerToCustomer) {
    return consumerToCustomerMapper.insert(consumerToCustomer) > 0;
  }

  @Override
  public boolean saveConsumerToConsumer(ConsumerToConsumer consumerToConsumer) {
    return consumerToConsumerMapper.insert(consumerToConsumer) > 0;
  }

  @Override
  public List<CustomerByPhone> getCustomerByTel(String phone) {
    return customerProfileMapper.getCustomerByTel(phone);
  }

  @Override
  public void checkPointImport(List<PointImportVO> pointImportVOS) {
    for (int i = 0; i < pointImportVOS.size(); i++) {
      PointImportVO pointImportVO = pointImportVOS.get(i);
      // 行号为0 + 1 且对于用户来说还有第一行标题
      checkPointImportSingle(pointImportVO, i + 2);
    }
  }

  @Override
  public void checkWithdrawImport(
          List<WithdrawImportVO> withdrawImportVOS, Integer month, PlatformType platform) {
    if (CollectionUtils.isEmpty(withdrawImportVOS)) {
      return;
    }
    if (pointCommService.isOrderMonthProcessed(month,platform.getCode())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该月份已经处理，请不要重复提交");
    }
    for (int i = 0; i < withdrawImportVOS.size(); i++) {
      WithdrawImportVO withdrawImportVO = withdrawImportVOS.get(i);
      checkWithDrawImportSingle(withdrawImportVO, platform, i + 2, month);
    }
  }

  @Override
  public boolean canUseFreeLogistic(User user) {
    checkNotNull(user, "用户不能为空");
    Long cpId = user.getCpId();
    String userId = user.getId();
    // 有身份或者在汉薇下过单则不能享受免邮资格
    if (hasIdentity(cpId) || orderMapper.hasSucceedPay(userId)) {
      return false;
    }
    return true;
  }

  /**
   * 处理提现落账
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public int executeWithdrawFeedback(
          List<WithdrawImportVO> withdrawImportVOS, Integer month, PlatformType platform) {
    int effected = 0;
    LOGGER.info("==== 校验落账数据导入 ====");
    checkWithdrawImport(withdrawImportVOS, month, platform);
    LOGGER.info("==== 数据校验通过 开始导入 ====");
    CustomerWithdrawal toUpdateByMonth = buildWithdrawalByMonth(month, DepositStatus.SUCCESS,platform);
    // 先把所有当月的数据都更新为成功
    pointCommService.updateCustomerWithdrawByMonth(toUpdateByMonth);
    if (CollectionUtils.isEmpty(withdrawImportVOS)) {
      // 负一表示没有落账, 返回
      return -1;
    }

    // 根据equals规则去重, remark算是否重复
    Set<WithdrawImportVO> nonRepeatSet = new LinkedHashSet<>(withdrawImportVOS);
    withdrawImportVOS.clear();
    withdrawImportVOS.addAll(nonRepeatSet);

    for (WithdrawImportVO withdrawImportVO : withdrawImportVOS) {
      Long cpId = withdrawImportVO.getCpId();
      Integer source = withdrawImportVO.getSource();
      List<CustomerWithdrawal> withdrawToDepositList =
              pointCommService.listWithDraw(cpId, month, source);
      // pre checked
      assert withdrawToDepositList.size() > 0;
      // 如果有多条则合并amount到一起
      BigDecimal totalAmount = BigDecimal.ZERO;
      for (CustomerWithdrawal w : withdrawToDepositList) {
        totalAmount = totalAmount.add(w.getAmount());
        // 更新状态为已落账返还
        w.setDepositStatus(DepositStatus.ERROR.getCode());
        w.setRemark(Strings.emptyToNull(withdrawImportVO.getErrorMsg()));
        pointCommService.updateCustomerWithdrawById(w);
      }
      try {
        pointCommService.modifyCommission(
                cpId,
                "withdraw_rollback",
                GradeCodeConstrants.GRANT_COMMISSION_CODE,
                com.hds.xquark.dal.type.PlatformType.fromCode(source),
                totalAmount,
                Trancd.ROLLBACK_C,
                TotalAuditType.API);
        effected++;
      } catch (Exception e) {
        LOGGER.info("记录 {} 落账失败", withdrawImportVO, e);
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, e.toString());
      }
    }
    return effected;
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public int savePointImport(List<PointImportVO> pointImportVOS, Integer usedType) {
    if (CollectionUtils.isEmpty(pointImportVOS)) {
      return 0;
    }
    LOGGER.info("==== 校验导入数据 ====");
    // 导入之前先校验格式
    checkPointImport(pointImportVOS);
    LOGGER.info("==== 数据校验通过 开始导入 ====");
    for (PointImportVO pointImportVO : pointImportVOS) {
      Long cpId = pointImportVO.getCpId();
      Integer pSource = pointImportVO.getPointSource();
      Integer cSource = pointImportVO.getCommSource();
      BigDecimal point = pointImportVO.getPoint();
      BigDecimal commission = pointImportVO.getCommission();

      com.hds.xquark.dal.type.PlatformType pType =
              com.hds.xquark.dal.type.PlatformType.fromCode(pSource);
      com.hds.xquark.dal.type.PlatformType cType =
              com.hds.xquark.dal.type.PlatformType.fromCode(cSource);

      // 发放积分、德分
      if (point.signum() > 0) {
        LOGGER.debug("==== 导入德分数据 {} ====", pointImportVO);
        pointCommService.grantPointWithProcedure(cpId, pType, point, Trancd.DEPOSIT_P);
      }
      if (commission.signum() > 0) {
        LOGGER.debug("==== 导入积分数据 {} ====", pointImportVO);
        if (usedType == 1) {
          pointCommService.grantCommissionWithProcedure(cpId, cType, commission, Trancd.DEPOSIT_C);
        } else {
          pointCommService.grantCommissionNoWithdrawal(cpId, cType, commission, Trancd.DEPOSIT_C);
        }
      }
    }
    LOGGER.info("==== 导入完成 ====");
    return pointImportVOS.size();
  }

  @Override
  public void updateTinCodeAndName(Long cpId, String tinCode, String accountName) {
    customerProfileMapper.updateTinCodeAndName(cpId, tinCode, accountName);
  }

  @Override
  public CustomerProfile queryCustomerProfileByCpId(Long cpId) {
    return customerProfileMapper.selectByPrimaryKey(cpId);
  }

  @Override
  public CareerLevelType getCurrLevelType() {
    final User user = (User) getCurrentUser();
    return getLevelType(user.getCpId());
  }

  @Override
  public CareerLevelType getLevelType(Long cpId) {
    CustomerCareerLevel careerLevel = careerLevelService.load(cpId);
    CareerLevelType ret = PW;
    if (careerLevel != null) {
      ret = CareerLevelType.getFinalLevelType(careerLevel.getHdsType(), careerLevel.getViviLifeType());
    }
    if (ret == RC) {
      // 得出是白人，需要进一步判断是不是纯白人
      boolean hasWeek = getUplineWeekIgnoreIdentity(cpId) != null;
      if (!hasWeek) {
        ret = PW;
      }
    }
    return ret;
  }

  @Override
  public List<CustomerProfile> getCustomerProfilesByCpId(Long cpId) {
    return customerProfileMapper.selectCustomerProfilesByCpId(cpId);
  }

  /**
   * 清除用户账号
   * @param cpId
   * @return
   */
  @Override
  public boolean clearCustomer(Long cpId) {
    User user = userMapper.selectByCpId(cpId);
    boolean b = clearCustomerProfileGoHVMALL(user, cpId);
    boolean b1 = clearCustomerProfileGoHDS(user, cpId);
    boolean b2 = clearCustomerProfileGoVVLIFE(user, cpId);
    return b && b1 && b2 == true ? true : false;
  }

  private boolean clearCustomerProfileGoHVMALL(User user, Long cpId) {
    try {
      // 1. 删除前查询当前用户在汉薇数据库是否存在
      boolean b = customerProfileMapper.selectExists(cpId);
      if (b) {
        // 2. 获取当前用户的上线
        CustomerProfile customerProfile = customerProfileMapper.selectByPrimaryKey(cpId);
        String upline = customerProfile.getUpline();
        Long uplineCpId = customerProfile.getUplineCpId();
        // 3. 获取当前用户的所有下线
        List<Long> downCpIds = customerProfileMapper.selectDownCpIdByUplineCpId(cpId);
        if (CollectionUtils.isNotEmpty(downCpIds)) {
          // 4. 把当前用户所有下线的上级改为当前用户的上级
          Integer rows = customerProfileMapper.updateDownCustomerUplineByCpIds(downCpIds, upline, uplineCpId);
          LOGGER.info("HVMALL--> table --> customerProfile, 当前用户 {} 的 {} 个下线变更上级关系", cpId, rows);
        }
        // 5. 更新当前用户信息
        customerProfileMapper.updateCustomerByCpId(cpId);
      } else {
        LOGGER.info("HVMALL--> table --> customerProfile, 当前用户 {} 不存在或已退出", cpId);
      }
      // 6. 更新当前用户的微信记录
      boolean b1 = customerProfileMapper.selectWechatUnionExistsByCpId(cpId);
      if (b1) {
        if (null != user && StringUtils.isNotEmpty(user.getUnionId())) {
          // 是否是多身份
          int count = customerProfileMapper.countWechatUnionByUnionId(user.getUnionId());
          if (count > 1) {
            customerProfileMapper.updateWechatUnionIdByUnionId(user.getUnionId());
          } else {
            customerProfileMapper.updateWechatUnionIdByCpId(cpId);
          }
        }
      } else {
        LOGGER.info("HVMALL--> table --> customerwechatunion, 当前用户 {} 不存在", cpId);
      }
      // 7. 更新当前用户的 user 信息
      boolean b2 = customerProfileMapper.selectUserExistsByCpId(cpId);
      if (b2) {
        customerProfileMapper.updateUserByCpId(cpId);
      } else {
        LOGGER.info("HVMALL--> table --> xquark_user, 当前用户 {} 不存在", cpId);
      }
      LOGGER.info("HVMALL-->当前用户 {} 已清除...", cpId);
      return true;
    } catch (Exception e) {
      LOGGER.error("HVMALL 清除用户账号发生异常, 异常信息 = {}", e.getMessage());
      e.printStackTrace();
      return false;
    }
  }

  private boolean clearCustomerProfileGoHDS(User user, Long cpId) {
    try {
      // 1. 删除前查询当前用户在中台数据库是否存在
      boolean b = customerProfileHDSMapper.selectExists(cpId);
      if (b) {
        // 2. 获取当前用户的上线
        CustomerProfile customerProfile = customerProfileHDSMapper.selectByPrimaryKey(cpId);
        String upline = customerProfile.getUpline();
        Long uplineCpId = customerProfile.getUplineCpId();
        // 3. 获取当前用户的所有下线
        List<Long> downCpIds = customerProfileHDSMapper.selectDownCpIdByUplineCpId(cpId);
        if (CollectionUtils.isNotEmpty(downCpIds)) {
          // 4. 把当前用户所有下线的上级改为当前用户的上级
          Integer rows = customerProfileHDSMapper.updateDownCustomerUplineByCpIds(downCpIds, upline, uplineCpId);
          LOGGER.info("HDS--> table --> customer_profile, 当前用户 {} 的 {} 个下线变更上级关系", cpId, rows);
        }
        // 5. 更新当前用户信息
        customerProfileHDSMapper.updateCustomerByCpId(cpId);
      } else {
        LOGGER.info("HDS--> table --> customer_profile, 当前用户 {} 不存在或已退出", cpId);
      }
      // 6. 更新当前用户的微信记录
      boolean b1 = customerProfileHDSMapper.selectWechatUnionExistsByCpId(cpId);
      boolean b2 = customerProfileHDSMapper.selectWechatExistsByCpId(cpId);
      if (b1) {
        customerProfileHDSMapper.updateWechatUnionIdByCpId(cpId);
      } else {
        LOGGER.info("HDS--> table --> customerwechatunion, 当前用户 {} 不存在", cpId);
      }
      if (b2) {
        if (null != user && StringUtils.isNotEmpty(user.getUnionId())) {
          // 是否是多身份
          int count = customerProfileHDSMapper.countCustomerWechatByUnionId(user.getUnionId());
          if (count > 1) {
            customerProfileHDSMapper.updateCustomerWechatByUnionId(user.getUnionId());
          } else {
            customerProfileHDSMapper.updateCustomerWechatByCpId(cpId);

          }
        }
      } else {
        LOGGER.info("HDS--> table --> customer_wechat, 当前用户 {} 不存在", cpId);
      }
      LOGGER.info("HDS-->当前用户 {} 已清除...", cpId);
      return true;
    } catch (Exception e) {
      LOGGER.error("HDS 清除用户账号发生异常, 异常信息 = {}", e.getMessage());
      e.printStackTrace();
      return false;
    }
  }

  private boolean clearCustomerProfileGoVVLIFE(User user, Long cpId) {
    try {
      // 1. 删除前查询当前用户在薇莱芙数据库是否存在
      boolean b = customerProfileVvlifeMapper.selectExists(cpId);
      if (b) {
        // 2. 获取当前用户的上线
        CustomerProfile customerProfile = customerProfileVvlifeMapper.selectByPrimaryKey(cpId);
        String upline = customerProfile.getUpline();
        Long uplineCpId = customerProfile.getUplineCpId();
        // 3. 获取当前用户的所有下线
        List<Long> downCpIds = customerProfileVvlifeMapper.selectDownCpIdByUplineCpId(cpId);
        if (CollectionUtils.isNotEmpty(downCpIds)) {
          // 4. 把当前用户所有下线的上级改为当前用户的上级
          Integer rows = customerProfileVvlifeMapper.updateDownCustomerUplineByCpIds(downCpIds, upline, uplineCpId);
          LOGGER.info("VVLIFE--> table --> customer_profile, 当前用户 {} 的 {} 个下线变更上级关系", cpId, rows);
        }
        // 5. 更新当前用户信息
        customerProfileVvlifeMapper.updateCustomerByCpId(cpId);
      } else {
        LOGGER.info("VVLIFE--> table --> customer_profile, 当前用户 {} 不存在或已退出", cpId);
      }
      // 6. 更新当前用户的微信记录
      boolean b1 = customerProfileVvlifeMapper.selectWechatUnionExistsByCpId(cpId);
      boolean b2 = customerProfileVvlifeMapper.selectWechatExistsByCpId(cpId);
      if (b1) {
        customerProfileVvlifeMapper.updateWechatUnionIdByCpId(cpId);
      } else {
        LOGGER.info("VVLIFE--> table --> customerwechatunion, 当前用户 {} 不存在", cpId);
      }
      if (b2) {
        if (null != user && StringUtils.isNotEmpty(user.getUnionId())) {
          // 是否是多身份
          int count = customerProfileVvlifeMapper.countCustomerWechatByUnionId(user.getUnionId());
          if (count > 1) {
            customerProfileVvlifeMapper.updateCustomerWechatByUnionId(user.getUnionId());
          } else {
            customerProfileVvlifeMapper.updateCustomerWechatByCpId(cpId);
          }
        }
      } else {
        LOGGER.info("VVLIFE--> table --> customer_wechat, 当前用户 {} 不存在", cpId);
      }
      LOGGER.info("VVLIFE-->当前用户 {} 已清除...", cpId);
      return true;
    } catch (Exception e) {
      LOGGER.error("VVLIFE 清除用户账号发生异常, 异常信息 = {}", e.getMessage());
      e.printStackTrace();
      return false;
    }
  }


  /**
   * 弱关系
   * @param cpId
   * @return
   */
  @Override
  public boolean isWeak(Long cpId) {
    return weakLinkMapper.isExist(cpId);
  }

  /**
   * 次强关系fix
   * @param cpId
   * @return
   */
  @Override
  public boolean isSubStrong(Long cpId) {
    // 被分享人有次强关系, 分享人有身份fix
    Long boundCpId = this.getBoundUpline(cpId);
    // 被分享人有次强关系, 分享人没有身份
    Long boundRC = this.getBoundRC(cpId);
    if(boundCpId != null || boundRC != null){
      return true;
    }
    return false;
  }

  /**
   * 强关系fix
   * @param cpId
   * @return
   */
  @Override
  public boolean isStrong(Long cpId) {
    return this.hasIdentity(cpId);
  }

  private void checkWithDrawImportSingle(
          WithdrawImportVO withdrawImportVO, PlatformType platform, int line, int month) {
    Integer rowMonth, rowSource;
    Long rowCpId;
    try {
      rowMonth = withdrawImportVO.getMonth();
      rowSource = withdrawImportVO.getSource();
      rowCpId = withdrawImportVO.getCpId();
    } catch (Exception e) {
      String msg = String.format("第 %d 备注信息格式不正确", line);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
    }
    if (rowMonth != month) {
      String msg = String.format("第 %d 行月份批次不正确", line);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
    }
    if (rowSource != platform.getCode()) {
      String msg = String.format("第 %d 行 平台与选择平台不匹配", line);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
    }
    if (!isProfileExists(rowCpId)) {
      String msg = String.format("第 %d 行用户 %d 不存在", line, rowCpId);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
    }

    if (!pointCommService.isCpIdWithdrawed(rowCpId, month, rowSource)) {
      String msg =
              String.format(
                      "第 %d 行 用户 %d 未在 %d 月 在 %s 平台申请提现",
                      line, rowCpId, month, PlatformType.fromCode(rowSource).getFullName());
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
    }
  }

  private void checkPointImportSingle(PointImportVO pointImportVO, int line) {
    Integer pSource = pointImportVO.getPointSource();
    Integer cSource = pointImportVO.getCommSource();
    BigDecimal point = pointImportVO.getPoint();
    BigDecimal commission = pointImportVO.getCommission();
    Long cpId = pointImportVO.getCpId();

    com.hds.xquark.dal.type.PlatformType pType =
            com.hds.xquark.dal.type.PlatformType.fromCode(pSource);
    com.hds.xquark.dal.type.PlatformType cType =
            com.hds.xquark.dal.type.PlatformType.fromCode(cSource);

    if (pType == null) {
      String msg = String.format("第 %d 行德分平台数据错误", line);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
    }
    if (cType == null) {
      String msg = String.format("第 %d 行积分平台数据错误", line);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
    }
    if (point == null || point.signum() < 0) {
      String msg = String.format("第 %d 行德分值错误, 德分必须大于等于0", line);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
    }
    if (commission == null || commission.signum() < 0) {
      String msg = String.format("第 %d 行积分值错误, 积分必须大于等于0", line);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
    }
    if (cpId == null) {
      String msg = String.format("第 %d 行cpId格式有误或", line);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
    }
    if (!customerProfileMapper.selectExists(cpId)) {
      String msg = String.format("第 %d 行cpId找不到对应的用户", line);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
    }
  }

  private CustomerWithdrawal buildWithdrawalByMonth(Integer month, DepositStatus status, PlatformType platformType) {
    CustomerWithdrawal ret = new CustomerWithdrawal();
    ret.setDepositStatus(status.getCode());
    ret.setProcessingMonth(month);
    ret.setSource(platformType.getCode());
    return ret;
  }

  private CustomerWithdrawal buildWithdrawalById(Long id, DepositStatus status) {
    CustomerWithdrawal ret = new CustomerWithdrawal();
    ret.setDepositStatus(status.getCode());
    ret.setId(id);
    return ret;
  }


  public static class LightenResult {
    private GlobalErrorCode code;
    private String message;
    private boolean success;

    LightenResult(GlobalErrorCode code, String message) {
      this.code = code;
      this.message = message;
      this.success = false;
    }

    LightenResult(boolean success) {
      this.code = GlobalErrorCode.SUCESS;
      this.success = success;
    }

    public GlobalErrorCode getCode() {
      return code;
    }

    public String getMessage() {
      return message;
    }

    public boolean isSuccess() {
      return success;
    }
  }
}
