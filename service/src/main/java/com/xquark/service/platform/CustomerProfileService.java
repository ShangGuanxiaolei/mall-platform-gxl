package com.xquark.service.platform;

import com.xquark.dal.model.*;
import com.xquark.dal.type.CareerLevelType;
import com.xquark.dal.type.CareerTotalType;
import com.xquark.dal.type.PlatformType;
import com.xquark.dal.vo.*;
import com.xquark.service.platform.impl.CustomerProfileServiceImpl;
import com.xquark.service.pricing.base.PromotionInfo;

import java.util.List;
import java.util.Map;

/**
 * created by
 *
 * @author wangxinhua at 18-6-7 下午1:35
 */
public interface CustomerProfileService {

  boolean isLighten(Long cpId);

  boolean lighten(Long cpId, PlatformType source);

  CustomerProfileServiceImpl.LightenResult lightenWrapped(Long cpId, PlatformType source);

  List<CustomerMessage> listUnReadMessages(
      Long cpId, String msgCode, Integer source, Integer limit);

  List<CustomerMessage> readUnReadMessages(
      Long cpId, String msgCode, Integer source, Integer limit);

  Map<CareerTotalType, String> listCareerTotal(Long cpId, String month);

  boolean updateReadStatusMultiple(List<CustomerMessage> messages);

  /** 判断某个用户是否是三网合一身份 */
  boolean hasIdentity(Long cpId);

  boolean hasIdentityConnection(Long cpId);

  boolean isConnectionRC(Long cpId);

  /** 根据unionId查询对应的用户 */
  List<BaseCustomerVO> listCustomerByUnionId(String unionId);

  List<WxVO> listCusbyUni(String unionId);

  boolean isWechatUnionExists(String unionId);

  String loadAvatarByCpIdAndUnionId(Long cpId, String unionId);

  /** 更新中台表手机号 */
  Boolean updatePhone(Long cpId, String phone);

  /**
   * 获取真实的upline 如果当前用户在customerProfile中有upline, 则是有身份的人, 返回null 如果当前用户已经绑定了关系,, 则忽略分享人,
   * 取已经绑定了的upline
   *
   * @param currCpId 当前用户的cpId
   * @param fromCpId 分享人cpId
   * @return 当前用户的真实upline
   */
  Long getUpline(Long currCpId, Long fromCpId);

  /**
   * 获取真实的upline 如果当前用户在customerProfile中有upline, 则是有身份的人, 返回null 如果当前用户已经绑定了关系,, 则忽略分享人, 拼团团长用
   * 取已经绑定了的upline
   *
   * @param currCpId 当前用户的cpId
   * @param fromCpId 分享人cpId
   * @return 当前用户的真实upline
   */
  Long getUplineX(Long currCpId, Long fromCpId);

  boolean isFirstOrder(String cpId);

  Long getBoundUpline(Long currCpId);

  /** 获取有身份的弱关系上级 */
  Long getUplineWeak(Long currCpId);

  /** 获取白人绑定的弱关系上级 */
  Long getFromConsumerCpId(Long currCpId);

  Long getBoundRC(Long currCpId);

  /**
   * 获取弱关系上级, 忽略身份
   */
  Long getUplineWeekIgnoreIdentity(Long currCpId);

  boolean isProfileExists(Long cpId);

  boolean hasUplineCpId(Long currCpId);

  boolean saveConsumerToCustomer(ConsumerToCustomer consumerToCustomer);

  boolean saveConsumerToConsumer(ConsumerToConsumer consumerToConsumer);

  List<CustomerByPhone> getCustomerByTel(String phone);

  void checkPointImport(List<PointImportVO> pointImportVOS);

  void checkWithdrawImport(
      List<WithdrawImportVO> withdrawImportVOS, Integer month, PlatformType platform);

  boolean canUseFreeLogistic(User user);

  int executeWithdrawFeedback(
      List<WithdrawImportVO> withdrawImportVOS, Integer month, PlatformType platform);

  int savePointImport(List<PointImportVO> pointImportVOS, Integer usedType);

  void updateTinCodeAndName(Long cpId, String tinCode, String accountName);

  CustomerProfile queryCustomerProfileByCpId(Long cpId);

    boolean getIdentify(Long currCpId);

    /** 找到当前人在弱关系表中第一个有身份的上级 如果找不到就返回总部id 2000002 */
  Long getFirstIdentityUpline(Long currCpId);

  Long getUpline(Long currCpId, Long fromCpId, PromotionInfo promotionInfo);

  Long getUplineStrong(Long currCpId, Long fromCpId);

  /**
   * 判断是否是没有下过订单的纯新人
   *
   * @param cpid
   * @return
   */
  boolean isNew(String cpid);

  CareerLevelType getCurrLevelType();

  /**
   * 获取身份类型
   */
   CareerLevelType getLevelType(Long currCpId);

  /**
   * 弱关系
   * @param cpId
   * @return
   */
  boolean isWeak(Long cpId);

  /**
   * 次强关系
   * @param cpId
   * @return
   */
  boolean isSubStrong(Long cpId);

  /**
   * 强关系
   * @param cpId
   * @return
   */
  boolean isStrong(Long cpId);

  boolean isPureWhite(Long currCpId);

  boolean isVip(Long cpId);

  boolean isSp(Long cpId);

  List<CustomerProfile> getCustomerProfilesByCpId(Long cpId);

  boolean clearCustomer(Long cpId);
  /**
   * 代理
   * @param cpId
   * @return
   */
  boolean isProxy(Long cpId);

  /**
   * 会员
   * @param cpId
   * @return
   */
  boolean isMember(Long cpId);
}
