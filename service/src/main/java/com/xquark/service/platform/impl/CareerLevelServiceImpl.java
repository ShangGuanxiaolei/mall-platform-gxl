package com.xquark.service.platform.impl;

import static com.xquark.dal.consts.AuditConstrant.INSERT;
import static com.xquark.dal.consts.AuditConstrant.UPDATE;

import com.xquark.dal.mapper.CustomerCareerLevelAuditMapper;
import com.xquark.dal.mapper.CustomerCareerLevelMapper;
import com.xquark.dal.model.CustomerCareerLevel;
import com.xquark.dal.model.CustomerCareerLevelAudit;
import com.xquark.dal.type.PlatformType;
import com.xquark.helper.Transformer;
import com.xquark.service.platform.CareerLevelService;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * created by
 *
 * @author wangxinhua at 18-6-7 下午1:26
 */
@Service
public class CareerLevelServiceImpl implements CareerLevelService {

  private static final String IDENTITY_RC = "RC";
  private static final String IDENTITY_DS = "DS";
  private static final String IDENTITY_SP = "SP";

  private final CustomerCareerLevelMapper careerLevelMapper;

  private final CustomerCareerLevelAuditMapper customerCareerLevelAuditMapper;

  @Autowired
  public CareerLevelServiceImpl(CustomerCareerLevelMapper careerLevelMapper,
      CustomerCareerLevelAuditMapper customerCareerLevelAuditMapper) {
    this.careerLevelMapper = careerLevelMapper;
    this.customerCareerLevelAuditMapper = customerCareerLevelAuditMapper;
  }

  @Override
  @Transactional
  public boolean save(CustomerCareerLevel careerLevel, PlatformType source) {
    boolean ret = careerLevelMapper.insert(careerLevel) > 0;
    CustomerCareerLevelAudit audit = copyAudit(careerLevel, INSERT, source);
    customerCareerLevelAuditMapper.insert(audit);
    return ret;
  }

  @Override
  @Transactional
  public boolean update(CustomerCareerLevel careerLevel, PlatformType source) {
    boolean ret = careerLevelMapper.updateByPrimaryKeySelective(careerLevel) > 0;
    CustomerCareerLevelAudit audit = copyAudit(careerLevel, UPDATE, source);
    audit.setAuditDate(new Date());
    customerCareerLevelAuditMapper.insert(audit);
    return ret;
  }

  @Override
  public CustomerCareerLevel load(Long cpId) {
    return careerLevelMapper.selectByPrimaryKey(cpId);
  }

  @Override
  public boolean isVip(Long cpId) {
    CustomerCareerLevel level = this.load(cpId);

    if (level == null){
      return  false;
    }
    if (StringUtils.isNotBlank(level.getHdsType()) || StringUtils.isNotBlank(level.getViviLifeType())){
      if (IDENTITY_DS.equals(level.getHdsType()) && IDENTITY_RC.equals(level.getViviLifeType())){
        return  true;
      }
      if(StringUtils.isBlank(level.getHdsType()) && IDENTITY_DS.equals(level.getViviLifeType())){
        return true;
      }
      if(IDENTITY_DS.equals(level.getHdsType()) && IDENTITY_DS.equals(level.getViviLifeType())){
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean isShopkeeper(Long cpId) {
    CustomerCareerLevel level = this.load(cpId);

    if (level == null){
      return  false;
    }

    if (StringUtils.isNotBlank(level.getHdsType()) || StringUtils.isNotBlank(level.getViviLifeType())){
      if (IDENTITY_SP.equals(level.getHdsType()) || IDENTITY_SP.equals(level.getViviLifeType())) {
        return true;
      }
    }
    return false;
  }

  private CustomerCareerLevelAudit copyAudit(CustomerCareerLevel careerLevel,
                                             int auditType, PlatformType source) {
    CustomerCareerLevelAudit careerLevelAudit = Transformer
        .fromBean(careerLevel, CustomerCareerLevelAudit.class);
    careerLevelAudit.setAuditType(auditType);
    careerLevelAudit.setAuditUser(source);
    return careerLevelAudit;
  }
}
