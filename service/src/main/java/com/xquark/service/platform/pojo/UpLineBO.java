package com.xquark.service.platform.pojo;

import java.util.List;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/5/28
 * Time:17:33
 * Des:
 */
public class UpLineBO {
    private Long cpId;
    private Long upLineCpId;
    private List<Long> cpIdList; // 标记分享人及其上级列表
    private int currentDeep; // 防止出现意外情况

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }

    public Long getUpLineCpId() {
        return upLineCpId;
    }

    public void setUpLineCpId(Long upLineCpId) {
        this.upLineCpId = upLineCpId;
    }

    public List<Long> getCpIdList() {
        return cpIdList;
    }

    public void setCpIdList(List<Long> cpIdList) {
        this.cpIdList = cpIdList;
    }

    public int getCurrentDeep() {
        return currentDeep;
    }

    public void setCurrentDeep(int currentDeep) {
        this.currentDeep = currentDeep;
    }

    @Override
    public String toString() {
        return "UpLineBO{" +
                "cpId=" + cpId +
                ", upLineCpId=" + upLineCpId +
                ", cpIdList=" + cpIdList +
                ", currentDeep=" + currentDeep +
                '}';
    }
}