package com.xquark.service.platform;

import com.xquark.dal.model.CustomerCareerLevel;
import com.xquark.dal.type.PlatformType;

/**
 * created by
 *
 * @author wangxinhua at 18-6-7 下午1:26
 */
public interface CareerLevelService {

  boolean save(CustomerCareerLevel careerLevel, PlatformType source);

  boolean update(CustomerCareerLevel careerLevel, PlatformType source);

  CustomerCareerLevel load(Long cpId);

  /**
   * 判断是否是vip
   * @param cpId
   * @return true:是vip
   */
  boolean isVip(Long cpId);

  /**
   * 功能描述: 判断是否是店主(包含代理)
   * @author Kwonghom
   * @date 2019/1/3
   * @param [cpId]
   * @return boolean
   */
  boolean isShopkeeper(Long cpId);
}
