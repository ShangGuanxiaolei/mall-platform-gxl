package com.xquark.service.customer;


import com.xquark.dal.model.CustomerQuestion;
import com.xquark.dal.model.QuestionTypeVo;

import java.util.List;

public interface CustomerQuestionService {

    List<QuestionTypeVo> selectQuestionAll();

    boolean updateHelpful(String help,int id);

    /**
     * 根据问题详情id查询问题内容
     * @param id
     * @return
     */
    String selectContentById(String id);
}
