package com.xquark.service.customer.impl;



import com.xquark.dal.mapper.CustomerQuestionMapper;
import com.xquark.dal.model.CustomerQuestion;
import com.xquark.dal.model.QuestionTypeVo;
import com.xquark.service.customer.CustomerQuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service("customerQuestionService")
public class CustomerQuestionServiceImpl implements CustomerQuestionService {

        private static final Logger logger = LoggerFactory.getLogger(CustomerQuestionServiceImpl.class);


    @Autowired
    private CustomerQuestionMapper customerQuestionMapper;

    /**
     * 查询全部信息
     * @return
     */
    @Override
    public List<QuestionTypeVo> selectQuestionAll() {

        List<QuestionTypeVo> questionTypeList=customerQuestionMapper.selectQueType();
        for (int i = 0; i < questionTypeList.size(); i++) {
            String typeId=questionTypeList.get(i).getId().toString();
            List<CustomerQuestion> list=customerQuestionMapper.selectQuestionAll(typeId);
            questionTypeList.get(i).setCustomerQuestionList(list);
        }
        return questionTypeList;
    }

    @Override
    public boolean updateHelpful(String help, int id) {
        boolean a = false;
        if(help.equals("helpful")) {
            int updateHelpful = customerQuestionMapper.updateHelpful(id);
            if (updateHelpful < 1) {
                return a;
            }
            a = true;
            return a;
        }else if (help.equals("unhelpful")){
            int updateHelpful = customerQuestionMapper.updateUnHelpful(id);
            if (updateHelpful < 1) {
                return a;
            }
            a = true;
            return a;
        }
        return a;
    }

    @Override
    public String selectContentById(String id) {
        String content="";
        String result=customerQuestionMapper.selectContentById(id);
        if(""!=result && null!=result){
            content=result;
        }
        return content;
    }


}
