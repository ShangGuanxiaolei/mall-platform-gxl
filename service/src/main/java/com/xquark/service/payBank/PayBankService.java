package com.xquark.service.payBank;

import java.util.List;

import com.xquark.dal.model.PayBank;
import com.xquark.dal.model.PayBankWay;
import com.xquark.dal.type.PaymentMode;

public interface PayBankService {

  List<PayBankWay> queryHotPayBanksCreditCard();

  List<PayBankWay> queryAllPayBanksCreditCard();

  List<PayBankWay> queryHotPayBanksDebitCard();

  List<PayBankWay> queryAllPayBanksDebitCard();

  /**
   * 将不支持的银行设为无效
   */
  int updateBankStatusFalse(List<String> bankCodeList, PaymentMode mode);

  /**
   * 第三方支持的系统未支持的银行列表
   */
  List<String> noSupportBank(List<String> bankCodeInUse, PaymentMode mode);

  List<PayBank> obtainCommonlyBankList();
}
