package com.xquark.service.payBank.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.PayBankMapper;
import com.xquark.dal.model.PayBank;
import com.xquark.dal.model.PayBankWay;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.payBank.PayBankService;

@Service("payBankService")
public class PayBankServiceImpl implements PayBankService {

  @Autowired
  private PayBankMapper payBankMapper;

  @Override
  public List<PayBankWay> queryHotPayBanksCreditCard() {
    //热门银行列表
    return payBankMapper.queryHotPayBanksCreditCard();
  }

  @Override
  public List<PayBankWay> queryAllPayBanksCreditCard() {
    //所有银行列表
    return payBankMapper.queryAllPayBanksCreditCard();
  }

  @Override
  public List<PayBankWay> queryHotPayBanksDebitCard() {
    //热门银行列表
    return payBankMapper.queryHotPayBanksDebitCard();
  }

  @Override
  public List<PayBankWay> queryAllPayBanksDebitCard() {
    //所有银行列表
    return payBankMapper.queryAllPayBanksDebitCard();
  }

  @Override
  public int updateBankStatusFalse(List<String> bankCodeList, PaymentMode mode) {
    return payBankMapper.updateBankStatusFalse(bankCodeList, mode);
  }

  @Override
  public List<String> noSupportBank(List<String> bankCodeInUse, PaymentMode mode) {
    List<String> supportBanks = payBankMapper.supportBank(mode);
    bankCodeInUse.removeAll(supportBanks);
    return bankCodeInUse;
  }

  @Override
  public List<PayBank> obtainCommonlyBankList() {
    return payBankMapper.obtainCommonlyBankList();
  }
}
