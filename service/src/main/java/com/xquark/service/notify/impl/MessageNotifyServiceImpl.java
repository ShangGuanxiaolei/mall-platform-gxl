package com.xquark.service.notify.impl;

import com.xquark.dal.mapper.MessageNotifyMapper;
import com.xquark.dal.model.MessageNotify;
import com.xquark.service.notify.MessageNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageNotifyServiceImpl implements MessageNotifyService {
    @Autowired
    private MessageNotifyMapper messageNotifyMapper;
    @Override
    public MessageNotify selectMessageNotifyByTime() {
        return messageNotifyMapper.selectMessageNotifyByTime();
    }
}
