package com.xquark.service.notify;

import com.xquark.dal.model.MessageNotify;

public interface MessageNotifyService {
    MessageNotify selectMessageNotifyByTime();
}
