package com.xquark.service.common;

import com.xquark.dal.IUser;
import com.xquark.dal.model.IExample;
import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.service.BaseService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

public abstract class BaseServiceImpl implements BaseService {

  protected Logger log = LoggerFactory.getLogger(getClass());

  /**
   * 获取当前用户信息 如果是未登录的匿名用户，系统根据匿名用户唯一码自动创建一个用户 具体逻辑查看：UniqueNoFilter
   * @return 当前用户
   */
  @SuppressWarnings("unchecked")
  public User getCurrentUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof User) {
        return (User) principal;
      } else if (principal instanceof Merchant) {
        WebApplicationContext webApplicationContext = ContextLoader
            .getCurrentWebApplicationContext();
        UserService userService = (UserService) webApplicationContext.getBean("userService");
        ShopService shopService = (ShopService) webApplicationContext.getBean("shopService");
        Shop shop = shopService.load(((Merchant) principal).getShopId());
        return userService.load(shop.getOwnerId());
      }
      if (!auth.getClass().getSimpleName().contains("Anonymous")) {
        log.error("Unknown authentication encountered, ignore it. " + auth);
      }
    }
    throw new BizException(GlobalErrorCode.UNAUTHORIZED, "need login first.");
  }

  @SuppressWarnings("unchecked")
  public <T extends IUser> T getCurrentIUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (!auth.getClass().getSimpleName().contains("Anonymous")) {
        log.error("Unknown authentication encountered, ignore it. " + auth);
      }
      return (T) principal;
    }
    throw new BizException(GlobalErrorCode.UNAUTHORIZED, "need login first.");
  }

  protected <T extends IExample> T pagingExample(Class<T> clazz, String order,
      Sort.Direction direction, Pageable pageable) {
    final T example;
    try {
      example = clazz.newInstance();
    } catch (Exception e) {
      log.error(e.getMessage());
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "服务器错误");
    }
    if (StringUtils.isNoneBlank(order)) {
      String orderByClause = "`" + order + "` ";
      if (direction != null) {
        orderByClause += direction.name();
      }
      example.setOrderByClause(orderByClause);
    }
    if (pageable != null) {
      example.setOffset(pageable.getOffset());
      example.setLimit(pageable.getPageSize());
    }
    return example;
  }
}