package com.xquark.service.push.umeng.push.android;

import org.json.JSONObject;

import com.xquark.service.push.umeng.push.AndroidNotification;

public class AndroidGroupcast extends AndroidNotification {

  public AndroidGroupcast(String appkey, String appMasterSecret) throws Exception {
    setAppMasterSecret(appMasterSecret);
    setPredefinedKeyValue("appkey", appkey);
    this.setPredefinedKeyValue("type", "groupcast");
  }

  public void setFilter(JSONObject filter) throws Exception {
    setPredefinedKeyValue("filter", filter);
  }
}
