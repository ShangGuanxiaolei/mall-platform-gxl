package com.xquark.service.push.umeng.push.ios;

import org.json.JSONObject;

import com.xquark.service.push.umeng.push.IOSNotification;

public class IOSGroupcast extends IOSNotification {

  public IOSGroupcast(String appkey, String appMasterSecret) throws Exception {
    setAppMasterSecret(appMasterSecret);
    setPredefinedKeyValue("appkey", appkey);
    this.setPredefinedKeyValue("type", "groupcast");
  }

  public void setFilter(JSONObject filter) throws Exception {
    setPredefinedKeyValue("filter", filter);
  }
}
