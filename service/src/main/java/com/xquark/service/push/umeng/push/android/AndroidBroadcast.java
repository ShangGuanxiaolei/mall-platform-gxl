package com.xquark.service.push.umeng.push.android;

import com.xquark.service.push.umeng.push.AndroidNotification;

public class AndroidBroadcast extends AndroidNotification {

  public AndroidBroadcast(String appkey, String appMasterSecret) throws Exception {
    setAppMasterSecret(appMasterSecret);
    setPredefinedKeyValue("appkey", appkey);
    this.setPredefinedKeyValue("type", "broadcast");
  }
}
