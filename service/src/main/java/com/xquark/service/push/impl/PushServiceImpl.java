package com.xquark.service.push.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.xquark.dal.model.PushMessage;
import com.xquark.dal.model.User;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.push.BaiduPushMessage;
import com.xquark.service.push.PushService;
import com.xquark.service.push.umeng.push.AndroidNotification;
import com.xquark.service.push.umeng.push.PushClient;
import com.xquark.service.push.umeng.push.android.AndroidBroadcast;
import com.xquark.service.push.umeng.push.android.AndroidUnicast;
import com.xquark.service.push.umeng.push.ios.IOSBroadcast;
import com.xquark.service.push.umeng.push.ios.IOSUnicast;
import com.xquark.service.user.UserService;
import com.xquark.thirds.baidu.yun.channel.auth.ChannelKeyPair;
import com.xquark.thirds.baidu.yun.channel.client.BaiduChannelClient;
import com.xquark.thirds.baidu.yun.channel.exception.ChannelClientException;
import com.xquark.thirds.baidu.yun.channel.exception.ChannelServerException;
import com.xquark.thirds.baidu.yun.channel.model.PushBroadcastMessageRequest;
import com.xquark.thirds.baidu.yun.channel.model.PushBroadcastMessageResponse;
import com.xquark.thirds.baidu.yun.channel.model.PushTagMessageRequest;
import com.xquark.thirds.baidu.yun.channel.model.PushTagMessageResponse;
import com.xquark.thirds.baidu.yun.channel.model.PushUnicastMessageRequest;
import com.xquark.thirds.baidu.yun.channel.model.PushUnicastMessageResponse;
import com.xquark.thirds.baidu.yun.core.log.YunLogEvent;
import com.xquark.thirds.baidu.yun.core.log.YunLogHandler;
import com.xquark.thirds.umpay.api.util.StringUtil;
import com.xquark.utils.CommonUtils;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

@Service
public class PushServiceImpl extends BaseServiceImpl implements PushService {

  private Logger log = LoggerFactory.getLogger(getClass());
//	@Value("${sms.gateway}")
//	private String smsGatway;

  private final static String ALI_SMS_DEFAULT_TEMPLATE = "SMS_138065692";

  @Value("${sms.gatewayEngine}")
  private String smsEngineGatway;

  @Value("${sms.gateway.off}")
  private boolean smsGatewayOff;

  @Value("${sms.response.success.code}")
  private String responseCode;

  @Value("${baidu.android.apiKey}")
  private String baiduAndroidApiKey;

  @Value("${baidu.android.secretKey}")
  private String baiduAndroidSecretKey;

  @Value("${baidu.ios.apiKey}")
  private String baiduIosApiKey;

  @Value("${baidu.ios.secretKey}")
  private String baiduIosSecretKey;

  @Value("${baidu.ios.deploy}")
  private String baiduIosDeploy;

  // 1 kkkd; 2 kuchuan; 3 xiangqu.
  @Value("${mandao.sms.kkkd.appid}")
  private String kkkdAppid;

  @Value("${mandao.sms.xiangqu.appid}")
  private String xiangquAppid;

  @Autowired
  private RestOperations restTemplate;

  @Value("${umeng.androidAppkey}")
  private String androidAppkey;

  @Value("${umeng.androidAppMasterSecret}")
  private String androidAppMasterSecret;

  @Value("${umeng.iosAppkey}")
  private String iosAppkey;

  @Value("${umeng.iosAppMasterSecret}")
  private String iosAppMasterSecret;

  @Value("${ali.sms.accessKeyId}")
  private String aliAccessKeyId;

  @Value("${ali.sms.accessKeySecret}")
  private String aliAccessKeySecret;

  @Autowired
  private UserService userService;

  private PushClient client = new PushClient();

//	public boolean sendSms(String mobile, String msg) {
//		return sendSms(mobile, msg, UserPartnerType.KKKD);
//	}

//	public boolean sendSms(String mobile, String msg, UserPartnerType userType) {
//		String appid = kkkdAppid;
//		if(userType == UserPartnerType.XIANGQU ){
//			appid = xiangquAppid;
//		}
//		
//		if(smsGatewayOff) {
//			log.info("sending MOCK sms to " + mobile + ", content:" + msg);
//			return true;
//		}
//		try {
//			Map<String, ?> m = CommonUtils.asMap("mobile", mobile, "content", msg, "appid", appid, "data", "");
//			log.info("sending sms to " + mobile + " content:" + msg + " appid:" + appid);
//			String resp = restTemplate.getForObject(smsEngineGatway, String.class, m);
//			return checkGatewayResponse(resp);
//		} catch (Exception e) {
//			log.error("fail send sms", e);
//			return false;
//		}
//	}

  @Override
  public boolean sendSmsEngine(String mobile, String content) {
//    return sendSmsEngine(mobile, content, UserPartnerType.KKKD, "");
    return sendSmsEngine(mobile, content, UserPartnerType.ALISMS, "", "");
  }

  @Override
  public boolean sendSmsEngine(String mobile, String content, UserPartnerType userType,
      String paramsData, String templateCode) {
    // set sms app id
    String appid = kkkdAppid;
    if (userType == UserPartnerType.XIANGQU) {
      appid = xiangquAppid;
    }

    // check sms swith status, when sms is setting to OFF, no sms will be sent out
    if (smsGatewayOff) {
      log.info("sending MOCK sms to " + mobile + ", SmsMessageType:" + content);
      return true;
    }

    try {
      if(userType == UserPartnerType.ALISMS){
        aliSmsSend(mobile, content, templateCode);
      }else {
        Map<String, ?> m = CommonUtils
                .asMap("mobile", mobile, "content", content, "appid", appid, "data", paramsData);
        log.info("sending sms to " + mobile + " content:" + content + " appid:" + appid);

        String url = "http://mall.qinyuan.cn/sms/engine/send?" + mapToParam(m);
        HttpInvokeResult result = PoolingHttpClients.get(url);
      }
      return true;
    } catch (Exception e) {
      log.error("fail send sms", e);
      return false;
    }
  }

  private void aliSmsSend(String mobile, String content, String templateCode) {
    //设置超时时间-可自行调整
    System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
    System.setProperty("sun.net.client.defaultReadTimeout", "10000");

    //初始化ascClient需要的几个参数
    final String product = "Dysmsapi";//短信API产品名称（短信产品名固定，无需修改）
    final String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）

    //替换成你的AK
    final String accessKeyId = aliAccessKeyId;//你的accessKeyId,参考本文档步骤2
    final String accessKeySecret = aliAccessKeySecret;//你的accessKeySecret，参考本文档步骤2

    //初始化ascClient,暂时不支持多region（请勿修改）
    IClientProfile profile = DefaultProfile.getProfile("cn-shanghai", accessKeyId,
            accessKeySecret);
    try {
      DefaultProfile.addEndpoint("cn-shanghai", "cn-shanghai", product, domain);
    } catch (ClientException e) {
      e.printStackTrace();
    }
    IAcsClient acsClient = new DefaultAcsClient(profile);
    //组装请求对象
    SendSmsRequest request = new SendSmsRequest();
    //使用post提交
    request.setMethod(MethodType.POST);

    //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及
    // 时性稍有延迟,验证码类型的短信推荐使用单条调用的方式；发送国际/港澳台消息时，接收号码格式为00+国际区号+号码，
    // 如“0085200000000”
    request.setPhoneNumbers(mobile);
    //必填:短信签名-可在短信控制台中找到
    request.setSignName("汉薇商城");
    //必填:短信模板-可在短信控制台中找到
    if (StringUtils.isBlank(templateCode)) {
      templateCode = ALI_SMS_DEFAULT_TEMPLATE;
    }
    request.setTemplateCode(templateCode);

    //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
    //友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示
    // 成\\r\\n,否则会导致JSON在服务端解析失败
    request.setTemplateParam("{ \"code\":\"" + content + "\"}");

    //请求失败这里会抛ClientException异常
    SendSmsResponse sendSmsResponse = null;
    try {
      sendSmsResponse = acsClient.getAcsResponse(request);
    } catch (ClientException e) {
      e.printStackTrace();
    }
    if(sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
      //请求成功
      System.out.println("＝＝＝＝＝＝＝＝＝＝＝＝＝success");
    }
  }

  private String mapToParam(Map<String, ?> params) {
    checkNotNull(params);
    StringBuilder builder = new StringBuilder();
    for (Entry<String, ?> entry : params.entrySet()) {
      try {
        builder.append(entry.getKey()).append("=")
            .append(URLEncoder.encode(entry.getValue().toString(), "UTF-8")).append("&");
      } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
      }
    }
    return builder.toString();
  }

  private boolean checkGatewayResponse(String resp) {
    return resp.contains("\"data\":true");
  }

  @Override
  public boolean sendUnicastMessageByBaidu(BaiduPushMessage message) {
    boolean retVal = false;
    if (message.valid()) {
      // 1. 设置developer平台的ApiKey/SecretKey
      ChannelKeyPair pair;
      if (message.getBaiduDeviceType() == 4) {
        if (log.isDebugEnabled()) {
          log.info("push到ios客户端,apiKey：" + baiduIosApiKey + ",secretKey：" + baiduIosSecretKey);
        }
        pair = new ChannelKeyPair(baiduIosApiKey, baiduIosSecretKey);
      } else {
        if (log.isDebugEnabled()) {
          log.info("push到android客户端,apiKey：" + baiduAndroidApiKey + ",secretKey："
              + baiduAndroidSecretKey);
        }
        pair = new ChannelKeyPair(baiduAndroidApiKey, baiduAndroidSecretKey);
      }
      // 2. 创建BaiduChannelClient对象实例
      BaiduChannelClient channelClient = new BaiduChannelClient(pair);
      // 3. 若要了解交互细节，请注册YunLogHandler类
      if (log.isDebugEnabled()) {
        channelClient.setChannelLogHandler(new YunLogHandler() {
          @Override
          public void onHandle(YunLogEvent event) {
            log.info(event.getMessage());
          }
        });
      }
      try {
        // 4. 创建请求类对象
        PushUnicastMessageRequest request = new PushUnicastMessageRequest();
        request.setDeviceType(
            message.getBaiduDeviceType()); // device_type => 1: web 2: pc 3:android 4:ios 5:wp
        // 如果是ios的部署状态为生产环境
        if (message.getBaiduDeviceType() == 4) {
          if (baiduIosDeploy.equals("Developer")) {
            if (log.isDebugEnabled()) {
              log.info("push到ios客户端,测试环境");
            }
            request.setDeployStatus(1); // DeployStatus => 1: Developer 2: Production
          } else if (baiduIosDeploy.equals("Production")) {
            if (log.isDebugEnabled()) {
              log.info("push到ios客户端,生产环境");
            }
            request.setDeployStatus(2); // DeployStatus => 1: Developer 2: Production
          } else {
            if (log.isDebugEnabled()) {
              log.info("push到ios客户端,默认使用测试环境");
            }
            request.setDeployStatus(1); // DeployStatus => 1: Developer 2: Production
          }
        }
        request.setMessageType(message.getBaiduMessageType());
        request.setChannelId(message.getBaiduChannelId());
        request.setUserId(message.getBaiduUserId());
        request.setMessage(BaiduPushMessage.toMessage(message));

        // 5. 调用pushMessage接口
        PushUnicastMessageResponse response = channelClient.pushUnicastMessage(request);
        retVal = response.getSuccessAmount() > 0;

        // 6. 认证推送成功
        if (log.isDebugEnabled()) {
          log.info("push amount : " + response.getSuccessAmount());
        }
      } catch (ChannelClientException e) {
        // 处理客户端错误异常
        log.error("client：fail push message to baidu", e);
      } catch (ChannelServerException e) {
        // 处理服务端错误异常
        log.error(String
            .format("server：request_id: %d, error_code: %d, error_message: %s", e.getRequestId(),
                e.getErrorCode(), e.getErrorMsg()), e);
      }
    }
    return retVal;
  }

  @Override
  public boolean sendTagMessageByBaidu(BaiduPushMessage message) {
    boolean retVal = false;
    if (message.valid()) {
      // 1. 设置developer平台的ApiKey/SecretKey
      ChannelKeyPair pair;
      if (message.getBaiduDeviceType() == 4) {
        if (log.isDebugEnabled()) {
          log.info("push到ios客户端,apiKey：" + baiduIosApiKey + ",secretKey：" + baiduIosSecretKey);
        }
        pair = new ChannelKeyPair(baiduIosApiKey, baiduIosSecretKey);
      } else {
        if (log.isDebugEnabled()) {
          log.info("push到android客户端,apiKey：" + baiduAndroidApiKey + ",secretKey："
              + baiduAndroidSecretKey);
        }
        pair = new ChannelKeyPair(baiduAndroidApiKey, baiduAndroidSecretKey);
      }
      // 2. 创建BaiduChannelClient对象实例
      BaiduChannelClient channelClient = new BaiduChannelClient(pair);
      // 3. 若要了解交互细节，请注册YunLogHandler类
      if (log.isDebugEnabled()) {
        channelClient.setChannelLogHandler(new YunLogHandler() {
          @Override
          public void onHandle(YunLogEvent event) {
            log.info(event.getMessage());
          }
        });
      }
      try {
        // 4. 创建请求类对象
        PushTagMessageRequest request = new PushTagMessageRequest();
        request.setDeviceType(
            message.getBaiduDeviceType()); // device_type => 1: web 2: pc 3:android 4:ios 5:wp
        // 如果是ios的部署状态为生产环境
        if (message.getBaiduDeviceType() == 4) {
          if (baiduIosDeploy.equals("Developer")) {
            if (log.isDebugEnabled()) {
              log.info("push到ios客户端,测试环境");
            }
            request.setDeployStatus(1); // DeployStatus => 1: Developer 2: Production
          } else if (baiduIosDeploy.equals("Production")) {
            if (log.isDebugEnabled()) {
              log.info("push到ios客户端,生产环境");
            }
            request.setDeployStatus(2); // DeployStatus => 1: Developer 2: Production
          } else {
            if (log.isDebugEnabled()) {
              log.info("push到ios客户端,默认使用测试环境");
            }
            request.setDeployStatus(1); // DeployStatus => 1: Developer 2: Production
          }
        }
        request.setMessageType(message.getBaiduMessageType());
        request.setTagName(message.getBaiduTagName());

        request.setMessage(BaiduPushMessage.toMessage(message));

        // 5. 调用pushMessage接口
        PushTagMessageResponse response = channelClient.pushTagMessage(request);
        retVal = response.getSuccessAmount() > 0;

        // 6. 认证推送成功
        if (log.isDebugEnabled()) {
          log.info("push amount : " + response.getSuccessAmount());
        }
      } catch (ChannelClientException e) {
        // 处理客户端错误异常
        log.error("client：fail push message to baidu", e);
      } catch (ChannelServerException e) {
        // 处理服务端错误异常
        log.info(String.format("server：request_id: %d, error_code: %d, error_message: %s",
            e.getRequestId(), e.getErrorCode(), e.getErrorMsg()));
        /*
         * group not found, 找不到用户往往由于上层对同一id在android和ios都进行了推送,大部分情况必有一条会失败,这里过滤掉
         */
        if (e.getErrorCode() == 30611) {
          retVal = true;
        }

      }
    }
    return retVal;
  }

  @Override
  public boolean sendBroadcastMessageByBaidu(BaiduPushMessage message) {
    boolean retVal = false;
    if (message.valid()) {
      // 1. 设置developer平台的ApiKey/SecretKey
      ChannelKeyPair pair;
      if (message.getBaiduDeviceType() == 4) {
        if (log.isDebugEnabled()) {
          log.info("push到ios客户端,apiKey：" + baiduIosApiKey + ",secretKey：" + baiduIosSecretKey);
        }
        pair = new ChannelKeyPair(baiduIosApiKey, baiduIosSecretKey);
      } else {
        if (log.isDebugEnabled()) {
          log.info("push到android客户端,apiKey：" + baiduAndroidApiKey + ",secretKey："
              + baiduAndroidSecretKey);
        }
        pair = new ChannelKeyPair(baiduAndroidApiKey, baiduAndroidSecretKey);
      }
      // 2. 创建BaiduChannelClient对象实例
      BaiduChannelClient channelClient = new BaiduChannelClient(pair);
      // 3. 若要了解交互细节，请注册YunLogHandler类
      if (log.isDebugEnabled()) {
        channelClient.setChannelLogHandler(new YunLogHandler() {
          @Override
          public void onHandle(YunLogEvent event) {
            log.info(event.getMessage());
          }
        });
      }
      try {
        // 4. 创建请求类对象
        PushBroadcastMessageRequest request = new PushBroadcastMessageRequest();
        request.setDeviceType(message.getBaiduDeviceType());
        // 如果是ios的部署状态为生产环境
        if (message.getBaiduDeviceType() == 4) {
          if (baiduIosDeploy.equals("Developer")) {
            if (log.isDebugEnabled()) {
              log.info("push到ios客户端,测试环境");
            }
            request.setDeployStatus(1); // DeployStatus => 1: Developer 2: Production
          } else if (baiduIosDeploy.equals("Production")) {
            if (log.isDebugEnabled()) {
              log.info("push到ios客户端,生产环境");
            }
            request.setDeployStatus(2); // DeployStatus => 1: Developer 2: Production
          } else {
            if (log.isDebugEnabled()) {
              log.info("push到ios客户端,默认使用测试环境");
            }
            request.setDeployStatus(1); // DeployStatus => 1: Developer 2: Production
          }
        }
        request.setMessageType(message.getBaiduMessageType());
        request.setMessage(BaiduPushMessage.toMessage(message));

        // 5. 调用pushMessage接口
        PushBroadcastMessageResponse response = channelClient.pushBroadcastMessage(request);
        retVal = response.getSuccessAmount() > 0;

        // 6. 认证推送成功
        if (log.isDebugEnabled()) {
          log.info("push amount : " + response.getSuccessAmount());
        }
      } catch (ChannelClientException e) {
        // 处理客户端错误异常
        log.error("client：fail push message to baidu", e);
      } catch (ChannelServerException e) {
        // 处理服务端错误异常
        log.error(String.format("server：request_id: %d, error_code: %d, error_message: %s",
            e.getRequestId(), e.getErrorCode(), e.getErrorMsg()), e);
        /*
         * group not found, 找不到用户往往由于上层对同一id在android和ios都进行了推送,大部分情况必有一条会失败,这里过滤掉
         */
        if (e.getErrorCode() == 30611) {
          retVal = true;
        }
      }
    }
    return retVal;
  }

  /**
   * Android广播消息提醒
   */
  private boolean sendAndroidBroadcast(String title, String content) throws Exception {
    AndroidBroadcast broadcast = new AndroidBroadcast(androidAppkey, androidAppMasterSecret);
    broadcast.setTicker(title);
    broadcast.setTitle(title);
    broadcast.setText(content);
    broadcast.goAppAfterOpen();
    broadcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
    // TODO Set 'production_mode' to 'false' if it's a test device.
    // For how to register a test device, please see the developer doc.
    broadcast.setProductionMode();
    return client.send(broadcast);
  }

  /**
   * Android推送消息提醒
   */
  private boolean sendAndroidUnicast(String deviceToken, String title, String content)
      throws Exception {
    AndroidUnicast unicast = new AndroidUnicast(androidAppkey, androidAppMasterSecret);
    // TODO Set your device token
    unicast.setDeviceToken(deviceToken);
    unicast.setTicker(title);
    unicast.setTitle(title);
    unicast.setText(content);
    unicast.goAppAfterOpen();
    unicast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
    // TODO Set 'production_mode' to 'false' if it's a test device.
    // For how to register a test device, please see the developer doc.
    unicast.setProductionMode();
    return client.send(unicast);
  }

  /**
   * Android静默推送，用于提醒Android客户端刷新首页数据
   */
  private boolean sendAndroidCustom(String deviceToken, String content) throws Exception {
    AndroidUnicast unicast = new AndroidUnicast(androidAppkey, androidAppMasterSecret);
    // TODO Set your device token
    unicast.setDeviceToken(deviceToken);
    unicast.goCustomAfterOpen(content);
    unicast.setDisplayType(AndroidNotification.DisplayType.MESSAGE);
    unicast.setCustomField(content);
    // TODO Set 'production_mode' to 'false' if it's a test device.
    // For how to register a test device, please see the developer doc.
    unicast.setProductionMode();
    return client.send(unicast);
  }

  /**
   * ios静默推送，用于提醒ios客户端刷新首页数据
   */
  private boolean sendIOSCustom(String deviceToken, String content) throws Exception {
    IOSUnicast unicast = new IOSUnicast(iosAppkey, iosAppMasterSecret);
    // TODO Set your device token
    unicast.setDeviceToken(deviceToken);
    unicast.setContentAvailable(1);
    unicast.setCustomizedField("refresh", content);
    // TODO set 'production_mode' to 'true' if your app is under production mode
    unicast.setProductionMode();
    return client.send(unicast);
  }

  /**
   * ios广播消息提醒
   */
  private boolean sendIOSBroadcast(String content) throws Exception {
    IOSBroadcast broadcast = new IOSBroadcast(iosAppkey, iosAppMasterSecret);

    broadcast.setAlert(content);
    broadcast.setBadge(0);
    broadcast.setSound("default");
    // TODO set 'production_mode' to 'true' if your app is under production mode
    broadcast.setProductionMode();
    return client.send(broadcast);
  }

  /**
   * ios推送消息提醒
   */
  private boolean sendIOSUnicast(String deviceToken, String content) throws Exception {
    log.info("start sendIOSUnicast");
    IOSUnicast unicast = new IOSUnicast(iosAppkey, iosAppMasterSecret);
    // TODO Set your device token
    unicast.setDeviceToken(deviceToken);
    unicast.setAlert(content);
    unicast.setBadge(0);
    unicast.setSound("default");
    // TODO set 'production_mode' to 'true' if your app is under production mode
    unicast.setProductionMode();
    log.info("start client.send");
    return client.send(unicast);
  }

  /**
   * 根据友盟的唯一码push消息到指定客户端
   */
  @Override
  public boolean sendUnicastMessageByUmeng(PushMessage message) {
    log.info("start sendUnicastMessageByUmeng");
    boolean retVal = false;
    String userId = message.getUserId();
    String title = message.getTitle();
    String content = message.getDesc();
    log.info("userId = " + userId + " title = " + title + " content = " + content);
    try {
      User user = userService.load(userId);
      // 查询用户表中存的android和ios的token信息，如果有，则推送
      String androidToken = user.getCid2();
      String iosToken = user.getCid3();
      log.info("androidToken = " + androidToken + " iosToken = " + iosToken);
      if (StringUtil.isNotEmpty(androidToken)) {
        sendAndroidUnicast(androidToken, title, content);
        sendAndroidCustom(androidToken, message.getRefreshType().toString());
      }
      if (StringUtil.isNotEmpty(iosToken)) {
        sendIOSUnicast(iosToken, content);
        sendIOSCustom(iosToken, message.getRefreshType().toString());
      }
    } catch (Exception e) {
      log.error(e.toString());
    }
    return retVal;
  }

  /**
   * 通过友盟push广播消息到所有客户端
   */
  @Override
  public boolean sendBroadcastMessageByUmeng(PushMessage message) {
    boolean retVal = false;
    String title = message.getTitle();
    String content = message.getDesc();
    try {
      sendAndroidBroadcast(title, content);
      sendIOSBroadcast(content);
    } catch (Exception e) {
      log.error(e.toString());
    }
    return retVal;
  }

  public static void main(String[] args) {
    try {
      PushClient client = new PushClient();
      AndroidUnicast unicast = new AndroidUnicast("57eb8192e0f55a51740007d2",
          "gu36m7lieennfyc7pfsfcpybfdz2bscz");
      // TODO Set your device token
      unicast.setDeviceToken("AslJW2cakORy3vRmeoqN-_0pJbENAtFGtHm50PQJQCJv");
      unicast.setTicker("111");
      unicast.setTitle("222");
      unicast.setText("333");
      unicast.goAppAfterOpen();
      unicast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
      // TODO Set 'production_mode' to 'false' if it's a test device.
      // For how to register a test device, please see the developer doc.
      unicast.setProductionMode();
      client.send(unicast);
    } catch (Exception e) {
      e.printStackTrace();
    }


  }


}
