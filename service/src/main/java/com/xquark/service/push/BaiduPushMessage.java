package com.xquark.service.push;


import java.util.Date;

import org.json.JSONObject;
import org.parboiled.common.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xquark.dal.model.PushMessage;
import com.xquark.dal.type.PushMessageDeviceType;
import com.xquark.dal.type.PushMessageType;

/**
 * push到百度云推送客户端的消息对象
 *
 * @author odin
 */
public class BaiduPushMessage extends PushMessage {

  private static final long serialVersionUID = 1L;
  private static Logger log = LoggerFactory.getLogger(PushMessage.class);

  /**
   * 设备类型，取值范围为：1～5 云推送支持多种设备，各种设备的类型编号如下： 1：浏览器设备； 2：PC设备； 3：Andriod设备； 4：iOS设备； 5：Windows Phone设备；
   * 如果存在此字段，则向指定的设备类型推送消息。 默认为android设备类型。
   */
  public Integer getBaiduDeviceType() {
    PushMessageDeviceType type = this.getDeviceType();
    if (type == PushMessageDeviceType.ANDROID) {
      return 3;
    } else if (type == PushMessageDeviceType.IOS) {
      return 4;
    } else if (type == PushMessageDeviceType.WEB) {
      return 1;
    } else if (type == PushMessageDeviceType.PC) {
      return 2;
    } else if (type == PushMessageDeviceType.WP) {
      return 5;
    }
    return 3;
  }

  /**
   * 消息类型 0：消息（透传给应用的消息体） 1：通知（对应设备上的消息通知） 默认值为0。
   */
  public Integer getBaiduMessageType() {
    PushMessageType type = this.getType();
    if (type == PushMessageType.MESSAGE) {
      return 0;
    } else if (type == PushMessageType.NOTIFICATION) {
      return 1;
    }
    return 0;
  }

  public boolean valid() {
    return true;
  }

  public static String toMessage(PushMessage message) {
    String ret = null;
    try {
      String desc = null;
      if (message.getDesc().length() > 20) {
        //desc = message.getDesc().substring(0,20)+"...";
        desc = "点击查看详情...";
      } else {
        desc = message.getDesc();
      }
      String title = null;
      if (message.getTitle().length() > 16) {
        title = message.getTitle().substring(0, 16);
      } else {
        title = message.getTitle();
      }
      JSONObject jsonObject = new JSONObject();
      if (message.getDeviceType() == PushMessageDeviceType.ANDROID) {
        jsonObject.put("title", title);
        jsonObject.put("desc", desc);
        if (StringUtils.isNotEmpty(message.getImageUrl())) {
          jsonObject.put("imageUrl", message.getImageUrl());
        } else {
          jsonObject.put("imageUrl", "default");
        }
        jsonObject.put("detailUrl", message.getDetailUrl());
        if (message.getCreatedAt() != null) {
          jsonObject.put("createdAt", message.getCreatedAt().getTime());
        } else {
          jsonObject.put("createdAt", System.currentTimeMillis());
        }
        if (message.getMsgType() != null) {
          jsonObject.put("type", message.getMsgType());
        } else {
          jsonObject.put("type", 1);  //  default is system msg
        }
        jsonObject
            .put("userId", message.getBaiduTagName() != null ? message.getBaiduTagName() : "");
        if (message.getOrderId() != null && !message.getOrderId().isEmpty()) {
          jsonObject.put("data0", message.getOrderId());
        }
        ret = jsonObject.toString();
        if (ret.length() > 2048) {
          log.warn("push到android客户端时，message转换json后长度超过2K字符, ret=" + ret);
          //throw new RuntimeException("push到android客户端时，message转换json后长度超过2K字符");
        }
      } else if (message.getDeviceType() == PushMessageDeviceType.IOS) {
        /**
         * 苹果的APN有256B长度限制，所以对json字段做了缩小设置
         */
        jsonObject.put("head", title);
        jsonObject.put("desc", desc);
        jsonObject.put("durl", message.getDetailUrl());
        if (message.getCreatedAt() != null) {
          jsonObject.put("at", message.getCreatedAt().getTime());
        } else {
          jsonObject.put("at", System.currentTimeMillis());
        }
        if (StringUtils.isNotEmpty(message.getImageUrl())) {
          jsonObject.put("iurl", message.getImageUrl());
        } else {
          jsonObject.put("iurl", "default");
        }
        jsonObject.put("uid", message.getBaiduTagName() != null ? message.getBaiduTagName() : "");
        if (message.getMsgType() != null) {
          jsonObject.put("type", message.getMsgType());
        } else {
          jsonObject.put("type", 1);  //  default is system msg
        }

        if (message.getOrderId() != null && !message.getOrderId().isEmpty()) {
          jsonObject.put("d0", message.getOrderId());
        }
        /**
         * 根据百度格式自定义设置，不能使用JSONObject生成嵌套的json格式（嵌套的json格式会多生成双引号造成百度解析出错）
         */
        String aTitle = title + " " + desc;
        title =
            aTitle.substring(0, aTitle.length() < 16 ? aTitle.length() : 16) + (aTitle.length() < 16
                ? "..." : "");
        ret = "{\"aps\":{\"alert\":\"" + title + "\"}," + jsonObject.toString().substring(1);
        if (ret.getBytes().length > 256) {
          jsonObject.remove("iurl");
          // FIXME 消息长度不要抛异常
          log.warn("push到ios客户端时，message转换json后长度超过256字符, ret=" + ret);
          ret = "{\"aps\":{\"alert\":\"" + title + "\"}," + jsonObject.toString().substring(1);
          // throw new RuntimeException("push到ios客户端时，message转换json后长度超过256字符");
        }
      }
      /**
       Class<? extends PushMessage> messageClass = message.getClass();
       Field[] fields = messageClass.getDeclaredFields();
       for (Field field : fields) {
       if (!(field.getName().equals("log") || field.getName().equals("serialVersionUID"))) {
       PropertyDescriptor pd = new PropertyDescriptor(field.getName(), messageClass);
       Method method = pd.getReadMethod();
       Object data = method.invoke(message, null);
       if (data == null){
       data = "null";
       }
       jsonObject.put(field.getName(),data);
       }
       }
       */
    } catch (Exception e) {
      log.error("pushmessage转换json出错", e);
    }
    log.debug("finally pushString=" + ret);
    return ret;
  }

  public static void main(String[] argc) {
    PushMessage message = new PushMessage();
    message.setDeviceType(PushMessageDeviceType.ANDROID);
    message.setCreatedAt(new Date());
    message.setUpdatedAt(new Date());
    message.setTitle("标题标题标题标题标题标题标题标题标题标题标题标题标题标题标题标题标题标题");
    message.setDesc("欢迎大家欢迎大家欢迎大家欢迎大家欢迎大家欢迎大家欢迎大家欢迎大家欢迎大家欢迎大家欢迎大家欢迎大家欢迎大家欢迎大家欢迎大家");
    message.setImageUrl("imageurl");
    message.setMsgType(2001);
    message.setDetailUrl("/message/idxd");
    String out = BaiduPushMessage.toMessage(message);
    System.out.println(out);
    System.out.println(out.getBytes().length);
    message.setDeviceType(PushMessageDeviceType.IOS);
    out = BaiduPushMessage.toMessage(message);
    System.out.println(out);
    System.out.println(out.getBytes().length);
  }
}
