package com.xquark.service.push.umeng.push.ios;

import com.xquark.service.push.umeng.push.IOSNotification;

public class IOSBroadcast extends IOSNotification {

  public IOSBroadcast(String appkey, String appMasterSecret) throws Exception {
    setAppMasterSecret(appMasterSecret);
    setPredefinedKeyValue("appkey", appkey);
    this.setPredefinedKeyValue("type", "broadcast");

  }
}
