package com.xquark.service.push;

import com.xquark.dal.model.PushMessage;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.service.BaseService;

public interface PushService extends BaseService {
  /**
   * 发送手机短信
   * send mobile message
   * @param mobile
   * @param msg
   * @return
   */
//	boolean sendSms(String mobile, String msg);
//	
//	boolean sendSms(String mobile, String msg, UserPartnerType userType);

  /**
   * 根据百度云的唯一码push消息到指定客户端
   */
  boolean sendUnicastMessageByBaidu(BaiduPushMessage message);

  /**
   * 根据百度云的tag push消息到一群客户端
   */
  boolean sendTagMessageByBaidu(BaiduPushMessage message);

  /**
   * 通过百度云push广播消息到所有客户端
   */
  boolean sendBroadcastMessageByBaidu(BaiduPushMessage message);

  boolean sendSmsEngine(String mobile, String content);

  boolean sendSmsEngine(String mobile, String content, UserPartnerType userType,
      String paramsData, String templateCode);

  /**
   * 根据友盟的唯一码push消息到指定客户端
   */
  boolean sendUnicastMessageByUmeng(PushMessage message);

  /**
   * 通过友盟push广播消息到所有客户端
   */
  boolean sendBroadcastMessageByUmeng(PushMessage message);

}
