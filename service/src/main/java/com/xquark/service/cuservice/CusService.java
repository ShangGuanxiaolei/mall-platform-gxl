package com.xquark.service.cuservice;

import com.vdlm.common.lang.CollectionUtil;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.SFOrderLogDal;
import com.xquark.service.logistics.LogisticsGoodsService;
import com.xquark.service.vo.SFOrderCheck;
import com.xquark.service.vo.SFOrderLog;
import com.xquark.service.wms.WmsExpressService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/1/4
 * Time: 13:37
 * Description: 客户
 */
@Service
public class CusService {
     Logger  log= LoggerFactory.getLogger(this.getClass());
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private LogisticsGoodsService logisticsGoodsService;
    @Autowired
    private WmsExpressService wmsExpressService;

    /*
     * @Author chp
     * @Description //物流查询
     * @Date
     * @Param
     * @return
     **/
    public Map<String,Object>  queryLogistics(String orderNo){
        String dest = orderMapper.queryDest(orderNo);
//        Map map  = orderMapper.queryLogistics(orderNo);
       OrderVO orderVO  = orderMapper.selectOrderVOByOrderNo(orderNo,null);
        Map<String,Object> map=new LinkedHashMap<>();
        map.put("orderNo",orderVO.getOrderNo());
        map.put("productImg",orderVO.getOrderItems().get(0).getProductImg());
        map.put("setProductImgUrl",orderVO.getOrderItems().get(0).getProductImgUrl());
        map.put("logistics_company",orderVO.getLogisticsCompany());
        map.put("logistics_order_no",orderVO.getLogisticsOrderNo());
        map.put("status",orderVO.getStatus().name());
        map.put("totalFee",orderVO.getTotalFee());

        SFOrderLog lastLogis = getLastLogis(getLogis(orderNo, dest));
        if(lastLogis!=null){
            map.put("logistis",lastLogis);
        }
        else {map.put("logistis","");}
        return  map;
    }

  /**
   * @Author chp
   * @Description //查询物流详情
   * @Date
   * @Param
   * @return
   **/
    public List<SFOrderLog>  getLogis(String orderNo,String dest){
        List<SFOrderLog> sfOrderLogs = null;
        try {
            if (("SF_REFUNDABLE").equals(dest) || "SF_NOT_REFUNDABLE".equals(dest)) {
                SFOrderCheck sfOrderCheck = logisticsGoodsService.getOrderDetail(orderNo);
                String billSn = sfOrderCheck.getOrderItems().get(0).getShippingSn();
                sfOrderLogs = logisticsGoodsService.getSFOrderLog(orderNo, billSn);

                return sfOrderLogs;
            } else if("CLIENT_REFUNDABLE".equals(dest)|| "CLIENT_NOT_REFUNDABLE".equals(dest)) {
                List<SFOrderLogDal> sfOrderLogDals = wmsExpressService.getRouteByOrderNo(orderNo);
                sfOrderLogs = new ArrayList<>();
                for (SFOrderLogDal s : sfOrderLogDals) {
                    SFOrderLog sfOrderLog = new SFOrderLog();
                    sfOrderLog.setTime(s.getTime().getTime() / 1000 + "");
                    sfOrderLog.setCommont(s.getCommont());
                    sfOrderLogs.add(sfOrderLog);
                }
                return sfOrderLogs;
            }else{

                Map waybillMap = logisticsGoodsService.getbillNoByOrderNo(orderNo);
                String billNo = (String)waybillMap.get("logistics_order_no");
                //运单号可能为多个，由逗号分隔
                List<String> billNoList = Arrays.asList(billNo.split(","));
                String type = (String)waybillMap.get("logistics_company_type");
                //查询第三方物流信息
                List<SFOrderLog> thirdLogistics = logisticsGoodsService.getThirdLogistics(billNoList.get(0),type);
                if(CollectionUtil.isNotEmpty(thirdLogistics)){
                    return thirdLogistics;
                }
            }
        } catch (Exception e) {
            log.error("路由查询错误", e);
        }
        return null;
    }
    /*
     * @Author chp
     * @Description //获取最新物流信息
     * @Date
     * @Param
     * @return
     **/
     public SFOrderLog  getLastLogis(List<SFOrderLog> list) {
         if (list != null && !list.isEmpty()) {
             Collections.sort(list, new Comparator<SFOrderLog>() {
                 public int compare(SFOrderLog arg0, SFOrderLog arg1) {
                     Long hits0 = Long.parseLong(arg0.getTime());
                     Long hits1 = Long.parseLong(arg1.getTime());
                     if (hits1 > hits0) {
                         return 1;
                     } else if (hits1 == hits0) {
                         return 0;
                     } else {
                         return -1;
                     }
                 }
             });
             return list.get(0);
         }
         return  null;
     }

    /*
     * @Author chp
     * @Description 修改地址信息
     * @Param
     * @return
     **/
    public Map<String,Object> queryAdress(String orderNo){
        OrderVO orderVO  = orderMapper.selectOrderVOByOrderNo(orderNo,null);
        Map<String,Object> map=   new LinkedHashMap<>();
        map.put("orderNo",orderVO.getOrderNo());
        map.put("productImg",orderVO.getOrderItems().get(0).getProductImg());
        map.put("setProductImgUrl",orderVO.getOrderItems().get(0).getProductImgUrl());
        map.put("createAt",orderVO.getCreatedAt());
        map.put("status",orderVO.getStatus().name());
        map.put("totalFee",orderVO.getTotalFee());
        return map;
    }



}
