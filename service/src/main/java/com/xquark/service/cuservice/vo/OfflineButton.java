package com.xquark.service.cuservice.vo;

/**
 * 在线客服button
 */
public class OfflineButton {

    private String btId;

    private String btName;

    public String getBtId() {
        return btId;
    }

    public void setBtId(String btId) {
        this.btId = btId;
    }

    public String getBtName() {
        return btName;
    }

    public void setBtName(String btName) {
        this.btName = btName;
    }
}
