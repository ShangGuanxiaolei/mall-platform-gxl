package com.xquark.service.cuservice.vo;

public enum Constant {

    MY_ORDER(true),//我的订单
    LOGISTI_CQUERY(true), //查询物流
    CONSULT_APPLY(true),//售后申请
    MODIFY_ADRESS(true);//修改地址

    private final boolean cancelable;

    Constant(boolean cancelable) {
        this.cancelable = cancelable;
    }

    public boolean isCancelable() {
        return cancelable;
    }
}
