package com.xquark.service.cuservice;

import com.xquark.service.cuservice.vo.ConsultOrderResult;
import com.xquark.service.cuservice.vo.OfflineButton;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * 客服订单
 */
public interface ConsultOrderService {
    /**
     * 在线客服我的订单
     * @param pageable
     * @return
     */
    List<ConsultOrderResult> selectMyOrder(Pageable pageable);

    /**
     * 依照订单状态查询订单列表
     * @param pageable
     * @param userId
     * @param type
     * @return
     */
    List<ConsultOrderResult> selectOrderByStatus(String userId, Pageable pageable, String type);

    /**
     * 物流订单列表
     * @param userId
     * @param pageable
     * @param type
     * @return
     */
    List<ConsultOrderResult> selectLogisticsList(String userId, Pageable pageable, String type);

    /**
     * 售后订单列表 发货时间14天内+售后中
     * @param userId
     * @param pageable
     * @param type
     * @return
     */
    List<ConsultOrderResult> selectConsultApplyList(String userId, Pageable pageable, String type);

    /**
     * 修改地址列表
     * @param userId
     * @param pageable
     * @param type
     * @return
     */
    List<ConsultOrderResult> selectModifyAdressList(String userId, Pageable pageable, String type);

    /**
     * 查询订单信息
     * @param orderNo
     * @param modifyAdressDateFlag 修改地址时间30分钟
     * @return
     */
    ConsultOrderResult selectOrderByOrderNo(String orderNo, Integer modifyAdressDateFlag);

    /**
     * 售后查询订单信息
     * @param orderNo
     * @return
     */
    ConsultOrderResult selectConsultApplyByNo(String orderNo);

    /**
     * 我的订单中查询订单信息
     * @param orderNo
     * @return
     */
    ConsultOrderResult selectMyOrderByNo(String orderNo);

    /**
     * 修改物流地址中查询订单
     * @param orderNo
     * @return
     */
    ConsultOrderResult selectModifyAdressByNo(String orderNo);

    /**
     * 查询物流中查询订单
     * @param orderNo
     * @return
     */
    ConsultOrderResult selectLogisticsByNo(String orderNo);

    /**
     * 获取在线客服button列表
     * @return
     */
    List<OfflineButton> selectOfflineButtonList();
}
