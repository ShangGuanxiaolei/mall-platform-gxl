package com.xquark.service.cuservice.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import com.xquark.service.vo.SFOrderLog;

import org.apache.commons.lang3.ObjectUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 售后申请
 */
public class ConsultOrderResult {

    private String orderId; //订单id
    private String productImg;

    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String productImgUrl; //商品图片

    private String orderNo; // 交易订单号

    private Date createdAt; //创建时间

    private BigDecimal totalFee; //订单总价格

    private String status; //订单状态

    /** 是否具备申请售后的能力 capableToRefund 为true跳转售后*/
    private Boolean capableToRefund;

    private String statusName; //订单状态中文名

    private Integer amount; //商品数量

    private BigDecimal price; //商品价格

    private String logisticsCompany; // 物流公司

    private String logisticsOrderNo; // 物流公司订单号

    private List<SFOrderLog> orderLogList; //所有物流信息

    private  String orderLogInfo; //物流信息

    private BigDecimal paidFee; // 订单付款总金额

    private OrderAddress orderAddress;  //订单地址
    private List<ProductItem> orderItems;//商品列表

    private String itemProductName; //主商品名称
    private String dest; // 该订单对应商品的合作方
    //物流专用dest
    private String destForLogistics;
    private String certificateId;// 身份证
    private String logisticsNo;// 物流单号
    private boolean afterSaleStatus; //true是售后中

    public boolean isAfterSaleStatus() {
        return afterSaleStatus;
    }

    public void setAfterSaleStatus(boolean afterSaleStatus) {
        this.afterSaleStatus = afterSaleStatus;
    }

    public String getLogisticsNo() {
        return logisticsNo;
    }

    public void setLogisticsNo(String logisticsNo) {
        this.logisticsNo = logisticsNo;
    }

    public String getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(String certificateId) {
        this.certificateId = certificateId;
    }

    public String getDestForLogistics() {
        return destForLogistics;
    }

    public void setDestForLogistics(String destForLogistics) {
        this.destForLogistics = destForLogistics;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    @ApiModelProperty(value = "退款金额")
    private BigDecimal refundFee; // 退款金额，对于新接口为 商品额退款+运费退款

    public BigDecimal getRefundFee() {
        return ObjectUtils.defaultIfNull(refundFee, BigDecimal.ZERO);
    }

    public void setRefundFee(BigDecimal refundFee) {
        this.refundFee = refundFee;
    }

    public List<ProductItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<ProductItem> orderItems) {
        this.orderItems = orderItems;
    }

    public String getItemProductName() {
        return itemProductName;
    }

    public void setItemProductName(String itemProductName) {
        this.itemProductName = itemProductName;
    }

    public OrderAddress getOrderAddress() {
        return orderAddress;
    }

    public void setOrderAddress(OrderAddress orderAddress) {
        this.orderAddress = orderAddress;
    }

    public BigDecimal getPaidFee() {
        return paidFee;
    }

    public void setPaidFee(BigDecimal paidFee) {
        this.paidFee = paidFee;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsOrderNo() {
        return logisticsOrderNo;
    }

    public void setLogisticsOrderNo(String logisticsOrderNo) {
        this.logisticsOrderNo = logisticsOrderNo;
    }

    public List<SFOrderLog> getOrderLogList() {
        return orderLogList;
    }

    public void setOrderLogList(List<SFOrderLog> orderLogList) {
        this.orderLogList = orderLogList;
    }

    public String getOrderLogInfo() {
        return orderLogInfo;
    }

    public void setOrderLogInfo(String orderLogInfo) {
        this.orderLogInfo = orderLogInfo;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProductImg() {
        return productImg;
    }

    public void setProductImg(String productImg) {
        this.productImg = productImg;
    }

    public String getProductImgUrl() {
        return productImgUrl;
    }

    public void setProductImgUrl(String productImgUrl) {
        this.productImgUrl = productImgUrl;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Boolean getCapableToRefund() {
        return capableToRefund;
    }

    public void setCapableToRefund(Boolean capableToRefund) {
        this.capableToRefund = capableToRefund;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "ConsultOrderResult{" +
                "orderId='" + orderId + '\'' +
                ", productImg='" + productImg + '\'' +
                ", productImgUrl='" + productImgUrl + '\'' +
                ", orderNo='" + orderNo + '\'' +
                ", createdAt=" + createdAt +
                ", totalFee=" + totalFee +
                ", status='" + status + '\'' +
                ", capableToRefund=" + capableToRefund +
                ", statusName='" + statusName + '\'' +
                ", amount=" + amount +
                ", price=" + price +
                ", logisticsCompany='" + logisticsCompany + '\'' +
                ", logisticsOrderNo='" + logisticsOrderNo + '\'' +
                ", orderLogList=" + orderLogList +
                ", orderLogInfo='" + orderLogInfo + '\'' +
                ", paidFee=" + paidFee +
                ", orderAddress=" + orderAddress +
                ", orderItems=" + orderItems +
                ", itemProductName='" + itemProductName + '\'' +
                ", dest='" + dest + '\'' +
                ", destForLogistics='" + destForLogistics + '\'' +
                ", certificateId='" + certificateId + '\'' +
                ", logisticsNo='" + logisticsNo + '\'' +
                ", afterSaleStatus=" + afterSaleStatus +
                ", refundFee=" + refundFee +
                '}';
    }
}
