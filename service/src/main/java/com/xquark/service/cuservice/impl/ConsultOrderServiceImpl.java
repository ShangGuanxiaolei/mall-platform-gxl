package com.xquark.service.cuservice.impl;

import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.SystemRegion;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.ErpServiceDestinationFactory;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.SFOrderLogDal;
import com.xquark.service.cuservice.ConsultOrderService;
import com.xquark.service.cuservice.CusService;
import com.xquark.service.cuservice.vo.Constant;
import com.xquark.service.cuservice.vo.ConsultOrderResult;
import com.xquark.service.cuservice.vo.OfflineButton;
import com.xquark.service.cuservice.vo.ProductItem;
import com.xquark.service.logistics.LogisticsGoodsService;
import com.xquark.service.order.OrderService;
import com.xquark.service.systemRegion.SystemRegionService;
import com.xquark.service.vo.MultiWaybillInfo;
import com.xquark.service.vo.SFOrderCheck;
import com.xquark.service.vo.SFOrderLog;
import com.xquark.service.wms.WmsExpressService;
import com.xquark.utils.StringUtil;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Service
public class ConsultOrderServiceImpl implements ConsultOrderService {
    private final Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private CusService cusService;
    @Autowired
    private SystemRegionService systemRegionService;
    @Autowired
    private LogisticsGoodsService logisticsGoodsService;
    @Autowired
    private WmsExpressService wmsExpressService;
    @Override
    public List<ConsultOrderResult> selectMyOrder(Pageable pageable) {
        List<OrderVO> list = orderService.listByStatus(null, pageable, "", "");
        List<ConsultOrderResult> corList= this.convertOrderList(list);
        return corList;
    }

    /**
     * 转化数据
     * @param list
     * @return
     */
    private List<ConsultOrderResult> convertOrderList(List<OrderVO> list) {
        if(list.isEmpty()){
            return null;
        }
        List<ConsultOrderResult> corList = new ArrayList<>();
        for (OrderVO orderVO: list){
            if(orderVO != null){
                ConsultOrderResult consultOrderResult = this.setOrderVOToConsultOrderResult(orderVO);
                corList.add(consultOrderResult);
            }
        }
        return corList;
    }

    /**
     * 售后订单列表、物流订单列别、修改地址订单列表共用
     * @param userId
     * @param pageable
     * @param type
     * @return
     */
    @Override
    public List<ConsultOrderResult> selectOrderByStatus(String userId, Pageable pageable,String type) {

        //userId="cb0ad4fj";//要删这一行
        List<String> orderStatusList = this.getOrderStatusList(type);//查询条件包含订单类型
        if(orderStatusList.isEmpty()){
            return null;
        }
        List<OrderVO> list = orderService.selectByBuyerAndStatusList(userId, orderStatusList, pageable,type);
        List<ConsultOrderResult> consultOrderResults = this.convertOrderList(list);
        return consultOrderResults;
    }

    @Override
    public List<ConsultOrderResult> selectLogisticsList(String userId, Pageable pageable, String type) {
        return this.selectOrderByStatus(userId,pageable,type);
    }

    @Override
    public List<ConsultOrderResult> selectConsultApplyList(String userId, Pageable pageable, String type) {
        return this.selectOrderByStatus(userId,pageable,type);
    }

    @Override
    public List<ConsultOrderResult> selectModifyAdressList(String userId, Pageable pageable, String type) {
        return this.selectOrderByStatus(userId,pageable,type);
    }

    @Override
    public ConsultOrderResult selectOrderByOrderNo(String orderNo, Integer modifyAdressDateFlag) {
        OrderVO orderVO = orderMapper.selectOrderVOByOrderNo(orderNo,modifyAdressDateFlag);
        ConsultOrderResult cor = this.setOrderVOToConsultOrderResult(orderVO);
        return cor;
    }

    @Override
    public ConsultOrderResult selectConsultApplyByNo(String orderNo) {
        return this.selectOrderByOrderNo(orderNo,null);
    }

    @Override
    public ConsultOrderResult selectMyOrderByNo(String orderNo) {
        return this.selectOrderByOrderNo(orderNo,null);
    }

    @Override
    public ConsultOrderResult selectModifyAdressByNo(String orderNo) {
        //30分钟内可以修改地址
        return this.selectOrderByOrderNo(orderNo,30);
    }

    @Override
    public ConsultOrderResult selectLogisticsByNo(String orderNo) {
        ConsultOrderResult consultOrderResult = this.selectOrderByOrderNo(orderNo,null);
        if(consultOrderResult == null){
            return null;
        }
        //查询物流信息
        String dest = orderMapper.queryDest(orderNo);
        List<SFOrderLog> logis = this.cusService.getLogis(orderNo, dest);
        SFOrderLog lastLogis = this.cusService.getLastLogis(logis);
        if(lastLogis != null){
            consultOrderResult.setOrderLogInfo(lastLogis.getCommont());
        }

        return consultOrderResult;
    }

    @Override
    public List<OfflineButton> selectOfflineButtonList() {
        List<OfflineButton> list = new ArrayList<>();
        //button的显示列表，暂时写在代码中，最好写到数据库或配置中心
        OfflineButton ob = new OfflineButton();
        ob.setBtId(Constant.MY_ORDER.name());
        ob.setBtName("我的订单");
        list.add(ob);
        ob = new OfflineButton();
        ob.setBtId(Constant.LOGISTI_CQUERY.name());
        ob.setBtName("物流查询");
        list.add(ob);
        ob = new OfflineButton();
        ob.setBtId(Constant.CONSULT_APPLY.name());
        ob.setBtName("申请售后");
        list.add(ob);
        ob = new OfflineButton();
        ob.setBtId(Constant.MODIFY_ADRESS.name());
        ob.setBtName("修改地址");
        list.add(ob);
        return list;
    }

    /**
     * 转化数据
     * @param orderVO
     * @return
     */
    private ConsultOrderResult setOrderVOToConsultOrderResult(OrderVO orderVO) {
        if (orderVO == null){
            return null;
        }
        ConsultOrderResult cor = new ConsultOrderResult();
        cor.setOrderId(orderVO.getId());
        cor.setCreatedAt(orderVO.getCreatedAt());
        cor.setOrderNo(orderVO.getOrderNo());
        cor.setProductImg(orderVO.getOrderItems().get(0).getProductImg());
        cor.setProductImgUrl(orderVO.getOrderItems().get(0).getProductImg());
        cor.setStatus(orderVO.getStatus().name());
        cor.setTotalFee(orderVO.getTotalFee());
        cor.setStatusName(this.getStatusNameByStatus(orderVO.getStatus().name())); //TODO
        cor.setPrice(orderVO.getOrderItems().get(0).getPrice());
        cor.setAmount(orderVO.getOrderItems().get(0).getAmount());
        cor.setLogisticsCompany(orderVO.getLogisticsCompany());
        cor.setLogisticsOrderNo(orderVO.getLogisticsOrderNo());
        cor.setPaidFee(orderVO.getPaidFee());//实际支付
        cor.setItemProductName(orderVO.getOrderItems().get(0).getProductName());
        cor.setOrderItems(this.orderItemsListToOrderProductItemList(orderVO.getOrderItems(),cor.getStatusName()));
        //用户收货信息
        this.getSysteRegion(orderVO);
        cor.setOrderAddress(orderVO.getOrderAddress());
        cor.setDest(orderVO.getDest());
        cor.setRefundFee(orderVO.getRefundFee());
        cor.setDestForLogistics(orderVO.getDest());
        cor.setCertificateId(orderVO.getCertificateId());
        cor.setCapableToRefund(orderVO.getCapableToRefund());

        //物流查询
        List<MultiWaybillInfo> logicInfo = this.getLogicInfo(cor.getDest(), cor.getOrderNo());
        if(CollectionUtils.isNotEmpty(logicInfo)){
            MultiWaybillInfo m = logicInfo.get(0);
            if(!Objects.isNull(m)){
                List<SFOrderLog> sfOrderLogs = m.getWayBillInfo();
                cor.setOrderLogList(sfOrderLogs);
                if(CollectionUtils.isNotEmpty(sfOrderLogs)){
                    //最新物流信息
                    cor.setOrderLogInfo(sfOrderLogs.get(0).getCommont());
                    Map waybillMap = logisticsGoodsService.getbillNoByOrderNo(cor.getOrderNo());
                    String billNo = (String)waybillMap.get("logistics_order_no");
                    //运单号
                    cor.setLogisticsNo(billNo);
                    String logisticsCompany = (String)waybillMap.get("logistics_company");
                    cor.setLogisticsCompany(logisticsCompany);
                }
            }
        }

        //跳转到售后中
        if("售后中".equals(cor.getStatusName()))
            cor.setAfterSaleStatus(true);
        else {
            cor.setAfterSaleStatus(false);
        }

        //设置前端是否显示申请退款
        if (cor.getDest() != null) {
            if (cor.getDest().contains("NOT_REFUNDABLE")) {
                //存在包含关系
                cor.setDest(ErpServiceDestinationFactory.getErp("NOT_REFUNDABLE"));
            } else {
                //不存在包含关系
                cor.setDest(ErpServiceDestinationFactory.getErp("REFUNDABLE"));
            }
        }

        return cor;
    }

    private void getSysteRegion(OrderVO orderVO) {
        OrderAddress oa = orderVO.getOrderAddress();
        List<SystemRegion> systemRegionList = systemRegionService.listParents(oa.getZoneId());
        StringBuilder details = new StringBuilder();
        for (SystemRegion systemRegion : systemRegionList) {
            details.append(systemRegion.getName());
        }
        details.append(oa.getStreet());

        oa.setSystemRegions(systemRegionList);

        orderVO.setAddressDetails(details.toString());
    }

    private List<ProductItem> orderItemsListToOrderProductItemList(List<OrderItem> orderItems, String status){
        if(orderItems == null || orderItems.isEmpty()){
            return null;
        }
        List<ProductItem> productItems = new ArrayList<>();
        for (OrderItem oi: orderItems) {
            ProductItem productItem = this.setOrderItemToProductItem(oi);
            productItems.add(productItem);
        }
        //去重
        if(CollectionUtils.isNotEmpty(productItems)){
            productItems = productItems.stream()
                    .collect(Collectors.collectingAndThen(Collectors.toCollection(() ->
                            new TreeSet<>(Comparator.comparing(f -> f.getId()))), ArrayList::new));
        }
        return productItems;
    }

    private ProductItem setOrderItemToProductItem(OrderItem oi){
        ProductItem pi = new ProductItem();
        pi.setOrderId(oi.getOrderId());
        pi.setPrice(oi.getPrice());
        pi.setProductId(oi.getProductId());
        pi.setProductImg(oi.getProductImg());
        pi.setProductImgUrl(oi.getProductImgUrl());
        pi.setProductName(oi.getProductName());
        pi.setSkuId(oi.getSkuId());
        pi.setAmount(oi.getAmount());
        pi.setSku(oi.getSku());
        pi.setSkuStr(oi.getSkuStr());
        pi.setMarketPrice(oi.getMarketPrice());
        pi.setId(IdTypeHandler.decode(pi.getProductId()));
        return pi;
    }


    /**
     * 获取订单状态名称
     * @param status
     * @return
     */
    private String getStatusNameByStatus(String status) {
        if (StringUtils.isBlank(status)){
            return "无状态";
        }
        switch (status){
            case "SUCCESS":
                return "已完成";
            case "SUBMITTED":
                return "待付款";
            case "CANCELLED":
                return "已取消";
            case "PAID":
                return "待发货";
            case "DELIVERY":
                return "已发货";
            case "SHIPPED":
                return "待收货";
            case "CHANGING":
                return "售后中";
            case "REISSUING":
                return "售后中";
            case "REFUNDING":
                return "售后中";
            case "COMMENT":
                return "待评价";
            case "PAIDNOSTOCK":
                return "待发货";
            case "CLOSED":
                return "已关闭";
            default:
                return "无状态";
        }
    }

    /**
     * 获取订单状态
     * @param type
     * @return
     */
    private List<String> getOrderStatusList(String type) {
        List<String> orderStatusList = new ArrayList<>();

        //物流查询
        if (Constant.LOGISTI_CQUERY.name().equalsIgnoreCase(type)){
            orderStatusList.add(OrderStatus.PAID.name());
            orderStatusList.add(OrderStatus.DELIVERY.name());
            orderStatusList.add(OrderStatus.SUCCESS.name());
            orderStatusList.add(OrderStatus.SHIPPED.name());
        }

        //申请售后
        if (Constant.CONSULT_APPLY.name().equalsIgnoreCase(type)){//已完成14天内+售后中
            orderStatusList.add(OrderStatus.REFUNDING.name());
            orderStatusList.add(OrderStatus.SHIPPED.name());
            orderStatusList.add(OrderStatus.SUCCESS.name());
        }
        //修改地址
        if (Constant.MODIFY_ADRESS.name().equalsIgnoreCase(type)){
            orderStatusList.add(OrderStatus.PAID.name());
        }
        return orderStatusList;
    }

    private List<MultiWaybillInfo> getLogicInfo(String dest, String orderNo){
        if(StringUtils.isBlank(dest) || StringUtils.isBlank(orderNo))
            return null;
        List<SFOrderLog> sfOrderLogs = null;
        List<MultiWaybillInfo> multiWaybillInfoList =null;
        try {
            if (("SF_REFUNDABLE").equals(dest) || "SF_NOT_REFUNDABLE".equals(dest)) {
                SFOrderCheck sfOrderCheck = logisticsGoodsService.getOrderDetail(orderNo);
                String billSn = sfOrderCheck.getOrderItems().get(0).getShippingSn();
                sfOrderLogs = logisticsGoodsService.getSFOrderLog(orderNo, billSn);
                MultiWaybillInfo multiWaybill = new MultiWaybillInfo();
                multiWaybill.setWayBillNo(billSn);
                multiWaybill.setWayBillInfo(sfOrderLogs);
                multiWaybillInfoList= new ArrayList<>();
                multiWaybillInfoList.add(multiWaybill);
                return multiWaybillInfoList;
            } else if("CLIENT_REFUNDABLE".equals(dest)|| "CLIENT_NOT_REFUNDABLE".equals(dest)) {
                List<SFOrderLogDal> sfOrderLogDals = wmsExpressService.getRouteByOrderNo(orderNo);
                sfOrderLogs = new ArrayList<>();
                for (SFOrderLogDal s : sfOrderLogDals) {
                    SFOrderLog sfOrderLog = new SFOrderLog();
                    sfOrderLog.setTime(s.getTime().getTime() / 1000 + "");
                    sfOrderLog.setCommont(s.getCommont());
                    sfOrderLogs.add(sfOrderLog);
                }
                Map waybillMap = logisticsGoodsService.getbillNoByOrderNo(orderNo);
                String billNo = (String)waybillMap.get("logistics_order_no");
                MultiWaybillInfo multiWaybill = new MultiWaybillInfo();
                multiWaybill.setWayBillNo(billNo);
                multiWaybill.setWayBillInfo(sfOrderLogs);
                multiWaybillInfoList= new ArrayList<>();
                multiWaybillInfoList.add(multiWaybill);
                return multiWaybillInfoList;
            }else{
                //查询第三方物流信息
                Map waybillMap = logisticsGoodsService.getbillNoByOrderNo(orderNo);
                String billNo = (String)waybillMap.get("logistics_order_no");
                //运单号可能为多个，由逗号分隔
                List<String> billNoList = Arrays.asList(billNo.split(","));
                String type = (String)waybillMap.get("logistics_company_type");
                String logisticsCompany =(String)waybillMap.get("logistics_company");
                String phone = (String)waybillMap.get("phone");
                //取收件人手机号后四位
                String end4Phone ="";

                if(StringUtil.isNotNull(phone)){
                    end4Phone=phone.substring(phone.length()-4);
                }

                multiWaybillInfoList= new ArrayList<>();
                for (String wayBillNo : billNoList) {
                    //分别调用阿里云市场物流接口查询物流信息
                    List<SFOrderLog> thirdLogistics = new ArrayList<>();
                    if(StringUtil.isNotNull(logisticsCompany)&&logisticsCompany.contains("顺丰")){
                        //如果是顺丰 运单号+ : + 收件人手机号后四位
                        thirdLogistics = logisticsGoodsService.getThirdLogistics(wayBillNo+":"+end4Phone,type);

                    }else{
                        thirdLogistics = logisticsGoodsService.getThirdLogistics(wayBillNo,type);

                    }
                    MultiWaybillInfo multiWaybill = new MultiWaybillInfo();
                    multiWaybill.setWayBillNo(wayBillNo);
                    multiWaybill.setWayBillInfo(thirdLogistics);
                    multiWaybillInfoList.add(multiWaybill);
                }

                return multiWaybillInfoList;
            }
        } catch (Exception e) {
            log.error("在线客服查询物流信息异常" + orderNo);
        }
        return null;
    }
}
