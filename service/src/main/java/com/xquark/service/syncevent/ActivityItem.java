package com.xquark.service.syncevent;

import java.math.BigDecimal;

public class ActivityItem extends BaseSyncItem {

  private String actId;
  private String actName;
  private String productId;
  private String shopId;
  private BigDecimal markPrice;
  private BigDecimal actPrice;
  private int type = 1;      // 固定为1 (表示来源于快店)

  public String getActId() {
    return actId;
  }

  public void setActId(String actId) {
    this.actId = actId;
  }

  public BigDecimal getMarkPrice() {
    return markPrice;
  }

  public void setMarkPrice(BigDecimal markPrice) {
    this.markPrice = markPrice;
  }

  public BigDecimal getActPrice() {
    return actPrice;
  }

  public void setActPrice(BigDecimal actPrice) {
    this.actPrice = actPrice;
  }

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getActName() {
    return actName;
  }

  public void setActName(String actName) {
    this.actName = actName;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }
}
