package com.xquark.service.syncevent;

import java.util.List;

public interface SyncEventService {

//	/**
//	 * 初始全量同步
//	 */
//	void startFirstSync();

  /**
   * 同步指定店铺数据和商品
   */
  void startSpecifySync(String shopId);

  /**
   * 更新指定店铺数据
   */
  void startUpdateSync(String shopId, Boolean syncProd);

  /**
   * 封店同步(三方下架/删除所有已同步商品)
   */
  void closeShop(String shopId);

  /**
   * 指定事件类型与ids同步 用于数据同步
   */
  void fixSync(Integer event, Integer type, String shopId, List<String> ids);

  /**
   * 根据该库商品状态同步(商品修复) 用于数据同步
   */
  void fixSync(String id);

  /**
   * 根据该库商品状态同步(商品修复) 用于数据同步
   */
  void fixSync();

  /**
   * 发同步事件
   */
  public void publishEvent(KDSyncData ev);

  /**
   * 入队列
   */
  void pushMQ(KDSyncData ev);

}
