package com.xquark.service.syncevent;

import java.math.BigDecimal;
import java.util.Date;

import com.xquark.dal.status.ProductStatus;

public class ProdItem extends BaseSyncItem {

  private String shopId;
  private String productId;
  private String title;
  private String image;      // 图片url
  private String url;        // 商品url
  private Integer sellNum;    // 商品销量
  private BigDecimal price;  // 最低价格
  private Date createTime;    //
  private Boolean archive;    // 商品是否已逻辑删除
  //private ProductStatus status;  // 商品在快店的上下架状态
  private int status = 1;          // 商品在快店的上下架状态  0: 上架  1 or else: 下架
  private Boolean isDelay;       // 是否延迟发货
  private Integer delayDays;   // 延迟发货天数
  private int type = 1;      // 固定为1 (表示来源于快店)
  //private Integer skuAmt; // 商品sku数量

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public Integer getSellNum() {
    return sellNum;
  }

  public void setSellNum(Integer sellNum) {
    this.sellNum = sellNum;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Boolean getIsDelay() {
    return isDelay;
  }

  public void setIsDelay(Boolean isDelay) {
    this.isDelay = isDelay;
  }

  public Integer getDelayDays() {
    return delayDays;
  }

  public void setDelayDays(Integer delayDays) {
    this.delayDays = delayDays;
  }

  //	public Integer getSkuAmt() {
//		return skuAmt;
//	}
//	public void setSkuAmt(Integer skuAmt) {
//		this.skuAmt = skuAmt;
//	}
  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }
}
