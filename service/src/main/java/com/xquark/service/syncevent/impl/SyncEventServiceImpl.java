package com.xquark.service.syncevent.impl;

import com.xquark.dal.mapper.ProdSyncMapper;
import com.xquark.dal.mapper.ProductImageMapper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.ProdSync;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.ProductImage;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.Sku;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.ProductStatus;
import com.xquark.event.SyncActionEvent;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductImageVO;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.rocketmq.SyncRocketMq;
import com.xquark.service.shop.ShopService;
import com.xquark.service.sync.impl.SyncProductEventPublisher;
import com.xquark.service.syncevent.KDSyncData;
import com.xquark.service.syncevent.SyncEventService;
import com.xquark.service.syncevent.SyncEventType;
import com.xquark.service.user.UserService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.ObjectUtils;
import org.parboiled.common.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service("SyncEventService")
public class SyncEventServiceImpl extends BaseServiceImpl implements SyncEventService {

  @Autowired
  private ProdSyncMapper prodSyncMapper;

  @Autowired
  private SkuMapper skuMapper;

  @Autowired
  private ProductImageMapper productImageMapper;

  @Autowired
  private ProductService productService;

  @Autowired
  private ProductMapper productMapper;

  @Autowired
  private ShopService shopService;

  @Autowired
  private UserService userService;

  @Autowired
  private SyncProductEventPublisher syncProductEventPublisher;

  @Autowired
  ApplicationContext applicationContext;

  @Autowired
  private SyncRocketMq syncRocketMq;

  @Value("${site.web.host.name}")
  String siteHost;

  @Value("${sync.scheme.switch}")
  private Integer syncSwitch;

  @Value("${syncpath}")
  private String flagString;

  private class Map4Sync {

    public Map4Sync(String shopId, Integer event) {
      this.setShopId(shopId);
      this.setEvent(event);
    }

    private String shopId;
    private Integer event;

    public String getShopId() {
      return shopId;
    }

    public void setShopId(String shopId) {
      this.shopId = shopId;
    }

    public Integer getEvent() {
      return event;
    }

    public void setEvent(Integer event) {
      this.event = event;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + getOuterType().hashCode();
      result = prime * result + ((event == null) ? 0 : event.hashCode());
      result = prime * result
          + ((shopId == null) ? 0 : shopId.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      Map4Sync other = (Map4Sync) obj;
      if (!getOuterType().equals(other.getOuterType())) {
        return false;
      }
      if (event == null) {
        if (other.event != null) {
          return false;
        }
      } else if (!event.equals(other.event)) {
        return false;
      }
      if (shopId == null) {
        if (other.shopId != null) {
          return false;
        }
      } else if (!shopId.equals(other.shopId)) {
        return false;
      }
      return true;
    }

    private SyncEventServiceImpl getOuterType() {
      return SyncEventServiceImpl.this;
    }
  }

  ;

  @Override
  public void publishEvent(KDSyncData ev) {
    applicationContext.publishEvent(new SyncActionEvent(ev.getEvType(), ev));
  }

  @Override
  public void startUpdateSync(String shopId, Boolean syncProd) {
    if (StringUtils.isEmpty(shopId)) {
      return;
    }

    if (syncProd == null) {
      syncProd = false;
    }

    Shop shop = shopService.load(shopId);
    if (shop == null) {
      return;
    }

    if (syncSwitch == 0) {
      KDSyncData shopEv = new KDSyncData();
      shopEv.setSiteHost(siteHost);
      shopEv.setTimeStamp(new Date());
      shopEv.setPartnerFlag(KDSyncData.XIANGQU_DST);

      shopEv.setEvType(SyncEventType.SHOP_UPDATE);
      String phone = userService.load(shop.getOwnerId()).getPhone();
      shopEv.setUserPhone(StringUtils.isNotEmpty(phone) ? phone : "");
      shopEv.setShop(shop);
      publishEvent(shopEv);

      if (syncProd) {
        List<Product> prods = productService.listProductsByOnsaleAt(shopId, "", "", null,
            Collections.<String, Object>emptyMap());
        for (Product product : prods) {
          List<ProductVO> aProductList = new ArrayList<ProductVO>();
          ProductVO avo = product2VO(product);
          KDSyncData prodEv = new KDSyncData();
          prodEv.setSiteHost(siteHost);
          prodEv.setTimeStamp(new Date());
          prodEv.setPartnerFlag(KDSyncData.XIANGQU_DST);
          aProductList.add(avo);
          prodEv.setProdList(aProductList);
          prodEv.setEvType(SyncEventType.ATTR_UPDATE);
          prodEv.setShop(null);
          publishEvent(prodEv);
        }
      }
    } else {
      prodSyncMapper.updateForSynced(shopId, true);
      /**SyncMqEvent event = new SyncMqEvent();
       if (syncProd) {
       event.setEvent(2);
       event.setType(SyncEvType.EV_SHOP.ordinal());
       } else {
       event.setEvent(1);
       event.setType(SyncEvType.EV_SHOP.ordinal());
       }
       List<String> ids = new ArrayList<String>();
       ids.add(shop.getId());
       event.setIds(ids);
       event.setTimestamp(new Date().getTime());
       syncRocketMq.sendToMQ(event);**/
    }
  }

  @Override
  public void startSpecifySync(String shopId) {
    List<ProdSync> shops = prodSyncMapper.selectByPassedShopId(shopId);
    if (shops == null || shops.size() == 0) {
      return;
    }

    if (syncSwitch == 0) {
      KDSyncData shopEv = new KDSyncData();
      shopEv.setSiteHost(siteHost);
      shopEv.setTimeStamp(new Date());
      shopEv.setPartnerFlag(KDSyncData.XIANGQU_DST);

      shopEv.setEvType(SyncEventType.SHOP_CREATE);
      Shop shop = shopService.load(shopId);
      String phone = userService.load(shop.getOwnerId()).getPhone();
      shopEv.setUserPhone(StringUtils.isNotEmpty(phone) ? phone : "");
      shopEv.setShop(shop);
      publishEvent(shopEv);

      try {
        Thread.sleep(3000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      List<Product> prods = productService
          .listProductsByOnsaleAt(shopId, "", "", null, Collections.<String, Object>emptyMap());
      if (prods == null || prods.size() == 0) {
        return;
      }
      for (Product product : prods) {
        List<ProductVO> aProductList = new ArrayList<ProductVO>();
        ProductVO avo = product2VO(product);
        aProductList.add(avo);
        KDSyncData prodEv = new KDSyncData();
        prodEv.setSiteHost(siteHost);
        prodEv.setTimeStamp(new Date());
        prodEv.setPartnerFlag(KDSyncData.XIANGQU_DST);
        prodEv.setProdList(aProductList);
        prodEv.setEvType(SyncEventType.STATUS_ONSALE);
        prodEv.setShop(null);
        publishEvent(prodEv);
      }
    } else {
      Shop shop = shopService.load(shopId);
      if (shop == null) {
        return;
      }
      /**SyncMqEvent event = new SyncMqEvent();
       List<String> ids = new ArrayList<String>();
       ids.add(shop.getId());
       event.setIds(ids);
       event.setEvent(1);
       event.setType(SyncEvType.EV_SHOP.ordinal());
       event.setTimestamp(new Date().getTime());
       syncRocketMq.sendToMQ(event);**/
    }
    // 4 该店铺更新为已同步
    prodSyncMapper.updateForSynced(shopId, true);
  }

  private ProductVO product2VO(Product product) {
    ProductVO avo = new ProductVO(product);

    List<ProductImage> imgs = productImageMapper.selectByProductId(product.getId());
    List<ProductImageVO> imgsVO = new ArrayList<ProductImageVO>();
    for (ProductImage i : imgs) {
      ProductImageVO iVO = new ProductImageVO();
      iVO.setImg(i.getImg());
      imgsVO.add(iVO);
    }
    avo.setImgs(imgsVO);
    List<Sku> skus = skuMapper.selectByProductId(product.getId());
    avo.setSkus(skus);

    return avo;
  }

  private Boolean shopSyncEvent(KDSyncData ev) {
    if (ev.getEvType().equals(SyncEventType.SHOP_CREATE) ||
        ev.getEvType().equals(SyncEventType.SHOP_UPDATE)) {
      return true;
    }
    return false;
  }

  private Boolean productSyncEvent(KDSyncData ev) {
    if (ev.getEvType().equals(SyncEventType.STATUS_ONSALE) ||
        ev.getEvType().equals(SyncEventType.STATUS_INSTOCK) ||
        ev.getEvType().equals(SyncEventType.ATTR_UPDATE)) {
      return true;
    }
    return false;
  }

  private Boolean ActivitySyncEvent(KDSyncData ev) {
    if (ev.getEvType().equals(SyncEventType.ACTIVITY_START) ||
        ev.getEvType().equals(SyncEventType.ACTIVITY_CLOSE)) {
      return true;
    }
    return false;
  }


  private Boolean syncFilter(KDSyncData ev) {
    Boolean ret = false;
    if (ev == null) {
      return ret;
    }

    List<ProdSync> filterList = new ArrayList<ProdSync>();
    //filterList = prodSyncMapper.loadAll();
    filterList = prodSyncMapper.loadAllPassed();
    if (filterList == null || filterList.size() == 0) {
      return ret;
    }

    for (ProdSync aShop : filterList) {
      if (shopSyncEvent(ev)) {
        Shop shop = ev.getShop();
        if (shop != null && shop.getId().equals(aShop.getShopId())) {
          ret = true;
          break;
        }
      } else if (productSyncEvent(ev)) {
        List<ProductVO> prodList = ev.getProdList();
        if (prodList == null && filterList.size() == 0) {
          break;
        }
        if (prodList.get(0).getShopId().equals(aShop.getShopId())) {
          ret = true;
          break;
        }
      } else if (ActivitySyncEvent(ev)) {
        if (ev.getActList().get(0).getShopId().equals(aShop.getShopId())) {
          ret = true;
          break;
        }
      } else {
      }
    } // end for

    return ret;
  }

  @Override
  public void pushMQ(KDSyncData ev) {
    if (flagString.equals("C:\\sync.inf")) {
      return;
    }

    if (!syncFilter(ev)) {
      return;
    }

    Long index = 0L;
    if (shopSyncEvent(ev)) {
      index = IdTypeHandler.decode(ev.getShop().getId());
    } else if (productSyncEvent(ev)) {
      index = IdTypeHandler.decode(ev.getProdList().get(0).getId());  // 暂时不支持商品列表
      Shop aShop = shopService.load(ev.getProdList().get(0).getShopId());
      if (aShop != null) {
        List<ProdSync> aList = prodSyncMapper.selectByPassedShopId(aShop.getId());
        if (aList != null && aList.size() > 0) {
          ev.setUnionId(aList.get(0).getUnionId());
        }
      }
    } else if (ActivitySyncEvent(ev)) {
      index = IdTypeHandler.decode(ev.getActList().get(0).getProductId());  // 暂时不支持商品列表
    } else {
      return;
    }

    syncProductEventPublisher.publish(index, ev.getEvType(), ev);
  }

  @Override
  public void fixSync() {
    List<Product> spfs = productMapper.select4SyncFix();
    if (spfs == null || spfs.size() == 0) {
      return;
    }

    List<ProdSync> psList = prodSyncMapper.loadAllPassed();
    Set<String> psSet = new HashSet<String>();
    for (ProdSync ps : psList) {
      psSet.add(ps.getShopId());
    }

    Map<Map4Sync, List<String>> aMap = new HashMap<Map4Sync, List<String>>();
    for (Product p : spfs) {
      if (psSet.contains(p.getShopId())) {
        Integer event = 1;  // edit
        if (p.getArchive()) {
          event = 3;  // delete
        } else if (ProductStatus.ONSALE.equals(p.getStatus())) {
          event = 1; // edit
        } else {
          event = 2;  // instock
        }
        Map4Sync aKey = new Map4Sync(p.getShopId(), event);
        List<String> list = ObjectUtils.defaultIfNull(aMap.get(aKey), new ArrayList<String>());
        list.add(p.getId());
        aMap.put(aKey, list);
      } else {
        continue;
      }
    }

    Set<Map4Sync> keys = aMap.keySet();
    for (Iterator it = keys.iterator(); it.hasNext(); ) {
      Map4Sync s = (Map4Sync) it.next();
      //System.out.println(aMap.get(s));
      List<String> values = aMap.get(s);
      for (int i = 0; i < values.size(); i += 10) {
        /**SyncMqEvent ev = new SyncMqEvent();
         ev.setIds(values.subList(i, i+10 > values.size() ? values.size() : i+10));
         ev.setEvent(s.getEvent());
         ev.setType(SyncEvType.EV_PRODUCT.ordinal()); // 同步商品
         ev.setTimestamp(new Date().getTime());
         System.out.println(JSON.toJSONString(ev));
         syncRocketMq.sendToMQ(ev);**/
      }
    }
  }

  @Override
  public void fixSync(String id) {
    if (id == null) {
      fixSync();
      return;
    }

    Product product = productMapper.select4SyncFixById(id);
    if (product == null) {
      return;
    }
    int event = 1;   // 1:edit  2:instock 3:delete
    if (product.getArchive()) {
      event = 3;
    } else if (ProductStatus.ONSALE.equals(product.getStatus())) {
      event = 1;
    } else {
      event = 2;
    }

    /**SyncMqEvent ev = new SyncMqEvent();
     List<String> eventIds = new ArrayList<String>();
     eventIds.add(product.getId());
     ev.setIds(eventIds);
     ev.setEvent(event);
     ev.setType(SyncEvType.EV_PRODUCT.ordinal()); // 同步商品
     ev.setTimestamp(new Date().getTime());
     System.out.println(JSON.toJSONString(ev));
     syncRocketMq.sendToMQ(ev);**/

  }

  @Override
  public void fixSync(Integer event, Integer type, String shopId, List<String> ids) {
    if (type < 1 || type > 3 || event < 0 || event > 3) {
      return;
    }
    /**SyncMqEvent ev = new SyncMqEvent();
     List<String> eventIds = new ArrayList<String>();
     if (type == 1) {
     eventIds.add(shopId);
     Shop shop = shopService.load(shopId);
     if (shop == null) return ;
     } else {
     eventIds.addAll(ids);
     }
     ev.setIds(eventIds);
     ev.setEvent(event);
     ev.setType(type);
     ev.setTimestamp(new Date().getTime());
     syncRocketMq.sendToMQ(ev);**/

    // 该店铺更新为已同步
    prodSyncMapper.updateForSynced(shopId, true);
  }

  @Override
  public void closeShop(String shopId) {
    /**SyncMqEvent event = new SyncMqEvent();
     List<String> ids = new ArrayList<String>();
     ids.add(shopId);
     event.setIds(ids);
     event.setEvent(3);
     event.setType(SyncEvType.EV_SHOP.ordinal());
     event.setTimestamp(new Date().getTime());
     syncRocketMq.sendToMQ(event);		**/
  }

}
	
