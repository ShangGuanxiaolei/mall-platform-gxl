package com.xquark.service.syncevent;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.model.Shop;
import com.xquark.dal.status.ProductStatus;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.sync.SyncContent;
import com.xquark.service.sync.SyncEvent;

public class KDSyncData implements SyncContent {

  private static final long serialVersionUID = 1L;

  private Logger log = LoggerFactory.getLogger(getClass());

  public static final Long XIANGQU_DST = 1L;

  private String siteHost;

  private String userPhone;

  private String unionId;

  /**
   * 第三方标识 (按位)
   */
  private Long partnerFlag;

  /**
   * 消息类型
   */
  SyncEvent evType;

  private Shop shop;

  /**
   * 更新商品
   */
  private List<ProductVO> prodList;

  /**
   * 活动商品列表
   */
  private List<ActivityItem> actList;

  /**
   * 操作时间戳
   */
  private Date timeStamp = new Date();

  public Long getPartnerFlag() {
    return partnerFlag;
  }

  public void setPartnerFlag(Long partnerFlag) {
    this.partnerFlag = partnerFlag;
  }

  public SyncEvent getEvType() {
    return evType;
  }

  public void setEvType(SyncEvent evType) {
    this.evType = evType;
  }

  public List<ProductVO> getProdList() {
    return prodList;
  }

  public void setProdList(List<ProductVO> prodList) {
    this.prodList = prodList;
  }

  public Date getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(Date timeStamp) {
    this.timeStamp = timeStamp;
  }

  public Shop getShop() {
    return shop;
  }

  public void setShop(Shop shop) {
    this.shop = shop;
  }

  public String getUserPhone() {
    return userPhone;
  }

  public void setUserPhone(String userPhone) {
    this.userPhone = userPhone;
  }

  public String getUnionId() {
    return unionId;
  }

  public void setUnionId(String unionId) {
    this.unionId = unionId;
  }

  public String getSiteHost() {
    return siteHost;
  }

  public void setSiteHost(String siteHost) {
    this.siteHost = siteHost;
  }

  private ShopItem generateShopIinfo() {
    ShopItem item = new ShopItem();
    item.setTimestamp(new Date());
    item.setEventCode(this.evType.getTopic());
    item.setUserId(this.getShop().getOwnerId());
    item.setPhone(this.userPhone);
    item.setUrl(siteHost + "/shop/" + this.getShop().getId());
    item.setArchive(this.getShop().getArchive());
    item.setShopId(this.getShop().getId());
    item.setName(this.getShop().getName());
    item.setNick(this.getShop().getName());
    item.setImage(this.getShop().getImg());
    item.setDescription(this.getShop().getDescription());
    item.setCreateTime(this.getShop().getCreatedAt());
    log.debug(JSONObject.toJSONString(item));
    return item;
  }

  private ProdItem generateProductinfo() {
    ProdItem item = new ProdItem();
    item.setTimestamp(new Date());
    item.setEventCode(this.evType.getTopic());

    ProductVO aVo = this.getProdList().get(0); // TODO: 商品列表
    BeanUtils.copyProperties(aVo, item);
    item.setStatus(ProductStatus.ONSALE.equals(aVo.getStatus()) ? 0 : 1);
    if (aVo.getDelayed() != null && aVo.getDelayed() != 0) {
      item.setIsDelay(true);
      item.setDelayDays(aVo.getDelayAt());
    } else if (aVo.getDelayAt() == null) {
      item.setIsDelay(false);
      item.setDelayDays(0);
    } else {
      item.setIsDelay(null);
      item.setDelayDays(null);
    }
    item.setProductId(aVo.getId());
    item.setTitle(aVo.getName());
    item.setImage(aVo.getImg());
    item.setUrl(siteHost + "/p/" + aVo.getId() + "?partner=xiangqu&union_id=" + this.getUnionId());
    item.setSellNum(aVo.getSales());
    item.setCreateTime(aVo.getCreatedAt());
    item.setPrice(aVo.getPrice());
    log.debug(JSONObject.toJSONString(item));
    return item;
  }

  private ActivityItem generateActivityIinfo() {
    if (this.getActList() != null && this.getActList().get(0) != null) {
      this.getActList().get(0).setTimestamp(new Date());
      this.getActList().get(0).setEventCode(this.evType.getTopic());
      return this.getActList().get(0); // 活动商品列表
    }

    return null;
  }

  public byte[] toJSONBytes() {

    if (this.evType.equals(SyncEventType.SHOP_CREATE) ||
        this.evType.equals(SyncEventType.SHOP_UPDATE)) {
      return JSONObject.toJSONBytes(generateShopIinfo());
    }

    if (this.evType.equals(SyncEventType.STATUS_ONSALE) ||
        this.evType.equals(SyncEventType.STATUS_INSTOCK) ||
        this.evType.equals(SyncEventType.ATTR_UPDATE)) {
      return JSONObject.toJSONBytes(generateProductinfo());
    }

    if (this.evType.equals(SyncEventType.ACTIVITY_START) ||
        this.evType.equals(SyncEventType.ACTIVITY_CLOSE)) {
      return JSONObject.toJSONBytes(generateActivityIinfo());
    }

    log.debug("syncData unexpected event type...");
    return null;
  }

  public List<ActivityItem> getActList() {
    return actList;
  }

  public void setActList(List<ActivityItem> actList) {
    this.actList = actList;
  }

}
