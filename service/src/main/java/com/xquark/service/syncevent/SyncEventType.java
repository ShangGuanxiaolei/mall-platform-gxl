package com.xquark.service.syncevent;

import com.xquark.service.sync.SimpleSyncEvent;
import com.xquark.service.sync.SyncEvent;

public interface SyncEventType {

  /**
   * 店铺创建
   */
  SyncEvent SHOP_CREATE = new SimpleSyncEvent((byte) 1, (byte) 1);

  /**
   * 店铺更新  店铺名称, 拥有者昵称,
   */
  SyncEvent SHOP_UPDATE = new SimpleSyncEvent((byte) 1, (byte) 2);

  /**
   * 创建商品, 发布商品, 商品上架
   */
  SyncEvent STATUS_ONSALE = new SimpleSyncEvent((byte) 1, (byte) 3);

  /**
   * 商品下架 ,  STATUS_DELETE  删除商品  对于第三方提供 下架或删除之一 即可
   */
  SyncEvent STATUS_INSTOCK = new SimpleSyncEvent((byte) 1, (byte) 4);

  /**
   * 商品修改  图片(修改,尺寸变更), 名称, 价格, 描述, 库存, 销量
   */
  SyncEvent ATTR_UPDATE = new SimpleSyncEvent((byte) 1, (byte) 5);

  /**
   * 快店活动开始or结束
   */
  SyncEvent ACTIVITY_START = new SimpleSyncEvent((byte) 1, (byte) 6);
  SyncEvent ACTIVITY_CLOSE = new SimpleSyncEvent((byte) 1, (byte) 7);

}
