package com.xquark.service.syncevent;

import java.util.Date;

public class BaseSyncItem {

  private byte eventCode;  // 事件号
  //	private String dst;				// 第三方标识串
  private Date timestamp;    // 本次事件时间戳

  //	public String getDst() {
//		return dst;
//	}
//	public void setDst(String dst) {
//		this.dst = dst;
//	}
  public Date getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  public byte getEventCode() {
    return eventCode;
  }

  public void setEventCode(byte eventCode) {
    this.eventCode = eventCode;
  }
}
