package com.xquark.service.rocketmq;

import com.xquark.dal.vo.SyncMqEvent;

public interface SyncRocketMq {

  public boolean sendToMQ(SyncMqEvent event);

  public boolean sendToMQImpl(byte[] body);

}