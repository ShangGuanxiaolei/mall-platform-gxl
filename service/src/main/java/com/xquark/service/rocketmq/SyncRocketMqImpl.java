package com.xquark.service.rocketmq;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.rocketmq.client.exception.MQClientException;
import com.alibaba.rocketmq.client.producer.SendResult;
import com.alibaba.rocketmq.client.producer.SendStatus;
import com.alibaba.rocketmq.common.message.Message;
import com.ouer.rocketmq.client.ext.DefaultMQSessionFactory;
import com.ouer.rocketmq.client.ext.wrapper.MQProducerProxy;
import com.xquark.dal.mapper.ProdSyncMapper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.model.ProdSync;
import com.xquark.dal.model.Product;
import com.xquark.dal.vo.SyncMqEvent;
import com.xquark.service.product.ProductService;


@Service("syncRocketMq")
public class SyncRocketMqImpl implements SyncRocketMq {

  protected final Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private ProdSyncMapper prodSyncMapper;

  @Autowired
  private ProductService productService;

  @Autowired
  private ProductMapper productMapper;

  MQProducerProxy syncProducer;
  final String syncProducerGroup = "KKKD";
  final String syncTopic = "KD_SYNC_XQ";
  final String syncTags = "TagA";


  public void init() {
    DefaultMQSessionFactory mqSessionFactory = new DefaultMQSessionFactory();
    try {
      mqSessionFactory.init();
      log.info("MQ初始化成功[mqSessionFactory]...");
    } catch (MQClientException e) {
      log.error("MQ初始化失败[mqSessionFactory]...", e);
    }
    initSyncProducer(mqSessionFactory);
  }

  public void initSyncProducer(DefaultMQSessionFactory mqSessionFactory) {
    try {
      syncProducer = mqSessionFactory.createAndStartMQProducer(syncProducerGroup);
      log.info("MQ初始化成功[initSyncProducer]...");
    } catch (MQClientException e) {
      log.error("MQ初始化失败[initSyncProducer]...", e);
    }
  }

  @Override
  public boolean sendToMQ(SyncMqEvent event) {
    boolean ret = false;
    if (event == null) {
      return ret;
    }

    List<ProdSync> filterList = prodSyncMapper.loadAllPassed();
    if (filterList == null || filterList.size() == 0) {
      return ret;
    }

    String idString = event.getIds().get(0);
    if (idString == null) {
      return ret;
    }

    if (event.getType() == 1) { // shop
      for (ProdSync aShop : filterList) {
        if (idString.equals(aShop.getShopId())) {
          ret = true;
          break;
        }
      }
    } else if (event.getType() == 2) { // product
      //Product p = productService.load(idString);
      Product p = productMapper.selectByAdmin(idString);
      if (p == null || p.getShopId() == null) {
        return ret;
      }
      for (ProdSync aShop : filterList) {
        if (p.getShopId().equals(aShop.getShopId())) {
          ret = true;
          break;
        }
      }
    } else if (event.getType() == 3) { // activity
      //Product p = productService.load(idString);
      Product p = productMapper.selectByAdmin(idString);
      if (p == null || p.getShopId() == null) {
        return ret;
      }
      for (ProdSync aShop : filterList) {
        if (p.getShopId().equals(aShop.getShopId())) {
          ret = true;
          break;
        }
      }
    }
    if (ret == true) {
      return sendToMQImpl(JSONObject.toJSONBytes(event));
    } else {
      return ret;
    }
  }

  @Override
  public boolean sendToMQImpl(byte[] body) {
    if (syncProducer == null) {
      log.info("MQ初始化失败[initSyncProducer]...");
      return false;
    }
    // 消息内容，需要和消费者约束
    Message msg = new Message(syncTopic, syncTags, body);
    // 发送消息
    SendResult sendResult;
    try {
      sendResult = syncProducer.send(msg);
    } catch (Exception ex) {
      log.error("MQ消息发送失败[sendSyncInfoToMQ]...", ex);
      return false;
    }
    try {
      log.debug("sendToMQ:" + new String(body, "UTF-8"));
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return (sendResult.getSendStatus() == SendStatus.SEND_OK);
  }

  @PreDestroy
  public void destory() {
    if (syncProducer != null) {
      syncProducer.shutdown();
      log.info("MQ销毁成功[syncProducer]...");
    }
  }


}