package com.xquark.service.vo;

import java.util.List;
import org.neo4j.cypher.internal.compiler.v2_1.functions.Str;

/**
 * User: kong Date: 18-7-24. Time: 上午11:13 顺丰订单明细
 */
public class OrderItems {

  //产品信息
  private List<OrderProduct> OrderProducts;

  //物流单号
  private String shippingSn;

  //订单状态字符串
  private String shippingStatusStr;

  //订单编号
  private String orderSn;

  //物流公司名称
  private String shippingTypeName;

  public List<OrderProduct> getOrderProducts() {
    return OrderProducts;
  }

  public void setSfOrderProducts(List<OrderProduct> OrderProducts) {
    this.OrderProducts = OrderProducts;
  }

  public String getShippingSn() {
    return shippingSn;
  }

  public void setShippingSn(String shippingSn) {
    this.shippingSn = shippingSn;
  }

  public String getShippingStatusStr() {
    return shippingStatusStr;
  }

  public void setShippingStatusStr(String shippingStatusStr) {
    this.shippingStatusStr = shippingStatusStr;
  }

  public String getOrderSn() {
    return orderSn;
  }

  public void setOrderSn(String orderSn) {
    this.orderSn = orderSn;
  }

  public String getShippingTypeName() {
    return shippingTypeName;
  }

  public void setShippingTypeName(String shippingTypeName) {
    this.shippingTypeName = shippingTypeName;
  }
}
