package com.xquark.service.vo;

import org.neo4j.cypher.internal.compiler.v2_1.ast.In;

/**
 * User: kong Date: 2018/7/9. Time: 22:39
 */
public class ShippingGoods {

  //商品id
  private Integer productId;

  //商品编码
  private String productSn;

  //商品索引
  private String productIndex;

  //实际的分摊运费
  private Integer shippingFee;

  public Integer getProductId() {
    return productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public String getProductSn() {
    return productSn;
  }

  public void setProductSn(String productSn) {
    this.productSn = productSn;
  }

  public String getProductIndex() {
    return productIndex;
  }

  public void setProductIndex(String productIndex) {
    this.productIndex = productIndex;
  }

  public Integer getShippingFee() {
    return shippingFee;
  }

  public void setShippingFee(Integer shippingFee) {
    this.shippingFee = shippingFee;
  }

  @Override
  public String toString() {
    return "ShippingGoods{" +
        "productId=" + productId +
        ", productSn='" + productSn + '\'' +
        ", productIndex='" + productIndex + '\'' +
        ", shippingFee=" + shippingFee +
        '}';
  }
}
