package com.xquark.service.vo;

import java.util.List;

/**
 * User: kong Date: 18-7-24. Time: 上午11:06 顺丰订单查询返回结果
 */
public class SFOrderCheck {

  //订单数量
  private Long orderNum;

  //订单明细
  private List<OrderItems> OrderItems;

  public Long getOrderNum() {
    return orderNum;
  }

  public void setOrderNum(Long orderNum) {
    this.orderNum = orderNum;
  }

  public List<OrderItems> getOrderItems() {
    return OrderItems;
  }

  public void setOrderItems(List<OrderItems> OrderItems) {
    this.OrderItems = OrderItems;
  }

  @Override
  public String toString() {
    return "SFOrderCheck{" +
        "orderNum=" + orderNum +
        ", sfOrderItems=" + OrderItems +
        '}';
  }
}
