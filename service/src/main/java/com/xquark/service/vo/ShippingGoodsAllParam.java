package com.xquark.service.vo;

import java.util.List;

/**
 * User: kong Date: 2018/7/9. Time: 22:48
 */
public class ShippingGoodsAllParam {

  private List<ShippingGoodsParam> shippingGoodsParams;

  private Integer userrank;

  public List<ShippingGoodsParam> getShippingGoodsParams() {
    return shippingGoodsParams;
  }

  public void setShippingGoodsParams(
      List<ShippingGoodsParam> shippingGoodsParams) {
    this.shippingGoodsParams = shippingGoodsParams;
  }

  public Integer getUserrank() {
    return userrank;
  }

  public void setUserrank(Integer userrank) {
    this.userrank = userrank;
  }

  @Override
  public String toString() {
    return "ShippingGoodsAllParam{" +
        "shippingGoodsParams=" + shippingGoodsParams +
        ", userrank=" + userrank +
        '}';
  }
}
