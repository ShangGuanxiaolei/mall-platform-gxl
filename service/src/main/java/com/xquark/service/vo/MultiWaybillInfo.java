package com.xquark.service.vo;

import java.util.List;

/**
 * @类名: MultiWaybillInfo
 * @描述: 多条运单信息
 * @程序猿: LuXiaoLing
 * @日期: 2019/2/20 14:31
 * @版本号: V1.0 .
 */
public class MultiWaybillInfo {
    //运单号
    private String wayBillNo;
    //运单物流信息
    private List<SFOrderLog> wayBillInfo ;

    public String getWayBillNo() {
        return wayBillNo;
    }

    public void setWayBillNo(String wayBillNo) {
        this.wayBillNo = wayBillNo;
    }

    public List<SFOrderLog> getWayBillInfo() {
        return wayBillInfo;
    }

    public void setWayBillInfo(List<SFOrderLog> wayBillInfo) {
        this.wayBillInfo = wayBillInfo;
    }
}
