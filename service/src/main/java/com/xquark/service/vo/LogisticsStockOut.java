package com.xquark.service.vo;

/**
 * User: kong Date: 18-7-20. Time: 下午5:31 顺丰库存查询
 */
public class LogisticsStockOut {

  //订单号
  private String orderNo;

  //顺丰区域id
  private Integer sfZoneId;

  //最大可销售库存
  private Integer sfStock;

  //产品id
  private Long productId;

  //产品名称
  private String name;

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public Integer getSfZoneId() {
    return sfZoneId;
  }

  public void setSfZoneId(Integer sfZoneId) {
    this.sfZoneId = sfZoneId;
  }

  public Integer getSfStock() {
    return sfStock;
  }

  public void setSfStock(Integer sfStock) {
    this.sfStock = sfStock;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "LogisticsStockOut{" +
        "orderNo='" + orderNo + '\'' +
        ", sfZoneId=" + sfZoneId +
        ", sfStock=" + sfStock +
        ", productId=" + productId +
        ", name='" + name + '\'' +
        '}';
  }
}
