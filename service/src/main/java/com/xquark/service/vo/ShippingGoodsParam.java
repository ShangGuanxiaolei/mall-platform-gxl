package com.xquark.service.vo;

/**
 * User: kong Date: 2018/7/9. Time: 22:12
 */
public class ShippingGoodsParam {

  //实际售价，单位分
  private Double sellPrice;

  //商品编码
  private String productSn;

  //商品索引
  private String productIndex;

  //商品重量，单位g
  private Double weight;

  //商品编号
  private String merchantNumber;

  //区域id
  private Integer regionId;

  //城市id
  private Integer cityId;

  //商品id
  private Long productId;

  //温控属性
  private String sfshipping;


  public Double getSellPrice() {
    return sellPrice;
  }

  public void setSellPrice(Double sellPrice) {
    this.sellPrice = sellPrice;
  }

  public String getProductSn() {
    return productSn;
  }

  public void setProductSn(String productSn) {
    this.productSn = productSn;
  }

  public String getProductIndex() {
    return productIndex;
  }

  public void setProductIndex(String productIndex) {
    this.productIndex = productIndex;
  }

  public Double getWeight() {
    return weight;
  }

  public void setWeight(Double weight) {
    this.weight = weight;
  }

  public String getMerchantNumber() {
    return merchantNumber;
  }

  public void setMerchantNumber(String merchantNumber) {
    this.merchantNumber = merchantNumber;
  }

  public Integer getRegionId() {
    return regionId;
  }

  public void setRegionId(Integer regionId) {
    this.regionId = regionId;
  }

  public Integer getCityId() {
    return cityId;
  }

  public void setCityId(Integer cityId) {
    this.cityId = cityId;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public String getSfshipping() {
    return sfshipping;
  }

  public void setSfshipping(String sfshipping) {
    this.sfshipping = sfshipping;
  }

  @Override
  public String toString() {
    return "ShippingGoodsParam{" +
        "sellPrice=" + sellPrice +
        ", productSn='" + productSn + '\'' +
        ", productIndex='" + productIndex + '\'' +
        ", weight=" + weight +
        ", merchantNumber='" + merchantNumber + '\'' +
        ", regionId=" + regionId +
        ", cityId=" + cityId +
        ", productId=" + productId +
        ", sfshipping='" + sfshipping + '\'' +
        '}';
  }
}
