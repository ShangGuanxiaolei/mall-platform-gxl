package com.xquark.service.vo;

/**
 * User: kong Date: 2018/7/7. Time: 21:59 顺丰物流返回参数
 */
public class LogisticsStock {

  //商品id
  private Long productId;

  //可售数量
  private Integer saleNum;

  //时间效应
  private Integer shipTime;

  //库存状态
  private Integer stockStatus;

  //仓库id
  private Integer warehouseId;

  //最大可销售数量
  private Integer maxSaleNum;

  //区域id
  private Integer regionId;

  //


  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public Integer getSaleNum() {
    return saleNum;
  }

  public void setSaleNum(Integer saleNum) {
    this.saleNum = saleNum;
  }

  public Integer getShipTime() {
    return shipTime;
  }

  public void setShipTime(Integer shipTime) {
    this.shipTime = shipTime;
  }

  public Integer getStockStatus() {
    return stockStatus;
  }

  public void setStockStatus(Integer stockStatus) {
    this.stockStatus = stockStatus;
  }

  public Integer getWarehouseId() {
    return warehouseId;
  }

  public void setWarehouseId(Integer warehouseId) {
    this.warehouseId = warehouseId;
  }

  public Integer getMaxSaleNum() {
    return maxSaleNum;
  }

  public void setMaxSaleNum(Integer maxSaleNum) {
    this.maxSaleNum = maxSaleNum;
  }

  public Integer getRegionId() {
    return regionId;
  }

  public void setRegionId(Integer regionId) {
    this.regionId = regionId;
  }

  @Override
  public String toString() {
    return "LogisticsStock{" +
        "stock=" + productId +
        ", saleNum=" + saleNum +
        ", shipTime=" + shipTime +
        ", stockStatus=" + stockStatus +
        ", warehouseId=" + warehouseId +
        ", maxSaleNum=" + maxSaleNum +
        ", regionId=" + regionId +
        '}';
  }
}
