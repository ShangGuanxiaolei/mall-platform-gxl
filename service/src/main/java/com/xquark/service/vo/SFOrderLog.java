package com.xquark.service.vo;

/**
 * User: kong Date: 18-7-24. Time: 下午2:23 顺丰订单路由
 */
public class SFOrderLog {

  //操作时间
  private String time;

  //物流信息
  private String commont;

  public String getCommont() {
    return commont;
  }

  public void setCommont(String commont) {
    this.commont = commont;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }
}
