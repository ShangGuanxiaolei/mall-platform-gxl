package com.xquark.service.vo;

/**
 * User: kong Date: 18-7-24. Time: 上午9:40 顺丰子订单信息
 */
public class SubOrder {

  //子订单id
  private Long orderId;

  //子订单编号
  private String orderSn;

  //子订单状态
  private String orderStatus;

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public String getOrderSn() {
    return orderSn;
  }

  public void setOrderSn(String orderSn) {
    this.orderSn = orderSn;
  }

  public String getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(String orderStatus) {
    this.orderStatus = orderStatus;
  }

  @Override
  public String toString() {
    return "SubOrder{" +
        "orderId=" + orderId +
        ", orderSn='" + orderSn + '\'' +
        ", orderStatus='" + orderStatus + '\'' +
        '}';
  }
}
