package com.xquark.service.vo;

import java.util.List;

/**
 * User: kong Date: 2018/7/9. Time: 22:50
 */
public class ShippingGoodsAll {

  private Integer totalshippingfee;

  private Integer selfshippingfee;

  private Integer busishippingfee;

  private List<ShippingGoods> productlist;

  public Integer getTotalshippingfee() {
    return totalshippingfee;
  }

  public void setTotalshippingfee(Integer totalshippingfee) {
    this.totalshippingfee = totalshippingfee;
  }

  public Integer getSelfshippingfee() {
    return selfshippingfee;
  }

  public void setSelfshippingfee(Integer selfshippingfee) {
    this.selfshippingfee = selfshippingfee;
  }

  public Integer getBusishippingfee() {
    return busishippingfee;
  }

  public void setBusishippingfee(Integer busishippingfee) {
    this.busishippingfee = busishippingfee;
  }

  public List<ShippingGoods> getProductlist() {
    return productlist;
  }

  public void setProductlist(List<ShippingGoods> productlist) {
    this.productlist = productlist;
  }

  @Override
  public String toString() {
    return "ShippingGoodsAll{" +
        "totalshippingfee=" + totalshippingfee +
        ", selfshippingfee=" + selfshippingfee +
        ", busishippingfee=" + busishippingfee +
        ", productlist=" + productlist +
        '}';
  }
}
