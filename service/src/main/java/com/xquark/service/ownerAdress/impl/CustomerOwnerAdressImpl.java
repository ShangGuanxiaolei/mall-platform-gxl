package com.xquark.service.ownerAdress.impl;

import com.xquark.dal.mapper.CustomerProfileMapper;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.mapper.SystemRegionMapper;
import com.xquark.dal.mapper.UserMapper;
import com.xquark.dal.model.CustomerProfile;
import com.xquark.dal.model.Order;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.OrderSortType;
import com.xquark.service.ownerAdress.CustomerOwnerAdress;
import com.xquark.service.platform.CareerLevelService;
import com.xquark.service.platform.CustomerProfileService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerOwnerAdressImpl implements CustomerOwnerAdress {

    private static final Log logger = LogFactory.getLog(CustomerOwnerAdressImpl.class);

    @Autowired
    private CustomerProfileMapper customerProfileMapper;
    @Autowired
    private CareerLevelService careerLevelService;
    @Autowired
    private SystemRegionMapper systemRegionMapper;
    @Autowired
    private CustomerProfileService customerProfileService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public void setOwnerAdress(String buyerId, String orderNo, Long cpId){
        try {
            if (null == cpId || 0 == cpId){
                cpId = userMapper.selectCpIdNotEncodingByUserId(buyerId);
                //cpId = userService.selectCpIdByUserId(buyerId);

            }
            //1.判断是否白人
            boolean isWhite = this.isWhite(cpId);
            if(isWhite){
                //2.判断是否是vip套餐
                boolean isVipRight = this.isVipRight(orderNo);
                if (isVipRight){//是
                    this.updateOwnerAdress(orderNo,buyerId,cpId);
                }else {
                    //3.不是VIP套餐，校验是否是首单
                    String firstOder = this.getFirstOder(buyerId);
                    boolean isWhiteUpdate = this.isWhiteCanUpdate(cpId, buyerId);
                    if (StringUtils.isNotBlank(firstOder) && isWhiteUpdate){//是首单且非vip
                         this.updateOwnerAdress(firstOder,buyerId,cpId);
                    }
                }
            }
        }catch (Exception e){
            logger.error("设置归属城市和归属店铺异常"+ e.getMessage());
        }
    }

    /**
     * 是否是首单
     * @param buyerId
     * @return
     */
    private String getFirstOder(String buyerId) {

        // 支付时间升序，且支付成功（PAID，PAIDNOSTOCK，DELIVERY,SUCCESS......），ORDER_TYPE = VIP_RIGHT, buyerId=? limit 1
        // 判空null，退出
        // if Order.orderNo == orderNo 为首单
        // 有身份或者在汉薇下过单为首单
        String firstOrderNo = orderMapper.hasSucceedPay4Hv(buyerId);
        if (StringUtils.isNotBlank(firstOrderNo)){
            return firstOrderNo;
        }
        return null
                ;
    }

    /**
     * 是否是vip套餐
     * @param orderNo
     * @return
     * @throws Exception
     */
    private boolean isVipRight(String orderNo) throws Exception{
        Order order = orderMapper.selectByOrderNo(orderNo);
        if (OrderSortType.VIP_RIGHT.equals(order.getOrderType())) {
            return true;
        }
        return false;
    }

    /**
     * 判断是否是白人
     * @param cpId
     * @return
     */
    private boolean isWhite(Long cpId){
        return !customerProfileService.hasIdentity(cpId);
    }

    /**
     * HV平台设置归属城市和归属店铺
     * @param orderNo
     * @param buyerId
     * @param cpId
     */
    private void updateOwnerAdress(String orderNo, String buyerId,Long cpId){
        //1.查询归属城市
        String homeCityId = "";
        //1.1 正常关联查询
        homeCityId = this.systemRegionMapper.selectHomeCityIdByOrderNo(orderNo);
        //1.2 若是省直辖县
        if(StringUtils.isBlank(homeCityId)){
            homeCityId = this.systemRegionMapper.selectHomeCityIdByOrderNoAndParentId(orderNo);
        }
        if (StringUtils.isNotBlank(homeCityId)){
            //2.查询归属店铺
            String storeId = this.systemRegionMapper.selectStoreIdByHomeCityId(homeCityId);
            if (StringUtils.isNotBlank(storeId)){
                //3.更新归属店铺和归属城市
                CustomerProfile cp = new CustomerProfile();
                cp.setCpId(cpId);
                cp.setRegionId(Long.parseLong(homeCityId));
                cp.setStoreId(storeId);
                int i = this.customerProfileMapper.updateByPrimaryKeySelective(cp);
                logger.info(buyerId + "：更新归属店铺和归属城市，更新" + i + "条");
            }
        }
    }

    private boolean isWhiteCanUpdate(Long cpId, String buyerId){
        //过滤vip套餐在定时任务执行期间下非vip套餐
        List<Order> orders = orderMapper.selectByTypeAndBuyerId(buyerId,"VIP_RIGHT");
        if (orders.isEmpty()) {
            return true;
        }
        for (Order order : orders) {
            // 是否有vip套餐
            if (OrderStatus.PAID.equals(order.getStatus()) ||
                    OrderStatus.SUCCESS.equals(order.getStatus()) ||
                    OrderStatus.SHIPPED.equals(order.getStatus()) ||
                    OrderStatus.DELIVERY.equals(order.getStatus())) {
                return false;
            }
        }
        //过滤是否成为vip
        return !this.careerLevelService.isVip(cpId);
    }
}
