package com.xquark.service.ownerAdress;

public interface CustomerOwnerAdress {

    /**
     *设置归属城市和归属店铺
     * @param buyerId 注册用户ID
     * @param orderNo 交易订单号
     * @param sellerId  卖家ID
     */
    void setOwnerAdress(String buyerId, String orderNo, Long cpId);
}
