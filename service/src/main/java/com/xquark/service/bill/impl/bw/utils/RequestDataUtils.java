package com.xquark.service.bill.impl.bw.utils;

import com.alibaba.fastjson.JSONObject;
import com.xquark.service.bill.impl.bw.FieldConstants;
import java.util.Date;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * User: huangjie
 * Date: 2018/5/22.
 * Time: 下午4:48
 * 工具类:获取符合百望规范的json数据对象缓存，每个对象是构成最终请求体的一部分
 */
@Component
public class RequestDataUtils {

    /**
     * 企业在百望的appid
     */
    @Value("${bill.electronic.appId}")
    private String appId;


    /**
     * 请求流水号的前缀
     */
    @Value("${bill.electronic.prefix}")
    private String reqPrefix;

    /**
     * 销售方的纳税识别号
     */
    @Value("${bill.electronic.seller.tax.code}")
    private String sellerTaxCode;

    /**
     * 销售方的名称
     */
    @Value("${bill.electronic.seller.name}")
    private String sellerName;

    /**
     * 开票人
     */
    @Value("${bill.electronic.seller.drawer}")
    private String drawer;

    /**
     * 销售方的地址和电话
     */
    @Value("${bill.electronic.seller.address}")
    private String sellerAddress;

    /**
     * 销售方的银行账号
     */
    @Value("${bill.electronic.seller.bank}")
    private String sellerBank;

    private final static String REQUEST_CODE = "DZFPQZ";
    private final static String RESPONSE_CODE = "DS";

    private volatile JSONObject globalInfo;
    private volatile JSONObject data;
    private volatile JSONObject fpkj;
    private  JSONObject returnBody=JSONObject.parseObject("{\"returnCode\":\"\",\"returnMessage\":\"\" }");

    /**
     * 获取globalInfo数据模板
     *
     * @return jsonObject
     */
    public JSONObject getGlobalInfo() {
        if (globalInfo == null) {
            globalInfo = new JSONObject();
            globalInfo.put("appId", appId);
            globalInfo.put("interfaceId", "");
            globalInfo.put("interfaceCode", Utils.dfxj1001);
            globalInfo.put("requestCode", REQUEST_CODE);
            globalInfo.put("responseCode", RESPONSE_CODE);
        }
        JSONObject ret =(JSONObject) globalInfo.clone();
        //生成一个随机的处理流水号
        ret.put("requestTime", new Date().toString());
        ret.put(FieldConstants.REQEUST_EXCHANGE_ID,REQUEST_CODE+Utils.dfxj1001+Utils.formatToDay()+Utils.randNineData());
        return ret;
    }


    /**
     * 获取data数据模板
     *
     * @return jsonObject
     */
    public JSONObject getData() {
        if (data == null) {
            data = new JSONObject();
            data.put("dataDescription",JSONObject.parseObject("{\"zipcode\":\"0\"}"));
        }
        return (JSONObject) data.clone();
    }


    /**
     * 获取returnStateInfo数据模板
     *
     * @return jsonObject
     */
    public JSONObject getReturnStateInfo() {
        return (JSONObject) returnBody.clone();
    }


    /**
     * 获取发票开具数据模板
     * @return jsonObject
     */
    public JSONObject getFPKJ(){
        if (fpkj == null) {
            fpkj = new JSONObject();
            fpkj.put("ZSFS","0");
            fpkj.put("KPLX","0");
            fpkj.put("XSF_NSRSBH",sellerTaxCode);
            fpkj.put("XSF_MC",sellerName);
            fpkj.put("XSF_DZDH",sellerAddress);
            fpkj.put("XSF_YHZH",sellerBank);
            fpkj.put("KPR",drawer);
            fpkj.put("SKR","");
            fpkj.put("FHR","");
            fpkj.put("HYLX","0");
            fpkj.put("TSPZ","00");
        }
        JSONObject ret= (JSONObject) fpkj.clone();
        //设置开票流水号
        ret.put(FieldConstants.BILL_EXCHANGE_ID,reqPrefix+Utils.formatToTime() + "01");
        return ret;

    }
}
