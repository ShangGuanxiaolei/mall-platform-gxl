package com.xquark.service.bill.impl.bw;

/**
 * User: huangjie
 * Date: 2018/5/23.
 * Time: 上午10:25
 * 常量
 */
public class FieldConstants {

     /**
     * 全局信息
     */
    final static String GLOBAL_INFO = "globalInfo";

    /**
     * 数据
     */
    final static String DATA = "Data";


    /**
     * 正文
     */
    final static String CONTENT = "content";

    /**
     * 正文摘要
     */
    final static String CONTENT_KEY = "contentKey";

    /**
     * 订单总金额
     */
    final static String ORDER_TOTAL_FEE = "HJJE";

    /**
     * 价税合计
     */
    final static String ORDER_TOTAL_FEE_PLUS_TAX = "JSHJ";

    /**
     * 合计税额
     */
    final static String ORDER_TOTAL_TAX = "HJSE";

    /***
     * 请求正文
     */
    final static String REQEUST_BODY = "REQUEST_COMMON_FPKJ";


    /***
     * 数据交换请求流水号
     */
    final public static String REQEUST_EXCHANGE_ID = "dataExchangeId";

    /***
     * 开票请求流水号
     */
    final public static String BILL_EXCHANGE_ID = "FPQQLSH";

    /***
     * 发票代码
     */
    final public static String BILL_CODE = "FP_DM";

    /***
     * 发票号码
     */
    final public static String BILL_NUMBER = "FP_HM";

    /***
     * 发票校验码
     */
    final public static String BILL_VALIDATE_CODE = "JYM";


    /***
     * 发票pdf地址
     */
    final public static String BILL_PDF_URL = "PDF_URL";


    /***
     * 收票地址
     */
    final public static String BILL_RECEIVE_URL = "SP_URL";

    /***
     * 开票时间
     */
    final public static String BILL_DATE = "KPRQ";

    /**
     * 开具请求中的商品集合
     */
    final static String BILL_PRODUCT_COLLECTION = "COMMON_FPKJ_XMXXS";

    /**
     * 开票行性质
     */
    final static String BILL_COMPANY_TYPE = "FPHXZ";

    /**
     * 自行商品编码
     */
    final static String DIY_PRODUCT_CODE = "ZXBM";


    /**
     * 商品编码
     */
    final static String PRODUCT_CODE = "SPBM";


    /**
     * 项目名称
     */
    final static String PRODUCT_NAME = "XMMC";

    /**
     * 规格型号
     */
    final static String PRODUCT_SKU = "GGXH";

    /**
     * 单位
     */
    final static String PRODUCT_DW = "DW";

    /**
     * 商品数量
     */
    final static String PRODUCT_AMOUNT = "XMSL";

    /**
     * 商品单价
     */
    final static String PRODUCT_PRICE = "XMDJ";

    /**
     * 商品金额
     */
    final static String PRODUCT_JE = "XMJE";


    /**
     * json对象入口属性
     */
    final static String DATA_INTERFACE = "interface";

    /**
     * 税率
     */
    final static String PRODUCT_TAX_PERCENT = "SL";

    /**
     * 税额
     */
    final static String PRODUCT_TAX = "SE";

    /**
     * 开具请求中的商品项目
     */
    final static String BILL_PRODUCT_COLLECTION_ITEM = "COMMON_FPKJ_XMXX";

    /**
     * 购买方手机号
     */
    final static String RECEIVE_PHONE = "GMF_SJH";

    /**
     * 购买方邮箱
     */
    final static String RECEIVE_MAIL = "GMF_DZYX";


    /**
     * 购买方名称
     */
    final static String RECEIVE_NAME = "GMF_MC";

    /**
     * 购买方纳税人识别号
     */
    final static String BUYER_TAX_NUMBER="GMF_NSRSBH";

    /***
     * 返回信息
     */
    final static String RETURN_INFO="returnStateInfo" ;


    /***
     * 错误码
     */
    final static String RETURN_CODE_SUCCESS="0000" ;


    /***
     * 备注
     */
    final static String BUYER_COMMENT="BZ" ;

    /***
     * push通道
     */
    final public static String PUSH_CHANNEL = "push";

    /***
     * 短信通道
     */
    final public static String SMS_CHANNEL = "sms";
}
