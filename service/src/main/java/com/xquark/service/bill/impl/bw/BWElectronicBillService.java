package com.xquark.service.bill.impl.bw;

import static com.xquark.service.bill.impl.bw.FieldConstants.BILL_CODE;
import static com.xquark.service.bill.impl.bw.FieldConstants.BILL_EXCHANGE_ID;
import static com.xquark.service.bill.impl.bw.FieldConstants.BILL_NUMBER;
import static com.xquark.service.bill.impl.bw.FieldConstants.BILL_PDF_URL;
import static com.xquark.service.bill.impl.bw.FieldConstants.BILL_PRODUCT_COLLECTION;
import static com.xquark.service.bill.impl.bw.FieldConstants.BILL_RECEIVE_URL;
import static com.xquark.service.bill.impl.bw.FieldConstants.BILL_VALIDATE_CODE;
import static com.xquark.service.bill.impl.bw.FieldConstants.BUYER_TAX_NUMBER;
import static com.xquark.service.bill.impl.bw.FieldConstants.CONTENT;
import static com.xquark.service.bill.impl.bw.FieldConstants.CONTENT_KEY;
import static com.xquark.service.bill.impl.bw.FieldConstants.DATA;
import static com.xquark.service.bill.impl.bw.FieldConstants.DATA_INTERFACE;
import static com.xquark.service.bill.impl.bw.FieldConstants.GLOBAL_INFO;
import static com.xquark.service.bill.impl.bw.FieldConstants.ORDER_TOTAL_FEE;
import static com.xquark.service.bill.impl.bw.FieldConstants.ORDER_TOTAL_FEE_PLUS_TAX;
import static com.xquark.service.bill.impl.bw.FieldConstants.ORDER_TOTAL_TAX;
import static com.xquark.service.bill.impl.bw.FieldConstants.PRODUCT_AMOUNT;
import static com.xquark.service.bill.impl.bw.FieldConstants.PRODUCT_CODE;
import static com.xquark.service.bill.impl.bw.FieldConstants.PRODUCT_DW;
import static com.xquark.service.bill.impl.bw.FieldConstants.PRODUCT_JE;
import static com.xquark.service.bill.impl.bw.FieldConstants.PRODUCT_NAME;
import static com.xquark.service.bill.impl.bw.FieldConstants.PRODUCT_PRICE;
import static com.xquark.service.bill.impl.bw.FieldConstants.PRODUCT_SKU;
import static com.xquark.service.bill.impl.bw.FieldConstants.PRODUCT_TAX;
import static com.xquark.service.bill.impl.bw.FieldConstants.PRODUCT_TAX_PERCENT;
import static com.xquark.service.bill.impl.bw.FieldConstants.RECEIVE_MAIL;
import static com.xquark.service.bill.impl.bw.FieldConstants.RECEIVE_NAME;
import static com.xquark.service.bill.impl.bw.FieldConstants.RECEIVE_PHONE;
import static com.xquark.service.bill.impl.bw.FieldConstants.REQEUST_BODY;
import static com.xquark.service.bill.impl.bw.FieldConstants.RETURN_CODE_SUCCESS;
import static com.xquark.service.bill.impl.bw.FieldConstants.RETURN_INFO;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.xquark.dal.IUser;
import com.xquark.dal.mapper.ElectronicBillResultMapper;
import com.xquark.dal.model.CompanyBill;
import com.xquark.dal.model.ElectronicBillResult;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.PersonalBill;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.service.BaseService;
import com.xquark.service.bill.ElectronicBillService;
import com.xquark.service.bill.impl.bw.utils.BwAesUtils;
import com.xquark.service.bill.impl.bw.utils.RequestDataUtils;
import com.xquark.service.bill.impl.bw.utils.RequestUtils;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.user.UserService;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sun.misc.BASE64Encoder;

/**
 * User: huangjie
 * Date: 2018/5/22.
 * Time: 下午3:09
 * 百望第三方电子发票开票服务
 */
@Service
public class BWElectronicBillService implements ElectronicBillService, BaseService {

    private RequestDataUtils requestDataUtils;

    private ElectronicBillResultMapper electronicBillResultMapper;

    private UserService userService;
    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    private static final Logger LOG= LoggerFactory.getLogger(BWElectronicBillService.class);
    @Autowired
    public void setElectronicBillResultMapper(ElectronicBillResultMapper electronicBillResultMapper) {
        this.electronicBillResultMapper = electronicBillResultMapper;
    }

    @Autowired
    public void setRequestDataUtils(RequestDataUtils requestDataUtils) {
        this.requestDataUtils = requestDataUtils;
    }

    /**
     * 时间的格式
     */
    private static String DATE_Format="yyyyMMddHHmmss";


    /**
     * 商品单位
     */
    private static String DEFAULT_PRODUCT_UNIT = "件";

    /**
     * 默认商品数量
     */
    private static String DEFAULT_PRODUCT_NUM = "1";

    /**
     * 税额
     */
    private static String DEFAULT_TAX = "0.17";


    /**
     * 编码
     */
    private static String CODE="UTF-8";


    /**
     * AES对称加密密码
     */
    @Value("${bill.electronic.aes.password}")
    private String aesPassword;


    /**
     * 开票请求的地址
     */
    @Value("${bill.electronic.request.url}")
    private String requestUrl;

    @Override
    public IUser getCurrentUser() throws BizException {
        return getCurrentUser();
    }


    /**
   * 处理requestBody的回调
   */
  private interface  BillCallBack{
    void callBack(JSONObject obj);
  }


    /***
     * 处理开票处理请求的主流程
     * 1.构造开票请求主体
     * 2.构造订单的概括
     * 3.构造商品明细
     * 4.对正文进行加密
     * 5.发送请求
     * 6.处理响应
     * @param callBack
     */
    private ElectronicBillResult billFlow(MainOrderVO orderDetail, BillCallBack callBack) {
        //构造开票请求主体
        JSONObject req = new JSONObject();
        JSONObject inter=constructBillReqBody();
        req.put(DATA_INTERFACE,inter);
        inter.put(RETURN_INFO,requestDataUtils.getReturnStateInfo());
        //构造订单的概括信息
        JSONObject common=new JSONObject();
        JSONObject fpkj = requestDataUtils.getFPKJ();
        common.put(REQEUST_BODY,fpkj);
        callBack.callBack(fpkj);
        fillBillWithOrderDetail(fpkj,orderDetail.getTotalFee().setScale(2,BigDecimal.ROUND_HALF_EVEN));
        //构造商品明细
        fpkj.put(BILL_PRODUCT_COLLECTION, constructProductDetail(orderDetail));
        //对正文进行加密
        inter.put(DATA, constructEncodedBody(common));
        //调用请求
        LOG.debug("req json:{}",req.toJSONString());
        String ret = RequestUtils.getHttpConnectResult(req.toJSONString(),requestUrl);
        JSONObject retJson = JSONObject.parseObject(ret);
        Objects.requireNonNull(retJson);
        //获取开票结果
        ElectronicBillResult ebr=new ElectronicBillResult();
        ebr.setOrderId(orderDetail.getId());
        ebr.setExchangeId(fpkj.getString(BILL_EXCHANGE_ID));
        dealResult(retJson,ebr);
        return ebr;
    }

    /**
     * 获取开票请求主体
     * @return json对象
     */
    private JSONObject constructBillReqBody(){
        JSONObject inter = new JSONObject();
        JSONObject globalInfo = requestDataUtils.getGlobalInfo();
        inter.put(GLOBAL_INFO, globalInfo);
        return  inter;
    }


    /**
     * 填充订单的基本信息
     * @param fpkj
     */
    private void fillBillWithOrderDetail(JSONObject fpkj, BigDecimal total){
        BigDecimal tax = total.multiply(new BigDecimal(DEFAULT_TAX)).setScale(2,BigDecimal.ROUND_HALF_EVEN);
        fpkj.put(ORDER_TOTAL_FEE, total.toString());
        fpkj.put(ORDER_TOTAL_FEE_PLUS_TAX, total.add(tax).setScale(2,BigDecimal.ROUND_HALF_EVEN).toString());
        fpkj.put(ORDER_TOTAL_TAX, tax.toString());
    }


    /**
     * 根据订单构造商品明细
     * @return json对象
     */
    private JSONObject constructProductDetail(MainOrderVO orderDetail) {
        JSONObject productCollection = new JSONObject();
        List<JSONObject> products = new ArrayList<>();
        productCollection.put(FieldConstants.BILL_PRODUCT_COLLECTION_ITEM, products);
      List<OrderVO> oi = orderDetail.getOrders();
        Objects.requireNonNull(oi);
      for (OrderVO vo : oi) {
        List<OrderItem> items = vo.getOrderItems();
        for (OrderItem o : items) {
          JSONObject product = new JSONObject();
          product.put(FieldConstants.BILL_COMPANY_TYPE, "0");
          //product.put(FieldConstants.DIY_PRODUCT_CODE, o.getProduct().getCode());//传递自定义编码报错，原因不明
          product.put(PRODUCT_NAME, "红高粱"/*o.getProduct().getName()*/);
          product.put(PRODUCT_SKU, "500克"/*o.getSkuStr()*/);
          product.put(PRODUCT_DW, "袋"/*DEFAULT_PRODUCT_UNIT*/);
          product.put(PRODUCT_AMOUNT, o.getAmount().toString());
          product.put(PRODUCT_CODE, "1010101050000000000");//todo 此处为测试商品编号
          product.put(PRODUCT_PRICE, o.getTotalDiscountPrice()
              .setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());
          product.put(PRODUCT_JE, o.getTotalDiscountPrice()
              .setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());
          product.put(PRODUCT_TAX_PERCENT, DEFAULT_TAX);
          BigDecimal se = o.getTotalDiscountPrice().multiply(new BigDecimal(DEFAULT_TAX))
              .setScale(2, BigDecimal.ROUND_HALF_EVEN);
          product.put(PRODUCT_TAX, se.toString());
          products.add(product);
        }
      }
        return productCollection;
    }


    /***
     * 构造加密后的请求正文
     * @param raw
     * @return
     */
    private JSONObject constructEncodedBody(JSONObject raw) {
        //获取json正文，并用base64编码
        LOG.debug("raw content:{}",raw.toJSONString());
      JSONObject data = null;
      try {
        String content = new BASE64Encoder().encodeBuffer(raw.toJSONString().getBytes(CODE))
            .replace("\r\n", "").
                replace("\n", "").replace("\r", "");
        //获取json正文的摘要，并用base64编码
        String contentMD5 = BwAesUtils.MD5(content.getBytes(CODE));
        String contentKey = BwAesUtils
            .encryptBASE64(BwAesUtils.encrypt(contentMD5.getBytes(CODE), aesPassword)).
                replaceAll("\r\n", "").replaceAll("\n", "").replace("\r", "");
        data = requestDataUtils.getData();
        data.put(CONTENT, content);
        data.put(CONTENT_KEY, contentKey);
      } catch (Exception e) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
            "bill electronic service decode or encode fail", e);
      }
        return data;
    }

    /**
     * 处理开票结果
     * @param result
     */
    private void dealResult(JSONObject result, ElectronicBillResult electronicBillResult) {
        JSONObject returnInfo = result.getJSONObject(DATA_INTERFACE).getJSONObject(RETURN_INFO);
        String retCode=returnInfo.getString("returnCode");
        electronicBillResult.setReturnCode(retCode);
        if(retCode.equals(RETURN_CODE_SUCCESS)){
            String encodedContent=returnInfo.getJSONObject(DATA).getString(CONTENT);
          String decodedContent;
          try {
            decodedContent = new String(BwAesUtils.decryptBASE64(encodedContent), CODE);
          } catch (Exception e) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                "decryptBASE64 electronic bill result fail", e);
          }
            Preconditions.checkState(StringUtils.isNotBlank(decodedContent));
            JSONObject retContent = JSONObject.parseObject(decodedContent);
            //把返回的数据封装到实例对象上
            electronicBillResult.setExchangeId(Optional.of(retContent.getString(BILL_EXCHANGE_ID)).get());
            electronicBillResult.setBillCode(Optional.of(retContent.getString(BILL_CODE)).get());
            electronicBillResult.setBillNumber(Optional.of(retContent.getString(BILL_NUMBER)).get());
            electronicBillResult.setValidateCode(Optional.of(retContent.getString(BILL_VALIDATE_CODE)).get());
            electronicBillResult.setPdfUrl(Optional.of(retContent.getString(BILL_PDF_URL)).get());
            electronicBillResult.setReceiveUrl(Optional.of(retContent.getString(BILL_RECEIVE_URL)).get());
            String d=Optional.of(retContent.getString(FieldConstants.BILL_DATE)).get();
            try {
                SimpleDateFormat df = new SimpleDateFormat(DATE_Format);
                electronicBillResult.setDate(df.parse(d));
            } catch (ParseException e) {
                throw new RuntimeException("Failed to parse date: ", e);
            }
            electronicBillResultMapper.insert(electronicBillResult);
        }else{
            electronicBillResultMapper.insertFailBill(electronicBillResult);
        }
    }

    /**
     * 处理个人的开票请求
     * @param bill 开票请求
     * @param orderDetail 订单详情
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(propagation=Propagation.REQUIRES_NEW)
    public ElectronicBillResult makePersonalBill(final PersonalBill bill,
        final MainOrderVO orderDetail) {
        return billFlow(orderDetail, new BillCallBack() {
            @Override
            public void callBack(JSONObject obj) {
                obj.put(RECEIVE_PHONE, bill.getReceivePhone());
                obj.put(RECEIVE_MAIL, bill.getReceiveMail());
                obj.put(RECEIVE_NAME,userService.load(orderDetail.getBuyerId()).getName());
            }
        });
    }

    /***
     * 处理公司的电子开票请求
     * @param bill 开票请求
     * @param orderDetail 订单详情
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(propagation=Propagation.REQUIRES_NEW)
    public ElectronicBillResult makeCompanyBill(final CompanyBill bill,
        final MainOrderVO orderDetail) {
        return billFlow(orderDetail, new BillCallBack() {
            @Override
            public void callBack(JSONObject obj) {
                obj.put(RECEIVE_NAME,userService.load(orderDetail.getBuyerId()).getName());
                obj.put(BUYER_TAX_NUMBER, bill.getIdentifyNumber());
                obj.put(RECEIVE_PHONE, bill.getReceivePhone());
                obj.put(RECEIVE_MAIL, bill.getReceiveMail());
                obj.put(FieldConstants.BUYER_COMMENT,Optional.fromNullable(bill.getProductDetail()).or(""));
            }
        });
    }
}
