package com.xquark.service.bill;

import com.xquark.dal.model.AddedTaxBill;
import com.xquark.dal.model.CompanyBill;
import com.xquark.dal.model.PersonalBill;

/**
 * Created by IntelliJ IDEA.
 * User: huangjie
 * Date: 2018/5/22.
 * Time: 下午2:45
 * 处理开票请求的数据服务
 */
public interface BillService {

    /**
     * 处理个人的开票请求
     * @param bill 开票请求
     */
    void makePersonalBill(PersonalBill bill);


    /**
     * 处理公司的开票请求
     * @param bill 开票请求
     */
    void makeCompanyBill(CompanyBill bill);

    /**
     * 处理增值税的开票请求
     * @param bill 开票请求
     */
    void makeAddedTaxBill(AddedTaxBill bill);
}
