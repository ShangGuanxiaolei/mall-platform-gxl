package com.xquark.service.bill.impl;

import com.xquark.aop.anno.NotNull;
import com.xquark.dal.mapper.AddedTaxBillMapper;
import com.xquark.dal.mapper.CompanyBillMapper;
import com.xquark.dal.mapper.PersonalBillMapper;
import com.xquark.dal.model.AddedTaxBill;
import com.xquark.dal.model.CompanyBill;
import com.xquark.dal.model.ElectronicBillResult;
import com.xquark.dal.model.PersonalBill;
import com.xquark.dal.type.BillType;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.service.bill.BillService;
import com.xquark.service.bill.ElectronicBillService;
import com.xquark.service.mail.MailHeader;
import com.xquark.service.mail.MailSender;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderService;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: huangjie Date: 2018/5/22. Time: 下午3:07 汉德森开票服务
 */
@Service
@Transactional
public class HdsBillService implements BillService, ApplicationContextAware {

  @Value("${mailServer.username}")
  private String MAIL_ACCOUNT;

  private String Mail_SUBJECT = "测试";

  /**
   * 邮件头部
   */
  private MailHeader mailHeader = new MailHeader();

  /**
   * 财务的邮箱
   */
  @Value("${bill.electronic.email}")
  private String financialEmail;

  /**
   * 发给财务的邮件内容模板
   */
  private static String EMAIL_TEMPLATE = "bill";

  /**
   * 个人发票数据访问mapper
   */
  private PersonalBillMapper personalBillMapper;

  /**
   * 公司发票数据访问mapper
   */
  private CompanyBillMapper companyBillMapper;

  /**
   * 增值税发票数据访问mapper
   */
  private AddedTaxBillMapper addedTaxBillMapper;


  /**
   * 发送邮件
   */
  private MailSender mailSender;

  @Autowired
  public void setMailSender(MailSender mailSender) {
    this.mailSender = mailSender;
  }

  /**
   * 订单服务
   */
  private OrderService orderService;

  private MainOrderService mainOrderService;

  @Autowired
  public void setMainOrderService(MainOrderService mainOrderService) {
    this.mainOrderService = mainOrderService;
  }

  /**
   * 电子发票开票服务
   */
  private ElectronicBillService electronicBillService;

  @Autowired
  public void setElectronicBillService(ElectronicBillService electronicBillService) {
    this.electronicBillService = electronicBillService;
  }

  @Autowired
  public void setOrderService(OrderService orderService) {
    this.orderService = orderService;
  }

  @Autowired
  public void setAddedTaxBillMapper(AddedTaxBillMapper addedTaxBillMapper) {
    this.addedTaxBillMapper = addedTaxBillMapper;
  }


  @Autowired
  public void setPersonalMapper(PersonalBillMapper personalMapper) {
    this.personalBillMapper = personalMapper;
  }

  @Autowired
  public void setCompanyBillMapper(CompanyBillMapper companyBillMapper) {
    this.companyBillMapper = companyBillMapper;
  }

  /**
   * 处理个人开票请求
   *
   * @param bill 开票请求
   */
  @Override
  public void makePersonalBill(@NotNull PersonalBill bill) {
    personalBillMapper.insert(bill);
    MainOrderVO ov = mainOrderService.loadVO(bill.getOrderId());
    //准备邮件渲染的内容
    Map<String, Object> params = new HashMap<>();
    if (bill.getType().equals(BillType.ELECTRONIC)) {
      //调用第三方接口开电子发票
      ElectronicBillResult result = electronicBillService.makePersonalBill(bill, ov);
    }
    //发送邮件给财务
    mailSender.sendMessage(mailHeader, EMAIL_TEMPLATE, params);
  }

  /**
   * 处理公司开票请求
   *
   * @param bill 开票请求
   */
  @Override
  public void makeCompanyBill(@NotNull CompanyBill bill) {
    companyBillMapper.insert(bill);
    MainOrderVO ov = mainOrderService.loadVO(bill.getOrderId());
    Map<String, Object> params = new HashMap<>();
    if (bill.getType().equals(BillType.ELECTRONIC)) {
      ElectronicBillResult result = electronicBillService.makeCompanyBill(bill, ov);
    }
    mailSender.sendMessage(mailHeader, EMAIL_TEMPLATE, params);
  }

  /**
   * 处理增值税开票请求
   *
   * @param bill 开票请求
   */
  @Override
  public void makeAddedTaxBill(@NotNull AddedTaxBill bill) {
    Map<String, Object> params = new HashMap<>();
    addedTaxBillMapper.insert(bill);
    mailSender.sendMessage(mailHeader, EMAIL_TEMPLATE, params);
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    mailHeader.setFrom(MAIL_ACCOUNT);
    mailHeader.setTo(new String[]{financialEmail});
    mailHeader.setSubject(Mail_SUBJECT);
  }
}
