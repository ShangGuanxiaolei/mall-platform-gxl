package com.xquark.service.bill;

import com.xquark.dal.model.CompanyBill;
import com.xquark.dal.model.ElectronicBillResult;
import com.xquark.dal.model.PersonalBill;
import com.xquark.dal.vo.MainOrderVO;

/**
 * Created by IntelliJ IDEA.
 * User: huangjie
 * Date: 2018/5/22.
 * Time: 下午2:51
 * 第三方电子发票接口
 */
public interface ElectronicBillService {


    /**
     * 开具个人电子发票
     * @param bill 开票请求
     * @param orderDetail 订单详情
     * @return 电子发票开票结果
     */
    ElectronicBillResult makePersonalBill(PersonalBill bill, MainOrderVO orderDetail);

    /***
     * 开具公司发票
     * @param bill 开票请求
     * @param orderDetail 订单详情
     * @return 电子发票开票结果
     */
    ElectronicBillResult makeCompanyBill(CompanyBill bill, MainOrderVO orderDetail);

}
