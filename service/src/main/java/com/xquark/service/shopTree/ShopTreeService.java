package com.xquark.service.shopTree;


import com.xquark.dal.model.ShopTree;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.vo.TwitterChildrenCmDisplayVO;
import com.xquark.dal.vo.TwitterChildrenCmVO;
import com.xquark.service.BaseEntityService;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface ShopTreeService extends BaseEntityService<ShopTree> {

  public int insertShopTree(ShopTree shopTree);

  public int insertRootShopTree(String shopId);

  public ShopTree selectRootShopByShopId(String shopId);

  public Long selectParentUserIdByShopId(String shopId, Integer shopDepth);

  public List<ShopTree> selectChildren(String shopId);

  public List<ShopTree> getParents(String shopId);

  public Long countDirectChildren(String rootShopId, String currentShopId);

  public Long countChildren(String rootShopId, String currentShopId);

  public List<ShopTree> listDirectChildren(String rootShopId, String currentShopId);

  public List<ShopTree> listDirectChildrenPage(String rootShopId, String currentShopId,
      Pageable pageable);

  public List<ShopTree> listChildren(String rootShopId, String currentShopId);

  public List<ShopTree> listChildrenPage(String rootShopId, String currentShopId,
      Pageable pageable);

  public List<TwitterChildrenCmVO> selectChildrenCmByRootIdAndAccountType(String shopId,
      AccountType accountType);

  public List<TwitterChildrenCmDisplayVO> selectChildrenCmByRootId(String shopId,
      Map<String, Object> params, @Param("page") Pageable pageable);

  public Long countChildrenCmByRootId(String shopId, Map<String, Object> params);

  public List<ShopTree> listDirectChildren4Admin(String rootShopId, String currentShopId,
      Map<String, Object> params, Pageable pageable);

  public List<ShopTree> listChildren4Admin(String rootShopId, String currentShopId,
      Map<String, Object> params, Pageable pageable);

  public int updateParentShop(String rootShopId, String newParentShopId, String shopId);

  public int updateParentShopDepth(String rootShopId, String newParentShopId, String shopId);

  public ShopTree selectDirectParent(String rootShopId, String shopId);

  public boolean changeParent(String rootShopId, String newParentShopId, String shopId);

  public List<ShopTree> listChildrenExceptShopList(String rootShopId, String currentShopId,
      List shopIds);

  public List<ShopTree> listRootShopTree();

  int deleteByDescendant(String shopId);

  /**
   * b2b代理用户树状图显示
   */
  List<ShopTree> listDirectChildrenAgent(String rootShopId, String currentShopId,
      Pageable pageable);

  /**
   * b2b代理用户树状图显示
   */
  Long countChildrenAgent(String rootShopId, String currentShopId);

  /**
   * 根据手机号或名称查询该shop的级层关系
   */
  List<ShopTree> listByPhoneOrName(String rootShopId, String keys);

  /**
   * 获取总店店铺
   */
  ShopTree getRootShop();

}
