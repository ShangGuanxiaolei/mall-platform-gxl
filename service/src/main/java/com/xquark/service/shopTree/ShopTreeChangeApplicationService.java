package com.xquark.service.shopTree;

import com.xquark.dal.model.ShopTreeChangeApplication;
import com.xquark.dal.model.UserTwitter;
import com.xquark.dal.vo.UserTwitterApplyVO;
import com.xquark.service.ArchivableEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface ShopTreeChangeApplicationService extends
    ArchivableEntityService<ShopTreeChangeApplication> {

  /**
   * ShopTreeChangeApplication
   */
  int create(ShopTreeChangeApplication shopTreeChangeApplication);

  /**
   * ShopTreeChangeApplication
   */
  int update(ShopTreeChangeApplication shopTreeChangeApplication);

  /**
   * 通过 id ShopTreeChangeApplication
   */
  ShopTreeChangeApplication load(String id);


  List<ShopTreeChangeApplication> selectByOwnShopIdApplying(String shopId);
}
