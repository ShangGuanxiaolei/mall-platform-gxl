package com.xquark.service.shopTree.impl;


import com.xquark.dal.mapper.ShopTreeChangeApplicationMapper;
import com.xquark.dal.model.ShopTreeChangeApplication;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.shopTree.ShopTreeChangeApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("shopTreeChangeApplicationService")
public class ShopTreeChangeApplicationServiceImpl extends BaseServiceImpl implements
    ShopTreeChangeApplicationService {

  @Autowired
  private ShopTreeChangeApplicationMapper shopTreeChangeApplicationMapper;


  @Override
  public int create(ShopTreeChangeApplication shopTreeChangeApplication) {
    return shopTreeChangeApplicationMapper.insert(shopTreeChangeApplication);
  }

  @Override
  public int update(ShopTreeChangeApplication shopTreeChangeApplication) {
    return shopTreeChangeApplicationMapper.updateByPrimaryKey(shopTreeChangeApplication);
  }

  @Override
  public int insert(ShopTreeChangeApplication shopTreeChangeApplication) {
    return shopTreeChangeApplicationMapper.insert(shopTreeChangeApplication);
  }

  @Override
  public int insertOrder(ShopTreeChangeApplication shopTreeChangeApplication) {
    return shopTreeChangeApplicationMapper.insert(shopTreeChangeApplication);
  }

  @Override
  public ShopTreeChangeApplication load(String id) {
    return shopTreeChangeApplicationMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<ShopTreeChangeApplication> selectByOwnShopIdApplying(String shopId) {
    return shopTreeChangeApplicationMapper.selectByOwnShopIdApplying(shopId);
  }

  @Override
  public int delete(String id) {
    return shopTreeChangeApplicationMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int undelete(String id) {
    return shopTreeChangeApplicationMapper.unDeleteByPrimaryKey(id);
  }
}
