package com.xquark.service.shopTree.impl;

import com.xquark.dal.mapper.ShopMapper;
import com.xquark.dal.mapper.ShopTreeMapper;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.vo.TwitterChildrenCmDisplayVO;
import com.xquark.dal.vo.TwitterChildrenCmVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.shopTree.ShopTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("shopTreeService")
public class ShopTreeServiceImpl extends BaseServiceImpl implements ShopTreeService {

  @Autowired
  private ShopTreeMapper shopTreeMapper;
  @Autowired
  private ShopMapper shopMapper;


  @Override
  public int insert(ShopTree shopTree) {
    return 0;
  }

  @Override
  public int insertOrder(ShopTree shopTree) {
    return 0;
  }

  @Override
  public ShopTree load(String id) {
    return null;
  }

  public int insertShopTree(ShopTree shopTree) {
    return shopTreeMapper.insertShopTree(shopTree);
  }

  @Override
  public int insertRootShopTree(String shopId) {
    ShopTree shopTree = new ShopTree();
    shopTree.setDescendantShopId(shopId);
    shopTree.setAncestorShopId(shopId);
    shopTree.setRootShopId(shopId);
    shopTree.setShopDepth(0);
    return 0;
  }

  @Override
  public ShopTree selectRootShopByShopId(String shopId) {
    return shopTreeMapper.selectRootShopByShopId(shopId);
  }

  @Override
  public Long selectParentUserIdByShopId(String shopId, Integer shopDepth) {
    return shopTreeMapper.selectParentUserIdByShopId(shopId, shopDepth);
  }

  @Override
  public List<ShopTree> selectChildren(String shopId) {
    return shopTreeMapper.selectChildren(shopId);
  }

  @Override
  public List<ShopTree> getParents(String shopId) {
    return shopTreeMapper.getParents(shopId);
  }

  @Override
  public Long countDirectChildren(String rootShopId, String currentShopId) {
    return shopTreeMapper.countDirectChildren(rootShopId, currentShopId);
  }

  @Override
  public Long countChildren(String rootShopId, String currentShopId) {
    return shopTreeMapper.countChildren(rootShopId, currentShopId);
  }

  @Override
  public List<ShopTree> listDirectChildren(String rootShopId, String currentShopId) {
    return shopTreeMapper.listDirectChildren(rootShopId, currentShopId, null);
  }

  @Override
  public List<ShopTree> listDirectChildrenPage(String rootShopId, String currentShopId,
      Pageable pageable) {
    return shopTreeMapper.listDirectChildren(rootShopId, currentShopId, pageable);
  }

  @Override
  public List<ShopTree> listChildren(String rootShopId, String currentShopId) {
    return shopTreeMapper.listChildren(rootShopId, currentShopId);
  }

  @Override
  public List<ShopTree> listChildrenPage(String rootShopId, String currentShopId,
      Pageable pageable) {
    return shopTreeMapper.listChildren(rootShopId, currentShopId);
  }

  @Override
  public List<TwitterChildrenCmVO> selectChildrenCmByRootIdAndAccountType(String shopId,
      AccountType accountType) {
    return shopTreeMapper.selectChildrenCmByRootIdAndAccountType(shopId, accountType);
  }

  @Override
  public List<TwitterChildrenCmDisplayVO> selectChildrenCmByRootId(String shopId,
      Map<String, Object> params, Pageable pageable) {
    return shopTreeMapper.selectChildrenCmByRootId(shopId, params, pageable);
  }

  @Override
  public Long countChildrenCmByRootId(String shopId, Map<String, Object> params) {
    return shopTreeMapper.countChildrenCmByRootId(shopId, params);
  }

  @Override
  public List<ShopTree> listDirectChildren4Admin(String rootShopId, String currentShopId,
      Map<String, Object> params, Pageable pageable) {
    return shopTreeMapper.listDirectChildren4Admin(rootShopId, currentShopId, null, pageable);
  }

  @Override
  public List<ShopTree> listChildren4Admin(String rootShopId, String currentShopId,
      Map<String, Object> params, Pageable pageable) {
    return shopTreeMapper.listChildren4Admin(rootShopId, currentShopId, params, pageable);
  }

  @Override
  public int updateParentShop(String rootShopId, String newParentShopId, String shopId) {
    return shopTreeMapper.updateParentShop(rootShopId, newParentShopId, shopId);
  }

  @Override
  public int updateParentShopDepth(String rootShopId, String newParentShopId, String shopId) {
    return shopTreeMapper.updateParentShopDepth(rootShopId, newParentShopId, shopId);
  }

  @Override
  public ShopTree selectDirectParent(String rootShopId, String shopId) {
    return shopTreeMapper.selectDirectParent(rootShopId, shopId);
  }

  @Override
  public boolean changeParent(String rootShopId, String newParentShopId, String shopId) {
    //获得待变更上级节点以及所有下级并且以深度升序排序
    List<ShopTree> listShopTree = shopTreeMapper.listChildrenIncludingItself(rootShopId, shopId);
    //删除待变更节点以及所有下级与其上级的关系
    for (ShopTree shopTree : listShopTree) {
      String currentShopId = shopTree.getDescendantShopId();
      // 不能将节点移动到该节点的子节点中
      if (currentShopId.equals(newParentShopId)) {
        throw new BizException(GlobalErrorCode.UNKNOWN, "不能将代理移动到该代理的下级代理中");
      }

      int depth = shopTree.getShopDepth();
      List<ShopTree> listParentShopTree = shopTreeMapper
          .listParentIncludingItselfDepth(rootShopId, currentShopId, depth);

      for (ShopTree parentShopTree : listParentShopTree) {
        String id = parentShopTree.getId();
        shopTreeMapper.trueDeleteByPrimaryKey(id);
      }
    }
    //顺序重新建立与新节点的关系
    for (ShopTree shopTree : listShopTree) {
      if (shopTree.getShopDepth().compareTo(0) == 0) {
        shopTree.setAncestorShopId(newParentShopId);
        break;
      }
    }
    for (ShopTree shopTree : listShopTree) {
      if (shopTree.getShopDepth().compareTo(0) == 0) {
        shopTreeMapper.insertShopTree(shopTree);
      } else {
        shopTreeMapper.insertShopTreeDepth(shopTree);
      }
    }
    return true;
  }

  @Override
  public List<ShopTree> listChildrenExceptShopList(String rootShopId, String currentShopId,
      List shopIds) {
    return shopTreeMapper.listChildrenExceptShopList(rootShopId, currentShopId, shopIds);
  }

  @Override
  public List<ShopTree> listRootShopTree() {
    return shopTreeMapper.listRootShopTree();
  }

  @Override
  public int deleteByDescendant(String shopId) {
    return shopTreeMapper.deleteByDescendant(shopId);
  }

  /**
   * b2b代理用户树状图显示
   */
  @Override
  public List<ShopTree> listDirectChildrenAgent(String rootShopId, String currentShopId,
      Pageable pageable) {
    return shopTreeMapper.listDirectChildrenAgent(rootShopId, currentShopId, pageable);
  }

  /**
   * b2b代理用户树状图显示
   */
  @Override
  public Long countChildrenAgent(String rootShopId, String currentShopId) {
    return shopTreeMapper.countChildrenAgent(rootShopId, currentShopId);
  }

  /**
   * 根据手机号或名称查询该shop的级层关系
   */
  @Override
  public List<ShopTree> listByPhoneOrName(String rootShopId, String keys) {
    return shopTreeMapper.listByPhoneOrName(rootShopId, keys);
  }

  /**
   * 获取总店店铺
   */
  @Override
  public ShopTree getRootShop() {
    return shopTreeMapper.getRootShop();
  }

}
