package com.xquark.service.fullReduce.fullPieces.impl;

import com.xquark.dal.mapper.*;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionFullPieces;
import com.xquark.dal.model.PromotionFullPiecesDiscount;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.PromotionScope;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionFullPiecesDiscountVO;
import com.xquark.dal.vo.PromotionFullPiecesVO;
import com.xquark.dal.vo.PromotionReduceOrderVO;
import com.xquark.service.cart.vo.CartPromotionInfo;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.fullReduce.fullPieces.PromotionFullPiecesService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by wangxinhua on 17-9-21. DESC:
 */
@Service
public class PromotionFullPiecesServiceImpl extends BaseServiceImpl implements
        PromotionFullPiecesService {

    private PromotionFullPiecesMapper fullCutMapper;

    private PromotionFullPiecesDiscountMapper fullPiecesDiscountMapper;

    private PromotionMapper promotionMapper;

    private PromotionFullReduceOrderMapper reduceOrderMapper;

    private ProductMapper productMapper;

    @Autowired
    public void setFullCutMapper(PromotionFullPiecesMapper fullPiecesMapper) {
        this.fullCutMapper = fullPiecesMapper;
    }

    @Autowired
    public void setFullPiecesDiscountMapper(
            PromotionFullPiecesDiscountMapper fullPiecesDiscountMapper) {
        this.fullPiecesDiscountMapper = fullPiecesDiscountMapper;
    }

    @Autowired
    public void setPromotionMapper(PromotionMapper promotionMapper) {
        this.promotionMapper = promotionMapper;
    }

    @Autowired
    public void setReduceOrderMapper(PromotionFullReduceOrderMapper reduceOrderMapper) {
        this.reduceOrderMapper = reduceOrderMapper;
    }

    @Autowired
    public void setProductMapper(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    @Override
    @Transactional
    public boolean closeWithDiscounts(String id) {
        return this.close(id) && fullPiecesDiscountMapper.deleteByPromotionId(id) > 0;
    }

    @Override
    public boolean close(String promotionId) {
        return fullCutMapper.close(promotionId) > 0;
    }

    @Override
    public List<Promotion> listPromotion(String order, Sort.Direction direction, Pageable pageable,
                                         String keyWord) {
        return fullCutMapper.listPromotion(order, direction, pageable, keyWord);
    }

    @Override
    public Long countPromotion(String keyword) {
        return fullCutMapper.countPromotion(keyword);
    }

    @Override
    public List<PromotionFullPiecesDiscount> listDiscountsByPromotionId(String id, String order,
                                                                        Sort.Direction direction, Pageable pageable) {
        return fullPiecesDiscountMapper.listByPromotionId(id, order, direction, pageable);
    }

    @Override
    public List<PromotionFullPiecesDiscount> listDiscountsByPromotionId(String id) {
        return this.listDiscountsByPromotionId(id, null, null, null);
    }

    @Override
    public List<PromotionFullPiecesVO> listPromotionVO(String order, Sort.Direction direction,
                                                       Pageable pageable, String keyWord) {
        return fullCutMapper.listPromotionVO(order, direction, pageable, keyWord);
    }

    @Override
    public List<PromotionFullPiecesVO> listPromotionVO() {
        return this.listPromotionVO(null, null, null, null);
    }

    @Override
    public List<PromotionFullPiecesVO> listPromotionVOByProductId(String productId, Boolean isShare,
                                                                  String order, Sort.Direction direction, Pageable pageable) {
        return fullCutMapper.listPromotionVOByProductId(productId, isShare, order, direction, pageable);
    }

    @Override
    public List<PromotionFullPiecesVO> listPromotionVOByProductId(String productId, Boolean isShare) {
        return this.listPromotionVOByProductId(productId, isShare, null, null, null);
    }

    @Override
    public List<PromotionFullPiecesVO> listPromotionVOByProductId(String productId) {
        return this.listPromotionVOByProductId(productId, null);
    }

    @Override
    public List<Product> listProductByPromotionId(String id, Boolean gift, String order,
                                                  Sort.Direction direction, Pageable pageable) {
        return fullCutMapper.listProductByPromotionId(id, gift, order, direction, pageable);
    }

    @Override
    public List<Product> listProductByPromotionId(String id, Boolean gift) {
        return this.listProductByPromotionId(id, gift, null, null, null);
    }

    @Override
    public boolean deleteProduct(String promotionId, String productId) {
        return fullCutMapper.deleteProduct(promotionId, productId) > 0;
    }

    @Override
    public boolean addProduct(PromotionFullPieces promotionFullPieces) {
        String promotionId = promotionFullPieces.getPromotionId();
        String productId = promotionFullPieces.getProductId();
        if (hasAdded(promotionId, productId)) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该商品以添加");
        }
        return fullCutMapper.insert(promotionFullPieces) > 0;
    }

    @Override
    public boolean hasAdded(String promotionId, String productId) {
        return fullCutMapper.hasAdded(promotionId, productId) > 0;
    }

    @Override
    public boolean emptyProduct(String promotionId, Boolean gift) {
        return fullCutMapper.emptyProduct(promotionId, gift) > 0;
    }

    @Override
    public Long countProductByPromotionId(String id, Boolean gift) {
        return fullCutMapper.countProductByPromotionId(id, gift);
    }

    @Override
    public PromotionFullPiecesVO load(String id) {
        return fullCutMapper.selectById(id);
    }

    @Override
    public Promotion preSave(Promotion promotion) {
        if (StringUtils.isBlank(promotion.getId())) {
            promotion.setId(null);
            promotion.setArchive(true);
            int saveResult = promotionMapper.insert(promotion);
            if (saveResult > 0) {
                promotion.setId(IdTypeHandler.encode(Long.parseLong(promotion.getId())));
                return promotion;
            }
        }
        return promotion;
    }

    @Override
    @Transactional
    public String save(PromotionFullPiecesVO promotionFullPiecesVO) {
        boolean result;
        String fullPiecesId = promotionFullPiecesVO.getId();
        List<PromotionFullPiecesDiscountVO> discounts = promotionFullPiecesVO.getDiscounts();
        if (StringUtils.isBlank(fullPiecesId)) {
            // save
            int saveResult = promotionMapper.insert(promotionFullPiecesVO);
            fullPiecesId = IdTypeHandler.encode(Long.parseLong(promotionFullPiecesVO.getId()));
            int discountSaveResult = 1;
            if (discounts != null) {
                for (PromotionFullPiecesDiscount discount : discounts) {
                    discount.setPromotionId(fullPiecesId);
                }
                discountSaveResult = fullPiecesDiscountMapper.saveList(discounts);
            }
            result = discountSaveResult > 0 && saveResult > 0;
        } else {
            // update
            Promotion promotionExists = promotionMapper.selectByPrimaryKey(fullPiecesId);
            if (promotionExists == null) {
                throw new BizException(GlobalErrorCode.NOT_FOUND, "更新的活动不存在");
            }
            PromotionScope nowScope = promotionFullPiecesVO.getScope() == null ? PromotionScope.ALL
                    : promotionExists.getScope();
            if (nowScope != promotionExists.getScope() && nowScope == PromotionScope.ALL) {
                // 重新选择为全店商品则清空商品关联表
                this.emptyProduct(fullPiecesId, false);
            }
            // 预保存时已经将archive设置为了true
            promotionFullPiecesVO.setArchive(false);
            int updateResult = promotionMapper.update(promotionFullPiecesVO);
            boolean discountUpdateResult = true;
            if (discounts != null) {
                for (PromotionFullPiecesDiscount discount : discounts) {
                    if (StringUtils.isBlank(discount.getPromotionId())) {
                        discount.setPromotionId(fullPiecesId);
                    }
                    discountUpdateResult = discountUpdateResult && this.saveDiscount(discount);
                    if (!discountUpdateResult) {
                        log.error("{} update failed", discount.getId());
                    }
                }
            }
            result = discountUpdateResult && updateResult > 0;
        }
        return fullPiecesId;
    }

    @Override
    public boolean updateDiscount(PromotionFullPiecesDiscount discount) {
        return fullPiecesDiscountMapper.updateByPrimaryKeySelective(discount) > 0;
    }

    @Override
    public List<PromotionReduceOrderVO> listOrderDetail(String order, Sort.Direction direction,
                                                        Pageable pageable) {
        return reduceOrderMapper.listOrderDetail(PromotionType.FULLPIECES, order, direction, pageable);
    }

    @Override
    public Long countOrderDetail() {
        return reduceOrderMapper.countOrderDetail(PromotionType.FULLPIECES);
    }

    private boolean saveDiscount(PromotionFullPiecesDiscount discount) {
        String id = discount.getId();
        boolean result;
        if (StringUtils.isBlank(id)) {
            result = fullPiecesDiscountMapper.insert(discount) > 0;
        } else {
            result = this.updateDiscount(discount);
        }
        return result;
    }

    @Override
    public boolean deleteDiscount(String promotionId, String discountId) {
        return fullPiecesDiscountMapper.deleteInPromotion(promotionId, discountId) > 0;
    }

    @Override
    public Long countDiscount(String promotionId) {
        return fullPiecesDiscountMapper.countDiscount(promotionId);
    }

    @Override
    public boolean inPromotion(String productId, String excludePromotionId, String shopId) {
        return this.fullPiecesDiscountMapper.inPromotion(productId);
    }

    @Override
    public void changeStatus(CartPromotionInfo info, String productId, String shopId) {
        info.setInFullPieces(this.inPromotion(productId, null, shopId));
    }
}
