package com.xquark.service.fullReduce.fullCut.impl;

import static com.xquark.dal.type.PromotionUserScope.ALL;
import static com.xquark.dal.type.PromotionUserScope.EMPLOYEE;
import static com.xquark.service.pricing.impl.pricing.DiscountPricingResult.PROMOTION_COMPARATOR;
import static org.parboiled.common.Preconditions.checkNotNull;

import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.mapper.PromotionFullCutDiscountMapper;
import com.xquark.dal.mapper.PromotionFullCutMapper;
import com.xquark.dal.mapper.PromotionFullReduceOrderMapper;
import com.xquark.dal.mapper.PromotionMapper;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionFullCut;
import com.xquark.dal.model.PromotionFullCutDiscount;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.PromotionScope;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.PromotionUserScope;
import com.xquark.dal.vo.PromotionFullCutDiscountVO;
import com.xquark.dal.vo.PromotionFullCutProductInfoVO;
import com.xquark.dal.vo.PromotionFullCutProductVO;
import com.xquark.dal.vo.PromotionFullCutVO;
import com.xquark.dal.vo.PromotionReduceOrderVO;
import com.xquark.service.cart.vo.CartPromotionInfo;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.fullReduce.fullCut.PromotionFullCutService;
import com.xquark.service.user.UserService;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangxinhua on 17-9-21. DESC:
 */
@Service
public class PromotionFullCutServiceImpl extends BaseServiceImpl implements
        PromotionFullCutService {

    private PromotionFullCutMapper fullCutMapper;

    private PromotionFullCutDiscountMapper fullCutDiscountMapper;

    private PromotionMapper promotionMapper;

    private PromotionFullReduceOrderMapper reduceOrderMapper;

    private ProductMapper productMapper;

    private UserService userService;

    @Autowired
    public void setFullCutMapper(PromotionFullCutMapper fullCutMapper) {
        this.fullCutMapper = fullCutMapper;
    }

    @Autowired
    public void setFullCutDiscountMapper(PromotionFullCutDiscountMapper fullCutDiscountMapper) {
        this.fullCutDiscountMapper = fullCutDiscountMapper;
    }

    @Autowired
    public void setPromotionMapper(PromotionMapper promotionMapper) {
        this.promotionMapper = promotionMapper;
    }

    @Autowired
    public void setReduceOrderMapper(PromotionFullReduceOrderMapper reduceOrderMapper) {
        this.reduceOrderMapper = reduceOrderMapper;
    }

    @Autowired
    public void setProductMapper(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    @Transactional
    public boolean closeWithDiscounts(String id) {
        return this.close(id) && fullCutDiscountMapper.deleteByPromotionId(id) > 0;
    }

    @Override
    public boolean close(String promotionId) {
        return fullCutMapper.close(promotionId) > 0;
    }

    @Override
    public List<Promotion> listPromotion(String order,
                                         Sort.Direction direction, Pageable pageable, String keyWord) {
        return fullCutMapper.listPromotion(order, direction, pageable, keyWord);
    }

    @Override
    public Long countPromotion(String keyword) {
        return fullCutMapper.countPromotion(keyword);
    }

    @Override
    public List<PromotionFullCutDiscount> listDiscountsByPromotionId(String id, String order,
                                                                     Sort.Direction direction, Pageable pageable) {
        return fullCutDiscountMapper.listByPromotionId(id, order, direction, pageable);
    }

    @Override
    public List<PromotionFullCutDiscount> listDiscountsByPromotionId(String id) {
        return this.listDiscountsByPromotionId(id, null, null, null);
    }

    @Override
    public List<PromotionFullCutVO> listPromotionVO(String order, Sort.Direction direction,
                                                    Pageable pageable, String keyWord) {
        return fullCutMapper.listPromotionVO(order, direction, pageable, keyWord);
    }

    @Override
    public List<PromotionFullCutVO> listPromotionVO() {
        return this.listPromotionVO(null, null, null, null);
    }

    @Override
    public List<PromotionFullCutVO> listPromotionVOByProductId(String productId, Boolean isShare,
                                                               String order, Sort.Direction direction, Pageable pageable) {
        return fullCutMapper
                .listPromotionVOByProductId(productId, isShare, order, direction, pageable);
    }

    @Override
    public List<PromotionFullCutVO> listPromotionVOByProductId(String productId, Boolean isShare) {
        return this.listPromotionVOByProductId(productId, isShare, null, null, null);
    }

    @Override
    public List<PromotionFullCutVO> listPromotionVOByProductId(String productId) {
        return this.listPromotionVOByProductId(productId, null);
    }

    @Override
    public List<Product> listProductByPromotionId(String id, Boolean gift, String order,
                                                  Sort.Direction direction, Pageable pageable) {
        return fullCutMapper.listProductByPromotionId(id, gift, order, direction, pageable);
    }

    @Override
    public List<Product> listProductByPromotionId(String id, Boolean gift) {
        return this.listProductByPromotionId(id, gift, null, null, null);
    }

    @Override
    public boolean deleteProduct(String promotionId, String productId) {
        return fullCutMapper.deleteProduct(promotionId, productId) > 0;
    }

    @Override
    public boolean addProduct(PromotionFullCut promotionFullCut) {
        String promotionId = promotionFullCut.getPromotionId();
        String productId = promotionFullCut.getProductId();
        if (hasAdded(promotionId, productId)) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该商品以添加");
        }
        return fullCutMapper.insert(promotionFullCut) > 0;
    }

    @Override
    public boolean hasAdded(String promotionId, String productId) {
        return fullCutMapper.hasAdded(promotionId, productId) > 0;
    }

    @Override
    public boolean emptyProduct(String promotionId, Boolean gift) {
        return fullCutMapper.emptyProduct(promotionId, gift) > 0;
    }

    @Override
    public Long countProductByPromotionId(String id, Boolean gift) {
        return fullCutMapper.countProductByPromotionId(id, gift);
    }

    @Override
    public PromotionFullCutVO load(String id) {
        return fullCutMapper.selectById(id);
    }

    @Override
    public Promotion preSave(Promotion promotion) {
        if (StringUtils.isBlank(promotion.getId())) {
            promotion.setId(null);
            promotion.setArchive(true);
            int saveResult = promotionMapper.insert(promotion);
            if (saveResult > 0) {
                promotion.setId(IdTypeHandler.encode(Long.parseLong(promotion.getId())));
                return promotion;
            }
        }
        return promotion;
    }

    @Override
    @Transactional
    public Boolean save(PromotionFullCutProductVO productVO) {
        String fullCutId = productVO.getId();
        boolean result;
        if (StringUtils.isBlank(fullCutId)) {
            result = promotionMapper.insert(productVO) > 0;
            fullCutId = IdTypeHandler.encode(Long.parseLong(productVO.getId()));
        } else {
            result = promotionMapper.update(productVO) > 0;
        }
        // 重建商品关联关系
        List<? extends PromotionFullCut> fullCuts = productVO.getProducts();
        if (productVO.getScope() == PromotionScope.PRODUCT && CollectionUtils
                .isNotEmpty(fullCuts)) {
            emptyProduct(fullCutId, false);
            for (PromotionFullCut fullCut : fullCuts) {
                fullCut.setPromotionId(fullCutId);
                addProduct(fullCut);
            }
        }
        return result;
    }

    @Override
    @Transactional
    public String save(PromotionFullCutVO promotionFullCutVO) {
        boolean result;
        String fullCutId = promotionFullCutVO.getId();
        List<PromotionFullCutDiscountVO> discounts = promotionFullCutVO.getDiscounts();
        if (StringUtils.isBlank(fullCutId)) {
            // save
            int saveResult = promotionMapper.insert(promotionFullCutVO);
            fullCutId = IdTypeHandler.encode(Long.parseLong(promotionFullCutVO.getId()));
            int discountSaveResult = 1;
            if (discounts != null) {
                for (PromotionFullCutDiscount discount : discounts) {
                    discount.setFullCutId(fullCutId);
                }
                discountSaveResult = fullCutDiscountMapper.saveList(discounts);
            }
            result = discountSaveResult > 0 && saveResult > 0;
        } else {
            // update
            Promotion promotionExists = promotionMapper.selectByPrimaryKey(fullCutId);
            if (promotionExists == null) {
                throw new BizException(GlobalErrorCode.NOT_FOUND, "更新的活动不存在");
            }
            PromotionScope nowScope = promotionFullCutVO.getScope() == null ? PromotionScope.ALL
                    : promotionExists.getScope();
            if (nowScope != promotionExists.getScope() && nowScope == PromotionScope.ALL) {
                // 重新选择为全店商品则清空商品关联表
                this.emptyProduct(fullCutId, false);
            }
            // 预保存时已经将archive设置为了true
            promotionFullCutVO.setArchive(false);
            int updateResult = promotionMapper.update(promotionFullCutVO);
            boolean discountUpdateResult = true;
            if (discounts != null) {
                for (PromotionFullCutDiscountVO discount : discounts) {
                    if (StringUtils.isBlank(discount.getFullCutId())) {
                        discount.setFullCutId(fullCutId);
                    }
                    discountUpdateResult = discountUpdateResult && this.saveDiscount(discount);
                    if (!discountUpdateResult) {
                        log.error("{} update failed", discount.getId());
                    }
                }
            }
            result = discountUpdateResult && updateResult > 0;
        }
        if (!result) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "保存失败");
        }
        return fullCutId;
    }

    @Override
    public boolean updateDiscount(PromotionFullCutDiscount discount) {
        return fullCutDiscountMapper.updateByPrimaryKeySelective(discount) > 0;
    }

    private boolean saveDiscount(PromotionFullCutDiscount discount) {
        String id = discount.getId();
        boolean result;
        if (StringUtils.isBlank(id)) {
            result = fullCutDiscountMapper.insert(discount) > 0;
        } else {
            result = this.updateDiscount(discount);
        }
        return result;
    }

    @Override
    public List<PromotionReduceOrderVO> listOrderDetail(String order, Sort.Direction direction,
                                                        Pageable pageable) {
        return reduceOrderMapper.listOrderDetail(PromotionType.FULLCUT, order, direction, pageable);
    }

    @Override
    public boolean deleteDiscount(String promotionId, String discountId) {
        return fullCutDiscountMapper.deleteInPromotion(promotionId, discountId) > 0;
    }

    @Override
    public Long countDiscount(String promotionId) {
        return fullCutDiscountMapper.countDiscount(promotionId);
    }

    @Override
    public Long countOrderDetail() {
        return reduceOrderMapper.countOrderDetail(PromotionType.FULLCUT);
    }

    @Override
    public boolean inPromotion(String productId, String excludePromotionId, String shopId) {
        return fullCutMapper.inPromotion(productId, excludePromotionId, null, null);
    }

    @Override
    public boolean inPromotion(String productId, String promotionId, PromotionType type,
                               PromotionUserScope scope) {
        checkNotNull(scope, "用户范围不能为空");
        return fullCutMapper.inPromotion(productId, promotionId, type, scope);
    }

    @Override
    public Promotion selectCanUsePromotion(String productId, PromotionUserScope userScope) {
        List<Promotion> promotions = fullCutMapper.selectCanUsePromotion(productId, userScope);
        if (CollectionUtils.isEmpty(promotions)) {
            return null;
        }
        if (promotions.size() > 1) {
            log.error("单个商品参与了多个活动, 商品id: {}", productId);
        }
        return Collections.min(promotions, PROMOTION_COMPARATOR);
    }

    @Override
    public boolean hasPromotion(PromotionUserScope userScope) {
        return fullCutMapper.selectHasCanUsePromotion(userScope);
    }

    @Override
    public Promotion selectCanUsePromotion(String productId, String userId) {
        boolean isEmployee = userService.isRegistedEmployee(userId);
        Promotion result;
        if (isEmployee) {
            result = selectCanUsePromotion(productId, EMPLOYEE);
            if (result == null) {
                // 内部员工对普通优惠也可见
                result = selectCanUsePromotion(productId, ALL);
            }
            return result;
        }
        return selectCanUsePromotion(productId, ALL);
    }

    @Override
    public void changeStatus(CartPromotionInfo info, String productId, String shopId) {
        info.setInFullCut(this.inPromotion(productId, null, shopId));
    }

    @Override
    public PromotionFullCutProductVO loadProductVO(String id) {
        PromotionFullCutProductVO vo = fullCutMapper.selectProductVOById(id);
        if (vo == null) {
            return null;
        }
        PromotionScope scope = vo.getScope();
        // 商品范围则额外查询商品信息
        if (scope == PromotionScope.PRODUCT) {
            List<PromotionFullCutProductInfoVO> productInfo = listVOWithProductInfo(id);
            vo.setProducts(productInfo);
        }
        return vo;
    }

    /**
     * 查询活动关联商品信息
     *
     * @param id 活动id
     * @return 活动商品列表
     */
    @Override
    public List<PromotionFullCutProductInfoVO> listVOWithProductInfo(String id) {
        return fullCutMapper.listVOWithProductInfo(id, null);
    }

    /**
     * 查询活动关联商品信息
     *
     * @param id       活动id
     * @param pageable 分页对象
     * @return 活动商品列表
     */
    @Override
    public List<PromotionFullCutProductInfoVO> listVOWithProductInfo(String id, Pageable pageable) {
        return fullCutMapper.listVOWithProductInfo(id, pageable);
    }

    @Override
    public Long countVOWithProductInfo(String id) {
        return fullCutMapper.countVOWithProductInfo(id);
    }
}
