package com.xquark.service.fullReduce;

import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.mapper.PromotionFullReduceOrderMapper;
import com.xquark.dal.mapper.PromotionMapper;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.PromotionFullReduceDiscount;
import com.xquark.dal.model.Sku;
import com.xquark.dal.type.PromotionDiscountType;
import com.xquark.dal.vo.PromotionFullReduceVO;
import com.xquark.dal.vo.PromotionReduceDetailVO;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.pricing.vo.FullReducePricingResultVO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by wangxinhua on 17-10-9. DESC: 处理满减、满件公共服务 TODO 将满减满件相似方法移到此服务
 */
@Service
public class PromotionFullReduceService {

  private ProductMapper productMapper;

  private PromotionFullReduceOrderMapper fullReduceOrderMapper;

  private PromotionMapper promotionMapper;

  @Autowired
  public void setProductMapper(ProductMapper productMapper) {
    this.productMapper = productMapper;
  }

  @Autowired
  public void setFullReduceOrderMapper(PromotionFullReduceOrderMapper fullReduceOrderMapper) {
    this.fullReduceOrderMapper = fullReduceOrderMapper;
  }

  @Autowired
  public void setPromotionMapper(PromotionMapper promotionMapper) {
    this.promotionMapper = promotionMapper;
  }

  /**
   * 查询商品是否参加了满减、满件活动
   *
   * @param productId 商品id
   * @return true or false
   */
  public boolean inPromotion(String productId) {
    return promotionMapper.countReducePromotionByProductId(productId, new Date(), null) > 0;
  }

  /**
   * 查询一组商品中是否有商品参与了满减、满件活动
   *
   * @param productIds 商品Id集合
   * @return true or false
   */
  public boolean inPromotion(List<String> productIds) {
    if (CollectionUtils.isEmpty(productIds)) {
      throw new IllegalArgumentException("商品id不鞥为空");
    }
    for (String id : productIds) {
      if (inPromotion(id)) {
        return true;
      }
    }
    return false;
  }

  /**
   * 查询订单对应的满减、满件明细信息
   *
   * @param orderId 订单id
   * @return {@link List<PromotionReduceDetailVO>}
   */
  public List<PromotionReduceDetailVO> listDetailByOrderId(String orderId) {
    return fullReduceOrderMapper.listDetailByOrderId(orderId);
  }

  /**
   * 计算优惠结果 多个优惠在选定的商品范围内计算优惠结果 共享活动累计与非共享活动想比较，取最大优惠
   *
   * @param promotionFullReduceVOListMap 活动 - cartItem map
   * @return 优惠结果
   */
  public FullReducePricingResultVO calculateForPromotions(
      Map<PromotionFullReduceVO, List<CartItemVO>> promotionFullReduceVOListMap) {
    FullReducePricingResultVO resultVO = FullReducePricingResultVO.initPricing();
    if (MapUtils.isEmpty(promotionFullReduceVOListMap)) {
      return resultVO;
    }
    // 共享活动结果
    FullReducePricingResultVO sharedResultVO = FullReducePricingResultVO.initPricing();
    // 非共享活动结果
    FullReducePricingResultVO nonSharedResultVO = FullReducePricingResultVO.initPricing();

    // 单个entry单独计算
    for (Map.Entry<PromotionFullReduceVO, List<CartItemVO>> entry : promotionFullReduceVOListMap
        .entrySet()) {
      PromotionFullReduceVO promotion = entry.getKey();
      boolean isShare = promotion.getIsShare();
      // 计算折扣
      FullReducePricingResultVO calculatedResult = this.calculateDiscountFee(entry);
      // 若计算结果有效则加入添加到结果中
      if (calculatedResult.isValid()) {
        if (isShare) {
          sharedResultVO.merge(calculatedResult);
        } else {
          // 非共享活动如果计算值大于原值则清空原值后重新放入
          if (calculatedResult.compareTo(nonSharedResultVO) >= 0) {
            nonSharedResultVO = calculatedResult;
          }
        }
      }
    }
    // 共享、非共享活动中取最大优惠
    resultVO.merge(sharedResultVO.compareTo(nonSharedResultVO) > 0 ? sharedResultVO
        : nonSharedResultVO);
    return resultVO;
  }

  /**
   * 计算entry对象的结果 将一个活动与活动包含的商品看作一个活动范围， 每个活动范围作为一个MapEntry单独处理
   *
   * @param promotionEntry map - entry
   * @return 计算结果
   */
  protected FullReducePricingResultVO calculateDiscountFee(
      Map.Entry<PromotionFullReduceVO, List<CartItemVO>> promotionEntry) {
    List<CartItemVO> cartItems = promotionEntry.getValue();
    PromotionFullReduceVO promotion = promotionEntry.getKey();
    // 没有关联商品的活动应该清除
    assert CollectionUtils.isNotEmpty(cartItems);
    BigDecimal totalPrice = BigDecimal.ZERO;
    BigDecimal originalTotalPrice = BigDecimal.ZERO;
    Integer amount = 0;
    for (CartItemVO cartItem : cartItems) {
      Integer cartItemAmount = cartItem.getAmount();
      Sku sku = cartItem.getSku();
      totalPrice = totalPrice.add(sku.getPrice().multiply(new BigDecimal(cartItemAmount)));
      originalTotalPrice = originalTotalPrice
          .add(sku.getOriginalPrice().multiply(new BigDecimal(cartItemAmount)));
      amount += cartItemAmount;
    }
    // 取活动范围内商品的总金额，总数量，按该金额计算
    return this.calculateDiscountFee(totalPrice, originalTotalPrice, amount, promotion);
  }

  /**
   * 计算满减、满件的折扣价
   *
   * @param price 原价
   * @param originalPrice 现价
   * @param amount 数量
   * @param promotionFullReduceVO 活动对象
   * @return 计算结果
   */
  @Transactional
  protected <T extends Number> FullReducePricingResultVO calculateDiscountFee(BigDecimal price,
      BigDecimal originalPrice, Integer amount,
      PromotionFullReduceVO<T> promotionFullReduceVO) {
    List<? extends PromotionFullReduceDiscount<T>> discounts = promotionFullReduceVO
        .getDiscounts();
    FullReducePricingResultVO resultVO = FullReducePricingResultVO
        .initPricing(price, originalPrice, amount);
    BigDecimal discountFee = BigDecimal.ZERO;
    if (discounts == null || discounts.size() == 0) {
      return resultVO;
    }

    // 取出满足条件的最大折扣
    // 返回一定为Number的子类，此处强制转型为安全的
    @SuppressWarnings("unchecked")
    PromotionFullReduceDiscount reduceDiscount = promotionFullReduceVO.getSatisfiedDiscount(
        (T) resultVO.getCompareNumber(promotionFullReduceVO));

    // 没有匹配到折扣则直接返回
    if (reduceDiscount == null) {
      return resultVO;
    }

    boolean freeDelivery = reduceDiscount.getFreeDelivery();
    if (freeDelivery) {
      resultVO.setFreeDelivery(true);
    }
    // 赠品
    String giftIds = reduceDiscount.getGiftIds();
    if (StringUtils.isNotBlank(giftIds)) {
      List<Product> giftProducts = productMapper
          .selectByIds(StringUtils.split(giftIds, ","));
      if (giftProducts != null) {
        resultVO.getGiftProducts().addAll(giftProducts);
      }
    }

    PromotionDiscountType type = reduceDiscount.getDiscountType();
    if (type == null) {
      return resultVO;
    }

        BigDecimal reduceDiscountFee;
        // 根据折扣类型计算价格
        if (type == PromotionDiscountType.CASH) {
            reduceDiscountFee = reduceDiscount.getCash();
        } else {
            reduceDiscountFee = price
                    .subtract(price.multiply(reduceDiscount.getRealDiscount())
                        .divide(BigDecimal.valueOf(10), 2, BigDecimal.ROUND_HALF_EVEN));
        }
        discountFee = discountFee.add(reduceDiscountFee);

    promotionFullReduceVO.setDiscountFee(discountFee);
    resultVO.put(promotionFullReduceVO, discountFee);
    return resultVO;
  }


}
