package com.xquark.service.fullReduce.fullPieces;

import com.xquark.dal.model.Product;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionFullPieces;
import com.xquark.dal.model.PromotionFullPiecesDiscount;
import com.xquark.dal.vo.PromotionFullPiecesVO;
import com.xquark.dal.vo.PromotionReduceOrderVO;
import com.xquark.service.product.ProductPromotion;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Created by wangxinhua on 17-9-21. DESC:
 */
public interface PromotionFullPiecesService extends ProductPromotion {

  boolean closeWithDiscounts(String id);

  boolean close(String promotionId);

  List<Promotion> listPromotion(String order, Sort.Direction direction, Pageable pageable,
      String keyWord);

  Long countPromotion(String keyword);

  List<PromotionFullPiecesDiscount> listDiscountsByPromotionId(String id, String order,
      Sort.Direction direction, Pageable pageable);

  List<PromotionFullPiecesDiscount> listDiscountsByPromotionId(String id);

  List<PromotionFullPiecesVO> listPromotionVO(String order, Sort.Direction direction,
      Pageable pageable, String keyWord);

  List<PromotionFullPiecesVO> listPromotionVO();

  List<PromotionFullPiecesVO> listPromotionVOByProductId(String productId, Boolean isShare,
      String order, Sort.Direction direction, Pageable pageable);

  List<PromotionFullPiecesVO> listPromotionVOByProductId(String productId, Boolean isShare);

  List<PromotionFullPiecesVO> listPromotionVOByProductId(String productId);

  List<Product> listProductByPromotionId(String id, Boolean gift, String order,
      Sort.Direction direction, Pageable pageable);

  List<Product> listProductByPromotionId(String id, Boolean gift);

  boolean deleteProduct(String promotionId, String productId);

  boolean addProduct(PromotionFullPieces promotionFullCut);

  boolean hasAdded(String promotionId, String product);

  boolean emptyProduct(String promotionId, Boolean gift);

  Long countProductByPromotionId(String id, Boolean gift);

  PromotionFullPiecesVO load(String id);

  Promotion preSave(Promotion promotion);

  String save(PromotionFullPiecesVO promotionFullCut);

  boolean updateDiscount(PromotionFullPiecesDiscount discount);

  List<PromotionReduceOrderVO> listOrderDetail(String order, Sort.Direction direction,
      Pageable pageable);

  Long countOrderDetail();

  boolean deleteDiscount(String promotionId, String discountId);

  Long countDiscount(String promotionId);
}
