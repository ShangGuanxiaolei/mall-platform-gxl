package com.xquark.service.fullReduce.fullCut;

import com.xquark.dal.model.Product;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionFullCut;
import com.xquark.dal.model.PromotionFullCutDiscount;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.PromotionUserScope;
import com.xquark.dal.vo.PromotionFullCutProductInfoVO;
import com.xquark.dal.vo.PromotionFullCutProductVO;
import com.xquark.dal.vo.PromotionFullCutVO;
import com.xquark.dal.vo.PromotionReduceOrderVO;
import com.xquark.service.product.ProductPromotion;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Created by wangxinhua on 17-9-21. DESC:
 */
public interface PromotionFullCutService extends ProductPromotion {

    boolean closeWithDiscounts(String id);

    boolean close(String promotionId);

    List<com.xquark.dal.model.Promotion> listPromotion(String order, Sort.Direction direction,
                                                       Pageable pageable, String keyWord);

    Long countPromotion(String keyword);

    List<PromotionFullCutDiscount> listDiscountsByPromotionId(String id, String order,
                                                              Sort.Direction direction, Pageable pageable);

    List<PromotionFullCutDiscount> listDiscountsByPromotionId(String id);

    List<PromotionFullCutVO> listPromotionVO(String order, Sort.Direction direction,
                                             Pageable pageable, String keyWord);

    List<PromotionFullCutVO> listPromotionVO();

    List<PromotionFullCutVO> listPromotionVOByProductId(String productId, Boolean isShare,
                                                        String order, Sort.Direction direction, Pageable pageable);

    List<PromotionFullCutVO> listPromotionVOByProductId(String productId, Boolean isShare);

    List<PromotionFullCutVO> listPromotionVOByProductId(String productId);

    List<Product> listProductByPromotionId(String id, Boolean gift, String order,
                                           Sort.Direction direction, Pageable pageable);

    List<Product> listProductByPromotionId(String id, Boolean gift);

    boolean deleteProduct(String promotionId, String productId);

    boolean addProduct(PromotionFullCut promotionFullCut);

    boolean hasAdded(String promotionId, String product);

    boolean emptyProduct(String promotionId, Boolean gift);

    Long countProductByPromotionId(String id, Boolean gift);

    PromotionFullCutVO load(String id);

    /**
     * 获取后台优惠及商品VO
     *
     * @param promotionId 活动id
     * @return {@link PromotionFullCutProductVO} 实例
     */
    PromotionFullCutProductVO loadProductVO(String promotionId);

    Promotion preSave(Promotion promotion);

    Boolean save(PromotionFullCutProductVO productVO);

    String save(PromotionFullCutVO promotionFullCut);

    boolean updateDiscount(PromotionFullCutDiscount discount);

    List<PromotionReduceOrderVO> listOrderDetail(String order, Sort.Direction direction,
                                                 Pageable pageable);

    Long countOrderDetail();

    Long countDiscount(String promotionId);

    boolean deleteDiscount(String promotionId, String discountId);

    /**
     * 判断商品是否已经参与了相同定向人群的活动
     *
     * @param productId   商品id
     * @param promotionId
     * @param type
     * @param scope       人群范围
     * @return 是否已参加
     */
    boolean inPromotion(String productId, String promotionId, PromotionType type,
                        PromotionUserScope scope);

    /**
     * 根据商品和优惠人群查询满足条件的活动 前提: 全场优惠与商品自己的优惠不可共存
     *
     * @param productId 商品id
     * @param userScope 适用人群
     * @return 满足的活动
     */
    Promotion selectCanUsePromotion(String productId, PromotionUserScope userScope);

    boolean hasPromotion(PromotionUserScope userScope);

    /**
     * 根据商品及用户获取满足条件的活动
     *
     * @param productId 商品id
     * @param userId    用户id
     * @return 满足的活动
     */
    Promotion selectCanUsePromotion(String productId, String userId);

    List<PromotionFullCutProductInfoVO> listVOWithProductInfo(String id);

    List<PromotionFullCutProductInfoVO> listVOWithProductInfo(String id, Pageable pageable);

    Long countVOWithProductInfo(String id);
}
