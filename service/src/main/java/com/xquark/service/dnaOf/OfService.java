package com.xquark.service.dnaOf;

import com.xquark.dal.mapper.OfReportMapper;
import com.xquark.dal.vo.OfReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/2/21
 * Time: 18:02
 * Description: 欧飞接口service
 */
@Service
public class OfService {
    @Autowired
    private OfReportMapper ofReportMapper;

    public List<Map<String,String>> queryQuestionnaire(){
        List<Map<String, String>> maps =ofReportMapper.queryQuestionnaire();
        return maps;
    }

    public Integer insertOfReport(OfReport ofReport ){
        int i = ofReportMapper.insertOfReport(ofReport);
        return i;
    }

    public Integer checkSubjectPeopleByName(Integer cpid, String name) {
        return ofReportMapper.selectSubjectPeopleByName(cpid, name);
    }

    public OfReport getBindedOfreportByName(Integer cpid, String name) {
        OfReport ofReport = ofReportMapper.selectBindedOfreportByName(cpid, name);
        if (null != ofReport) {
            return ofReport;
        }
        return new OfReport();
    }

    public Boolean checkSampleCode(String sampleCode) {
        Integer isSampleCode = ofReportMapper.checkSampleCode(sampleCode);
        if (isSampleCode >= 1) {
            return true;
        }
        return false;
    }
}