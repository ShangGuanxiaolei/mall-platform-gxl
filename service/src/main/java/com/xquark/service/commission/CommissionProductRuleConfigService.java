package com.xquark.service.commission;

import com.xquark.dal.model.CommissionProductRuleConfig;
import com.xquark.dal.model.CommissionRule;
import com.xquark.dal.model.Order;
import com.xquark.dal.vo.CommissionProductRuleConfigVO;
import org.springframework.data.domain.Pageable;

import java.util.List;


/**
 */
public interface CommissionProductRuleConfigService {

  int insert(CommissionProductRuleConfig rule);

  int updateByPrimaryKey(CommissionProductRuleConfig rule);

  int updateByPrimaryKeySelective(CommissionProductRuleConfig rule);

  CommissionProductRuleConfigVO selectByPrimaryKey(String id);

  int modify(CommissionProductRuleConfig role);

  int delete(String id);

  /**
   * 列表查询
   */
  List<CommissionProductRuleConfigVO> list(Pageable page, String keyword);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(String keyword);

}
