package com.xquark.service.commission.impl;

import com.xquark.dal.mapper.CommissionMapper;
import com.xquark.dal.model.Commission;
import com.xquark.dal.model.MonthWithdraw;
import com.xquark.dal.type.CommissionType;
import com.xquark.dal.vo.CommissionVO;
import com.xquark.service.commission.CommissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("commissionService")
public class CommissionServiceImpl implements CommissionService {

  @Autowired
  private CommissionMapper commissionMapper;

  public Long countCommissionsByAdmin(Map<String, Object> params) {
    return commissionMapper.countCommissionsByAdmin(params);
  }

  @Override
  public List<CommissionVO> listCommissionsByAdmin(
      Map<String, Object> params, Pageable page) {
    return commissionMapper.listCommissionsByAdmin(params, page);
  }

  @Override
  public List<Commission> listByOrderId(String orderId) {
    return commissionMapper.listByOrderId(orderId);
  }

  @Override
  public List<Commission> listByOrderIdAndType(String orderId, CommissionType commissionType) {
    return commissionMapper.listByOrderIdAndType(orderId, commissionType);
  }

  /***
   * 查询某个用户当月佣金
   * @param userId
   * @return
   */
  @Override
  public BigDecimal getFeeByMonth(String userId) {
    return commissionMapper.getFeeByMonth(userId);
  }

  /**
   * 获取每个收益的orderNo
   */
  @Override
  public List<CommissionVO> listOrderNoByAdmin(Map<String, Object> params, Pageable page) {
    return commissionMapper.listOrderNoByAdmin(params, page);
  }

  @Override
  public Commission getOrderItemCommission(String orderId, String orderItemId, String userId) {

    return commissionMapper.getOrderItemCommission(orderId, orderItemId, userId);
  }

  /**
   * 2b查询本年度每个月的收益
   */
  @Override
  public List<BigDecimal> getFeeOfMonths(String userId) {

    Date date = new Date();
    int monthNow = date.getMonth();

    BigDecimal[] withdrawArray = new BigDecimal[monthNow + 1];
    for (int i = 0; i < monthNow + 1; i++) {
      withdrawArray[i] = new BigDecimal(0);
    }

    List<MonthWithdraw> monthWithdrawList = commissionMapper.getFeeOfMonths(userId);

    for (MonthWithdraw monthWithdraw : monthWithdrawList) {
      String month = monthWithdraw.getDate().substring(5);
      int i = Integer.parseInt(month);
      withdrawArray[i - 1] = monthWithdraw.getWithdraw();
    }
    return Arrays.asList(withdrawArray);
  }

  /**
   * 2b获取累计收益
   */
  @Override
  public BigDecimal getWithdrawAll(String userId) {

    return commissionMapper.getWithdrawAll(userId);
  }

  /**
   * 2b获取指定月份收益
   */
  @Override
  public BigDecimal getWithdrawOfMonth(String userId, String lastMonthStr) {

    return commissionMapper.getWithdrawOfMonth(userId, lastMonthStr);
  }

  /**
   * 2b查询收益订单列表的orderNo
   */
  @Override
  public List<CommissionVO> listSuccessOrderNo(Map<String, Object> params, Pageable page) {

    return commissionMapper.listSuccessOrderNo(params, page);
  }

  /**
   * 获取有收益的订单数
   */
  @Override
  public long countNumberByDate(Map<String, Object> params) {
    return commissionMapper.countNumberByDate(params);
  }

  /**
   * 获取卖出的商品数量
   */
  @Override
  public long getSaleProductsByDate(Map<String, Object> params) {
    return commissionMapper.getSaleProductsByDate(params);
  }

  @Override
  public Boolean updateOffered(String idStr, Boolean offered) {
    String[] ids = idStr.split(",");
    return commissionMapper.updateOffered(ids, offered) > 0;
  }

  /**
   * 今日收益
   */
  @Override
  public BigDecimal getTodayWithdraw(String userId) {
    return commissionMapper.getTodayWithdraw(userId);
  }

  /**
   * 本月收益
   */
  @Override
  public BigDecimal getMonthWithdraw(String userId) {
    return commissionMapper.getMonthWithdraw(userId);
  }

  /**
   * 我的收益，根据类型分页获取明细收益信息
   */
  @Override
  public List<CommissionVO> listByApp(Map<String, Object> params, Pageable page) {
    return commissionMapper.listByApp(params, page);
  }

}
