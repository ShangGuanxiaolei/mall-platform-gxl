package com.xquark.service.commission.impl;

import com.google.common.collect.ImmutableMap;
import com.xquark.dal.mapper.CommissionMapper;
import com.xquark.dal.mapper.CommissionProductRuleConfigMapper;
import com.xquark.dal.mapper.CommissionRuleMapper;
import com.xquark.dal.mapper.ShopTreeMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.CommissionStatus;
import com.xquark.dal.status.RoleBelong;
import com.xquark.dal.type.CommissionPolicy;
import com.xquark.dal.type.CommissionRuleName;
import com.xquark.dal.type.CommissionType;
import com.xquark.dal.vo.CommissionProductRuleConfigVO;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.commission.CommissionProductRuleConfigService;
import com.xquark.service.commission.CommissionRuleEvaluation;
import com.xquark.service.commission.CommissionRuleService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.order.OrderAddressService;
import com.xquark.service.order.OrderService;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.user.RoleService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.*;

/**
 *
 */
@Service("commissionProductRuleConfigService")
public class CommissionProductRuleConfigServiceImpl extends BaseServiceImpl implements
    CommissionProductRuleConfigService {


  @Autowired
  private CommissionProductRuleConfigMapper commissionProductRuleConfigMapper;

  @Override
  public int insert(CommissionProductRuleConfig rule) {
    return commissionProductRuleConfigMapper.insert(rule);
  }

  @Override
  public int updateByPrimaryKey(CommissionProductRuleConfig rule) {
    return commissionProductRuleConfigMapper.updateByPrimaryKey(rule);
  }

  @Override
  public int updateByPrimaryKeySelective(CommissionProductRuleConfig rule) {
    return commissionProductRuleConfigMapper.updateByPrimaryKeySelective(rule);
  }

  @Override
  public CommissionProductRuleConfigVO selectByPrimaryKey(String id) {
    return commissionProductRuleConfigMapper.selectByPrimaryKey(id);
  }

  @Override
  public int modify(CommissionProductRuleConfig role) {
    return commissionProductRuleConfigMapper.modify(role);
  }

  @Override
  public int delete(String id) {
    return commissionProductRuleConfigMapper.delete(id);
  }

  /**
   * 列表查询
   */
  @Override
  public List<CommissionProductRuleConfigVO> list(Pageable page, String keyword) {
    return commissionProductRuleConfigMapper.list(page, keyword);
  }

  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long selectCnt(String keyword) {
    return commissionProductRuleConfigMapper.selectCnt(keyword);
  }


}
