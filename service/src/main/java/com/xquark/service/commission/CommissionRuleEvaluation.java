package com.xquark.service.commission;

import com.xquark.dal.model.*;
import com.xquark.dal.type.CommissionPolicy;
import com.xquark.dal.type.CommissionRuleName;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 分佣规则计算
 */
public abstract class CommissionRuleEvaluation {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private CommissionRuleName ruleName;

  private CommissionPolicy policy;

  public CommissionRuleName getRuleName() {
    return ruleName;
  }

  public void setRuleName(CommissionRuleName ruleName) {
    this.ruleName = ruleName;
  }

  public CommissionPolicy getPolicy() {
    return policy;
  }

  public void setPolicy(CommissionPolicy policy) {
    this.policy = policy;
  }

  public CommissionRuleEvaluation(CommissionPolicy policy, CommissionRuleName ruleName) {
    this.policy = policy;
    this.ruleName = ruleName;
  }

  /**
   * 按单个rule执行佣金计算, 将计算结果返回供存储及账务处理
   */
  public abstract List<Commission> eval(Order order, CommissionRule rule);

  /**
   * 找到分佣的对象
   */
  public abstract List<String> findCommissioner(Order order, CommissionRule rule);

  /**
   * 默认的商品佣金配置选择,按优先级和extconditions的满足度
   */
  public CommissionProductRuleConfig select(
      List<CommissionProductRuleConfig> commissionProductRuleConfigs) {
    if (CollectionUtils.isEmpty(commissionProductRuleConfigs)) {
      return null;
    }

    if (commissionProductRuleConfigs.size() == 1) {
      return commissionProductRuleConfigs.get(0);
    }

    //Desc
    Collections.sort(commissionProductRuleConfigs, new Comparator<CommissionProductRuleConfig>() {
      @Override
      public int compare(CommissionProductRuleConfig o1, CommissionProductRuleConfig o2) {
        if (o1.getPriority() > o2.getPriority()) {
          return -1;
        } else if (o1.getPriority() < o2.getPriority()) {
          return 1;
        } else {
          return 0;
        }
      }
    });

    //TODO 增加extconditions的处理

    return commissionProductRuleConfigs.get(0);
  }

  /**
   * 根据配置计算佣金金额
   *
   * @return 如果无法计算或得出的佣金为0, 则返回null
   */
  public BigDecimal calcFee(CommissionProductRuleConfig commissionProductRuleConfig,
      OrderItem orderItem, BigDecimal multiplierForRate) {
    BigDecimal ret;
    if (commissionProductRuleConfig == null) {
      return null;
    } else {
      //优先根据fee计算commission, 如果没有再根据rate计算
      BigDecimal configFee = commissionProductRuleConfig.getCommissionFee();
      BigDecimal configRate = commissionProductRuleConfig.getCommissionRate();

      if (configFee != null && configFee.compareTo(BigDecimal.ZERO) > 0) {
        ret = configFee.multiply(new BigDecimal(orderItem.getAmount()));
      } else if (configRate != null && configRate.compareTo(BigDecimal.ZERO) > 0) {
        //此处暂时根据交易价格和rate计算佣金
        ret = orderItem.getPrice().multiply(new BigDecimal(orderItem.getAmount()))
            .multiply(configRate);
      } else {
        log.error(
            "commissionProductRuleConfig:" + commissionProductRuleConfig.getId() + ", 无法计算佣金.");
        return null;
      }
    }

    return ret;
  }

}
