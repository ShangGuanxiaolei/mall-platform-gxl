package com.xquark.service.commission.impl;

import com.google.common.collect.ImmutableMap;
import com.xquark.cache.FounderAreaCache;
import com.xquark.dal.mapper.CommissionMapper;
import com.xquark.dal.mapper.CommissionProductRuleConfigMapper;
import com.xquark.dal.mapper.CommissionRuleMapper;
import com.xquark.dal.mapper.ShopTreeMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.CommissionStatus;
import com.xquark.dal.status.RoleBelong;
import com.xquark.dal.type.CommissionPolicy;
import com.xquark.dal.type.CommissionRuleName;
import com.xquark.dal.type.CommissionType;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.commission.CommissionRuleEvaluation;
import com.xquark.service.commission.CommissionRuleService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.order.OrderAddressService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.vo.OrderAddressVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.user.RoleService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.*;

/**
 *
 */
@Service("commissionRuleService")
public class CommissionRuleServiceImpl extends BaseServiceImpl implements CommissionRuleService {

  @Autowired
  private OrderService orderService;

  @Autowired
  private UserService userService;

  @Autowired
  private ProductService productService;

  @Autowired
  private CommissionMapper commissionMapper;

  @Autowired
  private CommissionRuleMapper commissionRuleMapper;

  @Autowired
  private CommissionProductRuleConfigMapper commissionProductRuleConfigMapper;

  @Autowired
  private ShopService shopService;

  @Autowired
  private OrderAddressService orderAddressService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private ShopTreeMapper shopTreeMapper;

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private RoleService roleService;

  @Autowired
  private FounderAreaCache founderAreaCache;

  @Autowired
  private AddressService addressService;


  private ImmutableMap<String, CommissionRuleEvaluation> evaluations;

  @PostConstruct
  public void init() {
    evaluations = new ImmutableMap.Builder<String, CommissionRuleEvaluation>()
        .put(
            CommissionPolicy.USER_AGENT_DEFAULT.name() + "|"
                + CommissionRuleName.GENERAL_DIRECT_TO_DIRECTOR.name(),
            new CommissionRuleEvaluation(
                CommissionPolicy.USER_AGENT_DEFAULT,
                CommissionRuleName.GENERAL_DIRECT_TO_DIRECTOR) {

              @Override
              public List<Commission> eval(Order order, CommissionRule rule) {
                List<Commission> ret = new ArrayList<Commission>();

                List<String> commissionerSellerIds = this.findCommissioner(order, rule);
                if (CollectionUtils.isEmpty(commissionerSellerIds)) {
                  return ret;
                }

                //满足计算条件, 开始计算佣金
                List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
                for (OrderItem orderItem : orderItems) {
                  String ruleLog =
                      this.getPolicy().name() + "|" + this.getRuleName().name() + "|" + rule.getId()
                          + "|" + rule.getRuleName() + "|" + rule.getDescription() + "|" + rule
                          .getParams();

                  //TODO 目前仅用到最简单的优先级条件, 后续通过extconditions的条件, 更加灵活配置分销体系的激励机制
                  CommissionProductRuleConfig commissionProductRuleConfig = this.select(
                      commissionProductRuleConfigMapper
                          .listByCommissionRuleAndProduct(rule.getId(), orderItem.getProductId()));

                  BigDecimal cmFee = this
                      .calcFee(commissionProductRuleConfig, orderItem, BigDecimal.ZERO);
                  if (cmFee == null) {
                    continue;
                  }

                  Commission generalDirectToDirectorCommission = new Commission(order.getId(),
                      orderItem.getId(), orderItem.getSkuId(), orderItem.getPrice(),
                      commissionProductRuleConfig.getCommissionFee() == null ||
                          commissionProductRuleConfig.getCommissionFee().compareTo(BigDecimal.ZERO)
                              <= 0 ? commissionProductRuleConfig.getCommissionRate().doubleValue()
                          : commissionProductRuleConfig.getCommissionFee().doubleValue()
                      , CommissionType.GENERAL_DIRECT_TO_DIRECTOR, orderItem.getAmount(), cmFee,
                      commissionerSellerIds.get(0), CommissionStatus.SUCCESS, true, ruleLog);
                  ret.add(generalDirectToDirectorCommission);
                }

                return ret;
              }

              @Override
              public List<String> findCommissioner(Order order, CommissionRule rule) {
                UserAgent currentUserAgent = userAgentService.selectByUserId(order.getBuyerId());
                Role currentRole = roleService.selectByPrimaryKey(currentUserAgent.getRole());
                Shop buyerShop = shopService.findByUser(order.getBuyerId());
                ShopTree directParentShop = shopTreeService
                    .selectDirectParent(order.getRootShopId(), buyerShop.getId());
                UserAgent parentUserAgent = userAgentService
                    .selectByUserId(shopService.load(order.getShopId()).getOwnerId());

                //对于当前角色不是总顾问或当前场景不属于B2B的情况, 跳过规则计算
                if (!currentRole.getCode().equals(AgentType.GENERAL.name()) || !currentRole
                    .getBelong().equals(RoleBelong.B2B)) {
                  return null;
                }

                //找不到上级代理, 或者代理是总部, 跳过规则计算
                if (parentUserAgent == null ||
                    (directParentShop.getRootShopId()
                        .equals(directParentShop.getAncestorShopId()))) {
                  return null;
                }

                //对于上级角色不是董事或联合创始人,或当前场景不属于B2B的情况, 跳过规则计算
                Role parentRole = roleService.selectByPrimaryKey(parentUserAgent.getRole());
                if (!(parentRole.getBelong().equals(RoleBelong.B2B) && (
                    parentRole.getCode().equals(AgentType.DIRECTOR.name()) || parentRole.getCode()
                        .equals(AgentType.FOUNDER.name())))) {
                  return null;
                }

                return Arrays.asList(new String[]{parentUserAgent.getUserId()});
              }
            }
        ).put(
            CommissionPolicy.USER_AGENT_DEFAULT.name() + "|"
                + CommissionRuleName.GENERAL_INDIRECT_TO_DIRECTOR.name(),
            new CommissionRuleEvaluation(
                CommissionPolicy.USER_AGENT_DEFAULT,
                CommissionRuleName.GENERAL_INDIRECT_TO_DIRECTOR) {

              @Override
              public List<Commission> eval(Order order, CommissionRule rule) {
                List<Commission> ret = new ArrayList<Commission>();

                List<String> commissionerSellerIds = this.findCommissioner(order, rule);
                if (CollectionUtils.isEmpty(commissionerSellerIds)) {
                  return ret;
                }

                //满足计算条件, 开始计算佣金
                List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
                for (OrderItem orderItem : orderItems) {
                  String ruleLog =
                      this.getPolicy().name() + "|" + this.getRuleName().name() + "|" + rule.getId()
                          + "|" + rule.getRuleName() + "|" + rule.getDescription() + "|" + rule
                          .getParams();

                  //TODO 目前仅用到最简单的优先级条件, 后续通过extconditions的条件, 更加灵活配置分销体系的激励机制
                  CommissionProductRuleConfig commissionProductRuleConfig = this.select(
                      commissionProductRuleConfigMapper
                          .listByCommissionRuleAndProduct(rule.getId(), orderItem.getProductId()));
                  BigDecimal cmFee = this
                      .calcFee(commissionProductRuleConfig, orderItem, BigDecimal.ZERO);
                  if (cmFee == null) {
                    continue;
                  }

                  Commission generalDirectToDirectorCommission = new Commission(order.getId(),
                      orderItem.getId(), orderItem.getSkuId(), orderItem.getPrice(),
                      commissionProductRuleConfig.getCommissionFee() == null ||
                          commissionProductRuleConfig.getCommissionFee().compareTo(BigDecimal.ZERO)
                              <= 0 ? commissionProductRuleConfig.getCommissionRate().doubleValue()
                          : commissionProductRuleConfig.getCommissionFee().doubleValue()
                      , CommissionType.GENERAL_INDIRECT_TO_DIRECTOR, orderItem.getAmount(), cmFee,
                      commissionerSellerIds.get(0), CommissionStatus.SUCCESS, true, ruleLog);
                  ret.add(generalDirectToDirectorCommission);
                }

                return ret;
              }

              @Override
              public List<String> findCommissioner(Order order, CommissionRule rule) {
                UserAgent currentUserAgent = userAgentService.selectByUserId(order.getBuyerId());
                Role currentRole = roleService.selectByPrimaryKey(currentUserAgent.getRole());
                Shop buyerShop = shopService.findByUser(order.getBuyerId());
                ShopTree directParentShop = shopTreeService
                    .selectDirectParent(order.getRootShopId(), buyerShop.getId());
                UserAgent parentUserAgent = userAgentService
                    .selectByUserId(shopService.load(order.getShopId()).getOwnerId());

                //对于当前角色不是总顾问或当前场景不属于B2B的情况, 跳过规则计算
                if (!currentRole.getCode().equals(AgentType.GENERAL.name()) || !currentRole
                    .getBelong().equals(RoleBelong.B2B)) {
                  return null;
                }
                //找不到上级代理, 或者代理是总部, 跳过规则计算
                if (parentUserAgent == null ||
                    (directParentShop.getRootShopId()
                        .equals(directParentShop.getAncestorShopId()))) {
                  return null;
                }

                //对于上级角色不是总代或当前场景不属于B2B的情况, 跳过规则计算
                Role parentRole = roleService.selectByPrimaryKey(parentUserAgent.getRole());
                if (!(parentRole.getBelong().equals(RoleBelong.B2B) && parentRole.getCode()
                    .equals(AgentType.GENERAL.name()))) {
                  return null;
                }

                //找总顾问的上级董事或联合创始人,找到才计算间接分佣
                ShopTree inDirectParentShop = shopTreeService
                    .selectDirectParent(order.getRootShopId(),
                        directParentShop.getAncestorShopId());
                UserAgentVO inDirectParentUserAgent = userAgentService.selectByUserId(
                    shopService.load(inDirectParentShop.getAncestorShopId()).getOwnerId());
                if (inDirectParentUserAgent == null ||
                    (inDirectParentShop.getRootShopId()
                        .equals(inDirectParentShop.getAncestorShopId()))) {
                  return null;
                }

                Role inDirectParentRole = roleService
                    .selectByPrimaryKey(inDirectParentUserAgent.getRole());
                if (!(inDirectParentRole.getCode().equals(AgentType.DIRECTOR.name())
                    || inDirectParentRole.getCode().equals(AgentType.FOUNDER.name()))
                    || !inDirectParentRole.getBelong().equals(RoleBelong.B2B)) {
                  return null;
                }

                return Arrays.asList(new String[]{inDirectParentUserAgent.getUserId()});
              }
            }
        ).put(
            CommissionPolicy.USER_AGENT_DEFAULT.name() + "|"
                + CommissionRuleName.DIRECTORS_TEAM_TO_DIRECTORS.name(),
            new CommissionRuleEvaluation(
                CommissionPolicy.USER_AGENT_DEFAULT,
                CommissionRuleName.DIRECTORS_TEAM_TO_DIRECTORS) {

              @Override
              public List<Commission> eval(Order order, CommissionRule rule) {
                List<Commission> ret = new ArrayList<Commission>();

                List<String> commissionerSellerIds = this.findCommissioner(order, rule);
                if (CollectionUtils.isEmpty(commissionerSellerIds)) {
                  return ret;
                }

                //满足计算条件, 开始计算佣金
                List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
                for (OrderItem orderItem : orderItems) {
                  String ruleLog =
                      this.getPolicy().name() + "|" + this.getRuleName().name() + "|" + rule.getId()
                          + "|" + rule.getRuleName() + "|" + rule.getDescription() + "|" + rule
                          .getParams();

                  //TODO 目前仅用到最简单的优先级条件, 后续通过extconditions的条件, 更加灵活配置分销体系的激励机制
                  CommissionProductRuleConfig commissionProductRuleConfig = this.select(
                      commissionProductRuleConfigMapper
                          .listByCommissionRuleAndProduct(rule.getId(), orderItem.getProductId()));
                  BigDecimal cmFee = this
                      .calcFee(commissionProductRuleConfig, orderItem, BigDecimal.ZERO);
                  if (cmFee == null) {
                    continue;
                  }

                  for (String commissionerSellerId : commissionerSellerIds) {
                    Commission generalDirectToDirectorCommission = new Commission(order.getId(),
                        orderItem.getId(), orderItem.getSkuId(), orderItem.getPrice(),
                        commissionProductRuleConfig.getCommissionFee() == null ||
                            commissionProductRuleConfig.getCommissionFee()
                                .compareTo(BigDecimal.ZERO) <= 0 ? commissionProductRuleConfig
                            .getCommissionRate().doubleValue()
                            : commissionProductRuleConfig.getCommissionFee().doubleValue()
                        , CommissionType.DIRECTORS_TEAM_TO_DIRECTORS, orderItem.getAmount(), cmFee,
                        commissionerSellerId, CommissionStatus.SUCCESS, true, ruleLog);
                    ret.add(generalDirectToDirectorCommission);
                  }
                }

                return ret;
              }

              @Override
              public List<String> findCommissioner(Order order, CommissionRule rule) {
                List<String> ret = null;

                UserAgent currentUserAgent = userAgentService.selectByUserId(order.getBuyerId());
                Role currentRole = roleService.selectByPrimaryKey(currentUserAgent.getRole());
                Shop buyerShop = shopService.findByUser(order.getBuyerId());
                ShopTree directParentShop = shopTreeService
                    .selectDirectParent(order.getRootShopId(), buyerShop.getId());
                UserAgent parentUserAgent = userAgentService
                    .selectByUserId(shopService.load(order.getShopId()).getOwnerId());

                // fix bug 21 董事和总代每次拿货会计算上级应得佣金，二星或者一星用户拿货时还会再次记录，导致上级会重复抽取5块钱*盒数的佣金
                // 只有买家类型是总代或董事，才进行分佣计算
                UserAgent userAgent = userAgentService.selectByUserId(order.getBuyerId());
                if (userAgent == null || userAgent.getType().equals(AgentType.FIRST) || userAgent
                    .getType().equals(AgentType.SECOND) || userAgent.getType()
                    .equals(AgentType.SPECIAL)) {
                  return ret;
                }

                //当前场景不属于B2B的情况, 跳过规则计算
                if (!currentRole.getBelong().equals(RoleBelong.B2B)) {
                  return ret;
                }
                //找不到上级代理, 或者代理是总部, 跳过规则计算
                if (parentUserAgent == null ||
                    (directParentShop.getRootShopId()
                        .equals(directParentShop.getAncestorShopId()))) {
                  return ret;
                }

                //开始查找三级董事,并对董事团队(包括董事)的直招上级董事和间招上级董事计算佣金
                List<ShopTree> parentShopTrees = shopTreeMapper
                    .listParentIncludingItself(order.getRootShopId(),
                        buyerShop.getId());//从买家开始查找所有上级,包括买家自己
                if (CollectionUtils.isEmpty(parentShopTrees) || parentShopTrees.size() == 1) {
                  return ret;
                }

                ret = new ArrayList<String>();
                int commissionDepth = 2;
                boolean isTeamLeaderDirector = true;
                for (int i = 0; i < parentShopTrees.size(); i++) {
                  ShopTree tree = parentShopTrees.get(i);
                  UserAgentVO commissionerAgent = userAgentService
                      .selectByUserId(shopService.load(tree.getAncestorShopId()).getOwnerId());
                  if (commissionerAgent == null ||
                      (tree.getRootShopId().equals(tree.getAncestorShopId()))) {
                    log.info("DIRECTORS_TEAM_TO_DIRECTORS:found root shop.");
                    break;
                  }

                  Role commissionToRole = roleService
                      .selectByPrimaryKey(commissionerAgent.getRole());
                  if (!(commissionToRole.getBelong().equals(RoleBelong.B2B) && (
                      commissionToRole.getCode().equals(AgentType.DIRECTOR.name())
                          || commissionToRole.getCode().equals(AgentType.FOUNDER.name())))) {
                    continue;
                  } else {
                    if (isTeamLeaderDirector) {
                      isTeamLeaderDirector = false;
                      continue;
                    }
                    ret.add(commissionerAgent.getUserId());
                    if (ret.size() == commissionDepth) {
                      break;
                    }
                  }
                }
                return ret;
              }
            }
        ).put(
            CommissionPolicy.USER_AGENT_DEFAULT.name() + "|" + CommissionRuleName.TEAM_ORDER.name(),
            new CommissionRuleEvaluation(
                CommissionPolicy.USER_AGENT_DEFAULT, CommissionRuleName.TEAM_ORDER) {

              @Override
              public List<Commission> eval(Order order, CommissionRule rule) {
                List<Commission> ret = new ArrayList<Commission>();

                List<String> commissionerSellerIds = this.findCommissioner(order, rule);
                if (CollectionUtils.isEmpty(commissionerSellerIds)) {
                  return ret;
                }

                //满足计算条件, 开始计算佣金
                List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
                for (OrderItem orderItem : orderItems) {
                  String ruleLog =
                      this.getPolicy().name() + "|" + this.getRuleName().name() + "|" + rule.getId()
                          + "|" + rule.getRuleName() + "|" + rule.getDescription() + "|" + rule
                          .getParams();

                  //TODO 目前仅用到最简单的优先级条件, 后续通过extconditions的条件, 更加灵活配置分销体系的激励机制
                  CommissionProductRuleConfig commissionProductRuleConfig = this.select(
                      commissionProductRuleConfigMapper
                          .listByCommissionRuleAndProduct(rule.getId(), orderItem.getProductId()));
                  BigDecimal cmFee = this
                      .calcFee(commissionProductRuleConfig, orderItem, BigDecimal.ZERO);
                  if (cmFee == null) {
                    continue;
                  }

                  for (String commissionerSellerId : commissionerSellerIds) {
                    Commission generalDirectToDirectorCommission = new Commission(order.getId(),
                        orderItem.getId(), orderItem.getSkuId(), orderItem.getPrice(),
                        commissionProductRuleConfig.getCommissionFee() == null ||
                            commissionProductRuleConfig.getCommissionFee()
                                .compareTo(BigDecimal.ZERO) <= 0 ? commissionProductRuleConfig
                            .getCommissionRate().doubleValue()
                            : commissionProductRuleConfig.getCommissionFee().doubleValue()
                        , CommissionType.TEAM_ORDER, orderItem.getAmount(), cmFee,
                        commissionerSellerId, CommissionStatus.SUCCESS, true, ruleLog);
                    ret.add(generalDirectToDirectorCommission);
                  }
                }

                return ret;
              }

              @Override
              public List<String> findCommissioner(Order order, CommissionRule rule) {
                List<String> ret = null;

                UserAgent currentUserAgent = userAgentService.selectByUserId(order.getBuyerId());
                Role currentRole = roleService.selectByPrimaryKey(currentUserAgent.getRole());
                Shop buyerShop = shopService.findByUser(order.getBuyerId());
                ShopTree directParentShop = shopTreeService
                    .selectDirectParent(order.getRootShopId(), buyerShop.getId());
                UserAgent parentUserAgent = userAgentService
                    .selectByUserId(shopService.load(order.getShopId()).getOwnerId());

                // 只有买家类型是总代或董事或联合创始人，才进行分佣计算
                UserAgent userAgent = userAgentService.selectByUserId(order.getBuyerId());
                if (userAgent == null || userAgent.getType().equals(AgentType.FIRST) || userAgent
                    .getType().equals(AgentType.SECOND) || userAgent.getType()
                    .equals(AgentType.SPECIAL)) {
                  return ret;
                }

                //当前场景不属于B2B的情况, 跳过规则计算
                if (!currentRole.getBelong().equals(RoleBelong.B2B)) {
                  return ret;
                }
                //找不到上级代理, 或者代理是总部, 跳过规则计算
                if (parentUserAgent == null ||
                    (directParentShop.getRootShopId()
                        .equals(directParentShop.getAncestorShopId()))) {
                  return ret;
                }

                //查找买家上级，只要上级是联合创始人，则该联合创始人能享受团队分佣
                List<ShopTree> parentShopTrees = shopTreeMapper
                    .listParentIncludingItselfDepth(order.getRootShopId(), buyerShop.getId(),
                        1);//从买家开始查找所有上级,不包括买家自己
                if (CollectionUtils.isEmpty(parentShopTrees) || parentShopTrees.size() == 0) {
                  return ret;
                }

                ret = new ArrayList<String>();
                for (int i = 0; i < parentShopTrees.size(); i++) {
                  ShopTree tree = parentShopTrees.get(i);
                  UserAgentVO commissionerAgent = userAgentService
                      .selectByUserId(shopService.load(tree.getAncestorShopId()).getOwnerId());
                  if (commissionerAgent == null ||
                      (tree.getRootShopId().equals(tree.getAncestorShopId()))) {
                    log.info("TEAM_ORDER:found root shop.");
                    break;
                  }

                  Role commissionToRole = roleService
                      .selectByPrimaryKey(commissionerAgent.getRole());
                  if (!(commissionToRole.getBelong().equals(RoleBelong.B2B) && (commissionToRole
                      .getCode().equals(AgentType.FOUNDER.name())))) {
                    continue;
                  } else {
                    ret.add(commissionerAgent.getUserId());
                  }
                }
                return ret;
              }
            }
        ).put(
            CommissionPolicy.USER_AGENT_DEFAULT.name() + "|" + CommissionRuleName.AREA_ORDER.name(),
            new CommissionRuleEvaluation(
                CommissionPolicy.USER_AGENT_DEFAULT, CommissionRuleName.AREA_ORDER) {

              @Override
              public List<Commission> eval(Order order, CommissionRule rule) {
                List<Commission> ret = new ArrayList<Commission>();

                List<String> commissionerSellerIds = this.findCommissioner(order, rule);
                if (CollectionUtils.isEmpty(commissionerSellerIds)) {
                  return ret;
                }

                //满足计算条件, 开始计算佣金
                List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
                for (OrderItem orderItem : orderItems) {
                  String ruleLog =
                      this.getPolicy().name() + "|" + this.getRuleName().name() + "|" + rule.getId()
                          + "|" + rule.getRuleName() + "|" + rule.getDescription() + "|" + rule
                          .getParams();

                  //TODO 目前仅用到最简单的优先级条件, 后续通过extconditions的条件, 更加灵活配置分销体系的激励机制
                  CommissionProductRuleConfig commissionProductRuleConfig = this.select(
                      commissionProductRuleConfigMapper
                          .listByCommissionRuleAndProduct(rule.getId(), orderItem.getProductId()));
                  BigDecimal cmFee = this
                      .calcFee(commissionProductRuleConfig, orderItem, BigDecimal.ZERO);
                  if (cmFee == null) {
                    continue;
                  }

                  for (String commissionerSellerId : commissionerSellerIds) {
                    Commission generalDirectToDirectorCommission = new Commission(order.getId(),
                        orderItem.getId(), orderItem.getSkuId(), orderItem.getPrice(),
                        commissionProductRuleConfig.getCommissionFee() == null ||
                            commissionProductRuleConfig.getCommissionFee()
                                .compareTo(BigDecimal.ZERO) <= 0 ? commissionProductRuleConfig
                            .getCommissionRate().doubleValue()
                            : commissionProductRuleConfig.getCommissionFee().doubleValue()
                        , CommissionType.AREA_ORDER, orderItem.getAmount(), cmFee,
                        commissionerSellerId, CommissionStatus.SUCCESS, true, ruleLog);
                    ret.add(generalDirectToDirectorCommission);
                  }
                }

                return ret;
              }

              @Override
              public List<String> findCommissioner(Order order, CommissionRule rule) {
                List<String> ret = null;

                UserAgent currentUserAgent = userAgentService.selectByUserId(order.getBuyerId());
                Role currentRole = roleService.selectByPrimaryKey(currentUserAgent.getRole());
                Shop buyerShop = shopService.findByUser(order.getBuyerId());
                ShopTree directParentShop = shopTreeService
                    .selectDirectParent(order.getRootShopId(), buyerShop.getId());
                UserAgent parentUserAgent = userAgentService
                    .selectByUserId(shopService.load(order.getShopId()).getOwnerId());

                // 只有买家类型是总代或董事或联合创始人，才进行分佣计算
                UserAgent userAgent = userAgentService.selectByUserId(order.getBuyerId());
                if (userAgent == null || userAgent.getType().equals(AgentType.FIRST) || userAgent
                    .getType().equals(AgentType.SECOND) || userAgent.getType()
                    .equals(AgentType.SPECIAL)) {
                  return ret;
                }

                //当前场景不属于B2B的情况, 跳过规则计算
                if (!currentRole.getBelong().equals(RoleBelong.B2B)) {
                  return ret;
                }
                //找不到上级代理, 或者代理是总部, 跳过规则计算
                if (parentUserAgent == null ||
                    (directParentShop.getRootShopId()
                        .equals(directParentShop.getAncestorShopId()))) {
                  return ret;
                }

                // 针对联合创始人的封地政策，订单为该区域的所有订单，负责本区域的联合创始人每一盒订单都可以奖励1元；
                // 如果订单是自提订单，订单区域按买家默认收货地址为准，否则按订单地址为准
                String area = "";
                if (order.getIsPickup() == 1) {
                  AddressVO addressVO = addressService.getDefault(order.getBuyerId());
                  if (addressVO == null) {
                    return ret;
                  }
                  area = addressVO.getZoneId();
                } else {
                  OrderAddressVO orderAddressVO = orderAddressService
                      .selectByOrderId(order.getId());
                  if (orderAddressVO == null) {
                    return ret;
                  }
                  area = orderAddressVO.getZoneId();
                }

                // 获得该订单地区对应的封地区域的联合创始人
                ret = getAreaUser(area);
                return ret;
              }
            }
        ).put(
            CommissionPolicy.USER_AGENT_DEFAULT.name() + "|"
                + CommissionRuleName.GENERAL_DIRECT_TO_GENERAL.name(),
            new CommissionRuleEvaluation(
                CommissionPolicy.USER_AGENT_DEFAULT, CommissionRuleName.GENERAL_DIRECT_TO_GENERAL) {

              @Override
              public List<Commission> eval(Order order, CommissionRule rule) {
                List<Commission> ret = new ArrayList<Commission>();

                List<String> commissionerSellerIds = this.findCommissioner(order, rule);
                if (CollectionUtils.isEmpty(commissionerSellerIds)) {
                  return ret;
                }

                //满足计算条件, 开始计算佣金
                List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
                for (OrderItem orderItem : orderItems) {
                  String ruleLog =
                      this.getPolicy().name() + "|" + this.getRuleName().name() + "|" + rule.getId()
                          + "|" + rule.getRuleName() + "|" + rule.getDescription() + "|" + rule
                          .getParams();

                  //TODO 目前仅用到最简单的优先级条件, 后续通过extconditions的条件, 更加灵活配置分销体系的激励机制
                  CommissionProductRuleConfig commissionProductRuleConfig = this.select(
                      commissionProductRuleConfigMapper
                          .listByCommissionRuleAndProduct(rule.getId(), orderItem.getProductId()));

                  for (String commissionerSellerId : commissionerSellerIds) {
                    Commission generalDirectToDirectorCommission = new Commission(order.getId(),
                        orderItem.getId(), orderItem.getSkuId(), orderItem.getPrice(),
                        commissionProductRuleConfig.getCommissionFee().doubleValue()
                        , CommissionType.GENERAL_DIRECT_TO_GENERAL, orderItem.getAmount(),
                        commissionProductRuleConfig.getCommissionFee(), commissionerSellerId,
                        CommissionStatus.SUCCESS, true, ruleLog);
                    ret.add(generalDirectToDirectorCommission);
                  }
                }

                return ret;
              }

              @Override
              public List<String> findCommissioner(Order order, CommissionRule rule) {
                List<String> ret = null;

                UserAgent currentUserAgent = userAgentService.selectByUserId(order.getBuyerId());
                Role currentRole = roleService.selectByPrimaryKey(currentUserAgent.getRole());
                Shop buyerShop = shopService.findByUser(order.getBuyerId());
                ShopTree directParentShop = shopTreeService
                    .selectDirectParent(order.getRootShopId(), buyerShop.getId());
                UserAgent parentUserAgent = userAgentService.selectByUserId(
                    shopService.load(directParentShop.getAncestorShopId()).getOwnerId());

                // 只有买家类型是总顾问，才进行分佣计算
                UserAgent userAgent = userAgentService.selectByUserId(order.getBuyerId());
                if (userAgent == null || !userAgent.getType().equals(AgentType.GENERAL)) {
                  return ret;
                }

                //当前场景不属于B2B的情况, 跳过规则计算
                if (!currentRole.getBelong().equals(RoleBelong.B2B)) {
                  return ret;
                }
                //找不到上级代理, 或者代理是总部, 或者上级代理不是总顾问跳过规则计算
                if (parentUserAgent == null ||
                    (directParentShop.getRootShopId().equals(directParentShop.getAncestorShopId()))
                    || parentUserAgent.getType() != AgentType.GENERAL) {
                  return ret;
                }

                // 判断如果这是该总顾问买家第一笔成交订单，则需要返利给上级总顾问2000元
                ret = new ArrayList<String>();
                long countSuccess = orderService
                    .countBeforeSuccessOrder(order.getId(), order.getBuyerId());
                if (countSuccess == 0) {
                  ret.add(parentUserAgent.getUserId());
                }
                return ret;
              }
            }
        ).build();
  }

  @Override
  @Transactional
  public void execute(Order order) {
    CommissionPolicy policy = getPolicy(order);
    List<CommissionRule> rules = commissionRuleMapper.listByPolicy(policy);

    if (CollectionUtils.isNotEmpty(rules)) {
      //按rule index顺序执行
      Collections.sort(rules, new Comparator<CommissionRule>() {
        @Override
        public int compare(CommissionRule o1, CommissionRule o2) {
          if (o1.getIndex() > o2.getIndex()) {
            return 1;
          } else if (o1.getIndex() < o2.getIndex()) {
            return -1;
          } else {
            return 0;
          }
        }
      });

      List<Commission> commissions = new ArrayList<Commission>();
      for (CommissionRule rule : rules) {
        CommissionRuleEvaluation evaluation = evaluations
            .get(rule.getPolicyName().name() + "|" + rule.getRuleName());
        if (evaluation == null) {
          log.warn(rule.getPolicyName().name() + "|" + rule.getRuleName()
              + " rule evaluation not found.");
          continue;
        }
        commissions.addAll(evaluation.eval(order, rule));
      }

      //TODO 对执行结果进行检查后保存
      for (Commission commission : commissions) {
        commissionMapper.insert(commission);
      }
    }
  }

  private CommissionPolicy getPolicy(Order order) {
    //TODO 根据不同的总部shopid计算当前的Policy
    return CommissionPolicy.USER_AGENT_DEFAULT;
  }

  /**
   * 查找某个订单地区对应是否有该封地区域的联合创始人
   */
  private List<String> getAreaUser(String area) {
    List<String> users = new ArrayList<String>();
    Map<Object, Object> founders = founderAreaCache.entries();
    String tuser_id = "";
    int zone_size = 0;
    for (Object in : founders.keySet()) {
      String userId = (String) in;
      List<Zone> zones = (List<Zone>) founders.get(in);
      // 如果出现区域包含关系的多个联合创始人，按最小化进行分佣，即只分给最小区域的创始人
      for (Zone zone : zones) {
        if (zone.getId().equals(area)) {
          if (zone_size == 0) {
            zone_size = zones.size();
            tuser_id = userId;
          } else {
            if (zones.size() < zone_size) {
              zone_size = zones.size();
              tuser_id = userId;
            }
          }
        }
      }
    }
    if (StringUtils.isNotEmpty(tuser_id)) {
      users.add(tuser_id);
    }
    return users;
  }

  @Override
  public CommissionRule selectByPrimaryKey(String id) {
    return commissionRuleMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(CommissionRule role) {
    return commissionRuleMapper.insert(role);
  }

  @Override
  public int modify(CommissionRule role) {
    return commissionRuleMapper.modify(role);
  }


  @Override
  public int delete(String id) {
    return commissionRuleMapper.delete(id);
  }

  /**
   * 列表查询页面
   */
  @Override
  public List<CommissionRule> list(Pageable page, String keyword) {
    return commissionRuleMapper.list(page, keyword);
  }

  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long selectCnt(String keyword) {
    return commissionRuleMapper.selectCnt(keyword);
  }


}
