package com.xquark.service.commission;

import com.xquark.dal.model.Commission;
import com.xquark.dal.type.CommissionType;
import com.xquark.dal.vo.CommissionVO;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface CommissionService {

  Long countCommissionsByAdmin(Map<String, Object> params);

  List<CommissionVO> listCommissionsByAdmin(Map<String, Object> params, Pageable page);

  List<Commission> listByOrderId(String orderId);

  List<Commission> listByOrderIdAndType(String orderId, CommissionType commissionType);

  /***
   * 查询某个用户当月佣金
   * @param userId
   * @return
   */
  BigDecimal getFeeByMonth(String userId);

  /**
   * 查询每一个收益的orderNo
   */
  List<CommissionVO> listOrderNoByAdmin(Map<String, Object> params, Pageable pageable);

  Commission getOrderItemCommission(String id, String id1, String userId);

  /**
   * 查询本年度每个月的佣金
   */
  List<BigDecimal> getFeeOfMonths(String userId);

  /**
   * 2b获取累计收益
   */
  BigDecimal getWithdrawAll(String userId);

  /**
   * 2b获取指定月份收益
   */
  BigDecimal getWithdrawOfMonth(String userId, String lastMonthStr);

  /**
   * 2b查询收益列表的orderNo
   */
  List<CommissionVO> listSuccessOrderNo(Map<String, Object> params, Pageable pageable);

  /**
   * 获取有收益的订单数
   */
  long countNumberByDate(Map<String, Object> params);

  long getSaleProductsByDate(Map<String, Object> params);

  Boolean updateOffered(String idStr, Boolean offered);

  /**
   * 今日收益
   */
  BigDecimal getTodayWithdraw(String userId);

  /**
   * 本月收益
   */
  BigDecimal getMonthWithdraw(String userId);

  /**
   * 我的收益，根据类型分页获取明细收益信息
   */
  List<CommissionVO> listByApp(Map<String, Object> params, Pageable page);

}
