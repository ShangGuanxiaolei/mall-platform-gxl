package com.xquark.service.commission;

import com.xquark.dal.model.CommissionRule;
import com.xquark.dal.model.Order;
import org.springframework.data.domain.Pageable;

import java.util.List;


/**
 * 本服务根据不同佣金计算规则计算不同条件下应得的佣金
 * <li>Policy: 佣金计算的套餐</li>
 * <li>Rule: 佣金计算套餐中的一个具体的分佣规则,该规则一般来说跟订单事件相关, 但也可以是某种周期性计算的规则</li>
 */
public interface CommissionRuleService {

  /**
   * 根据当前订单计算即时分账
   */
  void execute(Order order);

  CommissionRule selectByPrimaryKey(String id);

  int insert(CommissionRule role);

  int modify(CommissionRule role);

  int delete(String id);

  /**
   * 列表查询页面
   */
  List<CommissionRule> list(Pageable page, String keyword);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(String keyword);

}
