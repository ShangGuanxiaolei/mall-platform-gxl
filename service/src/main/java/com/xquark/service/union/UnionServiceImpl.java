package com.xquark.service.union;

import com.alibaba.fastjson.JSON;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.CommissionProductType;
import com.xquark.dal.status.CommissionStatus;
import com.xquark.dal.type.*;
import com.xquark.dal.vo.PartnerTypeRelationVO;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.commission.CommissionRuleService;
import com.xquark.service.distribution.DistributionConfigService;
import com.xquark.service.domain.DomainService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderAddressService;
import com.xquark.service.order.OrderService;
import com.xquark.service.pay.PayRequestApiService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductSkuVO;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.twitter.TwitterProductComService;
import com.xquark.service.twitter.TwitterShopComService;
import com.xquark.service.twitter.UserTwitterService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.userFamily.FamilyCardSettingService;
import com.xquark.userFamily.UserCardService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service("unionService")
public class UnionServiceImpl implements UnionService {

  @Autowired
  private OrderService orderService;

  @Autowired
  private UserService userService;

  @Autowired
  private ProductService productService;

  @Autowired
  private CommissionMapper commissionMapper;

  @Autowired
  private CommissionRuleService commissionRuleService;

  @Autowired
  private AccountApiService accountApiService;

  @Autowired
  private PayRequestApiService payRequestApiService;

  @Autowired
  private DistributionConfigService distributionConfigService;

  @Autowired
  private DomainService domainService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private TwitterProductComService twitterProductComService;

  @Autowired
  private TwitterShopComService twitterShopComService;

  @Autowired
  private UserTwitterService userTwitterService;

  @Autowired
  private OrderAddressService orderAddressService;


  @Autowired
  private ZoneService zoneService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private FamilyCardSettingService familyCardSettingService;

  @Autowired
  private UserCardService userCardService;

  @Autowired
  private TwitterLevelMapper twitterLevelMapper;

  @Autowired
  private ShopTreeMapper shopTreeMapper;

  @Autowired
  private PartnerProductCommissionDeMapper partnerProductCommissionDeMapper;

  @Autowired
  private PartnerShopCommissionMapper partnerShopCommissionMapper;

  @Autowired
  private PartnerTypeMapper partnerTypeMapper;

  @Autowired
  private PartnerTypeRelationMapper partnerTypeRelationMapper;


  @Value("${kkkd.commission.rate}")
  private Double kkkdRate;

  //分润后可以提现的天数
  @Value("${commission.withdraw.date}")
  private Integer commissionWithdrawDate;

  protected Logger log = LoggerFactory.getLogger(getClass());

  private BigDecimal B100 = new BigDecimal("100");

  @Override
  public void calc(String orderId) {
    // 1. 根据订单上的unionId字段，计算订单的分账记录 commission
    Order order = orderService.load(orderId);
    if (StringUtils.isBlank(order.getUnionId())) {
      log.info("order[" + order.getOrderNo() + "] has no union id, skip union calculation.");
      return;
    }

    //删除可能存在的已有的分润记录
    commissionMapper.cleanCommissionByOrderIdAndType(order.getId(), CommissionType.PLATFORM);
    commissionMapper.cleanCommissionByOrderIdAndType(order.getId(), CommissionType.CPS);

    List<OrderItem> orderItems = orderService.listOrderItems(orderId);
    Commission comm = null;

    //应该算佣金的钱，减去运费和退款金额  301-10-300
    BigDecimal totalFee = order.getTotalFee().add(order.getDiscountFee())
        .subtract(order.getLogisticsFee())
        .subtract(order.getRefundFee()).subtract(order.getRefundPlatformFee());

    if (totalFee.compareTo(BigDecimal.ZERO) < 1) {
      return;  //如果退款金额加上运费超过了订单总额，则无需支付佣金
    }

    //order item各商品的总额，用来算比例
    BigDecimal totalGoodsFee = BigDecimal.ZERO;

    for (OrderItem orderItem : orderItems) {
      double prod_rate = productService
          .loadCommissionRate(orderItem.getProductId(), order.getUnionId()).doubleValue();
      totalGoodsFee = totalGoodsFee
          .add(orderItem.getPrice().multiply(BigDecimal.valueOf(orderItem.getAmount())));
      if (prod_rate == .0) {
        continue;
      }
    }
    //兼容历史数据，如果panter和union_id想同的为平台佣金，否则为cps佣金
    CommissionType commissionType = CommissionType.CPS;
    Domain domain = domainService.loadByCode(order.getPartner());
    if (domain == null || domain.getAdminUserId().equals(order.getUnionId())) {
      commissionType = CommissionType.PLATFORM;
    }

    BigDecimal skuFee = null;
    BigDecimal cmFee = null;
    for (OrderItem orderItem : orderItems) {
      double prod_rate = productService
          .loadCommissionRate(orderItem.getProductId(), order.getUnionId()).doubleValue();
      skuFee = orderItem.getPrice().multiply(BigDecimal.valueOf(orderItem.getAmount()))
          .divide(totalGoodsFee, 4, BigDecimal.ROUND_DOWN).multiply(totalFee);
      if (prod_rate == .0) {
        continue;
      }
      /** 目前改价是直接修改订单金额，按比例计算佣金  **/
      cmFee = skuFee.multiply(BigDecimal.valueOf(prod_rate)).setScale(2, BigDecimal.ROUND_DOWN);
      //占比     单体*数量/订单商品总价*订单价
      if (StringUtils.isNotBlank(order.getUnionId())) {
        comm = new Commission(order.getId(), orderItem.getId(), orderItem.getSkuId(),
            orderItem.getPrice(),
            prod_rate, commissionType, orderItem.getAmount(), cmFee, order.getUnionId(),
            CommissionStatus.NEW);
      } else {
        ProductSkuVO productSkuVO = productService
            .load(orderItem.getProductId(), orderItem.getSkuId());
        if (productSkuVO.isDistributed()) {
          Product sourceProduct = productSkuVO.getSourceProduct();
          Shop shop = shopService.load(sourceProduct.getShopId());
          comm = new Commission(order.getId(), orderItem.getId(), orderItem.getSkuId(),
              orderItem.getPrice(),
              prod_rate, commissionType, orderItem.getAmount(), cmFee, shop.getOwnerId(),
              CommissionStatus.NEW);
        }
      }

      commissionMapper.insert(comm);
    }
  }


  @Override
  public void calcWechatCommission(String orderId) {

    Order order = orderService.load(orderId);

    // 限时抢购与积分订单不参与分佣
    if (order.getOrderType() == OrderSortType.YUNDOU
        || order.getOrderType() == OrderSortType.FLASHSALE) {
      return;
    }

    List<OrderItem> orderItems = orderService.listOrderItems(orderId);

    //应该算佣金的钱，减去运费和退款金额  301-10-300
    BigDecimal totalFee = order.getTotalFee().add(order.getDiscountFee())
        .subtract(order.getLogisticsFee())
        .subtract(order.getRefundFee()).subtract(order.getRefundPlatformFee());

    //order item各商品的总额，用来算比例
    BigDecimal totalGoodsFee = BigDecimal.ZERO;

    for (OrderItem orderItem : orderItems) {
      totalGoodsFee = totalGoodsFee
          .add(orderItem.getPrice().multiply(BigDecimal.valueOf(orderItem.getAmount())));
    }

    // 查询买家是否本身是推客，如果是，判断是否有勾选“推客自己获得佣金”,走推客自己获取佣金模式
    // 否则普通消费者购买，走三级分佣模式
    boolean isTwitter = false;
    boolean calTwitter = false;
    String buyerId = order.getBuyerId();
    List<UserTwitter> twitters = userTwitterService.selectByUserId(buyerId);
    if (twitters != null && twitters.size() > 0 && twitters.get(0) != null) {
      isTwitter = true;
      TwitterShopCommission twitterShopCommission = twitterShopComService.selectDefault();
      calTwitter = twitterShopCommission.getSelfCommission();
    }

    // 如果卖家是总部，而且买家不是推客,不计算佣金
    if (order.getShopId().equals(order.getRootShopId()) && !isTwitter) {
      return;
    }

    BigDecimal skuFee = null;
    for (OrderItem orderItem : orderItems) {
      // 如果此订单有折扣，则订单佣金需要减去折扣计算
      BigDecimal discountFee = order.getDiscountFee();
      skuFee = orderItem.getPrice().multiply(BigDecimal.valueOf(orderItem.getAmount()))
          .divide(totalGoodsFee, 4, BigDecimal.ROUND_DOWN)
          .multiply((totalFee.subtract(discountFee)));
      // 推客自己购买商品计算佣金
      if (isTwitter) {
        if (calTwitter) {
          calcTwitterCommission(order, orderItem, skuFee);
        }
      } else {
        // 计算推客三级分佣
        calcThreeCommission(order, orderItem, skuFee);
        // 计算合伙人分佣
        calcPartnerCommission(order, orderItem, skuFee);
      }
    }
  }

  @Override
  public void calcUserAgentCommission(String orderId) {
    Order order = orderService.load(orderId);
    commissionRuleService.execute(order);
  }

  @Override
  public void onSuccess(String orderId) {
    commissionMapper.updateStatus(orderId, CommissionStatus.SUCCESS);
    distribution(orderId);
  }

  @Override
  public void distribution(String orderId) {
    Order order = orderService.load(orderId);
    if (StringUtils.isBlank(order.getPartner())) {
      return;
    }

    Domain domain = domainService.loadByCode(order.getPartner());
    if (domain == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "域为空 orderNo=[" + order.getOrderNo() + "]");
    }

    List<Commission> cms = listByOrderId(order.getId());
    //兼容老数据和重复提交
    if (cms.size() != 1) {
      return;
    }
    BigDecimal totalCm = BigDecimal.ZERO;
    for (Commission cm : cms) {
      totalCm = totalCm.add(cm.getFee());
    }
    Commission comm = null;
    SubAccount fromAccount, toAccount;
    PayRequest request = null;
    BigDecimal cpsCm = BigDecimal.ZERO;

    //三方分佣
    if (StringUtils.isNoneBlank(order.getTuId())) {
      //扣除cps佣金
      //取最新比率
      DistributionConfig config = distributionConfigService
          .selectByUser(domain.getAdminUserId(), order.getTuId());
      if (config != null && config.getCpsRate() != null) {
        cpsCm = totalCm.multiply(config.getCpsRate());
        cpsCm = cpsCm.setScale(2, BigDecimal.ROUND_DOWN);
        comm = new Commission(order.getId(), cms.get(0).getOrderItemId(), null, totalCm,
            config.getCpsRate().doubleValue(), CommissionType.CPS, null, cpsCm,
            config.getPromoter(), CommissionStatus.SUCCESS);
        commissionMapper.insert(comm);

        String payNo = payRequestApiService.generatePayNo();
        //从平台的佣金账户转到cps分销者
        fromAccount = accountApiService
            .findSubAccountByUserId(domain.getAdminUserId(), AccountType.COMMISSION);
        toAccount = accountApiService
            .findSubAccountByUserId(comm.getUserId(), AccountType.COMMISSION);
        request = new PayRequest(payNo, payNo, PayRequestBizType.COMMISSION,
            PayRequestPayType.INSTANT, comm.getFee(), fromAccount.getId(), toAccount.getId(), null);
        payRequestApiService.payRequest(request);
      }
    }
  }

  @Override
  public void onCancel(String orderId) {
    commissionMapper.updateStatus(orderId, CommissionStatus.CLOSED);
  }

  @Override
  public Commission loadByOrderItem(String id) {
    return commissionMapper.selectByOrderItem(id);
  }

  @Override
  public Commission loadByOrderItemAndUserId(String id, String userId) {
    return commissionMapper.loadByOrderItemAndUserId(id, userId);
  }

  @Override
  public List<Commission> listByOrderId(String orderId) {
    return commissionMapper.listByOrderId(orderId);
  }

  /**
   * 把超过30天的佣金账户转入到可用账户
   *
   * @return 成功条数
   */
  @Override
  public Long autoCommissionToAvailable() {
    List<Commission> commissions = commissionMapper.listCanWithdraw(commissionWithdrawDate);
    String payNo = payRequestApiService.generatePayNo();
    int successCount = 0;
    for (Commission cm : commissions) {
      try {
        processCommissionEntry(cm, payNo);
        successCount++;
      } catch (Exception e) {
        log.error("processCommissionEntry failed:Commission id=" + cm.getId() + " orderId=" + cm
            .getOrderId(), e);
      }

      try {
        Thread.sleep(100); //100毫秒执行一条记录，暂时解决并发
      } catch (InterruptedException e) {
        log.error(e.getMessage(), e);
      }
    }

    return (long) successCount;
  }

  /**
   * 立即将佣金账户转入到可用账户
   *
   * @return 成功条数
   */
  @Override
  public Long autoCommissionToAvailableTest() {
    List<Commission> commissions = commissionMapper.listCanWithdrawTest();
    String payNo = payRequestApiService.generatePayNo();
    int successCount = 0;
    for (Commission cm : commissions) {
      try {
        processCommissionEntry(cm, payNo);
        successCount++;
      } catch (Exception e) {
        log.error("processCommissionEntry failed:Commission id=" + cm.getId() + " orderId=" + cm
            .getOrderId(), e);
      }

      try {
        Thread.sleep(100); //100毫秒执行一条记录，暂时解决并发
      } catch (InterruptedException e) {
        log.error(e.getMessage(), e);
      }
    }

    return (long) successCount;
  }

  @Override
  @Transactional
  public Boolean processCommissionEntry(Commission cm, String payNo) {
    SubAccount fromAccount, toAccount;
    //从买家的可消费账户里的钱转到 广告主的佣金账户
    fromAccount = accountApiService.findSubAccountByUserId(cm.getUserId(), AccountType.COMMISSION);
    toAccount = accountApiService.findSubAccountByUserId(cm.getUserId(), AccountType.AVAILABLE);
    PayRequest request = new PayRequest(payNo, payNo, PayRequestBizType.BATCH_PAY,
        PayRequestPayType.INSTANT, cm.getFee(), fromAccount.getId(), toAccount.getId(), null);

    Boolean ret = payRequestApiService.payRequest(request);

    log.info(JSON.toJSONString(cm));

    if (ret.equals(Boolean.TRUE)) {
      int rowCount = commissionMapper.finish(cm.getOrderId());
      if (rowCount == 0) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            "Race Condition:Commission id=" + cm.getId() + " orderId=" + cm.getOrderId()
                + " already finish withdrawn");
      }
    }

    return ret;
  }

  /**
   * 计算推客三级分佣
   */
  public void calcThreeCommission(Order order, OrderItem orderItem, BigDecimal skuFee) {
    Commission comm = null;

    String rootShopId = shopTreeService.selectRootShopByShopId(order.getShopId()).getRootShopId();
    String currentShopId = order.getShopId();

    TwitterProductCommission productCommission = twitterProductComService
        .selectComByProductIdAndShopId(orderItem.getProductId(), rootShopId);
    TwitterShopCommission shopCommission = twitterShopComService.selectDefaultByshopId(rootShopId);

    commissionMapper.cleanCommissionByOrderIdAndType(order.getId(), CommissionType.ONECOMMISSION);
    commissionMapper.cleanCommissionByOrderIdAndType(order.getId(), CommissionType.TWOCOMMISSION);
    commissionMapper.cleanCommissionByOrderIdAndType(order.getId(), CommissionType.THREECOMMISSION);

    // 分佣类型，按比例或按固定金额等
    CommissionProductType commissionProductType = CommissionProductType.PER;
    double firstRate = 0.0;
    double secondRate = 0.0;
    double thirdRate = 0.0;

    // 如果该商品有配置商品佣金，则按照配置的商品佣金进行计算
    if (productCommission != null) {
      commissionProductType = productCommission.getType();
      firstRate = productCommission.getFirstLevelRate();
      secondRate = productCommission.getSecondLevelRate();
      thirdRate = productCommission.getThirdLevelRate();
    }
    // 否则按照默认的推客佣金设置进行计算
    else {
      if (shopCommission != null) {
        commissionProductType = CommissionProductType.PER;
        firstRate = shopCommission.getFirstLevelRate();
        secondRate = shopCommission.getSecondLevelRate();
        thirdRate = shopCommission.getThirdLevelRate();
      }
    }

    if (commissionProductType == null) {
      return;
    }

    // 三级分佣对应的用户
    Long firstUserId = shopTreeService.selectParentUserIdByShopId(currentShopId, 0);
    Long secondUserId = shopTreeService.selectParentUserIdByShopId(currentShopId, 1);
    Long thirdUserId = shopTreeService.selectParentUserIdByShopId(currentShopId, 2);

    BigDecimal firstCmFee = BigDecimal.ZERO;
    BigDecimal secondCmFee = BigDecimal.ZERO;
    BigDecimal thirdCmFee = BigDecimal.ZERO;
    // 分佣类型按比例
    if (commissionProductType == CommissionProductType.PER) {
      firstCmFee = skuFee
          .multiply(BigDecimal.valueOf(firstRate).divide(B100, 4, BigDecimal.ROUND_DOWN))
          .setScale(2, BigDecimal.ROUND_DOWN);
      secondCmFee = skuFee
          .multiply(BigDecimal.valueOf(secondRate).divide(B100, 4, BigDecimal.ROUND_DOWN))
          .setScale(2, BigDecimal.ROUND_DOWN);
      thirdCmFee = skuFee
          .multiply(BigDecimal.valueOf(thirdRate).divide(B100, 4, BigDecimal.ROUND_DOWN))
          .setScale(2, BigDecimal.ROUND_DOWN);
    }
    // 分佣类型按固定金额
    else if (commissionProductType == CommissionProductType.AMOUNT) {
      firstCmFee = BigDecimal.valueOf(firstRate);
      secondCmFee = BigDecimal.valueOf(secondRate);
      thirdCmFee = BigDecimal.valueOf(thirdRate);
    }

    // 特定推客等级分佣逻辑加入,判断推客用户是否设置了特定等级
    if (firstUserId != null) {
      // 查询是否有配置推客等级，有的话，根据配置的等级计算佣金
      TwitterLevel twitterLevel = twitterLevelMapper.selectByUserId(rootShopId, "" + firstUserId);
      if (twitterLevel != null) {
        firstRate = (BigDecimal.valueOf(firstRate)
            .multiply(BigDecimal.valueOf(twitterLevel.getFirstLevelRate()))).doubleValue();
        firstCmFee = firstCmFee.multiply(BigDecimal.valueOf(twitterLevel.getFirstLevelRate()));
      }

      comm = new Commission(order.getId(), orderItem.getId(), orderItem.getSkuId(),
          orderItem.getPrice(),
          firstRate, CommissionType.ONECOMMISSION, orderItem.getAmount(), firstCmFee,
          IdTypeHandler.encode(firstUserId), CommissionStatus.NEW);
      commissionMapper.insert(comm);
    }

    if (secondUserId != null) {
      // 查询是否有配置推客等级，有的话，根据配置的等级计算佣金
      TwitterLevel twitterLevel = twitterLevelMapper.selectByUserId(rootShopId, "" + secondUserId);
      if (twitterLevel != null) {
        secondRate = (BigDecimal.valueOf(secondRate)
            .multiply(BigDecimal.valueOf(twitterLevel.getSecondLevelRate()))).doubleValue();
        secondCmFee = secondCmFee.multiply(BigDecimal.valueOf(twitterLevel.getSecondLevelRate()));
      }

      comm = new Commission(order.getId(), orderItem.getId(), orderItem.getSkuId(),
          orderItem.getPrice(),
          secondRate, CommissionType.TWOCOMMISSION, orderItem.getAmount(), secondCmFee,
          IdTypeHandler.encode(secondUserId), CommissionStatus.NEW);
      commissionMapper.insert(comm);
    }

    if (thirdUserId != null) {
      // 查询是否有配置推客等级，有的话，根据配置的等级计算佣金
      TwitterLevel twitterLevel = twitterLevelMapper.selectByUserId(rootShopId, "" + thirdUserId);
      if (twitterLevel != null) {
        thirdRate = (BigDecimal.valueOf(thirdRate)
            .multiply(BigDecimal.valueOf(twitterLevel.getThirdLevelRate()))).doubleValue();
        thirdCmFee = thirdCmFee.multiply(BigDecimal.valueOf(twitterLevel.getThirdLevelRate()));
      }

      comm = new Commission(order.getId(), orderItem.getId(), orderItem.getSkuId(),
          orderItem.getPrice(),
          thirdRate, CommissionType.THREECOMMISSION, orderItem.getAmount(), thirdCmFee,
          IdTypeHandler.encode(thirdUserId), CommissionStatus.NEW);
      commissionMapper.insert(comm);
    }
    log.info("order[" + order.getOrderNo() + "] orderItem [" + orderItem.getId()
        + "]  of  three commission (product) calculation has finished.");

  }

  /**
   * 计算合伙人分佣
   */
  public void calcPartnerCommission(Order order, OrderItem orderItem, BigDecimal skuFee) {
    Commission comm = null;

    String rootShopId = shopTreeService.selectRootShopByShopId(order.getShopId()).getRootShopId();
    String currentShopId = order.getShopId();

    commissionMapper.cleanCommissionByOrderIdAndType(order.getId(), CommissionType.TEAM);
    commissionMapper.cleanCommissionByOrderIdAndType(order.getId(), CommissionType.PLATFORM);
    commissionMapper.cleanCommissionByOrderIdAndType(order.getId(), CommissionType.SHAREHOLDER);

    // 团队合伙人分红
    BigDecimal teamDe = null;
    // 	股东合伙人分红
    BigDecimal shareholderDe = null;
    // 平台合伙人分红
    BigDecimal platformDe = null;

    BigDecimal teamCmFee = BigDecimal.ZERO;
    BigDecimal shareholderCmFee = BigDecimal.ZERO;
    BigDecimal platformCmFee = BigDecimal.ZERO;

    // 判断是否有配置该商品的合伙人佣金设置
    List<PartnerProductCommissionDe> productCommissionDes = partnerProductCommissionDeMapper
        .getByProductId(orderItem.getProductId());
    if (productCommissionDes != null) {
      for (PartnerProductCommissionDe de : productCommissionDes) {
        if (de.getType() == CommissionType.PLATFORM) {
          platformDe = de.getRate();
        }
        if (de.getType() == CommissionType.TEAM) {
          teamDe = de.getRate();
        }
        if (de.getType() == CommissionType.SHAREHOLDER) {
          shareholderDe = de.getRate();
        }
      }
    }
    // 商品没有特殊配置，则取系统默认的合伙人分红设置
    else {
      List<PartnerShopCommission> shopCommissions = partnerShopCommissionMapper
          .selectByShopId(rootShopId);
      for (PartnerShopCommission de : shopCommissions) {
        if (de.getType() == CommissionType.PLATFORM) {
          platformDe = de.getRate();
        }
        if (de.getType() == CommissionType.TEAM) {
          teamDe = de.getRate();
        }
        if (de.getType() == CommissionType.SHAREHOLDER) {
          shareholderDe = de.getRate();
        }
      }
    }

    // 首先查找卖家推客上级是否有团队合伙人，如果有，则第一个团队合伙人进行团队合伙人分佣逻辑
    ShopTree partnerShop = shopTreeMapper.getDirectPartner(rootShopId, currentShopId);
    if (partnerShop != null) {
      String partnerShopId = partnerShop.getAncestorShopId();
      Shop shop = shopService.load(partnerShopId);
      String partnerUserId = shop.getOwnerId();

      // 获取该合伙人的团队类型，如果有特别设置类型，则分红比例需要乘以系数，否则按默认比例计算
      PartnerType partnerType = partnerTypeMapper.selectTeamByUserId(partnerUserId);
      if (partnerType != null && teamDe != null) {
        teamDe = teamDe.multiply(partnerType.getRate());
      }

      if (teamDe != null) {
        teamCmFee = skuFee.multiply(teamDe.divide(B100, 4, BigDecimal.ROUND_DOWN))
            .setScale(2, BigDecimal.ROUND_DOWN);
        comm = new Commission(order.getId(), orderItem.getId(), orderItem.getSkuId(),
            orderItem.getPrice(),
            teamDe.doubleValue(), CommissionType.TEAM, orderItem.getAmount(), teamCmFee,
            partnerUserId, CommissionStatus.NEW);
        commissionMapper.insert(comm);
      }
    }

    // 查找系统内所有的股份合伙人和平台合伙人，这些人所有的订单都是能分红的
    List<PartnerTypeRelationVO> allPlatformAndHolder = partnerTypeRelationMapper
        .getAllPlatformAndHolder();
    for (PartnerTypeRelationVO partnerTypeRelationVO : allPlatformAndHolder) {
      String userId = partnerTypeRelationVO.getUserId();
      BigDecimal rate = partnerTypeRelationVO.getRate();
      CommissionType type = partnerTypeRelationVO.getType();
      // 如果设置的合伙人类型为默认，则比例取合伙人的默认比例
      // 否则则计算加倍的比例
      if (type == CommissionType.SHAREHOLDER && shareholderDe != null) {
        if (rate == null) {
          rate = shareholderDe;
        } else {
          rate = rate.multiply(shareholderDe);
        }
      }
      if (type == CommissionType.PLATFORM && platformDe != null) {
        if (rate == null) {
          rate = platformDe;
        } else {
          rate = rate.multiply(platformDe);
        }
      }

      // 插入系统内所有的股份合伙人和平台合伙人分佣数据
      if (rate != null) {
        teamCmFee = skuFee.multiply(rate.divide(B100, 4, BigDecimal.ROUND_DOWN))
            .setScale(2, BigDecimal.ROUND_DOWN);
        comm = new Commission(order.getId(), orderItem.getId(), orderItem.getSkuId(),
            orderItem.getPrice(),
            rate.doubleValue(), type, orderItem.getAmount(), teamCmFee, userId,
            CommissionStatus.NEW);
        commissionMapper.insert(comm);
      }

    }

    log.info("order[" + order.getOrderNo() + "] orderItem [" + orderItem.getId()
        + "]  of  three commission (partner) calculation has finished.");

  }

  /**
   * 计算推客自己购买商品佣金
   */
  public void calcTwitterCommission(Order order, OrderItem orderItem, BigDecimal skuFee) {
    Commission comm = null;

    String rootShopId = shopTreeService.selectRootShopByShopId(order.getShopId()).getRootShopId();
    String currentShopId = shopService.findByUser(order.getBuyerId()).getId();

    TwitterProductCommission productCommission = twitterProductComService
        .selectComByProductIdAndShopId(orderItem.getProductId(), rootShopId);
    TwitterShopCommission shopCommission = twitterShopComService.selectDefaultByshopId(rootShopId);

    commissionMapper.cleanCommissionByOrderIdAndType(order.getId(), CommissionType.PRODUCT);

    // 分佣类型，按比例或按固定金额等
    CommissionProductType commissionProductType = CommissionProductType.PER;
    double firstRate = 0.0;

    // 如果该商品有配置商品佣金，则按照配置的商品佣金进行计算
    if (productCommission != null) {
      commissionProductType = productCommission.getType();
      firstRate = productCommission.getFirstLevelRate();
    }
    // 否则按照默认的推客佣金设置进行计算
    else {
      if (shopCommission != null) {
        commissionProductType = CommissionProductType.PER;
        firstRate = shopCommission.getFirstLevelRate();
      }
    }

    if (commissionProductType == null) {
      return;
    }

    // 推客对应的用户
    Long firstUserId = shopTreeService.selectParentUserIdByShopId(currentShopId, 0);

    BigDecimal firstCmFee = BigDecimal.ZERO;
    // 分佣类型按比例
    if (commissionProductType == CommissionProductType.PER) {
      firstCmFee = skuFee
          .multiply(BigDecimal.valueOf(firstRate).divide(B100, 4, BigDecimal.ROUND_DOWN))
          .setScale(2, BigDecimal.ROUND_DOWN);
    }
    // 分佣类型按固定金额
    else if (commissionProductType == CommissionProductType.AMOUNT) {
      firstCmFee = BigDecimal.valueOf(firstRate);
    }

    // 特定推客等级分佣逻辑加入,判断推客用户是否设置了特定等级
    if (firstUserId != null) {
      // 查询是否有配置推客等级，有的话，根据配置的等级计算佣金
      TwitterLevel twitterLevel = twitterLevelMapper.selectByUserId(rootShopId, "" + firstUserId);
      if (twitterLevel != null) {
        firstRate = (BigDecimal.valueOf(firstRate)
            .multiply(BigDecimal.valueOf(twitterLevel.getFirstLevelRate()))).doubleValue();
        firstCmFee = firstCmFee.multiply(BigDecimal.valueOf(twitterLevel.getFirstLevelRate()));
      }

      comm = new Commission(order.getId(), orderItem.getId(), orderItem.getSkuId(),
          orderItem.getPrice(),
          firstRate, CommissionType.PRODUCT, orderItem.getAmount(), firstCmFee,
          IdTypeHandler.encode(firstUserId), CommissionStatus.NEW);
      commissionMapper.insert(comm);
    }

    log.info("order[" + order.getOrderNo() + "] orderItem [" + orderItem.getId()
        + "]  of  three commission (product) calculation has finished.");

  }

}
