package com.xquark.service.union;

import java.util.ArrayList;
import java.util.List;

import com.xquark.dal.model.Commission;

/**
 * @author ahlon
 */
public interface UnionService {

  /**
   * 记录分账数据
   */
  void calc(String orderId);

  /**
   * 记录金银卡分账数据
   */
  void calcWechatCommission(String orderId);

  /**
   * 记录代理商分账数据
   */
  void calcUserAgentCommission(String orderId);

  /**
   * 执行分账，涉及到卖家账户，推广者账户
   */
  void onSuccess(String orderId);

  /**
   * 取消
   */
  void onCancel(String orderId);

  Commission loadByOrderItem(String id);

  Commission loadByOrderItemAndUserId(String id, String userId);

  List<Commission> listByOrderId(String orderId);

  Long autoCommissionToAvailable();

  Boolean processCommissionEntry(Commission cm, String payNo);

  void distribution(String orderId);

  Long autoCommissionToAvailableTest();
}
