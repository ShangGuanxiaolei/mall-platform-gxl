package com.xquark.service.lottery;

import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.mapper.PromotionConfigMapper;
import com.xquark.dal.mapper.PromotionLotteryMapper;
import com.xquark.dal.model.PromotionConfig;
import com.xquark.dal.model.PromotionList;
import com.xquark.dal.model.PromotionLottery;
import com.xquark.dal.model.WinningItem;
import com.xquark.service.base.RainLotteryJobService;
import com.xquark.service.cache.GuavaCacheManager;
import com.xquark.utils.DateUtils;
import io.vavr.Tuple2;
import io.vavr.control.Either;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.neo4j.cypher.internal.compiler.v2_1.ast.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.xquark.dal.consts.RainLotteryConstrants.*;

/**
 * @author wangxinhua
 * @date 2019-04-28
 * @since 1.0
 */
@Service
public class LotteryJobService extends RainLotteryJobService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private GuavaCacheManager guavaCacheManager;

    @Autowired
    private PromotionLotteryMapper promotionLotteryMapper;

    @Autowired
    private PromotionConfigMapper promotionConfigMapper;

    @Autowired
    private LotteryService lotteryService;

    private final static double HOT_PERCENT = 0.8;


    // 映射奖品的时间分布, 只用于520活动
//    private final  Tuple2<LocalTime, LocalTime> FIRST_PRIZE_DISTRIBUTION
//            // 一等奖分布在19点到21点, 在19点到20点之间把奖品发完
//            = new Tuple2<>(LocalTime.of(prizeStartTime, 0), LocalTime.of(prizeEndTime, 0));

    // 除一等奖外百分之80的奖品在这个时间段发放
    private final Tuple2<LocalTime, LocalTime> MOST_PRIZE_DISTRIBUTION
            = new Tuple2<>(LocalTime.of(12, 0), LocalTime.of(18, 59));

    private final  Tuple2<LocalTime, LocalTime> ALL_DAY_PRIZE_DISTRIBUTION
            // 发到十点为止
            = new Tuple2<>(LocalTime.of(10, 0), LocalTime.of(23, 59));

    /**
     * 往Go端的奖品队列推送奖品
     * @param force 强制发放
     */
    public Either<String, Boolean> pushPrices(boolean force) {
        if (this.isNotActivity(LOTTERY)) {
            return Either.left("不在抽奖时间段内");
        }
        final Date now = new Date();
        final Date endOfToday = DateUtils.toDate(LocalDateTime.of(LocalDate.now(),
                ALL_DAY_PRIZE_DISTRIBUTION._2));
        if (now.after(endOfToday) && !force) {
            // 超过当天最大的派发时间后则停止往队里里面发放
            return Either.left("超过今日派发最大时间, 不再派发奖品");
        }
        final PromotionLottery promotionLottery = promotionLotteryMapper.selectTodayLotteryActivityLimitVO();
        if (promotionLottery == null) {
            log.info("未找到今日抽奖[{}]活动配置跳过...", LocalDateTime.now());
            return Either.left("未找到今日抽奖活动配置跳过...");
        }
        final String promotionId = promotionLottery.getId();
        final String winningListKey = WINNING_LIST.apply(promotionId);
        final String winningListItemKey = WINNING_LIST_ITEM.apply(promotionId);

        final RedisUtils<Object> redisUtils = RedisUtilFactory.getInstance(Object.class);

        if (!redisUtils.hasKey(winningListItemKey) && !force) {
            return Either.left("奖品队列未初始化");
        }

        // 获取在当前时间段之前的奖品
        List<Integer> itemsToPush = redisUtils.lPopWhile(winningListItemKey, o -> {
            // TODO 解决类型丢失的问题
            final LinkedHashMap item = (LinkedHashMap) o;
            final long expectTimeStamp = (long) item.get("expectTime");
            final Date expect = new Date(expectTimeStamp);
            final int prize = (int) item.get("prize");
            final boolean isHit = expect.before(now);
            if (isHit) {
                log.info("{}等奖, 预计发放时间-> {} 发放到奖品队列", prize, formatter.format(expect));
            }
            return isHit;
        }).stream().map(o -> {
            final LinkedHashMap item = (LinkedHashMap) o;
            return (int) item.get("prize");
        }).collect(Collectors.toList());

        String formattedDate = formatter.format(now);
        if (CollectionUtils.isEmpty(itemsToPush)) {
            return Either.left("当前时段 -> " + formattedDate + " 没有奖品");
        }

        final Date endOfDay = DateUtils.getEndOfDay(promotionLottery.getPromotionDate());
        final long expiredTime = Duration.between(now.toInstant(), endOfDay.toInstant()).toMillis();
        final RedisUtils<Integer> intRedis = RedisUtilFactory.getInstance(Integer.class);
        intRedis.leftPushAllList(winningListKey, itemsToPush);
        intRedis.expire(winningListKey, expiredTime, TimeUnit.MILLISECONDS);
        return Either.right(Boolean.TRUE);
    }

    /**
     * 推送奖品
     *
     * @param force 是否强制刷新
     */
    public Either<String, Boolean> initPrices(boolean force) {
        //一等奖开始发放时间
        final Integer prizeStartTime = this.configFisrtPrizeTime(
                new HashMap() {{
                    put("configType", "Lottery");
                    put("configName", "prizeStartTime");
                }});
        //一等奖结束发放时间
        final Integer prizeEndTime = this.configFisrtPrizeTime(
                new HashMap() {{
                    put("configType", "Lottery");
                    put("configName", "prizeEndTime");
                }});

        if (this.isNotActivity(LOTTERY)) {
            return Either.left("不在抽奖时间段内");
        }

        final PromotionLottery promotionLottery = promotionLotteryMapper.selectTodayLotteryActivityLimit();
        if (Objects.isNull(promotionLottery)) {
            log.info("未找到今日抽奖[{}]活动配置跳过...", LocalDateTime.now());
            return Either.left("未找到今日抽奖活动配置跳过...");
        }

        final String promotionId = promotionLottery.getId();
        final RedisUtils<Object> redisUtils = RedisUtilFactory.getInstance(Object.class);
        final String winningListItemKey = WINNING_LIST_ITEM.apply(promotionId);
        final boolean hasKey = redisUtils.hasKey(winningListItemKey);
        if (hasKey && !force) {
            return Either.left("活动id: " + promotionId + "奖品配置已初始化");
        }

        final List<PrizeProb> prizeProbList = lotteryService.listPrizeProp(promotionLottery.getId());
        final Date now = new Date();
        final Date endOfDay = DateUtils.getEndOfDay(promotionLottery.getPromotionDate());
        final long expiredTime = Duration.between(now.toInstant(), endOfDay.toInstant()).toMillis();
        // 非强制刷新, 删除奖品列表重新push
        redisUtils.del(winningListItemKey);

        // 活动日期
        final LocalDate promotionDate = DateUtils.toLocalDateTime(promotionLottery.getPromotionDate())
                .toLocalDate();

        // 不同的奖品放到不同的时间段
        final List<WinningItem> winningItemList = prizeProbList.stream()
                .flatMap(prop -> {
                    // TODO 需要考虑数据库里面的amount值是累加还是只取单个
                    final int prize = prop.getPrize();
                    final int amt = prop.getAmount();
                    if (prize == 1) {
                        return buildTimeDistribution(promotionDate,
                                new Tuple2<>(LocalTime.of(prizeStartTime, 0), LocalTime.of(prizeEndTime, 0)),
                                prize, amt);
                    }
                    // 在热门时段发放的奖品
                    final int hotAmt = (int) (amt * HOT_PERCENT);
                    // 剩余时段发放普通奖品
                    final int restAmt = amt - hotAmt;
                    return Stream.concat(buildTimeDistribution(promotionDate, MOST_PRIZE_DISTRIBUTION, prize, hotAmt),
                            buildTimeDistribution(promotionDate, ALL_DAY_PRIZE_DISTRIBUTION, prize, restAmt));
                })
                // 使用lPush, Go端封装的也是lPop, 需要反转顺序
                .sorted(Comparator.comparing(WinningItem::getExpectTime).reversed())
                .collect(Collectors.toList());

        redisUtils.leftPushAllList(winningListItemKey, winningItemList);
        redisUtils.expire(winningListItemKey, expiredTime, TimeUnit.MILLISECONDS);

        winningItemList.forEach(item -> log.info("奖品 {}, 预计发放时间 {} 已推送到待发放队列",
                item.getPrize(), formatter.format(item.getExpectTime())));

        return Either.right(Boolean.TRUE);
    }

    /**
     * 构造奖品的时间分布数组
     *
     * @param date         活动日期
     * @param distribution 时间分布
     * @param prize        奖品等级
     * @param amt          奖品数量
     * @return 奖品数组
     */
    private Stream<WinningItem> buildTimeDistribution(LocalDate date, Tuple2<LocalTime, LocalTime> distribution, int prize, int amt) {
        if (amt == 0) {
            return Stream.empty();
        }
        final List<WinningItem> ret = new ArrayList<>();
        final LocalTime start = distribution._1;
        final LocalTime end = distribution._2;
        // 每隔几分钟发一次奖品
        final long step = Duration.between(start, end).getSeconds() / amt;
        LocalTime init = start;
        int pushed = 0;
        // 由于精度问题可能会往后延部分
        while (init.isBefore(end) || pushed < amt) {
            if (pushed >= amt) {
                // 控制推多了
                break;
            }
            final Date expect = DateUtils.toDate(LocalDateTime.of(date, init));
            ret.add(new WinningItem(prize, expect));
            init = init.plusSeconds(step);
            pushed++;
        }
        return ret.stream();
    }

    /**
     * 初始化抽奖活动redis
     */
    public Either<String, Boolean> init() {

        if (isNotActivity(LOTTERY)) {
            return Either.left("不在活动时间范围内");
        }

        final PromotionLottery promotionLottery = promotionLotteryMapper.selectTodayLotteryActivityLimit();
        if (promotionLottery == null) {
            return Either.left("今天没有抽奖活动");
        }

        final String promotionId = promotionLottery.getId();

        PromotionList promotion = promotionListMapper.selectByPrimaryKey(promotionLottery.getPromotionListId());
        if (promotion == null) {
            final String msg = String.format("未找到今日[%s]活动配置跳过...",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDateTime.now()));
            log.info(msg);
            return Either.left(msg);
        }

        RedisUtils<Integer> intRedis = RedisUtilFactory.getInstance(Integer.class);
        RedisUtils<List> listRedis = RedisUtilFactory.getInstance(List.class);
        //
        final Date now = new Date();
        final long expireAt = Duration.between(now.toInstant(), promotion.getTriggerEndTime().toInstant())
                .toMillis();
        final String lotteryProbabilityKey = LOTTERY_PROBABILITY.apply(promotionId);

        List<ScoreProb> scoreProbList = lotteryService.listScoreProp(promotionLottery.getId());

        @SuppressWarnings("unchecked")
        List<ScoreProb> redisProbList = listRedis.get(lotteryProbabilityKey);
        if (CollectionUtils.isEmpty(redisProbList)) {
            // FIXME 潜在bug: 过期时间, 因为这个key一直会更新, 所以过期时间会一直往后推
            listRedis.setAndExpire(lotteryProbabilityKey, scoreProbList,
                    expireAt, TimeUnit.MILLISECONDS);
        } else {
            // 已存在则替换
            listRedis.set(lotteryProbabilityKey, scoreProbList);
        }

        final String lotteryTimesKey = LOTTERY_TIMES.apply(promotionId);
        Integer lotteryTimes = intRedis.get(lotteryTimesKey);
        if (lotteryTimes == null || !lotteryTimes.equals(promotionLottery.getConsumptionScore())) {
            lotteryTimes = promotionLottery.getLotteryTimes();
            intRedis.setAndExpire(lotteryTimesKey, lotteryTimes,
                    expireAt, TimeUnit.MILLISECONDS);
        }

        return Either.right(Boolean.TRUE);
    }

    /**
     * 通过guavaCache缓存红包雨配置
     */
    public PromotionLottery cachedPromotionLottery() {
        PromotionLottery promotionLottery;
        promotionLottery = (PromotionLottery) guavaCacheManager.get("promotionLottery");
        if (promotionLottery == null) {
            promotionLottery = promotionLotteryMapper.selectTodayLotteryActivityLimit();
            guavaCacheManager.putIfExpire("promotionLottery", promotionLottery);
        }
        return promotionLottery;
    }

    /**
     * 开始分配当天的抽奖信息
     */
    private void activitySession(List<PrizeProb> prizeProbList) {
        prizeProbList.forEach(prizeProb -> {
            final int prize = prizeProb.getPrize();
            log.info("=========初始化级别奖品[{}]=============", prize);
            ordinaryRankHandle(prizeProb.getAmount(), getOrdinarySessionRankKey(prize));
            activityRankHandle(prizeProb.getAmount(), getActivitySessionRankKey(prize));
        });
    }

    /**
     * 非红包雨期间发放的奖品
     *
     * @param rankInt 总奖品
     * @param key     非红包雨期间发放的奖品Key
     */
    private void ordinaryRankHandle(Integer rankInt, String key) {
        if (rankInt >= MIN_LIMIT) {
            RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
            BigDecimal rankDecimal = BigDecimal.valueOf(rankInt).
                    multiply(BigDecimal.valueOf(0.1)).setScale(0, BigDecimal.ROUND_HALF_UP);
            LotteryProbabilityStrategy lotteryProbabilityStrategy = new LotteryProbabilityStrategy();
            Integer[] integers = lotteryProbabilityStrategy.obtainAmount(rankDecimal, 10);
            if (ArrayUtils.isNotEmpty(integers)) {
                redisUtils.leftPushAll(key, integers);
                redisUtils.expire(key, 86390, TimeUnit.SECONDS);
            }
        } else {
            log.info("[{}]-奖品小于5普通场次不计算", key);
        }
    }

    /**
     * 红包雨期间发放的奖品
     *
     * @param rankInt 总奖品
     * @param key     红包雨期间发放的奖品Key
     */
    private void activityRankHandle(Integer rankInt, String key) {
        RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
        BigDecimal rankDecimal = BigDecimal.valueOf(rankInt);
        if (rankInt >= MIN_LIMIT) {
            rankDecimal = BigDecimal.valueOf(rankInt).
                    multiply(BigDecimal.valueOf(0.9)).setScale(0, BigDecimal.ROUND_HALF_DOWN);
        }
        LotteryProbabilityStrategy lotteryProbabilityStrategy = new LotteryProbabilityStrategy();
        Integer[] integers = lotteryProbabilityStrategy.obtainAmount(rankDecimal, 3);
        if (ArrayUtils.isNotEmpty(integers)) {
            redisUtils.leftPushAll(key, integers);
            redisUtils.expire(key, 86390, TimeUnit.SECONDS);
        }
    }

    /**
     * 活动内再次细分8次
     */
    private void activityRankPart(Integer rankInt, String key, long expire) {
        RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
        BigDecimal rankDecimal = BigDecimal.valueOf(rankInt);
        LotteryProbabilityStrategy lotteryProbabilityStrategy = new LotteryProbabilityStrategy();
        Integer[] integers = lotteryProbabilityStrategy.obtainAmount(rankDecimal, 8);
        if (ArrayUtils.isNotEmpty(integers)) {
            redisUtils.leftPushAll(key, integers);
            redisUtils.expire(key, 1, TimeUnit.HOURS);
        }
    }

    private Integer configFisrtPrizeTime(Map map){
        Integer hour = new Integer(0);
        PromotionConfig promotionConfig = promotionConfigMapper.selectByConfig(map);
        if(null!=promotionConfig){
            hour=Integer.valueOf(promotionConfig.getConfigValue());
        }
        return hour;
    }

//    public static void main(String[] args) {
//        final Date date = DateUtils.toDate(LocalDateTime.of(LocalDate.now(),
//                ALL_DAY_PRIZE_DISTRIBUTION._2));
//        System.out.println(formatter.format(date));
//    }
}
