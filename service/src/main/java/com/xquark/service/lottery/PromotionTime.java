package com.xquark.service.lottery;

import java.util.Date;
import java.util.Objects;

/**
 * @author Jack Zhu
 * @date 2019/03/21
 */
public class PromotionTime {

    private String id;
    private Date startTime;
    private Date endTime;

    public PromotionTime() {
    }

    public PromotionTime(String id, Date startTime, Date endTime) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getId() {
        return id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    @Override
    public String toString() {
        return "PromotionTime{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PromotionTime that = (PromotionTime) o;
        return id.equals(that.id) &&
                startTime.equals(that.startTime) &&
                endTime.equals(that.endTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, startTime, endTime);
    }
}
