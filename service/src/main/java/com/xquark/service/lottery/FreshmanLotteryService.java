package com.xquark.service.lottery;

import com.xquark.dal.model.FreshmanLottery;

import java.util.List;

public interface FreshmanLotteryService {

    List<FreshmanLottery> getAllFreshmanLotteryIsOpen();

    Integer insertFreshmanLotteryLog(Integer currentCpid, Integer point);

    Integer queryFreshmanLotteryCountByCpid(Long currentCpid);
}
