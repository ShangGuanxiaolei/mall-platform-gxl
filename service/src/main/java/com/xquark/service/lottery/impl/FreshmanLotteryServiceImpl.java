package com.xquark.service.lottery.impl;

import com.xquark.dal.mapper.FreshmanLotteryMapper;
import com.xquark.dal.model.FreshmanLottery;
import com.xquark.service.lottery.FreshmanLotteryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FreshmanLotteryServiceImpl implements FreshmanLotteryService {

    @Autowired
    private FreshmanLotteryMapper freshmanLotteryMapper;

    @Override
    public List<FreshmanLottery> getAllFreshmanLotteryIsOpen() {
        return freshmanLotteryMapper.getAllFreshmanLotteryIsOpen();
    }

    @Override
    public Integer insertFreshmanLotteryLog(Integer currentCpid, Integer point) {
        return freshmanLotteryMapper.insertFreshmanLotteryLog(currentCpid, point);
    }

    @Override
    public Integer queryFreshmanLotteryCountByCpid(Long currentCpid) {
        return freshmanLotteryMapper.queryFreshmanLotteryCountByCpid(currentCpid);
    }
}
