package com.xquark.service.lottery;

import java.util.Objects;

public class ScoreProb {
    private Integer score;
    private Integer probability;

    ScoreProb(Integer score, Integer probability) {
        Objects.requireNonNull(score);
        Objects.requireNonNull(probability);
        this.score = score;
        this.probability = probability;
    }

    ScoreProb() {
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getProbability() {
        return probability;
    }

    public void setProbability(Integer probability) {
        this.probability = probability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScoreProb scoreProb = (ScoreProb) o;
        return score.equals(scoreProb.score) &&
                probability.equals(scoreProb.probability);
    }

    @Override
    public int hashCode() {
        return Objects.hash(score, probability);
    }
}