package com.xquark.service.lottery;

import com.vdlm.common.lang.ArrayUtil;

import java.math.BigDecimal;
import java.util.Random;

/**
 * 奖品随机算法
 *
 * @author hs
 * @since 2019/1/27
 */
public class LotteryProbabilityStrategy {

    private Random r = new Random();

    public Integer[] obtainAmount(BigDecimal aggregateAmount, Integer peopleAmount) {

        if (0 == BigDecimal.ZERO.compareTo(aggregateAmount)){
            return ArrayUtil.EMPTY_INTEGER_OBJECT_ARRAY;
        }

        LeftMoneyPackage leftMoneyPackage = new LeftMoneyPackage(peopleAmount, aggregateAmount.intValue());
        Integer[] amounts;

        if (aggregateAmount.intValue() >= peopleAmount) {
            amounts = new Integer[leftMoneyPackage.remainSize];
            while (leftMoneyPackage.remainSize != 0) {
                final double randomMoney = getRandomMoneyA(leftMoneyPackage);
                amounts[leftMoneyPackage.remainSize] = (int) randomMoney;
            }
        }else {
            amounts = new Integer[leftMoneyPackage.remainMoney];
            while(leftMoneyPackage.remainMoney != 0) {
                amounts[leftMoneyPackage.remainMoney-1] = 1;
                leftMoneyPackage.remainMoney--;
            }
        }
        return amounts;
    }

    /**
     * 微信红包的具体算法
     */
    private double getRandomMoneyA(LeftMoneyPackage leftMoneyPackage) {
        if (leftMoneyPackage.remainSize == 1) {
            leftMoneyPackage.remainSize--;
            return (double) Math.round(leftMoneyPackage.remainMoney * 1) / 1;
        }
        double min = 1;
        double max = leftMoneyPackage.remainMoney / leftMoneyPackage.remainSize * 2;
        double money = r.nextDouble() * max;
        money = money <= min ? 1 : money;
        money = Math.floor(money * 1) / 1;
        leftMoneyPackage.remainSize--;
        leftMoneyPackage.remainMoney -= money;
        return money;
    }

    private static class LeftMoneyPackage {
        private int remainSize;
        private int remainMoney;

        LeftMoneyPackage(int remainSize, int remainMoney) {
            this.remainSize = remainSize;
            this.remainMoney = remainMoney;
        }

        @Override
        public String toString() {
            return "LeftMoneyPackage{" +
                    "remainSize=" + remainSize +
                    ", remainMoney=" + remainMoney +
                    '}';
        }
    }

    public static void main(String[] args) {
        int value = 3;
        int clockTimes = 3;
        if(value >= clockTimes) {
            LotteryProbabilityStrategy lotteryProbabilityStrategy = new LotteryProbabilityStrategy();
            Integer[] integers = lotteryProbabilityStrategy.obtainAmount(new BigDecimal(3), 3);
            int lCount = 0;
            for (Integer integer : integers) {
                System.out.println("时间[" + lCount++ + "]" + "===" + "分的普通商品数量[" + integer + "]");
            }
        }else {
            long l = 1;
            while(value>0) {
                System.out.println("平均推..."+l++);
                value--;
            }
        }
    }
}
