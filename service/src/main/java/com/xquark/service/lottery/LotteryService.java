package com.xquark.service.lottery;

import com.xquark.dal.mapper.PromotionLotteryProbabilityMapper;
import com.xquark.dal.model.vo.LotteryPrizeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wangxinhua
 * @date 2019-04-28
 * @since 1.0
 */
@Service
public class LotteryService {

    private final PromotionLotteryProbabilityMapper lotteryProbabilityMapper;

    @Autowired
    public LotteryService(PromotionLotteryProbabilityMapper lotteryProbabilityMapper) {
        this.lotteryProbabilityMapper = lotteryProbabilityMapper;
    }

    /**
     * 获取前端展示的德分抽奖数据
     *
     * @param id 活动id
     * @return 抽奖数据列表
     */
    public List<ScoreProb> listScoreProp(String id) {
        return lotteryProbabilityMapper.listByPromotionLotteryId(id, LotteryPrizeType.SOCRE.getCode())
                .stream()
                .map(p -> new ScoreProb(p.getPrize(), p.getProbability().intValue()))
                .collect(Collectors.toList());
    }

    /**
     * 获取前端展示的奖品抽奖数据
     *
     * @param id 活动id
     * @return 抽奖数据列表
     */
    public List<PrizeProb> listPrizeProp(String id) {
        return lotteryProbabilityMapper.listByPromotionLotteryId(id, LotteryPrizeType.PRODUC.getCode())
                .stream()
                .map(p -> new PrizeProb(p.getPrize(), p.getAmount()))
                .collect(Collectors.toList());
    }
}
