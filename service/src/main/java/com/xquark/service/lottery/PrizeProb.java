package com.xquark.service.lottery;

import java.util.Objects;

/**
 * @author Jack Zhu
 * @date 2019/03/13
 */
public class PrizeProb {
    private int prize;
    private int amount;

    public PrizeProb(Integer prize, Integer amount) {
        Objects.requireNonNull(prize);
        Objects.requireNonNull(amount);
        this.prize = prize;
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getPrize() {
        return prize;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }
}
