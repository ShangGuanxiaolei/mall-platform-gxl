package com.xquark.service.orderHeader;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderHeader;

import java.util.List;

/**
 * User: kong Date: 2018/6/28. Time: 10:52
 */
public interface OrderHeaderService {

  int updateStatusByOrderNo(String status, String orderNo);

  /**
   * 更新订单业绩计算月份
   *
   * @param otherMonth
   * @param orderNo
   * @return
   */
  int updateOtherMonthByOrderNo(int otherMonth, String orderNo);

  void syncOrderHeaderStatus(Order order);

  void syncOrderHeaderStatus(OrderHeader toUpdate);

  Long loadFromCpId(String orderNo);

  List<OrderHeader> selectWhiteOrderList(String toDay);

  /**
   * 根据订单编号修改加入类型
   *
   * @param orderNo 主订单编号
   * @return 更新是否成功（大于0）
   */
  int updateJoinTypeByOrderNoForPiece(String orderNo, String cpId);


  int updateJoinTypeByOrderNo(int joinType,String orderNo);
  Integer selectJoinTypeByOrderNo(String orderNo);
}