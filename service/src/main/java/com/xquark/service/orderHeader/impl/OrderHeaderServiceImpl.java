package com.xquark.service.orderHeader.impl;

import com.xquark.dal.mapper.OrderHeaderMapper;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderHeader;
import com.xquark.dal.status.OrderHeaderStatusMapping;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.OrderSortType;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.orderHeader.OrderHeaderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import static com.google.common.base.Preconditions.checkState;
import static org.parboiled.common.Preconditions.checkNotNull;

/**
 * User: kong Date: 2018/6/28. Time: 10:53
 */
@Service
@Transactional
public class OrderHeaderServiceImpl extends BaseServiceImpl implements OrderHeaderService {

  @Autowired
  private OrderHeaderMapper orderHeaderMapper;

  @Autowired
  private OrderMapper orderMapper;

  @Override
  public int updateStatusByOrderNo(String status, String orderNo) {
    return orderHeaderMapper.updateStatusByOrderNo(status, orderNo);
  }

  @Override
  public int updateOtherMonthByOrderNo(int otherMonth, String orderNo) {
    return orderHeaderMapper.updateOtherMonthByOrderNo(otherMonth, orderNo);
  }

  /**
   * 同步同步推送订单状态 TODO 事务问题
   */
  @Override
  public void syncOrderHeaderStatus(Order order) {
    Order o = orderMapper.selectByOrderNo(order.getOrderNo());
    if (o == null) {
      return;
    }
    OrderStatus orderStatus = o.getStatus();
    // 特殊处理拼团的逻辑, 因为PAIDNOSTOCK状态跟大会冲突
    boolean isPieceNoStock = OrderSortType.isPiece(order.getOrderType()) && Objects.equals(orderStatus,
            OrderStatus.PAIDNOSTOCK);
    int status = isPieceNoStock ? OrderHeaderStatusMapping.PIECE_PAID_NOSTOCK
            : OrderHeaderStatusMapping.mapping(orderStatus);
    OrderHeader headerToUpdate = new OrderHeader();
    headerToUpdate.setOrderid(order.getOrderNo());
    if (status == OrderHeaderStatusMapping.NOT_MATCH) {
      // 特殊处理, CLOSED可能映射到orderHeader的两个状态
      if (orderStatus != OrderStatus.CLOSED) {
        // 一小时后退款, 更新为11
        return;
      }
      // 汉薇订单没有一小时内取消与退款状态, 需要手动判断映射
      if (o.isRefundAfterOneHour()) {
        status = OrderHeaderStatusMapping.CANCELLED;
      } else {
        status = OrderHeaderStatusMapping.CLOSED;
      }
    }
    headerToUpdate.setStatus(status);
    if (orderStatus == OrderStatus.PAID || orderStatus == OrderStatus.PAIDNOSTOCK) {
      headerToUpdate.setPaytime(new Date());
      headerToUpdate.setPaytimeFinish(new Date());
    }
    syncOrderHeaderStatus(headerToUpdate);
  }

  @Override
  public void syncOrderHeaderStatus(OrderHeader toUpdate) {
    checkNotNull(toUpdate);
    String orderNo = toUpdate.getOrderid();
    checkState(StringUtils.isNotBlank(orderNo), "订单编号不能为空");
    try {
      int effected = orderHeaderMapper.updateStatusSelective(toUpdate);
      if (effected == 0) {
        log.warn("订单{} 在order header表中不存在", orderNo);
      }
    } catch (Exception e) {
      log.error("订单状态同步更新到orderHeader出错, 订单明细: {}", toUpdate, e);
    }
  }

  @Override
  public Long loadFromCpId(String orderNo) {
    return orderHeaderMapper.selectFromCpId(orderNo);
  }

  @Override
  public List<OrderHeader> selectWhiteOrderList(String toDay) {
    return orderHeaderMapper.selectWhiteOrderList(toDay);
  }

  @Override
  public int updateJoinTypeByOrderNoForPiece(String orderNo, String cpId) {
    return orderHeaderMapper.updateJoinTypeByOrderNoForPiece(orderNo, cpId);
  }

  @Override
  public int updateJoinTypeByOrderNo(int joinType,String orderNo) {
    return orderHeaderMapper.updateJoinTypeByOrderNo(joinType,orderNo);
  }



  /**
   * 根据订单号查询orderheader表的joinType
   * @param orderNo
   * @return
   */
  @Override
  public Integer selectJoinTypeByOrderNo(String orderNo) {
    return orderHeaderMapper.selectJoinTypeByOrderNo(orderNo);
  }
}
