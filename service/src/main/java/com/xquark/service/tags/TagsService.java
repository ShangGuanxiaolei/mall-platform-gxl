package com.xquark.service.tags;

import com.xquark.aop.anno.NeedCache;
import com.xquark.dal.model.Tags;
import com.xquark.dal.type.TagsCategory;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

/**
 * Created by wangxinhua on 17-11-15. DESC: 标签Service 接口
 */
public interface TagsService {


  /**
   * 保存标签
   *
   * @param tag 标签对象
   * @return true or false
   */
  public boolean save(@NeedCache Tags tag);

  /**
   * 删除某个标签
   *
   * @param name 标签名称
   * @return true or false
   */
  public boolean deleteByName(String name);

  /**
   * 如果数据库中没有则保存标签，否则返回数据库中的值
   *
   * @param tag 要保存的标签对象
   * @return 若数据库中不存在则返回保存后的tag(id经过编码), 若存在则返回数据库中的数据，若保存失败则返回null
   */
  Tags saveIfAbsent(Tags tag);

  /**
   * 查看标签
   *
   * @param id 标签id
   * @return {@link Tags} tag 对象
   */
  public Tags load(String id);

  /**
   * 根据名称读取标签
   *
   * @param name 标签名称
   * @return {@link Tags} tag 对象
   */
  public Tags loadByName(String name);

  /**
   * 查找某个tagId是否存在
   *
   * @param id tagId
   * @return true or false
   */
  public boolean exists(String id);

  /**
   * 查找某个tagName是否存在
   *
   * @param tagName 标签名
   * @return true or false
   */
  public boolean existsName(String tagName);

  /**
   * 修改标签信息
   *
   * @param tags 标签对象
   * @return true or false
   */
  public boolean update(Tags tags);

  /**
   * 返回集合以及查询总数的map
   *
   * @param pageable 分页对象
   * @return 分页集合以及总数 list, total
   * @throws com.xquark.service.error.BizException 如果查询出错则抛出业务异常
   */
  Map<String, Object> list(Pageable pageable, TagsCategory tagsCategory);

  /**
   * 查询所有标签
   *
   * @param pageable 分页
   * @return {@link List<Tags>} 标签集合或空集合
   */
  public List<Tags> listTags(Pageable pageable);

  /**
   * 根据多个id及类型查询
   *
   * @param ids 多个id
   * @param category 标签类别
   * @return {@link List<Tags>} 标签ids
   */
  public List<Tags> listTags(List<String> ids, TagsCategory category);

  /**
   * 所有标签总数
   *
   * @return 查询结果
   */
  public long count();

  /**
   * 指定类型标签总数
   *
   * @return 查询结果
   */
  public long count(TagsCategory category);

  /**
   * 根据标签分类查询
   *
   * @param pageable 分页对象
   * @return {@link List<Tags>} 标签集合或空集合
   */
  public List<Tags> listTags(Pageable pageable, TagsCategory category);


}
