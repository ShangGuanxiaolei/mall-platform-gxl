package com.xquark.service.tags.impl;

import com.xquark.dal.mapper.ProductTagsMapper;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.ProductTags;
import com.xquark.dal.model.Tags;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.tags.RelationTagsService;
import com.xquark.service.tags.TagsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangxinhua on 17-11-15. DESC:
 */
@Service(value = "productTagsService")
public class ProductTagsService implements RelationTagsService<Product> {

  private final ProductTagsMapper productTagsMapper;

  private TagsService tagsService;

  @Autowired
  public ProductTagsService(ProductTagsMapper productTagsMapper, TagsService tagsService) {
    this.productTagsMapper = productTagsMapper;
    this.tagsService = tagsService;
  }

  @Override
  @Transactional
  public boolean saveWithRelation(String objId, Tags... tags) {
    assert tags != null;
    boolean result = true;
    for (Tags tag : tags) {
      Tags tagsAfterSave = tagsService.saveIfAbsent(tag);
      if (tagsAfterSave == null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "保存标签失败");
      }
      String tagId = tagsAfterSave.getId();
      if (tagged(objId, tagId)) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该商品已经关联过这个标签");
      }
      result = result && productTagsMapper.insert(new ProductTags(tagId, objId)) > 0;
    }
    return result;
  }

  @Override
  public List<Tags> listTags(String objId, Pageable pageable) {
    return productTagsMapper.listTagsByProductId(objId, pageable);
  }

  @Override
  public List<Product> listObjByTagIds(Pageable pageable, String... ids) {
    return productTagsMapper.listProductByTagIds(pageable, ids);
  }

  @Override
  public List<Product> listObjByTagNames(Pageable pageable, String... names) {
    return productTagsMapper.listProductByTagNames(pageable, names);
  }

  @Override
  public boolean deleteByNameWithRelation(String name) {
    return false;
  }

  @Override
  public boolean tagged(String productId, String tagId) {
    return productTagsMapper.selectTagged(productId, tagId);
  }

  @Override
  public boolean unTag(String productId, String name) {
    return productTagsMapper.unTag(productId, name);
  }

  @Override
  public boolean clear(String objId) {
    return productTagsMapper.deleteByProductId(objId) > 0;
  }

}
