package com.xquark.service.tags;

import com.xquark.dal.mapper.TagsMapper;
import com.xquark.dal.model.Tags;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.TagsCategory;
import com.xquark.service.CacheDBService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by wangxinhua on 17-11-16. DESC: 标签基础实现类
 */
@Service
public class BaseTagsService extends BaseServiceImpl implements TagsService, CacheDBService<Tags> {

  private TagsMapper tagsMapper;

  /**
   * 子类注入tagsMapper
   */
  @Autowired
  protected void setTagsMapper(TagsMapper tagsMapper) {
    this.tagsMapper = tagsMapper;
  }

  @Override
  public boolean save(Tags tag) {
    tag.setCategory(TagsCategory.PRODUCT);
    boolean result = tagsMapper.insert(tag) > 0;
    long longId = Long.parseLong(tag.getId());
    tag.setId(IdTypeHandler.encode(longId));
    return result;
  }

  @Override
  public Tags saveIfAbsent(Tags tag) {
    Tags tagsExists = loadByName(tag.getName());
    if (tagsExists != null) {
      return tagsExists;
    }
    tag.setCategory(TagsCategory.PRODUCT);
    if (save(tag)) {
      return tag;
    }
    return null;
  }

  @Override
  public Tags load(String id) {
    return tagsMapper.selectByPrimaryKey(id);
  }

  @Override
  public Tags loadByKey(String key) {
    return load(key);
  }

  @Override
  public Tags loadByName(String name) {
    return tagsMapper.selectByName(name);
  }

  @Override
  public Map<String, Object> list(Pageable pageable, TagsCategory category) {
    List<Tags> list;
    long count;
    try {
      list = this.listTags(pageable, category);
      count = this.count(category);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "查询标签失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", list);
    result.put("total", count);
    return new HashMap<>(result);
  }

  @Override
  public List<Tags> listTags(Pageable pageable) {
    return this.listTags(pageable, null);
  }

  @Override
  public List<Tags> listTags(List<String> ids, TagsCategory category) {
    return tagsMapper.selectMultiple(ids, category);
  }

  @Override
  public List<Tags> listTags(Pageable pageable, TagsCategory category) {
    return tagsMapper.listTags(pageable, category);
  }

  @Override
  public long count() {
    return this.count(null);
  }

  @Override
  public long count(TagsCategory category) {
    return tagsMapper.count(category);
  }

  @Override
  public boolean exists(String id) {
    return tagsMapper.exist(id);
  }

  @Override
  public boolean existsName(String tagName) {
    return tagsMapper.existName(tagName);
  }

  @Override
  public boolean deleteByName(String name) {
    return tagsMapper.deleteByName(name) > 0;
  }

  @Override
  public boolean update(Tags tags) {
    return tagsMapper.updateByPrimaryKeySelective(tags) > 0;
  }

}
