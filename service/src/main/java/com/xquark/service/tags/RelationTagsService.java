package com.xquark.service.tags;

import com.xquark.dal.model.CanTag;
import com.xquark.dal.model.Tags;
import java.util.List;
import org.springframework.data.domain.Pageable;

/**
 * Created by wangxinhua on 17-12-8. DESC:
 */
public interface RelationTagsService<T extends CanTag> {


  /**
   * 若标签已存在且未进行过关联则关联，若标签不存在则同时创建标签
   *
   * @param objId 关联对象id
   * @param tags 标签
   * @return true or false
   */
  public boolean saveWithRelation(String objId, Tags... tags);


  /**
   * 根据被关联对象的id查询出所有关联的标签
   *
   * @param objId 被关联对象id
   * @return {@link List<Tags>} 关联标签集合
   */
  public List<Tags> listTags(String objId, Pageable pageable);

  /**
   * 根据多个标签id查询出所有的关联对象
   *
   * @param pageable 分页对象
   * @param ids 标签id数组
   * @return 关联对象集合
   */
  public List<T> listObjByTagIds(Pageable pageable, String... ids);

  /**
   * 根据多个标签名称查询出所有的关联对象
   *
   * @param pageable 分页对象
   * @param names 标签名称数组
   * @return 关联对象集合
   */
  public List<T> listObjByTagNames(Pageable pageable, String... names);

  /**
   * 删除某个标签 && 级联删除关联
   *
   * @param name 标签名称
   * @return true or false
   */
  public boolean deleteByNameWithRelation(String name);

  /**
   * 查询是否打过标签
   *
   * @param objId 对象id
   * @param tagId 标签id
   * @return true or false
   */
  public boolean tagged(String objId, String tagId);

  /**
   * 解除标签绑定
   *
   * @param objId 对象id
   * @param name 标签名
   * @return true or false
   */
  public boolean unTag(String objId, String name);

  /**
   * 清除某个关联了标签的对象的所有标签 通过具体实现类决定清除对应的关联表
   *
   * @param objId 被关联对象id
   * @return true or false
   */
  public boolean clear(String objId);

}
