package com.xquark.service.tags.impl;

import com.xquark.dal.mapper.FeedbackTagsMapper;
import com.xquark.dal.model.Feedback;
import com.xquark.dal.model.FeedbackTags;
import com.xquark.dal.model.Tags;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.tags.RelationTagsService;
import com.xquark.service.tags.TagsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangxinhua on 17-12-8. DESC:
 */
@Service
public class FeedBackTagsService implements RelationTagsService<Feedback> {

  private FeedbackTagsMapper feedbackTagsMapper;

  private TagsService tagsService;

  @Autowired
  public FeedBackTagsService(FeedbackTagsMapper feedbackTagsMapper, TagsService tagsService) {
    this.feedbackTagsMapper = feedbackTagsMapper;
    this.tagsService = tagsService;
  }

  @Override
  @Transactional
  public boolean saveWithRelation(String objId, Tags... tags) {
    assert tags != null;
    boolean result = true;
    for (Tags tag : tags) {
      Tags tagsAfterSave = tagsService.saveIfAbsent(tag);
      if (tagsAfterSave == null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "保存标签失败");
      }
      String tagId = tagsAfterSave.getId();
      if (tagged(objId, tagId)) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该反馈已经关联过这个标签");
      }
      result = result && feedbackTagsMapper.insert(new FeedbackTags(tagId, objId)) > 0;
    }
    return result;
  }

  @Override
  public boolean clear(String objId) {
    return feedbackTagsMapper.deleteByFeedBackId(objId) > 0;
  }

  @Override
  public List<Tags> listTags(String objId, Pageable pageable) {
    throw new UnsupportedOperationException("该操作暂不支持");
  }

  @Override
  public List<Feedback> listObjByTagIds(Pageable pageable, String... ids) {
    return null;
  }

  @Override
  public List<Feedback> listObjByTagNames(Pageable pageable, String... names) {
    return null;
  }

  @Override
  public boolean deleteByNameWithRelation(String name) {
    return false;
  }

  @Override
  public boolean tagged(String objId, String tagId) {
    return false;
  }

  @Override
  public boolean unTag(String objId, String name) {
    return false;
  }
}
