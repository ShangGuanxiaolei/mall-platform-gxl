package com.xquark.service.sf.impl;

import com.alibaba.fastjson.JSON;
import com.xquark.service.order.OrderService;
import com.xquark.service.product.SFProductService;
import com.xquark.service.product.dto.PushProductMsg;
import com.xquark.service.product.dto.PushShipmentNum;
import com.xquark.service.product.dto.PushSplitOrder;
import com.xquark.service.sf.MSGTYPE;
import com.xquark.service.sf.SFMessageService;
import com.xquark.utils.ConstantUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 顺丰消息推送实现类
 * @author tanggb
 * @date 2019/05/17 12:36
 */
@Service("SFMessageService")
public class SFMessageServiceImpl implements SFMessageService {
    private final static Logger LOGGER = LoggerFactory.getLogger(SFMessageServiceImpl.class);

    @Autowired
    private OrderService orderService;
    @Autowired
    private SFProductService sfProductService;

    @Override
    public String splitOrder(PushSplitOrder pushSplitOrder) {
        LOGGER.info(ConstantUtil.IN_PARAMETER_FORMAT, this.getClass().getSimpleName(), "splitOrder", JSON.toJSONString(pushSplitOrder));
        if(pushSplitOrder.getMsg_type() != MSGTYPE.SPLITORDER.getValue()){
            return "no";
        }
        //验证订单号
        int orderNo = orderService.checkOrderNo(pushSplitOrder.getParent_order_sn());
        if(orderNo < 1 ){
            return "no";
        }
        //保存顺丰订单编码
        int flogistics = sfProductService.saveSFsplitOrder(pushSplitOrder.getParent_order_sn(),pushSplitOrder.getOuter_id());
        LOGGER.info("保存顺丰订单号结果(0:失败,1:成功)：{}",flogistics);
        if(flogistics < 1){
            return "no";
        }
        return "ok";
    }

    @Override
    public String sfLogisticsNum(String json) {

        return "ok";
    }

    @Override
    public String shipmentNumber(PushShipmentNum pushShipmentNum) {
        LOGGER.info(ConstantUtil.IN_PARAMETER_FORMAT, this.getClass().getSimpleName(), "shipmentNumber", JSON.toJSONString(pushShipmentNum));
        //验证订单号,该订单号为顺丰订单号，字段：partnerOrderNo对应
        int orderNo = orderService.checkOrderNo(pushShipmentNum.getOrder_sn());
        if(orderNo < 1 ){
            return "no";
        }
        //发货状态，在更改状态前，判断是否为发货的后续状态
        if(pushShipmentNum.getMsg_type() == MSGTYPE.SHIP.getValue()){
            String status = "SHIPPED";
            int i = sfProductService.updateSFOrderStatus(pushShipmentNum.getOrder_sn(), status);
            if(i < 1){
                return "no";
            }
        }

        //收货状态
        if(pushShipmentNum.getMsg_type() == MSGTYPE.SIGNING.getValue()){
            String status = "SUCCESS";
            int i = sfProductService.updateSFOrderStatus(pushShipmentNum.getOrder_sn(), status);
            if(i < 1){
                return "no";
            }
        }

        return "ok";
    }

    @Override
    public String updateProductMsg(PushProductMsg pushProductMsg) {
        LOGGER.info(ConstantUtil.IN_PARAMETER_FORMAT, this.getClass().getSimpleName(), "updateProductMsg", JSON.toJSONString(pushProductMsg));
        //验证开发者账号
        int i = sfProductService.checkDevAccount(pushProductMsg.getDevAccount());
        if(i < 1 ){
            return "no";
        }

        if(TYPE.MODIFY_ALL.getCode().equals(pushProductMsg.getType())){
            //修改顺丰商品,通过encode获取顺丰商品信息
            for (Object encode : pushProductMsg.getProductSns()){
                sfProductService.pullProductByEncode((String) encode);
            }
        }

        //顺丰批量商品下架
        if(TYPE.OFF_SALE.getCode().equals(pushProductMsg.getType())){
            String status = "INSTOCK";
            int productStatus = sfProductService.updateSFProductStatus(pushProductMsg.getProductSns(), status);
            if(productStatus < 1){
                return "no";
            }
        }

        return "ok";
    }

    public enum  TYPE{
        MODIFY_ALL("MODIFY_ALL","修改商品"),
        OFF_SALE("OFF_SALE","下架"),
        ON_SALE("ON_SALE","上架");

        private String code;

        private String value;

        TYPE(String code, String value) {
            this.code = code;
            this.value = value;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

}
