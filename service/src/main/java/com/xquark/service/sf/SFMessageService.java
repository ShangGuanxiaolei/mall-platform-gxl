package com.xquark.service.sf;


import com.xquark.service.product.dto.PushProductMsg;
import com.xquark.service.product.dto.PushShipmentNum;
import com.xquark.service.product.dto.PushSplitOrder;

/**
 * 顺丰商品消息推送
 * @author tanggb
 * @date 2019/05/17 12:37
 */
public interface SFMessageService {

    /**
     * 顺丰拆单
     * @param pushSplitOrder
     * @return
     */
    String splitOrder(PushSplitOrder pushSplitOrder);

    /**
     * 顺丰物流单号
     * @param json
     * @return
     */
    String sfLogisticsNum(String json);

    /**
     * 顺丰商品订单物流单号,签收推送
     * @param pushShipmentNum
     * @return
     */
    String shipmentNumber(PushShipmentNum pushShipmentNum);

    /**
     * 顺丰商品修改及上下架推送
     * @param pushProductMsg
     * @return
     */
    String updateProductMsg(PushProductMsg pushProductMsg);
}
