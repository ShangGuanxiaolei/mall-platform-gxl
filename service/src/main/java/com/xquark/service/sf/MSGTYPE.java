package com.xquark.service.sf;

public enum  MSGTYPE{
    SPLITORDER(1,"拆单"),
    SHIP(2,"发货"),
    SIGNING(3,"签收");

    private int value;

    private String message;

    MSGTYPE(int value, String message){
        this.value = value;
        this.message = message;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
