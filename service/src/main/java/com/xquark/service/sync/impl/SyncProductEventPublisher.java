/**
 *
 */
package com.xquark.service.sync.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.service.sync.SyncContent;
import com.xquark.service.sync.SyncEvent;
import com.xquark.service.sync.SyncEventPublisher;
import com.xquark.service.sync.internal.SyncTaskBean;
import com.xquark.service.sync.queue.SyncProductQueues;

/**
 * @author Wayne.Wang<5 waynewang @ gmail.com>
 * @since 9:39:38 AM Aug 21, 2014
 */
public class SyncProductEventPublisher implements SyncEventPublisher {

  private SyncProductQueues queues;

  final Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  public void setQueues(SyncProductQueues queues) {
    this.queues = queues;
  }

//	@Override
//	public boolean publish(long index, SyncEvent event, SyncContent content) {
//		final byte idx = (byte) (index % this.queues.getSize()); // 先简单取模吧
//
//		final SyncTaskBean bean = new SyncTaskBean(idx, event, content);
//
//		this.queues.add(bean);
//
//		return false;
//	}

  @SuppressWarnings("static-access")
  @Override
  public boolean publish(long index, SyncEvent event, SyncContent content) {
    boolean ret = false;
    final byte idx = (byte) (index % this.queues.getSize()); // 先简单取模吧

    final SyncTaskBean bean = new SyncTaskBean(idx, event, content);
    ret = this.queues.add(bean);
    log.info("add queue idx:" + idx + " result:" + ret + ", size:" + (this.queues.getQueues())[idx]
        .size() + ",event(" + event.getGroup() + "," + event.getTopic() + ")");
    return ret;
  }

}
