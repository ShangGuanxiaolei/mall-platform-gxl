/**
 *
 */
package com.xquark.service.sync;


/**
 * @author Wayne.Wang<5 waynewang @ gmail.com>
 * @since 9:37:53 AM Aug 21, 2014
 */
public interface SyncEventPublisher {

  boolean publish(long index, SyncEvent event, SyncContent content);
}
