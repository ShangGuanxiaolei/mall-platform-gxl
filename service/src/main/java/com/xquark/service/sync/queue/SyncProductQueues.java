/**
 *
 */
package com.xquark.service.sync.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.xquark.service.sync.internal.SyncTaskBean;

/**
 * @author Wayne.Wang<5 waynewang @ gmail.com>
 * @since 9:59:52 AM Aug 21, 2014
 */
public class SyncProductQueues {

  private Logger log = LoggerFactory.getLogger(getClass());
  @Value("${syncpath}")
  private String flagString;

  @Value("${fqpath}")
  private String path;

  @Value("${fqsize}")
  private int capacity;

  private byte size = 8;
  private static BlockingFQueue[] queues;

  public void setPath(String path) {
    this.path = path;
  }

  public void setCapacity(int capacity) {
    this.capacity = capacity;
  }

  public byte getSize() {
    return size;
  }

  public void setSize(byte size) {
    this.size = size;
  }

  public void init() {
    if (log.isInfoEnabled()) {
      log.info("start to init " + getClass().getSimpleName());
    }

    queues = new BlockingFQueue[size];

    if (flagString.equals("C:\\sync.inf")) {
      log.info("ignore local evn for product sync");
    } else {
      for (int i = 0; i < size; i++) {
        final String path = this.path + "Q" + i;
        queues[i] = new BlockingFQueue(path, this.capacity);

        if (log.isInfoEnabled()) {
          log.info("success to create FQueue({}, {})", path, capacity);
        }
      }
    }

    if (log.isInfoEnabled()) {
      log.info("success to init " + getClass().getSimpleName());
    }
  }

  public void destroy() {
    if (log.isInfoEnabled()) {
      log.info("start to destroy " + getClass().getSimpleName());
    }

    for (BlockingFQueue queue : queues) {
      queue.close();
    }

    if (log.isInfoEnabled()) {
      log.info("success to destroy " + getClass().getSimpleName());
    }
  }

  public boolean add(SyncTaskBean bean) {
    return queues[bean.getIndex()].add(bean.getData());
  }

  public static BlockingFQueue[] getQueues() {
    return queues;
  }
}
