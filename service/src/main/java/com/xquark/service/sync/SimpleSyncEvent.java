/**
 *
 */
package com.xquark.service.sync;

/**
 * @author Wayne.Wang<5 waynewang @ gmail.com>
 * @since 2:14:17 PM Aug 21, 2014
 */
public class SimpleSyncEvent implements SyncEvent {

  private byte group;
  private byte topic;

  public SimpleSyncEvent(byte group, byte topic) {
    this.group = group;
    this.topic = topic;
  }

  public byte getGroup() {
    return group;
  }

  public void setGroup(byte group) {
    this.group = group;
  }

  public byte getTopic() {
    return topic;
  }

  public void setTopic(byte topic) {
    this.topic = topic;
  }
}
