/**
 *
 */
package com.xquark.service.sync;

/**
 * @author Wayne.Wang<5 waynewang @ gmail.com>
 * @since 9:33:43 AM Aug 21, 2014
 */
public interface SyncEvent {

  /**
   * 获取事件组名
   */
  byte getGroup();

  /**
   * 获取事件主题
   */
  byte getTopic();
}
