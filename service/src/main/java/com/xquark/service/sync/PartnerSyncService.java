package com.xquark.service.sync;

import java.util.List;

import com.xquark.dal.model.Product;
import com.xquark.dal.vo.SyncActivityVO;
import com.xquark.dal.vo.SyncShopVO;

public interface PartnerSyncService {

  SyncShopVO shopCreate(String partner, String shopId);

  SyncShopVO shopUpdate(String partner, String shopId);

  List<Product> productUpdate(String partner, List<String> prodList);

  List<Product> productCreate(String partner, String shopId, Integer page);

  List<SyncActivityVO> activityStart(String partner, String actId, List<String> prodList);

  List<SyncActivityVO> activityStop(String partner, List<String> prodList);

}
