/**
 *
 */
package com.xquark.service.sync.internal;

import com.xquark.service.sync.SimpleSyncEvent;
import com.xquark.service.sync.SyncContent;
import com.xquark.service.sync.SyncEvent;

/**
 * @author Wayne.Wang<5 waynewang @ gmail.com>
 * @since 9:43:06 AM Aug 21, 2014
 */
public class SyncTaskBean {

  static int SPLIT_IDX = 5;

  private byte[] data;

  SyncTaskBean() {
  }

  public SyncTaskBean(byte idx, SyncEvent event, SyncContent content) {
    final byte[] data = content.toJSONBytes();
    if (data == null) {
      throw new IllegalArgumentException("invalid content, empty JSON data");
    }
    // 预留五个空位
    this.data = new byte[data.length + SPLIT_IDX];
    System.arraycopy(data, 0, this.data, SPLIT_IDX, data.length);

    this.data[0] = idx; // 队列序号
    this.data[1] = 0; //重试次数
    this.data[2] = event.getGroup();
    this.data[3] = event.getTopic();
  }

  /**
   * 队列序号
   */
  public byte getIndex() {
    return this.data[0];
  }

  /**
   * 重试次数
   */
  public byte getRetryTimes() {
    return this.data[1];
  }

  public byte increaseAndGetRetryTimes() {
    // TODO 考虑到256
    this.data[1] += this.data[1] + 1;
    return this.data[1];
  }

  public byte[] getData() {
    return data;
  }

  public byte getGroup() {
    return this.data[2];
  }

  public byte getTopic() {
    return this.data[3];
  }

  public SyncEvent getEvent() {
    return new SimpleSyncEvent(this.getGroup(), this.getTopic());
  }

  public static SyncTaskBean parse(byte[] data) {
    final SyncTaskBean bean = new SyncTaskBean();
    bean.data = data;
    return bean;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("index=").append(this.getIndex());
    sb.append(",retryTimes=").append(this.getRetryTimes());
    sb.append(",event=(").append(this.getGroup()).append(",").append(this.getTopic()).append(")");
    sb.append(",content=").append(new String(this.data, SPLIT_IDX, this.data.length - SPLIT_IDX));
    return sb.toString();
  }
}
