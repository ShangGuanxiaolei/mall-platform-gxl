/**
 *
 */
package com.xquark.service.sync;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.ini4j.Ini;
import org.ini4j.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

/**
 * @author Wayne.Wang<5 waynewang @ gmail.com>
 * @since 2:11:33 PM Aug 21, 2014
 */
public class SyncConf {

  static final String URL = "url";

  final Logger log = LoggerFactory.getLogger(getClass());

  final ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();

  Map<Byte/* group */, Conf> defaultConfs = new HashMap<Byte/* group */, Conf>();
  Map<Byte/* group */, Map<Byte/* topic */, Conf>> confs = new HashMap<Byte/* group */, Map<Byte/* topic */, Conf>>();

  int exp_times;
  String path;
  long lastModifieds;

  class Conf {

    String url;

    public String getUrl() {
      return url;
    }
  }

  public void setPath(String path) {
    this.path = path;
  }

  String getPath() {
    return this.path;
  }

  void resetConf() throws IOException {
    final File file = new File(this.getPath());
    if (!file.exists()) {
      throw new FileNotFoundException(this.getPath());
    }
    // 判断文件是否有变化
    final long lastModified = file.lastModified();
    // 文件没变化，无需加载配置
    if (this.lastModifieds >= lastModified) {

      return;
    }
    if (log.isDebugEnabled()) {
      log.debug("will reload sync.inf");
    }

    Ini conf;
    try {
      conf = new Ini(file);
    } catch (Exception e) {
      throw new RuntimeException("can not create Ini4j by File:" + file.getAbsolutePath());
    }

    final Map<Byte, Conf> defaultConfsMap = new HashMap<Byte, Conf>();
    final Map<Byte, Map<Byte, Conf>> confsMap = new HashMap<Byte, Map<Byte, Conf>>();

    for (Map.Entry<String, Profile.Section> entry : conf.entrySet()) {

      if (entry.getValue().getParent() != null) {
        continue;
      }

      final byte group = Byte.parseByte(entry.getKey());

      //TODO 不够通用，以后再弄
      // 默认的url
      final String defaultUrl = entry.getValue().get("url");
      Conf defaultConf = null;
      if (StringUtils.isNotBlank(defaultUrl)) {
        defaultConf = new Conf();
        defaultConf.url = defaultUrl;
        defaultConfsMap.put(group, defaultConf);
      }

      final Profile.Section sect = entry.getValue().getChild(URL);

      if (sect == null) {
        continue;
      }

      confsMap.put(group, this.create(sect, defaultConf));
    }

    this.defaultConfs = defaultConfsMap;
    this.confs = confsMap;
    this.lastModifieds = lastModified;

    if (log.isInfoEnabled()) {
      log.info("defaultConfs:" + JSON.toJSONString(defaultConfs, true));
      log.info("confs:" + JSON.toJSONString(confs, true));
    }
  }

  Map<Byte, Conf> create(Profile.Section sect, Conf defaultConf) {
    final Map<Byte, Conf> results = new HashMap<Byte, Conf>();
    for (Map.Entry<String, String> entry : sect.entrySet()) {
      Conf result = new Conf();
      result.url = entry.getValue();
      results.put(Byte.valueOf(entry.getKey()), result);
    }

    return results;
  }

  public void init() throws IOException {
    if (log.isInfoEnabled()) {
      log.info("start to init " + getClass().getSimpleName());
    }

    if (StringUtils.isNotEmpty(this.getPath())) {
      File file = new File(this.path);
      if (!file.exists()) {
        this.path = null;
      }
    }

    if (StringUtils.isEmpty(this.getPath())) {
      final URL url = this.getClass().getResource("/sync.inf");
      if (url == null) {
        //throw new FileNotFoundException("/sync.inf");
        log.error("file not found: sync.inf");
        return;
      }
      this.setPath(url.getFile());
    }
    this.resetConf();

    exec.scheduleWithFixedDelay(new Runnable() {

      @Override
      public void run() {
        try {
          SyncConf.this.resetConf();
        } catch (Exception e) {
          log.error("Error to reload sync.inf", e);
        }
      }
    }, 15, 15, TimeUnit.SECONDS);

    if (log.isInfoEnabled()) {
      log.info("success to init " + getClass().getSimpleName());
    }
  }

  public void destroy() {
    if (log.isInfoEnabled()) {
      log.info("start to destroy " + getClass().getSimpleName());
    }

    this.exec.shutdown();

    if (log.isInfoEnabled()) {
      log.info("success to destroy " + getClass().getSimpleName());
    }
  }

  /**
   * 获取请求的url地址
   */
  public String getUrl(SyncEvent event) {
    Conf conf = null;
    Map<Byte, Conf> confMap = confs.get(event.getGroup());
    if (confMap != null) {
      conf = confMap.get(event.getTopic());
    }

    if (conf == null) {
      conf = defaultConfs.get(event.getGroup());
    }

    if (conf == null) {
      return null;
    }

    return conf.url;
  }
}
