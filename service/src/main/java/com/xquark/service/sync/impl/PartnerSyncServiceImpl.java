package com.xquark.service.sync.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.ProdSyncMapper;
import com.xquark.dal.model.Activity;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.dal.vo.SyncActivityVO;
import com.xquark.dal.vo.SyncShopVO;
import com.xquark.service.activity.ActivityService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.fragment.FragmentImageService;
import com.xquark.service.fragment.ProductFragmentService;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.sync.PartnerSyncService;
import com.xquark.service.user.UserService;

@Service("PartnerSyncService")
public class PartnerSyncServiceImpl extends BaseServiceImpl implements PartnerSyncService {

  @Autowired
  private UserService userService;

  @Autowired
  private ProductService productService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private ActivityService activityService;

  @Autowired
  private ProdSyncMapper prodSyncMapper;

  @Autowired
  private ProductFragmentService productFragmentService;

  @Autowired

  private FragmentImageService fragmentImageService;

  final private int pageSize = 20;

  @Value("${qiniu.extfmt}")
  String qiniuExtFmt;

  @Value("${site.web.host.name}")
  private String siteHost;
    
    /*private long getPageSize(Long amount) {
//    	if (amount > 1000)
//    		return 100;
//    	else if (amount > 200)
//    		return 50;
//    	else 
//    		return 20;
    	return pageSize;
    }*/

  private long getPageCount(Long amount) {
    long ret = 0;
//    	if (amount > 1000)
//    		ret = amount / 100;
//    	else if (amount > 200)
//    		ret = amount / 50;
//    	else if (amount > 50)
//    		ret = amount / 20;
    ret = amount / pageSize;
    return ret + 1;
  }

  @Override
  public SyncShopVO shopCreate(String partner, String shopId) {
    SyncShopVO vo = shopUpdate(partner, shopId);
    vo.setPageSize(getPageCount(productService.countProductsBySales(shopId)));
    return vo;
  }

  @Override
  public SyncShopVO shopUpdate(String partner, String shopId) {
    if (StringUtils.isEmpty(shopId) || StringUtils.isEmpty(partner)) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "parameter error...");
    }
    Shop shop = shopService.load(shopId);
    if (shop == null) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "shop not found in kkkd...");
    }
    User user = userService.load(shop.getOwnerId());

    SyncShopVO vo = new SyncShopVO();
    vo.setUserId(shop.getOwnerId());
    vo.setPhone(user != null ? user.getPhone() : "");
    vo.setUrl(siteHost + "/shop/" + shop.getId());
    vo.setShopId(shop.getId());
    vo.setName(shop.getName());
    vo.setNick(shop.getName());
    vo.setImage(shop.getImg());
    vo.setDescription(shop.getDescription());
    vo.setArchive(shop.getArchive());
    vo.setCreateTime(shop.getCreatedAt());
    return vo;
  }

  @Override
  public List<Product> productUpdate(String partner, List<String> prodList) {
    if (prodList == null || prodList.size() == 0 || StringUtils.isEmpty(partner)) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "parameter error...");
    }

    String[] ids = {};
    //List<Product> prods = productService.selectByIds(prodList.toArray(ids));
    //return generatorSyncProds(prods);
    return productService.selectByIds(prodList.toArray(ids));
  }

  @Override
  public List<Product> productCreate(String partner, String shopId, Integer page) {
    if (StringUtils.isEmpty(partner) || StringUtils.isEmpty(shopId)) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "parameter error...");
    }

    return productService.listProducts4Sync(shopId,
        new PageRequest(page - 1, pageSize));
  }

  @Override
  public List<SyncActivityVO> activityStart(String partner, String actId, List<String> prodList) {
    if (prodList == null || prodList.size() == 0 || StringUtils.isEmpty(partner)) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "parameter error...");
    }
    List<SyncActivityVO> ret = new ArrayList<SyncActivityVO>();

    String[] ids = {};
    Activity activity = activityService.obtainProductCurrentActivities(prodList.get(0));
    List<Product> prods = productService.selectByIds(prodList.toArray(ids));
    for (Product p : prods) {
      SyncActivityVO item = new SyncActivityVO();
      item.setActId(activity.getId());
      item.setActName(activity != null ? activity.getName() : "activity");
      item.setActPrice(p.getPrice());
      item.setMarkPrice(p.getMarketPrice());
      item.setProductId(p.getId());
      item.setShopId(p.getShopId());
      ret.add(item);
    }
    return ret;
  }

  @Override
  public List<SyncActivityVO> activityStop(String partner, List<String> prodList) {
    List<SyncActivityVO> ret = new ArrayList<SyncActivityVO>();

    String[] ids = {};
    List<Product> prods = productService.selectByIds(prodList.toArray(ids));
    for (Product p : prods) {
      SyncActivityVO item = new SyncActivityVO();
      //item.setActId(actId);
      item.setActPrice(p.getPrice());
      item.setMarkPrice(p.getMarketPrice());
      item.setProductId(p.getId());
      item.setShopId(p.getShopId());
      ret.add(item);
    }
    return ret;
  }

}
