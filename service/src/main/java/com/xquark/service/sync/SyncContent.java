/**
 *
 */
package com.xquark.service.sync;

import java.io.Serializable;

/**
 * @author Wayne.Wang<5 waynewang @ gmail.com>
 * @since 9:30:44 AM Aug 21, 2014
 */
public interface SyncContent extends Serializable {

  /**
   * <pre>
   * 通过JSON格式传输数据
   * </pre>
   */
  byte[] toJSONBytes();
}
