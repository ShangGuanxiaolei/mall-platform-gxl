package com.xquark.service.config;

import com.xquark.dal.model.SystemConfigDO;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/6/13
 * Time:10:58
 * Des:
 */
public interface SystemConfigService {
    SystemConfigDO getSystemConfigDO(String name);
    String getValue(String name);
    Integer getIntegerValue(String name);
    int getIntValue(String name);
    float getFloatValue(String name);
    boolean existByName(String name);
}