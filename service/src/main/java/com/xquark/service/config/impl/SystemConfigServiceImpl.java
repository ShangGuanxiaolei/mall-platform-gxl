package com.xquark.service.config.impl;

import com.xquark.dal.mapper.SystemConfigMapper;
import com.xquark.dal.model.SystemConfigDO;
import com.xquark.service.config.SystemConfigService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/6/13
 * Time:10:58
 * Des:
 */
@Service
public class SystemConfigServiceImpl implements SystemConfigService {

    @Autowired
    private SystemConfigMapper systemConfigMapper;

    @Override
    public SystemConfigDO getSystemConfigDO(String name) {
        if(StringUtils.isBlank(name)) return null;
        return this.systemConfigMapper.getByName(name);
    }

    @Override
    public String getValue(String name) {
        if(StringUtils.isBlank(name)) return null;

        return systemConfigMapper.getValueByName(name);
    }

    @Override
    public Integer getIntegerValue(String name) {
        if(StringUtils.isBlank(name)) return null;
        String value = systemConfigMapper.getValueByName(name);

        if(StringUtils.isBlank(value)) return null;
        try {
            return Integer.valueOf(value);
        }catch (NumberFormatException e){
            // ignore
        }
        return null;
    }

    @Override
    public int getIntValue(String name) {
        if(StringUtils.isBlank(name)) return 0;
        String value = systemConfigMapper.getValueByName(name);

        if(StringUtils.isBlank(value)) return 0;
        try {
            return Integer.parseInt(value);
        }catch (NumberFormatException e){
            // ignore
        }
        return 0;
    }

    @Override
    public float getFloatValue(String name) {
        if(StringUtils.isBlank(name)) return 0;
        String value = systemConfigMapper.getValueByName(name);

        if(StringUtils.isBlank(value)) return 0;
        try {
            return Float.parseFloat(value);
        }catch (NumberFormatException e){
            // ignore
        }
        return 0;
    }

    @Override
    public boolean existByName(String name) {
        if(StringUtils.isBlank(name)) return false;
        return systemConfigMapper.existByName(name);
    }
}