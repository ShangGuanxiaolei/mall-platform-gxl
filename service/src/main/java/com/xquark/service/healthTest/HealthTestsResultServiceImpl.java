package com.xquark.service.healthTest;

import com.xquark.dal.mapper.HealthTestPhysiqueMapper;
import com.xquark.dal.mapper.HealthTestResultMapper;
import com.xquark.dal.mapper.HealthTestResultProductMapper;
import com.xquark.dal.model.HealthTestPhysique;
import com.xquark.dal.model.HealthTestResult;
import com.xquark.dal.model.HealthTestResultExample;
import com.xquark.dal.model.HealthTestResultProduct;
import com.xquark.dal.model.HealthTestResultProductExample;
import com.xquark.dal.model.Product;
import com.xquark.dal.vo.HealthTestResultVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangxinhua on 17-7-12. DESC:
 */
@Service
public class HealthTestsResultServiceImpl extends BaseServiceImpl implements
    HealthTestResultService {

  private HealthTestResultMapper resultMapper;

  private HealthTestPhysiqueMapper physiqueMapper;

  private HealthTestResultProductMapper resultProductMapper;

  @Override
  public HealthTestResult loadResult(String resultId) {
    return resultMapper.selectByPrimaryKey(resultId);
  }

  @Override
  public List<HealthTestResult> loadResultByModuleId(String moduleId) {
    HealthTestResultExample example = new HealthTestResultExample();
    example.or()
        .andModuleIdEqualTo(moduleId);
    return resultMapper.selectByExample(example);
  }

  @Override
  public List<HealthTestResult> loadResultInRange(String moduleId, double number) {
    HealthTestResultExample example = new HealthTestResultExample();
    example.or()
        .andModuleIdEqualTo(moduleId)
        .andStartLessThanOrEqualTo(number)
        .andEndGreaterThanOrEqualTo(number);
    return resultMapper.selectByExampleWithBLOBs(example);
  }

  @Override
  public List<HealthTestResultVO> loadResultVOInRange(String moduleId, double number) {
    return resultMapper.selectVOInPage(moduleId, number, null, null, null);
  }

  @Override
  public List<HealthTestResult> listResult(String moduleId, String order, Sort.Direction direction,
      Pageable pageable) {
    if (StringUtils.isBlank(moduleId)) {
      return new ArrayList<HealthTestResult>();
    }
    HealthTestResultExample example = this.pagingExample(order, direction, pageable);
    example.or()
        .andModuleIdEqualTo(moduleId);
    return resultMapper.selectByExample(example);
  }

  @Override
  public List<HealthTestResultVO> listResultVO(String moduleId, String order,
      Sort.Direction direction, Pageable pageable) {
    if (StringUtils.isBlank(moduleId)) {
      return new ArrayList<HealthTestResultVO>();
    }
    return resultMapper.selectVOInPage(moduleId, null, order, direction, pageable);
  }

  @Override
  public Long countResults(String moduleId) {
    HealthTestResultExample example = null;
    if (StringUtils.isNotBlank(moduleId)) {
      example = new HealthTestResultExample();
      example.or().andModuleIdEqualTo(moduleId);
    }
    return resultMapper.countByExample(example);
  }

  @Override
  public boolean saveResult(HealthTestResult healthTestResult) {
    String physiqueId = healthTestResult.getPhysiqueId();
    if (StringUtils.isNotBlank(physiqueId)) {
      HealthTestPhysique physique = physiqueMapper.selectByPrimaryKey(physiqueId);
      if (physique == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "体质类型不正确");
      }
    } else {
      healthTestResult.setPhysiqueId(null);
    }
    if (healthTestResult.getCreatedAt() == null) {
      healthTestResult.setCreatedAt(new Date());
    }
    if (healthTestResult.getUpdatedAt() == null) {
      healthTestResult.setUpdatedAt(new Date());
    }
    if (healthTestResult.getArchive() == null) {
      healthTestResult.setArchive(false);
    }
    return resultMapper.insert(healthTestResult) > 0;
  }

  @Override
  @Transactional
  public boolean updateResult(HealthTestResult healthTestResult) {
    String physiqueId = healthTestResult.getPhysiqueId();
    boolean updatePhysiqueResult = true;
    if (StringUtils.isNotBlank(physiqueId)) {
      HealthTestPhysique physique = physiqueMapper.selectByPrimaryKey(physiqueId);
      if (physique == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "体质类型不正确");
      }
      // 更新时如果修改了描述则修改体制描述
      String description = healthTestResult.getDescription();
      if (StringUtils.isNotBlank(description)) {
        healthTestResult.setDescription(null);
        physique.setDescription(description);
        physique.setUpdatedAt(new Date());
        updatePhysiqueResult = physiqueMapper.updateByPrimaryKeySelective(physique) > 0;
      }
    } else {
      // 如果为空字符串会导致id转码失败
      healthTestResult.setPhysiqueId(null);
    }
    healthTestResult.setUpdatedAt(new Date());
    boolean updateMainResult = resultMapper.updateByPrimaryKeySelective(healthTestResult) > 0;
    return updateMainResult && updatePhysiqueResult;
  }

  @Override
  public boolean delete(String id) {
    return resultMapper.deleteByPrimaryKey(id) > 0;
  }

  @Override
  public boolean saveResultProduct(HealthTestResultProduct resultProduct) {
    if (resultProduct.getCreatedAt() == null) {
      resultProduct.setCreatedAt(new Date());
    }
    if (resultProduct.getUpdatedAt() == null) {
      resultProduct.setUpdatedAt(new Date());
    }
    if (resultProduct.getArchive() == null) {
      resultProduct.setArchive(false);
    }
    return resultProductMapper.insert(resultProduct) > 0;
  }

  @Override
  public boolean bindProduct(HealthTestResultProduct resultProduct) {
    String resultId = resultProduct.getResultId();
    String productId = resultProduct.getProductId();
    if (hasBinded(resultId, productId)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "该商品已配置为推荐商品");
    }
    return this.saveResultProduct(resultProduct);
  }

  @Override
  public boolean unbindProduct(HealthTestResultProduct resultProduct) {
    String resultId = resultProduct.getResultId();
    String productId = resultProduct.getProductId();
    if (!hasBinded(resultId, productId)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "该商品还未设置为推荐商品");
    }
    HealthTestResultProductExample example = new HealthTestResultProductExample();
    example.or()
        .andResultIdEqualTo(resultId)
        .andProductIdEqualTo(productId);
    return resultProductMapper.deleteByExample(example) > 0;
  }

  @Override
  public List<Product> listProductsByResultId(String resultId, String order,
      Sort.Direction direction, Pageable pageable) {
    return resultProductMapper.listProductsByResultId(resultId, order, direction, pageable);
  }

  @Override
  public Long countProductsByResultId(String resultId) {
    return resultProductMapper.countProductsByResultId(resultId);
  }

  private boolean hasBinded(String resultId, String productId) {
    HealthTestResultProductExample example = new HealthTestResultProductExample();
    example.or()
        .andResultIdEqualTo(resultId)
        .andProductIdEqualTo(productId);
    List<HealthTestResultProduct> products = resultProductMapper.selectByExample(example);
    return products != null && products.size() > 0;
  }

  @Autowired
  public void setResultMapper(HealthTestResultMapper resultMapper) {
    this.resultMapper = resultMapper;
  }

  @Autowired
  public void setPhysiqueMapper(HealthTestPhysiqueMapper physiqueMapper) {
    this.physiqueMapper = physiqueMapper;
  }

  @Autowired
  public void setResultProductMapper(HealthTestResultProductMapper resultProductMapper) {
    this.resultProductMapper = resultProductMapper;
  }

  private HealthTestResultExample pagingExample(String order, Sort.Direction direction,
      Pageable pageable) {
    return super.pagingExample(HealthTestResultExample.class, order, direction, pageable);
  }
}
