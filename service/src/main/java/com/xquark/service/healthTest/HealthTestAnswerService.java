package com.xquark.service.healthTest;

import com.xquark.dal.model.HealthTestAnswer;
import com.xquark.dal.model.HealthTestAnswerExample;
import com.xquark.dal.model.HealthTestAnswerGroup;
import com.xquark.dal.model.HealthTestAnswerGroupExample;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Created by wangxinhua on 17-7-5. DESC:
 */
public interface HealthTestAnswerService {

  boolean save(HealthTestAnswer answer);

  boolean saveGroup(HealthTestAnswerGroup group);

  boolean delete(String id);

  boolean update(HealthTestAnswer answer);

  HealthTestAnswer load(String id);

  HealthTestAnswerGroup loadGroup(String groupId);

  List<HealthTestAnswer> list();

  List<HealthTestAnswer> listByGroup(String groupId, String order, Sort.Direction direction,
      Pageable pageable);

  List<HealthTestAnswer> list(String order, Sort.Direction direction, Pageable pageable);

  List<HealthTestAnswer> list(String questionId, String order, Sort.Direction direction,
      Pageable pageable);

  List<HealthTestAnswerGroup> listGroup(HealthTestAnswerGroupExample example);

  Long countGroup(HealthTestAnswerGroupExample example);

  Long count();

  Long count(HealthTestAnswerExample answerExample);

  Long countByQuestionId(String questionId);

  Long countByGroupId(String groupId);

  Integer countAlias(String groupId);

  List<HealthTestAnswer> list(HealthTestAnswerExample answerExample);

}
