package com.xquark.service.healthTest;

import com.xquark.dal.model.HealthTestQuestion;
import com.xquark.dal.model.HealthTestQuestionExample;
import com.xquark.dal.vo.HealthTestModuleQuestionVO;
import com.xquark.dal.vo.HealthTestQuestionVO;

import java.util.List;

/**
 * Created by wangxinhua on 17-7-4. DESC:
 */
public interface HealthTestQuestionService {

  boolean save(HealthTestQuestion question);

  boolean delete(String id);

  boolean deleteByModuleId(String moduleId);

  boolean update(HealthTestQuestion question);

  HealthTestQuestion load(String id);

  HealthTestQuestion loadByName(String name);

  HealthTestQuestionVO loadVO(String id);

  List<HealthTestQuestion> list(String moduleId);

  List<HealthTestQuestion> list(HealthTestQuestionExample questionExample);

  /**
   * 查询菜单及菜单下所有题目及题目下所有选项
   *
   * @return {@link HealthTestModuleQuestionVO} 集合或空集合
   */
  List<HealthTestModuleQuestionVO> listWithModule(String[] moduleIds, Integer requiredSex);

  Long count(HealthTestQuestionExample questionExample);

  List<HealthTestQuestionVO> listVO(HealthTestQuestionExample questionExample);

  HealthTestDescription getResultDescription(HealthTestModuleQuestionVO moduleQuestionVO);
}
