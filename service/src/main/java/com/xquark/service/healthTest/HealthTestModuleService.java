package com.xquark.service.healthTest;

import com.xquark.dal.model.HealthTestModule;
import com.xquark.dal.model.HealthTestModuleExample;
import com.xquark.dal.vo.HealthModuleQuestionsSave;
import com.xquark.dal.vo.HealthTestModuleVO;
import com.xquark.service.error.BizException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Collection;
import java.util.List;

/**
 * Created by wangxinhua on 17-7-3. DESC: 健康测试菜单Service
 */
public interface HealthTestModuleService {

  boolean save(HealthTestModule healthTestModule);

  boolean save(Collection<HealthModuleQuestionsSave> moduleQuestionsSaveSet);

  boolean delete(String primaryKey);

  boolean update(HealthTestModule healthTestModule);

  HealthTestModule load(String primaryKey);

  HealthTestModule loadByName(String name);

  HealthTestModule loadByStaticName(String basicName);

  HealthTestModule loadChild(String parentId, String name);

  List<HealthTestModule> list();

  List<HealthTestModule> list(HealthTestModuleExample moduleExample);

  List<HealthTestModule> list(String parentId);

  List<HealthTestModule> list(String order, Sort.Direction direction, Pageable pageable);

  List<HealthTestModule> list(String parentId, String order, Sort.Direction direction,
      Pageable pageable);

  Integer loadSubCounts(String parentId);

  Long count(HealthTestModuleExample moduleExample);

  /**
   * 整理出整棵菜单数
   *
   * @return {@link List<HealthTestModuleVO>} 集合或空集合
   */
  List<HealthTestModuleVO> treeMenuList();

  /**
   * 整理出指定父节点下的菜单树
   *
   * @param parentId 父节点id
   * @return {@link List<HealthTestModuleVO>} 集合或空集合
   */
  List<HealthTestModuleVO> treeMenuList(String parentId) throws BizException;


  List<HealthTestModuleVO> treeMenuList(HealthTestModuleExample example);

  List<HealthTestModuleVO> treeMenuList(String parentId, HealthTestModuleExample example)
      throws BizException;

  HealthTestModule loadParent(String id);

  int countLevel(String id);
}
