package com.xquark.service.healthTest.impl;

import com.xquark.dal.mapper.HealthTestProfileMapper;
import com.xquark.dal.model.HealthTestProfile;
import com.xquark.dal.model.HealthTestProfileExample;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.healthTest.HealthTestProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by wangxinhua on 17-7-12. DESC:
 */
@Service
public class HealthTestProfileServiceImpl extends BaseServiceImpl implements
    HealthTestProfileService {

  private HealthTestProfileMapper profileMapper;

  @Override
  public boolean save(HealthTestProfile profile) {
    if (profile.getCreatedAt() == null) {
      profile.setCreatedAt(new Date());
    }
    if (profile.getUpdatedAt() == null) {
      profile.setUpdatedAt(new Date());
    }
    if (profile.getArchive() == null) {
      profile.setArchive(false);
    }
    return profileMapper.insert(profile) > 0;
  }

  @Override
  public HealthTestProfile load(String id) {
    return profileMapper.selectByPrimaryKey(id);
  }

  @Override
  public HealthTestProfile loadByUser(String userId) {
    HealthTestProfileExample example = new HealthTestProfileExample();
    example.or()
        .andUserIdEqualTo(userId);
    List<HealthTestProfile> profileList = profileMapper.selectByExample(example);
    if (profileList == null || profileList.size() == 0) {
      return null;
    }
    if (profileList.size() > 1) {
      log.warn("用户信息重复");
    }
    return profileList.get(0);
  }

  @Override
  public boolean update(HealthTestProfile profile) {
    if (profile.getUpdatedAt() == null) {
      profile.setUpdatedAt(new Date());
    }
    return profileMapper.updateByPrimaryKeySelective(profile) > 0;
  }

  @Override
  public boolean delete(String id) {
    return profileMapper.deleteByPrimaryKey(id) > 0;
  }

  @Autowired
  public void setProfileMapper(HealthTestProfileMapper profileMapper) {
    this.profileMapper = profileMapper;
  }
}
