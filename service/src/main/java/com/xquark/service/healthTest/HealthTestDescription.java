package com.xquark.service.healthTest;

import com.xquark.dal.model.Product;

import java.util.List;

/**
 * Created by wangxinhua on 17-7-11. DESC:
 */
public class HealthTestDescription implements Comparable<HealthTestDescription> {

  private String moduleName;
  private String description;
  private double totalScore;

  private List<Product> recommend;

  public HealthTestDescription() {
  }

  public HealthTestDescription(String moduleName, String description) {
    this.moduleName = moduleName;
    this.description = description;
  }

  public String getModuleName() {
    return moduleName;
  }

  public String getDescription() {
    return description;
  }

  public void setModuleName(String moduleName) {
    this.moduleName = moduleName;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public double getTotalScore() {
    return totalScore;
  }

  public void setTotalScore(double totalScore) {
    this.totalScore = totalScore;
  }

  public List<Product> getRecommend() {
    return recommend;
  }

  public void setRecommend(List<Product> recommend) {
    this.recommend = recommend;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    HealthTestDescription that = (HealthTestDescription) o;

    return moduleName.equals(that.moduleName);
  }

  @Override
  public int hashCode() {
    return moduleName.hashCode();
  }

  @Override
  public int compareTo(HealthTestDescription healthTestDescription) {
    if (healthTestDescription == null) {
      return -1;
    }
    return (int) (healthTestDescription.getTotalScore() - this.getTotalScore());
  }
}
