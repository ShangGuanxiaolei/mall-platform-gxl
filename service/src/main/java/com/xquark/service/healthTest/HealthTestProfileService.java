package com.xquark.service.healthTest;

import com.xquark.dal.model.HealthTestProfile;

/**
 * Created by wangxinhua on 17-7-12. DESC:
 */
public interface HealthTestProfileService {

  boolean save(HealthTestProfile profile);

  HealthTestProfile load(String id);

  HealthTestProfile loadByUser(String userId);

  boolean update(HealthTestProfile profile);

  boolean delete(String id);

}
