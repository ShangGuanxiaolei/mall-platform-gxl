package com.xquark.service.healthTest.impl;

import com.xquark.dal.mapper.HealthTestPhysiqueMapper;
import com.xquark.dal.model.HealthTestPhysique;
import com.xquark.dal.model.HealthTestPhysiqueExample;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.healthTest.HealthTestPhysiqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by wangxinhua on 17-7-10. DESC:
 */
@Service
public class HealthTestPhysiqueServiceImpl extends BaseServiceImpl implements
    HealthTestPhysiqueService {

  private HealthTestPhysiqueMapper physiqueMapper;

  @Override
  public List<HealthTestPhysique> list(String order, Sort.Direction direction, Pageable pageable) {
    HealthTestPhysiqueExample example = this.pagingExample(order, direction, pageable);
    return physiqueMapper.selectByExample(example);
  }

  @Override
  public List<HealthTestPhysique> list() {
    return this.list(null, null, null);
  }

  @Override
  public Long count() {
    return physiqueMapper.countByExample(null);
  }

  @Override
  public HealthTestPhysique load(String id) {
    return physiqueMapper.selectByPrimaryKey(id);
  }

  @Override
  public HealthTestPhysique loadByName(String name) {
    HealthTestPhysiqueExample example = new HealthTestPhysiqueExample();
    example.or()
        .andNameEqualTo(name);
    List<HealthTestPhysique> physiqueList = physiqueMapper.selectByExample(example);
    if (physiqueList == null || physiqueList.size() == 0) {
      return null;
    }
    if (physiqueList.size() > 1) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "包含重复体质");
    }
    return physiqueList.get(0);
  }

  @Override
  public boolean save(HealthTestPhysique physique) {
    if (physique.getCreatedAt() == null) {
      physique.setCreatedAt(new Date());
    }
    if (physique.getUpdatedAt() == null) {
      physique.setUpdatedAt(new Date());
    }
    if (physique.getArchive() == null) {
      physique.setArchive(false);
    }
    return physiqueMapper.insert(physique) > 0;
  }

  @Autowired
  public void setPhysiqueMapper(HealthTestPhysiqueMapper physiqueMapper) {
    this.physiqueMapper = physiqueMapper;
  }

  private HealthTestPhysiqueExample pagingExample(String order, Sort.Direction direction,
      Pageable pageable) {
    return super.pagingExample(HealthTestPhysiqueExample.class, order, direction, pageable);
  }
}
