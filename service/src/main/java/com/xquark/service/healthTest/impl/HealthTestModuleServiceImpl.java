package com.xquark.service.healthTest.impl;

import com.xquark.dal.mapper.HealthTestModuleMapper;
import com.xquark.dal.model.HealthTestModule;
import com.xquark.dal.model.HealthTestModuleExample;
import com.xquark.dal.model.HealthTestQuestion;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.GenderType;
import com.xquark.dal.vo.HealthModuleQuestionsSave;
import com.xquark.dal.vo.HealthTestModuleVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.healthTest.HealthTestModuleService;
import com.xquark.service.healthTest.HealthTestQuestionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by wangxinhua on 17-7-3. DESC: 健康测试菜单实现类
 */
@Service
public class HealthTestModuleServiceImpl extends BaseServiceImpl implements
    HealthTestModuleService {

  private HealthTestModuleMapper healthTestModuleMapper;

  private HealthTestQuestionService questionService;

  @Autowired
  public void setHealthTestModuleMapper(HealthTestModuleMapper healthTestModuleMapper) {
    this.healthTestModuleMapper = healthTestModuleMapper;
  }

  @Autowired
  public void setQuestionService(HealthTestQuestionService questionService) {
    this.questionService = questionService;
  }

  @Override
  public boolean save(HealthTestModule healthTestModule) {
    String parentId = healthTestModule.getParentId();
    if (StringUtils.isBlank(parentId)) {
      healthTestModule.setIsLeaf(false);
    } else {
      HealthTestModule exists = healthTestModuleMapper.selectByPrimaryKey(parentId);
      if (exists == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "上级菜单不存在");
      }
      healthTestModule.setIsLeaf(true);
    }
    int subCounts = healthTestModuleMapper.loadSubCounts(parentId);

    healthTestModule.setSortNo(++subCounts);
    healthTestModule.setCreatedAt(new Date());
    healthTestModule.setUpdatedAt(new Date());
    healthTestModule.setArchive(false);
    return healthTestModuleMapper.insert(healthTestModule) > 0;
  }

  @Override
  @Transactional
  public boolean save(Collection<HealthModuleQuestionsSave> moduleQuestionsSaveSet) {
    if (moduleQuestionsSaveSet == null || moduleQuestionsSaveSet.size() == 0) {
      return false;
    }
    for (HealthModuleQuestionsSave moduleQuestionsSave : moduleQuestionsSaveSet) {
      HealthTestModule moduleToSave = moduleQuestionsSave.getModule();
      List<HealthTestQuestion> questionSaveList = moduleQuestionsSave.getQuestionList();
      if (moduleToSave != null) {
        String moduleId;
        HealthTestModule moduleExists = this.loadByName(moduleToSave.getName());
        // 菜单不存在则创建， 存在则将问题挂到原菜单下
        if (moduleExists == null) {
          this.save(moduleToSave);
          moduleId = IdTypeHandler.encode(Long.parseLong(moduleToSave.getId()));
        } else {
          moduleId = moduleExists.getId();
        }
        if (questionSaveList != null && questionSaveList.size() > 0) {
          for (HealthTestQuestion questionToSave : questionSaveList) {
            if (questionToSave != null) {
              HealthTestQuestion questionExists = questionService
                  .loadByName(questionToSave.getName());
              if (questionExists == null) {
                questionToSave.setModuleId(moduleId);
                questionToSave.setRequiredSex(GenderType.N.getCode());
                questionService.save(questionToSave);
              } else {
                questionService.update(questionToSave);
              }
            }
          }
        }
      }
    }
    return true;
  }

  @Override
  @Transactional
  public boolean delete(String primaryKey) {

    List<String> ids = this.collectIds(primaryKey);
    try {
      for (String moduleId : ids) {
        questionService.deleteByModuleId(moduleId);
      }
      healthTestModuleMapper.deleteModules(ids.toArray(new String[ids.size()]));
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "菜单删除失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }

    return Boolean.TRUE;
  }

  @Override
  public boolean update(HealthTestModule healthTestModule) {
    healthTestModule.setUpdatedAt(new Date());
    return healthTestModuleMapper.updateByPrimaryKeySelective(healthTestModule) > 0;
  }

  @Override
  public HealthTestModule load(String primaryKey) {
    return healthTestModuleMapper.selectByPrimaryKey(primaryKey);
  }

  @Override
  public HealthTestModule loadByName(String name) {
    HealthTestModuleExample example = new HealthTestModuleExample();
    example.or()
        .andNameEqualTo(name);
    List<HealthTestModule> modules = healthTestModuleMapper.selectByExample(example);
    if (modules == null || modules.size() == 0) {
      return null;
    }
    if (modules.size() > 1) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "菜单名称重复");
    }
    return modules.get(0);
  }

  @Override
  public HealthTestModule loadByStaticName(String basicName) {
    HealthTestModuleExample example = new HealthTestModuleExample();
    example.or()
        .andStaticNameEqualTo(basicName);
    List<HealthTestModule> modules = healthTestModuleMapper.selectByExample(example);
    if (modules == null || modules.size() == 0) {
      return null;
    }
    return modules.get(0);
  }

  @Override
  public HealthTestModule loadChild(String parentId, String name) {
    return healthTestModuleMapper.selectChild(parentId, name);
  }

  @Override
  public List<HealthTestModule> list() {
    return this.list("sort_no", Sort.Direction.ASC, null);
  }

  @Override
  public List<HealthTestModule> list(HealthTestModuleExample moduleExample) {
    return healthTestModuleMapper.selectByExample(moduleExample);
  }

  @Override
  public List<HealthTestModule> list(String parentId) {
    return this.list(parentId, null, null, null);
  }

  @Override
  public List<HealthTestModule> list(String order, Sort.Direction direction, Pageable pageable) {
    return this.list(null, order, direction, pageable);
  }

  @Override
  public List<HealthTestModule> list(String parentId, String order, Sort.Direction direction,
      Pageable pageable) {
    HealthTestModuleExample moduleExample = createExample(parentId, order, direction, pageable);
    return healthTestModuleMapper.selectByExample(moduleExample);
  }

  @Override
  public Long count(HealthTestModuleExample moduleExample) {
    return healthTestModuleMapper.countByExample(moduleExample);
  }

  @Override
  public Integer loadSubCounts(String parentId) {
    return healthTestModuleMapper.loadSubCounts(parentId);
  }

  @Override
  public List<HealthTestModuleVO> treeMenuList() {
    List<HealthTestModule> list = this.list();
    if (list == null || list.size() <= 0) {
      return new ArrayList<HealthTestModuleVO>();
    }
    return this.treeMenuList(list, null);
  }

  @Override
  public List<HealthTestModuleVO> treeMenuList(String parentId) throws BizException {
    List<HealthTestModule> list = this.list();
    if (list == null || list.size() <= 0) {
      return new ArrayList<HealthTestModuleVO>();
    }
    HealthTestModule module = this.load(parentId);
    if (module == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "父节点不存在");
    }
    return this.treeMenuList(list, parentId);
  }

  @Override
  public List<HealthTestModuleVO> treeMenuList(HealthTestModuleExample example) {
    List<HealthTestModule> list = this.list(example);
    if (list == null || list.size() <= 0) {
      return new ArrayList<HealthTestModuleVO>();
    }
    return this.treeMenuList(list, null);
  }

  @Override
  public List<HealthTestModuleVO> treeMenuList(String parentId, HealthTestModuleExample example)
      throws BizException {
    List<HealthTestModule> list = this.list(example);
    if (list == null || list.size() <= 0) {
      return new ArrayList<HealthTestModuleVO>();
    }
    HealthTestModule module = this.load(parentId);
    if (module == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "父节点不存在");
    }
    return this.treeMenuList(list, parentId);
  }

  /**
   * 递归遍历菜单
   *
   * @param list 完整的菜单列表
   * @param parentId 菜单根节点id
   * @return {@link List<HealthTestModuleVO>} 集合或空集合
   */
  private List<HealthTestModuleVO> treeMenuList(List<HealthTestModule> list, String parentId) {
    List<HealthTestModuleVO> result = new ArrayList<HealthTestModuleVO>();
    for (HealthTestModule module : list) {
      if (StringUtils.equals(module.getParentId(), parentId)) {
        List<HealthTestModuleVO> children = treeMenuList(list, module.getId());
        HealthTestModuleVO moduleVO = new HealthTestModuleVO(module, children);
        result.add(moduleVO);
      }
    }
    return result;
  }

  /**
   * 递归查找所有id
   *
   * @param id 菜单id
   * @return List 或空集合
   */
  private List<String> collectIds(String id) {
    List<String> result = new ArrayList<String>();
    result.add(id);
    List<HealthTestModule> subList = this.list(id);
    for (HealthTestModule module : subList) {
      result.addAll(collectIds(module.getId()));
    }
    return result;
  }

  public static HealthTestModuleExample createExample(String parentId, String order,
      Sort.Direction direction, Pageable pageable) {
    HealthTestModuleExample moduleExample = new HealthTestModuleExample();
    if (StringUtils.isNotBlank(parentId)) {
      if ("0".equals(parentId)) {
        moduleExample.or().andParentIdIsNull();
      } else {
        moduleExample.or().andParentIdEqualTo(parentId);
      }
    }
    if (StringUtils.isBlank(order)) {
      order = "sort_no";
    }
    String orderByClause = "`" + order + "` ";
    if (direction != null) {
      orderByClause += direction.name();
    }
    moduleExample.setOrderByClause(orderByClause);
    if (pageable != null) {
      moduleExample.setOffset(pageable.getOffset());
      moduleExample.setLimit(pageable.getPageSize());
    }
    return moduleExample;
  }

  @Override
  public HealthTestModule loadParent(String id) {
    return healthTestModuleMapper.selectParent(id);
  }

  @Override
  public int countLevel(String id) {
    HealthTestModule parent = loadParent(id);
    if (parent == null) {
      return 0;
    } else {
      return countLevel(parent.getId()) + 1;
    }
  }

}
