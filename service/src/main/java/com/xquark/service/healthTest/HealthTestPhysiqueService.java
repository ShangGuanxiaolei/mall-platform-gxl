package com.xquark.service.healthTest;

import com.xquark.dal.model.HealthTestPhysique;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Created by wangxinhua on 17-7-10. DESC:
 */
public interface HealthTestPhysiqueService {

  List<HealthTestPhysique> list(String order, Sort.Direction direction, Pageable pageable);

  List<HealthTestPhysique> list();

  Long count();

  HealthTestPhysique load(String id);

  HealthTestPhysique loadByName(String name);

  boolean save(HealthTestPhysique physique);

}
