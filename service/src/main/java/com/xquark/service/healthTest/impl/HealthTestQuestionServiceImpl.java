package com.xquark.service.healthTest.impl;

import com.xquark.dal.mapper.HealthTestQuestionMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.status.HealthTestComputeType;
import com.xquark.dal.type.AnswerType;
import com.xquark.dal.vo.HealthTestModuleQuestionVO;
import com.xquark.dal.vo.HealthTestQuestionVO;
import com.xquark.dal.vo.HealthTestResultVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.healthTest.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by wangxinhua on 17-7-4. DESC:
 */
@Service
public class HealthTestQuestionServiceImpl extends BaseServiceImpl implements
    HealthTestQuestionService {

  private HealthTestQuestionMapper questionMapper;

  private HealthTestModuleService moduleService;

  private HealthTestAnswerService answerService;

  private HealthTestPhysiqueService physiqueService;

  private HealthTestResultService resultService;

  @Override
  public boolean save(HealthTestQuestion question) {
    Date now = new Date();
    if (question.getCreatedAt() == null) {
      question.setCreatedAt(now);
    }
    if (question.getUpdatedAt() == null) {
      question.setUpdatedAt(now);
    }
    if (question.getArchive() == null) {
      question.setArchive(false);
    }
    return questionMapper.insert(question) > 0;
  }

  @Override
  public boolean delete(String id) {
    // 答案根据groupId绑定，不需要级联删除答案
    return questionMapper.deleteByPrimaryKey(id) > 0;
  }

  @Override
  @Transactional
  public boolean deleteByModuleId(String moduleId) {
    HealthTestQuestionExample questionExample = new HealthTestQuestionExample();
    questionExample.or()
        .andModuleIdEqualTo(moduleId);
    List<HealthTestQuestion> questionList = questionMapper.selectByExample(questionExample);
    if (questionList == null || questionList.size() == 0) {
      return true;
    }
    try {
      questionMapper.deleteByExample(questionExample);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "删除问题失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    return Boolean.TRUE;
  }

  @Override
  public boolean update(HealthTestQuestion question) {
    question.setUpdatedAt(new Date());
    return questionMapper.updateByPrimaryKeySelective(question) > 0;
  }

  @Override
  public HealthTestQuestion load(String id) {
    return questionMapper.selectByPrimaryKey(id);
  }

  @Override
  public HealthTestQuestion loadByName(String name) {
    HealthTestQuestionExample example = new HealthTestQuestionExample();
    example.or().andNameEqualTo(name);
    List<HealthTestQuestion> questions = questionMapper.selectByExample(example);
    if (questions == null || questions.size() == 0) {
      return null;
    }
    if (questions.size() > 1) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "问题名称重复");
    }
    return questions.get(0);
  }

  @Override
  public HealthTestQuestionVO loadVO(String id) {
    return questionMapper.selectVOByPrimaryKey(id);
  }

  @Override
  public List<HealthTestQuestion> list(String moduleId) {
    HealthTestQuestionExample example = new HealthTestQuestionExample();
    example.or()
        .andModuleIdEqualTo(moduleId);
    return this.list(example);
  }

  @Override
  public List<HealthTestQuestion> list(HealthTestQuestionExample questionExample) {
    return questionMapper.selectByExample(questionExample);
  }

  @Override
  public List<HealthTestModuleQuestionVO> listWithModule(String[] moduleIds, Integer requiredSex) {
    if (moduleIds != null && moduleIds.length == 0) {
      moduleIds = null;
    }
    return questionMapper.selectQuestionWithModule(moduleIds, requiredSex);
  }

  @Override
  public Long count(HealthTestQuestionExample questionExample) {
    if (questionExample == null) {
      return 0L;
    }
    return questionMapper.countByExample(questionExample);
  }

  @Override
  public List<HealthTestQuestionVO> listVO(HealthTestQuestionExample questionExample) {
    return questionMapper.selectVOByExample(questionExample);
  }

  @Override
  public HealthTestDescription getResultDescription(HealthTestModuleQuestionVO moduleQuestionVO) {
    String moduleId = moduleQuestionVO.getModuleId();
    HealthTestModule module = moduleService.load(moduleId);
    List<HealthTestQuestionVO> questions = moduleQuestionVO.getQuestions();
    HealthTestDescription description = new HealthTestDescription();
    if (module != null) {
      description.setModuleName(module.getName());
      if (questions != null && questions.size() != 0) {
        int questionSize = questions.size();
        HealthTestComputeType type = HealthTestComputeType
            .valueOf(module.getType() != null ? module.getType() : "SUM");
        double sumUp = 0;
        for (HealthTestQuestion question : questions) {
          // TODO wangxinhua 可能需要支持多选
          String rightAnswerId = question.getAnswerId();
          if (StringUtils.isBlank(rightAnswerId)) {
            continue;
          }
          HealthTestAnswer answer = answerService.load(rightAnswerId);
          if (answer == null) {
            continue;
          }
          double score = answer.getScore() == null ? 0 : answer.getScore();
          int alias = answer.getAlias() == null ? 0 : answer.getAlias();
          switch (type) {
            case SUM:
              sumUp += score;
              break;
            case COUNT:
              if (alias == AnswerType.YES.getCode()) {
                sumUp++;
              }
              break;
            default:
          }
        }
        List<HealthTestResultVO> results;
        if (type == HealthTestComputeType.COUNT) {
          description.setTotalScore(sumUp);
          results = resultService.loadResultVOInRange(moduleId, sumUp);
        } else {
          double doubleSize = (double) questionSize;
          double total = ((sumUp - doubleSize) / (doubleSize * 4.0)) * 100.0;
          BigDecimal decimal = new BigDecimal(total);
          total = decimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
          description.setTotalScore(total >= 0 ? total : 0);
          results = resultService.loadResultVOInRange(moduleId, total);
        }
        if (results != null && results.size() > 0) {
          // 有多个条件满足也只取一个
          HealthTestResultVO healthTestResult = results.get(0);
          String physiqueId = healthTestResult.getPhysiqueId();
          String descriptionStr = null;
          // 非体质测试直接区描述，体质测试去体质表取描述
          if (StringUtils.isBlank(physiqueId)) {
            descriptionStr = healthTestResult.getDescription();
          } else {
            HealthTestPhysique physique = physiqueService.load(physiqueId);
            if (physique != null) {
              descriptionStr = physique.getDescription();
            }
          }
          description.setDescription(descriptionStr);
          List<Product> recommend = healthTestResult.getProducts();
          if (recommend != null) {
            description.setRecommend(recommend);
          }
        }
      }
    }

    return description;
  }

  @Autowired
  public void setQuestionMapper(HealthTestQuestionMapper questionMapper) {
    this.questionMapper = questionMapper;
  }

  @Autowired
  public void setModuleService(HealthTestModuleService moduleService) {
    this.moduleService = moduleService;
  }

  @Autowired
  public void setAnswerService(HealthTestAnswerService answerService) {
    this.answerService = answerService;
  }

  @Autowired
  public void setPhysiqueService(HealthTestPhysiqueService physiqueService) {
    this.physiqueService = physiqueService;
  }

  @Autowired
  public void setResultService(HealthTestResultService resultService) {
    this.resultService = resultService;
  }
}
