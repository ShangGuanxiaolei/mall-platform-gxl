package com.xquark.service.healthTest.impl;

import com.xquark.dal.mapper.HealthTestAnswerGroupMapper;
import com.xquark.dal.mapper.HealthTestAnswerMapper;
import com.xquark.dal.model.HealthTestAnswer;
import com.xquark.dal.model.HealthTestAnswerExample;
import com.xquark.dal.model.HealthTestAnswerGroup;
import com.xquark.dal.model.HealthTestAnswerGroupExample;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.healthTest.HealthTestAnswerService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by wangxinhua on 17-7-6. DESC:
 */
@Service
public class HealthTestAnswerServiceImpl extends BaseServiceImpl implements
    HealthTestAnswerService {

  private HealthTestAnswerMapper answerMapper;

  private HealthTestAnswerGroupMapper answerGroupMapper;

  @Override
  public boolean save(HealthTestAnswer answer) {
    if (answer.getCreatedAt() == null) {
      answer.setCreatedAt(new Date());
    }
    if (answer.getUpdatedAt() == null) {
      answer.setUpdatedAt(new Date());
    }
    if (answer.getArchive() == null) {
      answer.setArchive(false);
    }
    return answerMapper.insert(answer) > 0;
  }

  @Override
  public boolean saveGroup(HealthTestAnswerGroup group) {
    if (group.getCreatedAt() == null) {
      group.setCreatedAt(new Date());
    }
    if (group.getUpdatedAt() == null) {
      group.setUpdatedAt(new Date());
    }
    if (group.getArchive() == null) {
      group.setArchive(false);
    }
    return answerGroupMapper.insert(group) > 0;
  }

  @Override
  public boolean delete(String id) {
    return answerMapper.deleteByPrimaryKey(id) > 0;
  }

  @Override
  public boolean update(HealthTestAnswer answer) {
    answer.setUpdatedAt(new Date());
    return answerMapper.updateByPrimaryKeySelective(answer) > 0;
  }

  @Override
  public HealthTestAnswer load(String id) {
    return answerMapper.selectByPrimaryKey(id);
  }

  @Override
  public HealthTestAnswerGroup loadGroup(String groupId) {
    return answerGroupMapper.selectByPrimaryKey(groupId);
  }

  @Override
  public List<HealthTestAnswer> list() {
    return this.list(null, null, null);
  }

  @Override
  public List<HealthTestAnswer> listByGroup(String groupId, String order, Sort.Direction direction,
      Pageable pageable) {
    HealthTestAnswerExample answerExample;
    try {
      answerExample = pagingExample(HealthTestAnswerExample.class, order, direction, pageable);
    } catch (Exception e) {
      log.error(e.getMessage());
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "服务器错误");
    }
    if (groupId != null) {
      answerExample.or()
          .andGroupIdEqualTo(groupId);
    }
    return answerMapper.selectByExample(answerExample);
  }

  @Override
  public List<HealthTestAnswer> list(String order, Sort.Direction direction, Pageable pageable) {
    return this.list(null, null, null, null);
  }

  @Override
  public List<HealthTestAnswer> list(String questionId, String order, Sort.Direction direction,
      Pageable pageable) {
    HealthTestAnswerExample answerExample = pagingExample(order, direction, pageable);
    return answerMapper.selectByQuestionId(questionId, answerExample);
  }

  @Override
  public List<HealthTestAnswerGroup> listGroup(HealthTestAnswerGroupExample example) {
    return answerGroupMapper.selectByExample(example);
  }

  @Override
  public Long countGroup(HealthTestAnswerGroupExample example) {
    return answerGroupMapper.countByExample(example);
  }

  @Override
  public Long count() {
    return this.count(null);
  }

  @Override
  public Long count(HealthTestAnswerExample answerExample) {
    return answerMapper.countByExample(answerExample);
  }

  @Override
  public Long countByQuestionId(String questionId) {
    if (StringUtils.isBlank(questionId)) {
      return this.count();
    }
    return answerMapper.countByQuestionId(questionId);
  }

  @Override
  public Long countByGroupId(String groupId) {
    if (StringUtils.isBlank(groupId)) {
      return this.count();
    }
    HealthTestAnswerExample answerExample = new HealthTestAnswerExample();
    answerExample.or()
        .andGroupIdEqualTo(groupId);
    return answerMapper.countByExample(answerExample);
  }

  @Override
  public Integer countAlias(String groupId) {
    return answerMapper.countAlias(groupId);
  }

  @Override
  public List<HealthTestAnswer> list(HealthTestAnswerExample answerExample) {
    return null;
  }

  @Autowired
  public void setAnswerMapper(HealthTestAnswerMapper answerMapper) {
    this.answerMapper = answerMapper;
  }

  @Autowired
  public void setAnswerGroupMapper(HealthTestAnswerGroupMapper answerGroupMapper) {
    this.answerGroupMapper = answerGroupMapper;
  }

  private HealthTestAnswerExample pagingExample(String order, Sort.Direction direction,
      Pageable pageable) {
    return super.pagingExample(HealthTestAnswerExample.class, order, direction, pageable);
  }
}
