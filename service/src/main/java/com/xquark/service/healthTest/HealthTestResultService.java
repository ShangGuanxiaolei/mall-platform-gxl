package com.xquark.service.healthTest;

import com.xquark.dal.model.HealthTestResult;
import com.xquark.dal.model.HealthTestResultProduct;
import com.xquark.dal.model.Product;
import com.xquark.dal.vo.HealthTestResultVO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Created by wangxinhua on 17-7-12. DESC:
 */
public interface HealthTestResultService {

  HealthTestResult loadResult(String resultId);

  List<HealthTestResult> loadResultByModuleId(String moduleId);

  List<HealthTestResult> loadResultInRange(String moduleId, double number);

  List<HealthTestResultVO> loadResultVOInRange(String moduleId, double number);

  List<HealthTestResult> listResult(String moduleId, String order, Sort.Direction direction,
      Pageable pageable);

  List<HealthTestResultVO> listResultVO(String moduleId, String order, Sort.Direction direction,
      Pageable pageable);

  Long countResults(String moduleId);

  boolean saveResult(HealthTestResult healthTestResult);

  boolean updateResult(HealthTestResult healthTestResult);

  boolean delete(String id);

  boolean saveResultProduct(HealthTestResultProduct resultProduct);

  boolean bindProduct(HealthTestResultProduct resultProduct);

  boolean unbindProduct(HealthTestResultProduct resultProduct);

  List<Product> listProductsByResultId(String resultId, String order, Sort.Direction direction,
      Pageable pageable);

  Long countProductsByResultId(String resultId);
}
