package com.xquark.service.posc;

/**
 * @author wangxinhua.
 * @date 2018/9/28
 */
public interface PosCodeService {

  /**
   * 查询snCode是否存在
   * @param snCode snCode
   * @return 查询结果
   */
  boolean isSnCodeExists(String snCode);

}
