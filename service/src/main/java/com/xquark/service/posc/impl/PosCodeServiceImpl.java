package com.xquark.service.posc.impl;

import com.xquark.dal.mapper.PoscodeMapper;
import com.xquark.service.posc.PosCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author wangxinhua.
 * @date 2018/9/28
 */
@Service
public class PosCodeServiceImpl implements PosCodeService {

  private final PoscodeMapper poscodeMapper;

  @Autowired
  public PosCodeServiceImpl(PoscodeMapper poscodeMapper) {
    this.poscodeMapper = poscodeMapper;
  }

  @Override
  public boolean isSnCodeExists(String snCode) {
    return poscodeMapper.selectSnCodeExists(snCode);
  }

}
