package com.xquark.service;

/**
 * Created by wangxinhua on 17-11-17. DESC:
 */
public interface CacheDBService<T> {

  T loadByKey(String key);

}
