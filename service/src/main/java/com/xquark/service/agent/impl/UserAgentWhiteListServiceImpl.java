package com.xquark.service.agent.impl;

import com.xquark.dal.mapper.UserAgentWhiteListMapper;
import com.xquark.dal.model.UserAgentWhiteList;
import com.xquark.service.agent.UserAgentWhiteListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by wangxinhua on 17-5-16. DESC:
 */
@Service
public class UserAgentWhiteListServiceImpl implements UserAgentWhiteListService {

  @Autowired
  private UserAgentWhiteListMapper userAgentWhiteListMapper;

  @Override
  public Boolean save(UserAgentWhiteList whiteList) {
    return userAgentWhiteListMapper.insert(whiteList) > 0;
  }

  @Override
  public Boolean delete(String id) {
    return userAgentWhiteListMapper.delete(id) > 0;
  }

  @Override
  public Boolean deleteByPhone(String phone) {
    return userAgentWhiteListMapper.deleteByPhone(phone) > 0;
  }

  @Override
  public Boolean update(UserAgentWhiteList whiteList) {
    return userAgentWhiteListMapper.update(whiteList) > 0;
  }

  @Override
  public Boolean inList(String agentId) {
    UserAgentWhiteList whiteList = this.load(agentId);
    return whiteList != null && whiteList.getEnable();
  }

  @Override
  public Boolean searchInListWithPhone(String phone) {
    UserAgentWhiteList whiteList = this.loadByPhone(phone);
    return whiteList != null && whiteList.getEnable();
  }

  @Override
  public UserAgentWhiteList load(String id) {
    return userAgentWhiteListMapper.selectByPrimaryKey(id);
  }

  @Override
  public UserAgentWhiteList loadByPhone(String phone) {
    return userAgentWhiteListMapper.selectByPhone(phone);
  }

  @Override
  public List<UserAgentWhiteList> list(String order,
      org.springframework.data.domain.Pageable pageable, Sort.Direction direction, String keyword) {
    return userAgentWhiteListMapper.selectAll(order, pageable, direction, keyword);
  }

  @Override
  public int count() {
    return userAgentWhiteListMapper.selectTotal();
  }

}
