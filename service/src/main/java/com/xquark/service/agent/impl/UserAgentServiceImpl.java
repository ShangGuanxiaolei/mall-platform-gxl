package com.xquark.service.agent.impl;

import com.alibaba.fastjson.JSON;
import com.xquark.dal.mapper.ShopMapper;
import com.xquark.dal.mapper.UserAgentMapper;
import com.xquark.dal.mapper.UserMapper;
import com.xquark.dal.model.Address;
import com.xquark.dal.model.AuditRule;
import com.xquark.dal.model.Message;
import com.xquark.dal.model.Role;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserAgent;
import com.xquark.dal.model.UserAgentForm;
import com.xquark.dal.model.Zone;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.AgentStatus;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.RoleBelong;
import com.xquark.dal.type.PushMsgId;
import com.xquark.dal.type.PushMsgType;
import com.xquark.dal.vo.UserAgentCommissionVO;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.event.MessageNotifyEvent;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.auditRule.AuditRuleService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.msg.MessageService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.user.RoleService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.thirds.umpay.api.util.StringUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by chh on 16-12-6.
 */
@Service("userAgentServiceImpl")
public class UserAgentServiceImpl implements UserAgentService, InitializingBean {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private static Map<String, String> zoneMapping = new HashMap<String, String>();

  @Autowired
  private UserAgentMapper userAgentMapper;

  @Autowired
  private UserService userService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private ShopMapper shopMapper;

  @Autowired
  private UserMapper userMapper;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private RoleService roleService;

  @Autowired
  private AddressService addressService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private MessageService messageService;

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private AuditRuleService auditRuleService;

  @Override
  public UserAgentVO selectByPrimaryKey(String id) {
    return userAgentMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(UserAgent userAgent) {
    if (userAgent.getIdcard() == null) {
      userAgent.setIdcard("");
    }
    return userAgentMapper.insert(userAgent);
  }

  @Override
  public int modify(UserAgent userAgent) {
    return userAgentMapper.modify(userAgent);
  }


  /**
   * @param id
   * @return
   */
  @Override
  public int delete(String id) {
    // 删除代理申请的同时，将user表对应用户也要删除
    // UserAgentVO vo = selectByPrimaryKey(id);
    // userMapper.deleteByPrimaryKey(vo.getUserId());
    return userAgentMapper.delete(id);
  }


  /**
   * 服务端分页查询数据
   */
  @Override
  public List<UserAgentVO> list(Pageable pager, Map<String, Object> params) {
    return userAgentMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return userAgentMapper.selectCnt(params);
  }

  /**
   * 查询某个代理下级各个代理类型人数汇总
   */
  @Override
  public List<Map> getTotalByParentId(String parentId) {
    return userAgentMapper.getTotalByParentId(parentId);
  }

  /**
   * 分页查询某个代理下级某种代理类型所有的明细人员信息
   */
  @Override
  public List<UserAgentVO> listByParentAndType(Pageable pager, String parentId, String type) {
    return userAgentMapper.listByParentAndType(pager, parentId, type);
  }

  /**
   * 分页查询某个代理所有待审核的代理申请人信息
   */
  @Override
  public List<UserAgentVO> listApplyingByParent(Pageable pager, String parentId) {
    return userAgentMapper.listApplyingByParent(pager, parentId);
  }

  /**
   * 查询某个代理所有待审核的代理申请人总数
   */
  @Override
  public Long countApplyingByParent(String parentId) {
    return userAgentMapper.countApplyingByParent(parentId);
  }

  /**
   * 分页查询某个代理所有待审核的代理申请人信息
   */
  @Override
  public List<UserAgentVO> listUserByParent(Pageable pager, String parentId,
      Map<String, Object> params) {
    return userAgentMapper.listUserByParent(pager, parentId, params);
  }

  /**
   * 查询某个代理下面的代理总数
   */
  @Override
  public Long countUserByParent(String parentId, Map<String, Object> params) {
    return userAgentMapper.countUserByParent(parentId, params);
  }

  /**
   * 根据手机号查询某个代理
   */
  @Override
  public UserAgentVO selectByPhone(String phone) {
    return userAgentMapper.selectByPhone(phone);
  }

  /**
   * 根据手机号查询某个代理是否在已审核状态
   */
  @Override
  public UserAgentVO selectByPhoneActive(String phone) {
    return userAgentMapper.selectByPhoneActive(phone);
  }

  /**
   * 根据手机号查询某个代理是否在待审核状态
   */
  @Override
  public UserAgentVO selectByPhoneApplying(String phone) {
    return userAgentMapper.selectByPhoneApplying(phone);
  }

  /**
   * 根据微信号查询某个代理
   */
  @Override
  public UserAgentVO selectByWeixin(String weixin) {
    return userAgentMapper.selectByWeixin(weixin);
  }

  /**
   * 根据userId查询某个代理
   */
  @Override
  public UserAgentVO selectByUserId(String userId) {
    return userAgentMapper.selectByUserId(userId);
  }

  /**
   * 查询某个代理下面的代理总数
   */
  @Override
  public Long selectCntByParentId(String parentId) {
    return userAgentMapper.selectCntByParentId(parentId);
  }

  /**
   * 根据手机号或微信号查询
   */
  @Override
  public UserAgentVO selectByPhoneOrWeixin(String keys) {
    return userAgentMapper.selectByPhoneOrWeixin(keys);
  }

  /**
   * 得到经销商总店shopid
   */
  @Override
  public String getRootShopId() {
    String shopId = userAgentMapper.getRootShopId();
    return IdTypeHandler.encode(new Long(shopId));
  }

  /**
   * 分页查询所有代理的佣金信息
   */
  @Override
  public List<UserAgentCommissionVO> listUserCommission(Pageable pager,
      Map<String, Object> params) {
    return userAgentMapper.listUserCommission(pager, params);
  }

  /**
   * 分页查询某个代理的佣金信息
   */
  @Override
  public List<UserAgentCommissionVO> searchUserCommission(Pageable pager, String userId,
      String keys) {
    return userAgentMapper.searchUserCommission(pager, userId, keys);
  }

  /**
   * 查询所有代理的佣金信息总数
   */
  @Override
  public Long selectCntUserCommission(Map<String, Object> params) {
    return userAgentMapper.selectCntUserCommission(params);
  }

  /**
   * 查询本月新增代理
   */
  @Override
  public Long countMonthByB2B(String userId) {
    long count = 0;
    UserAgentVO userAgentVO = this.selectByUserId(userId);
    User user = userService.load(userId);
    String shopId = user.getShopId();
    String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
    // 对于董事，联合创始人 统计本月新增的所有代理人数总和（不包含直接或间接下级是董事的团队人数）
    // 对于总顾问，本月新增的直招代理人数总和
    // 对于一星顾问，二星顾问，特约, 本月新增的直招代理人数总和
    if (userAgentVO.getType() != AgentType.DIRECTOR && userAgentVO.getType() != AgentType.FOUNDER) {
      count = userAgentMapper.countMonthByB2BDirect(userId);
    } else {
      count = userAgentMapper.countMonthByB2BDirector(userId, shopId, rootShopId);
    }
    return count;
  }

  /**
   * 团队人数 团队人数的统计规则：计算包括直接和间接的下级代理总数
   */
  @Override
  public Long countTeamByB2B(String userId) {
    long count = 0;
    UserAgentVO userAgentVO = this.selectByUserId(userId);
    User user = userService.load(userId);
    String shopId = user.getShopId();
    String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
    count = userAgentMapper.countTeamByB2B(userId, shopId);
    return count;
  }

  /**
   * 统计新增用户，总用户，在线用户等信息
   */
  @Override
  public Map getUserInfo() {
    Map info = new HashMap();
    long newUser = userAgentMapper.getNewUser();
    long allUser = userAgentMapper.getAllUser();
    info.put("newUser", newUser);
    info.put("allUser", allUser);
    return info;
  }

  @Override
  public Map getSummary(String shopId) {
    // 缓存今天的各项值
    Map<String, Object> info = new HashMap<String, Object>();
    // 缓存一周的各项值
    Map<String, Object> nums = new HashMap<String, Object>();

    Map<String, Object> result = new HashMap<String, Object>();
    ArrayList dates = getWeekDay();

    // 订单数
    ArrayList orderlist = (ArrayList) userAgentMapper.getOrder(shopId, dates);
    HashMap ordermap = formatList(orderlist);
    info.put("order", ordermap.get("today"));
    nums.put("order", ordermap.get("week"));

    // 推客数
    ArrayList twitterlist = (ArrayList) userAgentMapper.getAgent(shopId, dates);
    HashMap twittermap = formatList(twitterlist);
    info.put("agent", twittermap.get("today"));
    nums.put("agent", twittermap.get("week"));

    // 交易额
    ArrayList amountlist = (ArrayList) userAgentMapper.getAmount(shopId, dates);
    HashMap amountmap = formatList(amountlist);
    info.put("amount", amountmap.get("today"));
    nums.put("amount", amountmap.get("week"));

    // 佣金
    ArrayList commissionlist = (ArrayList) userAgentMapper.getCommission(shopId, dates);
    HashMap commissionmap = formatList(commissionlist);
    info.put("commission", commissionmap.get("today"));
    nums.put("commission", commissionmap.get("week"));

    result.put("info", info);
    result.put("nums", nums);

    // 代理地区分布信息
    ArrayList<Map> zonelist = (ArrayList) userAgentMapper.getZones();
    ArrayList<String> zoneName = new ArrayList<String>();
    ArrayList<String> zoneCount = new ArrayList<String>();
    for (Map map : zonelist) {
      String name = (String) map.get("name");
      String zoneMap = zoneMapping.get(name);
      if (zoneMap != null) {
        name = zoneMap;
      }
      String count = "" + map.get("count");
      zoneName.add(name);
      zoneCount.add(count);
    }
    result.put("zoneName", zoneName);
    result.put("zoneCount", zoneCount);

    return result;
  }

  /**
   * 获取一周内的所有日期
   */
  private static ArrayList getWeekDay() {
    ArrayList<String> list = new ArrayList<String>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Calendar calendar = Calendar.getInstance();
    while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
      calendar.add(Calendar.DATE, -1);
    }
    for (int i = 0; i < 7; i++) {
      list.add(sdf.format(calendar.getTime()));
      calendar.add(Calendar.DATE, 1);
    }
    return list;
  }

  /**
   * 将从数据库中取出的每天对应的各项值map数据结构做一次格式化 返回当天的值，如数据库没有则是0,同时返回周一到周日的值list(如果某天没有值，则为0)
   */
  private HashMap formatList(ArrayList<Map> list) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String today = "0";
    String temp_num = "0";
    ArrayList weekvalue = new ArrayList();
    // 今天
    String nowstr = sdf.format(new Date());
    HashMap result = new HashMap();
    // 得到一周内的每天的str字符串
    ArrayList<String> dates = getWeekDay();
    // 循环得到当天的值和周一到周天顺序每天的值
    for (String date : dates) {
      temp_num = "0";
      for (Map value : list) {
        String s_date = (String) value.get("s_date");
        String c_num = "" + value.get("c_num");
        if (nowstr.equals(s_date)) {
          today = c_num;
        }
        if (date.equals(s_date)) {
          temp_num = c_num;
          break;
        }
      }
      weekvalue.add(temp_num);
    }
    result.put("today", today);
    result.put("week", weekvalue);
    return result;
  }

  /**
   * 将新增代理操作放到service层，增加事务控制
   */
  @Override
  @Transactional
  public String addAgent(String userId, UserAgentForm userAgentForm, String parentId, User parent,
      boolean isFromApp) {
    String result = "";
    UserAgent userAgent = new UserAgent();
    BeanUtils.copyProperties(userAgentForm, userAgent);
    userAgent.setArchive(false);
    userAgent.setStatus(AgentStatus.APPLYING);
    userAgent.setUserId(userId);

    // userid是否存在
    UserAgentVO vo = this.selectByUserId(userId);
    if (vo != null) {
      result = "4";
      return result;
    }

    // 设置用户角色id
    Role role = roleService
        .selectByCodeAndBelong(userAgentForm.getType().toString(), RoleBelong.B2B.toString());
    if (role == null) {
      result = "5";
      return result;
    }
    userAgent.setRole(role.getId());

    // 如果请求来源于微信分享链接,且要申请的手机号已经存在，则说明这个用户用手机号注册过app，且还没有申请代理
    // 这时需把之前手机号绑定的用户信息都删掉
    String phone = userAgent.getPhone();
    User user = userService.loadAnonymousByPhone(phone);
    if (!isFromApp && user != null) {
      userAgentMapper.deleteShopTreeByPhone(user.getShopId());
      userAgentMapper.deleteShopByPhone(phone);
      this.deleteInfoByPhone(phone);
    }

    // parentId不为空，说明是通过微信里进行的申请
    if (StringUtils.isNotEmpty(parentId)) {
      userAgent.setParentUserId(parentId);
      //userAgent.setType(agentType);
    } else {
      parentId = parent.getId();
      userAgent.setParentUserId(parentId);
    }

    // 根据上级代理userid获取对应的rootShop
    Shop parentShop = shopMapper.selectByUserId(parentId);
    String rootShopId = shopTreeService.selectRootShopByShopId(parentShop.getId()).getRootShopId();
    userAgent.setOwnerShopId(rootShopId);

    // 代理用户申请填写的地址设置为用户默认地址
    Address address = new Address();
    address.setUserId(userId);
    address.setZoneId(userAgentForm.getDistrict());
    address.setStreet(userAgentForm.getAddress());
    address.setConsignee(userAgentForm.getName());
    address.setPhone(userAgentForm.getPhone());
    address.setCommon(true);
    address.setIsDefault(true);
    address.setCertificateId(userAgentForm.getIdcard());
    addressService.saveUserAddress(address, true);

    userAgent.setAddress(address.getId());
    int x = this.insert(userAgent);

    // 申请时，先判断该用户有没有对应的shop，没有则要新建一个shop
    // 然后根据申请的上级代理，增加对应的shoptree数据
    Shop existShop = shopMapper.selectByUserId(userId);
    if (existShop == null) {
      // 创建用户的同时创建shop，设置用户的shopid
      Shop shop = new Shop();
      shop.setName(userAgentForm.getName());
      shop.setOwnerId(userId);
      shopService.insert(shop);
      existShop = shop;
    } else {
      // 将之前shop的名称变更成申请表单中填写的姓名
      Shop shop = new Shop();
      shop.setName(userAgentForm.getName());
      shop.setId(existShop.getId());
      shopMapper.updateByPrimaryKeySelective(shop);
    }
    // 更新对应user的shopid等字段
    User userUpdate = new User();
    userUpdate.setId(userId);
    userUpdate.setName(userAgentForm.getName());
    userUpdate.setPhone(userAgentForm.getPhone());
    userUpdate.setShopId(existShop.getId());
    userService.updateByBosUserInfo(userUpdate);

    // 根据申请的上级代理，增加对应的shoptree数据
    // 维护shoptree对应关系

    // 先删除掉该shop之前有过的shoptree相关信息
    shopTreeService.deleteByDescendant(existShop.getId());
    ShopTree shopTree = new ShopTree();
    shopTree.setRootShopId(rootShopId);
    shopTree.setAncestorShopId(parentShop.getId());
    shopTree.setDescendantShopId(existShop.getId());
    shopTreeService.insertShopTree(shopTree);

    // 推送消息给上级代理，提醒审核
    try {
      // json格式的订单数据字符串
      String data = getXcxData(userAgent, parentId, PushMsgType.MSG_AGENT_APPLY.getValue());
      pushMessage(PushMsgId.Agent_Apply.getId(), parentId, Long.toString(new Date().getTime()),
          PushMsgType.MSG_AGENT_APPLY.getValue(), null, data);
    } catch (Exception e) {
      log.error("推送消息给上级代理，提醒审核 error " + e.toString());
    }

    if (x > 0) {
      result = "ok";
    }
    return result;
  }

  private void pushMessage(Long msgId, String userId, String url, Integer type, String relatId,
      String data) {
    Message message = messageService.loadMessage(IdTypeHandler.encode(msgId));
    message.setData(data);
    MessageNotifyEvent event = new MessageNotifyEvent(message, userId, url, type, relatId);
    applicationContext.publishEvent(event);
  }

  /**
   * 将新增代理操作放到service层，增加事务控制
   */
  @Override
  @Transactional
  public String addAgent4Admin(UserAgentForm userAgentForm, String parentId, User parent,
      String extUid) {
    String result = "";
    // 用户是否存在
    boolean isExist = false;
    // 首先根据手机号，姓名创建用户和对应的shop信息
    String userId = "";
    String avatar = userAgentForm.getAvatar();
    // 判断手机号是否已经有对应用户，如果有，则不需要新建user和shop
    User user = userService.loadAnonymousByPhone(userAgentForm.getPhone());
    if (user == null) {
      User newUser = userService.register(userAgentForm.getPhone(), "123456");
      userId = newUser.getId();
      Shop newShop = new Shop();
      newShop.setName(userAgentForm.getPhone());
      newShop.setOwnerId(userId);
      if (StringUtils.isNotEmpty(avatar)) {
        newShop.setImg(avatar);
      }
      shopService.create(newShop);
    } else {
      userId = user.getId();
      isExist = true;
    }

    UserAgent userAgent = new UserAgent();
    BeanUtils.copyProperties(userAgentForm, userAgent);
    userAgent.setArchive(false);
    userAgent.setStatus(AgentStatus.ACTIVE);
    userAgent.setUserId(userId);

    // 设置用户角色id
    Role role = roleService
        .selectByCodeAndBelong(userAgentForm.getType().toString(), RoleBelong.B2B.toString());
    if (role == null) {
      result = "5";
      return result;
    }
    userAgent.setRole(role.getId());

    // parentId不为空，说明是通过微信里进行的申请
    if (StringUtils.isNotEmpty(parentId)) {
      userAgent.setParentUserId(parentId);
      //userAgent.setType(agentType);
    } else {
      parentId = parent.getId();
      userAgent.setParentUserId(parentId);
    }

    // 根据上级代理userid获取对应的rootShop
    Shop parentShop = shopMapper.selectByUserId(parentId);
    String rootShopId = shopTreeService.selectRootShopByShopId(parentShop.getId()).getRootShopId();
    userAgent.setOwnerShopId(rootShopId);

    // 代理用户申请填写的地址设置为用户默认地址
    Address address = new Address();

    // 如果是汇购网过来的请求，zoneid是汇购系统中的id，需要进行转换
    if (StringUtils.isNotEmpty(extUid)) {
      Zone zone = zoneService.selectByExtId(userAgentForm.getDistrict());
      address.setZoneId(zone.getId());
    } else {
      address.setZoneId(userAgentForm.getDistrict());
    }
    address.setUserId(userId);
    //address.setZoneId(userAgentForm.getDistrict());
    address.setStreet(userAgentForm.getAddress());
    address.setConsignee(userAgentForm.getName());
    address.setPhone(userAgentForm.getPhone());
    address.setCommon(true);
    address.setIsDefault(true);
    address.setCertificateId(userAgentForm.getIdcard());
    addressService.saveUserAddress(address, true);

    userAgent.setAddress(address.getId());
    int x = this.insert(userAgent);

    // 申请时，先判断该用户有没有对应的shop，没有则要新建一个shop
    // 然后根据申请的上级代理，增加对应的shoptree数据
    Shop existShop = shopMapper.selectByUserId(userId);
    if (existShop == null) {
      // 创建用户的同时创建shop，设置用户的shopid
      Shop shop = new Shop();
      shop.setName(userAgentForm.getName());
      shop.setOwnerId(userId);
      if (StringUtils.isNotEmpty(avatar)) {
        shop.setImg(avatar);
      }
      shopService.insert(shop);
      existShop = shop;
    } else {
      // 将之前shop的名称变更成申请表单中填写的姓名
      Shop shop = new Shop();
      shop.setName(userAgentForm.getName());
      shop.setId(existShop.getId());
      shopMapper.updateByPrimaryKeySelective(shop);
    }
    // 更新对应user的shopid等字段
    User userUpdate = new User();
    userUpdate.setId(userId);
    if (StringUtils.isNotEmpty(userAgentForm.getUnionId())) {
      userUpdate.setUnionId(userAgentForm.getUnionId());
    }
    if (StringUtils.isNotEmpty(avatar)) {
      userUpdate.setAvatar(avatar);
    }
    userUpdate.setName(userAgentForm.getName());
    userUpdate.setPhone(userAgentForm.getPhone());
    userUpdate.setShopId(existShop.getId());
    // 默认用户extuid为手机号，微信小程序会使用extuid进行登陆验证等
    if (StringUtils.isNotEmpty(extUid)) {
      userUpdate.setExtUserId(extUid);
    } else {
      userUpdate.setExtUserId(userAgentForm.getPhone());
    }

    // 用户微信名称
    if (StringUtils.isNotEmpty(userAgentForm.getWechatName())) {
      userUpdate.setWechatName(userAgentForm.getWechatName());
    }
    userService.updateByBosUserInfo(userUpdate);

    // 根据申请的上级代理，增加对应的shoptree数据
    // 维护shoptree对应关系

    // 先删除掉该shop之前有过的shoptree相关信息
    shopTreeService.deleteByDescendant(existShop.getId());
    ShopTree shopTree = new ShopTree();
    shopTree.setRootShopId(rootShopId);
    shopTree.setAncestorShopId(parentShop.getId());
    shopTree.setDescendantShopId(existShop.getId());
    shopTreeService.insertShopTree(shopTree);

    if (x > 0) {
      result = "ok";
    }

    // 推送消息给代理人，提醒审核代理申请
    try {
      // json格式的订单数据字符串
      String data = getXcxData(userAgent, parentId, PushMsgType.MSG_AGENT_APPLY.getValue());
      pushMessage(PushMsgId.Agent_Apply.getId(), parentId, Long.toString(new Date().getTime()),
          PushMsgType.MSG_AGENT_APPLY.getValue(), null, data);
    } catch (Exception e) {
      log.error("推送消息给代理人，提醒审核代理申请 error " + e.toString());
    }

    return result;
  }

  /**
   * 返回json格式的订单数据字符串，供小程序展示消息使用
   *
   * @return { "fromAccount": "18971777777", "fromAccountName": "汪洋", "toAccount": "18971888888",
   * "toAccountName": "曹洪辉", "id": "xxxxx", "agentType": "FIRST", "agentTypeStr": "一星", "avatar":
   * "http://images.handeson.com/Fvya4Mx5t46yHYT14gMrAiTx2pJf", "type": "4002" }
   */
  private String getXcxData(UserAgent userAgent, String parentId, long type) {
    String data = "";
    HashMap map = new HashMap();
    User parent = userService.load(parentId);
    User user = userService.load(userAgent.getUserId());
    Shop shop = shopService.findByUser(userAgent.getUserId());
    UserAgentVO parentAgentVO = this.selectByUserId(parentId);
    String addressId = userAgent.getAddress();
    AuditRule auditRule = auditRuleService
        .selectByAgentType(userAgent.getType(), parentAgentVO.getType());
    // 发送相关json数据给小程序
    map.put("fromAccount", userAgent.getPhone());
    map.put("fromAccountName", userAgent.getName());
    map.put("toAccount", parent.getPhone());
    map.put("toAccountName", parent.getName());
    map.put("id", IdTypeHandler.encode(Long.parseLong(userAgent.getId())));
    map.put("agentType", userAgent.getType());
    map.put("agentTypeStr", userAgent.getTypeStr());
    map.put("avatar", StringUtil.isEmpty(user.getAvatar()) ? shop.getImg() : user.getAvatar());
    map.put("name", userAgent.getName());
    map.put("phone", userAgent.getPhone());
    map.put("sex", userAgent.getSex());
    map.put("sexStr", userAgent.getSexStr());
    map.put("wechatName", user.getWechatName());
    map.put("auditType", auditRule.getAuditType());
    map.put("cDate", (new Date()).getTime());
    map.put("userId", user.getId());
    map.put("type", type);
    if (StringUtils.isNotEmpty(addressId)) {
      Address address = addressService.load(addressId);
      AddressVO addressVO = addressService.changeAddressToVo(address);
      map.put("address", addressVO.getDetails());
    }
    data = JSON.toJSONString(map);
    return data;
  }


  /**
   * 获取代理的合同图片
   */
  @Override
  public String getContractImg(String userId) {
    return userAgentMapper.getContractImg(userId);
  }

  /**
   * 删除掉手机号对应用户的相关信息
   */
  @Override
  public void deleteInfoByPhone(String phone) {
    userAgentMapper.deleteUserByPhone(phone);
    userAgentMapper.deleteUserAgentByPhone(phone);
  }

  /**
   * 得到所有代理用户信息
   */
  @Override
  public List<UserAgentVO> getAllUsers() {
    return userAgentMapper.getAllUsers();
  }

  /**
   * 查询手机号对应用户的所有直接上级和所有直接下级代理
   */
  @Override
  public List<String> getAllFriends(String phone) {
    return userAgentMapper.getAllFriends(phone);
  }

  /**
   * 判断两个手机号是否是上下级关系
   */
  @Override
  public UserAgentVO selectByPhoneAndParentPhone(String parentPhone, String phone) {
    return userAgentMapper.selectByPhoneAndParentPhone(parentPhone, phone);
  }

  /**
   * 查询某个代理的团队成员信息
   */
  @Override
  public List<UserAgentVO> listMembers(Pageable pager, String parentId) {
    return userAgentMapper.listMembers(pager, parentId);
  }

  /**
   * 获取某个用户，当月，当季，当年销售数据
   */
  @Override
  public List<Map> getMemberSales(String userId) {
    return userAgentMapper.getMemberSales(userId);
  }

  /**
   * 获取某个用户下级代理数据
   */
  @Override
  public List<Map> getMemberAgents(String userId) {
    return userAgentMapper.getMemberAgents(userId);
  }


  /**
   * 更新用户手势图片
   */
  @Override
  public int updateLifeImg(String userId, String url) {
    // 更新用户手势图片前需要清空掉合同图片字段，因为手势变化了后需要重新生成合同图片
    userAgentMapper.updateContractImg(userId, "");
    return userAgentMapper.updateLifeImg(userId, url);
  }

  /**
   * 更新用户身份证照片
   */
  @Override
  public int updateIdcardImg(String userId, String url) {
    return userAgentMapper.updateIdcardImg(userId, url);
  }

  @Override
  @Transactional
  public int deleteLogically(String agentId) {
    UserAgent userAgent = userAgentMapper.selectByPrimaryKey(agentId);
    if (userAgent == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "代理不存在");
    }
    String phone = userAgent.getPhone();
    User user = userService.load(userAgent.getUserId());
    if (user == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "用户不存在");
    }
    String shopId = user.getShopId();
    List<ShopTree> children = shopTreeService.selectChildren(shopId);
    if (children != null && children.size() > 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该代理有下级，不支持删除");
    }

    if (userAgentMapper.deleteUserAgentLogically(agentId) == 0
        || userAgentMapper.deleteUserLogically(phone) == 0
        || userAgentMapper.deleteShopLogically(shopId) == 0
        || userAgentMapper.deleteShopTreeLogically(shopId) == 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "代理删除失败");
    }
    return 1;
  }


  /**
   * 根据wxbotId查询某个代理
   */
  @Override
  public UserAgentVO selectByWxbotId(String wxbotId) {
    return userAgentMapper.selectByWxbotId(wxbotId);
  }

  /**
   * 根据手机号或身份证号查询某个代理
   */
  @Override
  public UserAgentVO selectByPhoneOrIdcard(String phoneOrIdcard) {
    return userAgentMapper.selectByPhoneOrIdcard(phoneOrIdcard);
  }

  /**
   * 更新wxbotId字段
   */
  @Override
  public int updateWxbotId(String id, String wxbotId) {
    return userAgentMapper.updateWxbotId(id, wxbotId);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    zoneMapping.put("内蒙古自治区", "内蒙古");
    zoneMapping.put("黑龙江省", "黑龙江");
    zoneMapping.put("广西壮族自治区", "广西");
    zoneMapping.put("新疆维吾尔自治区", "新疆");
    zoneMapping.put("香港特别行政区", "香港");
    zoneMapping.put("澳门特别行政区", "澳门");
    zoneMapping.put("宁夏回族自治区", "宁夏");
    zoneMapping.put("西藏自治区", "西藏");
  }


  /**
   * 服务端分页查询数据
   */
  @Override
  public List<UserAgentVO> listFounder(Pageable pager, Map<String, Object> params) {
    return userAgentMapper.listFounder(pager, params);
  }

  /**
   * 更新Area字段
   */
  @Override
  public int updateArea(String id, String area) {
    return userAgentMapper.updateArea(id, area);
  }

  /**
   * 查询所有联合创始人代理信息
   */
  @Override
  public List<UserAgentVO> getAllFounder() {
    return userAgentMapper.getAllFounder();
  }

}
