package com.xquark.service.agent;

import com.xquark.dal.model.User;
import com.xquark.dal.model.UserAgent;
import com.xquark.dal.model.UserAgentForm;
import com.xquark.dal.vo.UserAgentCommissionVO;
import com.xquark.dal.vo.UserAgentVO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * Created by chh on 16-12-6. 代理用户Service
 */
public interface UserAgentService {

  UserAgentVO selectByPrimaryKey(String id);

  int insert(UserAgent userAgent);

  int modify(UserAgent userAgent);


  /**
   * @param id
   * @return
   */
  int delete(String id);


  /**
   * 服务端分页查询数据
   */
  List<UserAgentVO> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);


  /**
   * 查询某个代理下级各个代理类型人数汇总
   */
  List<Map> getTotalByParentId(String parentId);

  /**
   * 分页查询某个代理下级某种代理类型所有的明细人员信息
   */
  List<UserAgentVO> listByParentAndType(Pageable pager, String parentId, String type);

  /**
   * 分页查询某个代理所有待审核的代理申请人信息
   */
  List<UserAgentVO> listApplyingByParent(Pageable pager, String parentId);

  /**
   * 查询某个代理所有待审核的代理申请人总数
   */
  Long countApplyingByParent(String parentId);

  /**
   * 分页查询某个代理所有的代理申请人信息
   */
  List<UserAgentVO> listUserByParent(Pageable pager, String parentId, Map<String, Object> params);

  /**
   * 查询某个代理下面的代理总数
   */
  Long countUserByParent(String parentId, Map<String, Object> params);

  /**
   * 根据手机号查询某个代理
   */
  UserAgentVO selectByPhone(String phone);

  /**
   * 根据手机号查询某个代理是否在已审核状态
   */
  UserAgentVO selectByPhoneActive(String phone);

  /**
   * 根据手机号查询某个代理是否在待审核状态
   */
  UserAgentVO selectByPhoneApplying(String phone);

  /**
   * 根据微信号查询某个代理
   */
  UserAgentVO selectByWeixin(String weixin);

  /**
   * 根据userId查询某个代理
   */
  UserAgentVO selectByUserId(String userId);

  /**
   * 查询某个代理下面的代理总数
   */
  Long selectCntByParentId(String parentId);

  /**
   * 根据手机号或微信号查询
   */
  UserAgentVO selectByPhoneOrWeixin(String keys);

  /**
   * 得到经销商总店shopid
   */
  String getRootShopId();

  /**
   * 分页查询所有代理的佣金信息
   */
  List<UserAgentCommissionVO> listUserCommission(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询某个代理的佣金信息
   */
  List<UserAgentCommissionVO> searchUserCommission(Pageable pager, String userId, String keys);

  /**
   * 查询所有代理的佣金信息总数
   */
  Long selectCntUserCommission(Map<String, Object> params);

  /**
   * 查询本月新增代理
   */
  Long countMonthByB2B(String userId);


  /**
   * 团队人数 团队人数的统计规则：计算包括直接和间接的下级代理总数
   */
  Long countTeamByB2B(String userId);

  /**
   * 统计新增用户，总用户，在线用户等信息
   */
  Map getUserInfo();

  // 获取今日数据,本周订单,代理,交易额,佣金数
  Map getSummary(String shopId);

  String addAgent(String userId, UserAgentForm userAgentForm, String parentId, User parent,
      boolean isFromApp);

  String addAgent4Admin(UserAgentForm userAgentForm, String parentId, User parent, String extUid);

  /**
   * 获取代理的合同图片
   */
  String getContractImg(String userId);

  /**
   * 删除掉手机号对应用户的相关信息
   */
  void deleteInfoByPhone(String phone);

  /**
   * 得到所有代理用户信息
   */
  List<UserAgentVO> getAllUsers();

  /**
   * 查询手机号对应用户的所有上级和所有直接下级代理
   */
  List<String> getAllFriends(String phone);

  /**
   * 判断两个手机号是否是上下级关系
   */
  UserAgentVO selectByPhoneAndParentPhone(String parentPhone, String phone);

  /**
   * 查询某个代理的团队成员信息
   */
  List<UserAgentVO> listMembers(Pageable pager, String parentId);

  /**
   * 获取某个用户，当月，当季，当年销售数据
   */
  List<Map> getMemberSales(String userId);

  /**
   * 获取某个用户下级代理数据
   */
  List<Map> getMemberAgents(String userId);

  /**
   * 更新用户手势图片
   */
  int updateLifeImg(String userId, String url);

  /**
   * 更新用户身份证照片
   */
  int updateIdcardImg(String userId, String url);

  /**
   * 逻辑删除代理
   */
  int deleteLogically(String agentId);


  /**
   * 根据wxbotId查询某个代理
   */
  UserAgentVO selectByWxbotId(String wxbotId);

  /**
   * 根据手机号或身份证号查询某个代理
   */
  UserAgentVO selectByPhoneOrIdcard(String phoneOrIdcard);

  /**
   * 更新wxbotId字段
   */
  int updateWxbotId(String id, String wxbotId);

  /**
   * 服务端分页查询数据
   */
  List<UserAgentVO> listFounder(Pageable pager, Map<String, Object> params);

  /**
   * 更新area字段
   */
  int updateArea(String id, String area);

  /**
   * 查询所有联合创始人代理信息
   */
  List<UserAgentVO> getAllFounder();

}
