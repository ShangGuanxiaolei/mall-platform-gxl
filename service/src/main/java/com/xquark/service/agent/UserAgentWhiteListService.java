package com.xquark.service.agent;

import com.xquark.dal.model.UserAgentWhiteList;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Created by wangxinhua on 17-5-16. DESC:
 */
public interface UserAgentWhiteListService {

  Boolean save(UserAgentWhiteList whiteList);

  Boolean delete(String id);

  Boolean deleteByPhone(String phone);

  Boolean update(UserAgentWhiteList whiteList);

  /**
   * 检查用户是否在白名单内
   */
  Boolean inList(String agentId);

  Boolean searchInListWithPhone(String phone);

  UserAgentWhiteList load(String id);

  UserAgentWhiteList loadByPhone(String phone);

  List<UserAgentWhiteList> list(String order, Pageable pageable, Sort.Direction direction,
      String keyword);

  int count();

}
