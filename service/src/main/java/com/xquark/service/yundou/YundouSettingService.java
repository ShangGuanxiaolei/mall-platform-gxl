package com.xquark.service.yundou;

import com.xquark.dal.model.YundouSetting;
import com.xquark.service.BaseEntityService;
import java.util.List;


public interface YundouSettingService extends BaseEntityService<YundouSetting> {


  // 全局积分设置 在容器初始化后才有效
  YundouSetting GLOBAL_SETTING = new YundouSetting();

  YundouSetting load(String id);

  int insert(YundouSetting team);

  int deleteForArchive(String id);

  int update(YundouSetting record);

  YundouSetting selectByShopId(String shopId);

  List<YundouSetting> list();

  YundouSetting getGlobalSetting();

  /**
   * 积分设置表永远只有记录
   *
   * @return YundouSetting 实例
   */
  YundouSetting loadUnique();

}

