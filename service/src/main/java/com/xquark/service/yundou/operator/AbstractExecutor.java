package com.xquark.service.yundou.operator;

import com.xquark.dal.model.YundouRule;
import com.xquark.dal.vo.YundouRuleVO;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.user.UserService;
import com.xquark.service.yundou.Result;
import com.xquark.service.yundou.operator.executor.IExecutor;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by wangxinhua on 17-5-10. DESC:
 */
public abstract class AbstractExecutor implements IExecutor {

  protected Logger logger = LoggerFactory.getLogger(this.getClass());

  protected UserService userService;

  protected UserAgentService userAgentService;

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setUserAgentService(UserAgentService userAgentService) {
    this.userAgentService = userAgentService;
  }

  /**
   * 通过积分规则及业务id计算出修改的积分结果 具体的计算逻辑由子类实现
   *
   * @param rule 规则vo
   * @param bizId 业务id
   * @return 计算结果
   * @see #calculateAmount(YundouRule, String)
   */
  @Override
  public List<Result> doExecute(YundouRuleVO rule, String bizId) {
    if (isSkip(rule, bizId)) {
      return Collections.emptyList();
    }
    // 计算积分数量
    Long amount = this.calculateAmount(rule, bizId);
    if (amount == 0) {
      return Collections.emptyList();
    }
    Result result = new Result(getUSerId(bizId), rule.getId(), amount);
    return Collections.singletonList(result);
  }

  /**
   * 积分计算默认实现， 根据积分规则中定义的积分数计算
   *
   * @param rule 积分规则
   * @param bizId 业务id
   * @return 增减的积分数 返回正数该积分会被扣减，返回负数该积分会被增加
   */
  protected abstract Long calculateAmount(YundouRule rule, String bizId);

  /**
   * 根据业务id获取用户的id
   *
   * @param bizId 业务id
   * @return 由实现类实现将业务id中的用户提取出来
   */
  protected abstract String getUSerId(String bizId);

  /**
   * 该执行器是否跳过 默认不跳过，由子类复写
   *
   * @return skip or not
   */
  protected boolean isSkip(YundouRuleVO ruleVO, String bizId) {
    return false;
  }
}
