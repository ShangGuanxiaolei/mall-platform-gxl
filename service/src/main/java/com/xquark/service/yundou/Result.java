package com.xquark.service.yundou;

/**
 * Created by wangxinhua on 17-5-10. DESC:
 */
public class Result {

  private String userId;
  private Long amount;
  private String ruleId;

  public Result(String userId, String ruleId, Long amount) {
    this.userId = userId;
    this.amount = amount;
    this.ruleId = ruleId;
  }

  public String getUserId() {
    return userId;
  }

  public Long getAmount() {
    return amount;
  }

  public String getRuleId() {
    return ruleId;
  }
}
