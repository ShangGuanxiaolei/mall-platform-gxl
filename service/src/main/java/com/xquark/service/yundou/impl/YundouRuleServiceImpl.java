package com.xquark.service.yundou.impl;

import com.xquark.dal.mapper.RoleMapper;
import com.xquark.dal.mapper.YundouRuleMapper;
import com.xquark.dal.model.YundouRule;
import com.xquark.dal.model.YundouRuleDetail;
import com.xquark.dal.vo.YundouRuleVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.yundou.YundouRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by wangxinhua on 17-5-3. DESC:
 */
@Service
public class YundouRuleServiceImpl implements YundouRuleService {

  private final YundouRuleMapper ruleMapper;

  @Autowired
  private RoleMapper roleMapper;

  @Autowired
  public YundouRuleServiceImpl(YundouRuleMapper ruleMapper) {
    this.ruleMapper = ruleMapper;
  }

  @Override
  public Boolean save(YundouRuleVO yundouRule) {

    YundouRule rule = new YundouRule(null, yundouRule.getOperationId(), yundouRule.getCode(),
        yundouRule.getName(),
        yundouRule.getDescription(), yundouRule.getDirection());
    rule.setEnable(true);
    Integer ruleResult = ruleMapper.insert(rule);
    Integer ruleDetailResult = 0;
    YundouRuleDetail ruleDetail = new YundouRuleDetail();
    // 添加到关联表
    ruleDetail.setRoleId("74");
    // ruleId没有经过idHandler转换
    ruleDetail.setRuleId(rule.getId());
    ruleDetail.setAmount(yundouRule.getAmount());
    ruleDetailResult = ruleMapper.insertDetail(ruleDetail);
    //}
    return ruleResult > 0 && ruleDetailResult > 0;
  }

  @Override
  public Boolean deleteById(String id) {
    return ruleMapper.deleteById(id) > 0;
  }

  @Override
  public Boolean deleteByCode(Integer code) {
    return ruleMapper.deleteByCode(code) > 0;
  }

  @Override
  public Boolean deleteDetail(String ruleId) {
    return ruleMapper.deleteDetail(ruleId) > 0;
  }

  @Override
  public Boolean updateById(YundouRuleVO yundouRule) {
    Integer ruleResult = ruleMapper.updateById(yundouRule);
    YundouRuleDetail detailExists = ruleMapper.selectDetail(yundouRule.getId());
    if (detailExists == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "规则详情不存在");
    }
    YundouRuleDetail ruleDetail = new YundouRuleDetail();

    ruleDetail.setId(detailExists.getId());
    ruleDetail.setRuleId(yundouRule.getId());
    ruleDetail.setAmount(yundouRule.getAmount());
    Integer ruleDetailResult = ruleMapper.updateDetail(ruleDetail);
    return ruleResult > 0 && ruleDetailResult > 0;
  }

  @Override
  public Boolean updateDetail(YundouRuleDetail detail) {
    return ruleMapper.updateDetail(detail) > 0;
  }

  @Override
  public YundouRule load(String id) {
    return ruleMapper.selectById(id);
  }

  @Override
  public YundouRuleDetail loadDetail(String ruleId) {
    return ruleMapper.selectDetail(ruleId);
  }

  @Override
  public YundouRuleDetail loadDetailAmount(String ruleId, String roleId) {
    return ruleMapper.selectAmount(ruleId, roleId);
  }

  @Override
  public YundouRule loadByName(String name) {
    return ruleMapper.selectByName(name);
  }

  @Override
  public YundouRuleVO loadRuleVO(String id) {
    return ruleMapper.selectYundouRuleVO(id);
  }

  @Override
  public List<YundouRule> list(String order, Pageable pageable, Sort.Direction direction) {
    return ruleMapper.selectAll(order, pageable, direction);
  }

  @Override
  public List<YundouRule> listByOperationId(String oId, String order, Pageable pageable,
      Sort.Direction direction) {
    return ruleMapper.selectByOperationId(oId, order, pageable, direction);
  }

  @Override
  public List<YundouRule> listByOperationCode(Integer code, String order, Pageable pageable,
      Sort.Direction direction) {
    return ruleMapper.selectByOperationCode(code, order, pageable, direction);
  }

  @Override
  public List<YundouRuleVO> listRuleDetail(String operationId, String order, Pageable pageable,
      Sort.Direction direction) {
    return ruleMapper.selectDetailList(operationId, order, pageable, direction);
  }

  @Override
  public List<YundouRuleVO> listRuleDetail(String operationId) {
    return this.listRuleDetail(operationId, null, null, null);
  }

  @Override
  public Integer count() {
    return ruleMapper.count();
  }

  @Override
  public Integer countByOperation(String operationId) {
    return ruleMapper.countByOperation(operationId);
  }
}
