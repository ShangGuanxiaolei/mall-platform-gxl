//package com.xquark.service.yundou.operator;
//
//import com.xquark.dal.type.YundouOperationType;
//import com.xquark.dal.util.SpringContextUtil;
//import org.springframework.stereotype.Component;
//
///**
// * Created by wangxinhua on 17-5-3.
// * DESC:
// */
//@Component
//public class OperatorCreator {
//
//    /**
//     * 根据code
//     * @param code 操作代码
//     * @return 操作者实现类
//     */
//    public IOperator create(Integer code) {
//        IOperator operator;
//        switch (code) {
//            case YundouOperationType.ORDER:
//                operator = SpringContextUtil.getBean(OrderOperator.class);
//                break;
//            default:
//                operator = null;
//        }
//        return operator;
//    }
//
//}
