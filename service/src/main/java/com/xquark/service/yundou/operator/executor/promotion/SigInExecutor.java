package com.xquark.service.yundou.operator.executor.promotion;

import com.vdlm.common.lang.Assert;
import com.xquark.dal.model.SignIn;
import com.xquark.dal.model.SignInRule;
import com.xquark.dal.model.User;
import com.xquark.dal.model.YundouRule;
import com.xquark.dal.vo.YundouRuleVO;
import com.xquark.helper.Generator;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.signin.SignInGenerator;
import com.xquark.service.signin.SignInService;
import com.xquark.service.yundou.operator.AbstractExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 17-8-18. DESC: 签到规则实现类
 */

@Component
public class SigInExecutor extends AbstractExecutor {

  private SignInService signInService;

  @Autowired
  public void setSignInService(SignInService signInService) {
    this.signInService = signInService;
  }

  @Override
  protected Long calculateAmount(YundouRule rule, String bizId) {
    SignIn signIn = signInService.load(bizId);
    Assert.notNull(signIn);

    String userId = signIn.getUserId();
    User user = userService.load(userId);
    if (user == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "用户不存在");
    }

    // 连续签到天数
    Long signInCount = signIn.getSignCount();

    // 大于7都取第七天的积分
    if (signInCount > 7L) {
      signInCount = 7L;
    }
    // 根据连续签到数取积分
    SignInRule signRule = signInService.loadSignInRule();
    Generator<Long> signInGenerator = SignInGenerator.getBuilder()
        .init(signRule.getInit())
        .max(signRule.getMax())
        .operation(signRule.getOperation())
        .operationNumber(signRule.getOperationNumber())
        .create();
    Long[] signInRuleArr = signInGenerator.limit(7);
    return signInRuleArr[signInCount.intValue() - 1];
  }

  /**
   * 通过签到id查找出签到用户id
   *
   * @param bizId 订单id
   * @return userId
   * @throws NullPointerException 如果签到对象不存在
   * @see AbstractExecutor#doExecute(YundouRuleVO, String)
   */
  @Override
  protected String getUSerId(String bizId) {
    SignIn signIn = signInService.load(bizId);
    return signIn.getUserId();
  }
}
