package com.xquark.service.yundou;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;

/**
 * Created by wangxinhua on 17-12-9. DESC:
 */
@ApiModel("积分相关")
public class YundouDeductionResultVO {

  @ApiModelProperty("当前用户对该商品能否使用积分")
  private boolean canUse;

  @ApiModelProperty("可以使用积分情况下需要使用多少积分")
  private Long payYundou;

  @ApiModelProperty("积分可以抵扣多少钱")
  private BigDecimal deductionPrice;

  @ApiModelProperty("单件优惠价格")
  private BigDecimal itemDeductionPrice;

  public YundouDeductionResultVO(OldPointDeductionResult result, int qty) {
    this.canUse = result.isCanUse();
    this.payYundou = result.getPayYundou();
    this.deductionPrice = result.getDeductionPrice();
    this.itemDeductionPrice = deductionPrice
        .divide(BigDecimal.valueOf(qty), 2, BigDecimal.ROUND_HALF_EVEN);
  }

  public boolean isCanUse() {
    return canUse;
  }

  public Long getPayYundou() {
    return payYundou;
  }

  public BigDecimal getDeductionPrice() {
    return deductionPrice;
  }

  public BigDecimal getItemDeductionPrice() {
    return itemDeductionPrice;
  }

}
