package com.xquark.service.yundou.impl;

import com.google.common.base.Preconditions;
import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.model.CommissionTotal;
import com.hds.xquark.dal.model.PointTotal;
import com.hds.xquark.service.point.PointCommService;
import com.xquark.dal.consts.PointConstants;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.User;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.user.UserService;
import com.xquark.service.yundou.PointCommDeductionResult;
import com.xquark.service.yundou.OldPointDeductionResult;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wangxinhua on 17-12-1. DESC: 积分抵扣计算service
 */
@Service
public class PointDeductionService {

  private ProductService productService;

  private UserService userService;

  private PointCommService pointCommService;

  @Autowired
  public PointDeductionService(ProductService productService) {
    this.productService = productService;
  }

  @Autowired
  public void setProductService(ProductService productService) {
    this.productService = productService;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setPointCommService(PointContextInitialize pointContextInitialize) {
    this.pointCommService = pointContextInitialize.getPointService();
  }

  /**
   * 根据一组cartItem及当前用户计算积分兑换比例
   *
   * @param cartItems {@link CartItemVO} 集合
   * @param user 相关用户
   * @return {@link OldPointDeductionResult} 计算结果
   */
  public OldPointDeductionResult calculateDeduction(List<CartItemVO> cartItems, User user) {

    Preconditions.checkNotNull(cartItems);
    Preconditions.checkNotNull(user);
    Preconditions.checkState(cartItems.size() > 0, "cartItems size must > 0");

    User dbUser = userService.load(user.getId());
    long currYundou = dbUser.getYundou();
    if (currYundou == 0) {
      return OldPointDeductionResult.emptyResult();
    }

    // 按积分兑换比例从小到大排序
    // 优先花掉积分抵扣量大的商品
    List<CartItemVO> arrayForCalculate = new ArrayList<>(cartItems);
    Collections.sort(arrayForCalculate, new Comparator<CartItemVO>() {
      @Override
      public int compare(CartItemVO o1, CartItemVO o2) {
        return o1.getProduct().getScaleOrUseGlobal() - o2.getProduct().getScaleOrUseGlobal();
      }
    });

    OldPointDeductionResult finalResult = OldPointDeductionResult.emptyResult();
    // 累加每个cartItem的所需积分数及抵扣价格
    for (CartItemVO cartItem : arrayForCalculate) {
      OldPointDeductionResult result = this.calculateDeduction(cartItem, dbUser);
      dbUser.setYundou(dbUser.getYundou() - result.getPayYundou());
      finalResult.merge(result);
    }

    return finalResult;
  }

  /**
   * 计算积分/德分优惠
   *
   * @param cartItems 多个cartItem项
   * @param user 用户
   * @param userSelectCommission 用户选择使用德分
   * @param userSelectPoint 用户选择使用积分
   * @return 积分优惠计算结果
   */
  public PointCommDeductionResult calculateDeductionNew(List<CartItemVO> cartItems,
      User user, BigDecimal userSelectCommission, BigDecimal userSelectPoint) {
    Preconditions.checkNotNull(cartItems);
    Preconditions.checkNotNull(user);
    Preconditions.checkState(cartItems.size() > 0, "cartItems size must > 0");

    // 用户最大可使用积分/德分数
    PointCommDeductionResult maxPointRet = PointCommDeductionResult.empty();
    for (CartItemVO cartItem : cartItems) {
      maxPointRet.merge(calculateMaxDeduction(cartItem, user));
    }

    // 正式结算时传入用户选择使用的积分/德分
    BigDecimal maxPoint = maxPointRet.getMaxUsingPoint();
    BigDecimal maxCommission = maxPointRet.getMaxUsingCommission();

    BigDecimal pointDeduction = BigDecimal.ZERO;
    BigDecimal commissionDeduction = BigDecimal.ZERO;
    if (userSelectPoint != null) {
      if (userSelectPoint.compareTo(maxPoint) > 0) {
        throw new BizException(GlobalErrorCode.UNKNOWN, "用户选择德分大于实际可使用德分");
      }
      pointDeduction = pointDeduction.add(userSelectPoint.divide(PointConstants.POINT_SCALA, 2,
          BigDecimal.ROUND_HALF_EVEN));
    }
    if (userSelectCommission != null) {
      if (userSelectCommission.compareTo(maxCommission) > 0) {
        throw new BizException(GlobalErrorCode.UNKNOWN, "用户选择积分大于实际可使用积分");
      }
      commissionDeduction = commissionDeduction
          .add(userSelectCommission.divide(PointConstants.COMMISSION_SCALA, 2,
              BigDecimal.ROUND_HALF_EVEN));
    }

    maxPointRet.setUsingPoint(userSelectPoint);
    maxPointRet.setUsingCommission(userSelectCommission);
    maxPointRet.setUsingCommissionDeductionPrice(commissionDeduction);
    maxPointRet.setUsingPointDeductionPrice(pointDeduction);
    return maxPointRet;
  }

  /**
   * 计算购物车item针对某个用户的最大可用德分/积分
   *
   * @param cartItem 购物车对象 包含一个product及一个sku
   * @param user 用户对象
   * @return {@link PointCommDeductionResult} 计算结果
   */
  public PointCommDeductionResult calculateMaxDeduction(CartItemVO cartItem, User user) {
    Preconditions.checkNotNull(cartItem);
    ProductVO productVO = cartItem.getProduct();
    Sku sku = cartItem.getSku();
    String skuId = sku.getId();
    Long cpId = user.getCpId();

    PointTotal pointTotal = pointCommService.loadByCpId(cpId);
    CommissionTotal commissionTotal = pointCommService.loadCommByCpId(cpId);
    BigDecimal price = sku.getPrice();
    BigDecimal pdCommission = productVO.getDeductionDPoint();
    // 德分抵扣金额
    PointCommDeductionResult ret = calculateMaxDeduction(commissionTotal, pointTotal,
        price, pdCommission, cartItem.getAmount());
    // 放入单个item可使用的积分/德分明细
    ret.getCommissionSkuMaxDeductionMap().put(skuId, pdCommission);
    ret.getPointSkuMaxDeductionMap().put(skuId, price);
    return ret;
  }

  /**
   * 根据cartItem及当前用户计算该商品的积分兑换比例
   *
   * @param cartItem 购物车对象 包含一个product及一个sku
   * @param user 用户对象
   * @return {@link OldPointDeductionResult} 计算结果
   */
  public OldPointDeductionResult calculateDeduction(CartItemVO cartItem, User user) {
    Preconditions.checkNotNull(cartItem);
    ProductVO productVO = cartItem.getProduct();
    Sku sku = cartItem.getSku();
    Preconditions.checkNotNull(productVO);
    Preconditions.checkNotNull(sku);

    Integer scale = productService.getScaleOrUseGlobal(productVO);
    // 有sku的情况下价格使用sku价格
    BigDecimal price = sku.getPrice();
    Integer amount = cartItem.getAmount();
    BigDecimal minYundouPrice = productVO.getMinYundouPrice();

    return this.calculateDeduction(scale, price, minYundouPrice, amount, user);
  }

  /**
   * 根据商品及当前用户计算该商品的兑换比例
   *
   * @param product 须计算的商品对象
   * @param user 用户对象
   * @return {@link OldPointDeductionResult} 计算结果
   */
  public OldPointDeductionResult calculateDeduction(Product product, User user) {
    Preconditions.checkNotNull(product);
    Preconditions.checkNotNull(user);
    // 积分比例
    Integer scale = productService.getScaleOrUseGlobal(product);
    BigDecimal price = product.getPrice();
    BigDecimal minYundouPrice = product.getMinYundouPrice();

    return this.calculateDeduction(scale, price, minYundouPrice, 1, user);
  }

  /**
   * 根据商品及其中的sku动态计算所需积分
   *
   * @param product 商品
   * @param sku sku
   * @param amount sku选定数量
   * @param user 所选用户
   * @return 计算结果
   */
  public OldPointDeductionResult calculateDeduction(Product product, Sku sku, int amount,
      User user) {
    Preconditions.checkNotNull(product);
    Preconditions.checkNotNull(sku);
    Preconditions.checkNotNull(user);

    Integer scale = productService.getScaleOrUseGlobal(product);
    BigDecimal price = sku.getPrice();
    BigDecimal minYundouPrice = product.getMinYundouPrice();
    return this.calculateDeduction(scale, price, minYundouPrice, amount, user);
  }

  /**
   * 计算用户当次可用最大德分跟积分
   *
   * @param totalCommission 用户当前德分数量
   * @param price 商品价格
   * @param pdPoint 商品配置的可兑换德分
   * @param qty 商品数量
   * @return {@link PointCommDeductionResult}
   */
  private PointCommDeductionResult calculateMaxDeduction(CommissionTotal totalCommission,
      PointTotal totalPoint, BigDecimal price,
      BigDecimal pdPoint, Integer qty) {
    // 当前德分
    BigDecimal currPoint = totalPoint == null ? BigDecimal.ZERO : totalPoint.getTotalUsable();

    // 当前佣金
    BigDecimal currCommission = totalCommission == null ? BigDecimal.ZERO
        : totalCommission.getUsableCommEcomm();
    BigDecimal bAmount = BigDecimal.valueOf(qty);
    // 所需德分
    BigDecimal needPoint = pdPoint.multiply(bAmount);
    // 总价
    BigDecimal totalPrice = price.multiply(bAmount);

    // 最大可用德分
    BigDecimal usablePoint;
    if (currPoint.compareTo(needPoint) > 0) {
      // 当前德分大于所需德分
      usablePoint = needPoint;
    } else {
      usablePoint = currPoint;
    }
    // 德分可抵扣金额
    BigDecimal maxPointDeductionPrice = usablePoint.divide(PointConstants.POINT_SCALA, 2,
        BigDecimal.ROUND_HALF_EVEN);

    // 计算德分抵扣后实际需要支付的现金
    BigDecimal afterPointPrice = totalPrice.subtract(maxPointDeductionPrice);

    // 最大可用佣金, 1比1即需支付金额
    BigDecimal needCommission = afterPointPrice.divide(PointConstants.COMMISSION_SCALA, 2,
        BigDecimal.ROUND_HALF_EVEN);
    BigDecimal usableCommission;
    if (currCommission.compareTo(needCommission) > 0) {
      usableCommission = needCommission;
    } else {
      usableCommission = currCommission;
    }
    // 按全额抵扣计算抵扣后价格
    return PointCommDeductionResult.build(usableCommission,
        usablePoint);
  }

  /**
   * 结合当前用户积分数以及商品积分比例计算积分购买商品所需要的积分数以及折扣 该方法不会从数据库中重新加载积分，必须保证传入的用户积分为数据库中的积分而非缓存积分
   *
   * @param usingScale 积分比例
   * @param price 原价
   * @param minPrice 使用积分加钱购必须支付的最低金额
   * @param amount 单品数量
   * @param user 对应用户
   * @return {@link OldPointDeductionResult} 计算结果
   */
  private OldPointDeductionResult calculateDeduction(Integer usingScale, BigDecimal price,
      BigDecimal minPrice, Integer amount, User user) {

    assert usingScale != null;
    assert price != null;
    assert minPrice != null;
    assert user != null;
    // 积分比例
    Long currYundou = user.getYundou();

    BigDecimal bAmount = BigDecimal.valueOf(amount);
    BigDecimal totalPrice = price.multiply(bAmount);
    BigDecimal totalMinPrice = minPrice.multiply(bAmount);

    // 可用积分抵扣的金额
    BigDecimal canUseYundouPrice = totalPrice.subtract(totalMinPrice);
    // 所需积分
    Long neededYundou = canUseYundouPrice.multiply(BigDecimal.valueOf(usingScale))
        .longValue();
    // 是否可以使用积分
    // 当前用户拥有的积分数比最小单元大就可以使用积分抵扣
    // 至少一积分才可用
    boolean canUseYundou = currYundou >= usingScale && neededYundou > 0;
    Long payYundou;
    BigDecimal deductionPrice;
    // 当前用户积分大于所需积分，可抵扣积分为所需积分，可抵扣价格为商品价格
    if (currYundou > neededYundou) {
      payYundou = neededYundou;
      deductionPrice = totalPrice;
    } else if (canUseYundou) {
      // 当前用户积分小于所需积分,使用掉的积分为原积分减去除以结果的余数
      // 可抵扣价格为剩余积分数除以该商品的积分兑换比例的商
      BigDecimal[] results = BigDecimal.valueOf(currYundou)
          .divideAndRemainder(BigDecimal.valueOf(usingScale));
      payYundou = currYundou - results[1].longValue();
      deductionPrice = results[0];
    } else {
      payYundou = 0L;
      deductionPrice = BigDecimal.ZERO;
    }
    return OldPointDeductionResult.buildResult(canUseYundou, payYundou, deductionPrice);
  }

}
