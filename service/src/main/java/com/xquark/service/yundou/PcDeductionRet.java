package com.xquark.service.yundou;

import java.math.BigDecimal;
import org.apache.commons.lang3.tuple.Pair;

/**
 * created by
 *
 * @author wangxinhua at 18-6-29 下午11:05
 */
public class PcDeductionRet {

  private final BigDecimal maxUsing;

  private BigDecimal using;

  private BigDecimal usingPacket;

  public PcDeductionRet(BigDecimal maxUsing) {
    this.maxUsing = maxUsing;
  }

  public PcDeductionRet(BigDecimal maxUsing, BigDecimal using, BigDecimal usingPacket) {
    this(maxUsing);
    this.using = using;
    this.usingPacket = usingPacket;
  }

  public PcDeductionRet(BigDecimal maxUsing, Pair<BigDecimal, BigDecimal> usingPair) {
    this(maxUsing, usingPair.getLeft(), usingPair.getRight());
  }

  public BigDecimal getMaxUsing() {
    return maxUsing;
  }

  public BigDecimal getUsing() {
    return using;
  }

  public void setUsing(BigDecimal using) {
    this.using = using;
  }

  public BigDecimal getUsingPacket() {
    return usingPacket;
  }

  public void setUsingPacket(BigDecimal usingPacket) {
    this.usingPacket = usingPacket;
  }
}
