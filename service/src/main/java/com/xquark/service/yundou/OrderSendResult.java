package com.xquark.service.yundou;

/**
 * User: a9175
 * Date: 2018/6/17.
 * Time: 15:26
 */
public class OrderSendResult {
    private String code;
    private String message;
    private String subcode;
    private String submessage;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubcode() {
        return subcode;
    }

    public void setSubcode(String subcode) {
        this.subcode = subcode;
    }

    public String getSubmessage() {
        return submessage;
    }

    public void setSubmessage(String submessage) {
        this.submessage = submessage;
    }

    @Override
    public String toString() {
        return "OrderSendResult{" +
            "code='" + code + '\'' +
            ", message='" + message + '\'' +
            ", subcode='" + subcode + '\'' +
            ", submessage='" + submessage + '\'' +
            '}';
    }
}
