package com.xquark.service.yundou.operator.executor;

import com.xquark.service.yundou.operator.executor.promotion.FanCardExchangeExecutor;
import com.xquark.service.yundou.operator.executor.promotion.SigInExecutor;
import com.xquark.service.yundou.operator.executor.stock.OrderDeductionExecutor;
import com.xquark.service.yundou.operator.executor.stock.RefundOrderExecutor;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wangxinhua on 17-12-2. DESC:
 */
public enum YundouRuleType {
  ORDER_DEDUCTION(20021, OrderDeductionExecutor.class), // 下单积分抵扣
  ORDER_REFUND(20022, RefundOrderExecutor.class), // 下单送积分
  SIGN_IN(30011, SigInExecutor.class),// 签到送积分
  FANCARD_EXCHANGE(40011, FanCardExchangeExecutor.class); // 范卡兑换

  private static final Map<Integer, YundouRuleType> CODE_MAP = new HashMap<>();

  static {
    for (YundouRuleType ruleType : YundouRuleType.values()) {
      CODE_MAP.put(ruleType.code, ruleType);
    }
  }

  private final int code;

  private final Class<? extends IExecutor> executor;

  YundouRuleType(int code, Class<? extends IExecutor> executor) {
    this.code = code;
    this.executor = executor;
  }

  public int getCode() {
    return code;
  }

  public Class<? extends IExecutor> getExecutor() {
    return executor;
  }

  /**
   * 根据code获取类型
   *
   * @param code 规则代码
   * @return 规则类型，若该code对应的类型不存在则返回null
   */
  public static YundouRuleType valueOf(int code) {
    return CODE_MAP.get(code);
  }

}
