package com.xquark.service.yundou.operator.executor;

import com.xquark.dal.vo.YundouRuleVO;
import com.xquark.service.yundou.Result;
import java.util.List;

/**
 * Created by wangxinhua on 17-5-9. DESC:
 */
public interface IExecutor {

  /**
   * 根据配置的规则和当前用户id返回应该给某用户修改的云豆数量 具体给哪个用户修改云豆数由相应的规则实现类决定
   *
   * @param rule 规则id
   * @param bizId 业务id
   * @return 一组积分计算结果 若计算结果为0或跳过则返回emptyList
   * @throws com.xquark.service.error.BizException 计算时的运行时异常都以bizException形式抛出
   */
  List<Result> doExecute(YundouRuleVO rule, String bizId);

}
