package com.xquark.service.yundou;

import com.xquark.dal.model.YundouOperation;
import com.xquark.dal.model.YundouOperationType;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Created by wangxinhua on 17-5-3. DESC:
 */
public interface YundouOperationService {

  Boolean save(YundouOperation operation);

  Boolean deleteById(String id);

  Boolean deleteByCode(Integer code);

  Boolean updateById(YundouOperation operation);

  YundouOperation load(String id);

  YundouOperationType loadType(String operationId);

  YundouOperation loadByName(String name);

  YundouOperation loadByCode(Integer code);

  List<YundouOperation> list(String order, Pageable pageable, Sort.Direction direction);

  List<YundouOperationType> listTypes();

  Integer count();

  Integer countTypes();
}
