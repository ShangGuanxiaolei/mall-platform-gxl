package com.xquark.service.yundou.impl;

import com.xquark.dal.IUser;
import com.xquark.dal.mapper.YundouOrderMapper;
import com.xquark.dal.mapper.YundouProductMapper;
import com.xquark.dal.model.User;
import com.xquark.dal.model.YundouOrder;
import com.xquark.dal.model.YundouProduct;
import com.xquark.dal.model.YundouSetting;
import com.xquark.dal.type.YundouCategoryType;
import com.xquark.dal.vo.YundouProductVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.user.UserService;
import com.xquark.service.yundou.YundouProductService;
import com.xquark.service.yundou.YundouSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service("yundouProductService")
public class YundouProductServiceImpl extends BaseServiceImpl implements YundouProductService {

  @Autowired
  private YundouProductMapper yundouProductMapper;

  @Autowired
  private YundouSettingService yundouSettingService;

  @Autowired
  private YundouOrderMapper yundouOrderMapper;

  @Autowired
  private UserService userService;

  @Override
  public int insert(YundouProduct yundouProduct) {
    return yundouProductMapper.insert(yundouProduct);
  }

  @Override
  public int insertOrder(YundouProduct yundouProduct) {
    return yundouProductMapper.insert(yundouProduct);
  }

  @Override
  public YundouProductVO load(String id) {
    return yundouProductMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return yundouProductMapper.updateForArchive(id);
  }

  @Override
  public int update(YundouProduct record) {
    return yundouProductMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<YundouProductVO> list(Pageable pager, Map<String, Object> params) {
    return yundouProductMapper.list(pager, params);
  }

  @Override
  public List<YundouProductVO> listWithCategory(YundouCategoryType categoryType, String order,
      Sort.Direction direction, Pageable pageable) {
    return yundouProductMapper.listWithCategory(categoryType, order, direction, pageable);
  }

  @Override
  public List<YundouProductVO> listWithCategory(YundouCategoryType[] categoryTypes) {
    return yundouProductMapper.listWithCategoryAndUnion(categoryTypes);
  }

  @Override
  public List<YundouProductVO> listAll() {
    return yundouProductMapper.listWithCategory(null, null, null, null);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return yundouProductMapper.selectCnt(params);
  }

  @Override
  public Long selectCntWithProducts(Map<String, Object> params) {
    return yundouProductMapper.selectCntWithProducts(params);
  }

  /**
   * 查询是否已经有该商品的分佣设置
   */
  @Override
  public Long countByProductId(String shopId, String productId) {
    return yundouProductMapper.countByProductId(shopId, productId);
  }

  @Override
  public List<Map<String, Object>> listCategory() {
    List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
    Map<String, ArrayList<YundouProductVO>> categoryMap = this.listCategoryMap();
    for (YundouCategoryType categoryType : YundouCategoryType.values()) {
      ArrayList<YundouProductVO> list = categoryMap.get(categoryType.name());
      if (list == null) {
        list = new ArrayList<YundouProductVO>();
      }
      Map<String, Object> item = new HashMap<String, Object>();
      item.put("title", categoryType.getName());
      item.put("category", categoryType.name());
      item.put("list", list);
      result.add(item);
    }
    Pageable pageable = new PageRequest(0, 100);
    // 我能购买的商品
    ArrayList<YundouProductVO> capableList = this.listCapable(pageable);
    Map<String, Object> capableMap = new HashMap<String, Object>();
    capableMap.put("title", "我能兑换的商品");
    capableMap.put("category", "capableList");
    capableMap.put("list", capableList);
    result.add(capableMap);

    ArrayList<YundouProductVO> all = (ArrayList<YundouProductVO>) this
        .listWithCategory(null, null, null, pageable);
    Map<String, Object> allMap = new HashMap<String, Object>();
    allMap.put("title", "全部商品");
    allMap.put("category", "all");
    allMap.put("list", all);
    result.add(allMap);
    return result;
  }

  @Override
  public List<List<YundouProductVO>> listCategoryList() {
    List<List<YundouProductVO>> result = new ArrayList<List<YundouProductVO>>();
    Map<String, ArrayList<YundouProductVO>> categoryMap = this.listCategoryMap();
    for (YundouCategoryType categoryType : YundouCategoryType.values()) {
      ArrayList<YundouProductVO> list = categoryMap.get(categoryType.name());
      if (list == null) {
        list = new ArrayList<YundouProductVO>();
      }
      result.add(list);
    }
    Pageable pageable = new PageRequest(0, 4);
    // 我能购买的商品
    ArrayList<YundouProductVO> capableList = this.listCapable(pageable);
    result.add(capableList);
    ArrayList<YundouProductVO> all = (ArrayList<YundouProductVO>) this
        .listWithCategory(null, null, null, pageable);
    // 所有商品
    result.add(all);
    return result;
  }

  @Override
  public Map<String, ArrayList<YundouProductVO>> listCategoryMap() {
    Map<String, ArrayList<YundouProductVO>> resultMap = new LinkedHashMap<String, ArrayList<YundouProductVO>>();
    // 分类商品
    List<YundouProductVO> productVOList = this.listWithCategory(YundouCategoryType.values());
    if (productVOList.size() == 0) {
      return resultMap;
    }
    for (YundouProductVO productVO : productVOList) {
      String categoryType = productVO.getCategory().name();
      ArrayList<YundouProductVO> existsList = resultMap.get(categoryType);
      if (existsList == null) {
        existsList = new ArrayList<YundouProductVO>();
        resultMap.put(categoryType, existsList);
      }
      existsList.add(productVO);
    }

    return resultMap;
  }

  @Override
  public ArrayList<YundouProductVO> listCapable() {
    return (ArrayList<YundouProductVO>) this.listCapable(null, null, null);
  }

  @Override
  public ArrayList<YundouProductVO> listCapable(Pageable pageable) {
    return (ArrayList<YundouProductVO>) this.listCapable(null, null, pageable);
  }

  @Override
  public List<YundouProductVO> listCapable(String order,
      Sort.Direction direction, Pageable pageable) {
    IUser user = getCurrentUser();
    if (!(user instanceof User)) {
      log.warn("用户类型不正确");
      return new ArrayList<YundouProductVO>();
    }
    YundouSetting setting = yundouSettingService.loadUnique();
    int amount = setting == null ? 10 : setting.getAmount();
    return yundouProductMapper.listWithUser(user.getId(), amount, order, direction, pageable);
  }

  @Override
  public YundouProductVO loadByProductId(String shopId, String productId) {
    return yundouProductMapper.selectByProductId(shopId, productId);
  }

  @Override
  public YundouProductVO loadByProductId(String productId) {
    return yundouProductMapper.selectByProductId(productId, null);
  }

  @Override
  public boolean checkIsYundouEnough(String yundouProductId) {
    YundouProduct yundouProduct = this.load(yundouProductId);
    if (yundouProduct == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "积分商品不存在");
    }
    return this.checkIsYundouEnough(yundouProduct, 1);
  }

  @Override
  public boolean checkIsYundouEnough(String yundouProductId, int amount) {
    YundouProduct yundouProduct = this.load(yundouProductId);
    if (yundouProduct == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "积分商品不存在");
    }
    return this.checkIsYundouEnough(yundouProduct, amount);
  }

  private boolean checkIsYundouEnough(YundouProduct yundouProduct, int amount) {
    if (amount == 0) {
      throw new IllegalArgumentException("数量不能为0");
    }
    YundouSetting setting = yundouSettingService.loadUnique();
    if (setting == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "后台未设置积分抵扣比例");
    }
    int yundouSetting = setting.getAmount();
    int canDeduction = yundouProduct.getCanDeduction();
    // 购买该商品所需要的积分
    long neededYundou = yundouSetting * canDeduction * amount;
    int neededLevel = yundouProduct.getLevel();
    IUser user = getCurrentUser();
    if (!(user instanceof User)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "用户类型不正确");
    }
    User dbUser = userService.load(user.getId());
    long userOwnedYundou = dbUser.getYundou();
    if (userOwnedYundou < neededLevel) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "您当前的积分余额不满足该专区限制, 无法购买");
    }
    return userOwnedYundou >= neededYundou;
  }

  @Override
  public BigDecimal getDiscountPrice(BigDecimal originalPrice, String yundouProductId) {
    if (originalPrice == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "原价不能为空");
    }
    YundouProduct yundouProduct = this.load(yundouProductId);
    if (yundouProduct == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "积分商品不存在");
    }
    return originalPrice.subtract(new BigDecimal(yundouProduct.getCanDeduction()))
        .setScale(2, RoundingMode.HALF_UP);
  }

  @Override
  public boolean saveOrderDetail(YundouOrder yundouOrder) {
    return this.yundouOrderMapper.insert(yundouOrder) > 0;
  }

  @Override
  public YundouProduct loadByOrderId(String orderId) {
    return this.yundouProductMapper.selectByOrderId(orderId);
  }
}
