package com.xquark.service.yundou.operator.executor;

import com.xquark.dal.util.SpringContextUtil;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 17-5-9. DESC:
 */
@Component
public class ExecutorCreator {

  /**
   * 根据规则代码获取相应的Executor
   *
   * @param code 规则代码
   * @return 对应的executor实现类
   * @see YundouRuleType#getExecutor()
   */
  public IExecutor create(Integer code) {
    YundouRuleType ruleType = YundouRuleType.valueOf(code);
    if (ruleType == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "积分规则代码不存在");
    }
    return SpringContextUtil.getBean(ruleType.getExecutor());
  }

}
