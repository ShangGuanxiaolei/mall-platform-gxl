package com.xquark.service.yundou.operator.executor.stock;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.YundouRule;
import com.xquark.dal.vo.YundouRuleVO;
import com.xquark.service.order.OrderService;
import com.xquark.service.yundou.operator.AbstractExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 17-12-2. DESC: 下单积分抵扣规则实现类
 */
@Component
public class OrderDeductionExecutor extends AbstractExecutor {

  private OrderService orderService;

  @Autowired
  public void setOrderService(OrderService orderService) {
    this.orderService = orderService;
  }

  @Override
  protected Long calculateAmount(YundouRule rule, String bizId) {
    Order order = orderService.load(bizId);
    if (order == null) {
      logger.warn("订单 {} 不存在", bizId);
      return 0L;
    }
    return -order.getPaidYundou();
  }

  /**
   * 根据签到id转换为userId, 供抽象类调用
   *
   * @param bizId 业务id
   * @return userId
   * @throws NullPointerException 如果订单不存在
   * @see AbstractExecutor#doExecute(YundouRuleVO, String)
   */
  @Override
  protected String getUSerId(String bizId) {
    Order order = orderService.load(bizId);
    return order.getBuyerId();
  }
}
