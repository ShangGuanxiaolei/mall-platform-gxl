//package com.xquark.service.yundou.operator;
//
//import com.xquark.dal.vo.YundouRuleVO;
//import com.xquark.service.error.BizException;
//import com.xquark.service.yundou.Result;
//import com.xquark.service.yundou.operator.executor.ExecutorCreator;
//import com.xquark.service.yundou.operator.executor.IExecutor;
//import java.util.ArrayList;
//import java.util.List;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Scope;
//import org.springframework.stereotype.Component;
//
///**
// * Created by wangxinhua on 17-5-3.
// * DESC:
// */
//@Component
//@Scope
//public class OrderOperator implements IOperator {
//
//    private static Logger logger = LoggerFactory.getLogger(OrderOperator.class);
//
//    @Autowired
//    private ExecutorCreator executorCreator;
//
//    @Override
//    public List<Result> doOperation(List<YundouRuleVO> rules, String orderId) {
//        logger.debug("计算订单 {} 云豆数", orderId);
//        List<Result> results = new ArrayList<Result>();
//        for (YundouRuleVO rule : rules) {
//            IExecutor executor = executorCreator.create(rule.getCode());
//            // 根据订单Id和规则获取结果
//            if (executor != null) {
//                logger.debug("获取executor : {}", executor.getClass().getName());
//                try {
//                    logger.debug("执行规则 {}", rule.getName());
//                    List<Result> exeResults = executor.doExecute(rule, orderId);
//                    results.addAll(exeResults);
//                } catch (BizException e) {
//                    throw e;
//                } catch (Exception e) {
//                    logger.error("执行规则 {} 错误", rule.getName(), e);
//                }
//            }
//        }
//        return results;
//    }
//}
