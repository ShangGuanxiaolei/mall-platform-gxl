package com.xquark.service.yundou.operator;

import com.xquark.dal.vo.YundouRuleVO;
import com.xquark.service.yundou.Result;
import java.util.List;

/**
 * Created by wangxinhua on 17-5-3. DESC:
 */
public interface IOperator {

  List<Result> doOperation(List<YundouRuleVO> rules, String id) throws Exception;

}
