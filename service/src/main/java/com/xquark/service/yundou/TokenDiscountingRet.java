package com.xquark.service.yundou;

import com.google.common.base.Optional;
import java.math.BigDecimal;

/**
 * @author wangxinhua.
 * @date 2018/12/18
 */
public class TokenDiscountingRet {

  private final BigDecimal maxUsing;
  private final BigDecimal using;
  private final BigDecimal discount;
  private final BigDecimal maxUsingComissionAddPoint;

  public TokenDiscountingRet(BigDecimal maxUsing, BigDecimal using, BigDecimal discount, BigDecimal maxUsingComissionAddPoint) {
    this.maxUsing = maxUsing;
    this.using = using;
    this.discount = discount;
    this.maxUsingComissionAddPoint = maxUsingComissionAddPoint;
  }

  public BigDecimal getMaxUsing() {
    return Optional.fromNullable(maxUsing).or(BigDecimal.ZERO);
  }

  public BigDecimal getUsing() {
    return Optional.fromNullable(using).or(BigDecimal.ZERO);
  }

  public BigDecimal getDiscount() {
    return Optional.fromNullable(discount).or(BigDecimal.ZERO);
  }

  public BigDecimal getMaxUsingComissionAddPoint() {
    return maxUsingComissionAddPoint;
  }
}
