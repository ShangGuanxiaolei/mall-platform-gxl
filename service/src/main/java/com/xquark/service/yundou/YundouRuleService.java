package com.xquark.service.yundou;

import com.xquark.dal.model.YundouRule;
import com.xquark.dal.model.YundouRuleDetail;
import com.xquark.dal.vo.YundouRuleVO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Created by wangxinhua on 17-5-3. DESC:
 */
public interface YundouRuleService {

  Boolean save(YundouRuleVO yundouRule);

  Boolean deleteById(String id);

  Boolean deleteByCode(Integer code);

  Boolean deleteDetail(String ruleId);

  Boolean updateById(YundouRuleVO yundouRule);

  Boolean updateDetail(YundouRuleDetail detail);

  YundouRule load(String id);

  YundouRuleDetail loadDetail(String ruleId);

  YundouRuleDetail loadDetailAmount(String ruleId, String roleId);

  YundouRule loadByName(String name);

  YundouRuleVO loadRuleVO(String id);

  List<YundouRule> list(String order, Pageable pageable, Sort.Direction direction);

  List<YundouRule> listByOperationId(String operationId, String order, Pageable pageable,
      Sort.Direction direction);

  List<YundouRule> listByOperationCode(Integer code, String order, Pageable pageable,
      Sort.Direction direction);

  List<YundouRuleVO> listRuleDetail(String operationId, String order, Pageable pageable,
      Sort.Direction direction);

  List<YundouRuleVO> listRuleDetail(String operationId);

  Integer count();

  Integer countByOperation(String operationId);
}
