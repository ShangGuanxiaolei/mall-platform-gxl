package com.xquark.service.yundou.operator;

import com.xquark.dal.vo.YundouRuleVO;
import com.xquark.service.yundou.Result;
import com.xquark.service.yundou.operator.executor.ExecutorCreator;
import com.xquark.service.yundou.operator.executor.IExecutor;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangxinhua on 17-5-10. DESC:
 */
@Component
public class CommonOperator implements IOperator {

  private static Logger logger = LoggerFactory.getLogger(CommonOperator.class);

  private ExecutorCreator executorCreator;

  /**
   * 根据规则和id得到云豆计算结果
   *
   * @param rules 规则List
   * @param bizId 业务id
   * @return 结果集合
   */
  @Transactional
  @Override
  public List<Result> doOperation(List<YundouRuleVO> rules, String bizId) throws Exception {
    logger.debug("计算 {} 云豆数", bizId);
    List<Result> results = new ArrayList<>();
    for (YundouRuleVO rule : rules) {
      // 根据规则代码创建对应的规则实现类
      IExecutor executor = executorCreator.create(rule.getCode());
      // 根据Id和规则获取结果
      if (executor != null) {
        logger.debug("获取executor : {}", executor.getClass().getName());
        try {
          if (rule.getEnable()) {
            logger.debug("执行规则 {}", rule.getName());
            List<Result> exeResults = executor.doExecute(rule, bizId);
            if (CollectionUtils.isNotEmpty(exeResults)) {
              results.addAll(exeResults);
            }
          } else {
            logger.debug("规则 {} 已被禁用, 跳过执行", rule.getName());
          }
        } catch (Exception e) {
          logger.error("执行规则 {} 错误", rule.getName(), e);
          throw e;
        }
      }
    }
    return results;
  }

  @Autowired
  public void setExecutorCreator(ExecutorCreator executorCreator) {
    this.executorCreator = executorCreator;
  }
}
