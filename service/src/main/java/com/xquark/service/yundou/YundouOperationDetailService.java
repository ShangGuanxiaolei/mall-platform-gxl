package com.xquark.service.yundou;

import com.xquark.dal.model.YundouOperationDetail;
import com.xquark.dal.type.YundouOperationType;
import com.xquark.dal.vo.YundouOperationDetailVO;
import com.xquark.dal.vo.YundouRuleDetailVO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by wangxinhua on 17-5-11. DESC:
 */
public interface YundouOperationDetailService {

  Boolean save(YundouOperationDetail detail);

  Boolean delete(String id);

  Boolean update(YundouOperationDetail detail);

  YundouOperationDetail selectById(String id);

  List<YundouOperationDetail> selectAll(String order, Pageable pageable, String direction);

  List<YundouOperationDetail> selectAll();

  List<YundouOperationDetail> selectByUser(String userId, String order, Pageable pageable,
      String direction);

  List<YundouOperationDetail> selectByUserAndDate(String userId, Date from, Date to, String order,
      Pageable pageable, String direction);

  List<YundouOperationDetail> selectLastWeekByUser(String userId, String order, Pageable pageable,
      String direction);

  List<YundouOperationDetail> selectLastMonthByUser(String userId, String order,
      Pageable pageable, String direction);

  List<YundouOperationDetail> selectLastYearByUser(String userId, String order, Pageable pageable,
      String direction);

  /**
   * 服务端分页查询数据
   */
  List<YundouOperationDetailVO> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, ?> params);

  /**
   * 战队概况数据统计
   */
  Map getSummary(String shopId);

  boolean hasDetail(YundouOperationType operationType, String bizId);

  List<YundouOperationDetail> listByOperationAndBizId(YundouOperationType operationType,
      String bizId);

  List<YundouRuleDetailVO> listRuleDetailByUser(String userId, String order,
      Sort.Direction direction, Pageable pageable);

  Long countRuleDetailByUserId(String userId);
}
