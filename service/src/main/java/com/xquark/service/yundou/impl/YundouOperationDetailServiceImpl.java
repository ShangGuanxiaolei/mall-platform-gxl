package com.xquark.service.yundou.impl;

import com.xquark.dal.mapper.YundouOperationDetailMapper;
import com.xquark.dal.model.YundouOperationDetail;
import com.xquark.dal.type.YundouOperationType;
import com.xquark.dal.vo.YundouOperationDetailVO;
import com.xquark.dal.vo.YundouRuleDetailVO;
import com.xquark.service.yundou.YundouOperationDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by wangxinhua on 17-5-11. DESC:
 */
@Service
public class YundouOperationDetailServiceImpl implements YundouOperationDetailService {

  @Autowired
  private YundouOperationDetailMapper detailMapper;

  private static final Logger logger = LoggerFactory
      .getLogger(YundouOperationDetailServiceImpl.class);

  @Override
  public Boolean save(YundouOperationDetail detail) {
    return detailMapper.insert(detail) > 0;
  }

  @Override
  public Boolean delete(String id) {
    return detailMapper.delete(id) > 0;
  }

  @Override
  public Boolean update(YundouOperationDetail detail) {
    return detailMapper.update(detail) > 0;
  }

  @Override
  public YundouOperationDetail selectById(String id) {
    return detailMapper.selectById(id);
  }

  @Override
  public List<YundouOperationDetail> selectAll(String order, Pageable pageable,
      String direction) {
    return detailMapper.selectAll(order, pageable, direction);
  }

  @Override
  public List<YundouOperationDetail> selectAll() {
    return detailMapper.selectAll(null, null, null);
  }

  @Override
  public List<YundouOperationDetail> selectByUser(String userId, String order, Pageable pageable,
      String direction) {
    return detailMapper.selectByUser(userId, order, pageable, direction);
  }

  @Override
  public List<YundouOperationDetail> selectByUserAndDate(String userId, Date from, Date to,
      String order, Pageable pageable, String direction) {
    return detailMapper.selectByUserAndDate(userId, from, to, order, pageable, direction);
  }

  @Override
  public List<YundouOperationDetail> selectLastWeekByUser(String userId, String order,
      Pageable pageable, String direction) {
    return detailMapper.selectLastWeekByUser(userId, order, pageable, direction);
  }

  @Override
  public List<YundouOperationDetail> selectLastMonthByUser(String userId, String order,
      Pageable pageable, String direction) {
    return detailMapper.selectLastMonthByUser(userId, order, pageable, direction);
  }

  @Override
  public List<YundouOperationDetail> selectLastYearByUser(String userId, String order,
      Pageable pageable, String direction) {
    return detailMapper.selectLastYearByUser(userId, order, pageable, direction);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<YundouOperationDetailVO> list(Pageable pager, Map<String, Object> params) {
    return detailMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, ?> params) {
    return detailMapper.selectCnt(params);
  }

  /**
   * 概况数据统计
   */
  @Override
  public Map getSummary(String shopId) {
    // 缓存今天的各项值
    Map<String, Object> info = new HashMap<String, Object>();
    // 缓存一周的各项值
    Map<String, Object> nums = new HashMap<String, Object>();

    Map<String, Object> result = new HashMap<String, Object>();
    ArrayList dates = getWeekDay();

    // 总发放积分
    long total_out = detailMapper.getTotalByDirection("1");
    // 总消耗积分
    long total_in = detailMapper.getTotalByDirection("0");

    info.put("total_out", total_out);
    info.put("total_in", total_in);

    ArrayList weekIn = (ArrayList) detailMapper.getWeekByDirection("0", dates);
    ArrayList weekOut = (ArrayList) detailMapper.getWeekByDirection("1", dates);

    // 近七天发放积分，消耗积分
    nums.put("out", formatList(weekOut));
    nums.put("in", formatList(weekIn));

    nums.put("weekDays", dates);
    result.put("info", info);
    result.put("nums", nums);
    return result;
  }

  /**
   * 获取一周内的所有日期
   */
  private static ArrayList getWeekDay() {
    ArrayList<String> list = new ArrayList<String>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Calendar calendar = Calendar.getInstance();
    while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
      calendar.add(Calendar.DATE, -1);
    }
    for (int i = 0; i < 7; i++) {
      list.add(sdf.format(calendar.getTime()));
      calendar.add(Calendar.DATE, 1);
    }
    return list;
  }

  /**
   * 将从数据库中取出的每天对应的各项值map数据结构做一次格式化 返回当天的值，如数据库没有则是0,同时返回周一到周日的值list(如果某天没有值，则为0)
   */
  private ArrayList formatList(ArrayList<Map> list) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    ArrayList weekamountvalue = new ArrayList();
    // 今天
    String nowstr = sdf.format(new Date());
    HashMap result = new HashMap();
    // 得到一周内的每天的str字符串
    ArrayList<String> dates = getWeekDay();
    // 循环得到当天的值和周一到周天顺序每天的值
    for (String date : dates) {
      String temp_amount = "0";
      for (Map value : list) {
        String s_date = (String) value.get("s_date");
        String c_amount = "" + value.get("c_amount");
        if (date.equals(s_date)) {
          temp_amount = c_amount;
          break;
        }
      }
      weekamountvalue.add(temp_amount);
    }
    return weekamountvalue;
  }

  @Override
  public boolean hasDetail(YundouOperationType operationType, String bizId) {
    return detailMapper.hasDetail(operationType.getCode(), bizId) > 0;
  }

  @Override
  public List<YundouOperationDetail> listByOperationAndBizId(YundouOperationType operationType,
      String bizId) {
    return detailMapper.selectByOperationAndBizId(operationType.getCode(), bizId);
  }

  @Override
  public List<YundouRuleDetailVO> listRuleDetailByUser(String userId, String order,
      Sort.Direction direction, Pageable pageable) {
    return detailMapper.listRuleDetailByUser(userId, order, direction, pageable);
  }

  @Override
  public Long countRuleDetailByUserId(String userId) {
    return detailMapper.countRuleDetailByUserId(userId);
  }
}
