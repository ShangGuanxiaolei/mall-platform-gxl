package com.xquark.service.yundou;

import com.xquark.dal.model.YundouOperation;
import com.xquark.dal.type.YundouOperationType;

import java.util.List;

/**
 * Created by wangxinhua on 17-5-3. DESC:
 */
public interface YundouAmountService {

  /**
   * 根据操作与业务id做相关处理， 返回用户id与相应云豆数
   *
   * @param operation 操作对象
   * @param buzId 业务id， 可以为订单id或其他id，实现IOperator做定制化处理
   * @return 用户与增减云豆数的集合
   */
  List<Result> modifyAmount(YundouOperation operation, String buzId);

  List<Result> modifyAmount(YundouOperationType operationType, String buzId);

}
