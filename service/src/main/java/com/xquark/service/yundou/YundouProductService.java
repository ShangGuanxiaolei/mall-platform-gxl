package com.xquark.service.yundou;

import com.xquark.dal.model.YundouOrder;
import com.xquark.dal.model.YundouProduct;
import com.xquark.dal.type.YundouCategoryType;
import com.xquark.dal.vo.YundouProductVO;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public interface YundouProductService extends BaseEntityService<YundouProduct> {

  YundouProductVO load(String id);

  int insert(YundouProduct yundouProduct);

  int deleteForArchive(String id);

  int update(YundouProduct record);

  /**
   * 服务端分页查询数据
   */
  List<YundouProductVO> list(Pageable pager, Map<String, Object> params);

  List<YundouProductVO> listWithCategory(YundouCategoryType categoryType, String order,
      Sort.Direction direction, Pageable pageable);

  List<YundouProductVO> listWithCategory(YundouCategoryType[] categoryTypes);

  List<YundouProductVO> listAll();

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  Long selectCntWithProducts(Map<String, Object> params);

  /**
   * 查询是否已经有该商品的分佣设置
   */
  Long countByProductId(String shopId, String productId);

  List<Map<String, Object>> listCategory();

  List<List<YundouProductVO>> listCategoryList();

  /**
   * 查询所有积分商品， 并根据类行分类
   */
  Map<String, ArrayList<YundouProductVO>> listCategoryMap();

  ArrayList<YundouProductVO> listCapable();

  ArrayList<YundouProductVO> listCapable(Pageable pageable);

  /**
   * 查询当前用户满足购买的积分条件的商品
   */
  List<YundouProductVO> listCapable(String order,
      Sort.Direction direction, Pageable pageable);

  YundouProductVO loadByProductId(String productId, String shopId);

  YundouProductVO loadByProductId(String productId);

  boolean checkIsYundouEnough(String yundouProductId);

  boolean checkIsYundouEnough(String yundouProductId, int amount);

  BigDecimal getDiscountPrice(BigDecimal originalPrice, String yundouProductId);

  boolean saveOrderDetail(YundouOrder yundouOrder);

  YundouProduct loadByOrderId(String orderId);

}

