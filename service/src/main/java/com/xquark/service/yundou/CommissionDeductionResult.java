package com.xquark.service.yundou;

import java.math.BigDecimal;

/**
 * created by
 *
 * @author wangxinhua at 18-6-29 下午11:08
 */
public class CommissionDeductionResult {

  private final BigDecimal maxUsing;

  private BigDecimal using;

  public CommissionDeductionResult(BigDecimal maxUsing) {
    this.maxUsing = maxUsing;
  }

  public BigDecimal getMaxUsing() {
    return maxUsing;
  }

  public BigDecimal getUsing() {
    return using;
  }

  public void setUsing(BigDecimal using) {
    this.using = using;
  }
}
