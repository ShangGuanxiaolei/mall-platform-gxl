package com.xquark.service.yundou;

import java.math.BigDecimal;

/**
 * @author wangxinhua.
 * @date 2018/12/18
 */
public class CommissionDiscountingRet extends TokenDiscountingRet {

  public CommissionDiscountingRet(BigDecimal maxUsing, BigDecimal using,
      BigDecimal discount, BigDecimal maxUsingComission) {
    super(maxUsing, using, discount, maxUsingComission);
  }
}
