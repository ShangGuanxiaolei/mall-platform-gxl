package com.xquark.service.yundou.impl;

import com.xquark.dal.model.YundouOperation;
import com.xquark.dal.model.YundouOperationDetail;
import com.xquark.dal.type.YundouOperationType;
import com.xquark.dal.vo.YundouRuleVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderService;
import com.xquark.service.user.UserService;
import com.xquark.service.yundou.Result;
import com.xquark.service.yundou.YundouAmountService;
import com.xquark.service.yundou.YundouOperationDetailService;
import com.xquark.service.yundou.YundouOperationService;
import com.xquark.service.yundou.YundouRuleService;
import com.xquark.service.yundou.operator.IOperator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangxinhua on 17-5-3. DESC:
 */
@Service
public class YundouAmountServiceImpl implements YundouAmountService {

  private static Logger logger = LoggerFactory.getLogger(YundouAmountServiceImpl.class);

  private YundouRuleService ruleService;

  private YundouOperationDetailService detailService;

  private IOperator operator;

  private YundouOperationService operationService;

  private OrderService orderService;

  private UserService userService;

  @Autowired
  public void setRuleService(YundouRuleService ruleService) {
    this.ruleService = ruleService;
  }

  @Autowired
  public void setDetailService(YundouOperationDetailService detailService) {
    this.detailService = detailService;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setOperator(IOperator operator) {
    this.operator = operator;
  }

  @Autowired
  public void setOperationService(YundouOperationService operationService) {
    this.operationService = operationService;
  }

  @Autowired
  public void setOrderService(OrderService orderService) {
    this.orderService = orderService;
  }

  @Override
  public List<Result> modifyAmount(YundouOperation operation, String bizId) {
    if (operation == null) {
      logger.warn("操作未定义");
      return new ArrayList<>();
    }
    String operationId = operation.getId();

    // 操作规则
    List<YundouRuleVO> rules = ruleService.listRuleDetail(operationId);
    if (rules == null || rules.size() <= 0) {
      return Collections.emptyList();
    }

    List<Result> results;
    try {
      results = operator.doOperation(rules, bizId);
      // 修改用户的云豆数量并记录到明细表
      saveAmount(results, bizId);
    } catch (Exception e) {
      logger.error("执行操作 {} 失败", operation.getName());
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "执行操作失败");
    }
    return results;
  }

  @Override
  @Transactional
  public List<Result> modifyAmount(YundouOperationType operationType, String buzId) {
    YundouOperation operation = operationService.loadByCode(operationType.getCode());
    if (operation == null) {
      logger.warn("操作未定义");
      return new ArrayList<>();
    }
    if (!operation.getEnable()) {
      logger.warn("操作已禁用， 跳过");
      return new ArrayList<>();
    }
    return this.modifyAmount(operation, buzId);
  }

  private void saveAmount(List<Result> results, String bizId) throws Exception {
    // 将返回的数据中用户id相同的整理到一起
    for (Result result : results) {
      String userId = result.getUserId();
      String ruleId = result.getRuleId();
      Long amount = result.getAmount();
      if (amount == 0L) {
        break;
      }
      // 修改用户的云豆数量
      try {
        userService.modifyYundou(userId, amount);
      } catch (Exception e) {
        logger.error("修改用户云豆数量错误", e);
        throw e;
      }
      YundouOperationDetail detail = new YundouOperationDetail();
      detail.setUserId(userId);
      detail.setRuleId(ruleId);
      detail.setAmount(amount);
      detail.setBizId(bizId);
      // 记录到明细表
      try {
        detailService.save(detail);
      } catch (Exception e) {
        logger.error("规则 {} 记录明细发生错误", ruleId, e);
        throw e;
      }
    }

  }

}
