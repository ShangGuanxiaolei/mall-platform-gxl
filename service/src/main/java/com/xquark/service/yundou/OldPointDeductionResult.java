package com.xquark.service.yundou;

import com.google.common.base.Preconditions;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.type.Money;
import com.xquark.utils.CommonUtils;
import java.math.BigDecimal;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Created by wangxinhua on 17-11-28. DESC:
 */
@ApiModel("积分/德分相关")
@Deprecated
public class OldPointDeductionResult {

  @ApiModelProperty("当前用户对该商品能否使用积分")
  private boolean canUse;

  @ApiModelProperty("可以使用积分情况下需要使用多少积分")
  private Long payYundou;

  @ApiModelProperty("积分/德分总共可以抵扣多少钱")
  private BigDecimal deductionPrice;

  private OldPointDeductionResult(boolean canUse, Long payYundou, BigDecimal deductionPrice) {
    this.canUse = canUse;
    this.payYundou = payYundou;
    this.deductionPrice = deductionPrice;
  }

  public boolean isCanUse() {
    return canUse;
  }

  public Long getPayYundou() {
    return payYundou;
  }

  public BigDecimal getDeductionPrice() {
    return deductionPrice;
  }

  /**
   * 将另一个结果合并到当前结果上
   *
   * @param another 需合并的结果对象
   */
  public void merge(OldPointDeductionResult another) {
    Preconditions.checkNotNull(another);
    this.canUse = this.canUse || another.canUse;
    this.payYundou += another.payYundou;
    this.deductionPrice = this.deductionPrice.add(another.getDeductionPrice());
  }

  public static OldPointDeductionResult emptyResult() {
    return new OldPointDeductionResult(false, 0L, BigDecimal.ZERO);
  }

  public static OldPointDeductionResult buildResult(boolean canUser, Long payYundou,
      BigDecimal deductionPrice) {
    return new OldPointDeductionResult(canUser, payYundou, deductionPrice);
  }

  /**
   * 获取 积分/减免金额 格式的字符串
   *
   * @return 格式化字符串 供前端展示
   */
  public String getYundouDeductionStr() {
    Money money = new Money(deductionPrice);
    return payYundou.toString().concat("积分/").concat(CommonUtils
        .getNumberFormatFromCache(LocaleContextHolder.getLocale(), money.getCurrency())
        .format(money.getAmount()));
  }
}
