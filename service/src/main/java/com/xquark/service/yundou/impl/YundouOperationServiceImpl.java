package com.xquark.service.yundou.impl;

import com.xquark.dal.mapper.YundouOperationMapper;
import com.xquark.dal.mapper.YundouRuleMapper;
import com.xquark.dal.model.YundouOperation;
import com.xquark.dal.model.YundouOperationType;
import com.xquark.service.yundou.YundouOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by wangxinhua on 17-5-3. DESC:
 */
@Service
public class YundouOperationServiceImpl implements YundouOperationService {

  @Autowired
  private YundouOperationMapper operationMapper;

  @Autowired
  private YundouRuleMapper yundouRuleMapper;

  @Override
  public Boolean save(YundouOperation operation) {
    return operationMapper.insert(operation) > 0;
  }

  @Override
  @Transactional
  public Boolean deleteById(String id) {
    boolean deleteRulesResult = yundouRuleMapper.deleteByOperationId(id) > 0;
    return operationMapper.deleteById(id) > 0;
  }

  @Override
  public Boolean deleteByCode(Integer code) {
    return operationMapper.deleteByCode(code) > 0;
  }

  @Override
  public Boolean updateById(YundouOperation operation) {
    return operationMapper.updateById(operation) > 0;
  }

  @Override
  public YundouOperation load(String id) {
    return operationMapper.selectById(id);
  }

  @Override
  public YundouOperationType loadType(String operationId) {
    return operationMapper.selectType(operationId);
  }

  @Override
  public YundouOperation loadByName(String name) {
    return operationMapper.selectByName(name);
  }

  @Override
  public YundouOperation loadByCode(Integer code) {
    return operationMapper.selectByCode(code);
  }

  @Override
  public List<YundouOperation> list(String order, Pageable pageable, Sort.Direction direction) {
    return operationMapper.selectAll(order, pageable, direction);
  }

  @Override
  public List<YundouOperationType> listTypes() {
    return operationMapper.selectTypes();
  }

  @Override
  public Integer count() {
    return operationMapper.count();
  }

  @Override
  public Integer countTypes() {
    return operationMapper.countTypes();
  }
}
