package com.xquark.service.yundou.impl;

import com.xquark.dal.mapper.YundouSettingMapper;
import com.xquark.dal.model.YundouSetting;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.yundou.YundouSettingService;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("yundouSettingService")
public class YundouSettingServiceImpl extends BaseServiceImpl implements YundouSettingService,
    InitializingBean {

  @Autowired
  YundouSettingMapper yundouSettingMapper;

  @Override
  public int insert(YundouSetting team) {
    return yundouSettingMapper.insert(team);
  }

  @Override
  public int insertOrder(YundouSetting yundouSetting) {
    return yundouSettingMapper.insert(yundouSetting);
  }

  @Override
  public YundouSetting load(String id) {
    return yundouSettingMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return yundouSettingMapper.updateForArchive(id);
  }

  @Override
  public int update(YundouSetting record) {
    int result = yundouSettingMapper.updateByPrimaryKeySelective(record);
    reloadYundouSetting();
    return result;
  }

  @Override
  public YundouSetting selectByShopId(String shopId) {
    return yundouSettingMapper.selectByShopId(shopId);
  }

  @Override
  public List<YundouSetting> list() {
    return yundouSettingMapper.list();
  }

  @Override
  public YundouSetting getGlobalSetting() {
    return GLOBAL_SETTING;
  }

  @Override
  public YundouSetting loadUnique() {
    List<YundouSetting> settings = this.list();
    if (settings.size() == 0) {
      return null;
    }
    if (settings.size() > 1) {
      log.warn("积分设置表包含多条记录");
    }
    return settings.get(0);
  }

  private void reloadYundouSetting() {
    YundouSetting setting = loadUnique();
    synchronized (GLOBAL_SETTING) {
      BeanUtils.copyProperties(setting, GLOBAL_SETTING);
    }
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    reloadYundouSetting();
  }
}
