package com.xquark.service.yundou;

import com.google.common.base.Preconditions;
import com.xquark.dal.consts.PointConstants;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.math.BigDecimal.ROUND_HALF_EVEN;
import static java.math.BigDecimal.ROUND_UNNECESSARY;

/**
 * created by
 *
 * @author wangxinhua at 18-6-13 下午1:48 积分、德分结果
 */
public class PointCommDeductionResult {

  private PointCommDeductionResult(BigDecimal maxUsingPoint,
      BigDecimal maxUsingCommission) {
    this.maxUsingPoint = maxUsingPoint;
    this.maxUsingCommission = maxUsingCommission;
    this.pointDeductionPrice = maxUsingPoint.divide(PointConstants.POINT_SCALA,
        BigDecimal.ROUND_HALF_EVEN);
    this.commissionDeductionPrice = maxUsingCommission.divide(PointConstants.COMMISSION_SCALA,
        BigDecimal.ROUND_HALF_EVEN);
  }

  private PointCommDeductionResult() {
    this.maxUsingPoint = BigDecimal.ZERO;
    this.maxUsingCommission = BigDecimal.ZERO;
    this.pointDeductionPrice = BigDecimal.ZERO;
    this.commissionDeductionPrice = BigDecimal.ZERO;
    this.maxUsingCommissionAddPoint = BigDecimal.ZERO;
  }

  /**
   * 计算值, 用户最多可使用积分
   */
  private BigDecimal maxUsingPoint;

  /**
   * 计算值, 用户最多可使用德分
   */
  private BigDecimal maxUsingCommission;

  /**
   * 积分可抵扣价格
   */
  private BigDecimal pointDeductionPrice;

  /**
   * 德分可抵扣价格
   */
  private BigDecimal commissionDeductionPrice;

  /**
   * 用户使用德分
   */
  private BigDecimal usingPoint = BigDecimal.ZERO;

  /**
   * 用户使用红包德分
   */
  private BigDecimal usingPacketPoint = BigDecimal.ZERO;

  /**
   * 用户使用积分
   */
  private BigDecimal usingCommission = BigDecimal.ZERO;

  /**
   * 实际积分抵扣价格
   */
  private BigDecimal usingPointDeductionPrice = BigDecimal.ZERO;

  /**
   * 德分 - sku可使用明细 key - skuId, value - 德分值
   */
  private final Map<String, BigDecimal> pointSkuMaxDeductionMap = new LinkedHashMap<>();

  /**
   * 积分 - sku可使用明细 key - skuId, value - 积分值
   */
  private final Map<String, BigDecimal> commissionSkuMaxDeductionMap = new LinkedHashMap<>();

  /**
   * 实际德分抵扣价格
   */
  private BigDecimal usingCommissionDeductionPrice = BigDecimal.ZERO;

  /**
   * 用户可以使用的最大积分（加上德分）
   */
  private BigDecimal maxUsingCommissionAddPoint;

  public BigDecimal getMaxUsingCommissionAddPoint() {
    return maxUsingCommissionAddPoint;
  }

  public void setMaxUsingCommissionAddPoint(BigDecimal maxUsingCommissionAddPoint) {
    this.maxUsingCommissionAddPoint = maxUsingCommissionAddPoint;
  }

  public static PointCommDeductionResult empty() {
    return new PointCommDeductionResult();
  }

  public static PointCommDeductionResult build(BigDecimal maxUsingCommission,
      BigDecimal maxUsingPoint) {
    return new PointCommDeductionResult(maxUsingPoint, maxUsingCommission);
  }

  public void merge(PointCommDeductionResult another) {
    Preconditions.checkNotNull(another);
    this.maxUsingPoint = this.maxUsingPoint.add(another.getMaxUsingPoint());
    this.maxUsingCommission = this.maxUsingCommission.add(another.getMaxUsingCommission());
    this.pointDeductionPrice = this.pointDeductionPrice.add(another.getPointDeductionPrice());
    this.commissionDeductionPrice = this.commissionDeductionPrice
        .add(another.getCommissionDeductionPrice());
    this.usingPointDeductionPrice = this.usingPointDeductionPrice
        .add(another.getUsingPointDeductionPrice());
    this.usingCommissionDeductionPrice = this.usingCommissionDeductionPrice
        .add(another.getUsingCommissionDeductionPrice());
    this.pointSkuMaxDeductionMap.putAll(another.getPointSkuMaxDeductionMap());
    this.commissionSkuMaxDeductionMap.putAll(another.getCommissionSkuMaxDeductionMap());
  }

  public BigDecimal getMaxUsingPoint() {
    return maxUsingPoint;
  }

  public BigDecimal getMaxUsingCommission() {
    return maxUsingCommission;
  }

  public BigDecimal getPointDeductionPrice() {
    return pointDeductionPrice;
  }

  public BigDecimal getCommissionDeductionPrice() {
    return commissionDeductionPrice;
  }

  public BigDecimal getUsingPointDeductionPrice() {
    return usingPointDeductionPrice;
  }

  public void setUsingPointDeductionPrice(BigDecimal usingPointDeductionPrice) {
    this.usingPointDeductionPrice = usingPointDeductionPrice;
  }

  public BigDecimal getUsingCommissionDeductionPrice() {
    return usingCommissionDeductionPrice;
  }

  public void setUsingCommissionDeductionPrice(BigDecimal usingCommissionDeductionPrice) {
    this.usingCommissionDeductionPrice = usingCommissionDeductionPrice;
  }

  /**
   * 获取总优惠金额
   *
   * @return 总优惠金额
   */
  public BigDecimal getTotalDeduction() {
    return usingCommissionDeductionPrice.add(usingPointDeductionPrice);
  }

  public BigDecimal getUsingPoint() {
    return usingPoint;
  }

  public void setUsingPoint(BigDecimal usingPoint) {
    this.usingPoint = usingPoint;
  }

  public BigDecimal getUsingCommission() {
    return usingCommission;
  }

  public void setUsingCommission(BigDecimal usingCommission) {
    this.usingCommission = usingCommission;
  }

  public Map<String, BigDecimal> getPointSkuMaxDeductionMap() {
    return pointSkuMaxDeductionMap;
  }

  public Map<String, BigDecimal> getCommissionSkuMaxDeductionMap() {
    return commissionSkuMaxDeductionMap;
  }

  public void setMaxUsingPoint(BigDecimal maxUsingPoint) {
    this.maxUsingPoint = maxUsingPoint;
  }

  public void setMaxUsingCommission(BigDecimal maxUsingCommission) {
    this.maxUsingCommission = maxUsingCommission;
  }

  public BigDecimal getUsingPacketPoint() {
    return usingPacketPoint;
  }

  public void setUsingPacketPoint(BigDecimal usingPacketPoint) {
    this.usingPacketPoint = usingPacketPoint;
  }
}
