package com.xquark.service.yundou.operator.executor.stock;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.YundouRule;
import com.xquark.dal.vo.YundouRuleVO;
import com.xquark.service.order.OrderService;
import com.xquark.service.yundou.YundouOperationDetailService;
import com.xquark.service.yundou.operator.AbstractExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 17-8-10. DESC:
 */
@Component
public class RefundOrderExecutor extends AbstractExecutor {

  private YundouOperationDetailService detailService;

  private OrderService orderService;

  @Autowired
  public void setDetailService(YundouOperationDetailService detailService) {
    this.detailService = detailService;
  }

  @Autowired
  public void setOrderService(OrderService orderService) {
    this.orderService = orderService;
  }

  @Override
  protected Long calculateAmount(YundouRule rule, String bizId) {
    Order order = orderService.load(bizId);
    if (order == null) {
      logger.warn("订单 {} 不存在, 不执行退积分", bizId);
      return 0L;
    }
    // 返还扣减掉的积分
    return order.getPaidYundou();
  }

  @Override
  protected String getUSerId(String bizId) {
    return orderService.load(bizId).getBuyerId();
  }

  @Override
  protected boolean isSkip(YundouRuleVO ruleVO, String bizId) {
    // 暂时关闭
    return true;
  }
}
