package com.xquark.service.yundou.operator.executor.promotion;

import com.xquark.dal.model.YundouRule;
import com.xquark.dal.model.YundouRuleDetail;
import com.xquark.service.yundou.YundouRuleService;
import com.xquark.service.yundou.operator.AbstractExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 17-12-8. DESC:
 */
@Component
public class FanCardExchangeExecutor extends AbstractExecutor {

  private static final long DEFAULT_ADD_YUNDOU = 1111L;

  private YundouRuleService yundouRuleService;

  @Autowired
  public FanCardExchangeExecutor(YundouRuleService yundouRuleService) {
    this.yundouRuleService = yundouRuleService;
  }

  @Override
  protected Long calculateAmount(YundouRule rule, String bizId) {
    assert rule != null;
    YundouRuleDetail detail = yundouRuleService.loadDetail(rule.getId());
    if (detail == null || detail.getAmount() == null) {
      return DEFAULT_ADD_YUNDOU;
    }
    return detail.getAmount();
  }

  @Override
  protected String getUSerId(String bizId) {
    return bizId;
  }

}
