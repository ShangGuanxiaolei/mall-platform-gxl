package com.xquark.service.systemRegion.impl;


import com.xquark.dal.mapper.SystemRegionMapper;
import com.xquark.dal.model.PostConf;
import com.xquark.dal.model.SystemRegion;
import com.xquark.dal.model.Zone;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.systemRegion.SystemRegionService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.ArrayUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * User: a9175
 * Date: 2018/6/19.
 * Time: 15:11
 */
@Service("systemRegionService")
public class SystemRegionServiceImpl extends BaseServiceImpl implements SystemRegionService {
    @Autowired
    private SystemRegionMapper systemRegionMapper;

    @Override
    public List<SystemRegion> listRoots() {
        return systemRegionMapper.listRoots();
    }


    @Override
    public List<SystemRegion> listChildren(String zoneId) {  //根据id 查找子级
        List<SystemRegion> list = systemRegionMapper.listChildren(zoneId);
        if (list == null) {
            return Collections.emptyList();
        }

        return list;
    }

    @Override
    public List<SystemRegion> listParents(String regionId) { //根据zone id 查找上级全部
      return systemRegionMapper.listParents(regionId);
    }

    private List<SystemRegion> findByIds(String... zoneIds) {
        if (ArrayUtils.isEmpty(zoneIds)) {
            return Collections.emptyList();
        }

        List<SystemRegion> list = systemRegionMapper.selectByIds(zoneIds);
        if (list == null) {
            return Collections.emptyList();
        }
        return list;
    }

    @Override
    public Map<String, List<String>> loadPostFreeList() {
        List<PostConf> zoneList = systemRegionMapper.selectPostConf();
        if (null == zoneList || 0 == zoneList.size()) {
            return null;
        }
        Map<String, List<String>> ret = new HashedMap<String, List<String>>();
        List<String> commonList = new ArrayList<String>();
        List<String> cityList = new ArrayList<String>();
        List<String> provList = new ArrayList<String>();
        for (PostConf aZone : zoneList) {
            if (aZone.getRegionType().equals("0")) {
                commonList.add(aZone.getName());
            } else if (aZone.getRegionType().equals("1")) {
                cityList.add(aZone.getName());
            } else if (aZone.getRegionType().equals("2")) {
                provList.add(aZone.getName());
            } else {
            }
        }
        ret.put("common", commonList);
        ret.put("city", cityList);
        ret.put("province", provList);
        return ret;
    }

  /**
   *
   * @param zoneId
   * @param street
   * @return
   */
  @Override
  public String buildAddressDetail(@NotBlank String zoneId, String street) {
    List<SystemRegion> regionList = this.listParents(zoneId);
    StringBuilder builder = new StringBuilder();
    if (CollectionUtils.isNotEmpty(regionList)) {
      String area = regionList.get(0).getName();
      String city = regionList.get(1).getName();
      String province = regionList.get(2).getName();
      builder.append(area).append(city).append(province).append(street);
      return builder.toString();
    }
    return "";
  }

    @Override
    public void updateZonePath(String id) {  //没有path
        try {
            SystemRegion systemRegion = systemRegionMapper.selectByPrimaryKey(Long.valueOf(id));
            SystemRegion parent = systemRegionMapper.selectByPrimaryKey(systemRegion.getParentId());
            if (parent != null) {
                //  systemRegionMapper.updatePath(id, parent.getPath() + parent.getId() + ">");
            }
            List<SystemRegion> children = systemRegionMapper.listChildren(id);
            for (SystemRegion z : children) {
                //  updateZonePath(z.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new UnsupportedOperationException();  //此功能未实现
        }

    }

    @Override
    public SystemRegion findParent(String zoneId) {  //找上一级
        return systemRegionMapper.findParent(zoneId);
    }

    @Override
    public SystemRegion load(String id) {
        return systemRegionMapper.selectByPrimaryKey(Long.valueOf(id));
    }  //根据主键id查找

    @Override
    public List<SystemRegion> listSiblings(String zoneId) {
        return systemRegionMapper.listSiblings(zoneId);
    }

    @Override
    public List<SystemRegion> listPostageZones() {
        return systemRegionMapper.listPostAgeArea();
    }

    @Override
    public List<SystemRegion> listPostageCityZones() {
        return systemRegionMapper.selectCity4PostAge();
    } //查找城市

    @Override
    public Map<String, String> listAllAddressExceptStreet() {
        try {
            List<SystemRegion> zoneList = systemRegionMapper.listAll();
            Map<String, SystemRegion> zoneMap = new HashMap<String, SystemRegion>();
            for (SystemRegion zone : zoneList) {
                zoneMap.put(zone.getId().toString(), zone);
            }
            Map<String, String> detailMap = new HashMap<String, String>();
            String details = null;
            for (String zoneId : zoneMap.keySet()) {
                SystemRegion zone = zoneMap.get(zoneId);
                details = "";
//            if (!zone.getPath().isEmpty()) {
//                String[] paths = zone.getPath().split(">");
//                for (String path : paths) {
//                    details += zoneMap.get(path).getName();
//                }
//            }
                details += zone.getName();
                detailMap.put(zoneId, details);
            }
            return detailMap;
        } catch (Exception e) {
            e.printStackTrace();
            throw new UnsupportedOperationException();  //此功能未实现
        }
    }

    /**
     * 通过汇购网对应的zoneid，查找代理系统的zone信息
     */
    @Override
    public SystemRegion selectByExtId(String extId) {
        try {
            return systemRegionMapper.selectByExtId(extId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new UnsupportedOperationException();  //此功能未实现
        }
    }

    /**
     * 查找某个地区下面所有的下级地区，包含自己
     */
    @Override
    public List<SystemRegion> listAllChildrenIncludeSelf(String zoneId, String likeZoneId) {
        try {
            return systemRegionMapper.listAllChildrenIncludeSelf(zoneId, likeZoneId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new UnsupportedOperationException();  //此功能未实现
        }


    }

  public List<SystemRegion> queryByIdList(List<Long> list) {
    return systemRegionMapper.queryByIdList(list);
  }

}
