package com.xquark.service.systemRegion;

import com.xquark.dal.model.SystemRegion;
import com.xquark.dal.model.Zone;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: a9175
 * Date: 2018/6/19.
 * Time: 15:10
 * ${DESCRIPTION}
 */
public interface SystemRegionService {
    SystemRegion load(String id);  //根据id获取

    List<SystemRegion> listRoots();  //获取所有的

    List<SystemRegion> listChildren(String zoneId); //获取下级

    List<SystemRegion> listSiblings(String zoneId);  //未实现

    SystemRegion findParent(String zoneId); //寻找上级

    /**
     * 获取地区以及地区的父级地区
     */
    List<SystemRegion> listParents(String zoneId); //未实现

    Map<String, List<String>> loadPostFreeList();

  String buildAddressDetail(String zoneId, String street);

    void updateZonePath(String id);

    List<SystemRegion> listPostageZones(); //所有的省

    List<SystemRegion> listPostageCityZones();

    Map<String, String> listAllAddressExceptStreet();

    /**
     * 通过汇购网对应的zoneid，查找代理系统的zone信息
     */
    SystemRegion selectByExtId(String extId); //未实现

    /**
     * 查找某个地区下面所有的下级地区，包含自己
     */
    List<SystemRegion> listAllChildrenIncludeSelf(String zoneId, String likeZoneId); //未实现

  List<SystemRegion> queryByIdList(List<Long> list);

}
