package com.xquark.service.mall;

import com.xquark.service.BaseService;

import java.util.Map;

/**
 * Created by hhcao on 16-9-13.
 */
public interface MallService extends BaseService {

  // 商城概况中今日交易额，今日订单数,总订单，已成功订单等
  Map getSummary(String shopId);

}
