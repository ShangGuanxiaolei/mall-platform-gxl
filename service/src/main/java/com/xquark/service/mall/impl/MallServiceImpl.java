package com.xquark.service.mall.impl;

import com.xquark.dal.mapper.MallMapper;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.mall.MallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by root on 16-9-13.
 */
@Service("mallService")
public class MallServiceImpl extends BaseServiceImpl implements MallService {

  @Autowired
  private MallMapper mallMapper;

  @Override
  public Map getSummary(String shopId) {
    // 缓存今天的各项值
    Map<String, Object> info = new HashMap<String, Object>();
    // 缓存一周的各项值
    Map<String, Object> nums = new HashMap<String, Object>();

    Map<String, Object> result = new HashMap<String, Object>();
    ArrayList dates = getWeekDay();

    // 今日交易额
    BigDecimal todayAmount = mallMapper.getTodayAmount(shopId);
    info.put("todayAmount", todayAmount == null ? new BigDecimal("0") : todayAmount);

    // 本周交易额
    /**BigDecimal weekAmount = mallMapper.getWeekAmount(shopId);
     info.put("weekAmount", weekAmount == null ? new BigDecimal("0") : weekAmount);

     // 本月交易额
     BigDecimal monthAmount = mallMapper.getMonthAmount(shopId);
     info.put("monthAmount", monthAmount == null ? new BigDecimal("0") : monthAmount);

     // 本年交易额
     BigDecimal yearAmount = mallMapper.getYearAmount(shopId);
     info.put("yearAmount", yearAmount == null ? new BigDecimal("0") : yearAmount);**/

    // 今日订单数
    BigDecimal todayOrder = mallMapper.getTodayOrder(shopId);
    info.put("todayOrder", todayOrder == null ? new BigDecimal("0") : todayOrder);

    // 待付款订单
    BigDecimal submitOrder = mallMapper.getSubmitOrder(shopId);
    info.put("submitOrder", submitOrder == null ? new BigDecimal("0") : submitOrder);

    // 待发货订单
    BigDecimal paidOrder = mallMapper.getPaidOrder(shopId);
    info.put("paidOrder", paidOrder == null ? new BigDecimal("0") : paidOrder);

    // 维权订单
    BigDecimal refundOrder = mallMapper.getRefundOrder(shopId);
    info.put("refundOrder", refundOrder == null ? new BigDecimal("0") : refundOrder);

    // 本周总订单
    ArrayList weekOrder = (ArrayList) mallMapper.getWeekOrder(shopId, dates);
    ArrayList weekOrdermap = formatList(weekOrder);
    nums.put("weekOrder", weekOrdermap);

    // 本周已成功订单
    ArrayList weekSuccessOrder = (ArrayList) mallMapper.getWeekSuccessOrder(shopId, dates);
    ArrayList weekSuccessOrdermap = formatList(weekSuccessOrder);
    nums.put("weekSuccessOrder", weekSuccessOrdermap);

    // 本周已发货订单
    ArrayList weekShipOrder = (ArrayList) mallMapper.getWeekShipOrder(shopId, dates);
    ArrayList weekShipOrdermap = formatList(weekShipOrder);
    nums.put("weekShipOrder", weekShipOrdermap);

    nums.put("weekDays", dates);

    // 销量排行商品
    ArrayList topProduct = (ArrayList) mallMapper.getTopProduct(shopId);

    result.put("info", info);
    result.put("nums", nums);
    result.put("topProduct", topProduct);
    return result;
  }

  /**
   * 获取一周内的所有日期
   */
  private static ArrayList getWeekDay() {
    ArrayList<String> list = new ArrayList<String>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Calendar calendar = Calendar.getInstance();
    while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
      calendar.add(Calendar.DATE, -1);
    }
    for (int i = 0; i < 7; i++) {
      list.add(sdf.format(calendar.getTime()));
      calendar.add(Calendar.DATE, 1);
    }
    return list;
  }

  /**
   * 将从数据库中取出的每天对应的各项值map数据结构做一次格式化 返回当天的值，如数据库没有则是0,同时返回周一到周日的值list(如果某天没有值，则为0)
   */
  private ArrayList formatList(ArrayList<Map> list) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String today = "0";
    String temp_num = "0";
    ArrayList weekvalue = new ArrayList();
    // 今天
    String nowstr = sdf.format(new Date());
    HashMap result = new HashMap();
    // 得到一周内的每天的str字符串
    ArrayList<String> dates = getWeekDay();
    // 循环得到当天的值和周一到周天顺序每天的值
    for (String date : dates) {
      temp_num = "0";
      for (Map value : list) {
        String s_date = (String) value.get("s_date");
        String c_num = "" + value.get("c_num");
        if (nowstr.equals(s_date)) {
          today = c_num;
        }
        if (date.equals(s_date)) {
          temp_num = c_num;
          break;
        }
      }
      weekvalue.add(temp_num);
    }
    //result.put("today", today);
    //result.put("week", weekvalue);
    return weekvalue;
  }

  /**
   * 获取一年内的所有月份
   */
  private static ArrayList getYearDay() {
    ArrayList<String> list = new ArrayList<String>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
    String year = sdf.format(new Date());
    for (int i = 1; i < 13; i++) {
      if (i < 10) {
        list.add(year + "-0" + i);
      } else {
        list.add(year + "-" + i);
      }
    }
    return list;
  }

  /**
   * 将从数据库中取出的每月对应的各项值map数据结构做一次格式化 返回每月的值list(如果某天没有值，则为0)
   */
  private ArrayList formatYearList(ArrayList<Map> list) {
    String temp_num = "0";
    ArrayList yearvalue = new ArrayList();
    // 得到一年内每月的str字符串
    ArrayList<String> dates = getYearDay();
    for (String date : dates) {
      temp_num = "0";
      for (Map value : list) {
        String s_date = (String) value.get("s_date");
        String c_num = "" + value.get("c_num");
        if (date.equals(s_date)) {
          temp_num = c_num;
          break;
        }
      }
      yearvalue.add(temp_num);
    }
    return yearvalue;
  }

}
