package com.xquark.service.trade.impl;


import com.xquark.dal.mapper.TradeMapper;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.trade.TradeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;


@Service("tradeService")
public class TradeServiceImpl extends BaseServiceImpl implements TradeService {

  private final static Logger LOG = LoggerFactory.getLogger(TradeServiceImpl.class);
  @Autowired
  private TradeMapper tradeMapper;

  @Override
  public Map getSummary() {
    // 缓存总的各项值
    Map<String, Object> info = new HashMap<String, Object>();
    // 缓存一周的各项值
    Map<String, Object> nums = new HashMap<String, Object>();

    Map<String, Object> result = new HashMap<String, Object>();
    ArrayList dates = getWeekDay();

    BigDecimal totalIn = tradeMapper.getTotalIn();
    BigDecimal totalOut = tradeMapper.getTotalOut();
    BigDecimal totalWaiting = tradeMapper.getTotalWaiting();
    info.put("totalIn", totalIn);
    info.put("totalOut", totalOut);
    info.put("totalWaiting", totalWaiting);

    // 交易额
    ArrayList amountlist = (ArrayList) tradeMapper.getAmount(dates);
    nums = formatList(amountlist);
    result.put("info", info);
    result.put("nums", nums);
    return result;
  }

  /**
   * 将从数据库中取出的每天对应的各项值map数据结构做一次格式化 返回当天的值，如数据库没有则是0,同时返回周一到周日的值list(如果某天没有值，则为0)
   */
  private HashMap formatList(ArrayList<Map> list) {
    String temp_num = "0";
    ArrayList dateList = new ArrayList();
    ArrayList valueList = new ArrayList();
    HashMap result = new HashMap();
    // 得到一周内的每天的str字符串
    ArrayList<String> dates = getWeekDay();
    // 循环得到当天的值和周一到周天顺序每天的值
    for (String date : dates) {
      dateList.add(date);
      temp_num = "0";
      for (Map value : list) {
        String s_date = (String) value.get("s_date");
        String c_num = "" + value.get("c_num");
        if (date.equals(s_date)) {
          temp_num = c_num;
          break;
        }
      }
      valueList.add(temp_num);
    }
    result.put("dateList", dateList);
    result.put("valueList", valueList);
    return result;
  }

  /**
   * 获取过去第几天的日期
   */
  public static String getPastDate(int past) {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
    Date today = calendar.getTime();
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    String result = format.format(today);
    return result;
  }

  /**
   * 获取一周内的所有日期
   */
  private static ArrayList getWeekDay() {
    ArrayList<String> list = new ArrayList<String>();
    for (int i = 6; i >= 0; i--) {
      list.add(getPastDate(i));
    }
    return list;
  }


}
