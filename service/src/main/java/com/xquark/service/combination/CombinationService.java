package com.xquark.service.combination;

import com.xquark.dal.model.Combination;
import com.xquark.dal.type.CombinationStatus;
import com.xquark.dal.vo.CombinationVO;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

/**
 * User: byy Date: 18-5-31. Time: 下午8:25 组合服务层
 */
public interface CombinationService {

  /**
   * 添加
   */
  boolean insert(Combination obj);

  /**
   * 通过id查找
   */
  Combination getByPrimaryKey(String id);

  /**
   * 更新
   */
  boolean update(Combination obj);


  /**
   * 通过id删除
   */
  boolean delete(String id);

  /**
   * 批量删除
   */
  boolean batchDelete(List<String> ids);

  /**
   * 计算数据的数量
   */
  Long count(Map<String, Object> params);


  /**
   * 分页查询
   */
  List<Combination> list(Pageable page, Map<String, Object> params);

  /**
   * 根据id，关键字，分页查找商品信息
   * @param id
   * @return
   */
  CombinationVO getVOByPrimaryId(String id,Pageable page,Map<String, Object> params);

  /**
   * 根据组合id改变组合状态
   * @param status
   * @param id
   * @return
   */
  boolean updateStatus(CombinationStatus status,String id);
}