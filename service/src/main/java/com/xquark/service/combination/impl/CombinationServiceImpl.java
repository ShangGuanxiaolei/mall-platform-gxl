package com.xquark.service.combination.impl;

import com.google.common.base.Preconditions;
import com.xquark.dal.mapper.CombinationMapper;
import com.xquark.dal.model.Combination;
import com.xquark.dal.type.CombinationStatus;
import com.xquark.dal.vo.CombinationVO;
import com.xquark.service.combination.CombinationService;
import com.xquark.service.common.BaseServiceImpl;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: byy Date: 18-5-31. Time: 下午8:33 组合服务层实体类
 */
@Service
@Transactional
public class CombinationServiceImpl extends BaseServiceImpl implements CombinationService {

  @Autowired
  private CombinationMapper baseMapper;

  /**
   * 根据id查询
   */
  @Override
  public Combination getByPrimaryKey(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    Combination combination=baseMapper.getByPrimaryKey(id);
    return combination;
  }

  /**
   * 插入数据
   */
  @Override
  public boolean insert(Combination obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.insert(obj) > 0;
  }

  /**
   * 修改数据
   */
  @Override
  public boolean update(Combination obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.update(obj) > 0;
  }

  @Override
  public boolean delete(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.delete(id) > 0;
  }

  /**
   * 批量删除
   *
   * @param ids id集合
   */
  @Override
  public boolean batchDelete(List<String> ids) {
    Preconditions.checkNotNull(ids, "param cant be null");
    final boolean ret = baseMapper.batchDelete(ids) > 0;
    return ret;
  }


  /**
   * 分页查询
   */
  @Override
  public List<Combination> list(Pageable page, Map<String, Object> params) {
    return baseMapper.list(page, params);
  }

  /**
   * 根据id，关键字，分页查找商品信息
   * @param id
   * @return
   */
  @Override
  public CombinationVO getVOByPrimaryId(String id, Pageable page, Map<String, Object> params) {
    CombinationVO combinationVO=baseMapper.getVOByPrimaryId(id,page,params);
    return combinationVO;

  }

  /**
   * 根据组合id改变组合状态
   * @param status
   * @param id
   * @return
   */
  @Override
  public boolean updateStatus(CombinationStatus status, String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    Combination combination=baseMapper.getByPrimaryKey(id);
    Preconditions.checkNotNull(combination);
    combination=new Combination();
    combination.setStatus(status);
    return baseMapper.update(combination) > 0;
  }


  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long count(Map<String, Object> params) {
    return baseMapper.count(params);
  }



}
