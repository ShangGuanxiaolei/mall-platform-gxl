package com.xquark.service.outpay.impl;

import com.xquark.dal.mapper.PaymentMerchantMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.PaymentMerchant;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderService;
import com.xquark.service.outpay.*;
import com.xquark.service.outpay.impl.tenpay.CommonUtil;
import com.xquark.service.outpay.impl.tenpay.MD5SignUtil;
import com.xquark.service.outpay.impl.tenpay.SDKRuntimeException;
import com.xquark.service.outpay.impl.tenpay.XMLUtil;
import com.xquark.service.pay.model.PaymentResponseVO;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component("weiXinPayment")
public class WeiXinPaymentImpl extends AbstractThirdPartyService {

  private Logger tenpayLogger = OutPayLogs.tenpayLogger;

//	@Value("${site.web.host.name}")
//	String siteHost;

  @Autowired
  private PaymentMerchantMapper paymentMerchantMapper;

//	@Value("${payment.merchant.appid.tenpay}")
//	String appId;
//
//	@Value("${payment.merchant.appsecret.tenpay}")
//	String appSecret;
//	
//	@Value("${payment.merchant.id.tenpay}")
//	String partner;
//	
//	@Value("${payment.merchant.secret.tenpay}")
//	String partnerKey;

  @Autowired
  private CashierService cashierService;

  @Autowired
  private OrderService orderService;

  public static final String notify_url = "/pay/tenpay/notify";

  public static final String input_charset = "UTF-8";

  @Value("${profiles.active}")
  String profile;

  @Override
  public PaymentResponseVO payRequest(HttpServletRequest req,
                                      HttpServletResponse resp, PaymentRequestVO request) {
    return null;
  }

  @Override
  public PaymentResponseVO payCallback(HttpServletRequest req,
      HttpServletResponse resp) {
    return null;
  }

  @Override
  public PaymentResponseVO payNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<String> loadBankList() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payRefund(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payRefund(PaymentRequestVO request, Order order) throws Exception {
    return null;
  }

  @Override
  public PaymentResponseVO payCancel(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payRefundNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payConfirm(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void payBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> maps)
      throws Exception {
    // TODO Auto-generated method stub

  }

  @Override
  public Map<String, String> payBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void refundBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> maps)
      throws Exception {
    // TODO Auto-generated method stub

  }

  @Override
  public Map<String, String> refundBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean sendSmsAgain(PaymentRequestVO request) throws Exception {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void closeTrade(String orderNo) {
    // TODO Auto-generated method stub

  }

  @Override
  public void tradeAccountReport(String startTime, String endTime) {
    // TODO Auto-generated method stub

  }

  @Override
  @SuppressWarnings("rawtypes")
  public PaymentResponseClientVO sign(PaymentRequestVO pay) {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "APP", PaymentMode.WEIXIN);

    RestTemplate restTemplate = new RestTemplate();
//		String appId = "wxe4564ca5ef3c23ef";
//		String mch_id = "1248744501";
//		String key = "jfoi34lkjsfsdf10dsflklaidoweqlsk";
    String url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
    String prepayid = "";
    String noncestr = CommonUtil.CreateNoncestr();
    String timestamp = Long.toString(Calendar.getInstance().getTimeInMillis() / 1000);

    Map<String, String> paramsMap = new HashMap<String, String>();
    paramsMap.put("appid", paymentMerchant.getAppId());
    paramsMap.put("mch_id", paymentMerchant.getMerchantId());
    paramsMap.put("nonce_str", noncestr);
    paramsMap.put("body", pay.getTradeNo());
    paramsMap.put("out_trade_no", pay.getTradeNo());
//		paramsMap.put("total_fee", pay.getTotalFee().multiply(new BigDecimal(100))+"");
    paramsMap.put("total_fee", pay.getTotalFee().multiply(new BigDecimal(100))
        .intValue() + ""); // 以分为单位
    paramsMap.put("spbill_create_ip", "127.0.0.1");// req.getRemoteHost());
    Calendar cal = Calendar.getInstance();
    paramsMap.put("time_start", DateFormatUtils.format(cal, "yyyyMMddHHmmss"));
    cal.add(Calendar.DAY_OF_MONTH, 3);
    paramsMap.put("time_expire", DateFormatUtils.format(cal, "yyyyMMddHHmmss"));
    paramsMap.put("notify_url", paymentMerchant.getNotifySite() + notify_url);
    paramsMap.put("trade_type", "APP");

    tenpayLogger.info("tenpay to sign payNo=[" + pay.getTradeNo() + "]" + paramsMap);

    String sign = null;
    try {
      sign = MD5SignUtil
          .Sign(CommonUtil.FormatQueryParaMap(paramsMap, false), paymentMerchant.getSecretKey());
      paramsMap.put("sign", sign);
    } catch (SDKRuntimeException e) {
      tenpayLogger.error("签名出错", e, pay);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }
    String respStr = restTemplate.postForObject(url, XMLUtil.toXml(paramsMap), String.class);

    Map map = null;
    try {
      map = XMLUtil.doXMLParse(respStr);
      if (map != null) {
        if ("SUCCESS".equals(map.get("result_code"))) {
          prepayid = (String) map.get("prepay_id");
        } else {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, (String) map.get("return_msg"));
        }
      }
    } catch (Exception e) {
      tenpayLogger.error("签名出错", e, pay);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }
    Map<String, String> signInfo = appSignature(noncestr, prepayid, timestamp);
    PaymentResponseClientVO response = new PaymentResponseClientVO(pay.getTradeNo(), prepayid,
        pay.getTotalFee(),
        pay.getOrderIds(), PaymentMode.WEIXIN);
    response.setPayInfo(signInfo);
    response.setSignInfo(signInfo.get("sign"));
    response.setSigned(genClientSigned(response.getSignInfo()));

    tenpayLogger.info("tenpay signed payNo=[" + pay.getTradeNo() + "]" + response);
    tenpayLogger.info("paymentMerchant payNo=[" + pay.getTradeNo() + "]" + paymentMerchant);

    return response;
  }

  private Map<String, String> appSignature(String noncestr, String prepayId, String timestamp) {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "APP", PaymentMode.WEIXIN);
    Map<String, String> paramsMap = new HashMap<String, String>();
    paramsMap.put("appid", paymentMerchant.getAppId());
    paramsMap.put("noncestr", noncestr);
    paramsMap.put("package", "Sign=WXPay");
    paramsMap.put("partnerid", paymentMerchant.getMerchantId());
    paramsMap.put("prepayid", prepayId);
    paramsMap.put("timestamp", timestamp);
    try {
      String sign = MD5SignUtil
          .Sign(CommonUtil.FormatQueryParaMap(paramsMap, false), paymentMerchant.getSecretKey());
      paramsMap.put("sign", sign);
    } catch (SDKRuntimeException e) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, e);
    }

    Map<String, String> params = new HashMap<String, String>();
    params.put("appid", paramsMap.get("appid"));
    params.put("noncestr", paramsMap.get("noncestr"));
    params.put("packageValue", paramsMap.get("package"));
    params.put("partnerId", paymentMerchant.getMerchantId());
    params.put("prepayId", prepayId);
    params.put("timeStamp", timestamp);
    params.put("sign", paramsMap.get("sign"));

    return params;
  }

  @Override
  public PaymentResponseVO payNotifyApp(HttpServletRequest req,
      HttpServletResponse resp) {
    return null;
  }

  @Override
  public void payLock(String extCouponId) {
  }

  @Override
  public void payUnLock(String extCouponId) {
  }
}
