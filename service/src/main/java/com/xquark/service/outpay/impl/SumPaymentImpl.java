package com.xquark.service.outpay.impl;

import com.xquark.dal.model.Order;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.outpay.AbstractThirdPartyService;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.outpay.PaymentResponseClientVO;
import com.xquark.service.outpay.ReceiverDetailVO;
import com.xquark.service.pay.model.PaymentResponseVO;
import com.xquark.thirds.sumpay.SumPayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.math.RoundingMode;
import java.util.*;

@Component("sumPayment")
public class SumPaymentImpl extends AbstractThirdPartyService {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Value("${site.web.host.name}")
  String siteHost;

  @Value("${payment.merchant.gateway.sumpay}")
  String SUM_GATEWAY_NEW;

  @Value("${payment.merchant.appId.sumpay}")
  String appId;

  @Value("${payment.merchant.id.sumpay}")
  String merId;

  /**
   * 异步通知
   */
  public static final String notify_url = "/pay/sumpay/notify";

  /**
   * 页面跳转同步通知页面路径
   */
  public static final String call_back_url = "/pay/sumpay/call_back";

  @Override
  public PaymentResponseVO payRequest(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request) {
    SortedMap<String, String> paraMap = new TreeMap<String, String>();
    buildTradePara(paraMap, request);
    String html = new SumPayUtils().buildRqeuest(paraMap, SUM_GATEWAY_NEW);
    log.debug(html);
    PrintWriter writer = null;
    try {
      writer = resp.getWriter();
      writer.print(html);
      writer.flush();
    } catch (Exception e) {
    } finally {
      if (writer != null) {
        writer.close();
      }
    }
    return null;
  }

  private void buildTradePara(SortedMap<String, String> paraMap, PaymentRequestVO request) {
    paraMap.put("appId", appId);//由平台统一分配合作商户唯一标识
    paraMap.put("merId", merId);//商户代码	必填

    paraMap.put("recCstno", "");//收方客户编号
    paraMap.put("cstNo", getCurrentUser().getId());//付方客户编号	必填
    paraMap.put("orderNo", request.getTradeNo());//订单号	必填
    paraMap.put("orderTime", "");//订单时间	必填
    paraMap.put("curType", "CNY");//支付币种	必填
    paraMap
        .put("orderAmt", request.getTotalFee().setScale(2, RoundingMode.DOWN).toString());//订单金额	必填
    paraMap.put("notifyUrl", siteHost + notify_url);//商户后台通知URL	必填
    paraMap.put("returnUrl", siteHost + call_back_url);//商户前台通知跳转URL
    paraMap.put("goodsName", request.getSubject());//商品名称：订单对应商品的详细信息	必填
    paraMap.put("goodsNum", "12");//商品数量：订单对应商品数量（备注描述）	必填
    paraMap.put("goodsType", "1");//商品类型：1-实物商品；2-虚拟商品	必填
    paraMap.put("logistics", "1");//是否物流：1-是；0-否	必填
    paraMap.put("address", "");//收货地址
    paraMap.put("remark", "");//备注字段
  }

  @Override
  public PaymentResponseVO payCallback(HttpServletRequest req,
      HttpServletResponse resp) {
    SortedMap<String, String> params = extractParams(req);
    try {
      if (!SumPayUtils.verify(params, params.get("sign"), "UTF-8")) {
        log.info("SumpayNotify.verifyNotify failed.");
        return null;
      }
    } catch (Exception e) {
      log.warn("payment verify failed.", e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "验证签名数据异常");
    }

    //TODO check p

    PaymentResponseVO p = new PaymentResponseVO();
    p.setPaymentMode(PaymentMode.SUMPAY);
    // 商户订单号
    p.setBillNo(params.get("order_no"));
    // 统统付交易号
    p.setBillNoPay(params.get("serial_no"));
    // 交易状态
    if (params.get("resp_code").equals("000000")) {
      p.setBillStatus(PaymentStatus.SUCCESS);
    } else {
      p.setBillStatus(PaymentStatus.PENDING);
    }
    return p;
  }

  @Override
  public PaymentResponseVO payNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    SortedMap<String, String> params = extractParams(req);
    try {
      if (!SumPayUtils.verify(params, params.get("sign"), "UTF-8")) {
        return null;
      }
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "验证签名数据异常");
    }

    //TODO check p

    PaymentResponseVO p = new PaymentResponseVO();
    p.setPaymentMode(PaymentMode.SUMPAY);
    // 商户订单号
    p.setBillNo(params.get("order_no"));
    // 统统付交易号
    p.setBillNoPay(params.get("serial_no"));
    // 交易状态
    if (params.get("resp_code").equals("000000")) {
      p.setBillStatus(PaymentStatus.SUCCESS);
    } else {
      p.setBillStatus(PaymentStatus.PENDING);
    }
    return p;
  }

  @SuppressWarnings("rawtypes")
  private SortedMap<String, String> extractParams(HttpServletRequest req) {
    SortedMap<String, String> params = new TreeMap<String, String>();
    Map requestParams = req.getParameterMap();
    for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
      String name = (String) iter.next();
      String[] values = (String[]) requestParams.get(name);
      String valueStr = "";
      for (int i = 0; i < values.length; i++) {
        valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
      }
      // 乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
      // valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
      params.put(name, valueStr);
    }
    return params;
  }

  @Override
  public List<String> loadBankList() {
    return null;
  }

  @Override
  public PaymentResponseVO payRefund(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    return null;
  }

  @Override
  public PaymentResponseVO payRefund(PaymentRequestVO request, Order order) throws Exception {
    return null;
  }

  @Override
  public PaymentResponseVO payCancel(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    return null;
  }

  @Override
  public PaymentResponseVO payRefundNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    return null;
  }

  @Override
  public PaymentResponseVO payConfirm(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    return null;
  }

  @Override
  public void payBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> maps)
      throws Exception {

  }

  @Override
  public Map<String, String> payBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    return null;
  }

  @Override
  public void refundBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> maps)
      throws Exception {

  }

  @Override
  public Map<String, String> refundBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    return null;
  }

  @Override
  public boolean sendSmsAgain(PaymentRequestVO request) throws Exception {
    return false;
  }

  @Override
  public void closeTrade(String orderNo) {

  }

  @Override
  public void tradeAccountReport(String startTime, String endTime) {

  }

  @Override
  public void payLock(String couponId) {

  }

  @Override
  public void payUnLock(String couponId) {

  }

  @Override
  public PaymentResponseClientVO sign(PaymentRequestVO pay) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payNotifyApp(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }
}
