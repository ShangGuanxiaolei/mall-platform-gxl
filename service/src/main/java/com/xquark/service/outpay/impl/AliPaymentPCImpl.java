package com.xquark.service.outpay.impl;

import com.xquark.dal.mapper.PaymentMerchantMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.PaymentMerchant;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.order.OrderService;
import com.xquark.service.outpay.AbstractThirdPartyService;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.outpay.PaymentResponseClientVO;
import com.xquark.service.outpay.ReceiverDetailVO;
import com.xquark.service.pay.model.PaymentResponseVO;
import com.xquark.thirds.alipay.pc.util.AlipayNotify;
import com.xquark.thirds.alipay.pc.util.AlipaySubmit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Component("aliPaymentPc")
public class AliPaymentPCImpl extends AbstractThirdPartyService {

//	@Value("${site.web.host.name}")
//	String siteHost;

  @Autowired
  private OrderService orderService;

  @Autowired
  private PaymentMerchantMapper paymentMerchantMapper;

//	//服务器异步通知页面路径
//	String notify_url = "http://115.236.11.98:8888/create_direct_pay_by_user-JAVA-UTF-8/notify_url.jsp";
//	//需http://格式的完整路径，不能加?id=123这类自定义参数
//
//	//页面跳转同步通知页面路径
//	String return_url = "http://115.236.11.98:8888/create_direct_pay_by_user-JAVA-UTF-8/return_url.jsp";
//	//需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/


  /**
   * 服务器异步通知页面路径
   */
  public static final String notify_url = "/pay/alipayPc/notify";

  /**
   * 页面跳转同步通知页面路径
   */
  public static final String call_back_url = "/pay/alipayPc/call_back";
  // 需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/

  /**
   * 操作中断返回地址 ，用户付款中途退出返回商户的地址。需http://格式的完整路径，不允许加?id=123这类自定义参数 TODO
   */
  public static final String merchant_url = "/pay/alipay/undone";

  String payment_type = "1";

//	@Value("${payment.merchant.id.alipay}")
//	String partner;
//	@Value("${payment.merchant.mail.alipay}")
//	String partnerMail;
//	@Value("${payment.merchant.userName.alipay}")
//	String partnerUserName;
//	
//	// 签名方式，选择项：0001(RSA)、MD5
//	@Value("${payment.merchant.sign_type.alipay}")
//	String sign_type;
//	@Value("${payment.merchant.pub_key.alipay}")
//	String pub_key;
//	@Value("${payment.merchant.secret.alipay}")
//	String secret;

  @Value("${profiles.active}")
  String profile;

  // 支付宝网关地址
////	private static final String ALIPAY_GATEWAY_NEW = "http://wappaygw.alipay.com/service/rest.htm?";
//	private static final String ALIPAY_GATEWAY_NEW = " https://mapi.alipay.com/gateway.do?";
//	
//	// 返回格式
//	private static final String format = "xml";
//	// 返回格式
//	private final static String v = "2.0";

  private final static String charset = "UTF-8";

  @Autowired
  private CashierService cashierService;

  @Override
  public PaymentResponseVO payRequest(HttpServletRequest req, HttpServletResponse resp,
      PaymentRequestVO request) {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);

    //防钓鱼时间戳
    String anti_phishing_key = "";
    //若要使用请调用类文件submit中的query_timestamp函数

    //客户端的IP地址
    String exter_invoke_ip = "";
    //非局域网的外网IP地址，如：221.0.0.1

    //把请求参数打包成数组
    Map<String, String> sParaTemp = new HashMap<String, String>();
    sParaTemp.put("service", "create_direct_pay_by_user");
    sParaTemp.put("partner", paymentMerchant.getMerchantId());
    sParaTemp.put("_input_charset", charset);
    sParaTemp.put("payment_type", payment_type);
    sParaTemp.put("notify_url", paymentMerchant.getNotifySite() + notify_url);
    sParaTemp.put("return_url", paymentMerchant.getCallbackSite() + call_back_url);
    sParaTemp.put("seller_email", paymentMerchant.getMerchantAccount());
    sParaTemp.put("out_trade_no", request.getTradeNo());
    sParaTemp.put("subject", request.getSubject());
    sParaTemp.put("total_fee", request.getTotalFee().toString());
    sParaTemp.put("body", request.getTradeNo());
    sParaTemp.put("show_url", "");
    sParaTemp.put("anti_phishing_key", anti_phishing_key);
    sParaTemp.put("exter_invoke_ip", exter_invoke_ip);

    //建立请求
    String sHtmlText = AlipaySubmit
        .buildRequest(sParaTemp, "get", "确认", paymentMerchant.getSignType(),
            paymentMerchant.getSecretKey());

    // 直接返回页面内容
    resp.setContentType("text/html");
    resp.setHeader("charset", "uft-8");
//		String html = AlipaySubmit.buildRequest(sParaTemp, "get", "确认", sign_type, secret);
    responseBody(sHtmlText, resp);
    return null;
  }

  @Override
  public PaymentResponseVO payCallback(HttpServletRequest req, HttpServletResponse resp) {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);

    //获取支付宝GET过来反馈信息
    Map<String, String> params = new HashMap<String, String>();
    Map<String, String[]> requestParams = req.getParameterMap();
    for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
      String name = iter.next();
      String[] values = (String[]) requestParams.get(name);
      String valueStr = "";
      for (int i = 0; i < values.length; i++) {
        valueStr = (i == values.length - 1) ? valueStr + values[i]
            : valueStr + values[i] + ",";
      }
      //乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
//			valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
      params.put(name, valueStr);
    }

    //获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
    //商户订单号

//		String out_trade_no = new String(req.getParameter("out_trade_no"));
//
//		//支付宝交易号
//
//		String trade_no = new String(req.getParameter("trade_no"));

    //交易状态
    String trade_status = new String(req.getParameter("trade_status"));

    //获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//

    //计算得出通知验证结果
    boolean verify_result = AlipayNotify
        .verify(params, paymentMerchant.getMerchantId(), paymentMerchant.getSignType(),
            paymentMerchant.getPubKey());

    if (verify_result) {//验证成功
      //////////////////////////////////////////////////////////////////////////////////////////
      //请在这里加上商户的业务逻辑程序代码

      //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
      if (trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS")) {
        //判断该笔订单是否在商户网站中已经做过处理
        //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
        //如果有做过处理，不执行商户的业务程序
      }

      //该页面可做页面美工编辑
//			out.println("验证成功<br />");
      //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
      PaymentResponseVO p = new PaymentResponseVO();
      p.setPaymentMode(PaymentMode.ALIPAY);
      p.setBillStatus(PaymentStatus.SUCCESS);
      // 商户订单号
      p.setBillNo(req.getParameter("out_trade_no"));
      // 支付宝交易号
      p.setBillNoPay(req.getParameter("trade_no"));
      // 交易状态
      p.setBillStatus(PaymentStatus.SUCCESS);

      super.savePayResult(p);

      return p;
      //////////////////////////////////////////////////////////////////////////////////////////
    } else {
      //该页面可做页面美工编辑
//			 out.println("验证失败");
      return null;
    }
  }

  @Override
  public PaymentResponseVO payNotify(HttpServletRequest req, HttpServletResponse resp) {
    PaymentResponseVO p = null;

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);

    //获取支付宝POST过来反馈信息
    Map<String, String> params = new HashMap<String, String>();
    Map<String, String[]> requestParams = req.getParameterMap();
    for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
      String name = (String) iter.next();
      String[] values = (String[]) requestParams.get(name);
      String valueStr = "";
      for (int i = 0; i < values.length; i++) {
        valueStr = (i == values.length - 1) ? valueStr + values[i]
            : valueStr + values[i] + ",";
      }
      //乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
      //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
      params.put(name, valueStr);
    }

    //获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//
    if (AlipayNotify.verify(params, paymentMerchant.getMerchantId(), paymentMerchant.getSignType(),
        paymentMerchant.getPubKey())) {//验证成功

      p = new PaymentResponseVO();
      p.setPaymentMode(PaymentMode.ALIPAY);
      // 商户订单号
      p.setBillNo(req.getParameter("out_trade_no"));
      // 支付宝交易号
      p.setBillNoPay(req.getParameter("trade_no"));
      // 交易状态
      PaymentStatus status;
      String s = req.getParameter("trade_status");

      if ("TRADE_SUCCESS".equalsIgnoreCase(s)) {
        status = PaymentStatus.SUCCESS;
      } else if ("TRADE_FINISHED".equalsIgnoreCase(s)) {
        status = PaymentStatus.SUCCESS;
      } else if ("TRADE_CLOSED".equalsIgnoreCase(s)) {
        status = PaymentStatus.CLOSED;
      } else {
        status = PaymentStatus.PENDING;
      }
      p.setBillStatus(status);
//			p.setBuyerEmail(doc_notify_data.selectSingleNode("//notify/buyer_email").getText());
//			p.setBuyerId(doc_notify_data.selectSingleNode("//notify/buyer_id").getText());
//			p.setTotalFee(doc_notify_data.selectSingleNode("//notify/total_fee").getText());
      p.setTotalFee(req.getParameter("total_fee"));
      p.setBuyerEmail(req.getParameter("buyer_email"));
      p.setBuyerId(req.getParameter("buyer_id"));
      p.setDetail(params.toString());
//			p.setBillStatus(PaymentStatus.SUCCESS);

      cashierService.handlePaymentResponse(p.getBillNo(), PaymentMode.ALIPAY, p);

      //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

//			out.println("success");	//请不要修改或删除

      //////////////////////////////////////////////////////////////////////////////////////////
    }
    return p;
  }

//	@SuppressWarnings("rawtypes")
//	private Map<String, String> extractParams(HttpServletRequest req) {
//		Map<String, String> params = new HashMap<String, String>();
//		Map requestParams = req.getParameterMap();
//		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
//			String name = (String) iter.next();
//			String[] values = (String[]) requestParams.get(name);
//			String valueStr = "";
//			for (int i = 0; i < values.length; i++) {
//				valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
//			}
//			// 乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
//			// valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
//			params.put(name, valueStr);
//		}
//		return params;
//	}

  @Override
  public List<String> loadBankList() {
    return new ArrayList<String>();
  }

  @Override
  public PaymentResponseVO payRefund(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    return null;
  }

  @Override
  public PaymentResponseVO payRefund(PaymentRequestVO request, Order order) throws Exception {
    return null;
  }

  @Override
  public PaymentResponseVO payCancel(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    return null;
  }

  @Override
  public PaymentResponseVO payRefundNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    return null;
  }

  @Override
  public PaymentResponseVO payConfirm(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    return null;
  }

  @Override
  public boolean sendSmsAgain(PaymentRequestVO request) throws Exception {
    return false;
  }

  @Override
  public void payBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> maps)
      throws Exception {

  }

  @Override
  public Map<String, String> payBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    return null;
  }

  @Override
  public void refundBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> maps)
      throws Exception {

  }

  @Override
  public Map<String, String> refundBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    return null;
  }

  @Override
  public void closeTrade(String orderNo) {

  }

  @Override
  public void tradeAccountReport(String startTime, String endTime) {

  }

  @Override
  public void payLock(String couponId) {

  }

  @Override
  public void payUnLock(String couponId) {

  }

  @Override
  public PaymentResponseClientVO sign(PaymentRequestVO pay) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payNotifyApp(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }
}
