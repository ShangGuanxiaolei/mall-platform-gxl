package com.xquark.service.outpay.impl.eps;

import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.outpay.impl.ThirdPaymentQueryRes;
import org.apache.commons.lang3.StringUtils;

/**
 * @author wangxinhua
 * @date 2019-05-04
 * @since 1.0
 */
public class EpsPaymentQueryRes extends ThirdPaymentQueryRes {

    public EpsPaymentQueryRes(String billNo, String status, String billNoPay,
                              PaymentMode paymentMode, String buyerEmail,
                              String buyerId, String totalFee,
                              String detail,
                              String paymentMerchantId, String code) {
        super(billNo, status, billNoPay, paymentMode, buyerEmail, buyerId, totalFee, detail, paymentMerchantId, code);
    }

    @Override
    public boolean isSuccess() {
        return StringUtils.equals(getCode(), "0");
    }

    @Override
    public boolean isPaid() {
        return getBillStatus().equals(PaymentStatus.SUCCESS);
    }

    @Override
    public PaymentStatus getBillStatus() {
        if (StringUtils.equals(getStatus(), "SUCCESS")) {
            return PaymentStatus.SUCCESS;
        }
        return PaymentStatus.WAITING;
    }
}
