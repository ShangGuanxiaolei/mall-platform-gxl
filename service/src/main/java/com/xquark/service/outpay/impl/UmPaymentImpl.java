package com.xquark.service.outpay.impl;

import com.xquark.dal.mapper.PaymentMerchantMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OutpayAgreement;
import com.xquark.dal.model.PaymentMerchant;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.BankCardType;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.outpay.*;
import com.xquark.service.pay.model.PaymentResponseVO;
import com.xquark.thirds.umpay.api.common.ReqData;
import com.xquark.thirds.umpay.api.paygate.v40.Mer2Plat_v40;
import com.xquark.thirds.umpay.api.paygate.v40.Plat2Mer_v40;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;
import com.xquark.utils.unionpay.AcpService;
import com.xquark.utils.unionpay.LogUtil;
import com.xquark.utils.unionpay.PayUtil;
import com.xquark.utils.unionpay.SDKConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * U付实现类 com.xquark.thirds.umpay修改源码记录 1.增加UmConfig类，获取配置文件的配置信息 2.重新实现ProFileUitl里的所有方法
 *
 * @author huxaya
 */

@Component("umPayment")
public class UmPaymentImpl extends AbstractThirdPartyService {

  private Logger umPayLogger = OutPayLogs.umPayLogger;

  @Autowired
  private OutPayAgreementService outPayAgreementService;

//	@Value("${site.web.host.name}")
//	String siteHost;

  @Autowired
  private PaymentMerchantMapper paymentMerchantMapper;

  // 异步通知
  public static final String notify_url = "/pay/umpay/notify";

  // 页面跳转同步通知页面路径
  public static final String call_back_url = "/pay/umpay/call_back";

  // 退款异步通知
  public static final String refund_notify_url = "/pay/umpay/refund_notify";

  // 操作中断返回地址 ，用户付款中途退出返回商户的地址。需http://格式的完整路径，不允许加?id=123这类自定义参数 TODO
  // public static final String merchant_url = "/pay/alipay/undone";

//	// 商户号
//	@Value("${payment.merchant.id.umpay}")
//	String partner;

  // 返回成功码
  private final static String successCode = "0000";

  // 返回格式
  private final static String v = "4.0";

  private final static String charset = "UTF-8";

//	private final static String signType = "RSA";

  @Autowired
  private RestOperations restTemplate;

  @Autowired
  private CashierService cashierService;

  @Value("${profiles.active}")
  String profile;

  @Override
  public PaymentResponseVO payRequest(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request) {
    PaymentResponseVO p = new PaymentResponseVO();

//		PaymentMerchant paymentMerchant = paymentMerchantMapper.selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);
    // 得到u付的交易号
//    String um_trade_no = loadUmTradeNo(request);
//
//    if (StringUtils.isNotBlank(request.getPayAgreementId())) {
//      log.info("umpay pay agree agreeId=[" + request.getPayAgreementId() + "]");
//      request.setOutTradeNo(um_trade_no);
//      sendSms(request); // 如果是协议支付，则获取短信验证码
//    } else {
//      firstPay(um_trade_no, resp, request); // 如果是首次支付，须跳转到U付页面
//    }
//
//    p.setBillNoPay(um_trade_no);
//    p.setBillNo(request.getTradeNo());
//    p.setPaymentMode(PaymentMode.UMPAY);
//    p.setBillStatus(PaymentStatus.PENDING);
//    p.setTotalFee(request.getTotalFee().toString());
//    p.setPayAgreementId(request.getPayAgreementId());
//
//    super.savePayResult(p);

    String um_trade_no = getUmpayTradeNo(request);

    p.setBillNoPay(um_trade_no);
    p.setBillNo(request.getTradeNo());
    p.setPaymentMode(PaymentMode.UMPAY);
    p.setBillStatus(PaymentStatus.SUCCESS);
    p.setTotalFee(request.getTotalFee().toString());

    super.savePayResult(p);

    return p;
  }

  @SuppressWarnings("unchecked")
  @Override
  public PaymentResponseVO payRefund(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.UMPAY);

    PaymentResponseVO p = null;
    Map<String, String> ht = new HashMap<String, String>();
    ht.put("service", "mer_refund");
    ht.put("sign_type", paymentMerchant.getSignType());
    ht.put("charset", charset);
    ht.put("notify_url", refund_notify_url);
    ht.put("mer_id", paymentMerchant.getMerchantId());
    ht.put("refund_no", UniqueNoUtils.next(UniqueNoType.ESO));
    ht.put("order_id", request.getTradeNo());
    ht.put("mer_date", DateFormatUtils.format(Calendar.getInstance(), "yyyyMMdd"));
    ht.put("refund_amount", request.getTotalFee().toString());
    ht.put("org_amount", request.getTotalFee().toString());
    ht.put("version", v);

    Map<String, String> data = null;
    try {
      ReqData reqData = Mer2Plat_v40.ReqDataByGet(ht);
      String html = HttpRequestUtil.httpRequest(reqData.getUrl());
      data = Plat2Mer_v40.getResData(html);
    } catch (Exception e) {
      log.error("U付退款时出错", e, request);
    } finally {
      if (data == null || !successCode.equals(data.get("ret_code"))) {
        log.error("U付交易取消时出错", data, request);
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            "U付退款时出错 payNo=[" + request.getTradeNo() + "]");
      }
    }

    return p;
  }

  @Override
  public PaymentResponseVO payRefund(PaymentRequestVO request, Order order) throws Exception {
    return null;
  }

  @SuppressWarnings("unchecked")
  @Override
  public PaymentResponseVO payCancel(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request) {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.UMPAY);
    PaymentResponseVO p = null;
    Map<String, String> ht = new HashMap<String, String>();
    ht.put("mer_id", paymentMerchant.getMerchantId());
    ht.put("order_id", request.getTradeNo());
    ht.put("mer_date",
        DateFormatUtils.format(Calendar.getInstance(), "yyyyMMdd"));
    ht.put("amount", request.getTotalFee().toString());
    ht.put("version", v);
    ht.put("service", "mer_cancel");
    ht.put("sign_type", paymentMerchant.getSignType());
    ht.put("charset", charset);

    Map<String, String> data = null;
    try {
      ReqData reqData = Mer2Plat_v40.ReqDataByGet(ht);
      String url = reqData.getUrl();
      String html = HttpRequestUtil.httpRequest(url);
      data = Plat2Mer_v40.getResData(html);
    } catch (Exception e) {
      log.error("U付交易取消时出错", e, request);
    } finally {
      if (data == null || !successCode.equals(data.get("ret_code"))) {
        log.error("U付交易取消时出错", data, request);
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            "U付交易取消时出错 payNo=[" + request.getTradeNo() + "]");
      }
    }

    return p;
  }

  private void checkData(Map<String, String> data, String rec_code, PaymentRequestVO request,
      String defaultErrorMsg) {
    if (data == null || !successCode.equals(rec_code)) {
      log.error(defaultErrorMsg, data, request);
      String error = data != null ? data.get("ret_msg") : defaultErrorMsg;
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, error);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public PaymentResponseVO payConfirm(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.UMPAY);

    OutpayAgreement agreement = outPayAgreementService.load(request
        .getPayAgreementId());

    Map<String, String> ht = new HashMap<String, String>();
    ht.put("service", "agreement_pay_confirm_shortcut");
    ht.put("sign_type", paymentMerchant.getSignType());
    ht.put("charset", charset);
    ht.put("version", v);
    ht.put("mer_id", paymentMerchant.getMerchantId());
    ht.put("trade_no", request.getOutTradeNo());
    ht.put("mer_cust_id", request.getUserId());
    ht.put("verify_code", request.getVerifyCode());
    // 使用协议进行支付，上送如下参数：支付协议号必填、商户用户标识和用户业务协议号必填其一
    ht.put("usr_busi_agreement_id", agreement.getBizAgreeId());
    ht.put("usr_pay_agreement_id", agreement.getPayAgreeId());
    // 在协议支付中，商户可选传如下参数，作为验证用户支付要素的凭证
    // ht.put("valid_date", request.getVerifyCode());

    Map<String, String> data = null;
    try {
      ReqData reqData = Mer2Plat_v40.ReqDataByGet(ht);
      String html = HttpRequestUtil.httpRequest(reqData.getUrl());
      data = Plat2Mer_v40.getResData(html);
    } catch (Exception e) {
      log.error("U付短信确认时出错", e, request);
    } finally {
      checkData(data, data.get("ret_code"), request,
          "U付短信确认出错payNo=[" + request.getTradeNo() + "]");
    }

    PaymentResponseVO p = new PaymentResponseVO();
    p.setBillStatus(PaymentStatus.SUCCESS);
    p.setPaymentMode(PaymentMode.UMPAY);
    // 商户订单号
    p.setBillNo(request.getTradeNo());
    // U付交易号
    p.setBillNoPay(request.getOutTradeNo());
    // 交易状态
    p.setBillStatus(PaymentStatus.SUCCESS);
    p.setTotalFee(request.getTotalFee().toString());
    p.setPaymentMerchantId(paymentMerchant.getId());
    super.savePayResult(p);
    return p;
  }

  @Override
  public boolean sendSmsAgain(PaymentRequestVO request) throws Exception {
    return sendSms(request);
  }

  @SuppressWarnings("unchecked")
  private boolean sendSms(PaymentRequestVO request) {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.UMPAY);

    OutpayAgreement agreement = outPayAgreementService.load(request
        .getPayAgreementId());
    if (agreement == null
        || !agreement.getUserId().equals(request.getUserId())) {
      log.error("支付协议号与用户不一致 " + request);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "支付协议号与用户不一致 " + request);
    }

    // 使用协议进行支付，上送如下参数：支付协议号必填、商户用户标识和用户业务协议号必填其一
    java.util.Map<String, Object> map = new java.util.HashMap<String, Object>();
    map.put("service", "req_smsverify_shortcut");
    map.put("charset", charset);
    map.put("mer_id", paymentMerchant.getMerchantId());
    map.put("sign_type", paymentMerchant.getSignType());
    map.put("version", v);
    map.put("trade_no", request.getOutTradeNo());
    // 协议支付请求参数
    map.put("mer_cust_id", agreement.getUserId());
    map.put("usr_busi_agreement_id", agreement.getBizAgreeId());
    map.put("usr_pay_agreement_id", agreement.getPayAgreeId());

    Map<String, String> data = null;
    try {

      ReqData reqData = Mer2Plat_v40.ReqDataByGet(map);
      String html = HttpRequestUtil.httpRequest(reqData.getUrl());
      data = Plat2Mer_v40.getResData(html);
    } catch (Exception e) {
      log.error("请求U付发送短信验证码出错", e, request);
    } finally {
      checkData(data, data.get("ret_code"), request,
          "请求U付短信验证码发送出错 payNo=[" + request.getTradeNo() + "]");
    }

    return true;
  }

  private void firstPay(String umTradeNo, HttpServletResponse resp,
      PaymentRequestVO request) {
    // 如果是首次支付，须跳转到U付页面
    java.util.Map<String, Object> map = new java.util.HashMap<String, Object>();
    map.put("tradeNo", umTradeNo);
    map.put("identityType", "");
    map.put("identityCode", "");
    map.put("card_Holder", "");
    map.put("merCustId", request.getUserId());
    map.put("payType", request.getCardType());
    map.put("gateId", request.getBankCode());
    // 直接返回页面内容
    String url = "https://m.soopay.net/q/html5/index.do?tradeNo="
        + umTradeNo + "&identityType="
        + "&identityCode=&card_Holder=&merCustId="
        + StringUtils.defaultString(request.getUserId()) + "&payType="
        + request.getCardType() + "&gateId=" + request.getBankCode();
    try {
      resp.sendRedirect(url);
    } catch (IOException e) {
      log.error("U付未能正确跳转到支付页面", e, request);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "未能正确跳转到支付页面payNo=[" + request.getTradeNo() + "]");
    }
  }

  /**
   * 预处理， 获得U付交易号
   */
  @SuppressWarnings("unchecked")
  private String loadUmTradeNo(PaymentRequestVO request) {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.UMPAY);

    java.util.Map<String, Object> map = new java.util.HashMap<String, Object>();
    map.put("service", "pay_req_shortcut");
    map.put("charset", charset);
    map.put("mer_id", paymentMerchant.getMerchantId());
    map.put("sign_type", paymentMerchant.getSignType());
    map.put("ret_url", paymentMerchant.getCallbackSite() + call_back_url);
    map.put("notify_url", paymentMerchant.getNotifySite() + notify_url);
    map.put("res_format", "HTML");
    map.put("version", v);
    map.put("goods_inf", request.getSubject());
    map.put("order_id", request.getTradeNo());
    map.put("mer_date",
        DateFormatUtils.format(Calendar.getInstance(), "yyyyMMdd"));
    map.put("amount", request.getTotalFee().multiply(new BigDecimal(100))
        .intValue()); // 以分为单位
    map.put("amt_type", "RMB");
    map.put("mer_priv", request.getUserId());
    map.put("mer_cust_id", request.getUserId());

    Map<String, String> data = null;
    try {
      ReqData reqData = Mer2Plat_v40.ReqDataByGet(map);
      String html = HttpRequestUtil.httpRequest(reqData.getUrl());
      data = Plat2Mer_v40.getResData(html);
      umPayLogger.info("umpay start data payNo=[" + request.getTradeNo() + "]" + data);
    } catch (Exception e) {
      log.error("U付未能正确生成支付数据", e, request);
    } finally {
      checkData(data, data.get("ret_code"), request,
          "未能正确生成支付数据payNo=[" + request.getTradeNo() + "]");
    }

    umPayLogger.info("umpay start token payNo=[" + request.getTradeNo() + "]" + map);
    umPayLogger.info("paymentMerchant payNo=[" + request.getTradeNo() + "]" + paymentMerchant);

    return data.get("trade_no");
  }

  @SuppressWarnings("unchecked")
  public String getUmpayTradeNo(PaymentRequestVO request){
    PaymentMerchant paymentMerchant = paymentMerchantMapper
            .selectByTradeType(profile, "WEB", PaymentMode.UMPAY);

    Map<String, String> contentData = new HashMap<String, String>();

    /***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
    contentData.put("version", PayUtil.version);
    contentData.put("encoding", PayUtil.encoding);
    contentData.put("signMethod", SDKConfig.getConfig().getSignMethod());
    contentData.put("txnType", "01");              		 	//交易类型 01:消费
    contentData.put("txnSubType", "01");           		 	//交易子类 01：消费
    contentData.put("bizType", "000201");          		 	//填写000201
    contentData.put("channelType", "08");          		 	//渠道类型 08手机

    /***商户接入参数***/
    contentData.put("merId", paymentMerchant.getMerchantId());   //商户号码，请改成自己申请的商户号或者open上注册得来的777商户号测试
    contentData.put("accessType", "0");            		 	//接入类型，商户接入填0 ，不需修改（0：直连商户， 1： 收单机构 2：平台商户）
    contentData.put("orderId", request.getTradeNo());        //商户订单号，8-40位数字字母，不能含“-”或“_”，可以自行定制规则
    contentData.put("txnTime", PayUtil.getCurrentTime());   //订单发送时间，取系统时间，格式为YYYYMMDDhhmmss，必须取当前时间，否则会报txnTime无效
    contentData.put("accType", "01");					 	//账号类型 01：银行卡02：存折03：IC卡帐号类型(卡介质)

    BigDecimal totalFee = request.getTotalFee().multiply(new BigDecimal(100));
    contentData.put("txnAmt", String.valueOf(totalFee.setScale(0, BigDecimal.ROUND_HALF_UP)));	//交易金额 单位为分，不能带小数点
    contentData.put("currencyCode", "156");                 //境内商户固定 156 人民币

    contentData.put("backUrl", PayUtil.backUrl);

    /**对请求参数进行签名并发送http post请求，接收同步应答报文**/
    Map<String, String> reqData = AcpService.sign(contentData,PayUtil.encoding);
    String requestAppUrl = SDKConfig.getConfig().getAppRequestUrl();
    Map<String, String> rspData = AcpService.post(reqData,requestAppUrl,PayUtil.encoding);

    if(!rspData.isEmpty()){
      if(AcpService.validate(rspData, PayUtil.encoding)){
        LogUtil.writeLog("验证签名成功");
        String respCode = rspData.get("respCode") ;
        if(("00").equals(respCode)){
          //成功,获取tn号
          return rspData.get("tn");
        }else{
          //其他应答码为失败请排查原因或做失败处理
        }
      }else{
        LogUtil.writeErrorLog("验证签名失败");
      }
    }else{
      //未返回正确的http状态
      LogUtil.writeErrorLog("未获取到返回报文或返回http状态码非200");
    }

    return null;
  }

  @SuppressWarnings("unchecked")
  @Override
  public PaymentResponseVO payCallback(HttpServletRequest req, HttpServletResponse resp) {
    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.UMPAY);
    Map<String, String> ht = new HashMap<String, String>();
    String name = "", values = "";
    for (Enumeration<String> names = req.getParameterNames(); names
        .hasMoreElements(); ht.put(name, values)) {
      name = (String) names.nextElement();
      values = req.getParameter(name);
//			System.out.println("name:" + name + "  values=" + values);
    }
    PaymentResponseVO p = null;
    Map<String, String> data = null;
    try {
      //验签
      umPayLogger.info("umpay start callback " + ht);
      data = Plat2Mer_v40.getPlatNotifyData(ht);
      p = new PaymentResponseVO();
      p.setDetail(data.toString());
      p.setPaymentMode(PaymentMode.UMPAY);
      Long totalFee = NumberUtils.toLong(req.getParameter("amount"));
      p.setTotalFee((double) totalFee / 100 + "");
      p.setBillNo(req.getParameter("order_id")); //商户订单号
      p.setBillNoPay(req.getParameter("trade_no")); //U付交易号
      p.setBillStatus(PaymentStatus.SUCCESS);
      p.setPaymentMerchantId(paymentMerchant.getId());
      this.savePayResult(p);
    } catch (Exception e) {
      log.error("U付Callback,验签失败", e);
    } finally {
      String ret_code = StringUtils.defaultIfBlank(data.get("ret_code"), data.get("error_code"));
      checkData(data, ret_code, null,
          "U付Callback,验签失败payNo=[" + req.getParameter("order_id") + "]");
    }
    return p;
  }

  @Override
  public PaymentResponseVO payRefundNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    PaymentResponseVO p = new PaymentResponseVO();

    return p;
  }

  @SuppressWarnings("unchecked")
  @Override
  public PaymentResponseVO payNotify(HttpServletRequest req,
      HttpServletResponse resp) {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.UMPAY);

    PaymentResponseVO p = new PaymentResponseVO();
    p.setPaymentMode(PaymentMode.UMPAY);

    // 获取联动平台支付结果通知数据（商户应采取循环遍历方式获取平台通知数据,不应采取固定编码的方式获取固定字段，
    // 否则当平台通知数据发生变化时，容易出现接收数据验签不通过情况）
    Map<String, String> ht = new HashMap<String, String>();
    String name = "", values = "";
    for (Enumeration<String> names = req.getParameterNames(); names
        .hasMoreElements(); ht.put(name, values)) {
      name = (String) names.nextElement();
      values = req.getParameter(name);
      // System.out.println("Notify name:" + name + "  values=" + values);
    }
    // 获取UMPAY平台请求商户的支付结果通知数据,并对请求数据进行验签,此时商户接收到的支付结果通知会存放在这里,
    // 商户可以根据此处的trade_state订单状态来更新订单。
    Map<String, String> reqData = null;
    umPayLogger.info("umpay notify " + ht);
    Map<String, String> resData = new HashMap<String, String>();
    try {
      // 如验证平台签名正确，即应响应UMPAY平台返回码为0000。【响应返回码代表通知是否成功，和通知的交易结果（支付失败、支付成功）无关】
      // 验签支付结果通知 如验签成功，则返回ret_code=0000
      reqData = Plat2Mer_v40.getPlatNotifyData(ht);
      p.setDetail(reqData.toString());
      p.setBillNo(req.getParameter("order_id")); // 商户订单号
      p.setBillNoPay(req.getParameter("trade_no")); // U付交易号
      Long totalFee = NumberUtils.toLong(req.getParameter("amount"));
      p.setTotalFee((double) totalFee / 100 + "");
      p.setBillStatus(PaymentStatus.SUCCESS); // 交易状态
      resData.put("ret_code", successCode);

      // 绑定用户
      if (StringUtils.isNotBlank(ht.get("mer_priv"))
          && StringUtils.isNotBlank(ht.get("usr_busi_agreement_id"))
          && StringUtils.isNotBlank(ht.get("usr_pay_agreement_id"))) {
        bindUmPay(ht);
      }
      p.setPaymentMerchantId(paymentMerchant.getId());
      cashierService.handlePaymentResponse(p.getBillNo(), PaymentMode.UMPAY, p);
    } catch (Exception e) {
      // 如果验签失败，则抛出异常，返回ret_code=1111
      log.error("u付notify,验签失败", e);
      resData.put("ret_code", "1111");
      p.setBillStatus(PaymentStatus.PENDING);
    } finally {
      String ret_code = StringUtils
          .defaultIfBlank(reqData.get("ret_code"), reqData.get("error_code"));
      checkData(reqData, ret_code, null,
          "U付notify,验签失败payNo=[" + req.getParameter("order_id") + "]");
    }

    // 验签后的数据都组织在resData中。
    // 生成平台响应UMPAY平台数据,将该串放入META标签，以下几个参数为结果通知必备参数。
    resData.put("mer_id", req.getParameter("mer_id"));
    resData.put("sign_type", req.getParameter("sign_type"));
    resData.put("version", req.getParameter("version"));
    resData.put("order_id", req.getParameter("order_id"));
    resData.put("mer_date", req.getParameter("mer_date"));
    // resData.put("ret_msg", "success");

    p.setRespDate(Mer2Plat_v40.merNotifyResData(resData));

    return p;
  }

  private void bindUmPay(Map<String, String> ht) {

    String payAgreeId = ht.get("usr_pay_agreement_id");
    String userId = ht.get("mer_priv");
    OutpayAgreement outPayAgreement = outPayAgreementService
        .loadBypayAgreeId(userId, payAgreeId, PaymentMode.UMPAY);

    if (outPayAgreement != null) {
      return;
    }

    BankCardType cardType = BankCardType.CREDITCARD;
    if (ht.get("pay_type").equals("DEBITCARD")) {
      cardType = BankCardType.DEBITCARD;
    }
    OutpayAgreement agreement = new OutpayAgreement();
    agreement.setUserId(userId);
    agreement.setBindStatus("bind");
    agreement.setBizAgreeId(ht.get("usr_busi_agreement_id"));
    agreement.setPayAgreeId(payAgreeId);
    agreement.setPayMode(PaymentMode.UMPAY);
    agreement.setCardType(cardType);
    agreement.setBankCode(ht.get("gate_id"));
    agreement.setAccountName(ht.get("card_holder"));
    agreement.setAccountNum(ht.get("last_four_cardid"));
    agreement.setIdentityNo(ht.get("identity_code"));
    agreement.setIdentityType(ht.get("identity_type"));
    agreement.setMediaType(ht.get("media_type"));
    agreement.setMediaNo(ht.get("media_id"));
    outPayAgreementService.insert(agreement);
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<String> loadBankList() {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.UMPAY);

    List<String> lsBanks = new ArrayList<String>();
    java.util.Map<String, String> map = new java.util.HashMap<String, String>();
    map.put("service", "query_mer_bank_shortcut");
    map.put("charset", charset);
    map.put("mer_id", paymentMerchant.getMerchantId());
    map.put("sign_type", paymentMerchant.getSignType());
    map.put("version", v);
    map.put("pay_type", "CREDITCARD");

    com.xquark.thirds.umpay.api.common.ReqData reqData;
    try {
      reqData = com.xquark.thirds.umpay.api.paygate.v40.Mer2Plat_v40
          .ReqDataByGet(map);
      String url = reqData.getUrl();
      // String html = restTemplate.getForObject(url, String.class);
      String html = HttpRequestUtil.httpRequest(url);

      Map<String, String> data = Plat2Mer_v40.getResData(html);
      if (data != null && data.size() > 0
          && data.containsKey("mer_bank_list")) {
        String[] banks = data.get("mer_bank_list").split("\\|");
        for (String bank : banks) {
          if (StringUtils.isBlank(bank)) {
            continue;
          }
          lsBanks.add(bank);
        }
      }
      return lsBanks;
    } catch (Exception e) {
      log.error("umpay loadBankList failed." + e.getMessage());
      throw new BizException(GlobalErrorCode.UNKNOWN, "获取U付银行列表时出错",
          e.fillInStackTrace());
    }
  }

  @Override
  public void payBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> maps) {

  }

  @Override
  public Map<String, String> payBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    return null;
  }

  @Override
  public void refundBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> maps)
      throws Exception {

  }

  @Override
  public Map<String, String> refundBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    return null;
  }

  @Override
  public void closeTrade(String orderNo) {

  }

  @Override
  public void tradeAccountReport(String startTime, String endTime) {

  }

  @Override
  public void payLock(String couponId) {

  }

  @Override
  public void payUnLock(String couponId) {

  }

  @Override
  public PaymentResponseClientVO sign(PaymentRequestVO request) {
    PaymentResponseClientVO response = null;
    // 得到u付的交易号
    String um_trade_no = null;
    try {
      um_trade_no = loadUmTradeNo(request);

      response = new PaymentResponseClientVO(request.getTradeNo(), um_trade_no,
          request.getTotalFee(),
          request.getOrderIds(), PaymentMode.UMPAY);

      if (StringUtils.isNotBlank(request.getPayAgreementId())) {
        log.info("umpay pay agree agreeId=[" + request.getPayAgreementId() + "]");
        request.setOutTradeNo(um_trade_no);
        response.setPayAgreementId(request.getPayAgreementId());
        sendSms(request); // 如果是协议支付，则获取短信验证码
      }

      Map<String, String> signInfo = new HashMap<String, String>();
      signInfo.put("outTradeNo", um_trade_no);
      response.setPayInfo(signInfo);
    } catch (Exception e) {
      log.error("签名出错 " + request + "  e" + e, e, request);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }

    response.setSignInfo(um_trade_no);
    response.setSigned(genClientSigned(response.getSignInfo()));
    return response;
  }

  @Override
  public PaymentResponseVO payNotifyApp(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }
}
