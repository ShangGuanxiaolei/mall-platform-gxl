package com.xquark.service.outpay.impl.tenpay;

import com.google.common.collect.ImmutableMap;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.outpay.impl.ThirdPaymentQueryRes;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * @author wangxinhua
 * @date 2019-04-19
 * @since 1.0
 */
public class WechatPaymentQueryRes extends ThirdPaymentQueryRes {

  private static final Map<String, PaymentStatus> STATUS_MAPPING =
      ImmutableMap.of(
          "SUCCESS",
          PaymentStatus.SUCCESS,
          "PAYERROR",
          PaymentStatus.CLOSED,
          "USERPAYING",
          PaymentStatus.PENDING,
          "NOTPAY",
          PaymentStatus.WAITING);

  public WechatPaymentQueryRes(
      String billNo,
      String status,
      String billNoPay,
      PaymentMode paymentMode,
      String buyerEmail,
      String buyerId,
      String totalFee,
      String detail,
      String paymentMerchantId,
      String code) {
    super(
        billNo,
        status,
        billNoPay,
        paymentMode,
        buyerEmail,
        buyerId,
        totalFee,
        detail,
        paymentMerchantId,
        code);
  }

  @Override
  public boolean isSuccess() {
    return StringUtils.equals(this.getCode(), "SUCCESS");
  }

  @Override
  public boolean isPaid() {
    return isSuccess() && StringUtils.equals(getStatus(), "SUCCESS");
  }

  @Override
  public PaymentStatus getBillStatus() {
    final String s = getStatus();
    if (StringUtils.isBlank(s)) {
      return PaymentStatus.WAITING;
    }
    final PaymentStatus status = STATUS_MAPPING.get(s);
    if (status == null) {
      return PaymentStatus.WAITING;
    }
    return status;
  }
}
