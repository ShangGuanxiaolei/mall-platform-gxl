package com.xquark.service.outpay.impl.tenpay;

import static com.xquark.service.outpay.impl.TenPaymentImpl.notify_mini_url;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.ImmutableMap;
import com.xquark.dal.mapper.PaymentSwitchConfigMapper;
import com.xquark.dal.model.User;
import com.xquark.dal.model.pay.PaymentConfig;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.outpay.impl.PaymentFacade;
import com.xquark.service.wechat.WechatService;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

/**
 * Created by wangxinhua. Date: 2018/8/27 Time: 下午6:38
 * 小程序银联支付
 */
@Service
public class EpsMiniTenpaySignService extends BaseEpsTenpaySignService {

  @Autowired
  protected EpsMiniTenpaySignService(PaymentFacade paymentFacade, RestOperations restTemplate,
      WechatService wechatService, PaymentSwitchConfigMapper paymentSwitchConfigMapper) {
    super(paymentFacade, restTemplate, wechatService, paymentSwitchConfigMapper);
  }

  @Override
  protected Map<String, String> buildParams(PaymentRequestVO pay,
      PaymentConfig paymentConfig, User payUser) {
    return ImmutableMap.<String, String>builder()
        .put("service", "pay.weixin.jspay")
        .put("mch_id", paymentConfig.getMchId())
        .put("out_trade_no", pay.getTradeNo())
        .put("body", pay.getTradeNo())
        .put("sub_openid", payUser.getLoginname())
        .put("total_fee", pay.getTotalFee().multiply(new BigDecimal(100)).intValue() + "")
        .put("mch_create_ip", StringUtils
            .defaultIfBlank(pay.getRemoteHost(), "127.0.0.1"))
        .put("notify_url", paymentConfig.getNotifySite() + notify_mini_url)
        .put("is_minipg", "1")
        .put("is_raw", "1")
        .put("nonce_str", CommonUtil.CreateNoncestr())
        .put("sub_appid", paymentConfig.getAppId())
        .build();
  }

  /**
   * 重写父类方法，修改订单状态
   * @param payInfo eps返回的payInfo信息
   * @return
   */
  @Override
  protected Map<String, String> handlePayInfo(String payInfo) {
    return JSON.parseObject(payInfo, STRING_MAP_REFERENCE);
  }

}
