package com.xquark.service.outpay;

import com.xquark.dal.model.Order;
import com.xquark.service.BaseService;
import com.xquark.service.pay.model.PaymentResponseVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface ThirdPartyPayment extends BaseService {

  /**
   * 请求支付，跳转第三方， 方法里面直接控制掉response body了
   */
  PaymentResponseVO payRequest(HttpServletRequest req, HttpServletResponse resp,
                               PaymentRequestVO request);

  /**
   * 三方支付同步返回
   *
   * @return null 表示非法请求
   */
  PaymentResponseVO payCallback(HttpServletRequest req, HttpServletResponse resp);

  /**
   * 三方支付异步回调
   *
   * @return null 表示非法请求
   */
  PaymentResponseVO payNotify(HttpServletRequest req, HttpServletResponse resp);

  List<String> loadBankList();

  PaymentResponseVO payRefund(HttpServletRequest req, HttpServletResponse resp,
      PaymentRequestVO request) throws Exception;

  PaymentResponseVO payRefund(PaymentRequestVO request, Order order) throws Exception;

  PaymentResponseVO payCancel(HttpServletRequest req, HttpServletResponse resp,
      PaymentRequestVO request) throws Exception;

  PaymentResponseVO payRefundNotify(HttpServletRequest req, HttpServletResponse resp);

  PaymentResponseVO payConfirm(HttpServletRequest req, HttpServletResponse resp,
      PaymentRequestVO request) throws Exception;

  void payBatchRequest(HttpServletRequest req, HttpServletResponse resp,
      List<ReceiverDetailVO> maps) throws Exception;

  Map<String, String> payBatchNotify(HttpServletRequest req, HttpServletResponse resp);

  void refundBatchRequest(HttpServletRequest req, HttpServletResponse resp,
      List<ReceiverDetailVO> maps) throws Exception;

  Map<String, String> refundBatchNotify(HttpServletRequest req, HttpServletResponse resp);

  void savePayResult(PaymentResponseVO payVO);

  boolean sendSmsAgain(PaymentRequestVO request) throws Exception;

  void closeTrade(String orderNo);

  void tradeAccountReport(String startTime, String endTime);

  void payLock(String extCouponId);

  void payUnLock(String extCouponId);

  PaymentResponseClientVO sign(PaymentRequestVO pay);

  PaymentResponseVO payNotifyApp(HttpServletRequest req,
      HttpServletResponse resp);

  String createPayRequestUrl(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO payRequest);

  PaymentResponseClientVO signApp(PaymentRequestVO pay);
}
