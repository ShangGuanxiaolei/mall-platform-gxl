package com.xquark.service.outpay.impl;

import com.xquark.dal.model.Order;
import com.xquark.service.outpay.AbstractThirdPartyService;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.outpay.PaymentResponseClientVO;
import com.xquark.service.outpay.ReceiverDetailVO;
import com.xquark.service.pay.model.PaymentResponseVO;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


@Component("esunPayment")
public class EsunPaymentImpl extends AbstractThirdPartyService {

  @Override
  public PaymentResponseVO payRequest(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payCallback(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<String> loadBankList() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payRefund(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payRefund(PaymentRequestVO request, Order order) throws Exception {
    return null;
  }

  @Override
  public PaymentResponseVO payCancel(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payRefundNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payConfirm(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void payBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> maps)
      throws Exception {
    // TODO Auto-generated method stub

  }

  @Override
  public Map<String, String> payBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void refundBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> maps)
      throws Exception {
    // TODO Auto-generated method stub

  }

  @Override
  public Map<String, String> refundBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean sendSmsAgain(PaymentRequestVO request) throws Exception {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void closeTrade(String orderNo) {
    // TODO Auto-generated method stub

  }

  @Override
  public void tradeAccountReport(String startTime, String endTime) {
    // TODO Auto-generated method stub

  }

  @Override
  public void payLock(String extCouponId) {
    // TODO Auto-generated method stub

  }

  @Override
  public void payUnLock(String extCouponId) {
    // TODO Auto-generated method stub

  }

  @Override
  public PaymentResponseClientVO sign(PaymentRequestVO pay) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payNotifyApp(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

}
