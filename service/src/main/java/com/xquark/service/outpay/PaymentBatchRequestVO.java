package com.xquark.service.outpay;

import java.util.Date;
import java.util.List;

public class PaymentBatchRequestVO {

  private String service;
  private String partner;
  private String _input_charset;
  private String sign_type;
  private String sign;
  private String notify_url;

  private String account_name;
  private List<ReceiverDetailVO> detail_data;
  private String batch_no;
  private String batch_num;
  private String batch_fee;
  private Date pay_date;
  private String email;
  private String buyer_account_name;
  private String extend_param = "kkkdtest";

  public String getService() {
    return service;
  }

  public void setService(String service) {
    this.service = service;
  }

  public String getPartner() {
    return partner;
  }

  public void setPartner(String partner) {
    this.partner = partner;
  }

  public String get_input_charset() {
    return _input_charset;
  }

  public void set_input_charset(String _input_charset) {
    this._input_charset = _input_charset;
  }

  public String getSign_type() {
    return sign_type;
  }

  public void setSign_type(String sign_type) {
    this.sign_type = sign_type;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  public String getNotify_url() {
    return notify_url;
  }

  public void setNotify_url(String notify_url) {
    this.notify_url = notify_url;
  }

  public String getAccount_name() {
    return account_name;
  }

  public void setAccount_name(String account_name) {
    this.account_name = account_name;
  }

  public List<ReceiverDetailVO> getDetail_data() {
    return detail_data;
  }

  public void setDetail_data(List<ReceiverDetailVO> detail_data) {
    this.detail_data = detail_data;
  }

  public String getBatch_no() {
    return batch_no;
  }

  public void setBatch_no(String batch_no) {
    this.batch_no = batch_no;
  }

  public String getBatch_num() {
    return batch_num;
  }

  public void setBatch_num(String batch_num) {
    this.batch_num = batch_num;
  }

  public String getBatch_fee() {
    return batch_fee;
  }

  public void setBatch_fee(String batch_fee) {
    this.batch_fee = batch_fee;
  }

  public Date getPay_date() {
    return pay_date;
  }

  public void setPay_date(Date pay_date) {
    this.pay_date = pay_date;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getBuyer_account_name() {
    return buyer_account_name;
  }

  public void setBuyer_account_name(String buyer_account_name) {
    this.buyer_account_name = buyer_account_name;
  }

  public String getExtend_param() {
    return extend_param;
  }

  public void setExtend_param(String extend_param) {
    this.extend_param = extend_param;
  }

  public String trans2String() {

    return "";
  }

}
