package com.xquark.service.outpay;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xquark.dal.type.PaymentMode;

@Component
public class ThirdPartyPaymentFactory implements InitializingBean {

  private Map<PaymentMode, ThirdPartyPayment> map;

  @Autowired
  ThirdPartyPayment aliPayment;

  @Autowired
  ThirdPartyPayment aliPaymentPc;

  @Autowired
  ThirdPartyPayment weiXinPayment;

  @Autowired
  ThirdPartyPayment umPayment;

  @Autowired
  ThirdPartyPayment xqPayment;

  public ThirdPartyPayment getPayment(PaymentMode type) {
    return map.get(type);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    map = new HashMap<PaymentMode, ThirdPartyPayment>();
    map.put(PaymentMode.ALIPAY, aliPayment);
    map.put(PaymentMode.ALIPAY_PC, aliPaymentPc);
    map.put(PaymentMode.WEIXIN, weiXinPayment);
    map.put(PaymentMode.UMPAY, umPayment);
    map.put(PaymentMode.XIANGQU, xqPayment);
  }
}
