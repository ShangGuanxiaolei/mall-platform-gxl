package com.xquark.service.outpay.impl;

import com.xquark.dal.mapper.PaymentMerchantMapper;
import com.xquark.dal.mapper.PaymentSwitchConfigMapper;
import com.xquark.dal.mapper.WechatAppConfigMapper;
import com.xquark.dal.model.PaymentSwitchConfig;
import com.xquark.dal.model.User;
import com.xquark.dal.model.pay.PaymentConfig;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.type.TradeType;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.outpay.PaySign;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.outpay.impl.tenpay.*;
import com.xquark.service.shop.ShopService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by wangxinhua. Date: 2018/8/27 Time: 上午10:13 支付 支付相关门面类
 */
@Component
public class PaymentFacade {
	
  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private PaymentSwitchConfigMapper paymentSwitchConfigMapper;

  private WechatAppConfigMapper wechatAppConfigMapper;

  private PaymentMerchantMapper paymentMerchantMapper;

  private ShopService shopService;

  @Value("${site.webapi.host.name}")
  String siteHost;

  @Value("${wechat.pay.notify.test.name}")
  String testNotifyHost;

  @Value("${profiles.active}")
  String profile;

  public boolean epsStatus() {
    PaymentSwitchConfig paymentSwitchConfig = paymentSwitchConfigMapper.selectByCodeAndProfile("EPS", profile);
    return paymentSwitchConfig.getStatus();
  }

  @Autowired
  public void setWechatAppConfigMapper(WechatAppConfigMapper wechatAppConfigMapper) {
    this.wechatAppConfigMapper = wechatAppConfigMapper;
  }

  @Autowired
  public void setPaymentMerchantMapper(PaymentMerchantMapper paymentMerchantMapper) {
    this.paymentMerchantMapper = paymentMerchantMapper;
  }

  @Autowired
  public void setShopService(ShopService shopService) {
    this.shopService = shopService;
  }

  /**
   * 获取支付配置
   *
   * @return 支付配置
   */
  public PaymentConfig loadPayment(String profile, PaymentMode paymentMode, String tradeType) {
    TradeType tradeTypeEuem;
    try {
      tradeTypeEuem = TradeType.valueOf(tradeType);
    } catch (Exception e) {
      throw new IllegalArgumentException("tradeType不正确");
    }
    // eps为开启状态，或者paymentMode为eps获取对应支付配置
    if (paymentMode == PaymentMode.EPS || epsStatus()) {
      //目前是：只要EPS支付为开启状态，就直接将查询到的paymentConfig返回，不执行后面的代码
      return paymentMerchantMapper.selectByTradeType(profile, tradeType, PaymentMode.EPS);
    }

    WechatAppConfig wechatAppConfig = wechatAppConfigMapper.selectByShopId(shopService.loadRootShop().getId());
    if (wechatAppConfig == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "微信支付配置未初始化");
    }
    WechatAppConfig paymentConfig = new WechatAppConfig();
    paymentConfig.setNotifySite(StringUtils.defaultIfBlank(wechatAppConfig.getNotifySite(), getNotifyHost()));
    switch (tradeTypeEuem) {
      case APP:
        paymentConfig.setAppId(wechatAppConfig.getAppAppId());
        paymentConfig.setAppSecret(wechatAppConfig.getAppSecret());
        paymentConfig.setMchId(wechatAppConfig.getAppMchId());
        paymentConfig.setMchKey(wechatAppConfig.getAppMchKey());
        paymentConfig.setApp_cert_password(wechatAppConfig.getApp_cert_password());
        paymentConfig.setCert_file(wechatAppConfig.getApp_cert_file());
        break;
      case MINIPROGRAM:
        paymentConfig.setAppId(wechatAppConfig.getAppMiniId());
        paymentConfig.setAppSecret(wechatAppConfig.getAppAppSecret());
        paymentConfig.setMchId(wechatAppConfig.getMiniMchId());
        paymentConfig.setMchKey(wechatAppConfig.getMiniMchKey());
        paymentConfig.setCert_file(wechatAppConfig.getCert_file());
        paymentConfig.setCert_password(wechatAppConfig.getCert_password());
        break;
      case H5:
        paymentConfig.setAppId(wechatAppConfig.getAppId());
        paymentConfig.setAppSecret(wechatAppConfig.getMchSecret());
        paymentConfig.setMchId(wechatAppConfig.getMchId());
        paymentConfig.setMchKey(wechatAppConfig.getMchKey());
        paymentConfig.setCert_file(wechatAppConfig.getCert_file());
        paymentConfig.setCert_password(wechatAppConfig.getCert_password());
        break;
      default:
    }
    return paymentConfig;
  }

  /**
   * 根据不同的paymentMode获取签名结果
   *
   * @param pay payRequest
   * @param payUser 当前申请支付用户
   * @return 支付结果
   */
  public Map<String, String> pay(PaymentRequestVO pay,
      User payUser) {
    TradeType tradeType = pay.getTradeType();
    Class<? extends PaySign> targetImpl = null;
    if(TradeType.MINIPROGRAM.equals(tradeType)){// 小程序支付
      if(epsStatus()){
        targetImpl = EpsMiniTenpaySignService.class;
      }else{
        targetImpl = MiniTenpaySignService.class;
      }
    }else if(TradeType.APP.equals(tradeType)){// APP支付
      if(epsStatus()){
        targetImpl = EpsAppTenpaySignService.class;
      }else{
        targetImpl = AppTenpaySignService.class;
      }
    }else if(TradeType.ALIPAY.equals(tradeType)){//支付宝支付
      targetImpl = EpsAlipayTenpaySignService.class;
    }
    if (targetImpl == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "支付类型 " + tradeType + " 不合法");
    }
    PaySign paySign = SpringContextUtil.getBean(targetImpl);
    if (paySign == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "支付类型 " + tradeType + " 没有对应实现");
    }
    Map<String, String> payResult = paySign.sign(pay, payUser);
    log.info("支付返回结果payResult：{}",payResult);
    return payResult;
  }

  private String getNotifyHost() {
    if (StringUtils.equalsIgnoreCase("dev", profile) || StringUtils.equalsIgnoreCase("uat", profile)) {
      return testNotifyHost;
    }
    return siteHost;
  }
}
