package com.xquark.service.outpay.impl.alipay;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;

public class RSA2 {

    public static String sign(String content, String privateKey, String input_charset) {
        String rsaSign = null;
        try {
            rsaSign = AlipaySignature
                    .rsaSign(content, privateKey, input_charset,"RSA2");
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }

        return rsaSign;
    }

    public static boolean verify(String content, String sign, String secret,
                                 String input_charset) {

        boolean result = false;
        String signResult = sign(content, secret, input_charset);
        if(signResult != null && signResult.equals(sign)){
            result = true;
        }
        return result;
    }
}
