package com.xquark.service.outpay.impl;

import com.xquark.dal.model.PaidAble;

import java.util.Optional;

/**
 * 三方支付
 * @author wangxinhua
 * @date 2019-04-19
 * @since 1.0
 */
@FunctionalInterface
public interface ThirdQueryAdapter<T extends ThirdPaymentQueryRes> {

    /**
     * 检查第三方支付状态
     * @param paidAble 可支付对象, 当前支持按主订单和子订单查询
     * @return Optional<T> 状态查询
     */
     Optional<T> query(PaidAble paidAble);

}
