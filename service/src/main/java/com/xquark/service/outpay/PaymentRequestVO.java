package com.xquark.service.outpay;

import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.type.TradeType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 支付请求对象
 *
 * @author odin
 */
public class PaymentRequestVO implements Serializable {

  private static final long serialVersionUID = 1L;

  @NotNull
  @Size(min = 1)
  String tradeNo; //快店交易订单号

  String outTradeNo; //外部订单号，类似U付协议支付时，需要传

  String userId;  //外部订单号，类似U付协议支付时，需要传

  String verifyCode; //验证码，类似U付协议支付时，需要传

  String outRequestNo; //内部退款编号，部分退款使用

  String subject; //支付订单标题

  String description; //支付订单描述

  String tradeUrl; //支付订单返回交易订单URL

  @NotNull
  BigDecimal totalFee; //支付金额

  String bankCode; // 银行代码

  String cardType; // 信用卡or储蓄卡

  String payAgreementId; //协议ID

  String couponId;//优惠券ID

  String outUserId;  //第三方用户ID

  String productId; //商品ID，想去用到

  String cashierItemId;//收银台ID

  List<String> orderIds; //订单ID

  PaymentMode payType;  //支付类型

  TradeType tradeType; // 交易类型

  Map<String, String> signInfo;

  private String remoteHost;

  String sign;  //与第三方支付渠道的签名参数及签名
  String signed; //与客户端的签名

  String code; //与客户端的签名

  private String openId;

  public String getOpenId() {
    return openId;
  }

  public void setOpenId(String openId) {
    this.openId = openId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getVerifyCode() {
    return verifyCode;
  }

  public void setVerifyCode(String verifyCode) {
    this.verifyCode = verifyCode;
  }

  public String getTradeNo() {
    return tradeNo;
  }

  public void setTradeNo(String tradeNo) {
    this.tradeNo = tradeNo;
  }

  public PaymentMode getPayType() {
    return payType;
  }

  public void setPayType(PaymentMode payType) {
    this.payType = payType;
  }

  public String getOutTradeNo() {
    return outTradeNo;
  }

  public void setOutTradeNo(String outTradeNo) {
    this.outTradeNo = outTradeNo;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getTradeUrl() {
    return tradeUrl;
  }

  public void setTradeUrl(String tradeUrl) {
    this.tradeUrl = tradeUrl;
  }

  public BigDecimal getTotalFee() {
    return totalFee;
  }

  public void setTotalFee(BigDecimal totalFee) {
    this.totalFee = totalFee;
  }

  public String getBankCode() {
    return bankCode;
  }

  public void setBankCode(String bankCode) {
    this.bankCode = bankCode;
  }

  public String getCardType() {
    return cardType;
  }

  public void setCardType(String cardType) {
    this.cardType = cardType;
  }

  public String getPayAgreementId() {
    return payAgreementId;
  }

  public void setPayAgreementId(String payAgreementId) {
    this.payAgreementId = payAgreementId;
  }


  public Map<String, String> getSignInfo() {
    return signInfo;
  }

  public void setSignInfo(Map<String, String> signInfo) {
    this.signInfo = signInfo;
  }

  public String getRemoteHost() {
    return remoteHost;
  }

  public void setRemoteHost(String remoteHost) {
    this.remoteHost = remoteHost;
  }

  public String getCouponId() {
    return couponId;
  }

  public void setCouponId(String couponId) {
    this.couponId = couponId;
  }

  public String getOutUserId() {
    return outUserId;
  }

  public void setOutUserId(String outUserId) {
    this.outUserId = outUserId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getCashierItemId() {
    return cashierItemId;
  }

  public void setCashierItemId(String cashierItemId) {
    this.cashierItemId = cashierItemId;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  public List<String> getOrderIds() {
    return orderIds;
  }

  public void setOrderIds(List<String> orderIds) {
    this.orderIds = orderIds;
  }

  public String getSigned() {
    return signed;
  }

  public void setSigned(String signed) {
    this.signed = signed;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String oauthCode) {
    this.code = oauthCode;
  }

  public String getOutRequestNo() {
    return outRequestNo;
  }

  public void setOutRequestNo(String outRequestNo) {
    this.outRequestNo = outRequestNo;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
  }

  public TradeType getTradeType() {
    return tradeType;
  }

  public void setTradeType(TradeType tradeType) {
    this.tradeType = tradeType;
  }
}
