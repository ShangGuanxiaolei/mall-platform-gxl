package com.xquark.service.outpay;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.xquark.dal.type.PaymentMode;

/**
 * 支付请求对象
 *
 * @author odin
 */
public class PaymentResponseClientVO implements Serializable {

  private static final long serialVersionUID = 1L;

  String tradeNo; //快店交易订单号

  String outTradeNo; //外部订单号，类似U付协议支付时，需要传

  BigDecimal totalFee; //支付金额

  List<String> orderIds; //订单ID

  PaymentMode payType;  //支付类型

  String payAgreementId; //快捷支付ID

  private String payStatus; //是否支付成功，红包金额全部抵掉订单金额时返回

  Map<String, String> payInfo; //第三方支付信息

  String signInfo; //写客户端的签名内容

  String signed; //与客户端的签名结果

  public PaymentResponseClientVO() {
  }

  public PaymentResponseClientVO(String tradeNo, String outTradeNo, BigDecimal totalFee,
      List<String> orderIds, PaymentMode payType) {
    this.tradeNo = tradeNo;
    this.outTradeNo = outTradeNo;
    this.totalFee = totalFee;
    this.orderIds = orderIds;
    this.payType = payType;
  }

  public String getPayStatus() {
    return payStatus;
  }

  public void setPayStatus(String payStatus) {
    this.payStatus = payStatus;
  }

  public String getTradeNo() {
    return tradeNo;
  }

  public void setTradeNo(String tradeNo) {
    this.tradeNo = tradeNo;
  }

  public String getOutTradeNo() {
    return outTradeNo;
  }

  public void setOutTradeNo(String outTradeNo) {
    this.outTradeNo = outTradeNo;
  }

  public BigDecimal getTotalFee() {
    return totalFee;
  }

  public void setTotalFee(BigDecimal totalFee) {
    this.totalFee = totalFee;
  }

  public List<String> getOrderIds() {
    return orderIds;
  }

  public void setOrderIds(List<String> orderIds) {
    this.orderIds = orderIds;
  }

  public PaymentMode getPayType() {
    return payType;
  }

  public void setPayType(PaymentMode payType) {
    this.payType = payType;
  }

  public Map<String, String> getPayInfo() {
    return payInfo;
  }

  public void setPayInfo(Map<String, String> payInfo) {
    this.payInfo = payInfo;
  }

  public String getSignInfo() {
    return signInfo;
  }

  public void setSignInfo(String signInfo) {
    this.signInfo = signInfo;
  }

  public String getSigned() {
    return signed;
  }

  public void setSigned(String signed) {
    this.signed = signed;
  }

  public String getPayAgreementId() {
    return payAgreementId;
  }

  public void setPayAgreementId(String payAgreementId) {
    this.payAgreementId = payAgreementId;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
  }

}
