package com.xquark.service.outpay;

import com.xquark.dal.model.User;
import java.util.Map;

/**
 * Created by wangxinhua. Date: 2018/8/27 Time: 下午7:16
 * 支付签名接口
 */
public interface PaySign {

  /**
   * 获取第三方支付签名, 并返回结果
   *
   * @param pay payRequest
   * @param payUser 当前申请支付用户
   * @return 支付申请结果
   */
  Map<String, String> sign(PaymentRequestVO pay, User payUser);

}
