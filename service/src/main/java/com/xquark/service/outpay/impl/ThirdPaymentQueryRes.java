package com.xquark.service.outpay.impl;

import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.PaymentMode;

/**
 * 第三方支付响应结果
 *
 * @author wangxinhua
 * @date 2019-04-19
 * @since 1.0
 */
public abstract class ThirdPaymentQueryRes {

  private final String billNo;
  private final String billNoPay;
  private final PaymentMode paymentMode;
  private final String buyerEmail;
  private final String buyerId;
  private final String totalFee;
  private final String detail;
  private final String paymentMerchantId;
  private final String code;
  private final String status;

  public ThirdPaymentQueryRes(
          String billNo,
          String status,
          String billNoPay,
          PaymentMode paymentMode,
          String buyerEmail,
          String buyerId,
          String totalFee,
          String detail,
          String paymentMerchantId,
          String code) {
    this.billNo = billNo;
    this.status = status;
    this.billNoPay = billNoPay;
    this.paymentMode = paymentMode;
    this.buyerEmail = buyerEmail;
    this.buyerId = buyerId;
    this.totalFee = totalFee;
    this.detail = detail;
    this.paymentMerchantId = paymentMerchantId;
    this.code = code;
  }

  /**
   * 是否成功
   *
   * @return true or false
   */
  public abstract boolean isSuccess();

  /**
   * 是否已经支付成功
   *
   * @return true or false
   */
  public abstract boolean isPaid();

  public abstract PaymentStatus getBillStatus();

  public String getCode() {
    return code;
  }

  public String getStatus() {
    return status;
  }

  public String getBillNo() {
    return billNo;
  }

  public String getBillNoPay() {
    return billNoPay;
  }

  public String getBuyerEmail() {
    return buyerEmail;
  }

  public String getBuyerId() {
    return buyerId;
  }

  public String getTotalFee() {
    return totalFee;
  }

  public String getDetail() {
    return detail;
  }

  public String getPaymentMerchantId() {
    return paymentMerchantId;
  }

  public PaymentMode getPaymentMode() {
    return paymentMode;
  }
}
