package com.xquark.service.outpay.impl.tenpay;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSON;
import com.xquark.utils.http.HttpInvokeResult;

/**
 * Http客户端工具类<br/> 这是内部调用类，请不要在外部调用。
 *
 * @author miklchen
 */
public class HttpClientUtil {

  public static final String SunX509 = "SunX509";
  public static final String JKS = "JKS";
  public static final String PKCS12 = "PKCS12";
  public static final String TLS = "TLS";

  /**
   * get HttpURLConnection
   *
   * @param strUrl url地址
   * @return HttpURLConnection
   */
  public static HttpURLConnection getHttpURLConnection(String strUrl)
      throws IOException {
    URL url = new URL(strUrl);
    HttpURLConnection httpURLConnection = (HttpURLConnection) url
        .openConnection();
    return httpURLConnection;
  }

  /**
   * get HttpsURLConnection
   *
   * @param strUrl url地址
   * @return HttpsURLConnection
   */
  public static HttpsURLConnection getHttpsURLConnection(String strUrl)
      throws IOException {
    URL url = new URL(strUrl);
    HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url
        .openConnection();
    return httpsURLConnection;
  }

  /**
   * 获取不带查询串的url
   *
   * @return String
   */
  public static String getURL(String strUrl) {

    if (null != strUrl) {
      int indexOf = strUrl.indexOf("?");
      if (-1 != indexOf) {
        return strUrl.substring(0, indexOf);
      }

      return strUrl;
    }

    return strUrl;

  }

  /**
   * 获取查询串
   *
   * @return String
   */
  public static String getQueryString(String strUrl) {

    if (null != strUrl) {
      int indexOf = strUrl.indexOf("?");
      if (-1 != indexOf) {
        return strUrl.substring(indexOf + 1, strUrl.length());
      }

      return "";
    }

    return strUrl;
  }

  /**
   * 查询字符串转换成Map<br/> name1=key1&name2=key2&...
   */
  @SuppressWarnings("rawtypes")
  public static Map queryString2Map(String queryString) {
    if (null == queryString || "".equals(queryString)) {
      return null;
    }

    Map m = new HashMap();
    String[] strArray = queryString.split("&");
    for (int index = 0; index < strArray.length; index++) {
      String pair = strArray[index];
      HttpClientUtil.putMapByPair(pair, m);
    }

    return m;

  }

  /**
   * 把键值添加至Map<br/> pair:name=value
   *
   * @param pair name=value
   */
  @SuppressWarnings({"rawtypes", "unchecked"})
  public static void putMapByPair(String pair, Map m) {

    if (null == pair || "".equals(pair)) {
      return;
    }

    int indexOf = pair.indexOf("=");
    if (-1 != indexOf) {
      String k = pair.substring(0, indexOf);
      String v = pair.substring(indexOf + 1, pair.length());
      if (null != k && !"".equals(k)) {
        m.put(k, v);
      }
    } else {
      m.put(pair, "");
    }
  }

  /**
   * BufferedReader转换成String<br/> 注意:流关闭需要自行处理
   *
   * @return String
   */
  public static String bufferedReader2String(BufferedReader reader) throws IOException {
    StringBuffer buf = new StringBuffer();
    String line = null;
    while ((line = reader.readLine()) != null) {
      buf.append(line);
      buf.append("\r\n");
    }

    return buf.toString();
  }

  /**
   * 处理输出<br/> 注意:流关闭需要自行处理
   */
  public static void doOutput(OutputStream out, byte[] data, int len)
      throws IOException {
    int dataLen = data.length;
    int off = 0;
    while (off < dataLen) {
      if (len >= dataLen) {
        out.write(data, off, dataLen);
      } else {
        out.write(data, off, len);
      }

      //刷新缓冲区
      out.flush();

      off += len;

      dataLen -= len;
    }

  }

  /**
   * 获取SSLContext
   */
  public static SSLContext getSSLContext(
      FileInputStream trustFileInputStream, String trustPasswd,
      FileInputStream keyFileInputStream, String keyPasswd)
      throws NoSuchAlgorithmException, KeyStoreException,
      CertificateException, IOException, UnrecoverableKeyException,
      KeyManagementException {

    // ca
    TrustManagerFactory tmf = TrustManagerFactory.getInstance(HttpClientUtil.SunX509);
    KeyStore trustKeyStore = KeyStore.getInstance(HttpClientUtil.JKS);
    trustKeyStore.load(trustFileInputStream, HttpClientUtil
        .str2CharArray(trustPasswd));
    tmf.init(trustKeyStore);

    final char[] kp = HttpClientUtil.str2CharArray(keyPasswd);
    KeyManagerFactory kmf = KeyManagerFactory.getInstance(HttpClientUtil.SunX509);
    KeyStore ks = KeyStore.getInstance(HttpClientUtil.PKCS12);
    ks.load(keyFileInputStream, kp);
    kmf.init(ks, kp);

    SecureRandom rand = new SecureRandom();
    SSLContext ctx = SSLContext.getInstance(HttpClientUtil.TLS);
    ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), rand);

    return ctx;
  }

  /**
   * 获取CA证书信息
   *
   * @param cafile CA证书文件
   * @return Certificate
   */
  public static Certificate getCertificate(File cafile)
      throws CertificateException, IOException {
    CertificateFactory cf = CertificateFactory.getInstance("X.509");
    FileInputStream in = new FileInputStream(cafile);
    Certificate cert = cf.generateCertificate(in);
    in.close();
    return cert;
  }

  /**
   * 字符串转换成char数组
   *
   * @return char[]
   */
  public static char[] str2CharArray(String str) {
    if (null == str) {
      return null;
    }

    return str.toCharArray();
  }

  /**
   * 存储ca证书成JKS格式
   */
  public static void storeCACert(Certificate cert, String alias,
      String password, OutputStream out) throws KeyStoreException,
      NoSuchAlgorithmException, CertificateException, IOException {
    KeyStore ks = KeyStore.getInstance("JKS");

    ks.load(null, null);

    ks.setCertificateEntry(alias, cert);

    // store keystore
    ks.store(out, HttpClientUtil.str2CharArray(password));

  }

  public static InputStream String2Inputstream(String str) {
    return new ByteArrayInputStream(str.getBytes());
  }
  
  public static HttpInvokeResult post(String url,Object params) {
	  
	  	PoolingHttpClientConnectionManager connManager =
		        new PoolingHttpClientConnectionManager();

	    // 创建自定义的httpclient对象
	    CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build();
	    // 创建post方式请求对象
	    HttpPost httpPost = new HttpPost(url);
	    // 设置参数到请求对象中
	    httpPost.setEntity(new StringEntity(JSON.toJSONString(params), StandardCharsets.UTF_8));

	    // 设置header信息
	    // 指定报文头【Content-type】、【User-Agent】
	    httpPost.setHeader("Content-type", "application/json");

	    // 执行请求操作，并拿到结果（同步阻塞）
	    CloseableHttpResponse response = null;
	    final HttpInvokeResult result = new HttpInvokeResult();
	    try {
	    	  response = client.execute(httpPost);
	    	  final HttpEntity entity = response.getEntity();
		      result.setResponse(response);
		      final StatusLine statusLine = response.getStatusLine();
		      result.setStatusCode(statusLine.getStatusCode());
		      if (statusLine.getStatusCode() >= 300) {
		    	  result.setReason(statusLine.getReasonPhrase());
		      }
	        result.setContent(EntityUtils.toString(entity, "UTF-8"));
	    } catch (final Throwable e) {
	        result.setReason(e.getMessage());
	        result.setException(e);
	    }
	    return result;
  }

}
