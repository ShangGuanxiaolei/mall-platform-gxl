package com.xquark.service.outpay.impl.alipay;

import com.alipay.api.response.AlipayTradeQueryResponse;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.outpay.impl.ThirdPaymentQueryRes;
import org.apache.commons.lang3.StringUtils;

/**
 * @author wangxinhua
 * @date 2019-04-19
 * @since 1.0
 */
public class AliPayPaymentQueryRes extends ThirdPaymentQueryRes {

  public AliPayPaymentQueryRes(
      String billNo,
      String status,
      String billNoPay,
      PaymentMode paymentMode,
      String buyerEmail,
      String buyerId,
      String totalFee,
      String detail,
      String paymentMerchantId,
      String code) {
    super(
        billNo,
        status,
        billNoPay,
        paymentMode,
        buyerEmail,
        buyerId,
        totalFee,
        detail,
        paymentMerchantId,
        code);
  }

  /**
   * 查询是否成功 {@link AlipayTradeQueryResponse#isSuccess()}
   *
   * @return true or false
   */
  @Override
  public boolean isSuccess() {
    return StringUtils.isEmpty(getCode());
  }

  @Override
  public boolean isPaid() {
    return isSuccess() && getBillStatus() == PaymentStatus.SUCCESS;
  }

  @Override
  public PaymentStatus getBillStatus() {
    final String s = getStatus();
    final PaymentStatus status;
    if ("TRADE_SUCCESS".equalsIgnoreCase(s)) {
      status = PaymentStatus.SUCCESS;
    } else if ("TRADE_FINISHED".equalsIgnoreCase(s)) {
      status = PaymentStatus.SUCCESS;
    } else if ("TRADE_CLOSED".equalsIgnoreCase(s)) {
      status = PaymentStatus.CLOSED;
    } else {
      status = PaymentStatus.PENDING;
    }
    return status;
  }
}
