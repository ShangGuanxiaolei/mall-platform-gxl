package com.xquark.service.outpay.impl.alipay;

import com.alipay.api.response.AlipayTradeQueryResponse;
import com.xquark.dal.model.PaidAble;
import com.xquark.service.outpay.impl.AliPaymentImpl;
import com.xquark.service.outpay.impl.ThirdQueryAdapter;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author wangxinhua
 * @date 2019-04-19
 * @since 1.0
 */
@Service
public class AliPayQueryAdapter implements ThirdQueryAdapter<AliPayPaymentQueryRes> {

  private final AliPaymentImpl aliPayment;

  @Autowired
  public AliPayQueryAdapter(AliPaymentImpl aliPayment) {
    this.aliPayment = aliPayment;
  }

  @Override
  public Optional<AliPayPaymentQueryRes> query(PaidAble paidAble) {
    final Pair<String, AlipayTradeQueryResponse> pair = aliPayment.queryRet(paidAble);
    final AlipayTradeQueryResponse response = pair.getRight();
    if (response == null) {
      return Optional.empty();
    }
    return Optional.of(
        new AliPayPaymentQueryRes(
            response.getOutTradeNo(),
            response.getTradeStatus(),
            response.getTradeNo(),
            paidAble.getPayType(),
            response.getBuyerUserId(),
            response.getBuyerLogonId(),
            response.getTotalAmount(),
            response.getBody(),
            pair.getLeft(),
            response.getSubCode()));
  }
}
