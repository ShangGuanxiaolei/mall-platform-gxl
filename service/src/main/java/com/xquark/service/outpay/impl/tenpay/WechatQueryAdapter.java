package com.xquark.service.outpay.impl.tenpay;

import com.google.common.base.Preconditions;
import com.xquark.dal.model.PaidAble;
import com.xquark.service.outpay.impl.OrderQuery;
import com.xquark.service.outpay.impl.ThirdQueryAdapter;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

/**
 * @author wangxinhua
 * @date 2019-04-19
 * @since 1.0
 */
@Service
public class WechatQueryAdapter implements ThirdQueryAdapter<WechatPaymentQueryRes> {

    private final OrderQuery orderQuery;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public WechatQueryAdapter(OrderQuery orderQuery) {
        this.orderQuery = orderQuery;
    }

    @Override
    public Optional<WechatPaymentQueryRes> query(PaidAble paidAble) {
        Preconditions.checkNotNull(paidAble, "订单不能为空");
        final Map<String, String> ret = orderQuery.signOrder(paidAble);
        if (MapUtils.isEmpty(ret)) {
            logger.error("支付单号: {} 微信返回错误", paidAble.getPayNo());
            return Optional.empty();
        }
        final String code = ret.get("return_code");
        final String tradeStatus = ret.get("trade_state");
        final String payNo = ret.get("out_trade_no");
        final String transactionId = ret.get("transaction_id");
        final String openId = ret.get("openid");
        final String totalFee = ret.get("total_fee");
        final String merchantId = ret.get("mch_id");

        return Optional.of(new WechatPaymentQueryRes(payNo, tradeStatus, transactionId,
                paidAble.getPayType(), openId, openId, totalFee, ret.toString(), merchantId, code));
    }

}
