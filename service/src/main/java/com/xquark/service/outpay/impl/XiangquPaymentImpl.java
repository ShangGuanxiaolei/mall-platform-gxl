package com.xquark.service.outpay.impl;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.model.Coupon;
import com.xquark.dal.model.Order;
import com.xquark.dal.status.CouponStatus;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.outpay.AbstractThirdPartyService;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.outpay.PaymentResponseClientVO;
import com.xquark.service.outpay.ReceiverDetailVO;
import com.xquark.service.pay.model.PaymentResponseVO;
import com.xquark.service.pricing.CouponService;
import com.xquark.service.pricing.CouponVO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

@Component("xqPayment")
public class XiangquPaymentImpl extends AbstractThirdPartyService {

  private Logger log = LoggerFactory.getLogger(getClass());

//	@Value("${site.web.host.name}")
//	String siteHost;

  @Value("${xiangqu.api.host.url}")
  private String xiangquWebSite;

  @Autowired
  private CouponService couponService;

  @Autowired
  private RestOperations restTemplate;

  /**
   * 页面跳转同步通知页面路径
   */
  public static final String call_back_url = "/pay/xiangqu/call_back";

  @Override
  public void payLock(String extCouponId) {
//		innerapi/coupon/lock?id=1531

    if (StringUtils.isNotBlank(extCouponId)) {
      String gateway = xiangquWebSite + "/innerapi/coupon/lock?id=" + extCouponId;
      String res = restTemplate.getForObject(gateway, String.class, new Object[]{});
      JSONObject json = JSONObject.parseObject(res);

      int rCode = json.getInteger("code");
      if (rCode != 200) {
        log.error("优惠券已锁定或已使用 [" + extCouponId + "]");
        throw new BizException(GlobalErrorCode.UNKNOWN, json.getString("msg"));
      }
    }
  }

  @Override
  public void payUnLock(String extCouponId) {
    if (StringUtils.isNotBlank(extCouponId)) {
      String gateway = xiangquWebSite + "/innerapi/coupon/unLock?id=" + extCouponId;
      String res = restTemplate.getForObject(gateway, String.class, new Object[]{});
      JSONObject json = JSONObject.parseObject(res);

      int rCode = json.getInteger("code");
      if (rCode != 200) {
        log.error("优惠券锁定失败[" + extCouponId + "]");
        throw new BizException(GlobalErrorCode.UNKNOWN, json.getString("msg"));
      }
    }
  }

  @Override
  public PaymentResponseVO payRequest(HttpServletRequest req, HttpServletResponse resp,
      PaymentRequestVO request) {

    CouponVO coupon = couponService.loadVO(request.getCouponId());
    PaymentResponseVO p = new PaymentResponseVO();
    p.setBillNoPay(""); //外部订单号
    p.setBillNo(request.getTradeNo()); // 商户订单号
    p.setPaymentMode(PaymentMode.XIANGQU);
    p.setTotalFee(request.getTotalFee().toString());

    if (StringUtils.isNotBlank(coupon.getExtCouponId())) {
      String gateway =
          xiangquWebSite + "/innerapi/coupon/use?id=" + coupon.getExtCouponId() + "&userId="
              + request.getOutUserId() + "&cost=" + request.getTotalFee() + "&kdProductId="
              + request.getProductId() + "&orderId=" + request.getTradeNo();
      String res = restTemplate.getForObject(gateway, String.class, new Object[]{});
      JSONObject json = JSONObject.parseObject(res);

      int rCode = json.getInteger("code");
      if (rCode == 200) {
        p.setBillStatus(PaymentStatus.SUCCESS);
      } else {
        p.setBillStatus(PaymentStatus.CLOSED);
        log.error("想去支付请求失败" + request);
        return null;
//				throw new BizException(GlobalErrorCode.UNKNOWN, json.getString("msg"));
      }
    }

    Coupon c = couponService.use(request.getCouponId(), p.getBillNo());
    if (c.getStatus() == CouponStatus.USED) {
      p.setBillStatus(PaymentStatus.SUCCESS);
    } else {
      p.setBillStatus(PaymentStatus.CLOSED);
    }
    p.setBillNoPay(request.getTradeNo());
    this.savePayResult(p);
    return p;
  }

  @Override
  public PaymentResponseVO payCallback(HttpServletRequest req, HttpServletResponse resp) {
//		String name = "",values="";
    for (Enumeration<String> names = req.getParameterNames(); names.hasMoreElements(); ) {
//	        name = (String)names.nextElement();
//	        values = req.getParameter(name);
//	        System.out.println("name:" + name + "  values=" + values);
    }
    PaymentResponseVO p = new PaymentResponseVO();
    p.setBillStatus(PaymentStatus.SUCCESS);
    p.setPaymentMode(PaymentMode.XIANGQU);
    // 商户订单号
    p.setBillNo(req.getParameter("orderNo"));
    // 外部交易号
    p.setBillNoPay("");
    // 交易状态
    p.setBillStatus(PaymentStatus.SUCCESS);

    return p;
  }

  @Override
  public PaymentResponseVO payNotify(HttpServletRequest req, HttpServletResponse resp) {
    return null;
  }

  @Override
  public List<String> loadBankList() {
    return new ArrayList<String>();
  }

  @Override
  public void payBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> params) throws Exception {

  }

  @Override
  public void refundBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> params) throws Exception {
  }

  @Override
  public Map<String, String> refundBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    return null;
  }

  @Override
  public PaymentResponseVO payRefund(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    return null;
  }

  @Override
  public PaymentResponseVO payRefund(PaymentRequestVO request, Order order) throws Exception {
    return null;
  }

  @Override
  public PaymentResponseVO payCancel(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    return null;
  }

  @Override
  public PaymentResponseVO payRefundNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    return null;
  }

  @Override
  public PaymentResponseVO payConfirm(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    return null;
  }

  @Override
  public boolean sendSmsAgain(PaymentRequestVO request) throws Exception {
    return false;
  }

  @Override
  public Map<String, String> payBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    return null;
  }

  @Override
  public void closeTrade(String bizNo) {
//		payUnLock();
  }

  @Override
  public void tradeAccountReport(String startTime, String endTime) {

  }

  @Override
  public PaymentResponseClientVO sign(PaymentRequestVO pay) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payNotifyApp(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }
}
