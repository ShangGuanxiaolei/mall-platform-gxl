package com.xquark.service.outpay.impl.alipay;

import com.google.common.collect.ImmutableMap;
import com.xquark.dal.model.PaidAble;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.type.TradeType;
import com.xquark.service.outpay.impl.OrderQuery;
import com.xquark.service.outpay.impl.ThirdQueryAdapter;
import com.xquark.service.outpay.impl.eps.EpsPaymentQueryRes;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

/**
 * @author wangxinhua
 * @date 2019-05-04
 * @since 1.0
 */
@Service
public class EpsQueryAdapter implements ThirdQueryAdapter<EpsPaymentQueryRes> {

    private final OrderQuery orderQuery;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final static Map<PaymentMode, TradeType> PAY_TABLE
            = ImmutableMap.of(PaymentMode.ALIPAY, TradeType.ALIPAY,
            PaymentMode.WEIXIN_MINIPROGRAM, TradeType.MINIPROGRAM,
            PaymentMode.WEIXIN_APP, TradeType.APP);

    @Autowired
    public EpsQueryAdapter(OrderQuery orderQuery) {
        this.orderQuery = orderQuery;
    }

    @Override
    public Optional<EpsPaymentQueryRes> query(PaidAble paidAble) {
        final PaymentMode payType = paidAble.getPayType();
        final TradeType tradeType = PAY_TABLE.get(payType);
        if (tradeType == null) {
            logger.error("支付类型 {} 不支持", payType);
            return Optional.empty();
        }
        final Map<String, String> res = orderQuery.queryByEps(paidAble, tradeType);
        final String status = res.get("status");
        final String resultCode = res.get("result_code");

        final String payNo = paidAble.getPayNo();
        if (StringUtils.isBlank(status) || StringUtils.isBlank(resultCode)) {
            logger.error("订单接口查询失败，请求单号[" + payNo + "]");
            return Optional.empty();
        }

        final String tradeStatus = res.get("trade_state");
        final String transactionId = res.get("transaction_id");
        final String openId = res.get("openid");
        final String totalFee = res.get("total_fee");
        final String paymentId = res.get("payment_id");

        return Optional.of(new EpsPaymentQueryRes(payNo,
                tradeStatus,
                transactionId,
                paidAble.getPayType(),
                openId,
                openId,
                totalFee,
                res.toString(),
                paymentId,
                resultCode));
    }
}
