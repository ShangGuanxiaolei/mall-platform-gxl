package com.xquark.service.outpay;

import java.util.List;

import com.xquark.dal.model.OutpayAgreement;
import com.xquark.dal.model.UserPayBankVO;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.BaseEntityService;

public interface OutPayAgreementService extends BaseEntityService<OutpayAgreement> {

  /**
   * 根据主键获得
   */
  OutpayAgreement load(String id);

  /**
   * 插入
   */
  int insert(OutpayAgreement snapshot);

  /**
   * 获取用户的协议银行
   */
  List<OutpayAgreement> listByUserId(String userId);

  /**
   * 解绑
   */
  int unbind(String id);

  OutpayAgreement loadBypayAgreeId(String userId, String payAgreeId, PaymentMode payMode);

  /**
   * 获取用户的签约银行记录
   */
  List<UserPayBankVO> listBankByUserId(String userId);

}
