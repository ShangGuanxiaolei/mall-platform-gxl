package com.xquark.service.outpay.impl.tenpay;

import static com.xquark.service.outpay.impl.TenPaymentImpl.notify_mini_url;
import static com.xquark.wechat.util.WeChatUtil.createSign;

import com.google.common.collect.ImmutableMap;
import com.xquark.dal.mapper.PaymentSwitchConfigMapper;
import com.xquark.dal.model.User;
import com.xquark.dal.model.pay.PaymentConfig;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.outpay.PaymentResponseClientVO;
import com.xquark.service.outpay.impl.PaymentFacade;
import com.xquark.service.wechat.WechatService;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

/**
 * Created by wangxinhua. Date: 2018/8/27 Time: 下午7:23
 */
@Service
public class MiniTenpaySignService extends BaseTenpaySignService {


  @Autowired
  protected MiniTenpaySignService(PaymentFacade paymentFacade,
                                  RestOperations restTemplate, WechatService wechatService, PaymentSwitchConfigMapper paymentSwitchConfigMapper) {
    super(paymentFacade, restTemplate, wechatService, paymentSwitchConfigMapper);
  }

  @Override
  protected Map<String, String> buildParams(PaymentRequestVO pay, PaymentConfig paymentConfig,
      User payUser) {
    String appId = paymentConfig.getAppId();
    return ImmutableMap.<String, String>builder()
        .put("appid", appId)
        .put("mch_id", paymentConfig.getMchId())
        .put("nonce_str", CommonUtil.CreateNoncestr())
        .put("body", pay.getTradeNo())
        .put("out_trade_no", pay.getTradeNo())
        .put("total_fee", pay.getTotalFee().multiply(new BigDecimal(100)).intValue() + "")
        .put("spbill_create_ip", StringUtils
            .defaultIfBlank(pay.getRemoteHost(), "127.0.0.1"))
        .put("notify_url", paymentConfig.getNotifySite() + notify_mini_url)

        // TODO: neeed confirm trade_type = "NATIVE" is not used by others
        .put("trade_type", "JSAPI")
        .put("openid", payUser.getLoginname())
        .build();
  }

  @Override
  protected Map<String, String> handleRes(PaymentRequestVO pay,
      PaymentConfig paymentConfig,
      Map<String, String> res) {
    if (res != null) {
      if (!"SUCCESS".equals(res.get("result_code"))) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            res.get("return_msg"));
      }
    }

    Map<String, String> payInfo = ImmutableMap.of(
        "app_id", res.get("appid"),
        "prepay_id", res.get("prepay_id"),
        "app_key", paymentConfig.getMchSecret(),
        "ticket", obtainJsapiTicket(),
        "trade_url", pay.getTradeUrl()
    );

    PaymentResponseClientVO response = new PaymentResponseClientVO(pay.getTradeNo(),
        res.get("prepay_id"),
        pay.getTotalFee(),
        pay.getOrderIds(), PaymentMode.WEIXIN_MINIPROGRAM);
    response.setPayInfo(payInfo);

    String app_id = response.getPayInfo().get("app_id");
    String app_key = response.getPayInfo().get("app_key");
    String trade_url = response.getPayInfo().get("trade_url");

    String packageStr = "prepay_id=" + response.getPayInfo().get("prepay_id");

    String timestamp = Long.toString(new Date().getTime() / 1000);

    String noncestr = CommonUtil.CreateNoncestr();

    String prepay_id = response.getPayInfo().get("prepay_id");

    SortedMap<Object, Object> params = new TreeMap<>();
    params.put("appId", app_id);
    params.put("timeStamp", timestamp);
    params.put("nonceStr", noncestr);

    params.put("package", "prepay_id=" + prepay_id);
    params.put("signType", "MD5");

    String paySign = createSign(app_key, "UTF-8", params);

    params.put("packageValue", "prepay_id=" + prepay_id);

    Map<String, String> map = new HashMap<>();
    map.put("appId", app_id);
    map.put("timeStamp", timestamp);
    map.put("nonceStr", noncestr);
    map.put("package", packageStr);
    map.put("signType", "MD5");
    map.put("paySign", paySign);
    map.put("trade_url", trade_url);
    return map;
  }

}
