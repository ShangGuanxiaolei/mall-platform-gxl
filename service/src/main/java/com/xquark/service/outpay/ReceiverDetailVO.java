package com.xquark.service.outpay;

import java.math.BigDecimal;

public class ReceiverDetailVO {

  private String swiftNum;
  private String receiverAccount;
  private String receiverName;
  private BigDecimal receiverFee;
  private String receiverRemark;
  private String status;
  private String msg;
  private String alipaySwiftNum;
  private String payTime;
  private String payAccount;
  private String payAccountId;
  private String payCharges;
  private String payStatus;
  private String orderNo;

  public String getSwiftNum() {
    return swiftNum;
  }

  public void setSwiftNum(String swiftNum) {
    this.swiftNum = swiftNum;
  }

  public String getReceiverAccount() {
    return receiverAccount;
  }

  public void setReceiverAccount(String receiverAccount) {
    this.receiverAccount = receiverAccount;
  }

  public String getReceiverName() {
    return receiverName;
  }

  public void setReceiverName(String receiverName) {
    this.receiverName = receiverName;
  }

  public BigDecimal getReceiverFee() {
    return receiverFee;
  }

  public void setReceiverFee(BigDecimal receiverFee) {
    this.receiverFee = receiverFee;
  }

  public String getReceiverRemark() {
    return receiverRemark;
  }

  public void setReceiverRemark(String receiverRemark) {
    this.receiverRemark = receiverRemark;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public String getAlipaySwiftNum() {
    return alipaySwiftNum;
  }

  public void setAlipaySwiftNum(String alipaySwiftNum) {
    this.alipaySwiftNum = alipaySwiftNum;
  }

  public String getPayTime() {
    return payTime;
  }

  public void setPayTime(String payTime) {
    this.payTime = payTime;
  }

  public String getPayAccount() {
    return payAccount;
  }

  public void setPayAccount(String payAccount) {
    this.payAccount = payAccount;
  }

  public String getPayAccountId() {
    return payAccountId;
  }

  public void setPayAccountId(String payAccountId) {
    this.payAccountId = payAccountId;
  }

  public String getPayCharges() {
    return payCharges;
  }

  public void setPayCharges(String payCharges) {
    this.payCharges = payCharges;
  }

  public String getPayStatus() {
    return payStatus;
  }

  public void setPayStatus(String payStatus) {
    this.payStatus = payStatus;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

}
