package com.xquark.service.outpay.impl.tenpay;

import static com.xquark.service.outpay.impl.TenPaymentImpl.notify_app_url;
import static com.xquark.wechat.util.WeChatUtil.createSign;

import com.google.common.collect.ImmutableMap;
import com.xquark.dal.mapper.PaymentSwitchConfigMapper;
import com.xquark.dal.model.User;
import com.xquark.dal.model.pay.PaymentConfig;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.outpay.impl.PaymentFacade;
import com.xquark.service.wechat.WechatService;
import java.math.BigDecimal;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

/**
 * Created by wangxinhua. Date: 2018/8/28 Time: 下午6:59
 */
@Service
public class AppTenpaySignService extends BaseTenpaySignService {

  @Autowired
  protected AppTenpaySignService(PaymentFacade paymentFacade,
      RestOperations restTemplate,
      WechatService wechatService,
      PaymentSwitchConfigMapper paymentSwitchConfigMapper) {
    super(paymentFacade, restTemplate, wechatService, paymentSwitchConfigMapper);
  }

  @Override
  protected Map<String, String> buildParams(PaymentRequestVO pay, PaymentConfig paymentConfig,
      User payUser) {
    return ImmutableMap.<String, String>builder()
        .put("appid", paymentConfig.getAppId())
        .put("mch_id", paymentConfig.getMchId())
        .put("nonce_str", CommonUtil.CreateNoncestr())
        .put("body", pay.getTradeNo())
        .put("out_trade_no", pay.getTradeNo())
        .put("total_fee", pay.getTotalFee().multiply(new BigDecimal(100)).intValue() + "")
        .put("spbill_create_ip", StringUtils
            .defaultIfBlank(pay.getRemoteHost(), "127.0.0.1"))
        .put("notify_url", paymentConfig.getNotifySite() + notify_app_url)
        .put("trade_type", "APP")
        .build();
  }

  @Override
  protected Map<String, String> handleRes(PaymentRequestVO pay, PaymentConfig paymentConfig,
      Map<String, String> res) {
    if (res != null) {
      if (!"SUCCESS".equals(res.get("result_code"))) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            res.get("return_msg"));
      }
    }

    String prepayId = res.get("prepay_id");
    String timestamp = Long.toString(System.currentTimeMillis() / 1000);

    Map<String, String> ret = new TreeMap<>();
    ret.put("appid", paymentConfig.getAppId());
    ret.put("partnerid", paymentConfig.getMchId());
    ret.put("prepayid", prepayId);
    ret.put("package", "Sign=WXPay");
    ret.put("noncestr", CommonUtil.CreateNoncestr());
    ret.put("timestamp", timestamp);
    String paySign = createSign(paymentConfig.getMchSecret(), "UTF-8", ret);
    ret.put("sign", paySign);
    // TODO：增加返回订单编号
    ret.put("bizNo", pay.getTradeNo());
    return ret;
  }

}
