package com.xquark.service.outpay.impl;

import com.alibaba.fastjson.JSON;
import com.xquark.dal.mapper.WechatAppConfigMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.model.pay.PaymentConfig;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.*;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.bank.WithdrawApplyService;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.error.PayNotifyException;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderRefundService;
import com.xquark.service.outpay.*;
import com.xquark.service.outpay.impl.tenpay.CommonUtil;
import com.xquark.service.outpay.impl.tenpay.MD5SignUtil;
import com.xquark.service.outpay.impl.tenpay.SDKRuntimeException;
import com.xquark.service.outpay.impl.tenpay.XMLUtil;
import com.xquark.service.pay.OutPayService;
import com.xquark.service.pay.PayRequestApiService;
import com.xquark.service.pay.PayRequestService;
import com.xquark.service.pay.model.PaymentResponseVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.user.UserService;
import com.xquark.service.wechat.WechatService;
import com.xquark.utils.EncryptUtil;
import com.xquark.utils.http.PoolingHttpClients;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.util.*;

@Component("tenPayment")
public class TenPaymentImpl extends AbstractThirdPartyService {

  private Logger tenpayLogger = OutPayLogs.tenpayLogger;

  @Value("${site.webapi.host.name}")
  String siteHost;

  @Autowired
  private PaymentFacade paymentFacade;

  @Autowired
  private OrderQuery orderQuery;

  private MainOrderService mainOrderService;

  @Autowired
  public void setMainOrderService(MainOrderService mainOrderService) {
    this.mainOrderService = mainOrderService;
  }

  @Autowired
  private WechatAppConfigMapper wechatAppConfigMapper;

  @Autowired
  private WechatService wechatService;

  @Autowired
  private ProductService productService;

  @Autowired
  WithdrawApplyService withdrawApplyService;

  @Autowired
  UserService userService;

  @Autowired
  private OutPayService outPayService;

  @Autowired
  PayRequestService payRequestService;

  @Autowired
  private PayRequestApiService payRequestApiService;

  @Autowired
  private AccountApiService accountApiService;

  @Value("${profiles.active}")
  String profile;

  @Value("${wechat.pay.notify.test.name}")
  String testNotifyHost;

  @Autowired
  private CashierService cashierService;

  @Autowired
  private OrderRefundService orderRefundService;

  public static final String notify_url = "/pay/tenpay/notify";

  public static final String notify_app_url = "/pay/tenpayApp/notify";

  public static final String notify_mini_url = "/pay/tenpayMiniProgram/notify";

  private static final String EPS_WEIXIN_URL = "https://pay.xrtpay.com:10443/xrtpay/gateway";

  private static final String WEINXIN_REFUND_URL = "https://api.mch.weixin.qq.com/secapi/pay/refund";

  private final static String CHARSET = "UTF-8";

  @Override
  public PaymentResponseVO payRequest(HttpServletRequest req,
                                      HttpServletResponse resp, PaymentRequestVO payRequest) {
    return null;
  }

  @Override
  public String createPayRequestUrl(HttpServletRequest req,
                                    HttpServletResponse resp, PaymentRequestVO payRequest) {
    try {
      String url = null;//createUrl(payRequest.getTradeNo());
      return url;
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "创建微信支付请求url失败", e);
    }
  }

  @Override
  public PaymentResponseVO payCallback(HttpServletRequest req,
                                       HttpServletResponse resp) {

    return null;
  }

  @SuppressWarnings("unchecked")
  public PaymentResponseVO payMiniNotify(HttpServletRequest req,
                                         HttpServletResponse resp) {

    String wxNotify = (String) req.getAttribute("wxNotify");
    Map<String, String> map;
    try {
      map = XMLUtil.doXMLParse(wxNotify);

      if (map != null) {

        tenpayLogger.info("tenpay notity " + map);

        WechatAppConfig wechatAppConfig = wechatAppConfigMapper
                .selectByProfile("test");
        tenpayLogger.info("paymentMerchant payNo=[" + map.get("out_trade_no") + "]"
                + wechatAppConfig);

        if ("0000".equals(map.get("result_code")) || "SUCCESS".equals(map.get("result_code"))) {
          PaymentResponseVO p = new PaymentResponseVO();
          String total_fee = map.get("total_fee");
          p.setPaymentMode(PaymentMode.WEIXIN_MINIPROGRAM);

          // 商户订单号
          p.setBillNo(map.get("out_trade_no"));
          // 财付通订单号
          p.setBillNoPay(map.get("transaction_id"));
          // 交易状态
          p.setBillStatus(PaymentStatus.SUCCESS);
          p.setBuyerEmail(map.get("openid"));
          p.setBuyerId(map.get("openid"));
          p.setTotalFee(new BigDecimal(total_fee).divide(new BigDecimal(100))
                  .setScale(2, BigDecimal.ROUND_DOWN).toString());
          p.setDetail(wxNotify);
          p.setBillStatus(PaymentStatus.SUCCESS);
          p.setPaymentMerchantId(wechatAppConfig.getId());
          cashierService.handlePaymentResponse(p.getBillNo(), PaymentMode.WEIXIN_MINIPROGRAM, p);
        }
      }
    } catch (Exception e) {
	  if(e instanceof PayNotifyException) {
    	  PayNotifyException ep = (PayNotifyException) e;
    	  throw new PayNotifyException(ep.getCode(),ep.getMsg(),ep.getData());
      }
      log.error("签名出错", e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }

    return null;
  }

  @SuppressWarnings("unchecked")
  @Override
  public PaymentResponseVO payNotify(HttpServletRequest req,
                                     HttpServletResponse resp) {

    String wxNotify = (String) req.getAttribute("wxNotify");
    Map<String, String> map;
    try {
      map = XMLUtil.doXMLParse(wxNotify);
      if (map != null) {
        tenpayLogger.info("tenpay notity " + map);

        WechatAppConfig wechatAppConfig = wechatAppConfigMapper
                .selectByAppId(map.get("appid"));
        tenpayLogger.info("paymentMerchant payNo=[" + map.get("out_trade_no") + "]"
                + wechatAppConfig);

        if ("SUCCESS".equals(map.get("result_code"))) {
          String notifySign = map.get("sign");
          map.remove("sign");
          String sign = MD5SignUtil.Sign(CommonUtil.FormatQueryParaMap(map, false),
                  wechatAppConfig.getMchKey());
          if (!sign.equals(notifySign)) {
            log.error("微信签名错误", map);
          }

          PaymentResponseVO p = new PaymentResponseVO();
          String total_fee = map.get("cash_fee");
          p.setPaymentMode(PaymentMode.WEIXIN);

          // 商户订单号
          p.setBillNo(map.get("out_trade_no"));
          // 财付通订单号
          p.setBillNoPay(map.get("transaction_id"));
          // 交易状态
          p.setBillStatus(PaymentStatus.SUCCESS);
          p.setBuyerEmail(map.get("openid"));
          p.setBuyerId(map.get("openid"));
          p.setTotalFee(new BigDecimal(total_fee).divide(new BigDecimal(100))
                  .setScale(2, BigDecimal.ROUND_DOWN).toString());
          p.setDetail(wxNotify);
          p.setBillStatus(PaymentStatus.SUCCESS);
          p.setPaymentMerchantId(wechatAppConfig.getId());
          cashierService.handlePaymentResponse(p.getBillNo(), PaymentMode.WEIXIN, p);
        }
      }
    } catch (Exception e) {
      log.error("签名出错", e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }

    return null;
  }

  @SuppressWarnings("unchecked")
  public PaymentResponseVO payAppNotify(HttpServletRequest req,
                                        HttpServletResponse resp) {

    String wxNotify = (String) req.getAttribute("wxNotify");

    Map<String, String> map;
    try {
      map = XMLUtil.doXMLParse(wxNotify);

      if (map != null) {

        tenpayLogger.info("tenpay notity " + map);

        WechatAppConfig wechatAppConfig = wechatAppConfigMapper
                .selectByAppAppId(req.getAttribute("appid").toString());
        tenpayLogger.info("paymentMerchant payNo=[" + map.get("out_trade_no") + "]"
                + wechatAppConfig);

        if ("0".equals(map.get("result_code")) || "SUCCESS".equals(map.get("result_code"))) {
          PaymentResponseVO p = new PaymentResponseVO();
          String total_fee = map.get("total_fee");
          p.setPaymentMode(PaymentMode.WEIXIN_APP);

          // 商户订单号
          p.setBillNo(map.get("out_trade_no"));
          // 财付通订单号
          p.setBillNoPay(map.get("transaction_id"));
          // 交易状态
          p.setBillStatus(PaymentStatus.SUCCESS);
          p.setBuyerEmail(map.get("openid"));
          p.setBuyerId(map.get("openid"));
          p.setTotalFee(new BigDecimal(total_fee).divide(new BigDecimal(100))
                  .setScale(2, BigDecimal.ROUND_DOWN).toString());
          p.setDetail(wxNotify);
          p.setBillStatus(PaymentStatus.SUCCESS);
          p.setPaymentMerchantId(wechatAppConfig.getId());
          cashierService.handlePaymentResponse(p.getBillNo(), PaymentMode.WEIXIN_APP, p);
        }
      }
    } catch (Exception e) {
      if(e instanceof PayNotifyException) {
    	  PayNotifyException ep = (PayNotifyException) e;
    	  throw new PayNotifyException(ep.getCode(),ep.getMsg(),ep.getData());
      }
      log.error("签名出错", e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }

    return null;
  }

  @Override
  public List<String> loadBankList() {
    return new ArrayList<String>();
  }

  @Override
  public void payBatchRequest(HttpServletRequest req,
                              HttpServletResponse resp, List<ReceiverDetailVO> maps) {
    // TODO Auto-generated method stub

  }

  @Override
  public Map<String, String> payBatchNotify(HttpServletRequest req,
                                            HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void refundBatchRequest(HttpServletRequest req,
                                 HttpServletResponse resp, List<ReceiverDetailVO> maps)
          throws Exception {
    // TODO Auto-generated method stub

  }

  @Override
  public Map<String, String> refundBatchNotify(HttpServletRequest req,
                                               HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payRefund(HttpServletRequest req,
                                     HttpServletResponse resp, PaymentRequestVO request)
          throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payRefund(PaymentRequestVO request, Order order) throws Exception {
    return null;
  }

  @Override
  public PaymentResponseVO payCancel(HttpServletRequest req,
                                     HttpServletResponse resp, PaymentRequestVO request)
          throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payRefundNotify(HttpServletRequest req,
                                           HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payConfirm(HttpServletRequest req,
                                      HttpServletResponse resp, PaymentRequestVO request) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean sendSmsAgain(PaymentRequestVO request) throws Exception {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void closeTrade(String orderNo) {

  }

  @Override
  public void tradeAccountReport(String startTime, String endTime) {

  }

  @Override
  public void payLock(String couponId) {

  }

  @Override
  public void payUnLock(String couponId) {

  }

  @Override
  @SuppressWarnings("rawtypes")
  public PaymentResponseClientVO signMiniProgram(PaymentRequestVO pay) {
    //TODO extract
    StringHttpMessageConverter httpMessageConverter = new StringHttpMessageConverter(
            Charset.forName(CHARSET));
    List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
    converters.add(httpMessageConverter);
    RestTemplate restTemplate = new RestTemplate(converters);

    String url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
    String prepayid = "";
    String noncestr = CommonUtil.CreateNoncestr();

    //FIXME  for getting root shopId
    String cashierItemId = pay.getCashierItemId();
    CashierItem cashierItem = cashierService.load(cashierItemId);
    String rootShopId = productService.load(cashierItem.getProductId()).getShopId();
    WechatAppConfig wechatAppConfig = wechatAppConfigMapper.selectByShopId(rootShopId);

    String appID = wechatAppConfig.getAppMiniId();
    String mchSecret = wechatAppConfig.getMiniMchKey();
    String merchantId = wechatAppConfig.getMiniMchId();

    String notifySite;
    if (profile.toLowerCase().compareTo("dev") == 0) {
      notifySite = testNotifyHost;
    } else {
      notifySite = siteHost;
    }

    String ticket = wechatService.obtainJsapiTicket(wechatAppConfig);

    String strOpenid = pay.getOpenId(); //(String) openMap.get("openid");

    Map<String, String> paramsMap = new HashMap<String, String>();
    paramsMap.put("appid", appID);
    paramsMap.put("mch_id", merchantId);
    paramsMap.put("nonce_str", noncestr);
    paramsMap.put("body", pay.getTradeNo());
    paramsMap.put("out_trade_no", pay.getTradeNo());
    paramsMap.put("total_fee", pay.getTotalFee().multiply(new BigDecimal(100)).intValue() + "");
    paramsMap.put("spbill_create_ip", StringUtils
            .defaultIfBlank(pay.getRemoteHost(), "127.0.0.1"));// req.getRemoteHost());
    // TODO 内外网微信支付回调穿透，注释2018-9-23 15：44
    paramsMap.put("notify_url", notifySite + notify_mini_url);

    // TODO: neeed confirm trade_type = "NATIVE" is not used by others
    paramsMap.put("trade_type", "JSAPI");
    paramsMap.put("openid", strOpenid);

    String sign = null;
    try {
      sign = MD5SignUtil.Sign(CommonUtil.FormatQueryParaMap(paramsMap, false), mchSecret);
      paramsMap.put("sign", sign);
    } catch (SDKRuntimeException e) {
      tenpayLogger.error("weixin sign failed error pay=" + pay + "  paramsMap=" + paramsMap);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }
    tenpayLogger.info("=============================" + XMLUtil.toXml(paramsMap));
    String respStr = restTemplate.postForObject(url, XMLUtil.toXml(paramsMap), String.class);
    tenpayLogger.info("=============================OKOKOKOKOKOKOK");

    Map<String, String> map = null;
    try {
      map = XMLUtil.doXMLParse(respStr);
      if (map != null) {
        if ("SUCCESS".equals(map.get("result_code"))) {
          prepayid = (String) map.get("prepay_id");
        } else {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                  (String) map.get("return_msg"));
        }
      }
    } catch (Exception e) {
      tenpayLogger.error("weixin parse xml failed error pay=" + pay + "  map=" + map);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }
    PaymentResponseClientVO response = new PaymentResponseClientVO(pay.getTradeNo(), prepayid,
            pay.getTotalFee(),
            pay.getOrderIds(), PaymentMode.WEIXIN_MINIPROGRAM);
    Map<String, String> payInfo = new HashMap<String, String>();
    payInfo.put("app_id", map.get("appid"));
    payInfo.put("prepay_id", map.get("prepay_id"));
    payInfo.put("app_key", mchSecret);
    payInfo.put("ticket", ticket);
    payInfo.put("trade_url", pay.getTradeUrl());

    response.setPayInfo(payInfo);

    tenpayLogger.info("tenpay signed payNo=[" + pay.getTradeNo() + "]" + response);

    return response;
  }

  @Override
  @SuppressWarnings("rawtypes")
  public PaymentResponseClientVO sign(PaymentRequestVO pay) {
    //TODO extract
    StringHttpMessageConverter httpMessageConverter = new StringHttpMessageConverter(
            Charset.forName(CHARSET));
    List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
    converters.add(httpMessageConverter);
    RestTemplate restTemplate = new RestTemplate(converters);

    String url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
    String prepayid = "";
    String noncestr = CommonUtil.CreateNoncestr();

    //FIXME  for getting root shopId
    String cashierItemId = pay.getCashierItemId();
    CashierItem cashierItem = cashierService.load(cashierItemId);
    String rootShopId = productService.load(cashierItem.getProductId()).getShopId();
    WechatAppConfig wechatAppConfig = wechatAppConfigMapper.selectByShopId(rootShopId);
    String appID = wechatAppConfig.getAppId();
    String mchSecret = wechatAppConfig.getMchKey();
    String merchantId = wechatAppConfig.getMchId();

    String notifySite;
    if (StringUtils.equalsIgnoreCase("dev", profile) || StringUtils.equalsIgnoreCase("uat", profile)) {
      notifySite = testNotifyHost;
    } else {
      notifySite = siteHost;
    }

    String ticket = wechatService.obtainJsapiTicket(wechatAppConfig);

    String strOpenid = pay.getOpenId(); //(String) openMap.get("openid");

    Map<String, String> paramsMap = new HashMap<String, String>();
    paramsMap.put("appid", appID);
    paramsMap.put("mch_id", merchantId);
    paramsMap.put("nonce_str", noncestr);
    paramsMap.put("body", pay.getTradeNo());
    paramsMap.put("out_trade_no", pay.getTradeNo());
    paramsMap.put("total_fee", pay.getTotalFee().multiply(new BigDecimal(100)).intValue() + "");
    paramsMap.put("spbill_create_ip", StringUtils
            .defaultIfBlank(pay.getRemoteHost(), "127.0.0.1"));// req.getRemoteHost());

    paramsMap.put("notify_url", notifySite + notify_url);

    // TODO: neeed confirm trade_type = "NATIVE" is not used by others
    paramsMap.put("trade_type", "JSAPI");
    paramsMap.put("openid", strOpenid);

    String sign;
    try {
      sign = MD5SignUtil.Sign(CommonUtil.FormatQueryParaMap(paramsMap, false), mchSecret);
      paramsMap.put("sign", sign);
    } catch (SDKRuntimeException e) {
      tenpayLogger.error("weixin sign failed error pay=" + pay + "  paramsMap=" + paramsMap);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }
    String respStr = restTemplate.postForObject(url, XMLUtil.toXml(paramsMap), String.class);

    Map<String, String> map = null;
    try {
      map = XMLUtil.doXMLParse(respStr);
      if (map != null) {
        if ("SUCCESS".equals(map.get("result_code"))) {
          prepayid = map.get("prepay_id");
        } else {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                  map.get("return_msg"));
        }
      }
    } catch (Exception e) {
      tenpayLogger.error("weixin parse xml failed error pay=" + pay + "  map=" + map);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }
    PaymentResponseClientVO response = new PaymentResponseClientVO(pay.getTradeNo(), prepayid,
            pay.getTotalFee(),
            pay.getOrderIds(), PaymentMode.WEIXIN);
    Map<String, String> payInfo = new HashMap<>();
    payInfo.put("app_id", map.get("appid"));
    payInfo.put("prepay_id", map.get("prepay_id"));
    payInfo.put("app_key", mchSecret);
    payInfo.put("ticket", ticket);
    payInfo.put("trade_url", pay.getTradeUrl());

    response.setPayInfo(payInfo);

    tenpayLogger.info("tenpay signed payNo=[" + pay.getTradeNo() + "]" + response);

    return response;
  }

  /**
   * app内微信支付统一下单
   * 使用 {@link PaymentFacade#pay(PaymentRequestVO, User)} 统一处理支付签名
   */
  @Deprecated
  @Override
  public PaymentResponseClientVO signApp(PaymentRequestVO pay) {
    //TODO extract
    StringHttpMessageConverter httpMessageConverter = new StringHttpMessageConverter(
            Charset.forName(CHARSET));
    List<HttpMessageConverter<?>> converters = new ArrayList<>();
    converters.add(httpMessageConverter);
    RestTemplate restTemplate = new RestTemplate(converters);

    String url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
    String prepayid = "";
    String noncestr = CommonUtil.CreateNoncestr();

    //FIXME  for getting root shopId
    String cashierItemId = pay.getCashierItemId();
    CashierItem cashierItem = cashierService.load(cashierItemId);
    String rootShopId = productService.load(cashierItem.getProductId()).getShopId();
    WechatAppConfig wechatAppConfig = wechatAppConfigMapper.selectByShopId(rootShopId);
    String appID = wechatAppConfig.getAppAppId();
    String mchSecret = wechatAppConfig.getAppMchKey();
    String merchantId = wechatAppConfig.getAppMchId();

    String notifySite;
    if (profile.toLowerCase().compareTo("dev") == 0) {
      notifySite = testNotifyHost;
    } else {
      notifySite = siteHost;
    }

    Map<String, String> paramsMap = new HashMap<String, String>();
    paramsMap.put("appid", appID);
    paramsMap.put("mch_id", merchantId);
    paramsMap.put("nonce_str", noncestr);
    paramsMap.put("body", pay.getTradeNo());
    paramsMap.put("out_trade_no", pay.getTradeNo());
    paramsMap.put("total_fee", pay.getTotalFee().multiply(new BigDecimal(100)).intValue() + "");
    paramsMap.put("spbill_create_ip", StringUtils
            .defaultIfBlank(pay.getRemoteHost(), "127.0.0.1"));// req.getRemoteHost());
    paramsMap.put("notify_url", notifySite + notify_app_url);
    paramsMap.put("trade_type", "APP");

    String sign = null;
    try {
      sign = MD5SignUtil.Sign(CommonUtil.FormatQueryParaMap(paramsMap, false), mchSecret);
      paramsMap.put("sign", sign);
    } catch (SDKRuntimeException e) {
      tenpayLogger.error("app weixin app sign failed error pay=" + pay + "  paramsMap="
              + paramsMap);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "app生成支付信息出错");
    }

    String respStr = restTemplate.postForObject(url, XMLUtil.toXml(paramsMap), String.class);

    Map map = null;
    try {
      map = XMLUtil.doXMLParse(respStr);
      if (map != null) {
        if ("SUCCESS".equals(map.get("result_code"))) {
          prepayid = (String) map.get("prepay_id");
        } else {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                  (String) map.get("return_msg"));
        }
      }
    } catch (Exception e) {
      tenpayLogger.error("app weixin parse xml failed error pay=" + pay + "  map=" + map);
    }

    PaymentResponseClientVO response = new PaymentResponseClientVO(pay.getTradeNo(), prepayid,
            pay.getTotalFee(),
            pay.getOrderIds(), PaymentMode.WEIXIN_APP);
    Map<String, String> payInfo = new HashMap<String, String>();
    payInfo.put("appid", appID);
    payInfo.put("appkey", mchSecret);
    payInfo.put("partnerid", merchantId);
    payInfo.put("prepayid", prepayid);
    payInfo.put("package", "Sign=WXPay");
    payInfo.put("trade_url", (String) pay.getTradeUrl());
    response.setPayInfo(payInfo);

    tenpayLogger.info("app tenpay signed payNo=[" + pay.getTradeNo() + "]" + response);

    return response;
  }

  @Override
  public PaymentResponseVO payNotifyApp(HttpServletRequest req,
                                        HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * 微信退款
   */
  public void refund(Order order) {
    // 退款接口默认使用未结算资金退款，当账户没有未结算资金就无法退款，因此后面会有判断如果是未结算资金就无法退款再调用一次使用可用余额进行退款
    refundWay(order, "REFUND_SOURCE_UNSETTLED_FUNDS");
  }

  /**
   * 通过指定的退款资金来源进行退款
   */
  private void refundWay(Order order, String way) {
    log.info("start execute wechat refund refundWay()方法, 当前订单号 orderNo={}, 支付单号 payNo={}, 支付类型 payType={}, 退款方式={}",
            order.getOrderNo(), order.getPayNo(), order.getPayType(), way);
    String rootShopId = order.getRootShopId();
    if (null == rootShopId) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "rootShopId为空");
    }

    WechatAppConfig wechatAppConfig = wechatAppConfigMapper.selectByShopId(rootShopId);
    if (null == wechatAppConfig) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "微信支付配置为空");
    }

    PaymentMode mode = order.getPayType();
    if (null == mode) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "支付类型为空");
    }

    String appID = "";
    String mchSecret = "";
    String merchantId = "";

    boolean result = orderQuery.sign(order, mode == PaymentMode.WEIXIN_APP ? TradeType.APP : TradeType.MINIPROGRAM);
    if (!result) {//根据微信订单查询接口返回判断使用哪个配置退款
      PaymentConfig paymentConfig;
      if (mode == PaymentMode.WEIXIN) {
        paymentConfig = paymentFacade.loadPayment(profile, PaymentMode.EPS, TradeType.WEB.toString());
        mchSecret = paymentConfig.getMchSecret();
        merchantId = paymentConfig.getMchId();
      } else if (mode == PaymentMode.WEIXIN_APP) {
        paymentConfig = paymentFacade.loadPayment(profile, PaymentMode.EPS, TradeType.APP.toString());
        mchSecret = paymentConfig.getMchSecret();
        merchantId = paymentConfig.getMchId();
      } else if (mode == PaymentMode.WEIXIN_MINIPROGRAM) {
        paymentConfig = paymentFacade.loadPayment(profile, PaymentMode.EPS, TradeType.MINIPROGRAM.toString());
        mchSecret = paymentConfig.getMchSecret();
        merchantId = paymentConfig.getMchId();
      }
    } else {
      if (mode == PaymentMode.WEIXIN) {
        appID = wechatAppConfig.getAppId();
        mchSecret = wechatAppConfig.getMchKey();
        merchantId = wechatAppConfig.getMchId();
      } else if (mode == PaymentMode.WEIXIN_APP) {
        appID = wechatAppConfig.getAppAppId();
        mchSecret = wechatAppConfig.getAppMchKey();
        merchantId = wechatAppConfig.getAppMchId();
      } else if (mode == PaymentMode.WEIXIN_MINIPROGRAM) {
        appID = wechatAppConfig.getAppMiniId();
        mchSecret = wechatAppConfig.getMiniMchKey();
        merchantId = wechatAppConfig.getMiniMchId();
      }
    }

    //获取整个主订单的消费金额
    Optional<MainOrder> mainOrderOptional = Optional.ofNullable(mainOrderService.load(order.getMainOrderId()));
    if (!mainOrderOptional.isPresent() || null == mainOrderOptional.get().getTotalFee()) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "订单金额格式不对");
    }

    Map<String, String> params = new HashMap<>();
    String nonceStr = CommonUtil.CreateNoncestr();

    if (!result) {
      params.put("service", "unified.trade.refund");  //接口类型
      params.put("mch_id", merchantId); //商户号
      params.put("out_trade_no", order.getPayNo()); //填入商家订单号
      params.put("out_refund_no", order.getOrderNo());  //填入退款订单号
      params.put("total_fee", "" + mainOrderOptional.get().getTotalFee().multiply(new BigDecimal(100)).intValue());//填入总金额
      params.put("refund_fee", "" + order.getRefundFee().multiply(new BigDecimal(100)).intValue());//填入退款金额
      params.put("op_user_id", merchantId); //操作员Id，默认就是商户号
      params.put("nonce_str", nonceStr);  //随机字符串

      try {
        String queryParaMap = CommonUtil.FormatQueryParaMap(params, false);
        params.put("sign", MD5SignUtil.Sign(queryParaMap, mchSecret));
      } catch (SDKRuntimeException e) {
        tenpayLogger.error("weixin refund sign failed error pay=" + order.getPayNo() + "  paramsMap=" + params);
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "生成退款支付签名信息出错");
      }

      log.info("wechat refundWay(), active query result={}, request params={}", result, params);

      try {
        String postEpsSSL = PoolingHttpClients.postEpsSSL(EPS_WEIXIN_URL, XMLUtil.toXml(params));
        if (postEpsSSL != null) {
          Map map = XMLUtil.doXMLParse(postEpsSSL);
          if (map != null) {
            tenpayLogger.info("EPS微信退款返回map:{}", JSON.toJSONString(map));

            String status = (String) map.get("status");
            String resultCode = (String) map.get("result_code");
            if ((StringUtils.isBlank(status) || StringUtils.isBlank(resultCode))) {
              throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "EPS微信退款返回错误");
            }

            if (("0".equals(status) && "0".equals(resultCode))) {
              orderRefundService.successByOrderId(order.getId());//修改退款订单状态为成功
            }
          }
        }
      } catch (Exception e) {
        tenpayLogger.error("weixin refund keyStore failed error pay=" + order.getPayNo() + "  paramsMap=" + params);
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "生成退款信息出错");
      }
    } else {
      params.put("appid", appID); //公众账号ID
      params.put("mch_id", merchantId); //商户号
      params.put("out_trade_no", order.getPayNo());   //填入商家订单号
      params.put("out_refund_no", order.getOrderNo()); //填入退款订单号
      params.put("total_fee", "" + mainOrderOptional.get().getTotalFee().multiply(new BigDecimal(100)).intValue());//填入总金额
      params.put("refund_fee", "" + order.getRefundFee().multiply(new BigDecimal(100)).intValue()); //填入退款金额
      params.put("op_user_id", merchantId);   //操作员Id，默认就是商户号
      params.put("nonce_str", nonceStr);      //随机字符串
      params.put("refund_account", way);      //退款资金来源

      try {
        params.put("sign", MD5SignUtil.Sign(CommonUtil.FormatQueryParaMap(params, false), mchSecret));
      } catch (SDKRuntimeException e) {
        tenpayLogger.error("weixin refund sign failed error pay=" + order.getPayNo() + "  paramsMap=" + params);
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "生成退款支付签名信息出错");
      }

      CloseableHttpClient httpclient = getCloseableHttpClient(wechatAppConfig, mode);
      try {
        HttpPost httpPost = new HttpPost(WEINXIN_REFUND_URL);
        tenpayLogger.info("executing request" + httpPost.getRequestLine());
        httpPost.setEntity(new StringEntity(XMLUtil.toXml(params), CHARSET));
        CloseableHttpResponse response = httpclient.execute(httpPost);
        try {
          HttpEntity entity = response.getEntity();
          if (entity != null) {
            tenpayLogger.info("Response content length: " + entity.getContentLength());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent()));
            StringBuilder sb = new StringBuilder();
            String text;
            while ((text = bufferedReader.readLine()) != null) {
              sb.append(text);
            }
            Map map = XMLUtil.doXMLParse(sb.toString());
            if (map != null) {
              tenpayLogger.info("map:{}", JSON.toJSONString(map));
              if (!("SUCCESS".equals(map.get("return_code")) && "SUCCESS".equals(map.get("result_code")))) {
                // 如果是未结算资金退款，且错误提示为账户资金不足，则再调用退款接口使用可用余额进行退款
                if ("REFUND_SOURCE_UNSETTLED_FUNDS".equals(way) && "NOTENOUGH".equals(map.get("err_code"))) {
                  refundWay(order, "REFUND_SOURCE_RECHARGE_FUNDS");
                } else {
                  throw new BizException(GlobalErrorCode.INTERNAL_ERROR, map.get("return_msg").toString());
                }
              } else {
                orderRefundService.successByOrderId(order.getId());
              }
            }
          }
          EntityUtils.consume(entity);
        } finally {
          response.close();
        }
      } catch (Exception e) {
        tenpayLogger.error("weixin refund keyStore failed error pay=" + order.getPayNo() + "  paramsMap=" + params);
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "生成退款信息出错");
      }
    }
  }

  /**
   * 微信打款
   *
   * @param id           提现申请单id
   * @param confirmMoney 提现申请单金额
   */
  public void withdraw(String id, String confirmMoney, String rootShopId) {

    WithdrawApply withdrawApply = withdrawApplyService.load(id);
    withdrawApply.setConfirmMoney(NumberUtils.createBigDecimal(confirmMoney));

    if (withdrawApply.getApplyMoney().compareTo(withdrawApply.getConfirmMoney()) < 0) {
      log.error(withdrawApply.getApplyNo() + " 核准金额=" + withdrawApply.getConfirmMoney()
              + " 不能大于申请金额：" + withdrawApply.getApplyMoney());
      throw new BizException(GlobalErrorCode.UNKNOWN,
              withdrawApply.getAccountName() + " 核准金额=" + withdrawApply.getConfirmMoney()
                      + " 不能大于申请金额：" + withdrawApply.getApplyMoney());
    }

    User wechatToUser = userService.load(withdrawApply.getUserId());
    if (wechatToUser == null) {
      log.error(wechatToUser.getId() + "打款用户无微信信息");
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "当前打款用户无微信信息");
    }

    // 从数据库中读取配置好的该公众号证书等相关信息
    WechatAppConfig wechatAppConfig = wechatAppConfigMapper.selectByShopId(rootShopId);
    String nonceStr = CommonUtil.CreateNoncestr();
    String appID = wechatAppConfig.getAppId();
    String mchSecret = wechatAppConfig.getMchKey();
    String merchantId = wechatAppConfig.getMchId();
    Map<String, String> params = new HashMap<String, String>();
    BigDecimal amount = withdrawApply.getConfirmMoney().multiply(new BigDecimal(100));
    //设置package订单参数
    params.put("mch_appid", appID);          //公众账号ID
    params.put("mchid", merchantId);          //商户号
    params.put("partner_trade_no", withdrawApply.getApplyNo());               //商户订单号
    params.put("openid", wechatToUser.getLoginname());               //用户openid
    params.put("check_name", "NO_CHECK");              //校验用户姓名选项
    params.put("amount", "" + amount.intValue());              //金额
    params.put("desc", "分佣");              //企业付款描述信息
    params.put("spbill_create_ip", "127.0.0.1");              //Ip地址
    params.put("nonce_str", nonceStr);              //随机字符串

    String sign;
    try {
      sign = MD5SignUtil.Sign(CommonUtil.FormatQueryParaMap(params, false), mchSecret);
      params.put("sign", sign);
    } catch (SDKRuntimeException e) {
      log.error(withdrawApply.getApplyNo() + "生成打款支付签名信息出错" + e.toString());
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "生成打款支付签名信息出错");
    }

    CloseableHttpClient httpclient = getCloseableHttpClient(wechatAppConfig,
            PaymentMode.WEIXIN);
    try {
      Map map;
      HttpPost httpget = new HttpPost(
              "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers");
      httpget.setEntity(new StringEntity(XMLUtil.toXml(params), CHARSET));
      CloseableHttpResponse response = httpclient.execute(httpget);
      try {
        HttpEntity entity = response.getEntity();
        System.out.println(response.getStatusLine());
        if (entity != null) {
          log.info("Response content length: " + entity.getContentLength());
          BufferedReader bufferedReader = new BufferedReader(
                  new InputStreamReader(entity.getContent()));
          StringBuilder sb = new StringBuilder();
          String text;
          while ((text = bufferedReader.readLine()) != null) {
            sb.append(text);
          }
          map = XMLUtil.doXMLParse(sb.toString());
          if (map != null) {
            if (!("SUCCESS".equals(map.get("return_code")) && "SUCCESS"
                    .equals(map.get("result_code")))) {
              log.error(withdrawApply.getApplyNo() + "微信打款出错" + map
                      .get("return_msg"));
              throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                      (String) map.get("return_msg"));
            } else {
              withdrawApplyService
                      .withdrawPendingByApplyNo(withdrawApply.getApplyNo());

              PayRequest request = trans2PayRequest4PayBatch(withdrawApply);
              request.setOutpayType(PaymentMode.WEIXIN.toString());
              request.setOutpayInfo(wechatToUser.getWeixinCode());
              if (payRequestService.onCreate(request)) {
                payRequestService.onPay(request);
                payRequestService.onSuccess(request);
                OutPay snapshot = trans2OutPay4PayBatch(request);
                snapshot.setOutStatus("SUCCESS");
                snapshot.setRequestId(request.getId());
                snapshot.setStatus(PaymentStatus.SUCCESS);
                snapshot.setTradeNo(withdrawApply.getApplyNo());
                snapshot.setDetail(((String) map.get("return_msg")).getBytes());
                outPayService.insert(snapshot);
                withdrawApplyService.withdrawSuccessByNo(withdrawApply.getApplyNo(),
                        withdrawApply.getConfirmMoney());
              } else {
                log.error(withdrawApply.getApplyNo() + "微信已经打款后服务端生成支付凭证出错");
                throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                        "微信已经打款后服务端生成支付凭证出错");
              }
            }
          }
        }
        EntityUtils.consume(entity);
      } finally {
        response.close();
      }
    } catch (Exception e) {
      log.error(withdrawApply.getApplyNo() + "微信打款信息出错" + e.toString());
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "微信打款信息出错");
    }


  }

  /**
   * 得到outpay支付凭证信息
   */
  private OutPay trans2OutPay4PayBatch(PayRequest payRequest) {
    OutPay aOp = new OutPay();
    aOp.setUserId(payRequest.getToSubAccountId());
    aOp.setOutId(payRequest.getOutpayType()); // WEIXIN微信支付  UNION银联支付 TENPAY财付通支付 ALIPAY支付宝
    aOp.setpOutpayId("0");                // 父亲支付ID，用于批量打款和批量退款中的子支付记录
    aOp.setRequestId(payRequest.getId());                    //
    aOp.setForOutpayId("0");                // 仅用于原路退回的操作，关联原始支付的OutPayId

    aOp.setOutAccountId(payRequest.getFromSubAccountId());                // 三方支付帐号编号
    aOp.setOutAccountName(payRequest.getFromSubAccountId());    // 支付帐号名称
    aOp.setOutStatus(
            "SUBMITTED");                                    //  支付请求的状态，这个状态不是必须的，对于支付宝就是一个字符串描述
    aOp.setOutstatusex("");                                            //  外部交易扩展状态
    aOp.setDetail("".getBytes());   // byte[]);
    aOp.setBillNo("");            //  商户订单号
    aOp.setTradeNo("");        // 第三方交易号

    aOp.setStatus(
            PaymentStatus.PENDING);            //系统内部的支付状态，SUBMITTED提交  FAILED支付失败  SUCCESS支付完成 CANCEL取消
    aOp.setOutpayType(PayRequestBizType.WITHDRAW.toString());
    aOp.setOutpayTypeEx(
            "ADMIN_WITHDRAW"); // 支付类型，USER_PAY用户即时到账,ADMIN_REFUND运营退款，ADMIN_WITHDRAW运营打款，信用还款CREDIT_REPAYMENT，CREDIT_REFUND_AFTER_REPAYMENT还款后的退款
    aOp.setAmount(payRequest.getAmount());
    aOp.setUpdatedAt(new Date());
    aOp.setCreatedAt(new Date());
    return aOp;
  }

  private PayRequest trans2PayRequest4PayBatch(WithdrawApply withdrawApply) {
    String payNo = payRequestApiService.generatePayNo();
    SubAccount fromAccount = accountApiService
            .findSubAccountByUserId(withdrawApply.getUserId(), AccountType.WITHDRAW);
    SubAccount toAccount = accountApiService
            .findSubAccountByUserId(userService.loadKkkdUserId(), AccountType.WITHDRAW);
    PayRequest request = new PayRequest(payNo, withdrawApply.getApplyNo(),
            PayRequestBizType.WITHDRAW, PayRequestPayType.WITHDRAW,
            withdrawApply.getApplyMoney(), fromAccount.getId(),
            toAccount.getId(), null);
    return request;
  }

  /**
   * 得到需要证书验证的httpclient
   */
  private CloseableHttpClient getCloseableHttpClient(WechatAppConfig wechatAppConfig,
                                                     PaymentMode mode) {
    CloseableHttpClient client;
    //证书存储在数据库中
    byte[] cert = null;
    //私钥（在安装证书时设置）
    String password = "";
    if (mode == PaymentMode.WEIXIN) {
      cert = wechatAppConfig.getCert_file();
      password = wechatAppConfig.getCert_password();
    } else if (mode == PaymentMode.WEIXIN_APP) {
      cert = wechatAppConfig.getApp_cert_file();
      password = wechatAppConfig.getApp_cert_password();
    } else if (mode == PaymentMode.WEIXIN_MINIPROGRAM) {
      cert = wechatAppConfig.getCert_file();
      password = wechatAppConfig.getCert_password();
    }
    if (cert == null || StringUtils.isEmpty(password)) {
      log.error("微信配置证书或密码为空");
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "微信配置证书或密码为空");
    }
    try {
      // 证书,证书密码需要解密后使用
      EncryptUtil des1 = new EncryptUtil();
      password = des1.decrypt(password);
      cert = des1.decrypt(cert);

      KeyStore keyStore = KeyStore.getInstance("PKCS12");
      InputStream instream = new ByteArrayInputStream(cert);
      keyStore.load(instream, password.toCharArray());
      // Trust own CA and all self-signed certs
      SSLContext sslcontext = SSLContexts.custom()
              .loadKeyMaterial(keyStore, password.toCharArray())
              .build();
      // Allow TLSv1 protocol only
      SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
              sslcontext,
              new String[]{"TLSv1"},
              null,
              SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
      client = HttpClients.custom()
              .setSSLSocketFactory(sslsf)
              .build();
    } catch (Exception e) {
      log.error("建立微信支付http出错" + e.toString());
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "建立微信支付http出错");
    }
    return client;
  }
}
