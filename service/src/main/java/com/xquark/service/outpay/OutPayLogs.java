package com.xquark.service.outpay;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OutPayLogs {

  public static final Logger stripeLogger = LoggerFactory.getLogger("stripe");

  public static final Logger alipayLogger = LoggerFactory.getLogger("alipay");

  public static final Logger tenpayLogger = LoggerFactory.getLogger("tenpay");

  public static final Logger umPayLogger = LoggerFactory.getLogger("umpay");
  
  public static final Logger thirdRefundLogger = LoggerFactory.getLogger("thirdRefund");

}
