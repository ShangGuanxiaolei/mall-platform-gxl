package com.xquark.service.outpay.impl.tenpay;

import com.alibaba.fastjson.JSON;
import com.xquark.dal.mapper.PaymentSwitchConfigMapper;
import com.xquark.dal.model.pay.PaymentConfig;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.outpay.impl.PaymentFacade;
import com.xquark.service.wechat.WechatService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.client.RestOperations;

import java.util.Map;

/**
 * @author wangxinhua.
 * @date 2018/9/27
 */
public abstract class BaseEpsTenpaySignService extends BaseTenpaySignService {

  protected BaseEpsTenpaySignService(PaymentFacade paymentFacade,
      RestOperations restTemplate,
      WechatService wechatService,
      PaymentSwitchConfigMapper paymentSwitchConfigMapper) {
    super(paymentFacade, restTemplate, wechatService, paymentSwitchConfigMapper);
  }

  @Override
  protected Map<String, String> handleRes(PaymentRequestVO pay, PaymentConfig paymentConfig,
      Map<String, String> res) {
    String status = res.get("status");
    String resultCode = res.get("result_code");
    if ( (StringUtils.isBlank(status) || StringUtils.isBlank(resultCode))
        && !"0".equals(resultCode) ) {//resultCode不返回0表示失败
      logger.error("EPS支付请求返回错误: status: {} result_code: {}", status, resultCode);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }
    String payInfoStr = res.get("pay_info");
    return handlePayInfo(payInfoStr);
  }

  /**
   * 子类可复写该方法实现自己的业务逻辑
   * @param payInfo eps返回的payInfo信息
   * @return 客户端需要的唤起支付信息
   */
  protected Map<String, String> handlePayInfo(String payInfo) {
    return JSON.parseObject(payInfo, STRING_MAP_REFERENCE);
  }

}
