package com.xquark.service.outpay;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.ImmutableMap;
import com.xquark.dal.mapper.PaymentSwitchConfigMapper;
import com.xquark.dal.model.PaidAble;
import com.xquark.dal.model.PaymentSwitchConfig;
import com.xquark.dal.type.PayChannelConst;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.error.PayNotifyException;
import com.xquark.service.error.ThirdRefundData;
import com.xquark.service.outpay.impl.ThirdPaymentQueryRes;
import com.xquark.service.outpay.impl.alipay.AliPayQueryAdapter;
import com.xquark.service.outpay.impl.alipay.EpsQueryAdapter;
import com.xquark.service.outpay.impl.tenpay.CommonUtil;
import com.xquark.service.outpay.impl.tenpay.HttpClientUtil;
import com.xquark.service.outpay.impl.tenpay.WechatQueryAdapter;
import com.xquark.utils.http.HttpInvokeResult;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * 查询第三方支付状态
 *
 * @author wangxinhua
 * @date 2019-04-19
 * @since 1.0
 */
@Service
public class ThirdPaymentQueryService {

    /**
     * 支付函数引用表
     */
    private final Map<PaymentMode, Function<PaidAble, Optional<? extends ThirdPaymentQueryRes>>> queryTable;

    private PaymentSwitchConfigMapper switchConfigMapper;
    
    private Logger thirdRefundLogger = OutPayLogs.thirdRefundLogger;

    @Value("${profiles.active}")
    private String profile;

    @Autowired
    public ThirdPaymentQueryService(WechatQueryAdapter wechatQueryAdapter,
                                    AliPayQueryAdapter aliPayQueryAdapter,
                                    EpsQueryAdapter epsQueryAdapter) {
        this.queryTable = ImmutableMap.of(
                PaymentMode.WEIXIN_APP,
                wechatQueryAdapter::query,
                PaymentMode.WEIXIN_MINIPROGRAM,
                wechatQueryAdapter::query,
                PaymentMode.ALIPAY,
                aliPayQueryAdapter::query,
                PaymentMode.EPS,
                epsQueryAdapter::query);
    }

    @Autowired
    public void setSwitchConfigMapper(PaymentSwitchConfigMapper switchConfigMapper) {
        this.switchConfigMapper = switchConfigMapper;
    }

    @SuppressWarnings("unchecked")
    public Optional<ThirdPaymentQueryRes> query(PaidAble paidAble) {
        Objects.requireNonNull(paidAble);
        final String payNo = paidAble.getPayNo();
        PaymentMode payType = paidAble.getPayType();

        checkNotNull(payType, "未设置支付方式");
        checkState(StringUtils.isNotBlank(payNo), "该订单未支付");

        final PaymentSwitchConfig eps = switchConfigMapper.selectByCodeAndProfile("EPS", profile);
        if (eps != null && eps.getStatus()) {
            // 银联支付方法, 走银联通道查询
            payType = PaymentMode.EPS;
        }
        final Function<PaidAble, Optional<? extends ThirdPaymentQueryRes>> paidFunc = queryTable.get(payType);
        if (paidFunc == null) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "支付方式不支持");
        }
        return (Optional<ThirdPaymentQueryRes>) paidFunc.apply(paidAble);
    }
    
    @Value("${pay.notify.third_refund_url:/refund/create}")
    private String thirdRefundUrl;
    
    public String doThirdRefund(PayNotifyException e) {
    	return doThirdRefund(e.getData());
    }

    public String doThirdRefund(ThirdRefundData data) {
        String respMsg = PayNotifyException.SUCCESSMSG;
        try {
            thirdRefundLogger.info("start third refund......");
            thirdRefundLogger.info("http [url:{},params:{}]",thirdRefundUrl, data);
            HttpInvokeResult repData = HttpClientUtil.post(thirdRefundUrl, data);
            thirdRefundLogger.info("third refund http result:{}",repData.getContent());
            //返回数据，退款成功返回成功标识不再通知，退款失败返回失败标识，下次继续通知
            JSONObject respContent = JSONObject.parseObject(repData.getContent());
            if(!repData.isOK() || 0 != respContent.getInteger("code")) // 请求失败时，下次继续通知
                respMsg = PayNotifyException.FAILMSG;
            else if(PayChannelConst.WECHAT.equalsIgnoreCase(data.getPayChannel()) ) {
                //请求成功时，对微信支付单独处理返回结果
                HashMap<String, String> xml = new HashMap<String, String>();
                xml.put("return_code", respMsg);
                xml.put("return_msg", "OK");
                respMsg = CommonUtil.ArrayToXml(xml);
            }
            thirdRefundLogger.info("end third refund, respMsg:{}",respMsg);

        } catch (Exception e1) {
            thirdRefundLogger.error("do third refund error..",e1);
            respMsg = PayNotifyException.FAILMSG;
        }
        return respMsg;
    }
}
