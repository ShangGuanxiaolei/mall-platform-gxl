package com.xquark.service.outpay;

import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.outpay.impl.alipay.RSA;
import com.xquark.service.pay.OutPayService;
import com.xquark.service.pay.model.PaymentResponseVO;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public abstract class AbstractThirdPartyService extends BaseServiceImpl implements
    ThirdPartyPayment {

  @Autowired
  private OutPayService outPayService;

  public void savePayResult(PaymentResponseVO payVO) {
    outPayService.savePayResult(payVO);
  }

  protected void responseBody(String html, HttpServletResponse resp) {

    PrintWriter writer = null;
    try {
      writer = resp.getWriter();
      writer.print(html);
      writer.flush();
    } catch (Exception e) {
      log.error("不能正确response", html);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "无法正确跳转");
    } finally {
      if (writer != null) {
        writer.close();
      }
    }
  }

  protected String genClientSigned(String signInfo) {
    String rsaPrivateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAL/MhkstDzMmRDntaYUTpd83jgX+L96pGd8P98xtZ3AUk4t12eHF3idvasKRhv3JJ8/ryCHmSF+1J7QgfF9kMnp0rrbE2P6cpJHnpt7PMCLZLzvW/yTUmP80gXjebgKa1Twht0S6FFtfSESO2udOqHWVg56mrBdhAw5VuoynJ+MBAgMBAAECgYEAgdZlbsyhBoI4G3LBROoJFrOX/CyXoUaeEefQWt4Q8MmtG/J6vaDiA5YkEnTLik+7GMDHaVNn2QpcR07agwTkOd81ect8Sa4Ilo1xOCoX0sXCRjlwOkXLDFiUrLpAIG/fyH5yzSH7LyylxMpbupFjYzR4KTjXjWD6wCQXWDgSAzUCQQDmpJ3mj4zyG13s3hn/iORJ/4IUxrbk1Nse0jXUwnhC8wpj7ubWT+tJ5NkQysV/J3uFIJ+jy+3bp9/Lrqee3Bt/AkEA1OKmuF0pBBtj/QwrGAw4ShYuN4FWyq8tNa/JAfHIxDy9bYKYMzMcjyjdsSZ/jHP5DhaHmylLO9ppJ67RVIVBfwJASp9wRyyi2aJpHT4vAzJzSk3U9IvmmNsVmj9BE3loF6Ey92pQXX62Dc5xPLGefKl1mXkYrZJJfwwtqGT17rwgWQJBALAP9yR436gm4w3v12AfExqqt3RuQpKESFajWBGEnq81MND19dw6RD9d4+NT3J8TdwIgvewkPbV1kAwit7s4Lg8CQDVDDU1GFlHpl3ZfAD998nBLRBPQ/rz9KgVmWngrZ5YyAYwKh6OAPpao2tagcPnc0/n72moP9mjGbZqdjW5cSHg=";
    return RSA.sign(signInfo, rsaPrivateKey, "UTF-8");
  }

  @Override
  public String createPayRequestUrl(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO payRequest) {
    return null;
  }

  @SuppressWarnings("rawtypes")
  public PaymentResponseClientVO signMiniProgram(PaymentRequestVO pay) {
    return null;
  }


  @Override
  public PaymentResponseClientVO signApp(PaymentRequestVO pay) {
    return null;
  }

}
