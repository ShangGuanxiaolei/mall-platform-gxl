package com.xquark.service.outpay.impl.alipay;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.httpclient.methods.multipart.FilePartSource;
import org.apache.commons.httpclient.methods.multipart.PartSource;

/* *
 *类名：AlipayFunction
 *功能：支付宝接口公用函数类
 *详细：该类是请求、通知返回两个文件所调用的公用函数核心处理文件，不需要修改
 *版本：3.3
 *日期：2012-08-14
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayCore {

  /**
   * 除去数组中的空值和签名参数
   *
   * @param sArray 签名参数组
   * @return 去掉空值与签名参数后的新签名参数组
   */
  public static Map<String, String> paraFilter(Map<String, String> sArray) {

    Map<String, String> result = new HashMap<String, String>();

    if (sArray == null || sArray.size() <= 0) {
      return result;
    }

    for (String key : sArray.keySet()) {
      String value = sArray.get(key);
      if (value == null || value.equals("") || key.equalsIgnoreCase("sign")) {
        continue;
      }
      result.put(key, value);
    }

    return result;
  }

  /**
   * 把数组所有元素排序，并按照“参数=参数值”的模式用“&”字符拼接成字符串
   *
   * @param params 需要排序并参与字符拼接的参数组
   * @return 拼接后字符串
   */
  public static String createLinkString(Map<String, String> params) {

    List<String> keys = new ArrayList<String>(params.keySet());
    Collections.sort(keys);

    String prestr = "";

    for (int i = 0; i < keys.size(); i++) {
      String key = keys.get(i);
      String value = params.get(key);

      if (i == keys.size() - 1) {// 拼接时，不包括最后一个&字符
        prestr = prestr + key + "=" + value;
      } else {
        prestr = prestr + key + "=" + value + "&";
      }
    }

    return prestr;
  }

  /**
   * 把数组所有元素按照固定参数排序，以“参数=参数值”的模式用“&”字符拼接成字符串
   *
   * @param params 需要参与字符拼接的参数组
   * @return 拼接后字符串
   */
  public static String createLinkStringNoSort(Map<String, String> params) {

//		Map<String, String> sParaSort = new HashMap<String, String>();
//		sParaSort.put("service", params.get("service"));
//		sParaSort.put("v", params.get("v"));
//		sParaSort.put("sec_id", params.get("sec_id"));
//		sParaSort.put("notify_data", params.get("notify_data"));
//
//		String prestr = "";
//		for (String key : sParaSort.keySet()) {
//			prestr = prestr + key + "=" + sParaSort.get(key) + "&";
//		}
//		prestr = prestr.substring(0, prestr.length() - 1);

//    String prestr = "service=" + params.get("service") + "&";
//    prestr += "v=" + params.get("v") + "&";
//    prestr += "sec_id=" + params.get("sec_id") + "&";
//    prestr += "notify_data=" + params.get("notify_data");


    StringBuffer signBuffer = new StringBuffer();
    signBuffer.append("app_id=" + params.get("app_id"));
    signBuffer.append("&biz_content={");

    StringBuffer jsonBuffer = new StringBuffer();
    jsonBuffer.append("\"timeout_express\":\"30m\",");
    jsonBuffer.append("\"seller_id\":\"" + params.get("seller_id") + "\",");
    jsonBuffer.append("\"product_code\":\"QUICK_MSECURITY_PAY\",");
    jsonBuffer.append("\"total_amount\":\"" + params.get("total_amount") + "\",");
    jsonBuffer.append("\"subject\":\"" + params.get("subject") + "\",");
    jsonBuffer.append("\"body\":\"支付宝支付\",");
    jsonBuffer.append("\"out_trade_no\":\"" + params.get("out_trade_no") + "\"");

    signBuffer.append(jsonBuffer.toString());
    signBuffer.append("}");

    signBuffer.append("&charset=utf-8");
    signBuffer.append("&method=alipay.trade.app.pay");
    signBuffer.append("&notify_url=" + params.get("notify_url"));
    signBuffer.append("&sign_type=" + params.get("sign_type"));
    signBuffer.append("&timestamp=" + params.get("gmt_create"));
    signBuffer.append("&version=1.0");

    return signBuffer.toString();
  }

  /**
   * 生成文件摘要
   *
   * @param strFilePath 文件路径
   * @param file_digest_type 摘要算法
   * @return 文件摘要结果
   */
  public static String getAbstract(String strFilePath, String file_digest_type) throws IOException {
    PartSource file = new FilePartSource(new File(strFilePath));
    if (file_digest_type.equals("MD5")) {
      return DigestUtils.md5Hex(file.createInputStream());
    } else if (file_digest_type.equals("SHA")) {
      return DigestUtils.sha256Hex(file.createInputStream());
    } else {
      return "";
    }
  }
}
