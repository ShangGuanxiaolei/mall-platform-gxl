package com.xquark.service.outpay.impl.tenpay;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.google.common.collect.ImmutableMap;
import com.xquark.dal.mapper.PaymentSwitchConfigMapper;
import com.xquark.dal.model.PaymentSwitchConfig;
import com.xquark.dal.model.User;
import com.xquark.dal.model.pay.PaymentConfig;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.outpay.AbstractThirdPartyService;
import com.xquark.service.outpay.PaySign;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.outpay.impl.AliPaymentImpl;
import com.xquark.service.outpay.impl.PaymentFacade;
import com.xquark.service.wechat.WechatService;
import com.xquark.thirds.alipay.pc.util.UtilDate;
import com.xquark.utils.http.PoolingHttpClients;
import org.apache.commons.lang3.StringUtils;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestOperations;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by wangxinhua. Date: 2018/8/27 Time: 下午4:16 微信签名基类
 */
public abstract class BaseTenpaySignService implements PaySign {

  protected final Logger logger = LoggerFactory.getLogger(this.getClass());

  protected final static TypeReference<Map<String, String>> STRING_MAP_REFERENCE =
      new TypeReference<Map<String, String>>() {};

  protected final PaymentFacade paymentFacade;

  protected final RestOperations restTemplate;

  private final WechatService wechatService;

  protected PaymentSwitchConfigMapper paymentSwitchConfigMapper;

  @Autowired
  protected BaseTenpaySignService(PaymentFacade paymentFacade,
      RestOperations restTemplate, WechatService wechatService,
      PaymentSwitchConfigMapper paymentSwitchConfigMapper) {
    this.paymentFacade = paymentFacade;
    this.restTemplate = restTemplate;
    this.wechatService = wechatService;
    this.paymentSwitchConfigMapper = paymentSwitchConfigMapper;
  }

  @Value("${profiles.active}")
  private String profile;

  private boolean epsStatus() {
    PaymentSwitchConfig paymentSwitchConfig = paymentSwitchConfigMapper.selectByCodeAndProfile("EPS", profile);
    return paymentSwitchConfig.getStatus();
  }

  /**
   * 调用微信地址获取支付信息, 供客户端调用
   *
   * @param pay payRequest
   * @param payUser 当前申请支付用户
   * @return 支付结果
   */
  @Override
  public Map<String, String> sign(PaymentRequestVO pay,
      User payUser) {
    // pre check args
    checkNotNull(pay);
    String apiUrl = getApiUrl(pay);
    checkArgument(StringUtils.isNotBlank(apiUrl), "api url can not be null or empty");

    PaymentMode paymentMode = pay.getPayType();
    PaymentConfig paymentConfig = paymentFacade.loadPayment(profile, paymentMode, pay.getTradeType().name());
    if (paymentConfig == null) {
      logger.error("支付配置 {} : {} 未初始化", paymentMode, pay.getTradeType());
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "支付配置未初始化");
    }
    //根据上下文传递的类型，判断进入哪个实现类去构建请求参数
    Map<String, String> reqParam = buildParams(pay, paymentConfig, payUser);
    String mchSecret = paymentConfig.getMchSecret();//获取商户证书
    String sign;
    try {
      // pay for wechat
      sign = MD5SignUtil.Sign(CommonUtil.FormatQueryParaMap(reqParam, false), mchSecret);
    } catch (SDKRuntimeException e) {
      logger.error("weixin pay failed error pay=" + pay + "  paramsMap=" + reqParam, e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }
    // 避免子类返回不可修改的map实现, 重新构造map
    Map<String, String> paramsMap = ImmutableMap.<String, String>builder()
        .putAll(reqParam)
        .put("sign", sign)
        .build();
    logger.info("[{}:{}]支付，请求数据paramsMap: {}",paymentMode,pay.getTradeType(),paramsMap);
    // 请求接口
    String respStr = PoolingHttpClients.postEpsSSL(apiUrl, XMLUtil.toXml(paramsMap));

    if(paymentMode == PaymentMode.ALIPAY){
      try {
        int index = respStr.indexOf("<pay_info>");
        int last = respStr.indexOf("</pay_info>");
        String payInfo = respStr.substring(index, last);
        String all = respStr.replace(payInfo, "<pay_info>{}");
        Map map = XMLUtil.doXMLParse(all);
        String replace = payInfo.replace("<pay_info>", "");
        String resPay = resPay(replace);
        map.put("pay_info", resPay);
        return handleRes(pay, paymentConfig, map);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    Map<String, String> map = null;
    try {
      map = XMLUtil.doXMLParse(respStr);
      if (map != null) {
        // handle response by impl
         return handleRes(pay, paymentConfig, map);
      }
    } catch (Exception e) {
      logger.error("weixin parse xml failed error pay=" + pay + "  map=" + map, e);
    }

    return null;
  }

  /**
   * 构造请求参数
   *
   * @param pay payRequest
   * @param payUser 当前申请支付用户
   * @return 构造的请求参数
   */
  protected abstract Map<String, String> buildParams(PaymentRequestVO pay,
      PaymentConfig paymentConfig, User payUser);

  /**
   * 处理返回结构
   *
   * @param res 返回结构map
   * @return map of payInfo
   * @throws BizException 如果返回结构有异常实现类应该排除业务异常
   */
  protected abstract Map<String, String> handleRes(
      PaymentRequestVO pay, PaymentConfig paymentConfig,
      Map<String, String> res);

  /**
   * 封装获取jsapi方法
   *
   * @return 最新的jsapi
   */
  protected String obtainJsapiTicket() {
    return wechatService.obtainJsapiTicket();
  }

  /**
   * 获取api地址
   *
   * @param requestVO 相关的配置
   */
  private String getApiUrl(PaymentRequestVO requestVO) {
    PaymentMode paymentMode = requestVO.getPayType();
    if (PaymentMode.WEIXIN == paymentMode) {
      if (epsStatus()) {
        //正式地址，app与小程序使用同一个
        return "https://pay.xrtpay.com:10443/xrtpay/gateway";
      }
      return "https://api.mch.weixin.qq.com/pay/unifiedorder";
    }else if(PaymentMode.ALIPAY == paymentMode){
      return "https://pay.xrtpay.com:10443/xrtpay/gateway";
    } else {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "payment mode not match");
    }
  }

  private String resPay(String payInfo){
    AliPayMsg apm = new AliPayMsg();
    apm.setCode("1");
    apm.setMsg("success");
    Map<String, String> map = new HashMap<>();
    map.put("alipay", payInfo);
    apm.setData(map);
    JSONObject resultJson = JSONObject.parseObject(JSON.toJSONString(apm));
    return resultJson.toString();
  }

  private class AliPayMsg {
    private String code;
    private String msg;
    private Object data;

    public String getCode() {
      return code;
    }

    public void setCode(String code) {
      this.code = code;
    }

    public String getMsg() {
      return msg;
    }

    public void setMsg(String msg) {
      this.msg = msg;
    }

    public Object getData() {
      return data;
    }

    public void setData(Object data) {
      this.data = data;
    }
  }

}
