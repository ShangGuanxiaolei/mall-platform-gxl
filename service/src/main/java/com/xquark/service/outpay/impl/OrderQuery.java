package com.xquark.service.outpay.impl;

import com.google.common.collect.ImmutableMap;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.PaidAble;
import com.xquark.dal.model.pay.PaymentConfig;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.type.TradeType;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.outpay.impl.tenpay.CommonUtil;
import com.xquark.service.outpay.impl.tenpay.MD5SignUtil;
import com.xquark.service.outpay.impl.tenpay.SDKRuntimeException;
import com.xquark.service.outpay.impl.tenpay.XMLUtil;
import com.xquark.utils.http.PoolingHttpClients;
import org.apache.commons.lang3.StringUtils;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/** 调用微信接口查询订单 */
@Component
public class OrderQuery {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private static final String API_URL = "https://api.mch.weixin.qq.com/pay/orderquery";

  private final PaymentFacade paymentFacade;

  @Value("${profiles.active}")
  private String profile;

  @Autowired
  protected OrderQuery(PaymentFacade paymentFacade) {
    this.paymentFacade = paymentFacade;
  }

  public Map<String, String> signOrder(PaidAble paidAble) {
    PaymentMode paymentMode = paidAble.getPayType();

    TradeType tradeType = paymentMode == PaymentMode.WEIXIN_APP ? TradeType.APP : TradeType.MINIPROGRAM;
    PaymentConfig paymentConfig = paymentFacade.loadPayment(profile, paymentMode, tradeType.name());

    if (paymentConfig == null) {
      logger.error("退款失败，支付配置未初始化=======================================");
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "支付配置未初始化");
    }

    logger.info("wechat refund signOrder, current paymentConfig={}, current profile={}", paymentConfig, profile);

    Map<String, String> reqParam = this.buildParams(paidAble, paymentConfig);

    String mchSecret = paymentConfig.getMchSecret(); // 获取商户证书

    String sign;
    try {
      sign = MD5SignUtil.Sign(CommonUtil.FormatQueryParaMap(reqParam, false), mchSecret);
    } catch (SDKRuntimeException e) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }

    // 避免子类返回不可修改的map实现, 重新构造map
    Map<String, String> paramsMap =
        ImmutableMap.<String, String>builder().putAll(reqParam).put("sign", sign).build();

    // 请求接口
    String respStr = PoolingHttpClients.postEpsSSL(API_URL, XMLUtil.toXml(paramsMap));
    try {
      return XMLUtil.doXMLParse(respStr);
    } catch (JDOMException | IOException e) {
        logger.error("微信返回格式不正确: {}", respStr, e);
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "微信返回格式不正确");
    }
  }

  public boolean sign(Order order, TradeType tradeType) {
    Map<String, String> map;
    try {
      map = signOrder(order);
      if (map != null) {
        return handleRes(map, order);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  private Map<String, String> buildParams(PaidAble paidAble, PaymentConfig paymentConfig) {
    return ImmutableMap.<String, String>builder()
        .put("appid", paymentConfig.getAppId())
        .put("mch_id", paymentConfig.getMchId())
        .put("out_trade_no", paidAble.getPayNo())
        .put("nonce_str", CommonUtil.CreateNoncestr())
        .build();
  }

  private boolean handleRes(Map<String, String> res, Order order) {
    logger.info("start execute wechat refund response result, map={}, order={}", res, order);
    String returnCode = res.get("return_code");
    String resultCode = res.get("result_code");

    if (StringUtils.isBlank(returnCode) || StringUtils.isBlank(resultCode)) {
      logger.error("订单接口查询失败，请求单号[" + order.getPayNo() + "]");
    }

    if ("SUCCESS".equals(returnCode) && "SUCCESS".equals(resultCode)) {
      String orderTradeNo = res.get("out_trade_no");
      if (orderTradeNo != null) {
        logger.info("查询订单成功, 订单号为[" + orderTradeNo + "]");
        return order.getPayNo().equals(res.get("out_trade_no"));
      }

    } else { // 订单不在微信原生接口支付
      String errorCode = res.get("err_code");
      if (errorCode != null) {
        logger.info("查询订单返回成功，resultCode返回失败，错误码[" + res.get("err_code") + "]");
        return !"ORDERNOTEXIST".equals(res.get("err_code"));
      }
    }

    return false;
  }

  private Map<String, String> buildParamsForEpsPayNo(PaidAble paidAble, PaymentConfig paymentConfig) {
    return ImmutableMap.<String, String>builder()
        .put("service", "unified.trade.query")
        .put("mch_id", paymentConfig.getMchId())
        .put("out_trade_no", paidAble.getPayNo())
        .put("nonce_str", CommonUtil.CreateNoncestr())
        .build();
  }

  public Map<String, String> queryByEps(PaidAble paidAble, TradeType tradeType) {

    String apiUrl = "http://pay.xrtpay.cn/xrtpay/gateway";

    PaymentMode paymentMode = paidAble.getPayType();

    PaymentConfig paymentConfig = paymentFacade.loadPayment(profile, paymentMode, tradeType.name());

    if (paymentConfig == null) {
      logger.error("查询失败，支付配置未初始化=======================================");
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "支付配置未初始化");
    }

    Map<String, String> reqParam = this.buildParamsForEpsPayNo(paidAble, paymentConfig);

    String mchSecret = paymentConfig.getMchSecret(); // 获取商户证书

    String sign;
    try {
      sign = MD5SignUtil.Sign(CommonUtil.FormatQueryParaMap(reqParam, false), mchSecret);
    } catch (SDKRuntimeException e) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付信息出错");
    }

    // 避免子类返回不可修改的map实现, 重新构造map
    Map<String, String> paramsMap =
        ImmutableMap.<String, String>builder().putAll(reqParam).put("sign", sign).build();

    // 请求接口
    String respStr = PoolingHttpClients.postEpsSSL(apiUrl, XMLUtil.toXml(paramsMap));

    try {
      Map<String, String> map = XMLUtil.doXMLParse(respStr);
      if (map != null) {
        logger.info("ebs 查询结果 {}",respStr);
        map.put("payment_id", paymentConfig.getPaymentId());
        return map;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }
}
