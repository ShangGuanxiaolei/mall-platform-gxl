package com.xquark.service.outpay.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.xquark.dal.mapper.PaymentMerchantMapper;
import com.xquark.dal.mapper.PaymentSwitchConfigMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.model.pay.PaymentConfig;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.type.TradeType;
import com.xquark.dal.vo.OrderVO;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.error.PayNotifyException;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderRefundService;
import com.xquark.service.order.OrderService;
import com.xquark.service.outpay.*;
import com.xquark.service.outpay.impl.alipay.AlipayNotify;
import com.xquark.service.outpay.impl.alipay.AlipaySubmit;
import com.xquark.service.outpay.impl.alipay.RSA;
import com.xquark.service.outpay.impl.tenpay.CommonUtil;
import com.xquark.service.outpay.impl.tenpay.MD5SignUtil;
import com.xquark.service.outpay.impl.tenpay.SDKRuntimeException;
import com.xquark.service.pay.model.PaymentResponseVO;
import com.xquark.thirds.alipay.pc.util.UtilDate;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;
import com.xquark.utils.XMLUtil;
import com.xquark.utils.http.PoolingHttpClients;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

@Component("aliPayment")
public class AliPaymentImpl extends AbstractThirdPartyService {

  private Logger alipayLogger = OutPayLogs.alipayLogger;

  @Autowired
  private OrderService orderService;

  @Autowired
  private PaymentMerchantMapper paymentMerchantMapper;

  @Autowired
  private PaymentFacade paymentFacade;

  /**
   * 异步通知
   */
  public static final String notify_url = "/pay/alipay/notify";

  /**
   * 页面跳转同步通知页面路径
   */
  public static final String call_back_url = "/pay/alipay/call_back";

  /**
   * 异步通知
   */
  public static final String paymentBatch_notify_url = "/paymentBatch/alipay/notify";

  public static final String refundBatch_notify_url = "/refundBatch/alipay/notify";

  /**
   * app接入方式的回调
   */
  public static final String notify_url_app = "/pay/alipay/notify_app";

  /**
   * 操作中断返回地址 ，用户付款中途退出返回商户的地址。需http://格式的完整路径，不允许加?id=123这类自定义参数 TODO
   */
  public static final String merchant_url = "/pay/alipay/undone";

  // 支付宝网关地址
  private static final String ALIPAY_GATEWAY_NEW = "https://openapi.alipay.com/gateway.do";

  private static final String ALIPAY_GATEWAY_BATCH = "https://mapi.alipay.com/gateway.do?";

  private final static String charset = "UTF-8";

  /** 银联退款请求地址 */
  private static final String EPS_ALIPAY_URL = "https://pay.xrtpay.com:10443/xrtpay/gateway";

  @Autowired
  private CashierService cashierService;

  @Autowired
  private OrderRefundService orderRefundService;

  @Autowired
  private MainOrderService mainOrderService;

  @Autowired
  private PaymentSwitchConfigMapper paymentSwitchConfigMapper;

  @Value("${profiles.active}")
  String profile;

  private boolean epsStatus() {
    PaymentSwitchConfig paymentSwitchConfig = paymentSwitchConfigMapper.selectByCodeAndProfile("EPS", profile);
    return paymentSwitchConfig.getStatus();
  }

  @Override
  public PaymentResponseClientVO sign(PaymentRequestVO pay) {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "APP", PaymentMode.ALIPAY);

    StringBuilder sb = new StringBuilder();
    sb.append("partner=\"");
    sb.append(paymentMerchant.getMerchantId());
    sb.append("\"&out_trade_no=\"");
    sb.append(pay.getTradeNo());
    sb.append("\"&subject=\"");
    sb.append(pay.getSubject());
    sb.append("\"&body=\"");
    sb.append(StringUtils.isEmpty(pay.getDescription()) ? pay.getSubject()
        : pay.getDescription());
    sb.append("\"&total_fee=\"");
    sb.append(pay.getTotalFee());
    sb.append("\"&notify_url=\"");
    sb.append(paymentMerchant.getNotifySite()).append(notify_url_app);
    sb.append("\"&service=\"mobile.securitypay.pay");
    sb.append("\"&_input_charset=\"" + charset);
    sb.append("\"&payment_type=\"1");
    sb.append("\"&seller_id=\"");
    sb.append(paymentMerchant.getMerchantAccount());
    // 支付的有效时间
    sb.append("\"&it_b_pay=\"3d\"");

    String sign = sb.toString();
    alipayLogger.debug("alipay to sign " + sb);
    String signed = RSA.sign(sign, paymentMerchant.getSecretKey(), charset);
    sign += "&sign_type=\"RSA\"";
    try {
      sign += "&sign=\"" + URLEncoder.encode(signed, charset) + "\"";
    } catch (UnsupportedEncodingException e) {
      alipayLogger.error("签名错误" + pay, e);
    }

    PaymentResponseClientVO response = new PaymentResponseClientVO(
        pay.getTradeNo(), pay.getOutTradeNo(), pay.getTotalFee(), pay.getOrderIds(),
        PaymentMode.ALIPAY
    );

    Map<String, String> payInfo = new HashMap<String, String>();
    payInfo.put("sign", sign);

    response.setPayInfo(payInfo);
    response.setSignInfo(sign);
    response.setSigned(genClientSigned(response.getSignInfo()));

    alipayLogger.info("alipay signed payNo=[" + pay.getTradeNo() + "]" + response);
    alipayLogger.info("paymentMerchant payNo=[" + pay.getTradeNo() + "]" + paymentMerchant);

    return response;
  }

  @Override
  public PaymentResponseVO payNotifyApp(HttpServletRequest req,
                                        HttpServletResponse resp) {
    // 获取支付宝POST过来反馈信息
    Map<String, String> params = extractParams(req);

    alipayLogger.info("alipay app notify " + params);

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "APP", PaymentMode.ALIPAY);
    // 获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//
    try {
      if (!AlipayNotify.verifyNofityApp(params, paymentMerchant.getMerchantId(), "0001",
          paymentMerchant.getPubKey(), paymentMerchant.getSecretKey())) {
        alipayLogger.info("AlipayNotify.verifyNotify failed. params=" + params);
        return null;
      }
    } catch (Exception e) {
      alipayLogger.warn("payment verify failed. params=" + params, e);
      return null;
    }
    alipayLogger.debug("AlipayNotify.verifyNofityApp success. params=[" + params + "] ");

    try {
      PaymentResponseVO p = new PaymentResponseVO();
      p.setPaymentMode(PaymentMode.ALIPAY);
      // 商户订单号
      p.setBillNo(new String(req.getParameter("out_trade_no").getBytes(
          "ISO-8859-1"), charset));
      // 支付宝交易号
      p.setBillNoPay(new String(req.getParameter("trade_no").getBytes(
          "ISO-8859-1"), charset));
      // 交易状态
      PaymentStatus status;
      String s = new String(req.getParameter("trade_status").getBytes(
          "ISO-8859-1"), charset);
      if ("TRADE_SUCCESS".equalsIgnoreCase(s)) {
        status = PaymentStatus.SUCCESS;
      } else if ("TRADE_FINISHED".equalsIgnoreCase(s)) {
        status = PaymentStatus.FINISHED;
      } else if ("TRADE_CLOSED".equalsIgnoreCase(s)) {
        status = PaymentStatus.CLOSED;
      } else {
        status = PaymentStatus.PENDING;
      }
      p.setBillStatus(status);
      p.setPaymentMerchantId(paymentMerchant.getId());

      super.savePayResult(p);

      String total_fee = new String(req.getParameter("total_fee").getBytes(
          "ISO-8859-1"), charset);
      BigDecimal paidFee = NumberUtils.createBigDecimal(total_fee);
      paidFee = paidFee.setScale(2, BigDecimal.ROUND_DOWN);
      if (status == PaymentStatus.SUCCESS || status == PaymentStatus.FINISHED) {
        cashierService.handlePaymentResponse(p.getBillNo(), PaymentMode.ALIPAY, p);
      }
      return p;
    } catch (UnsupportedEncodingException e) {
      alipayLogger.error("payment verify ok, but parse data failed. params="
          + params, e);
      return null;
    }
  }

  private  String getSignContent(Map<String, String> sortedParams) {
    StringBuffer content = new StringBuffer();
    List<String> keys = new ArrayList(sortedParams.keySet());
    Collections.sort(keys);
    int index = 0;

    for(int i = 0; i < keys.size(); ++i) {
      String key = (String)keys.get(i);
      String value = (String)sortedParams.get(key);
      if(com.alipay.api.internal.util.StringUtils.areNotEmpty(new String[]{key, value})) {
        content.append((index == 0?"":"&") + key + "=" + value);
        ++index;
      }
    }

    return content.toString();
  }

  public PaymentResponseVO payRequest(HttpServletRequest req, HttpServletResponse resp,
                                      PaymentRequestVO request) {

    try {
      PaymentMerchant paymentMerchant = paymentMerchantMapper.selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);
      alipayLogger.info("+++++++++++++++++++++++++++++++++========env test:" + profile);
      String timestamp = UtilDate.getDateFormatter();

      StringBuffer signBuffer = new StringBuffer();
      signBuffer.append("app_id=" + paymentMerchant.getAppId());
      signBuffer.append("&biz_content={");

      StringBuffer jsonBuffer = new StringBuffer();
      jsonBuffer.append("\"timeout_express\":\"30m\",");
      jsonBuffer.append("\"seller_id\":\"" + paymentMerchant.getMerchantId() + "\",");
      jsonBuffer.append("\"product_code\":\"QUICK_MSECURITY_PAY\",");
      jsonBuffer.append("\"total_amount\":\"" + request.getTotalFee().toString() + "\",");
      jsonBuffer.append("\"subject\":\"" + request.getSubject() + "\",");
      jsonBuffer.append("\"body\":\"支付宝支付\",");
      jsonBuffer.append("\"out_trade_no\":\"" + request.getTradeNo() + "\"");

      signBuffer.append(jsonBuffer.toString());
      signBuffer.append("}");
      signBuffer.append("&charset=utf-8");
      signBuffer.append("&method=alipay.trade.app.pay");
      signBuffer.append("&notify_url=" + paymentMerchant.getNotifySite());
      signBuffer.append("&sign_type=" + paymentMerchant.getSignType());
      signBuffer.append("&timestamp=" + timestamp);
      signBuffer.append("&version=1.0");

      String rsaSign = null;
      try {
        rsaSign = AlipaySignature
                .rsaSign(signBuffer.toString(), paymentMerchant.getSecretKey(), charset,
                        paymentMerchant.getSignType());
      } catch (AlipayApiException e) {
        log.error("建立支付宝签名时出错", request);
      }

      if (rsaSign == null) {
        throw new BizException(GlobalErrorCode.SIGN_ERROR, "建立支付宝签名时出错");
      }

      StringBuffer encodeBufer = new StringBuffer();
      encodeBufer.append("app_id=" + paymentMerchant.getAppId());
      try {
        encodeBufer.append("&biz_content={" + URLEncoder.encode(jsonBuffer.toString(), charset) + "}");
        encodeBufer.append("&charset=utf-8");
        encodeBufer.append("&method=alipay.trade.app.pay");
        encodeBufer.append("&notify_url=" + paymentMerchant.getNotifySite());
        encodeBufer.append("&sign_type=" + paymentMerchant.getSignType());
        encodeBufer.append("&timestamp=" + URLEncoder.encode(timestamp, charset));
        encodeBufer.append("&version=1.0");
        encodeBufer.append("&sign=" + URLEncoder.encode(rsaSign, charset));

      } catch (UnsupportedEncodingException e) {
        throw new BizException(GlobalErrorCode.SIGN_ERROR, "encode reponse encode error");
      }

      AliPayMsg apm = new AliPayMsg();
      apm.setCode("1");
      apm.setMsg("success");
      Map<String, String> map = new HashMap<String, String>();
      map.put("alipay", encodeBufer.toString());
      apm.setData(map);

      JSONObject resultJson = JSONObject.parseObject(JSON.toJSONString(apm));

      responseBody(resultJson.toString(), resp);
    }catch (Exception e){
      throw new BizException(GlobalErrorCode.SIGN_ERROR, "sign error");
    }

    return null;
  }

  @Override
  public PaymentResponseVO payCallback(HttpServletRequest req, HttpServletResponse resp) {
    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);
    Map<String, String> params = extractParams(req);
    alipayLogger.info("alipay callback " + params);
    if (!AlipayNotify
        .verifyReturn(params, paymentMerchant.getSignType(), paymentMerchant.getPubKey(),paymentMerchant.getSecretKey())) {
      return null;
    }
    PaymentResponseVO p = new PaymentResponseVO();
    p.setPaymentMode(PaymentMode.ALIPAY);
    p.setBillStatus(PaymentStatus.SUCCESS);
    // 商户订单号
    p.setBillNo(req.getParameter("out_trade_no"));
    // 支付宝交易号
    p.setBillNoPay(req.getParameter("trade_no"));
    // 交易状态
    p.setBillStatus(PaymentStatus.SUCCESS);
    p.setPaymentMerchantId(paymentMerchant.getId());
    super.savePayResult(p);

    return p;
  }

  @Override
  public PaymentResponseVO payNotify(HttpServletRequest req, HttpServletResponse resp) {
    PaymentMerchant paymentMerchant;
    if(epsStatus()){
      paymentMerchant = paymentMerchantMapper
              .selectByTradeType(profile, "ALIPAY", PaymentMode.EPS);

      Map<String, String> params = extractParams(req);
      Map map = null;
      for (String key : params.keySet()) {
        try {
          map = XMLUtil.doXMLParse(key);
        } catch (JDOMException | IOException e) {
          e.printStackTrace();
        }
      }
      if(map == null){
        alipayLogger.error("支付回调返回参数失败");
        return null;
      }
      if("0".equals(map.get("status").toString()) || "0".equals(map.get("result_code").toString())){
        PaymentResponseVO paymentResponseVO = new PaymentResponseVO();
        paymentResponseVO.setPaymentMode(PaymentMode.ALIPAY);
        paymentResponseVO.setBillNo(map.get("out_trade_no").toString());// 商户订单号
        paymentResponseVO.setBillNoPay(map.get("transaction_id").toString());
        paymentResponseVO.setPaymentMerchantId(paymentMerchant.getId());
        PaymentStatus status;
        String s = map.get("pay_result").toString();

        if ("0".equalsIgnoreCase(s)) {
          status = PaymentStatus.SUCCESS;
        } else {
          status = PaymentStatus.PENDING;
        }

        paymentResponseVO.setBillStatus(status);
        cashierService.handlePaymentResponse(paymentResponseVO.getBillNo(), PaymentMode.ALIPAY, paymentResponseVO);

        return paymentResponseVO;
      }
    }else{
      paymentMerchant = paymentMerchantMapper
              .selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);
      Map<String, String> params = extractParams(req);

      alipayLogger.info("alipay notify " + params);

      try {
        if (!AlipayNotify
                .verifyNotify(params, paymentMerchant.getMerchantId(), paymentMerchant.getSignType(),
                        paymentMerchant.getPubKey(), paymentMerchant.getSecretKey())) {
          alipayLogger.info("AlipayNotify.verifyNotify failed.");
          return null;
        }
      } catch (Exception e) {
        alipayLogger.warn("payment verify failed.", e);
        return null;
      }

      try {
        // 解密（如果是RSA签名需要解密，如果是MD5签名则下面一行清注释掉）
        if (paymentMerchant.getSignType().equals("0001")) {
          params = AlipayNotify.decrypt(params, paymentMerchant.getSecretKey());
        }
        PaymentResponseVO p = new PaymentResponseVO();
        p.setPaymentMode(PaymentMode.ALIPAY);
        // 商户订单号
        p.setBillNo(params.get("out_trade_no"));
        // 支付宝交易号
        p.setBillNoPay(params.get("trade_no"));
        // 交易状态
        PaymentStatus status;
        String s = params.get("trade_status");

        if ("TRADE_SUCCESS".equalsIgnoreCase(s)) {
          status = PaymentStatus.SUCCESS;
        } else if ("TRADE_FINISHED".equalsIgnoreCase(s)) {
          status = PaymentStatus.SUCCESS;
        } else if ("TRADE_CLOSED".equalsIgnoreCase(s)) {
          status = PaymentStatus.CLOSED;
        } else {
          status = PaymentStatus.PENDING;
        }

        p.setBillStatus(status);
        p.setBuyerEmail(params.get("seller_email"));
        p.setBuyerId(params.get("buyer_id"));
        p.setTotalFee(params.get("total_amount"));
        p.setDetail(params.toString());
        p.setBillStatus(status);
        p.setPaymentMerchantId(paymentMerchant.getId());
        cashierService.handlePaymentResponse(p.getBillNo(), PaymentMode.ALIPAY, p);

        return p;
      } catch (Exception e) {
    	  if(e instanceof PayNotifyException) {
        	  PayNotifyException ep = (PayNotifyException) e;
        	  throw new PayNotifyException(ep.getCode(),ep.getMsg(),ep.getData());
          }
        alipayLogger.error("payment verify ok, but parse data failed.", e);
        return null;
      }
    }
    return null;
  }

  @SuppressWarnings("rawtypes")
  private Map<String, String> extractParams(HttpServletRequest req) {
    Map<String, String> params = new HashMap<String, String>();
    Map requestParams = req.getParameterMap();
    for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
      String name = (String) iter.next();
      String[] values = (String[]) requestParams.get(name);
      String valueStr = "";
      for (int i = 0; i < values.length; i++) {
        valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
      }
      params.put(name, valueStr);
    }
    return params;
  }

  @Override
  public List<String> loadBankList() {
    return new ArrayList<String>();
  }

  @Override
  public PaymentResponseVO payRefund(HttpServletRequest req, HttpServletResponse resp, PaymentRequestVO request) throws Exception {
    return null;
  }

  @Override
  public void payBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> params) throws Exception {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);

    // 基础数据
    Map<String, String> sParaTemp = new HashMap<String, String>();
    sParaTemp.put("service", "batch_trans_notify");
    sParaTemp.put("partner", paymentMerchant.getMerchantId());
    sParaTemp.put("_input_charset", charset);
    //签名后设置
    sParaTemp.put("sign_type", "");
    sParaTemp.put("sign", "");
    sParaTemp.put("notify_url", paymentMerchant.getNotifySite() + "/bos" + paymentBatch_notify_url);
    //业务数据
    //付款帐号名
    sParaTemp.put("account_name", paymentMerchant.getMerchantName());
    sParaTemp.put("batch_no", UniqueNoUtils.next(UniqueNoType.P));
    sParaTemp.put("pay_date", getCurrentDate());
    sParaTemp.put("email", paymentMerchant.getMerchantAccount());
    sParaTemp.put("buyer_account_name", paymentMerchant.getMerchantAccount());
    //合并收款人的相关信息
    mergeReceiveDetailsPayRequestBatch(params, sParaTemp);

    alipayLogger.info("alipay batch " + params);

    // 直接返回页面内容
    resp.setContentType("text/html");
    String html = AlipaySubmit
        .buildRequest(ALIPAY_GATEWAY_BATCH, sParaTemp, "get", "确认", paymentMerchant.getSignType(),
            paymentMerchant.getSecretKey());
    System.out.println(html);
    PrintWriter writer = resp.getWriter();
    try {
      writer.print(html);
      writer.flush();
    } finally {
      writer.close();
    }
  }

  private static void mergeReceiveDetailsPayRequestBatch(List<ReceiverDetailVO> params,
      Map<String, String> sParaTemp) {
    String str = "";
    BigDecimal totalFee = new BigDecimal("0");
    if (params != null) {
      for (int i = 0; i < params.size(); i++) {
        if (!str.equals("")) {
          str += "|";
        }
        ReceiverDetailVO vo = (ReceiverDetailVO) params.get(i);
        String temp = vo.getSwiftNum() + "^" + vo.getReceiverAccount() + "^" + vo.getReceiverName()
            + "^" + vo.getReceiverFee() + "^" + vo.getReceiverRemark();
        str += temp;

        totalFee = totalFee.add(vo.getReceiverFee());
      }
    }
    sParaTemp.put("detail_data", str);
    sParaTemp.put("batch_fee", totalFee.toString());
    sParaTemp.put("batch_num", params.size() + "");
  }

  private static void mergeDetailsDrawBackBatch(List<ReceiverDetailVO> params,
      Map<String, String> sParaTemp) {
    String str = "";
    if (params != null) {
      for (int i = 0; i < params.size(); i++) {
        if (!str.equals("")) {
          str += "#";
        }
        ReceiverDetailVO vo = (ReceiverDetailVO) params.get(i);
        String temp = vo.getSwiftNum() + "^" + vo.getReceiverFee() + "^" + vo.getReceiverRemark();
        str += temp;
      }
    }
    sParaTemp.put("detail_data", str);
    sParaTemp.put("batch_num", params.size() + "");
  }

  private static String getCurrentDate() {
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyyMMdd");
    return localSimpleDateFormat.format(new Date(System.currentTimeMillis()));
  }

  private static String getCurrentTimestamp() {
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    return localSimpleDateFormat.format(new Date(System.currentTimeMillis()));
  }

  public Map<String, String> payBatchNotify(HttpServletRequest req, HttpServletResponse resp) {
    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);
    Map<String, String> params = extractParams(req);
    alipayLogger.info("alipay batch " + params);
    try {
      if (!AlipayNotify.verifyNotifyPayBatch(params, paymentMerchant.getMerchantId(),
          paymentMerchant.getSignType(),
          paymentMerchant.getPubKey(), paymentMerchant.getSecretKey(),
          AliPaymentImpl.paymentBatch_notify_url)) {
        log.info("AlipayNotify.verifyNotify failed.");
        return null;
      }
    } catch (Exception e) {
      log.warn("paymentBatch verify failed.", e);
      return null;
    }
    return params;
  }

  @Override
  public void refundBatchRequest(HttpServletRequest req,
      HttpServletResponse resp, List<ReceiverDetailVO> params) throws Exception {
    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);
    //基础数据
    alipayLogger.info("alipay batch refund " + params);
    Map<String, String> sParaTemp = new HashMap<String, String>();
    sParaTemp.put("service", "refund_fastpay_by_platform_pwd");
    sParaTemp.put("partner", paymentMerchant.getMerchantId());
    sParaTemp.put("_input_charset", charset);
    //签名后设置
    sParaTemp.put("sign_type", "");
    sParaTemp.put("sign", "");
    sParaTemp.put("notify_url",
        paymentMerchant.getNotifySite() + "/bos" + refundBatch_notify_url + "/" + params.get(0)
            .getOrderNo());

    //业务数据
    //付款帐号名
    sParaTemp.put("seller_email", paymentMerchant.getMerchantName());
    sParaTemp.put("seller_user_id", paymentMerchant.getMerchantId());
    sParaTemp.put("refund_date", getCurrentTimestamp());
    String batch_no = UniqueNoUtils.next();

    sParaTemp.put("batch_no", batch_no);
    //合并收款人的相关信息
    mergeDetailsDrawBackBatch(params, sParaTemp);

    // 直接返回页面内容
    resp.setContentType("text/html");
    String html = AlipaySubmit
        .buildRequest(ALIPAY_GATEWAY_BATCH, sParaTemp, "get", "确认", paymentMerchant.getSignType(),
            paymentMerchant.getSecretKey());
    System.out.println(html);
    PrintWriter writer = resp.getWriter();
    try {
      writer.print(html);
      writer.flush();
    } finally {
      writer.close();
    }
  }

  @Override
  public Map<String, String> refundBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);
    Map<String, String> params = extractParams(req);
    alipayLogger.info("alipay batch refund notify " + params);
    try {
      if (!AlipayNotify.verifyNotifyPayBatch(params, paymentMerchant.getMerchantId(),
          paymentMerchant.getSignType(),
          paymentMerchant.getPubKey(), paymentMerchant.getSecretKey(),
          AliPaymentImpl.refundBatch_notify_url)) {
        log.info("AlipayNotify.verifyNotify failed.");
        return null;
      }
    } catch (Exception e) {
      log.warn("paymentBatch verify failed.", e);
      return null;
    }
    return params;
  }

  public Pair<String, AlipayTradeQueryResponse> queryRet(PaidAble paidAble) {
    log.info("start execute alipay queryRet()主动查询方法, current query payNo is = {}, orderPayType is = {}",
            paidAble.getPayNo(), paidAble.getPayType());
    if (StringUtils.isEmpty(paidAble.getPayNo())) {
      alipayLogger.error("查询失败，支付编号为空");
      return null;
    }
    try {
      PaymentMerchant paymentMerchant =
          paymentMerchantMapper.selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);

      log.info("alipay queryRet()主动查询, paymentMerchant = {}, current profile = {}", paymentMerchant, profile);

      AlipayClient alipayClient =
          new DefaultAlipayClient(
              ALIPAY_GATEWAY_NEW,
              paymentMerchant.getAppId(),
              paymentMerchant.getSecretKey(),
              "json",
              charset,
              paymentMerchant.getPubKey(),
              paymentMerchant.getSignType());

      AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
      request.setBizContent("{" + "\"out_trade_no\":\"" + paidAble.getPayNo() + "\"" + "}");

      log.info("alipay queryRet()主动查询, alipayClient = {}, alipayRequest = {}", alipayClient, request);

      return Pair.of(paymentMerchant.getPaymentId(), alipayClient.execute(request));
    } catch (AlipayApiException e) {
      log.error("支付宝主动查询失败, ", e);
    }
    return null;
  }

  private boolean queryOrder(Order order) {
    Pair<String, AlipayTradeQueryResponse> pair = queryRet(order);
    AlipayTradeQueryResponse response = pair.getRight();
    if (response == null) {
      return false;
    }
    if (response.isSuccess()) {
      alipayLogger.info("调用支付宝查询成功");
      return true;
    } else {
      alipayLogger.error("调用支付宝查询失败");
      return false;
    }
  }

  @Override
  public PaymentResponseVO payRefund(PaymentRequestVO request, Order order) throws Exception {
    if(!queryOrder(order)){//调用支付宝订单查询接口
      return payRefundForEps(order);
    }else{
      return payRefund(request);
    }
  }

  private PaymentResponseVO payRefund(PaymentRequestVO request)
      throws Exception {
    log.info("start execute eps refund, 支付宝查询不到, 开始执行 eps 退款处理, " +
                    "当前处理的订单号 orderNo={}, 支付单号 payNo={}, 支付类型 payType={}, 当前 profile = {}" ,
            request.getOrderIds(), request.getOutTradeNo(), request.getPayType(), profile);

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);

    if (paymentMerchant == null)
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "获取支付配置信息失败");

    log.info("alipay refund paymentMerchant = {}", paymentMerchant);

    AlipayClient alipayClient = new DefaultAlipayClient(ALIPAY_GATEWAY_NEW,
        paymentMerchant.getAppId(),
        paymentMerchant.getSecretKey(), "json", charset, paymentMerchant.getPubKey(),
        paymentMerchant.getSignType());

    AlipayTradeRefundRequest alipayTradeRefundRequest = new AlipayTradeRefundRequest();

    String jsonBuffer = ("\"out_trade_no\":\"" + request.getOutTradeNo() + "\",") +
            "\"refund_amount\":\"" + request.getTotalFee().toString() + "\"," +
            "\"out_request_no\":\"" + request.getOutRequestNo() + "\"";
    alipayTradeRefundRequest.setBizContent("{" + jsonBuffer + "}");

    AlipayTradeRefundResponse response = alipayClient.execute(alipayTradeRefundRequest);

    alipayLogger.info("支付宝退款响应:{}",JSON.toJSONString(response));

    return null;
  }

  private PaymentResponseVO payRefundForEps(Order order) {
    log.info("start execute eps refund, 支付宝查询不到, 开始执行 eps 退款处理, " +
                    "当前处理的订单号 orderNo={}, 支付单号 payNo={}, 支付类型 payType={}, 当前 profile = {}" ,
            order.getOrderNo(), order.getPayNo(), order.getPayType(), profile);
    PaymentConfig paymentConfig = paymentFacade.loadPayment(profile, PaymentMode.EPS, TradeType.ALIPAY.toString());
    if (null == paymentConfig) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "获取支付配置信息失败");
    }
    String mchSecret = paymentConfig.getMchSecret();
    String merchantId = paymentConfig.getMchId();
    if (null == mchSecret || null == merchantId) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "商户号配置为空");
    }

    //获取整个主订单的消费金额
    Optional<MainOrder> mainOrderOptional = Optional.ofNullable(mainOrderService.load(order.getMainOrderId()));
    if(!mainOrderOptional.isPresent() || null == mainOrderOptional.get().getTotalFee()){
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "订单金额不合法");
    }

    Map<String, String> params = new HashMap<>();
    params.put("service", "unified.trade.refund");  //接口类型
    params.put("mch_id", merchantId); //商户号
    params.put("out_trade_no", order.getPayNo()); //填入商家订单号
    params.put("out_refund_no", order.getOrderNo());  //填入退款订单号
    params.put("total_fee", "" + mainOrderOptional.get().getTotalFee().multiply(new BigDecimal(100)).intValue());//填入总金额
    params.put("refund_fee", "" + order.getRefundFee().multiply(new BigDecimal(100)).intValue());//填入退款金额
    params.put("op_user_id", merchantId); //操作员Id，默认就是商户号
    params.put("nonce_str", CommonUtil.CreateNoncestr());  //随机字符串

    try {
      String sign = MD5SignUtil.Sign(CommonUtil.FormatQueryParaMap(params, false), mchSecret);
      params.put("sign", sign);
    } catch (SDKRuntimeException e) {
      alipayLogger.error("alipay refund sign failed error pay=" + order.getPayNo() + "  paramsMap=" + params);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "生成退款支付签名信息出错");
    }

    try {
      String postEpsSSL = PoolingHttpClients.postEpsSSL(EPS_ALIPAY_URL, com.xquark.service.outpay.impl.tenpay.XMLUtil.toXml(params));
      if (postEpsSSL != null) {
        Map map = com.xquark.service.outpay.impl.tenpay.XMLUtil.doXMLParse(postEpsSSL);
        if (map != null) {
          alipayLogger.info("EPS支付宝退款返回map:{}", JSON.toJSONString(map));

          String status = (String) map.get("status");
          String resultCode = (String) map.get("result_code");

          if((StringUtils.isBlank(status) || StringUtils.isBlank(resultCode))){
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "EPS退款返回失败");
          }

          if (("0".equals(status) && "0".equals(resultCode))) {
            orderRefundService.successByOrderId(order.getId());//修改退款订单状态为成功
          }else{
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "EPS退款返回失败");
          }
        }
      }else{
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "EPS退款返回失败");
      }
    }catch (Exception e) {
      alipayLogger.error("alipay refund keyStore failed error pay=" + order.getPayNo() + "  paramsMap=" + params);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "EPS退款返回失败");
    }

    return null;
  }

  @Override
  public PaymentResponseVO payCancel(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payRefundNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PaymentResponseVO payConfirm(HttpServletRequest req,
      HttpServletResponse resp, PaymentRequestVO request)
      throws Exception {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean sendSmsAgain(PaymentRequestVO request) throws Exception {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void closeTrade(String orderNo) {

    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);

    alipayLogger.info("alipay close orderNo=[" + orderNo + "]");
    // 基础数据
    Map<String, String> sParaTemp = new HashMap<String, String>();
    sParaTemp.put("service", "close_trade");
    sParaTemp.put("partner", paymentMerchant.getMerchantId());
    sParaTemp.put("_input_charset", charset);
    //签名后设置
    sParaTemp.put("sign_type", "");
    sParaTemp.put("sign", "");

    //业务数据
    //付款帐号名
    sParaTemp.put("out_order_no", orderNo);
    sParaTemp.put("trade_role", "S");

    String strxml = "";
    try {
      strxml = AlipaySubmit
          .buildRequest(ALIPAY_GATEWAY_BATCH, "", "", sParaTemp, paymentMerchant.getSignType(),
              paymentMerchant.getSecretKey());
      alipayLogger.info("alipay close trade resultXml content:" + strxml);
//			strxml = URLDecoder.decode(strxml, charset);
//			log.info("alipay close trade resultXml decode:"+strxml);
    } catch (Exception e) {
      alipayLogger.error("请求支付宝关闭订单接口异常，orderNo=" + orderNo);
    }
  }

  @SuppressWarnings("rawtypes")
  @Override
  public void tradeAccountReport(String startTime, String endTime) {
    PaymentMerchant paymentMerchant = paymentMerchantMapper
        .selectByTradeType(profile, "WEB", PaymentMode.ALIPAY);
    Map<String, String> sParaTemp = new HashMap<String, String>();
    sParaTemp.put("service", "export_trade_account_report");
    sParaTemp.put("partner", paymentMerchant.getMerchantId());
    sParaTemp.put("_input_charset", charset);
    //签名后设置
    sParaTemp.put("sign_type", "");
    sParaTemp.put("sign", "");

    //业务数据
    sParaTemp.put("gmt_create_start", startTime);
    sParaTemp.put("gmt_create_end", endTime);
    sParaTemp.put("user_id", paymentMerchant.getMerchantId());
    sParaTemp.put("no_coupon", "Y");
    sParaTemp.put("trans_code", "6001");

    String strxml = "";
    try {
      strxml = AlipaySubmit
          .buildRequest(ALIPAY_GATEWAY_BATCH, "", "", sParaTemp, paymentMerchant.getSignType(),
              paymentMerchant.getSecretKey());
      strxml = URLDecoder.decode(strxml, charset);
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "alipay close trade ", e);
    }

    if (StringUtils.isNotBlank(strxml)) {
      try {
        Map respMap = XMLUtil.doXMLParse(strxml);
        if (respMap != null) {
          String isSuccess = respMap.get("is_success").toString();
          if (isSuccess.equals("T")) {
            log.info("支付宝对账接口返回成功!");
            String content = ((Map) ((Map) respMap.get("response")).get("csv_result"))
                .get("csv_data").toString();
            int count = Integer.parseInt(
                ((Map) ((Map) respMap.get("response")).get("csv_result")).get("count").toString());
            if (StringUtils.isNotBlank(content) && count > 0) {
              String[] dataArray = content.split(",");
              int loop = 1;
              int cellCount = 14;
              while (loop <= count) {
                String orderNo = dataArray[loop * cellCount];
                if (StringUtils.isNotBlank(orderNo)) {
                  OrderVO order = orderService.loadByOrderNo(orderNo);
                  if (order != null && order.getStatus() == OrderStatus.SUBMITTED) {
                    log.error("订单(orderNo=" + orderNo + ")状态异常，支付宝已支付");
                  }
                }
                loop++;
              }
            }

          } else if (isSuccess.equals("F")) {
            log.info("支付宝对账接口返回成功!");
          } else {
            log.error("unknow Exception");
          }
        }
      } catch (JDOMException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }


  @Override
  public void payLock(String couponId) {

  }


  @Override
  public void payUnLock(String couponId) {

  }

  private class AliPayMsg {
    private String code;
    private String msg;
    private Object data;

    public String getCode() {
      return code;
    }

    public void setCode(String code) {
      this.code = code;
    }

    public String getMsg() {
      return msg;
    }

    public void setMsg(String msg) {
      this.msg = msg;
    }

    public Object getData() {
      return data;
    }

    public void setData(Object data) {
      this.data = data;
    }
  }
}
