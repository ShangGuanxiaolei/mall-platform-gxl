package com.xquark.service.outpay.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xquark.dal.mapper.OutpayAgreementMapper;
import com.xquark.dal.model.OutpayAgreement;
import com.xquark.dal.model.UserPayBankVO;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.outpay.OutPayAgreementService;

@Component("outPayAgreementService")
public class OutPayAgreementServiceImpl extends BaseServiceImpl implements OutPayAgreementService {

  @Autowired
  private OutpayAgreementMapper outpayAgreementMapper;

  @Override
  public OutpayAgreement load(String id) {
    return outpayAgreementMapper.selectByPrimaryKey(id);
  }

  @Override
  public OutpayAgreement loadBypayAgreeId(String userId, String payAgreeId, PaymentMode payMode) {
    return outpayAgreementMapper.findByPayAgreeId(userId, payAgreeId, payMode);
  }

  @Override
  public int insert(OutpayAgreement snapshot) {
    return outpayAgreementMapper.insert(snapshot);
  }

  @Override
  public int insertOrder(OutpayAgreement outpayAgreement) {
    return outpayAgreementMapper.insert(outpayAgreement);
  }

  @Override
  public List<OutpayAgreement> listByUserId(String userId) {
    return outpayAgreementMapper.listByUserId(userId);
  }

  @Override
  public List<UserPayBankVO> listBankByUserId(String userId) {
    List<UserPayBankVO> vos = outpayAgreementMapper.listBankByUserId(userId);
    for (UserPayBankVO vo : vos) {
      vo.setCardTypeName(vo.getCardType().equals("CREDITCARD") ? "信用卡" : "储蓄卡");
    }
    return vos;
  }

  @Override
  public int unbind(String id) {
    return outpayAgreementMapper.unbind(id);
  }
}
