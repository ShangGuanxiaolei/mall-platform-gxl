package com.xquark.service.outpay;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.xquark.dal.mapper.PaymentMerchantMapper;
import com.xquark.dal.model.PaymentMerchant;
import com.xquark.dal.type.PaymentMode;
import com.xquark.thirds.umpay.api.util.PkCertFactory;
import com.xquark.thirds.umpay.api.util.UmConfig;

@Component
public class UmpayFactory implements InitializingBean {

  @Autowired
  private PaymentMerchantMapper paymentMerchantMapper;

  @Value("${profiles.active}")
  String profile;

  @Override
  public void afterPropertiesSet() throws Exception {
    Map<String, String> config = new HashMap<String, String>();
    List<PaymentMerchant> merchants = paymentMerchantMapper.list(profile, PaymentMode.UMPAY);
    for (PaymentMerchant merchant : merchants) {
      config.put(PkCertFactory.platCertPath, merchant.getPubKey());
      config.put(merchant.getMerchantId() + PkCertFactory.pkSuffix, merchant.getSecretKey());
    }
    UmConfig.init(config);
  }
}
