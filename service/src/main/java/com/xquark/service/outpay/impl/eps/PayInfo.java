package com.xquark.service.outpay.impl.eps;

/**
 * Created by wangxinhua. Date: 2018/8/30 Time: 上午11:04
 */
public class PayInfo {

  private String appId;

  private String timeStamp;

  private String nonceStr;

  // 实际字段package, 与保留字冲突, 使用getter, setter
  private String pack;

  private String signType;

  private String paySign;

  public String getAppId() {
    return appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public String getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
  }

  public String getNonceStr() {
    return nonceStr;
  }

  public void setNonceStr(String nonceStr) {
    this.nonceStr = nonceStr;
  }

  public String getPackage() {
    return pack;
  }

  public void setPackage(String pack) {
    this.pack = pack;
  }

  public String getSignType() {
    return signType;
  }

  public void setSignType(String signType) {
    this.signType = signType;
  }

  public String getPaySign() {
    return paySign;
  }

  public void setPaySign(String paySign) {
    this.paySign = paySign;
  }
}
