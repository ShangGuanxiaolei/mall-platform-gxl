package com.xquark.service.pricing.vo;

import com.alibaba.fastjson.JSON;

/**
 * @auther liuwei
 * @date 2018/6/16 23:43
 */
public class ResponsePolySysnStock<T> {
  private String code;
  private String message;
  private T Quantity;

  public ResponsePolySysnStock() {
  }

  public ResponsePolySysnStock(String code, String message) {
    this(code, message, null);
  }

  public ResponsePolySysnStock(String code, String message, T quantity) {
    this.code = code;
    this.message = message;
    Quantity = quantity;
  }

  public static <T> ResponsePolySysnStock<T> error(String code, String message) {
    return new ResponsePolySysnStock<>(code, message);
  }

  public static <T> ResponsePolySysnStock<T> success(T t) {
    return new ResponsePolySysnStock<>("10000", "SUCCESS", t);
  }

  public String toJson() {
    return JSON.toJSONString(this);
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public T getQuantity() {
    return Quantity;
  }

  public void setQuantity(T quantity) {
    Quantity = quantity;
  }

  @Override
  public String toString() {
    return "ResponsePolySysnStock{" +
        "code='" + code + '\'' +
        ", message='" + message + '\'' +
        ", Quantity=" + Quantity +
        '}';
  }
}
