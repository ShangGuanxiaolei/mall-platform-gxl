package com.xquark.service.pricing.impl.pricing.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.xquark.dal.type.PromotionType.COUPON;
import static com.xquark.service.pricing.impl.pricing.DiscountPricingResult.FINAL_USING_COUPON_KEY;
import static com.xquark.service.pricing.impl.pricing.DiscountPricingResult.VALID_COUPONS_KEY;

import com.xquark.dal.mapper.TermRelationshipMapper;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.UserCouponType;
import com.xquark.dal.vo.UserCouponVo;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.coupon.UserCouponService;
import com.xquark.service.pricing.impl.pricing.DiscountPricingChain;
import com.xquark.service.pricing.impl.pricing.DiscountPricingResult;
import com.xquark.service.pricing.impl.pricing.PricingChainParams;
import com.xquark.service.pricing.impl.pricing.PricingContext;
import com.xquark.service.pricing.impl.pricing.SkuDiscountDetail;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import com.xquark.service.shop.ShopService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

public class CouponDiscountPricingChain extends DiscountPricingChain {

  private final UserCouponService userCouponService;

  private final ShopService shopService;

  private final TermRelationshipMapper termRelationshipMapper;

  public CouponDiscountPricingChain(UserCouponService userCouponService,
      ShopService shopService,
      TermRelationshipMapper termRelationshipMapper) {
    this.userCouponService = userCouponService;
    this.shopService = shopService;
    this.termRelationshipMapper = termRelationshipMapper;
  }

  @Override
  protected DiscountPricingResult calculatePrice(PricingChainParams params,
      PricingContext context) {

    DiscountPricingResult result = DiscountPricingResult.emptyResult();

    User buyer = params.getBuyer();
    Shop shop = params.getShop();
    String rootShopId = shopService.loadRootShop().getId();
    BigDecimal goodsFee = context.getCurrFee();
    BigDecimal currentDiscountFee = context.getCurrDiscount();

    UserSelectedProVO userSelectedProVO = params.getUserSelectedPro();

    List<String> productIds = new ArrayList<>();
    Map<String, BigDecimal> productFees = new HashMap<>();
    Map<String, BigDecimal> categoryFees = new HashMap<>();

    Map<String, BigDecimal> skuFees = new HashMap<>();
    // 本订单所有商品类别id，用于过滤商品类别优惠券
    for (CartItemVO cartItem : params.getCartItems()) {
      productIds.add(cartItem.getProductId());
      // 商品总优惠金额
      BigDecimal skuFee = cartItem.getSku().getPrice()
          .multiply(new BigDecimal(cartItem.getAmount()));
      productFees.put(cartItem.getProductId(), skuFee);
      skuFees.put(cartItem.getSkuId(), skuFee);
      List<String> categoryIds = termRelationshipMapper
          .getCategoryByProduct(cartItem.getProductId());
      for (String categoryId : categoryIds) {
        BigDecimal price = cartItem.getSku().getPrice()
            .multiply(new BigDecimal(cartItem.getAmount()));
        if (categoryFees.containsKey(categoryId)) {
          BigDecimal fee = categoryFees.get(categoryId);
          categoryFees.put(categoryId, price.add(fee));
        } else {
          categoryFees.put(categoryId, price);
        }
      }
    }

    //计算店铺优惠券，优惠券使用条件计算是以优惠价为基础计算，因此放到最后处理
    List<UserCouponVo> coupons = userCouponService
        .validUserCouponList(buyer.getId(), shop.getId(), rootShopId, productIds);
    UserCouponVo selected = null;
    if (CollectionUtils.isNotEmpty(coupons)) {
      coupons = new LinkedList<>(coupons);
      for (Iterator<UserCouponVo> iterator = coupons.iterator(); iterator.hasNext(); ) {
        UserCouponVo coupon = iterator.next();
        BigDecimal applyAbove = coupon.getApplyAbove();
        //计算是否符合使用条件
        // 店铺类型优惠券，根据订单总金额计算是否达到满减条件
        if (coupon.getType() == UserCouponType.SHOP) {
//					if (coupon != NO_COUPON && (goodsFee.add(logisticsFee ).subtract(discountFee).compareTo(applyAbove) < 0)) {
//						iterator.remove();
//					}
          // 20180410修改优惠券结算逻辑, 不包含运费
          if (goodsFee.subtract(currentDiscountFee).compareTo(applyAbove)
              < 0) {
            iterator.remove();
          }
        }
        // 商品类型优惠券，根据订单内每个商品的金额对应的优惠券计算是否达到满减条件
        else if (coupon.getType() == UserCouponType.PRODUCT) {
          String productId = coupon.getProductId();
          String[] pIds = productId.split(",");
          BigDecimal productFee = BigDecimal.ZERO;
          // 优惠券商品支持多选，如果一个优惠券包含多个商品，则需要累加这几个商品的总金额计算是否满足优惠
          for (String pId : pIds) {
            BigDecimal fee = productFees.get(pId);
            if (fee != null) {
              productFee = productFee.add(fee);
            }
          }
          if (productFee == null || productFee.compareTo(applyAbove) < 0) {
            iterator.remove();
          }
        }
        // 查找多个商品类型的优惠券金额，取最高的一个类型计算是否满足优惠
        else if (coupon.getType() == UserCouponType.CATEGORY) {
          String categoryId = coupon.getCategoryId();
          String[] cIds = categoryId.split(",");
          BigDecimal categoryFee = BigDecimal.ZERO;
          // 优惠券商品支持多选，如果一个优惠券包含多个商品，则需要累加这几个商品的总金额计算是否满足优惠
          for (String cId : cIds) {
            BigDecimal fee = categoryFees.get(cId);
            if (fee != null && fee.compareTo(categoryFee) > 0) {
              categoryFee = fee;
            }
          }
          if (categoryFee == null || (categoryFee.compareTo(applyAbove) < 0)) {
            iterator.remove();
          }
        }

        //从符合条件的优惠券中找到当前选中的
        if (userSelectedProVO != null && MapUtils
            .isNotEmpty(userSelectedProVO.getSelShopCouponMap())) {
          String selectedCouponId = userSelectedProVO.getSelShopCouponMap().get(shop);
          if (StringUtils.equals(selectedCouponId, coupon.getId())) {
            selected = coupon;
          }
        }
      }
    }

    if (CollectionUtils.isNotEmpty(coupons) && selected != null) {
      // 将优惠拆分到每个sku上并加入折扣
      result.addDiscountWithDetail(calculateScope(),
          calculateDiscountDetail(skuFees, selected));
      // 优惠券不在此处排序, 展示时才排序
//			Collections.sort(coupons, new PinnedComparator(selected));
      result.addExtraParam(FINAL_USING_COUPON_KEY, selected);
    }

    // 设置可选优惠
    result.addExtraParam(VALID_COUPONS_KEY, coupons);
    result.getCanUsePromotions().addAll(coupons);
    return result;
  }

  @Override
  protected PromotionType calculateScope() {
    return COUPON;
  }

  /**
   * 将优惠券折扣按比例分摊到每个商品上
   *
   * @param skuFees 商品id-价格 对应关系
   * @param selectedCoupon 选中的优惠券优惠金额
   * @return 每个商品可以优惠的金额
   */
  private Map<String, SkuDiscountDetail> calculateDiscountDetail(Map<String, BigDecimal> skuFees,
      UserCouponVo selectedCoupon) {
    String id = selectedCoupon.getId();
    return divideDiscount(id, skuFees, selectedCoupon.getDiscount(), 2);
  }

}
