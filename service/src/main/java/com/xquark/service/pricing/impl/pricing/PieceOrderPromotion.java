package com.xquark.service.pricing.impl.pricing;

import static com.xquark.dal.type.PromotionType.PIECE_GROUP;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.type.PromotionType;
import java.math.BigDecimal;

/**
 * @author wangxinhua
 * @since 18-11-2
 */
public class PieceOrderPromotion extends Promotion {

  public PieceOrderPromotion(String promotionId, BigDecimal discount) {
    setId(promotionId);
    setTitle("拼团优惠");
    setDiscount(discount);
    setType(PIECE_GROUP);
    setIsFreeDelivery(false);
  }
}
