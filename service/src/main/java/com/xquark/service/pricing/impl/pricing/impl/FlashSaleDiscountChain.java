package com.xquark.service.pricing.impl.pricing.impl;

import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.PromotionConstants;
import com.xquark.dal.model.CartItem;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionPgPrice;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.CouponView;
import com.xquark.dal.vo.FlashSalePromotionProductVO;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pricing.impl.pricing.FlashSaleViewPromotion;
import com.xquark.service.pricing.impl.pricing.PricingChainParams;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.user.UserService;
import io.vavr.collection.Map;
import io.vavr.collection.Stream;
import org.apache.commons.collections4.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * created by
 *
 * @author wangxinhua at 18-7-3 下午8:17
 */
public class FlashSaleDiscountChain extends PromotionDiscountPricingChain {

    protected final FlashSalePromotionProductService flashSaleService;

    public FlashSaleDiscountChain(UserService userService, FlashSalePromotionProductService flashSaleService,
                                  PgPromotionServiceAdapter pgPromotionServiceAdapter) {
        super(userService, pgPromotionServiceAdapter);
        this.flashSaleService = flashSaleService;
    }

    @Override
    protected Promotion loadUseAblePromotion(String productId,
                                             PricingChainParams params) {
        // FIXME 直接查promotion
        FlashSalePromotionProductVO promotionProductVO = (FlashSalePromotionProductVO) flashSaleService
                .loadPromotionProductByPId(productId);
        if (promotionProductVO != null) {
            Promotion promotion = promotionProductVO.getPromotion();
            promotion.setDiscount(promotionProductVO.getDiscount());
            promotion.setIsCountEarning(true);
            promotion.setIsPointUsed(true);
            promotion.setCutDownUsed(true);
            return promotion;
        }
        return null;
    }

    @Override
    protected CouponView buildCouponView(String promotionId, BigDecimal totalDiscount,
                                         boolean freeDelivery) {
        return new FlashSaleViewPromotion(promotionId, totalDiscount);
    }

    @Override
    protected void preCheckExtra(PricingChainParams params, Promotion promotion,
                                 CartItemVO currItem) {

        Date now = new Date();
        Boolean archive = Objects.requireNonNull(promotion.getArchive());
        Date validFrom = Objects.requireNonNull(promotion.getValidFrom());
        Date validTo = Objects.requireNonNull(promotion.getValidTo());

        if (archive || now.after(validTo)) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "您来晚了, 活动已结束");
        }
        if (now.before(validFrom)) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, currItem.getProduct().getName() + "未到开售时间");
        }

        String promotionId = promotion.getId();
        String productId = currItem.getProductId();
        String skuId = currItem.getSkuId();
        String userId = params.getBuyer().getId();
        RedisUtils<Long> redisUtils = RedisUtilFactory.getInstance(Long.class);

        String flashSaleAmountKey = PromotionConstants.getFlashSaleAmountKey(promotionId, skuId);
        Long currAmount = redisUtils.get(flashSaleAmountKey);
        // 当次购买数量
        Long thisAmount = currItem.getAmount().longValue();

        // 商品维度的购买件数
        final Map<String, Long> thisProdAmtMap = groupAmtWithProductId(params.getCartItems());

        // 当前商品的购买数量
        final Long thisProdAmt = thisProdAmtMap.get(productId)
                // 正常不应该发生异常
                .getOrElseThrow(BizException.ofSupplier(GlobalErrorCode.INTERNAL_ERROR, "内部错误， 请稍后再试"));

        if (currAmount == null || currAmount == 0) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "您来晚了, 商品已被抢完");
        }
        // 剩余库存不足
        if (thisAmount > currAmount) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动库存不足");
        }

        final List<PromotionPgPrice> prices =
                pgPromotionServiceAdapter.listByPromotionIdAndProductId(promotionId, productId, PromotionType.FLASHSALE);
        if (CollectionUtils.isEmpty(prices)) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动价格体系未设置");
        }
        final List<String> keys = prices.stream().map(PromotionPgPrice::getSkuId)
                    .map(IdTypeHandler::encode)
                    .map(s -> PromotionConstants.getUserAmountKey(userId, promotionId, s))
                    .collect(Collectors.toList());
        // 用户当前购买数量
        final Long amount = redisUtils.multiGet(keys).stream()
                .reduce(Long::sum).orElse(0L);
        // 用户限购数量
        String flashSaleLimitAmountKey = PromotionConstants.getFlashSaleLimitAmountKey(promotionId, productId);
        Long limitAmount = redisUtils.get(flashSaleLimitAmountKey);

        if (limitAmount == null) {
            // TODO 如果redis中未初始化
            limitAmount = 3L;
        }
        // 限购按购物车的商品维度
        if (amount + thisProdAmt > limitAmount) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, String.format("商品 %s 限购 %d 件哦",
                    currItem.getProduct().getName(), limitAmount));
        }
    }

    @Override
    protected PromotionType calculateScope() {
        return PromotionType.FLASHSALE;
    }

    /**
     * 按商品维度划分每个商品买了几件
     * @return Map<String, Integer> key - 商品id， value - 买了几件
     */
    private Map<String, Long> groupAmtWithProductId(List<? extends CartItem> cartItems) {
        return Stream.ofAll(cartItems).groupBy(CartItem::getProductId)
                .mapValues(items -> items.map(CartItem::getAmount)
                        .map(Integer::longValue)
                        .reduce(Long::sum));
    }
}
