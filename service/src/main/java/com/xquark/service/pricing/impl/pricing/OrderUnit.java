package com.xquark.service.pricing.impl.pricing;

import com.xquark.dal.model.MainOrder;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.User;
import java.util.List;
import java.util.Map;

/**
 * @author wangxinhua.
 * @date 2018/12/17
 */
public class OrderUnit {

  private final User buyer;
  private final MainOrder mainOrder;

  private final Map<Order, List<OrderItem>> orderMap;

  public OrderUnit(User buyer, MainOrder mainOrder,
      Map<Order, List<OrderItem>> orderMap) {
    this.buyer = buyer;
    this.mainOrder = mainOrder;
    this.orderMap = orderMap;
  }

  public User getBuyer() {
    return buyer;
  }

  public MainOrder getMainOrder() {
    return mainOrder;
  }

  public Map<Order, List<OrderItem>> getOrderMap() {
    return orderMap;
  }

}
