package com.xquark.service.pricing.base;

import com.xquark.dal.type.PromotionType;

/**
 * @author: yyc
 * @date: 19-4-19 上午10:52
 */
public class ReservePromotionInfo extends PromotionInfo {

  public ReservePromotionInfo() {}

  public ReservePromotionInfo(String promotionId, PromotionType promotionType) {
    super(promotionId, promotionType);
  }

  public ReservePromotionInfo(String promotionId, PromotionType promotionType, String preOrderNo) {
    super(promotionId, promotionType, preOrderNo);
  }
}
