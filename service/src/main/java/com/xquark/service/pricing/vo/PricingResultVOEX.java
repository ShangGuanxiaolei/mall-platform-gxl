package com.xquark.service.pricing.vo;

import org.springframework.beans.BeanUtils;

@Deprecated
public class PricingResultVOEX extends PricingResultVO {

  public PricingResultVOEX(PricingResultVO pricingResultx) {
    BeanUtils.copyProperties(pricingResultx, this);
  }


  private String shopId;

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

}
