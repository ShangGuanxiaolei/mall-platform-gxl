package com.xquark.service.pricing.impl.pricing.hooks;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.service.pricing.impl.pricing.OrderUnit;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author wangxinhua.
 * @date 2018/12/18
 */
public abstract class OrderItemHook extends OrderHook {

  @Override
  public void accept(OrderUnit unit) {
    for (Entry<Order, List<OrderItem>> entry : unit.getOrderMap().entrySet()) {
      Order order = entry.getKey();
      acceptOrder(order);
      for (OrderItem item : entry.getValue()) {
        acceptItem(order, item);
      }
    }
  }

  protected abstract void acceptOrder(Order order);

  protected abstract void acceptItem(Order order, OrderItem item);
}
