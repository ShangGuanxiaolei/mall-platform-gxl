package com.xquark.service.pricing.base;

import com.xquark.dal.type.PromotionType;

/**
 * @author wangxinhua
 * @since 18-11-1
 * nothing for now
 */
public class FlashSalePromotionInfo extends PromotionInfo {
  public FlashSalePromotionInfo() {
  }

  public FlashSalePromotionInfo(String promotionId, PromotionType promotionType) {
    super(promotionId, promotionType);
  }

}
