package com.xquark.service.pricing.impl;

import com.google.common.base.Preconditions;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.page.PageHelper;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.PromotionUserScope;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.dal.vo.*;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.groupon.ActivityGrouponService;
import com.xquark.service.pricing.PricingService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.pricing.base.PromotionInfo;
import com.xquark.service.pricing.impl.pricing.PricingMode;
import com.xquark.service.pricing.vo.CartPromotionResultVO;
import com.xquark.service.pricing.vo.ConfirmOrderPromotionVO;
import com.xquark.service.pricing.vo.PricingResultVO;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import com.xquark.service.promotion.PromotionInviteCodeService;
import com.xquark.service.user.UserService;
import com.xquark.utils.ResourceResolver;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.Map.Entry;

@Service("promotionService")
public class PromotionServiceImpl extends BaseServiceImpl implements PromotionService {
  //白名单魔法值
  private static final Long MAGIC_NUMBER = 9999999L;
  @Autowired
  private PromotionWhitelistMapper promotionWhitelistMapper;

  @Autowired
  private PromotionConfigMapper configmapper;
  @Autowired
  private PromotionGoodsMapper poGoodMapper;
  @Autowired
  private PromotionBaseInfoMapper promotionBaseMapper;

  @Autowired
  private PromotionMapper promotionMapper;

  @Autowired
  private PricingService pricingService;

  @Autowired
  private UserService userService;

  @Autowired
  private ActivityGrouponService activityGrouponService;

  @Autowired
  private PromotionInviteCodeService promotionInviteCodeService;

  @Override
  public Promotion load(String id) {
    return promotionMapper.selectByPrimaryKey(id);
  }

  @Override
  public Promotion loadByProductId(String productId, Date date) {
    return promotionMapper.selectByProductId(productId, date);
  }

  @Override
  public Promotion loadByProductId(String productId) {
    return this.loadByProductId(productId, null);
  }

  @Override
  public CartPromotionResultVO calculate4Cart(Map<Shop, List<CartItemVO>> cartItemMap, User buyer,
      Address address, boolean useDeduction) {
    return calculate4Cart(cartItemMap, buyer, null, address, useDeduction, true, null,
        PricingMode.CONFIRM);
  }

  @Override
  public CartPromotionResultVO calculate4Cart(Map<Shop, List<CartItemVO>> cartItemMap, User buyer,
      UserSelectedProVO userSelectedPro, Address address,
      boolean useDeduction, boolean autoSelect,
      PromotionInfo promotionInfo, PricingMode pricingMode) {
    Preconditions.checkNotNull(cartItemMap);
    Preconditions.checkNotNull(buyer);

    CartPromotionResultVO ret = new CartPromotionResultVO();

    Map<Shop, List<Promotion>> shopPromotions = new HashMap<>();
    Map<CartItem, List<Promotion>> cartItemPromotions = new HashMap<>();
    Map<Shop, List<? extends CouponView>> shopCoupons = new HashMap<>();
    Map<CartItem, List<UserCouponVo>> cartItemCoupons = new HashMap<>();
    Map<Shop, PricingResultVO> shopPricingResults = new HashMap<>();
    PricingResultVO totalPricingResultVO = PricingResultVO.getZeroPricingResult();

    for (Map.Entry<Shop, List<CartItemVO>> entry : cartItemMap.entrySet()) {
      Shop shop = entry.getKey();
      List<CartItemVO> cartItems = entry.getValue();
      PricingResultVO shopPricingResult = pricingService
          .calculate(pricingMode, shop, cartItems, buyer, userSelectedPro,
              address, useDeduction, promotionInfo);
      shopPricingResults.put(shop, shopPricingResult);
      shopCoupons.put(shop, shopPricingResult.getCouponViews());
      totalPricingResultVO.setLogistics(shopPricingResult.getLogistics());
      // 只针对单shop有效
      accumulate(totalPricingResultVO, shopPricingResult);
    }

    ret.setCartItemCoupons(cartItemCoupons);
    ret.setCartItemPromotions(cartItemPromotions);
    ret.setShopCoupons(shopCoupons);
    ret.setShopPromotions(shopPromotions);
    ret.setShopPricingResult(shopPricingResults);
    ret.setTotalPricingResult(totalPricingResultVO);
    return ret;
  }

  @Override
  public CartPromotionResultVO calculate4CartWithOutPromotions(
      Map<Shop, List<CartItemVO>> cartItemMap, User buyer, Address address) {
    Preconditions.checkNotNull(cartItemMap);
    Preconditions.checkNotNull(buyer);

    CartPromotionResultVO ret = new CartPromotionResultVO();

    Map<Shop, List<Promotion>> shopPromotions = new HashMap<>();
    Map<CartItem, List<Promotion>> cartItemPromotions = new HashMap<>();
    Map<Shop, List<? extends CouponView>> shopCoupons = new HashMap<>();
    Map<CartItem, List<UserCouponVo>> cartItemCoupons = new HashMap<>();
    Map<Shop, PricingResultVO> shopPricingResults = new HashMap<>();
    PricingResultVO totalPricingResultVO = PricingResultVO.getZeroPricingResult();

    for (Map.Entry<Shop, List<CartItemVO>> entry : cartItemMap.entrySet()) {
      Shop shop = entry.getKey();
      List<CartItemVO> cartItems = entry.getValue();
      List<String> productIds = new ArrayList<>();
      for (CartItemVO cartItemVO : cartItems) {
        productIds.add(cartItemVO.getId());
      }

      //按店铺订单计算价格
      PricingResultVO shopPricingResult = pricingService.calculateWithOutPromotions(shop,
          cartItems, buyer, address);
      shopPricingResults.put(shop, shopPricingResult);
      shopCoupons.put(shop, shopPricingResult.getCouponViews());
      // 只针对单shop有效
      accumulate(totalPricingResultVO, shopPricingResult);
    }

    ret.setCartItemCoupons(cartItemCoupons);
    ret.setCartItemPromotions(cartItemPromotions);
    ret.setShopCoupons(shopCoupons);
    ret.setShopPromotions(shopPromotions);
    ret.setShopPricingResult(shopPricingResults);
    ret.setTotalPricingResult(totalPricingResultVO);
    return ret;
  }

  @Override
  public ConfirmOrderPromotionVO calculate4ConfirmOrder(Map<Shop, List<CartItemVO>> cartItemMap,
      User buyer, UserSelectedProVO userSelectedPro, Address address, boolean useDeduction,
      PricingMode pricingMode) {
    return calculate4ConfirmOrder(cartItemMap, buyer, userSelectedPro, address, useDeduction,
        null, null, null, pricingMode);
  }

  @Override
  public ConfirmOrderPromotionVO calculate4ConfirmOrder(Map<Shop, List<CartItemVO>> cartItemMap,
      User buyer, UserSelectedProVO userSelectedPro,
      Address address, boolean useDeduction,
      BigDecimal selectPoint, BigDecimal selectCommission,
      PromotionInfo promotionInfo, PricingMode pricingMode) {
    Preconditions.checkNotNull(cartItemMap);
    Preconditions.checkNotNull(buyer);

    ConfirmOrderPromotionVO ret = new ConfirmOrderPromotionVO();

    Map<Shop, Promotion> shopPromotion = new HashMap<>();
    Map<CartItem, Promotion> cartItemPromotion = new HashMap<>();
    Map<Shop, UserCouponVo> shopCoupon = new HashMap<>();
    Map<CartItem, UserCouponVo> cartItemCoupon = new HashMap<>();
    Map<Shop, PricingResultVO> shopPricingResult = new HashMap<>();
    PricingResultVO totalPricingResult = PricingResultVO.getZeroPricingResult();

    for (Map.Entry<Shop, List<CartItemVO>> entry : cartItemMap.entrySet()) {

      Shop shop = entry.getKey();
      List<CartItemVO> cartItems = entry.getValue();

      //按店铺订单计算价格
      PricingResultVO temp = pricingService
          .calculate4ConfirmOrder(shop, cartItems, buyer, userSelectedPro, address, useDeduction,
              selectPoint, selectCommission, promotionInfo, pricingMode);
      shopPricingResult.put(shop, temp);

      //返回使用的店铺优惠券
      if (temp.getConfirmedShopCoupon() != null) {
        shopCoupon.put(shop, temp.getConfirmedShopCoupon());
      }

      //累加总价
      accumulate(totalPricingResult, temp);
    }

    ret.setShopCoupon(shopCoupon);
    ret.setShopPromotion(shopPromotion);
    ret.setCartItemCoupon(cartItemCoupon);
    ret.setCartItemPromotion(cartItemPromotion);
    ret.setShopPricingResult(shopPricingResult);
    ret.setTotalPricingResult(totalPricingResult);
    return ret;
  }

  private void accumulate(PricingResultVO total, PricingResultVO shopPricingResult) {
    total.setTotalFee(total.getTotalFee().add(shopPricingResult.getTotalFee()));
    total.setDiscountFee(total.getDiscountFee().add(shopPricingResult.getDiscountFee()));
    total.setLogisticsFee(total.getLogisticsFee().add(shopPricingResult.getLogisticsFee()));
    total.setLogisticsDiscount(
        total.getLogisticsDiscount().add(shopPricingResult.getLogisticsDiscount()));
    total.setCommissionLogisticsDiscount(total.getCommissionLogisticsDiscount());//FIXME 积分抵扣的邮费，店铺暂时没有此值
    total.setGoodsFee(total.getGoodsFee().add(shopPricingResult.getGoodsFee()));
    total.setTotalYundouFee(total.getTotalYundouFee() + shopPricingResult.getTotalYundouFee());

    // 以下设置( 覆盖 )只适用于单店铺
    // 如果需要支持多店铺需要merge各个结果
    total.setUsingPromotion(shopPricingResult.getUsingPromotion());
    total.setCouponDiscountFee(shopPricingResult.getCouponDiscountFee());
    total.setDiscountPricingResult(shopPricingResult.getDiscountPricingResult());
    total.setPointRet(shopPricingResult.getPointRet());
    total.setCouponViews(shopPricingResult.getCouponViews());
    total.setLogisticDiscountType(shopPricingResult.getLogisticDiscountType());
    total.setIsPayFree(shopPricingResult.getIsPayFree());

    Map<PromotionType, BigDecimal> thisDetail = total.getDiscountDetail();
    Map<PromotionType, BigDecimal> otherDetail = shopPricingResult.getDiscountDetail();
    for (Entry<PromotionType, BigDecimal> entry : otherDetail.entrySet()) {
      PromotionType key = entry.getKey();
      BigDecimal thisDiscount = thisDetail.get(key) == null ? BigDecimal.ZERO : thisDetail.get(key);
      thisDetail.put(key, entry.getValue().add(thisDiscount));
    }
  }

  @Override
  public int insert(Promotion promotion) {
    return promotionMapper.insert(promotion);
  }

  @Override
  public int update(Promotion promotion) {
    return promotionMapper.update(promotion);
  }

  @Override
  public Promotion loadByNameAndType(String name, PromotionType type) {
    return promotionMapper.selectByNameAndType(name, type);
  }

  /**
   * 活动列表，分页获取某个类型活动的所有信息
   */
  @Override
  public List<PromotionVO> getPromotionForApp(Pageable pager, String type) {
    return promotionMapper.getPromotionForApp(pager, type);
  }

  @Override
  public List<Promotion> listReducePromotionByProductId(String productId, Date date,
      Boolean isShare) {
    return promotionMapper.listReducePromotionByProductId(productId, date, isShare);
  }

  @Override
  public Long countPromotionByProductId(String productId, Date date, Boolean isShare) {
    return promotionMapper.countReducePromotionByProductId(productId, date, isShare);
  }

  @Override
  public Map<String, List<Promotion>> listReduceMapPromotionByProductId(String productId, Date date,
      Boolean isShare) {
    Map<String, List<Promotion>> result = new HashMap<String, List<Promotion>>();
    List<Promotion> reduceList = this.listReducePromotionByProductId(productId, date, isShare);
    if (CollectionUtils.isEmpty(reduceList)) {
      return result;
    }
    for (Promotion promotion : reduceList) {
      String key = promotion.getPromotionType().name();
      List<Promotion> list = result.computeIfAbsent(key, k -> new ArrayList<>());
      list.add(promotion);
    }
    return result;
  }

  @Override
  public List<String> listReducePromotionTitleByProductId(String productId, Date date,
      Boolean isShare) {
    return promotionMapper.listReducePromotionTitleByProductId(productId, date, isShare);
  }

  @Override
  public List<PromotionSimpleReduceVO> listSimpleReduceVO(String productId) {
    return listSimpleReduceVO(productId, null);
  }

  @Override
  public List<PromotionSimpleReduceVO> listSimpleReduceVO(String productId, final String userId) {
    List<PromotionSimpleReduceVO> reduceVOS = promotionMapper
        .listSimpleReducePromotionByProductId(productId);
    // 传了用户id且该用户不是员工则筛掉内购活动
    final boolean isEmployee = userService.isRegistedEmployee(userId);
    if (StringUtils.isNoneBlank(userId)) {
      CollectionUtils.filter(reduceVOS, object -> {
        PromotionUserScope userScope = object.getUserScope();
        return userScope != PromotionUserScope.EMPLOYEE || isEmployee;
      });
    }
    return reduceVOS;
  }

  @Override
  public Boolean hasActivate(PromotionType type) {
    return promotionMapper.selectHasActivate(type);
  }

  /**
   * 首頁查詢活動以及需要櫥窗展示的商品
   */
  @Override
  public PageHelper<PromotionVO> getMeetingSaleProList(Long cpId, Integer pageNum, Integer pageSize,
      String pType) {
    PageHelper<PromotionVO> helper = new PageHelper<>();
    helper.setPage(pageNum);
    helper.setPageSize(pageSize);

    Integer offSet = (pageNum - 1) * pageSize;
    int count = promotionBaseMapper.selectCountByPstatus();
    helper.setPageCount(count);
    List<PromotionBaseInfo> promotionBaseInfos = promotionBaseMapper
        .selectByPstatus(offSet, pageSize);
    List<PromotionVO> promotionVOS = new ArrayList<>();
    if (null != promotionBaseInfos && promotionBaseInfos.size() > 0) {
      for (PromotionBaseInfo promotionBaseInfo : promotionBaseInfos) {
        if (!promotionBaseInfo.getpType().contains("meeting")) {
          continue;
        }

        //校验是否在白名单中且大会是否开启白名单功能
        boolean isOpen = this.checkIsOpen(cpId,promotionBaseInfo.getpCode());
        boolean isWhite = this.checkIsWhite(cpId,promotionBaseInfo.getpCode());
        if(isOpen){
          if(!isWhite){
            continue;
          }
        }

        //活动信息
        PromotionVO promotion = new PromotionVO();
        boolean showCountDown = false;
        //显示方式 1 广告型/2 橱窗型
        String showType = "";
        //首页标题
        String meetingBannerTitle = "";
        //列表标题
        String meetingListTitle = "";
        //广告型  图片URL
        String broadUrl = "";
        // 橱窗型 图片URL
        String showUrl = "";
        //是否展示邀请码
        boolean isCode = false;
        //输入邀请码提示语
        String codeValidRemind = "";
        //邀请码错误提示语
        String codeInvalidRemind = "";
        //是否綁定邀请码
        boolean isBoundingInviteCode = false;
        //查询活动配置信息
        Map map = new HashMap();
        map.put("configType", promotionBaseInfo.getpType());
        //图片路径转换
        ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
            .getBean("resourceFacade");
        List<PromotionConfig> promotionConfigs = configmapper.selectListByConfigType(map);
        if (null != promotionConfigs && promotionConfigs.size() > 0) {
          for (PromotionConfig promotionConfig : promotionConfigs) {
            if ("meetingListTitle".equals(promotionConfig.getConfigName())
                && null != promotionConfig.getConfigValue()
                && !"".equals(promotionConfig.getConfigValue())) {
              meetingListTitle = promotionConfig.getConfigValue();
            }
            if ("meetingBannerTitle".equals(promotionConfig.getConfigName())
                && null != promotionConfig.getConfigValue()
                && !"".equals(promotionConfig.getConfigValue())) {
              meetingBannerTitle = promotionConfig.getConfigValue();
            }
            if ("broadUrl".equals(promotionConfig.getConfigName())
                && null != promotionConfig.getConfigValue()
                && !"".equals(promotionConfig.getConfigValue())) {
              broadUrl = resourceFacade.resolveUrl(promotionConfig.getConfigValue());
            }
            if ("showUrl".equals(promotionConfig.getConfigName())
                && null != promotionConfig.getConfigValue()
                && !"".equals(promotionConfig.getConfigValue())) {
              showUrl = resourceFacade.resolveUrl(promotionConfig.getConfigValue());
            }
            if ("showType".equals(promotionConfig.getConfigName())
                && null != promotionConfig.getConfigValue()
                && !"".equals(promotionConfig.getConfigValue())) {
              showType = promotionConfig.getConfigValue();
            }
            if ("SHOW_COUNT_DOWN".equals(promotionConfig.getConfigName())
                && null != promotionConfig.getConfigValue()
                && !"".equals(promotionConfig.getConfigValue())) {
              if ("1".equals(promotionConfig.getConfigValue())) {
                //1表示显示倒计时
                showCountDown = true;
              }
            }
            if ("isCode".equals(promotionConfig.getConfigName())
                && null != promotionConfig.getConfigValue()
                && !"".equals(promotionConfig.getConfigValue())) {
              if ("on".equals(promotionConfig.getConfigValue())) {
                //true表示需要邀请码
                isCode = true;
              }
            }
            if ("codeValidRemind".equals(promotionConfig.getConfigName())
                && null != promotionConfig.getConfigValue()
                && !"".equals(promotionConfig.getConfigValue())) {
              codeValidRemind = promotionConfig.getConfigValue();
            }
            if ("codeInvalidRemind".equals(promotionConfig.getConfigName())
                && null != promotionConfig.getConfigValue()
                && !"".equals(promotionConfig.getConfigValue())) {
              codeInvalidRemind = promotionConfig.getConfigValue();
            }
          }
        }

        String invateCode = promotionInviteCodeService
            .selectInviteCode(cpId.toString(), promotionBaseInfo.getpCode());
        if (StringUtils.isNotEmpty(invateCode)) {
          isBoundingInviteCode = true;
        }

        Map commenMap = new HashMap();
        commenMap.put("meetingListTitle", meetingListTitle);
        commenMap.put("meetingBannerTitle", meetingBannerTitle);
        commenMap.put("showType", showType);
        commenMap.put("broadUrl", broadUrl);
        commenMap.put("showUrl", showUrl);
        commenMap.put("pCode", promotionBaseInfo.getpCode());
        commenMap.put("isCode", isCode);
        commenMap.put("codeValidRemind", codeValidRemind);
        commenMap.put("codeInvalidRemind", codeInvalidRemind);
        commenMap.put("isBoundingInviteCode", isBoundingInviteCode);
        promotion.setCommentMap(commenMap);

        List<PromotionPGoodsVO> promotionPGoodsVOS = poGoodMapper
            .selectProProductByPcode(promotionBaseInfo.getpCode());
        if (null != promotionPGoodsVOS && promotionPGoodsVOS.size() > 0) {
          //设置有无赠品
          for (PromotionPGoodsVO promotionPGoodsVO : promotionPGoodsVOS) {
            String pCode = promotionBaseInfo.getpCode();
            String productId = String.valueOf(promotionPGoodsVO.getProductId());
            Integer integer = poGoodMapper.qualityGoods(pCode, productId);
            if (integer > 0) {
              promotionPGoodsVO.setQualityGoods(true);
            } else {
              promotionPGoodsVO.setQualityGoods(false);

            }
            //设置是否显示倒计时
            promotionPGoodsVO.setShowCountDown(showCountDown);
            //前台展示的买赠信息
            if (promotionPGoodsVO.getGift() == 1) {
              promotionPGoodsVO.setGiftType("买一赠一");
            } else if (promotionPGoodsVO.getGift() == 2) {
              promotionPGoodsVO.setGiftType("买二赠一");
            } else if (promotionPGoodsVO.getGift() == 3) {
              promotionPGoodsVO.setGiftType("买三赠一");
            }
          }
          promotion.setpGoodsVOS(promotionPGoodsVOS);
          promotion.setType(PromotionType.MEETING);
          promotionVOS.add(promotion);
        }else {
          promotion.setpGoodsVOS(promotionPGoodsVOS);
          promotion.setType(PromotionType.MEETING);
          promotionVOS.add(promotion);
        }
      }
      helper.setResult(promotionVOS);
      return helper;
    } else {
      helper.setPageCount(0);
      helper.setTotalPage(0);
      return helper;
    }
  }

  /**
   * 校验大会是否开启的
   * @param cpId
   * @param pCode
   * @return
   */
  @Override
  public boolean checkIsOpen(Long cpId, String pCode) {

    if(StringUtils.isBlank(pCode) || cpId == null){
      return false;
    }
    return this.promotionWhitelistMapper.checkMettingIsOpen(pCode, MAGIC_NUMBER);
  }

  /**
   * 校验白名单是否开启的
   * @param cpId
   * @param pCode
   * @return
   */
  @Override
  public boolean checkIsWhite(Long cpId, String pCode) {

    if(StringUtils.isBlank(pCode) || cpId == null){
      return false;
    }
    return this.promotionWhitelistMapper.checkIsWhiteExist(pCode, cpId);
  }

    @Override
    public String findPcodeByTypeOrStatusOrPid(String productId, String type, List<String> statusList) {
      String pCode = "";
      List<String> list = this.promotionMapper.selectPcodeByTypeOrStatusOrPid(productId, type, statusList);
      if(list != null && !list.isEmpty()){
          pCode = list.get(0);
      }
      return pCode;
    }

    @Override
  public String findPcode4Meeting(String productId) {
        List<String> list = new ArrayList<>();
        list.add("5");
        String pCode = findPcodeByTypeOrStatusOrPid(productId, "meeting", list);
        if(StringUtils.isBlank(pCode)){
            pCode = "";
        }
        return pCode;
  }
}
