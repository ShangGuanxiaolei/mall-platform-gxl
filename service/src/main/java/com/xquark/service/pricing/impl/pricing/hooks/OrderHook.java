package com.xquark.service.pricing.impl.pricing.hooks;

import com.xquark.service.pricing.impl.pricing.OrderUnit;
import com.xquark.utils.functional.Consumer;

/**
 * @author wangxinhua.
 * @date 2018/12/17
 */
public abstract class OrderHook implements Consumer<OrderUnit> {
}
