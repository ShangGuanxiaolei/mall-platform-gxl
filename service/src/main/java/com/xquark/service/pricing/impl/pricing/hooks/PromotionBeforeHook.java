package com.xquark.service.pricing.impl.pricing.hooks;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.type.OrderSortType;
import com.xquark.service.pricing.impl.pricing.OrderUnit;
import com.xquark.service.pricing.impl.pricing.SkuDiscountDetail;
import org.apache.commons.collections4.MapUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author wangxinhua.
 * @date 2018/12/17 保存订单前将活动信息写入orderItem
 */
public class PromotionBeforeHook extends OrderHook {

  private final Promotion usingPromotion;
  private final Map<String, SkuDiscountDetail> discountDetails;

  public PromotionBeforeHook(
      Promotion usingPromotion, Map<String, SkuDiscountDetail> discountDetails) {
    this.usingPromotion = usingPromotion;
    this.discountDetails = discountDetails;
  }

  @Override
  public void accept(OrderUnit unit) {
    if (MapUtils.isEmpty(discountDetails)) {
      return;
    }
    Map<Order, List<OrderItem>> orderMap = unit.getOrderMap();
    for (Entry<Order, List<OrderItem>> entry : orderMap.entrySet()) {
      Order order = entry.getKey();

      // 保存活动订单活动信息
      OrderSortType orderPromotionType = OrderSortType.valueOf(usingPromotion.getPromotionType().toString());
      order.setOrderType(orderPromotionType);
      order.setPromotionId(usingPromotion.getId());

      // 保存订单项信息
      for (OrderItem item : entry.getValue()) {
        SkuDiscountDetail couponDetail = discountDetails.get(item.getSkuId());
        if (couponDetail != null) {
          setOrderItemDetailDiscount(item, couponDetail);
        }
      }
    }
  }

  /**
   * 设置订单项的优惠详情
   *
   * @param orderItem 订单项
   * @param detailDiscount 优惠详情
   */
  private void setOrderItemDetailDiscount(OrderItem orderItem, SkuDiscountDetail detailDiscount) {
    BigDecimal price = orderItem.getPrice();
    if (detailDiscount != null) {
      // 数据库中记录单个的优惠值
      BigDecimal discount =
          detailDiscount
              .getDiscount()
              .divide(BigDecimal.valueOf(orderItem.getAmount()), 2, BigDecimal.ROUND_HALF_EVEN);
      orderItem.setPromotionType(detailDiscount.getPromotionType());
      orderItem.setPromotionId(detailDiscount.getPromotionId());
      // 优惠大于原价
      if (discount.compareTo(price) > 0) {
        orderItem.setDiscount(price);
        orderItem.setDiscountPrice(BigDecimal.ZERO);
      } else {
        BigDecimal discountPrice = price.subtract(discount);
        orderItem.setDiscount(discount);
        orderItem.setDiscountPrice(discountPrice);
      }
      return;
    }
    orderItem.setDiscountPrice(price);
    orderItem.setDiscount(BigDecimal.ZERO);
  }
}
