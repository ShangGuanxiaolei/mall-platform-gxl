package com.xquark.service.pricing;

import com.xquark.dal.model.Address;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.pricing.base.PromotionInfo;
import com.xquark.service.pricing.impl.pricing.PricingMode;
import com.xquark.service.pricing.vo.PricingResultVO;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface PricingService {

  PricingResultVO calculate(String shopId, String zoneId, String couponId);

  /**
   * key: skuId, val: sku amount
   */
  PricingResultVO calculate(Map<String, Integer> skuMap, String zoneId, String... couponId);

  // PricingResultVO calculate(Map<String, Integer> skuMap, String zoneId, String... couponId);

  // SkuPriceVO getSkuPrice(String skuId);

  PricingResultVO calculate(String orderId, String... couponId);

  PricingResultVO calculate(PricingMode pricingMode,
      Shop shop, List<CartItemVO> cartItems, User buyer,
      UserSelectedProVO userSelectedPro, Address address,
      boolean useDeduction,
      PromotionInfo promotionInfo);

  PricingResultVO calculateWithOutPromotions(Shop shop, List<CartItemVO> cartItems, User buyer,
      Address address);

  PricingResultVO calculate4ConfirmOrder(Shop shop, List<CartItemVO> cartItems, User buyer,
      UserSelectedProVO userSelectedPro, Address address, boolean useDeduction,
      BigDecimal selectPoint,
      BigDecimal selectCommission, PromotionInfo promotionInfo,
      PricingMode pricingMode);

}
