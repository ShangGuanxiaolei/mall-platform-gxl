package com.xquark.service.pricing.impl.pricing;

import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.dal.type.PromotionType;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.pricing.base.PromotionInfo;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by wangxinhua on 17-12-1. DESC: 通过该对象封装折扣计算链中需要的参数
 */
public class PricingChainParams {

  private final Shop shop;

  private final User buyer;

  private final PricingMode pricingMode;

  private final List<CartItemVO> cartItems;

  private final Map map;

  public PricingChainParams(Shop shop, User buyer,
      PricingMode pricingMode, List<CartItemVO> cartItems, Map map) {
    this.shop = shop;
    this.buyer = buyer;
    this.pricingMode = pricingMode;
    this.cartItems = cartItems;
    this.map = map;
  }

  private boolean autoSelect = true;

  private UserSelectedProVO userSelectedPro;

  private PromotionInfo promotionInfo;

  public Map getMap() {
    return map;
  }

  public Shop getShop() {
    return shop;
  }

  public User getBuyer() {
    return buyer;
  }

  public List<CartItemVO> getCartItems() {
    return cartItems;
  }

  public boolean isAutoSelect() {
    return autoSelect;
  }

  public UserSelectedProVO getUserSelectedPro() {
    return userSelectedPro;
  }

  private BigDecimal userSelectPoint;

  private BigDecimal userSelectCommission;

  public BigDecimal getUserSelectPoint() {
    return userSelectPoint;
  }

  public void setUserSelectPoint(BigDecimal userSelectPoint) {
    this.userSelectPoint = userSelectPoint;
  }

  public BigDecimal getUserSelectCommission() {
    return userSelectCommission;
  }

  public void setUserSelectCommission(BigDecimal userSelectCommission) {
    this.userSelectCommission = userSelectCommission;
  }

  public void setAutoSelect(boolean autoSelect) {
    this.autoSelect = autoSelect;
  }

  public void setUserSelectedPro(UserSelectedProVO userSelectedPro) {
    this.userSelectedPro = userSelectedPro;
  }

  public PromotionInfo getPromotionInfo() {
    return promotionInfo;
  }

  public void setPromotionInfo(PromotionInfo promotionInfo) {
    this.promotionInfo = promotionInfo;
  }

  public PromotionType getUserSelectedPromotion() {
    return promotionInfo == null ? null : promotionInfo.getPromotionType();
  }

  public String getUserSelectPromotionId() {
    return promotionInfo == null ? null : promotionInfo.getPromotionId();
  }

  public PricingMode getPricingMode() {
    return pricingMode;
  }
}
