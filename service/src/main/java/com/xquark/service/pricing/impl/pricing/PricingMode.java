package com.xquark.service.pricing.impl.pricing;

/**
 * @author wangxinhua.
 * @date 2018/11/14
 * 计算优惠时的计算模式
 */
public enum PricingMode {

  /**
   * 确认订单
   */
  CONFIRM,

  /**
   * 提交订单
   */
  SUBMIT,

  /**
   * 购物车结算
   */
  CART

}
