package com.xquark.service.pricing.base;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.type.PromotionType;

import java.util.Date;
import java.util.List;

/**
 * Created by wangxinhua on 17-11-21. DESC:
 */
public interface PromotionActivityService {

  /**
   * 根据id查询活动
   *
   * @param id 活动id
   * @return 活动对象
   */
  Promotion load(String id);

  /**
   * 保存活动
   *
   * @param promotion 要保存的活动对象
   * @return true or false
   */
  boolean save(Promotion promotion);


  /**
   * 更新活动
   *
   * @param promotion 要更新的活动对象
   * @return true or false
   */
  boolean update(Promotion promotion);

  /**
   * 逻辑删除活动
   *
   * @param id 活动id
   * @return true or false
   */
  boolean delete(String id);

  /**
   * 根据活动名称及类型查询活动
   *
   * @param name 活动名称
   * @param type 活动类型
   * @return 活动对象
   */
  Promotion loadByNameAndType(String name, PromotionType type);


  /**
   * 结束活动
   *
   * @param id 活动id
   * @return true or false
   */
  boolean close(String id);

  /**
   * 根据批次号获取活动时间
   * @param batchNo
   * @return
   */
  Promotion findDateByBatchNo(String batchNo);

  /**
   * 根据活动时间查询活动
   * @return
   */
  Promotion findPromotionByDate(String batchNo);

}
