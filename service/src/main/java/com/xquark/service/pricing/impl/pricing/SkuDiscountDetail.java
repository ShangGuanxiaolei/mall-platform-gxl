package com.xquark.service.pricing.impl.pricing;

import com.xquark.dal.type.PromotionType;
import java.math.BigDecimal;

/**
 * Created by wangxinhua on 2018/4/19. DESC: 每个商品的优惠详情
 */
public class SkuDiscountDetail {

  private String promotionId;

  private BigDecimal discount;

  private PromotionType promotionType;

  public SkuDiscountDetail(String promotionId, BigDecimal discount,
      PromotionType promotionType) {
    this.promotionId = promotionId;
    this.discount = discount;
    this.promotionType = promotionType;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public PromotionType getPromotionType() {
    return promotionType;
  }

  @Override
  public String toString() {
    return "SkuDiscountDetail{" +
        "promotionId='" + promotionId + '\'' +
        ", discount=" + discount +
        ", promotionType=" + promotionType +
        '}';
  }
}
