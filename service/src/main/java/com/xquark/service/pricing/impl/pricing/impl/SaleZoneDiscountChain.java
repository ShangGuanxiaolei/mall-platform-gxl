package com.xquark.service.pricing.impl.pricing.impl;

import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.mapper.CustomerWechartUnionMapper;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.Sku;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.CouponView;
import com.xquark.dal.vo.FlashSalePromotionProductVO;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pricing.impl.pricing.PricingChainParams;
import com.xquark.service.pricing.impl.pricing.SaleZoneViewPromotion;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.user.UserService;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author hs
 */
public class SaleZoneDiscountChain extends FlashSaleDiscountChain {

    private final OrderMapper orderMapper;
    private final CustomerWechartUnionMapper customerWechartUnionMapper;

    public SaleZoneDiscountChain(UserService userService, FlashSalePromotionProductService flashSaleService, PgPromotionServiceAdapter pgPromotionServiceAdapter,
                                 OrderMapper orderMapper, CustomerWechartUnionMapper customerWechartUnionMapper) {
        super(userService, flashSaleService, pgPromotionServiceAdapter);
        this.orderMapper = orderMapper;
        this.customerWechartUnionMapper = customerWechartUnionMapper;
    }


    @Override
    protected Promotion loadUseAblePromotion(String productId, PricingChainParams params) {
        // FIXME 直接查promotion
        FlashSalePromotionProductVO promotionProductVO =
                (FlashSalePromotionProductVO) flashSaleService.loadPromotionProductSaleZoneByPId(productId);
        if (promotionProductVO != null) {
            Promotion promotion = promotionProductVO.getPromotion();
            promotion.setDiscount(promotionProductVO.getDiscount());
            promotion.setIsFreeDelivery(false); // FIXME 秒杀暂时设置默认值
            return promotionProductVO.getPromotion();
        }
        return null;
    }

    @Override
    protected CouponView buildCouponView(
            String promotionId, BigDecimal totalDiscount, boolean freeDelivery) {
        return new SaleZoneViewPromotion(promotionId, totalDiscount);
    }

    @Override
    protected PromotionType calculateScope() {
        return PromotionType.SALEZONE;
    }

    @Override
    protected void preCheckExtra(PricingChainParams params, Promotion promotion, CartItemVO currItem) {
        super.preCheckExtra(params, promotion, currItem);

        if (orderMapper.checkSaleZone(params.getBuyer().getId())) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "同一用户只能参加一次专区活动哦");
        }
    }

    @Override
    protected BigDecimal getRealPrice(
            Promotion canUserPromotion, CartItemVO item, PricingChainParams params) {
        // 后台配置了活动价格则已配置的价格为准, 否则走原来的折扣逻辑
        BigDecimal price = canUserPromotion.getPrice();
        if (price != null) {
            BigDecimal promotionPoint = canUserPromotion.getPoint();
            // 如果活动单独配置了德分则把德分覆盖到对应的sku上
            if (promotionPoint != null) {
                item.getSku().setDeductionDPoint(promotionPoint);
            }
            return price;
        }

        return super.getRealPrice(canUserPromotion, item, params);
    }

    /**
     * add by hs
     * 一元专区一个批次只能参加一次
     * 批次号:Cpid:unionId
     */
    private Boolean checkSaleZoneJoinPolicyForPayAgain(Sku sku, Long cpId) {
        RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
        Long lCount = 0L;
        FlashSalePromotionProductVO promotionProductVO = (FlashSalePromotionProductVO) flashSaleService
                .loadPromotionProductSaleZoneByPId(sku.getProductId());
        if (promotionProductVO != null) {
            //key还要加上批次号
            List<String> customerWecharKeys = customerWechartUnionMapper.selectKeysByCpId(cpId);
            List<String> saleZoneBuyKeys = customerWecharKeys.stream().map((key) -> String.join(":",
                    promotionProductVO.getPromotion().getBatchNo(), key)).
                    collect(Collectors.toList());
            lCount = saleZoneBuyKeys.stream().filter(
                    (key) -> redisUtils.get(key) != null && redisUtils.get(key) != 0
            ).count();
        }
        return lCount > 0;
    }
}
