package com.xquark.service.pricing.impl;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.xquark.dal.model.*;
import com.xquark.dal.status.CouponStatus;
import com.xquark.dal.type.LogisticDiscountType;
import com.xquark.dal.vo.*;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.cart.CartService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.order.OrderService;
import com.xquark.service.pricing.CouponService;
import com.xquark.service.pricing.CouponVO;
import com.xquark.service.pricing.PricingService;
import com.xquark.service.pricing.base.PromotionInfo;
import com.xquark.service.pricing.impl.pricing.*;
import com.xquark.service.pricing.vo.PricingResultVO;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.PostageRet;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.RolePriceService;
import com.xquark.service.yundou.PointCommDeductionResult;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

import static com.xquark.dal.type.PromotionType.*;
import static com.xquark.service.pricing.impl.pricing.DiscountPricingResult.FINAL_USING_COUPON_KEY;
import static com.xquark.service.pricing.impl.pricing.DiscountPricingResult.VALID_COUPONS_KEY;

@Service("pricingService")
public class PricingServiceImpl extends BaseServiceImpl implements PricingService {

  @Autowired
  private CartService cartService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private ProductService productService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private CouponService couponService;

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private RolePriceService rolePriceService;

  @Autowired
  private DiscountPricingChain pricingChain;

  @Override
  public PricingResultVO calculate(String shopId, String zoneId, String couponId) {
    BigDecimal goodsFee = BigDecimal.ZERO;
    BigDecimal logisticsFee = BigDecimal.ZERO;
    BigDecimal discountFee = BigDecimal.ZERO;
    BigDecimal totalFee;

    PricingResultVO result = new PricingResultVO();

    List<CartItemVO> cartItems = cartService.listCartItems(shopId);
    for (CartItem cartItem : cartItems) {
      Sku sku = productService.loadSku(cartItem.getProductId(), cartItem.getSkuId());
      if (sku == null) {
        continue;
      }
      goodsFee = goodsFee.add(sku.getPrice().multiply(new BigDecimal(cartItem.getAmount())));
    }

    if (null != zoneId && !zoneId.isEmpty()) {
      logisticsFee = shopService.getCurPostage(shopId, zoneId);
    }

    if (StringUtils.isNotEmpty(couponId)) {
      CouponVO coupon = couponService.loadVO(couponId);
      if (CouponStatus.VALID == coupon.getStatus()) {
        discountFee = coupon.getActivity().getDiscount();
        result.setCoupon(coupon);
      }
    }

    if (discountFee.doubleValue() > goodsFee.add(logisticsFee).doubleValue()) {
      discountFee = goodsFee.add(logisticsFee);
    }

    totalFee = goodsFee.add(logisticsFee).subtract(discountFee);

    if (totalFee.doubleValue() < 0) {
      totalFee = new BigDecimal(0);
    }

    result.setGoodsFee(goodsFee.setScale(2));
    result.setLogisticsFee(logisticsFee.setScale(2));
    result.setDiscountFee(discountFee.setScale(2));
    result.setTotalFee(totalFee.setScale(2));

    return result;
  }

  @Override
  public PricingResultVO calculate(String orderId, String... couponIds) {
    OrderVO vo = orderService.loadVO(orderId);
    Map<String, Integer> skuMap = new HashMap<String, Integer>();
    String couponId = couponIds[0]; //暂时只支持一个优惠
    List<OrderItem> items = vo.getOrderItems();
    for (OrderItem item : items) {
      skuMap.put(item.getSkuId(), item.getAmount());
    }

    return calculate(skuMap, vo.getOrderAddress().getZoneId(), couponId);
  }

  @Override
  public PricingResultVO calculate(PricingMode pricingMode,
      Shop shop, List<CartItemVO> cartItems, User buyer,
      UserSelectedProVO userSelectedPro, Address address,
      boolean useDeduction,
      PromotionInfo promotionInfo) {

    Preconditions.checkNotNull(shop);
    Preconditions.checkNotNull(cartItems);
    Preconditions.checkState(cartItems.size() > 0, "cart item size is zero.");

    // 结算价
    BigDecimal goodsFee = BigDecimal.ZERO;
    // 邮费
    BigDecimal logisticsFee;
    BigDecimal logistics;
    BigDecimal logisticsDiscount;
    LogisticDiscountType logisticDiscountType;
    // 折扣
    BigDecimal discountFee = BigDecimal.ZERO;
    // 总价
    BigDecimal totalFee;

    PricingResultVO result = PricingResultVO.getZeroPricingResult();
    Integer amount = 0;

    //计算商品费用和
    for (CartItemVO cartItem : cartItems) {
      Sku sku = cartItem.getSku();
      sku.setItemAmount(cartItem.getAmount());
      Integer cartItemAmount = cartItem.getAmount();
      amount = amount + cartItemAmount;
      goodsFee = goodsFee.add(sku.getPrice().multiply(new BigDecimal(cartItem.getAmount())));
    }

    Map<String, Object>  map = new HashMap<>();
    map.put("address", address);

    // 构建优惠参数
    PricingChainParams params = new PricingChainParams(shop, buyer, pricingMode, cartItems, map);
    params.setAutoSelect(false);
    params.setUserSelectedPro(userSelectedPro);
    params.setPromotionInfo(promotionInfo);

    PricingContext context = new PricingContext(goodsFee);
    DiscountPricingResult discountResult = pricingChain
        .calculate(params, context);
    // TODO promotions 不应该从上下文中复制
    discountResult.setCurrUsingPromotions(context.getUsingPromotions());

    // 积分计算结果
    PointCommDeductionResult pointRet = context.getPointCommRet();

    // 总优惠
    discountFee = discountFee.add(discountResult.getTotalDiscount());

    BigDecimal couponDiscountFee = discountResult.getDiscount(COUPON);

    Map<String, Object> logisticsMap = discountResult.getLogisticsMap();
    logisticsFee = (BigDecimal) logisticsMap.get("logisticsFee");
    logisticsDiscount = (BigDecimal) logisticsMap.get("logisticsDiscount");
    logistics = (BigDecimal) logisticsMap.get("logistics");
    logisticDiscountType = (LogisticDiscountType) logisticsMap.get("logisticDiscountType");

    //优惠金额大于总需支付金额
    if (discountFee.compareTo(goodsFee.add(logisticsFee)) > 0) {
      discountFee = goodsFee;
    }

    //确认订单增加抵扣的运费以供前端计算
    totalFee = goodsFee.add(logistics).subtract(discountFee);
    if (totalFee.compareTo(BigDecimal.ZERO) <= 0) {
      totalFee = new BigDecimal(0);
    }

    // 优惠券信息统一放到 couponView中
    @Deprecated
    @SuppressWarnings("unchecked")
    List<UserCouponVo> coupons = Optional.fromNullable(
        discountResult.getExtraParam(VALID_COUPONS_KEY, List.class))
        .or(Collections.emptyList());

    List<CouponView> couponViews = discountResult.getSortedCanUsePromotonns();

    result.setCoupons(coupons);
    result.setCouponViews(couponViews);
    result.setGoodsFee(goodsFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));

    BigDecimal showLogistics = logistics.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    result.setLogistics(showLogistics);//前端显示的邮费
    BigDecimal showLogisticDiscountFees = logisticsDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    if (showLogisticDiscountFees.compareTo(BigDecimal.ZERO) != 0) {
      result.setLogistics(result.getLogisticsFee().add(showLogisticDiscountFees));
    }

    BigDecimal logisticsFees = logisticsFee.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    result.setLogisticsFee(logisticsFees);//邮费

    BigDecimal logisticDiscountFees = logisticsDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    result.setLogisticsDiscount(logisticDiscountFees);//邮费减免
    if (logisticDiscountFees.compareTo(BigDecimal.ZERO) != 0) {
      result.setLogisticsFee(result.getLogisticsFee().add(logisticDiscountFees));
    }
    result.setLogisticDiscountType(logisticDiscountType);
    result.setDiscountFee(discountFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    result.setCouponDiscountFee(couponDiscountFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    result.setTotalFee(totalFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    result.setPointRet(pointRet);
    result.setDiscountDetail(discountResult.getDiscountDetail());
    result.setDiscountPricingResult(discountResult);
    result.setIsPayFree(discountResult.getIsPayFree());

    return result;
  }

  /**
   * 只计算基础价格
   */
  @Override
  public PricingResultVO calculateWithOutPromotions(Shop shop, List<CartItemVO> cartItems,
      User buyer, Address address) {
    Preconditions.checkNotNull(shop);
    Preconditions.checkNotNull(cartItems);
    Preconditions.checkState(cartItems.size() > 0, "cart item size is zero.");

    // 结算价
    BigDecimal goodsFee = new BigDecimal(0);
    // 邮费
    BigDecimal logisticsFee = new BigDecimal(0);
    BigDecimal logisticsDiscount = new BigDecimal(0);
    // 折扣
    BigDecimal discountFee = new BigDecimal(0);
    // 总价
    BigDecimal totalFee;

    PricingResultVO result = PricingResultVO.getZeroPricingResult();
    List<Sku> skus = new ArrayList<>();
    List<String> skuIds = new ArrayList<>();
    Integer amount = 0;

    for (CartItemVO cartItem : cartItems) {
      skuIds.add(cartItem.getSku().getId());
      // 将cartitem的数量放入到sku中，以便后面计算运费使用
      Sku sku = cartItem.getSku();
      sku.setItemAmount(cartItem.getAmount());
      skus.add(sku);
      Integer cartItemAmount = cartItem.getAmount();
      amount = amount + cartItemAmount;

      goodsFee = goodsFee.add(sku.getPrice().multiply(new BigDecimal(cartItem.getAmount())));
    }

//		// 计算积分、活动等相关折扣
//		PricingChainParams params = PricingChainParams.Builder.aPricingChainParams(shop, buyer, cartItems)
//				.build();
//		DiscountPricingResult discountResult = pricingChain.calculate(params, new PricingContext(goodsFee));

//		discountFee = discountFee.add(discountResult.getTotalDiscount());
//		BigDecimal couponDiscountFee = discountResult.getDiscount(COUPON);

    //优惠金额大于总需支付金额
    if (discountFee.compareTo(goodsFee.add(logisticsFee)) == 1) {
      discountFee = goodsFee.add(logisticsFee);
    }

    totalFee = goodsFee.add(logisticsFee).subtract(discountFee);
    if (totalFee.compareTo(BigDecimal.ZERO) <= 0) {
      totalFee = new BigDecimal(0);
    }

    Shop rootShop = shopService.loadRootShop();

    //计算邮费
    if (CollectionUtils.isNotEmpty(skus) && address != null) {
      //logisticsFee = shopService.getShopPostage(shop.getId(), address.getZoneId(), skuIds, goodsFee);

      String rootShopId = rootShop.getId();
      if (StringUtils.isNotBlank(rootShopId)) {
        PostageRet postageRet = shopService
            .getShopPostageByProduct(rootShopId, address.getZoneId(), skus);
        logisticsFee = logisticsFee.add(postageRet.getLogisticsFee());
        logisticsDiscount = logisticsDiscount.add(postageRet.getLogisticsDiscount());
        //加入一元夺宝的业务逻辑
      }
    }

    result.setCoupons(Collections.<UserCouponVo>emptyList());
    result.setGoodsFee(goodsFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    result.setLogisticsFee(logisticsFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    result.setLogisticsDiscount(logisticsDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    result.setDiscountFee(discountFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));
//		result.setCouponDiscountFee(couponDiscountFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    result.setTotalFee(totalFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    return result;
  }

  @Override
  public PricingResultVO calculate4ConfirmOrder(Shop shop, List<CartItemVO> cartItems, User buyer,
      UserSelectedProVO userSelectedPro, Address address,
      boolean useDeduction, BigDecimal selectPoint,
      BigDecimal selectCommission, PromotionInfo promotionInfo,
      PricingMode pricingMode) {
    Preconditions.checkNotNull(shop);
    Preconditions.checkNotNull(cartItems);
    Preconditions.checkState(cartItems.size() > 0, "cart item size is zero.");

    BigDecimal goodsFee = BigDecimal.ZERO;
    BigDecimal logisticsFee;
    BigDecimal logisticsDiscount;
    BigDecimal commissionLogisticsDiscount;
    LogisticDiscountType logisticDiscountType;
    BigDecimal discountFee = BigDecimal.ZERO;
    BigDecimal totalFee;
    BigDecimal logistics;
    PricingResultVO result = PricingResultVO.getZeroPricingResult();

    //计算商品费用和
    for (CartItemVO cartItem : cartItems) {
      // 特权商品购买不需要花钱
      if (userSelectedPro == null || !"1".equals(userSelectedPro.getIsPrivilege())) {
        goodsFee = goodsFee.add(cartItem.getSku().getPrice()
            .multiply(new BigDecimal(cartItem.getAmount())));
      }
    }

    Map<String, Object> map = new HashMap<>();
    map.put("address", address);

    PricingChainParams params = new PricingChainParams(shop, buyer, pricingMode, cartItems, map);
    params.setUserSelectedPro(userSelectedPro);
    params.setPromotionInfo(promotionInfo);
    params.setAutoSelect(true);
    params.setUserSelectPoint(selectPoint);
    params.setUserSelectCommission(selectCommission);

    PricingContext context = new PricingContext(goodsFee);
    DiscountPricingResult discountResult = pricingChain
        .calculate(params, context);
    // TODO promotions 不应该从上下文中复制
    discountResult.setCurrUsingPromotions(context.getUsingPromotions());

    BigDecimal cutDownDiscount = discountResult.getDiscount(CUT_DOWN);
    BigDecimal pointDiscount = discountResult.getDiscount(POINT);
    BigDecimal commissionDiscount = discountResult.getDiscount(COMMISSION);
    //一元专区下单优惠
    BigDecimal maxPromotionDiscount = discountResult.getPromotionDiscount();

    // 总优惠
    // FIXME wangxinhua 折扣统一add
    discountFee = discountFee.add(cutDownDiscount)
        .add(pointDiscount)
        .add(commissionDiscount)
        .add(maxPromotionDiscount);
    // 优惠券优惠
    BigDecimal couponDiscountFee = discountResult.getDiscount(COUPON);

    Map<String, Object> logisticsMap = discountResult.getLogisticsMap();
    logisticsFee = (BigDecimal) logisticsMap.get("logisticsFee");
    logistics = (BigDecimal) logisticsMap.get("logistics");
    logisticsDiscount = (BigDecimal) logisticsMap.get("logisticsDiscount");
    logisticDiscountType = (LogisticDiscountType) logisticsMap.get("logisticDiscountType");
    commissionLogisticsDiscount = (BigDecimal) logisticsMap.get("commissionLogisticsDiscount");

    //设置积分抵扣掉邮费的值
    result.setCommissionLogisticsDiscount(commissionLogisticsDiscount);

    //优惠金额大于商品总金额
    if (discountFee.compareTo(goodsFee.add(logistics)) > 0) {
      discountFee = goodsFee;
    }
    totalFee = goodsFee.add(logistics).subtract(discountFee);
    if (totalFee.compareTo(BigDecimal.ZERO) <= 0) {
      totalFee = new BigDecimal(0);
    }

    UserCouponVo finalUsingCoupon = discountResult
        .getExtraParam(FINAL_USING_COUPON_KEY, UserCouponVo.class);
    if (finalUsingCoupon != null) {
      result.setConfirmedShopCoupon(finalUsingCoupon);
    }

    result.setGoodsFee(goodsFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    result.setLogisticsFee(logisticsFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    result.setLogisticsDiscount(logisticsDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    result.setLogisticDiscountType(logisticDiscountType);
    result.setDiscountFee(discountFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    result.setCouponDiscountFee(couponDiscountFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    result.setTotalFee(totalFee.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    result.setDiscountPricingResult(discountResult);
    result.setIsPayFree(discountResult.getIsPayFree());

    return result;
  }


  /**
   * 判断商品是否在b2b经销商定价中，如果是商品价格为经销商角色定价
   */
  private void checkB2bPurchase(CartItemVO cartItem) {
    String productId = cartItem.getProduct().getId();
    String userId = this.getCurrentUser().getId();
    //判断当前用户是否在代理角色中
    UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
    if (userAgentVO != null) {
      String role = userAgentVO.getRole();
      RolePriceVO rolePriceVO = rolePriceService.selectByProductAndRole(productId, role);
      if (rolePriceVO != null) {
        cartItem.getSku().setPrice(rolePriceVO.getPrice());
      }
    }
  }

  /**
   * 此优惠券是shop级别的优惠券，现阶段不支持
   */
  @Override
  public PricingResultVO calculate(Map<String, Integer> skuMap, String zoneId,
      String... couponIds) {
    BigDecimal goodsFee = BigDecimal.ZERO;
    BigDecimal logisticsFee = BigDecimal.ZERO;
    BigDecimal discountFee = BigDecimal.ZERO;
    BigDecimal totalFee = BigDecimal.ZERO;

    PricingResultVO result = PricingResultVO.getZeroPricingResult();

    List<Sku> skus = new ArrayList<>();
    for (String skuId : skuMap.keySet()) {
      Sku sku = productService.loadSku(skuId);
      skus.add(sku);
//			if (null != zoneId && ! zoneId.isEmpty()) {
//				logisticsFee = shopService.getCurPostage(productService.load(sku.getProductId()).getShopId(), zoneId);
//			}

      goodsFee = goodsFee.add(sku.getPrice().multiply(new BigDecimal(skuMap.get(skuId))));
    }

    if (skus != null && skus.size() > 0) {
      String shopId = productService.load(skus.get(0).getProductId()).getShopId();
      logisticsFee = shopService.getShopPostage(shopId, zoneId,
          new ArrayList<>(skuMap.keySet()), goodsFee);
    }

    if (couponIds != null && couponIds.length > 0) {
      for (int i = 0; i < couponIds.length; i++) {
        String couponId = couponIds[i];
        if (StringUtils.isNotEmpty(couponId)) {
          CouponVO coupon = couponService.loadVO(couponId);
          if (CouponStatus.VALID == coupon.getStatus()) {
            discountFee = discountFee.add(coupon.getDiscount());
            result.setCoupon(coupon);//如有多个红包，传入最后一个红包
          }
        }
      }
    }
    // 优惠券
    if (discountFee.doubleValue() > goodsFee.add(logisticsFee).doubleValue()) {
      discountFee = goodsFee.add(logisticsFee);
    }

    totalFee = goodsFee.add(logisticsFee).subtract(discountFee);
    if (totalFee.doubleValue() < 0) {
      totalFee = new BigDecimal(0);
    }

    result.setGoodsFee(goodsFee);
    result.setLogisticsFee(logisticsFee);
    result.setDiscountFee(discountFee);
    result.setTotalFee(totalFee);

    return result;
  }

  public static class PinnedComparator implements Comparator<CouponView> {

    private CouponView pinned;

    public PinnedComparator(CouponView pinned) {
      this.pinned = pinned;
    }

    @Override
    public int compare(CouponView o1, CouponView o2) {
      if (pinned != null && o1 == pinned) {
        return -1;
      } else if (pinned != null && o2 == pinned) {
        return 1;
      } else {
        int comRet = o1.getDiscount().compareTo(o2.getDiscount());
        switch (comRet) {
          case 1:
            return -1;
          case 0:
            return 0;
          case -1:
            return 1;
          default:
            return -1;
        }
      }
    }
  }

  public static final UserCouponVo NO_COUPON;

  public static final Promotion NO_PROMOTION;

  static {
    NO_COUPON = new UserCouponVo();
    NO_COUPON.setId("");
    NO_COUPON.setDiscount(BigDecimal.ZERO);
    NO_COUPON.setCouponName("不使用优惠");

    NO_PROMOTION = new Promotion();
    NO_PROMOTION.setId("");
    NO_PROMOTION.setDiscount(BigDecimal.ZERO);
  }
}
