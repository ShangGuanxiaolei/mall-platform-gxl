package com.xquark.service.pricing.impl.pricing.hooks;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.service.pricing.impl.pricing.SkuDiscountDetail;
import java.math.BigDecimal;
import java.util.Map;

/**
 * @author wangxinhua.
 * @date 2018/12/18
 */
public class CommissionBeforeHook extends OrderItemHook {

  private final BigDecimal paidTotal;
  private final Map<String, SkuDiscountDetail> devideDetail;

  public CommissionBeforeHook(BigDecimal paidTotal, Map<String, SkuDiscountDetail> devideDetail) {
    this.paidTotal = paidTotal;
    this.devideDetail = devideDetail;
  }

  @Override
  protected void acceptOrder(Order order) {
    order.setPaidCommission(paidTotal);
  }

  @Override
  protected void acceptItem(Order order, OrderItem item) {
    SkuDiscountDetail pointDetailSku = devideDetail.get(item.getSkuId());
    if (pointDetailSku != null) {
      item.setPaidCommission(pointDetailSku.getDiscount());
    }
  }
}
