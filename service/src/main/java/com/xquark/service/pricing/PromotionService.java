package com.xquark.service.pricing;

import com.xquark.dal.model.Address;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.dal.page.PageHelper;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionSimpleReduceVO;
import com.xquark.dal.vo.PromotionVO;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.pricing.base.PromotionInfo;
import com.xquark.service.pricing.impl.pricing.PricingMode;
import com.xquark.service.pricing.vo.CartPromotionResultVO;
import com.xquark.service.pricing.vo.ConfirmOrderPromotionVO;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface PromotionService {

  Promotion load(String id);

  Promotion loadByProductId(String productId, Date date);

  Promotion loadByProductId(String productId);

  /**
   * 在无选择优惠状态下计算优惠和结算信息
   */
  CartPromotionResultVO calculate4Cart(Map<Shop, List<CartItemVO>> cartItemMap, User buyer,
      Address address, boolean useDeduction);

  /**
   * 根据选择的优惠及参加的活动计算优惠和结算信息
   *  @param autoSelect 是否自动选择最优优惠
   * @param promotionInfo
   * @param pricingMode
   */
  CartPromotionResultVO calculate4Cart(Map<Shop, List<CartItemVO>> cartItemMap, User buyer,
      UserSelectedProVO userSelectedPro, Address address,
      boolean useDeduction, boolean autoSelect,
      PromotionInfo promotionInfo, PricingMode pricingMode);

  CartPromotionResultVO calculate4CartWithOutPromotions(Map<Shop, List<CartItemVO>> cartItemMap,
      User buyer, Address address);


  /**
   * 确认下单优惠计算
   */
  ConfirmOrderPromotionVO calculate4ConfirmOrder(Map<Shop, List<CartItemVO>> cartItemMap,
      User buyer,
      UserSelectedProVO userSelectedPro, Address address, boolean useDeduction,
      BigDecimal selectPoint,
      BigDecimal selectCommission, PromotionInfo promotionInfo,
      PricingMode pricingMode);

  /**
   * 确认下单优惠计算
   */
  ConfirmOrderPromotionVO calculate4ConfirmOrder(Map<Shop, List<CartItemVO>> cartItemMap,
      User buyer,
      UserSelectedProVO userSelectedPro, Address address, boolean useDeduction,
      PricingMode pricingMode);

  int insert(Promotion promotion);

  int update(Promotion promotion);

  Promotion loadByNameAndType(String name, PromotionType type);

  /**
   * 活动列表，分页获取某个类型活动的所有信息
   */
  List<PromotionVO> getPromotionForApp(Pageable pager, String type);

  List<Promotion> listReducePromotionByProductId(String productId, Date date, Boolean isShare);

  Long countPromotionByProductId(String productId, Date date, Boolean isShare);

  Map<String, List<Promotion>> listReduceMapPromotionByProductId(String productId, Date date,
      Boolean isShare);

  List<String> listReducePromotionTitleByProductId(String productId, Date date, Boolean isShare);

  List<PromotionSimpleReduceVO> listSimpleReduceVO(String productId);

  List<PromotionSimpleReduceVO> listSimpleReduceVO(String productId, String userId);

  Boolean hasActivate(PromotionType type);

  PageHelper<PromotionVO> getMeetingSaleProList(Long cpId,Integer pageNum,Integer pageSize, String pType);

  boolean checkIsOpen(Long cpId, String pCode);
  boolean checkIsWhite(Long cpId, String pCode);

  /**
   * 活动or拼团查询pcode
   * @param productId
   * @param type
   * @param statusList
   * @return
   */
  String findPcodeByTypeOrStatusOrPid(String productId, String type, List<String> statusList);

  /**
   * 大会活动查询pCode
   * @param productId
   * @return
   */
  String findPcode4Meeting(String productId);
}
