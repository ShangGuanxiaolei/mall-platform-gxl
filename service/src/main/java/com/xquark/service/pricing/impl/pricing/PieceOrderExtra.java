package com.xquark.service.pricing.impl.pricing;

import com.xquark.dal.model.mypiece.PGMemberInfoVo;
import com.xquark.dal.model.mypiece.PGTranInfoVo;

/**
 * @author wangxinhua
 * @since 18-11-2
 * 拼团订单附加参数
 */
public class PieceOrderExtra {

  /**
   * 该参数只在开团时才有
   */
  private final PGTranInfoVo pgTranInfoVo;

  /**
   * 无论开团还是参团都需要保存该参数
   */
  private final PGMemberInfoVo pgMemberInfoVo;

  private final boolean isGroupHead;

  /**
   * 是否是插队拼团
   */
  private final boolean isCutInLine;

  public PieceOrderExtra(PGTranInfoVo pgTranInfoVo,
      PGMemberInfoVo pgMemberInfoVo, boolean isGroupHead, boolean isCutInLine) {
    this.pgTranInfoVo = pgTranInfoVo;
    this.pgMemberInfoVo = pgMemberInfoVo;
    this.isGroupHead = isGroupHead;
    this.isCutInLine = isCutInLine;
  }

  public PGTranInfoVo getPgTranInfoVo() {
    return pgTranInfoVo;
  }

  public PGMemberInfoVo getPgMemberInfoVo() {
    return pgMemberInfoVo;
  }

  public boolean isGroupHead() {
    return isGroupHead;
  }

  public boolean getIsCutInLine() {
    return isCutInLine;
  }
}
