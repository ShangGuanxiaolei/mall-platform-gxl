package com.xquark.service.pricing.impl.pricing.impl;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionPgPrice;
import com.xquark.dal.model.Sku;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.CouponView;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.pricing.impl.pricing.*;
import com.xquark.service.pricing.impl.pricing.hooks.PromotionBeforeHook;
import com.xquark.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Created by wangxinhua on 2018/4/19. DESC: 满减与内购优惠父类 */
public abstract class PromotionDiscountPricingChain<T extends Promotion>
    extends DiscountPricingChain {

  protected final UserService userService;

  protected final PgPromotionServiceAdapter pgPromotionServiceAdapter;

  protected final Logger logger = LoggerFactory.getLogger(this.getClass());

  protected PromotionDiscountPricingChain(UserService userService, PgPromotionServiceAdapter pgPromotionServiceAdapter) {
    this.userService = userService;
    this.pgPromotionServiceAdapter = pgPromotionServiceAdapter;
  }

  /** 按每个商品单独计算优惠, 最有汇总为订单的一个优惠 */
  @Override
  protected DiscountPricingResult calculatePrice(
      PricingChainParams params, PricingContext context) {
    final DiscountPricingResult result = DiscountPricingResult.emptyResult();
    final List<CartItemVO> cartItems = params.getCartItems();
    final Map<String, SkuDiscountDetail> promotionDetail = new HashMap<>(cartItems.size());

    BigDecimal totalDiscount = BigDecimal.ZERO;
    boolean freeDelivery = false;
    boolean isCountEarning = true;

    // 记录每个商品的优惠详情
    Promotion usablePromotion = null;

    for (CartItemVO cartItem : cartItems) {
      final String productId = cartItem.getProductId();
      final T canUserPromotion = loadUseAblePromotion(productId, params);
      if (canUserPromotion != null) {
        final String skuId = cartItem.getSkuId();
        final Sku sku = cartItem.getSku();
        // FIXME 在订单确认页跟订单结算时通过该方法获取的活动可能因为过期等因素导致不一致
        // 需要在订单后追踪用户选择的折扣并在此处做校验
        // 活动暂时不支持购物车下单
        // 购物车结算不需要走校验逻辑
        // TODO 通用处理
        if (params.getPricingMode() != PricingMode.CART) {
          preCheck(params, canUserPromotion, cartItem);
        }
        usablePromotion = canUserPromotion;
        // 商品金额
        final BigDecimal amount = BigDecimal.valueOf(cartItem.getAmount());
        final BigDecimal itemPrice = sku.getPrice();
        final BigDecimal price = itemPrice.multiply(amount);
        // 多个优惠中有一个满足包邮条件就包邮
        freeDelivery = freeDelivery || canUserPromotion.getIsFreeDelivery();

        isCountEarning = isCountEarning && canUserPromotion.getIsCountEarning();

        // 配置了价格体系则走价格体系，否则走原逻辑
        final BigDecimal itemDiscountPrice = getRealPrice(canUserPromotion, cartItem, params);

        final BigDecimal discountPrice = itemDiscountPrice.multiply(amount);
        // 优惠的价格
        BigDecimal discount = price.subtract(discountPrice).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        if (discountPrice.signum() < 0) {
          discount = price;
        }
        promotionDetail.put(
            skuId, new SkuDiscountDetail(canUserPromotion.getId(), discount, calculateScope()));
        totalDiscount = totalDiscount.add(discount);

        // 计算结束后单独设置各自的折扣
        modifySkuAfter(itemDiscountPrice, sku);

        // 前提为活动不能互斥
        context.addCurrUsingPromotion(cartItem.getProductId(), canUserPromotion);

        // onItemFinish hook
        onItemFinish(params, context, result, cartItem, canUserPromotion);
      }
    }
    // 订单详情页中默认只显示一种优惠
    String pId = "";
    if (usablePromotion != null) {
      pId = usablePromotion.getId();
    }
    context.addDiscount(totalDiscount);
    PromotionType thisScope = calculateScope();
    result.getCanUsePromotions().add(buildCouponView(pId, totalDiscount, freeDelivery));
    result.setCountEarning(isCountEarning);
    result.setFreeDelivery(freeDelivery);
    result.addDiscountWithDetail(thisScope, promotionDetail);

    // 添加订单保存前钩子
    result.addBeforeOrderHook(new PromotionBeforeHook(usablePromotion, promotionDetail));
    logger.info("优惠 {} 计算完成, discount: {}", thisScope.getcName(), thisScope);

    return result;
  }

  /**
   * 计算拼团商品价格
   * @param canUserPromotion 活动信息
   * @param item 商品信息
   * @param params 链参数
   * @return 拼团商品实际价格
   */
  protected BigDecimal getRealPrice(
      T canUserPromotion, CartItemVO item, PricingChainParams params) {

    PromotionPgPrice pgPrice = getPromotionPgPrice(canUserPromotion, params, item);
    if (pgPrice != null) {
      // 配置了价格体系则使用价格体系的值
      return pgPrice.getPromotionPrice();
    }
    // 否则走原逻辑
    return canUserPromotion
        .getRealDiscount()
        .multiply(item.getSku().getPrice())
        .divide(BigDecimal.valueOf(10), 2, BigDecimal.ROUND_HALF_EVEN);
  }

  /**
   * 获取配置的价格体系
   * @param canUserPromotion 当前活动
   * @param params 活动参数
   * @param item 购物车对象
   * @return 活动价格体系
   */
  protected PromotionPgPrice getPromotionPgPrice(Promotion canUserPromotion, PricingChainParams params, CartItemVO item) {
    final String promotionId = canUserPromotion.getId();
    final Sku sku = item.getSku();
    final PromotionPgPrice promotionPgPrice = pgPromotionServiceAdapter.selectPromotionPgPriceByPromotionIdAndSkuId(promotionId,
            sku, calculateScope());
    if (promotionPgPrice != null) {
      // 价格体系则使用价格体系的德分
      BigDecimal point = promotionPgPrice.getPoint();
      canUserPromotion.setPoint(point);
      canUserPromotion.setPointDiscount(false);
      // 设置独立的价格体系，立减服务费等需要替换掉
      item.getDynamicPricing().setReduction(promotionPgPrice.getReduction());
      item.getDynamicPricing().setServerAmt(promotionPgPrice.getServerAmt());
      item.getDynamicPricing().setPromoAmt(promotionPgPrice.getPromoAmt());
      item.getDynamicPricing().setNetWorth(promotionPgPrice.getNetWorth());
    }
    return promotionPgPrice;
  }

  /** 参数预校验 */
  private void preCheck(PricingChainParams params, T promotion, CartItemVO currItem) {
    this.preCheckExtra(params, promotion, currItem);
  }

  /** 不同的优惠取不同的活动 */
  protected abstract T loadUseAblePromotion(String productId, PricingChainParams params);

  /**
   * 不同的优惠最后展示不同的优惠信息
   *
   * @param totalDiscount 计算出的总折扣
   * @param freeDelivery 是否包邮
   * @return instance of couponView
   */
  protected abstract CouponView buildCouponView(
      String promotionId, BigDecimal totalDiscount, boolean freeDelivery);

  /** 计算完成后对商品做相关更新 */
  protected void modifySkuAfter(BigDecimal discountPrice, Sku sku) {
    // 计算后把活动价放到sku里面
    sku.setPromotionPrice(discountPrice);
  }

  /** 每个活动附加的参数预校验, 默认不校验 */
  protected void preCheckExtra(PricingChainParams params, T promotion, CartItemVO currItem) {}

  /** 在方法调用结束时调用, 做一些收尾工作 */
  protected void onItemFinish(
      PricingChainParams params,
      PricingContext context,
      DiscountPricingResult ret,
      CartItemVO cartItem,
      T usingPromotion) {
    logger.debug("当前优惠 {} 结果: {} : 当前context: {}", calculateScope().getcName(), ret, context);
  }
}
