package com.xquark.service.pricing.impl.pricing.hooks;

import com.google.common.base.Supplier;
import com.xquark.dal.model.MainOrder;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.mypiece.PGMemberInfoVo;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.service.order.impl.PieceGroupDetailSupplier;
import com.xquark.service.pricing.impl.pricing.OrderUnit;
import com.xquark.service.pricing.impl.pricing.PieceOrderExtra;
import com.xquark.service.pricing.impl.pricing.impl.PgPromotionServiceAdapter;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author wangxinhua.
 * @date 2018/12/17
 */
public class PieceAfterHook extends OrderHook {

  private final PieceOrderExtra pieceOrderExtra;
  private final PgPromotionServiceAdapter promotionServiceAdapter;

  public PieceAfterHook(
      PieceOrderExtra pieceOrderExtra, PgPromotionServiceAdapter promotionServiceAdapter) {
    this.pieceOrderExtra = pieceOrderExtra;
    this.promotionServiceAdapter = promotionServiceAdapter;
  }

  @Override
  public void accept(OrderUnit unit) {
    // 保存拼团成员信息
    boolean isGroupHead = pieceOrderExtra.isGroupHead();
    PGTranInfoVo tranInfo = pieceOrderExtra.getPgTranInfoVo();
    if (isGroupHead && tranInfo != null) {
      // 发起人开团同时要保存开团信息
      promotionServiceAdapter.saveTranInfo(tranInfo);
    }
    PGMemberInfoVo pgMemberInfoVo = pieceOrderExtra.getPgMemberInfoVo();
    if (pgMemberInfoVo != null) {
      promotionServiceAdapter.saveMemberInfo(pgMemberInfoVo);
    }

    MainOrder mainOrder = unit.getMainOrder();
    for (Entry<Order, List<OrderItem>> entry : unit.getOrderMap().entrySet()) {
      Order order = entry.getKey();
      for (OrderItem orderItem : entry.getValue()) {
        Supplier<PromotionOrderDetail> orderDetailSupplier =
            new PieceGroupDetailSupplier(
                mainOrder.getOrderNo(),
                order.getOrderNo(),
                orderItem.getId(),
                unit.getBuyer().getCpId(),
                pieceOrderExtra);
        promotionServiceAdapter.saveOrderDetail(orderDetailSupplier.get());
      }
    }
  }
}
