package com.xquark.service.pricing.vo;

import com.alibaba.fastjson.JSON;

/**
 * 第三方供应商统一返回
 *
 * @author wangxinhua
 * @date 2019-04-23
 * @since 1.0
 */
public class ThirdSupplierResult<T> {

    private final String code;
    private final String message;
    private final T data;

    public ThirdSupplierResult(String code, String message) {
        this.code = code;
        this.message = message;
        this.data = null;
    }

    public ThirdSupplierResult(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> ThirdSupplierResult<T> error(String code, String message) {
        return new ThirdSupplierResult<>(code, message);
    }

    public static <T> ThirdSupplierResult<T> success(T t) {
        return new ThirdSupplierResult<>("10000", "SUCCESS", t);
    }

    public String toJson() {
        return JSON.toJSONString(this);
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }
}
