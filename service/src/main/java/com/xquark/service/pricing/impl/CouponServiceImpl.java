package com.xquark.service.pricing.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestOperations;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.mapper.CouponActivityMapper;
import com.xquark.dal.mapper.CouponMapper;
import com.xquark.dal.model.CashierItem;
import com.xquark.dal.model.Coupon;
import com.xquark.dal.model.CouponActivity;
import com.xquark.dal.model.User;
import com.xquark.dal.status.CouponStatus;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.CouponGrantRule;
import com.xquark.dal.type.CouponType;
import com.xquark.dal.type.PaymentChannel;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pricing.CouponService;
import com.xquark.service.pricing.CouponVO;

@Service("couponService")
public class CouponServiceImpl extends BaseServiceImpl implements CouponService {

  @Autowired
  private CouponMapper couponMapper;

  @Autowired
  private CashierService cashierService;

  @Autowired
  private CouponActivityMapper couponActivityMapper;

  @Autowired
  private RestOperations restTemplate;

  @Value("${xiangqu.web.site}")
  private String xiangquWebSite;

  @Override
  public CouponVO loadByCouponCode(String actCode, String couponCode) {
    Coupon coupon = couponMapper.selectByCouponCode(actCode, couponCode);
    if (coupon == null) {
      return null;
    }
    CouponActivity activity = couponActivityMapper.selectByPrimaryKey(coupon.getActivityId());
    return new CouponVO(coupon, activity);
  }

  @Override
  public void autoGrantCoupon(String partner, String userId, String deviceId) {

    List<CouponActivity> couponAct = couponActivityMapper.selectAutoGrantAct(partner);
    for (CouponActivity act : couponAct) {
      grantCoupon(act.getActCode(), userId, deviceId);
    }
  }

  @Override
  public Coupon grantCoupon(String actCode, String userId, String deviceId) {
    //此接口只留给想去调用，第三方的优惠券只能调用grantExtCoupon方法,
    //该方法只有首单5元调用和6.1红包优惠使用
    CouponActivity activity = couponActivityMapper.selectByActCode(actCode);
    if (!validGrant(activity, userId, deviceId)) {
      return null;
    }

    Coupon coupon = new Coupon(activity.getId(), UUID.randomUUID().toString(),
        activity.getDiscount(),
        CouponStatus.VALID, userId, deviceId, activity.getValidFrom(), activity.getValidTo());

    couponMapper.insert(coupon);
    return coupon;
  }

  /**
   * 检查优惠券是否可以发放
   */
  private boolean validGrant(CouponActivity activity, String userId, String deviceId) {
    boolean rc = true;

    if (activity == null || !activity.isValid()) {
      return false;
      //throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动已经失效，不能发放或者领取优惠券");
    }

    if (activity.getGrantRule() == CouponGrantRule.DEVICE_SINGLE) {

      if (StringUtils.isBlank(deviceId)) {
        return false;
      }

      List<Coupon> coupons = couponMapper.selectByActIdAndUserId(activity.getId(), userId);
      if (coupons.size() > 0) {
        return false;
      }

      coupons = couponMapper.selectByActIdAndDeviceId(activity.getId(), deviceId);
      if (coupons.size() > 0) {
        return false;
      }
    } else if (activity.getGrantRule() == CouponGrantRule.SINGLE) {
      List<Coupon> coupons = couponMapper.selectByActIdAndUserId(activity.getId(), userId);
      if (coupons.size() > 0) {
        return false;
      }
    }

    return rc;
  }

  @Override
  public boolean grantCoupon(Coupon coupon, String userId) {
    //此接口只留给想去调用，第三方的优惠券只能调用grantExtCoupon方法
    if (coupon == null
        || (StringUtils.isNoneBlank(coupon.getUserId())
        && !userId.equals(coupon.getUserId()))
        || coupon.getStatus() != CouponStatus.VALID) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "优惠码不存在或者已经发放");
    }

    if (userId.equals(coupon.getUserId())) {
      //已经发放，无需重新发放
      return true;
    }

    int rc = couponMapper.grant(coupon.getId(), userId);
    return rc == 1;
  }

  @Override
  public Coupon grantExtCoupon(String actCode, String extCouponId, BigDecimal amount,
      String userId) {
    CouponActivity activity = couponActivityMapper.selectByActCode(actCode);
    if (!activity.isValid()) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动已经失效，不能发放或者领取优惠券");
    }

    Coupon coupon = couponMapper.selectByExtCouponId(extCouponId, actCode);
    if (coupon != null) {
      //已经发放，无需重新发放
      if (userId.equals(coupon.getUserId())) {
        return coupon;
      } else {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "优惠券已使用或被其他订单锁定");
      }
    }

    coupon = new Coupon(activity.getId(), UUID.randomUUID().toString(), activity.getDiscount(),
        CouponStatus.VALID, userId, null, activity.getValidFrom(), activity.getValidTo());

    if (extCouponId != null && amount != null) {
      coupon.setExtCouponId(extCouponId);
      coupon.setDiscount(amount);
    }

    couponMapper.insert(coupon);

    return coupon;
  }

  @Override
  public List<CouponVO> listValids(BigDecimal price, User user) {
    List<CouponVO> result = new ArrayList<CouponVO>();
    List<Coupon> list = couponMapper
        .selectByUserAndStatus(user.getId(), CouponStatus.VALID, user.getPartner());
    for (Coupon c : list) {
      CouponActivity activity = couponActivityMapper.selectByPrimaryKey(c.getActivityId());

      if (price != null && activity.getMinPrice() != null
          && activity.getMinPrice().compareTo(price) > 0) {
        continue;
      }
      //再加层限制，防止之前重复发放
      if (activity.getGrantRule() == CouponGrantRule.DEVICE_SINGLE) {
        boolean repeat = false;
        for (CouponVO cVo : result) {
          if (cVo.getActivityId().equals(c.getActivityId())) {
            couponMapper.close(c.getId());
            log.warn("优惠券多次发放1 id=" + c.getId() + "]");
            repeat = true;
            break;
          }
        }
        if (repeat) {
          continue;
        }
      }

      CouponVO vo = new CouponVO(c, activity);
      if (vo.getActivity().getDefaultSelect()) {
        vo.setSelected(true);
      }
      result.add(vo);
    }

    return result;

  }

  @Override
  public List<CouponVO> listValids(BigDecimal price) {
    User user = (User) getCurrentUser();
    return listValids(price, user);
  }

  @Override
  public Map<String, List<CouponVO>> listValidsWithExt(BigDecimal price, String orderNo,
      String channel) {
    //TODO 该方法太乱，须重构，逻辑先从activity表里获取活动列表，再取coupon

    Map<String, List<CouponVO>> mResult = new LinkedHashMap<String, List<CouponVO>>();
    List<CouponVO> allCoupons = listValids(price);

    //首单5元  TODO:
    for (CouponVO coupon : allCoupons) {
      if ("XQ.HONGBAO".equals(coupon.getActivity().getActCode()))  // 想去红包动态去获取
      {
        continue;
      }
      List<CouponVO> cps = mResult.get(coupon.getActivity().getActCode());
      cps = ObjectUtils.defaultIfNull(cps, new ArrayList<CouponVO>());

      //再加层限制，防止之前重复发放
      if ((coupon.getActivity().getGrantRule() == CouponGrantRule.DEVICE_SINGLE ||
          coupon.getActivity().getGrantRule() == CouponGrantRule.SINGLE)
          && cps.size() > 0) {
        couponMapper.close(coupon.getId());
        log.warn("优惠券多次发放 id=" + coupon.getId() + "]");
        continue;
      }

      cps.add(coupon);
      mResult.put(coupon.getActivity().getActCode(), cps);
    }

    //想去红包
    mResult.put("XQ.HONGBAO", findXiangquCoupon());

    //被本订单锁定的优惠券重新加入到本次支付
    List<CashierItem> cashierItems = null;
    if (StringUtils.isNoneBlank(orderNo)) {
      cashierItems = cashierService
          .listByBatchBizNo(orderNo, getCurrentUser().getId(), PaymentStatus.PENDING);
    } else {
      cashierItems = new ArrayList<CashierItem>();
    }
    for (CashierItem item : cashierItems) {
      if (StringUtils.isBlank(item.getCouponId()) || item.getStatus() != PaymentStatus.PENDING) {
        continue;
      }
      CouponVO vo = loadVO(item.getCouponId());
      if (!vo.getActivity().isValid() || vo.getStatus() != CouponStatus.LOCKED) {
        continue;
      }
      List<CouponVO> lockedCoupon = mResult.get(vo.getActivity().getActCode());
      lockedCoupon = ObjectUtils.defaultIfNull(lockedCoupon, new ArrayList<CouponVO>());
      lockedCoupon.add(vo);
      mResult.put(vo.getActivity().getActCode(), lockedCoupon);
    }

    List<String> removeActCodes = new ArrayList<String>();
    for (String actCode : mResult.keySet()) {
      CouponActivity actObj = couponActivityMapper.selectByActCode(actCode);
      //控制使用最低额度
      if (price != null && actObj.getMinPrice() != null
          && actObj.getMinPrice().compareTo(price) > 0) {
        removeActCodes.add(actCode);
      } else if (StringUtils.isNotBlank(channel) && StringUtils.isNotBlank(actObj.getChannel())
          && !actObj.getChannel().equals(channel)) {
        removeActCodes.add(actCode);
      }

    }

    for (String removeActCode : removeActCodes) {
      mResult.remove(removeActCode);
    }

    return mResult;
  }

  @Override
  public Coupon use(String couponId, String payNo) {
    int rc = couponMapper.use(couponId, payNo);
    if (rc != 1) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "优惠已经不存在或者已经发放");
    }
    return couponMapper.selectByPrimaryKey(couponId);
  }

  @Override
  public Coupon lock(String couponId, String payNo) {
    int rc = couponMapper.lock(couponId, payNo);
    if (rc != 1) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "优惠已经不存在或者已经发放");
    }
    return couponMapper.selectByPrimaryKey(couponId);
  }

  @Override
  public Coupon unLock(String couponId, String payNo) {
    int rc = couponMapper.unLock(couponId);
    if (rc != 1) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "优惠已经不存在或者已经发放");
    }
    return couponMapper.selectByPrimaryKey(couponId);
  }

//    @Override
//    public Coupon restore(String couponId) {
//        couponMapper.updateStatus(couponId, CouponStatus.VALID);
//        return couponMapper.selectByPrimaryKey(couponId);
//    }

  @Override
  public CouponVO loadVO(String id) {
    Coupon coupon = couponMapper.selectByPrimaryKey(id);
    CouponActivity activity = couponActivityMapper.selectByPrimaryKey(coupon.getActivityId());
    return new CouponVO(coupon, activity);
  }

  @Override
  public Coupon load(String id) {
    return couponMapper.selectByPrimaryKey(id);
  }

  @Override
  public CouponActivity loadByActCode(String actCode) {
    return couponActivityMapper.selectByActCode(actCode);
  }

  @Override
  public List<Coupon> listCouponsByAdmin(Map<String, Object> params, Pageable page) {
    return couponMapper.listCouponsByAdmin(params, page);
  }

  @Override
  public List<CouponActivity> listCouponActivityByAdmin() {
    return couponActivityMapper.selectByAdmin();
  }

  @Override
  public Long countCouponsByAdmin(Map<String, Object> params) {
    return couponMapper.countCouponsByAdmin(params);
  }

  @Override
  @Transactional
  public int create(List<String> listCodes, Map<String, Object> params) {
    return couponMapper.create(listCodes, params);
  }

  @Override
  public List<Coupon> listByAdmin(Map<String, Object> params, Pageable pageable) {
    return couponMapper.selectByAdmin(params, pageable);
  }

  @Override
  public Boolean delete(String[] ids) {
    return couponMapper.delete(ids);
  }

  @Override
  public Coupon obtainCoupon(String code) {
    return couponMapper.obtainCoupon(code);
  }

  @Override
  public List<CashierItem> writeToCashier(List<Coupon> coupons, String pNo, BigDecimal totalFee,
      String userId, String productId, String productName, String partner,
      String bizNos) {
    List<Coupon> realCoupons = new ArrayList<Coupon>();

    for (Coupon coupon : coupons) {

      Coupon realCoupon = null;
      if (totalFee.compareTo(BigDecimal.ZERO) < 1) {
        break;
      }

      CouponActivity actObj = couponActivityMapper.selectByActCode(coupon.getActivityId());

//   			if( "XQ.FIRST".equals(coupon.getActivityId()) || "XQ.61".equals(coupon.getActivityId())){
      if (actObj.getGrantLocal() && actObj.getCouponType() == CouponType.REDUCTION) {
        //首单减5元和6.1红包
        realCoupon = load(coupon.getId());
        if (realCoupon == null || (StringUtils.isNoneBlank(realCoupon.getUserId())
            && !getCurrentUser().getId().equals(realCoupon.getUserId()))) {

          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "优惠码不存在或者已经被使用！id=[" + coupon.getId() + "]");
        }
        realCoupons.add(realCoupon);
      } else if ("XQ.COUPONCODE".equals(coupon.getActivityId())) {
        //优惠码
        realCoupon = loadByCouponCode(coupon.getActivityId(), coupon.getCode());
        if (realCoupon == null || (StringUtils.isNoneBlank(realCoupon.getUserId())
            && !getCurrentUser().getId().equals(realCoupon.getUserId()))) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "优惠码不存在或者已经使用！");
        }

        grantCoupon(realCoupon, getCurrentUser().getId());
        realCoupons.add(realCoupon);
      } else if ("XQ.HONGBAO".equals(coupon.getActivityId())) {
        List<CouponVO> hongbaos = findXiangquCoupon();
        for (CouponVO hongbao : hongbaos) {
          if (hongbao.getExtCouponId().equals(coupon.getExtCouponId())) {
            coupon.setDiscount(hongbao.getDiscount());
            break;
          }
        }

        Coupon c = couponMapper
            .selectByExtCouponId(coupon.getExtCouponId(), coupon.getActivityId());
        if (c != null) {
          coupon.setDiscount(c.getDiscount());
        }

        if (coupon.getDiscount() == null) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "红包不存在或者已经使用！id=[" + coupon.getId() + "]");
        }
        BigDecimal discount = coupon.getDiscount().setScale(2, BigDecimal.ROUND_DOWN);
        realCoupon = grantExtCoupon(coupon.getActivityId(), coupon.getExtCouponId(), discount,
            getCurrentUser().getId());
        realCoupons.add(realCoupon);
      }
      if (realCoupon == null) {
        continue;
      }
      if (totalFee.compareTo(realCoupon.getDiscount()) == -1) {
        realCoupon.setDiscount(totalFee);
      }
      totalFee = totalFee.subtract(realCoupon.getDiscount());

    }
    //更新收银台
    List<CashierItem> itmes = createCashier(realCoupons, pNo, userId, productId, productName,
        partner, bizNos);
//   		cashierService.save(itmes, pNo, CashierType.COUPON);
    return itmes;
  }

  private List<CashierItem> createCashier(List<Coupon> coupons, String bizNo,
      String userId, String productId, String productName, String partner,
      String bizNos) {
    List<CashierItem> items = new ArrayList<CashierItem>();
    if (coupons == null || coupons.size() == 0) {
      return items;
    }

//		BigDecimal totalFee = order.getTotalFee();
    for (Coupon coupon : coupons) {
      CashierItem couponItem = new CashierItem();
      couponItem.setBizNo(bizNo);
      couponItem.setPaymentChannel(PaymentChannel.PARTNER);
      couponItem.setUserId(userId);
      couponItem.setAmount(coupon.getDiscount());
      couponItem.setCouponId(coupon.getId());
      couponItem.setBankCode("");
      couponItem.setBankName("");
      couponItem.setAgreementId(null);
      couponItem.setProductId(productId);
      couponItem.setProductName(productName);
      couponItem.setPartner(partner);
      couponItem.setBatchBizNos(bizNos);
      couponItem.setStatus(PaymentStatus.PENDING);
      couponItem.setPaymentMode(PaymentMode.XIANGQU);
      items.add(couponItem);
    }
    return items;
  }

  @Override
  public CouponVO loadExtCouponByExtId(String extCouponId) {
    List<CouponVO> couponVOs = findXiangquCoupon();
    for (CouponVO vo : couponVOs) {
      if (vo.getExtCouponId().equals(extCouponId)) {
        return vo;
      }
    }
    return null;
  }

  /**
   * 获取想去红包列表
   */
  private List<CouponVO> findXiangquCoupon() {
    List<CouponVO> list = new ArrayList<CouponVO>();

    User user = (User) getCurrentUser();
    //通过第三方的userId,获取红包优惠
    if (!"xiangqu".equals(user.getPartner()) || StringUtils.isBlank(user.getExtUserId())) {
      return list;
    }

    String gateway =
        xiangquWebSite + "/restapi/coupon/list-valids-in-cart?userId=" + user.getExtUserId();
    try {
      String resp = restTemplate.getForObject(gateway, String.class, new Object[]{});
      JSONObject json = JSONObject.parseObject(resp);
      JSONArray array = json.getJSONArray("data");
      if (array != null && array.size() > 0) {
        for (int i = 0; i < array.size(); i++) {
          JSONObject hongbao = array.getJSONObject(i);
          CouponVO vo = new CouponVO();
          vo.setExtCouponId(hongbao.getString("id"));
          vo.setDiscount(NumberUtils.createBigDecimal(hongbao.getString("amount")));
          vo.setActivity(loadByActCode("XQ.HONGBAO"));
          vo.setStatus(CouponStatus.VALID);
          list.add(vo);
        }
      }
    } catch (Exception e) {
      log.error("想去调用红包接口出错，gateway=[" + gateway + "]");
    }
    return list;
  }

  @Override
  public List<CouponVO> dealCoupons(Map<String, List<CouponVO>> couponMap) {
    List<CouponVO> promotions = new ArrayList<CouponVO>();

    // TODO: 不能写死代码
//        if (couponMap.containsKey("XQ.FIRST")) {
//        	promotions.addAll(couponMap.get("XQ.FIRST"));
//        }
//        if (couponMap.containsKey("XQ.61")) {
//        	promotions.addAll(couponMap.get("XQ.61"));
//        }
//        
//        Set<Map.Entry<String, List<CouponVO>>> set = couponMap.entrySet();
//        for (Iterator<Map.Entry<String, List<CouponVO>>> it = set.iterator(); it.hasNext();) {
//            Map.Entry<String, List<CouponVO>> entry = (Map.Entry<String, List<CouponVO>>) it.next();
//            if (entry.getKey().equals("XQ.FIRST")) continue;
//            if (entry.getKey().equals("XQ.61")) continue;
//        	promotions.addAll(entry.getValue());
//        }

    //排序问题放营销系统解决
    if (couponMap.containsKey("XQ.FIRST")) {
      promotions.addAll(couponMap.get("XQ.FIRST"));
    }

    for (String actCode : couponMap.keySet()) {
      if (actCode.equals("XQ.FIRST")) {
        continue;
      }
      promotions.addAll(couponMap.get(actCode));
    }

    int cnt = 0;
    for (CouponVO vo : promotions) {
      if (CouponStatus.LOCKED.equals(vo.getStatus())) {
        vo.setSelected(true);
        cnt++;
      }
    }
    if (cnt != 0) {
      clearDefaultSelect(promotions);
    }

    return promotions;
  }

  private void clearDefaultSelect(List<CouponVO> list) {
    if (list == null || list.size() == 0) {
      return;
    }
    for (CouponVO vo : list) {
      if (vo.getActivity() != null && vo.getActivity().getDefaultSelect()) {
        vo.getActivity().setDefaultSelect(false);
        vo.setSelected(false);
      }
    }
  }

  @Override
  @Transactional
  public int autoCloseActivity() {
    List<CouponActivity> acts = couponActivityMapper.selectShouldCloseAct();

    for (CouponActivity act : acts) {
      couponMapper.closeByActId(act.getId());
      couponActivityMapper.close(act.getId());
    }

    return acts.size();
  }


}
