package com.xquark.service.pricing.vo;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;

public class OrderPricingVO {

  private BigDecimal totalFee;

  private BigDecimal discountFee;

  private BigDecimal logisticsFee;

  private List<PricingResultVOEX> pricingList;

  public BigDecimal getTotalFee() {
    return totalFee;
  }


  public void setTotalFee(BigDecimal totalFee) {
    this.totalFee = totalFee;
  }


  public BigDecimal getDiscountFee() {
    return discountFee;
  }


  public void setDiscountFee(BigDecimal discountFee) {
    this.discountFee = discountFee;
  }


  public List<PricingResultVOEX> getPricingList() {
    return pricingList;
  }


  public void setPricingList(List<PricingResultVOEX> pricingList) {
    this.pricingList = pricingList;
  }


  public BigDecimal getLogisticsFee() {
    return logisticsFee;
  }


  public void setLogisticsFee(BigDecimal logisticsFee) {
    this.logisticsFee = logisticsFee;
  }


}
