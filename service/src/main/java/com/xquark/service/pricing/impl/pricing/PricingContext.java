package com.xquark.service.pricing.impl.pricing;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionPgPrice;
import com.xquark.dal.type.LogisticDiscountType;
import com.xquark.service.yundou.CommissionDiscountingRet;
import com.xquark.service.yundou.PointCommDeductionResult;
import com.xquark.service.yundou.PointDiscountingRet;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/** Created by wangxinhua on 2018/4/18. DESC: 价格计算上下文对象 */
public class PricingContext {

  public PricingContext(BigDecimal currFee) {
    this.currFee = currFee;
  }

  /** 保存每次计算过后的当前折扣 */
  private BigDecimal currDiscount = BigDecimal.ZERO;

  /** 保存当前使用的活动信息 */
  private Map<String, Promotion> currPromotions = new HashMap<>();

  /** 计算时的商品原价 */
  private BigDecimal currFee;

  private PointDiscountingRet pointRet;
  private CommissionDiscountingRet commissionRet;

  /**是否包邮*/
  private boolean isFreeDelivery;

  /**邮费*/
  private BigDecimal logistics = BigDecimal.ZERO;

  /**邮费减免*/
  private BigDecimal logisticsDiscount = BigDecimal.ZERO;

  private LogisticDiscountType logisticDiscountType;

  /** 活动价格体系 */
  private PromotionPgPrice promotionPgPrice;

  public PromotionPgPrice getPromotionPgPrice() {
    return promotionPgPrice;
  }

  public void setPromotionPgPrice(PromotionPgPrice promotionPgPrice) {
    this.promotionPgPrice = promotionPgPrice;
  }

  public LogisticDiscountType getLogisticDiscountType() {
    return logisticDiscountType;
  }

  public void setLogisticDiscountType(LogisticDiscountType logisticDiscountType) {
    this.logisticDiscountType = logisticDiscountType;
  }

  public BigDecimal getLogistics() {
    return logistics;
  }

  public void setLogistics(BigDecimal logistics) {
    this.logistics = logistics;
  }

  public BigDecimal getLogisticsDiscount() {
    return logisticsDiscount;
  }

  public void setLogisticsDiscount(BigDecimal logisticsDiscount) {
    this.logisticsDiscount = logisticsDiscount;
  }

  public boolean isFreeDelivery() {
    return isFreeDelivery;
  }

  public void setFreeDelivery(boolean freeDelivery) {
    isFreeDelivery = freeDelivery;
  }

  public void addDiscount(BigDecimal discount) {
    if (discount == null) {
      return;
    }
    if (currFee.subtract(discount).signum() < 0) {
      currFee = BigDecimal.ZERO;
      return;
    }
    this.currDiscount = this.currDiscount.add(discount);
    this.currFee = this.currFee.subtract(discount);
  }

  public BigDecimal getCurrDiscount() {
    return currDiscount;
  }

  public BigDecimal getCurrFee() {
    return currFee;
  }

  public Promotion getCurrUsingPromotion(String prodId) {
    return currPromotions.get(prodId);
  }

  public Map<String, Promotion> getCurrPromotions() {
    return currPromotions;
  }

  public Map<String, Promotion> getUsingPromotions() {
    return currPromotions;
  }

  public void addCurrUsingPromotion(String prodId, Promotion promotion) {
    this.currPromotions.put(prodId, promotion);
  }

  public PointDiscountingRet getPointRet() {
    return pointRet;
  }

  public void setPointRet(PointDiscountingRet pointRet) {
    this.pointRet = pointRet;
  }

  public CommissionDiscountingRet getCommissionRet() {
    return commissionRet;
  }

  public void setCommissionRet(CommissionDiscountingRet commissionRet) {
    this.commissionRet = commissionRet;
  }

  /** 通过该方法取出积分、德分计算结果 */
  public PointCommDeductionResult getPointCommRet() {
    PointCommDeductionResult ret = PointCommDeductionResult.empty();
    if (pointRet != null) {
      ret.setMaxUsingPoint(pointRet.getMaxUsing());
      ret.setUsingPoint(pointRet.getUsing());
      ret.setUsingPointDeductionPrice(pointRet.getDiscount());
    }
    if (commissionRet != null) {
      ret.setMaxUsingCommission(commissionRet.getMaxUsing());
      ret.setUsingCommission(commissionRet.getUsing());
      ret.setUsingCommissionDeductionPrice(commissionRet.getMaxUsing());
      ret.setMaxUsingCommissionAddPoint(commissionRet.getMaxUsingComissionAddPoint());
    }
    return ret;
  }
}
