package com.xquark.service.pricing.vo;

import java.math.BigDecimal;

public class SkuPriceVO {

  // 市场价
  private BigDecimal marketPrice;

  // 现价，活动价
  private BigDecimal retailPrice;

  public BigDecimal getMarketPrice() {
    return marketPrice;
  }

  public void setMarketPrice(BigDecimal marketPrice) {
    this.marketPrice = marketPrice;
  }

  public BigDecimal getRetailPrice() {
    return retailPrice;
  }

  public void setRetailPrice(BigDecimal retailPrice) {
    this.retailPrice = retailPrice;
  }

}
