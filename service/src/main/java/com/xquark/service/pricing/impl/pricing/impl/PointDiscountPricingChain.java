package com.xquark.service.pricing.impl.pricing.impl;

import com.hds.xquark.dal.model.PointTotal;
import com.hds.xquark.service.point.PointCommService;
import com.xquark.dal.consts.PointConstants;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.Sku;
import com.xquark.dal.type.PromotionType;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pricing.impl.pricing.*;
import com.xquark.service.pricing.impl.pricing.hooks.PointBeforeHook;
import com.xquark.service.yundou.PointDiscountingRet;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import static com.xquark.service.pointgift.PointGiftUtil.pointDetail;

/**
 * created by
 *
 * @author wangxinhuat 18-6-29 下午11:01 德分优惠链
 */
public class PointDiscountPricingChain extends DiscountPricingChain {

  private static Logger logger = Logger.getLogger(PointDiscountPricingChain.class.getName());

  private final PointCommService pointCommService;

  public PointDiscountPricingChain(PointCommService pointCommService) {
    this.pointCommService = pointCommService;
  }

  @Override
  protected DiscountPricingResult calculatePrice(
      PricingChainParams params, PricingContext context) {
    DiscountPricingResult ret = DiscountPricingResult.emptyResult();
    PointTotal pointTotal = pointCommService.loadByCpId(params.getBuyer().getCpId());
    // 用户德分信息未初始化
    if (pointTotal == null) {
      return ret;
    }
    // 当前可用德分需要加上红包德分部分
    BigDecimal currPoint = pointTotal.getTotalUsableWithPacket();
    BigDecimal needPoint = BigDecimal.ZERO;
    Map<String, BigDecimal> maxPointDeductionMap = new HashMap<>();
    for (CartItemVO cartItem : params.getCartItems()) {
      Sku sku = cartItem.getSku();
      BigDecimal point = sku.getDeductionDPoint().multiply(cartItem.getBAmount());
      Promotion currUsingPromotion = context.getCurrUsingPromotion(cartItem.getProductId());
      if (currUsingPromotion != null) {
        // 如果活动单独配置了德分则覆盖商品的德分
        if (currUsingPromotion.getPoint() != null) {
          point = currUsingPromotion.getPoint().multiply(cartItem.getBAmount());
        }
        // 在有活动的情况下且配置了可用德分的情况下, 德分部分要同步打折
        if (!currUsingPromotion.getIsPointUsed()) {
          // 不能用德分的情况下point置为0
          point = BigDecimal.ZERO;
        } else if (currUsingPromotion.isPointDiscount()) {
          // 配置了活动可用德分，德分要同步打折
          BigDecimal discount = currUsingPromotion.getDiscount();
          if (discount != null && discount.signum() > 0) {
            point =
                point
                    .multiply(currUsingPromotion.getDiscount())
                    .divide(BigDecimal.valueOf(10), 2, BigDecimal.ROUND_HALF_EVEN);
          }
        }
      }

      BigDecimal pointForDivided =
          Optional.ofNullable(currUsingPromotion)
              .filter(p -> p.getPrice() != null && p.getPrice().compareTo(BigDecimal.ZERO) > 0)
              .map(Promotion::getPoint)
              .orElse(point);
      maxPointDeductionMap.put(sku.getId(), pointForDivided);
      if (point != null) {
        needPoint = needPoint.add(point);
      }
    }

    // 计算可使用的最大积分
    BigDecimal usablePoint;
    if (currPoint.compareTo(needPoint) > 0) {
      usablePoint = needPoint;
    } else {
      usablePoint = currPoint;
    }

    // 抹掉德分的小数位，让积分来抵扣
    usablePoint = usablePoint.setScale(0, BigDecimal.ROUND_DOWN);

    BigDecimal userSelectPoint = params.getUserSelectPoint();
    BigDecimal discount = BigDecimal.ZERO;

    // 最大折扣放在上下文中, 用来积分计算
    BigDecimal maxDiscount =
        usablePoint.divide(PointConstants.POINT_SCALA, 2, BigDecimal.ROUND_HALF_EVEN);
    BigDecimal currFee = context.getCurrFee();
    BigDecimal discountResult = currFee.subtract(maxDiscount);
    if (discountResult.signum() < 0) {
      maxDiscount = currFee;
    }
    context.addDiscount(maxDiscount);

    if (userSelectPoint != null) {
      if (userSelectPoint.compareTo(usablePoint) > 0) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "用户选择优惠大于实际可使用");
      }

      discount = userSelectPoint.divide(PointConstants.POINT_SCALA, 2, BigDecimal.ROUND_HALF_EVEN);
    }

    if (discount.signum() > 0) {
      // 放入折扣, 上下文中保存的值应该为 "计算值"
      // 即在订单确认页需要显示的折扣
      // 该处的折扣在订单提交时才计算因此不放入context
      Pair<BigDecimal, BigDecimal> pointPacket = pointDetail(pointTotal, userSelectPoint);
      BigDecimal usingPoint = pointPacket.getLeft();
      BigDecimal usingPointPacket = pointPacket.getRight();
      ret.addDiscount(discount, calculateScope());
      Map<String, SkuDiscountDetail> devidePointDetail =
          devidePointDiscount(maxPointDeductionMap, usingPoint);
      Map<String, SkuDiscountDetail> devidePacketPointDetail =
          devidePointDiscount(maxPointDeductionMap, usingPointPacket);
      ret.addBeforeOrderHook(
          new PointBeforeHook(
              usingPoint, usingPointPacket, devidePointDetail, devidePacketPointDetail));
    }
    context.setPointRet(
        new PointDiscountingRet(usablePoint, userSelectPoint, discount, BigDecimal.ZERO));
    return ret;
  }

  /** 摊薄德分到每个sku */
  private Map<String, SkuDiscountDetail> devidePointDiscount(
      Map<String, BigDecimal> pointMap, BigDecimal using) {
    return divideDiscount("", pointMap, using, 0);
  }

  @Override
  protected PromotionType calculateScope() {
    return PromotionType.POINT;
  }
}
