package com.xquark.service.pricing;

import org.springframework.beans.BeanUtils;

import com.xquark.dal.model.Coupon;
import com.xquark.dal.model.CouponActivity;

public class CouponVO extends Coupon {

  private CouponActivity activity;

  private boolean valid = true;

  private Boolean selected;

  public CouponVO() {
  }

  public CouponVO(Coupon c, CouponActivity activity) {
    BeanUtils.copyProperties(c, this);
    this.activity = activity;
  }

  public CouponActivity getActivity() {
    return activity;
  }

  public void setActivity(CouponActivity activity) {
    this.activity = activity;
  }

  public boolean isValid() {
    return valid;
  }

  public void setValid(boolean valid) {
    this.valid = valid;
  }

  public Boolean getSelected() {
    return selected;
  }

  public void setSelected(Boolean selected) {
    this.selected = selected;
  }
}
