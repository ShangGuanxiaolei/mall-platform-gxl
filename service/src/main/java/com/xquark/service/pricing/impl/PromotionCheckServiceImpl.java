package com.xquark.service.pricing.impl;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.status.PromotionStatus;
import com.xquark.dal.vo.FlashSalePromotionProductVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.pricing.PromotionCheckService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.yundou.YundouProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

/**
 * Created by wangxinhua on 17-7-27. DESC:
 */
@Service
public class PromotionCheckServiceImpl extends BaseServiceImpl implements PromotionCheckService {

  private YundouProductService yundouProductService;

  private PromotionService promotionService;

  @Autowired
  private FlashSalePromotionProductService flashSalePromotionProductService;

  @Override
  public PromotionStatus checkFlashSaleProduct(ProductVO productVO, Promotion promotion,
      String bizId,
      Model model) {
    FlashSalePromotionProductVO promotionProduct = (FlashSalePromotionProductVO) flashSalePromotionProductService
        .loadPromotionProductByPId(productVO.getId());
    if (promotionProduct != null) {
      productVO.setFlashsaleVO(promotionProduct);
      productVO.setIsFlashsale(true);
      productVO.setDiscountPrice(promotionProduct.getPromotionPrice());
    }
    return PromotionStatus.NORMAL;
  }


  @Autowired
  public void setYundouProductService(YundouProductService yundouProductService) {
    this.yundouProductService = yundouProductService;
  }

  @Autowired
  public void setPromotionService(PromotionService promotionService) {
    this.promotionService = promotionService;
  }

}
