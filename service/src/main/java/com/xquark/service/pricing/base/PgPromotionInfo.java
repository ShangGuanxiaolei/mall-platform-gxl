package com.xquark.service.pricing.base;

import com.xquark.dal.type.PromotionType;

/**
 * @author wangxinhua
 * @since 18-11-1
 */
public class PgPromotionInfo extends PromotionInfo {

  public PgPromotionInfo() {
  }


  /**
   * [optional] 不强制构造, 方便反射创建
   */
  public PgPromotionInfo(String promotionId, PromotionType promotionType, String tranCode, String detailCode,Boolean isFreePay) {
    super(promotionId, promotionType);
    this.tranCode = tranCode;
    this.detailCode = detailCode;
    this.isFreePay = isFreePay;
  }

  private Boolean isFreePay;
  /**
   * 拼团唯一标识代码
   */
  private String tranCode;

  private String detailCode;

  public String getTranCode() {
    return tranCode;
  }

  public Boolean isFreePay() {
    return isFreePay;
  }

  public void setFreePay(Boolean freePay) {
    isFreePay = freePay;
  }

  public void setTranCode(String tranCode) {
    this.tranCode = tranCode;
  }

  public String getDetailCode() {
    return detailCode;
  }

  public void setDetailCode(String detailCode) {
    this.detailCode = detailCode;
  }

}
