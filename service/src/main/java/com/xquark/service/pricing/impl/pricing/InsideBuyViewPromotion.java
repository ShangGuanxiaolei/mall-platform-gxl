package com.xquark.service.pricing.impl.pricing;

import static com.xquark.dal.type.PromotionType.INSIDE_BUY;

import com.xquark.dal.model.Promotion;
import java.math.BigDecimal;

/**
 * Created by wangxinhua on 2018/4/18. DESC:
 */
public class InsideBuyViewPromotion extends Promotion {

  public InsideBuyViewPromotion(BigDecimal discount, boolean freeDelivery) {
    setId("");
    setTitle("员工内购优惠");
    setDiscount(discount);
    setType(INSIDE_BUY);
    setIsFreeDelivery(freeDelivery);
  }

}
