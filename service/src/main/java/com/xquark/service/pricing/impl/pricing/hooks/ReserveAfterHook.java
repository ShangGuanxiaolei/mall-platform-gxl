package com.xquark.service.pricing.impl.pricing.hooks;

import com.xquark.dal.model.Order;
import com.xquark.dal.status.OrderStatus;
import com.xquark.service.pricing.impl.pricing.OrderUnit;
import com.xquark.service.reserve.PromotionReserveService;

import java.util.function.Predicate;

/**
 * 订单保存后更新预售活动订单信息
 * @author wangxinhua
 * @date 2019-05-07
 * @since 1.0
 */
public class ReserveAfterHook extends OrderHook {

    private final String preOrderNo;
    private final PromotionReserveService promotionReserveService;

    public ReserveAfterHook(String preOrderNo, PromotionReserveService promotionReserveService) {
        this.preOrderNo = preOrderNo;
        this.promotionReserveService = promotionReserveService;
    }

    @Override
    public void accept(OrderUnit orderUnit) {
        orderUnit.getOrderMap().keySet()
                .stream()
                .map(Order::getPreOrderNo)
                .filter(Predicate.isEqual(preOrderNo))
                .forEach(preOrderNo -> promotionReserveService.updateReserveOrderStatus(preOrderNo, OrderStatus.PAID));
    }
}
