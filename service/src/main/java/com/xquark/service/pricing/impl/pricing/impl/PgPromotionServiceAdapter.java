package com.xquark.service.pricing.impl.pricing.impl;

import com.xquark.dal.mapper.OrderItemMapper;
import com.xquark.dal.mapper.PromotionOrderDetailMapper;
import com.xquark.dal.mapper.PromotionTempStockMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.model.mypiece.PGMemberInfoVo;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.dal.model.mypiece.StockCheckBean;
import com.xquark.dal.model.promotion.OrderSkuAmount;
import com.xquark.dal.status.PieceStatus;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionGroupDetailVO;
import com.xquark.dal.vo.PromotionTranInfoVO;
import com.xquark.service.cantuan.PieceGroupPayCallBackService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.error.NonCheckBizException;
import com.xquark.service.pintuan.*;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pricing.base.PieceStateException;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.promotion.PromotionOrderDetailService;
import com.xquark.thirds.umpay.api.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author wangxinhua
 * @since 18-11-1 适配器service 适配新的平团业务
 */
@Service
public class PgPromotionServiceAdapter {

  private Logger logger = LoggerFactory.getLogger(PgPromotionServiceAdapter.class);

  private PromotionBaseInfoService promotionBaseInfoService;

  private RecordPromotionPgTranInfoService recordPromotionPgTranInfoService;

  private RecordPromotionPgMemberInfoService recordPromotionPgMemberInfoService;

  private PieceGroupPayCallBackService pieceGroupPayCallBackService;

  private PromotionGroupDetailService promotionGroupDetailService;

  private PromotionOrderDetailService promotionOrderDetailService;

  private RecordPromotionPgDetail recordPromotionPgDetailService;

  private PromotionPgMemberInfoService promotionPgMemberInfoService;

  private PromotionTranInfoService promotionTranInfoService;

  private GroupService groupService;

  private PromotionTempStockMapper tempStockMapper;

  private PromotionOrderDetailMapper promotionOrderDetailMapper;

  private PromotionPgPriceService promotionPgPriceService;

  private PromotionPgMasterPriceService promotionPgMasterPriceService;

  private CustomerProfileService customerProfileService;

  private SkuMapper skuMapper;

  private PromotionPgService promotionPgService;

  private PromotionGoodsDetailService promotionGoodsDetailService;

  private OrderItemMapper orderItemMapper;

  @Autowired
  public void setPromotionGoodsDetailService(
      PromotionGoodsDetailService promotionGoodsDetailService) {
    this.promotionGoodsDetailService = promotionGoodsDetailService;
  }

  @Autowired
  public void setPromotionBaseInfoService(PromotionBaseInfoService promotionBaseInfoService) {
    this.promotionBaseInfoService = promotionBaseInfoService;
  }

  @Autowired
  public void setRecordPromotionPgTranInfoService(
          RecordPromotionPgTranInfoService recordPromotionPgTranInfoService) {
    this.recordPromotionPgTranInfoService = recordPromotionPgTranInfoService;
  }

  @Autowired
  public void setRecordPromotionPgMemberInfoService(
          RecordPromotionPgMemberInfoService recordPromotionPgMemberInfoService) {
    this.recordPromotionPgMemberInfoService = recordPromotionPgMemberInfoService;
  }

  @Autowired
  public void setPieceGroupPayCallBackService(
          PieceGroupPayCallBackService pieceGroupPayCallBackService) {
    this.pieceGroupPayCallBackService = pieceGroupPayCallBackService;
  }

  @Autowired
  public void setPromotionGroupDetailService(
          PromotionGroupDetailService promotionGroupDetailService) {
    this.promotionGroupDetailService = promotionGroupDetailService;
  }

  @Autowired
  public void setPromotionOrderDetailService(
          PromotionOrderDetailService promotionOrderDetailService) {
    this.promotionOrderDetailService = promotionOrderDetailService;
  }

  @Autowired
  public void setRecordPromotionPgDetailService(
          RecordPromotionPgDetail recordPromotionPgDetailService) {
    this.recordPromotionPgDetailService = recordPromotionPgDetailService;
  }

  @Autowired
  public void setPromotionPgMemberInfoService(
          PromotionPgMemberInfoService promotionPgMemberInfoService) {
    this.promotionPgMemberInfoService = promotionPgMemberInfoService;
  }

  @Autowired
  public void setPromotionTranInfoService(PromotionTranInfoService promotionTranInfoService) {
    this.promotionTranInfoService = promotionTranInfoService;
  }

  @Autowired
  public void setGroupService(GroupService groupService) {
    this.groupService = groupService;
  }

  @Autowired
  public void setPromotionOrderDetailMapper(
          PromotionOrderDetailMapper promotionOrderDetailMapper) {
    this.promotionOrderDetailMapper = promotionOrderDetailMapper;
  }

  @Autowired
  public void setTempStockMapper(PromotionTempStockMapper tempStockMapper) {
    this.tempStockMapper = tempStockMapper;
  }

  @Autowired
  public void setPromotionPgPriceService(PromotionPgPriceService promotionPgPriceService) {
    this.promotionPgPriceService = promotionPgPriceService;
  }

  @Autowired
  public void setPromotionPgMasterPriceService(PromotionPgMasterPriceService promotionPgMasterPriceService) {
    this.promotionPgMasterPriceService = promotionPgMasterPriceService;
  }

  @Autowired
  public void setCustomerProfileService(CustomerProfileService customerProfileService) {
    this.customerProfileService = customerProfileService;
  }

  @Autowired
  public void setSkuMapper(SkuMapper skuMapper) {
    this.skuMapper = skuMapper;
  }

  @Autowired
  public void setOrderItemMapper(OrderItemMapper orderItemMapper) {
    this.orderItemMapper = orderItemMapper;
  }

  /**
   * 拼主价不设置
   */
  private static final int NOTSETUP = 0;

  /**
   * 拼主价统一设置
   */
  private static final int UNIFIEDSETUP = 1;

  /**
   * 拼主价分别设置
   */
  private static final int PARTSETUP = 2;

  @Autowired
  public void setPromotionPgService(PromotionPgService promotionPgService) {
    this.promotionPgService = promotionPgService;
  }

  public BigDecimal showOpenMemberPrice(String pDetailCode) {
    return promotionGroupDetailService.showOpenMemberPrice(pDetailCode);
  }

  /**
   * 获取当前团的订单信息
   *
   * @param tranCode tranCode
   * @return Amount
   */
  public List<OrderSkuAmount> loadOrderDetail(String tranCode) {
    return promotionOrderDetailService.loadDetailByTranCode(tranCode);
  }

  /**
   * 获取拼团信息
   */
  public PromotionBaseInfo loadBaseInfo(String pCode) {
    return promotionBaseInfoService.selectPromotionByPCode(pCode);
  }

  /**
   * 获取新人免单
   */
  public boolean loadFreshPromotion(String pDetailCode) {
    return promotionGroupDetailService.findFreshPromotionByPCode(pDetailCode);
  }

  /** 获取拼团折扣信息 */
  public BigDecimal loadPgDiscount(String pCode, String detailCode) {
    return recordPromotionPgTranInfoService.loadPgDiscount(pCode, detailCode);
  }

  /** 校验用户是否已经参与过拼团 */
  public boolean hadJoined(Long cpId, String tranCode) {
    // 注意valueOf方法会把 null -> "null"
    return pieceGroupPayCallBackService.hadJoined(tranCode, String.valueOf(cpId));
  }

  /**
   * 检查拼团状态
   */
  public void checkPgStatus(Long cpId, String pCode, String tranCode, Integer amount, String skuCode) {
    if (skuCode == null) {
      throw new NonCheckBizException(GlobalErrorCode.PIECE_ERROR, "活动商品skuCode不存在");
    }

    if (StringUtil.isNotEmpty(tranCode) && hadJoined(cpId, tranCode)) {
      throw new BizException(GlobalErrorCode.PIECE_ERROR, "请勿重复参团");
    }

    StockCheckBean stockCheckBean = new StockCheckBean();
    stockCheckBean.setP_code(pCode);
    stockCheckBean.setSku_code(skuCode);
    stockCheckBean.setTranCode(tranCode);
    if (amount != null && amount > 0) {
      stockCheckBean.setP_sku_num(amount);
    } else {
      stockCheckBean.setP_sku_num(3);
      logger.debug("获取的数量为空，设置默认值为3");
    }

    try {
      groupService.orderingCheck(stockCheckBean);
    } catch (PieceStateException e) {
      throw new NonCheckBizException(GlobalErrorCode.PIECE_ERROR, e.getMessage());
    }
  }

  /**
   * 查询拼团vo信息
   */
  public PGTranInfoVo loadPGTranInfo(String tranCode) {
    return recordPromotionPgTranInfoService.loadPGTranInfo(tranCode);
  }

  /** 保存拼团信息 */
  public boolean saveTranInfo(PGTranInfoVo tranInfo) {
    return recordPromotionPgTranInfoService.save(tranInfo);
  }

  /** 保存参团成员信息 */
  public boolean saveMemberInfo(PGMemberInfoVo memberInfo) {
    return recordPromotionPgMemberInfoService.save(memberInfo);
  }

  /** 查询参团信息 */
  public PGMemberInfoVo loadMemberInfo(String memberCode) {
    return recordPromotionPgMemberInfoService.loadByCode(memberCode);
  }

  /** 查询拼团人员详情 */
  public List<PromotionGroupDetailVO> listGroupDetailByTranCode(String tranCode) {
    return promotionGroupDetailService.selectGroupDetail(tranCode);
  }

  /** 根据订单号查询拼团人数 */
  public List<PromotionGroupDetailVO> listGroupDetailByOrderNo(String orderNo) {
    return promotionGroupDetailService.selectGroupDetailByOrderNo(orderNo);
  }

  /** 查询订单的拼团详情 */
  public PromotionOrderDetail loadPieceDetailByOrderNo(String orderNo) {
    return promotionOrderDetailService.loadDetailByOrderNo(orderNo);
  }

  /** 查询拼团是否已满 */
  public boolean isPieceFull(PromotionOrderDetail orderDetail) throws PieceStateException {
    String tranCode = orderDetail.getPiece_group_tran_code();
    String orderNo = orderDetail.getSub_order_no();
    PGTranInfoVo tranInfoVo = this.loadPGTranInfo(tranCode);
    if (tranInfoVo == null) {
      throw new PieceStateException(String.format("订单 %s 开团信息异常", orderNo));
      // 不修改订单状态
    }
    // 下单选择的拼团详情
    String detailCode = orderDetail.getP_detail_code();
    PromotionPgDetail detail =
        recordPromotionPgDetailService.selectByCode(detailCode, tranInfoVo.getpCode());
    if (detail == null) {
      throw new PieceStateException(String.format("订单 %s 拼团详情数据异常", orderNo));
    }
    int currMemberCount = recordPromotionPgMemberInfoService.countLegalMember(tranCode);
    int maxMemberCount = detail.getPieceGroupNum();
    return currMemberCount >= maxMemberCount;
  }

  /** 更新团长状态 */
  public boolean updateTranInfoStatus(String tranCode, PieceStatus status) {
    return recordPromotionPgTranInfoService.updateStatusByTranCode(
        tranCode, status.getCode(), null);
  }

  /** 更新拼团成员状态 */
  public boolean updateMemberInfoStatus(String memberCode, PieceStatus status) {
    return recordPromotionPgMemberInfoService.updateStatusByMemberCode(
        memberCode, status.getCode());
  }

  /** 查询拼团总人数 */
  public int countPgMember(String tranCode) {
    return promotionPgMemberInfoService.selectPgMemberCount(tranCode);
  }

  /** 查询当前参团人数 */
  public int countCurrPgMember(String tranCode) {
    return promotionPgMemberInfoService.selectSuccessPromotionMemberCount(tranCode);
  }

  /** 同步更新整个团中所有人的状态 */
  @Transactional(rollbackFor = Exception.class)
  public boolean batchUpdateTranAndMember(
          String tranCode, PieceStatus status, PieceStatus... orgiStatus) {
    int code = status.getCode();
    List<Integer> list = Optional.ofNullable(orgiStatus).map(Arrays::asList)
            .orElse(Collections.emptyList())
            .stream()
            .map(PieceStatus::getCode).collect(Collectors.toList());
    boolean tranRet =
            recordPromotionPgTranInfoService.updateStatusByTranCode(tranCode, code, list);
    boolean memberRet =
            recordPromotionPgMemberInfoService.updateStatusByTranCode(tranCode, code, list);
    logger.info("更新库存结果" + tranCode + "," + code + "," + list.toString());
    logger.info("更新库存结果" + tranRet + memberRet);
    return tranRet && memberRet;
  }

  /**
   * 只更新团中某个人的状态
   */
  public boolean updateDetailMember(String detailCode, PieceStatus status,
                                    PieceStatus... orgiStatus) {
    int code = status.getCode();
    List<Integer> list = Optional.ofNullable(orgiStatus).map(Arrays::asList)
        .orElse(Collections.emptyList())
        .stream()
        .map(PieceStatus::getCode).collect(Collectors.toList());
    return recordPromotionPgMemberInfoService.updateStatusByTranCodeAndMemberId(detailCode, code, list);
  }

  /** 同步更新拼团成功状态以及订单状态 */
  public boolean batchUpdateGroupedTran(
      String tranCode, PieceStatus status, PieceStatus orgiStatus) {
    promotionOrderDetailService.batchUpdateTranOrderStatus(tranCode);
    return this.batchUpdateTranAndMember(tranCode, status, orgiStatus);
  }

  /**
   * 只按订单更新状态到已付款
   * @param orderNo
   */
  public boolean updatePgOrderToPaidByOrderNo(String orderNo) {
    return promotionOrderDetailService.batchUpdatePgOrderToPaidByOrderNo(orderNo);
  }

  /**
   * 参团时判断自己的库存是否充足
   */
  public boolean isDetailPieceStockEnough(String pCode, String detailCode, String orderId, String tranCode) {
    int detailAmt = promotionOrderDetailService.selectTrnDetailAmount(detailCode, orderId);
    String skuId = getSkuId(orderId);
    if (StringUtils.isBlank(skuId)) {
      logger.error("商品信息不存在，skuId {}" + skuId);
      return false;
    }
    Sku sku = skuMapper.selectByPrimaryKey(skuId);
    String skuCode = sku.getSkuCode();
    if (StringUtils.isBlank(skuCode)) {
      logger.error("商品信息不存在，skuCode {}" + skuCode);
      return false;
    }
    int restAmount = promotionTranInfoService.selectRestStockNumBySkuCode(skuCode, pCode);
    return restAmount >= detailAmt;
  }

  /** 判断拼团库存是否充足 */
  public boolean isTranPieceStockEnough(String tranCode, String orderId, String pCode) {
    // 当前团所有已经支付的订单需要扣减的库存量
    Map<String, Integer> skuIdAmount = promotionOrderDetailService.sumTranTotalAmount(tranCode, orderId);
    String skuId = getSkuId(orderId);
    if (StringUtils.isBlank(skuId)) {
      logger.error("商品信息不存在，skuId {}" + skuId);
      return false;
    }
    Sku sku = skuMapper.selectByPrimaryKey(skuId);
    String skuCode = sku.getSkuCode();
    if (StringUtils.isBlank(skuCode)) {
      logger.error("商品信息不存在，skuCode {}" + skuCode);
      return false;
    }
    Integer tranAmount = Optional.ofNullable(skuIdAmount.get(skuId)).orElse(0);
    int restAmount = promotionTranInfoService.selectRestStockNumBySkuCode(skuCode, pCode);
    return restAmount >= tranAmount;
  }

  /**
   * 只修改detail的库存
   */
  public boolean modifyDetailPieceStock(String tranCode, String pCode, String detailCode, String orderId) {
    return modifyPieceStock(tranCode, getSkuId(orderId), pCode,
        () -> promotionOrderDetailService.selectTrnDetailAmount(detailCode, orderId));
  }

  /**
   * 修改整个团的库存
   */
  public boolean modifyFullPieceStock(String tranCode, String orderId, String pCode) {
    //所有规格的需要扣除的库存
    Map<String, Integer> skuIdAmount = promotionOrderDetailService.sumTranTotalAmount(tranCode, orderId);
    for (Map.Entry<String, Integer> entry : skuIdAmount.entrySet()) {
      String skuId = entry.getKey();//需要扣减库存的skuId
      Integer amount = entry.getValue();//需要扣减的库存
      //调用修改库存方法
      boolean b = modifyPieceStock(tranCode, skuId, pCode, () -> amount);
      if (!b) {//只要有更新库存失败的就返回false
        return Boolean.FALSE;
      }
    }
    return Boolean.TRUE;
  }

  private boolean modifyPieceStock(String tranCode, String skuId, String pCode, Supplier<Integer> amtSupplier) {
    // 待扣减库存
    int amt = amtSupplier.get();
    if (StringUtils.isBlank(skuId)) {
      logger.error("商品信息不存在，skuId {}" + skuId);
      return false;
    }
    Sku sku = skuMapper.selectByPrimaryKey(skuId);
    String skuCode = sku.getSkuCode();
    if (StringUtils.isBlank(skuCode)) {
      logger.error("商品信息不存在，skuCode {}" + skuCode);
      return false;
    }
    int restAmount = promotionTranInfoService.selectRestStockNumBySkuCode(skuCode, pCode);
    // 再次检查库存, 如果库存不足则返回
    int amount = restAmount - amt;
    logger.info("团 {} 修改库存 {} -> {}", tranCode, restAmount, amount);
    if (amt > restAmount) {
      // TODO 处理逻辑
      return false;
    }
    return this.updatePieceAmount(skuCode, amt);
  }

  private String getSkuId(String orderId) {
    List<OrderItem> orderItems = orderItemMapper.selectByOrderId(orderId);
    if (orderItems == null || orderItems.isEmpty()) {
      logger.error("拼团信息不存在，tranCode {}" + orderId);
      return null;
    }
    OrderItem orderItem = orderItems.get(0);
    return orderItem.getSkuId();
  }

  private String getSkuCode(String tranCode) {
    PGTranInfoVo pgTranInfoVo = recordPromotionPgTranInfoService.loadPGTranInfo(tranCode);
    if (pgTranInfoVo == null) {
      logger.error("拼团信息不存在，tranCode {}" + tranCode);
      return null;
    }
    String code = pgTranInfoVo.getGroupSkuCode();
    Sku sku = skuMapper.selectByCode(code);
    if (sku == null) {
      logger.error("商品信息不存在，code {}" + code);
      return null;
    }
    return sku.getSkuCode();
  }

  public boolean updatePieceAmount(String skuCode, Integer amount) {
    int rows = tempStockMapper.updateStockNumByPCode(skuCode, amount);
    if (rows > 1) {
      logger.warn("pCode {} 对应多个skuCode, 请检查后台配置", skuCode);
    }
    logger.info("更新库存" + skuCode + "结果：" + rows);
    return rows > 0;
  }

  /** 查询是否有未付款订单拼团订单 */
  public boolean hasUnPaidOrder(String tranCode, String userId) {
    return promotionOrderDetailService.hasUnPaidOrder(tranCode, userId);
  }

  public Integer selectPromotionPriceLimit(String pDetailCode) {
    return promotionGoodsDetailService.selectPromotionPriceLimit(pDetailCode);
  }

  /**
   * 查询已参团购买件数
   */
  public Integer selectBuyAmounts(String pCode, String userId) {
    return promotionBaseInfoService.selectBuyAmounts(pCode, userId);
  }

  /** 查询参团限购 */
  public Integer selectBuyLimits(String pCode) {
    return promotionBaseInfoService.selectBuyLimits(pCode);
  }

  /** 保存活动订单详情 */
  public void saveOrderDetail(PromotionOrderDetail detail) {
    promotionOrderDetailMapper.insert(detail);
  }

  /** 查询某个团的拼主订单编号 */
  public String selectOrderNoByTranCode(String tranCode) {
    return recordPromotionPgMemberInfoService.selectOrderNoByTranCode(tranCode);
  }

  /** 查询某个团的拼主cpId */
  public String selectGroupHeadMemberId(String tranCode) {
    return recordPromotionPgMemberInfoService.selectGroupHeadMemberId(tranCode);
  }

  /**
   * 判断某个团是否还允许插队拼团
   *
   * @param pCode 拼团code
   * @param finishTimeSupp 获取成团时间
   * @return 允许或不允许
   */
  public boolean isInCutLineTime(String pCode,
      Supplier<Date> finishTimeSupp) {
    Date finishTime = finishTimeSupp.get();
    if (finishTime == null) {
      // 取不到成团时间，则不认为是插队拼团
      return false;
    }
    PromotionPg pg =
        Optional.ofNullable(promotionPgService.selectPromotionPgBypCode(pCode))
            .orElseThrow(BizException.ofSupplier(GlobalErrorCode.INVALID_ARGUMENT, "活动不存在"));
    return pg.isInCutLine(finishTimeSupp.get());
  }

  public boolean isInCutLineTimeByTran(String tranCode) {
    PromotionTranInfoVO tranInfoVO =
        Optional.ofNullable(promotionTranInfoService.selectPromotionPgTranCode(tranCode))
            .orElseThrow(BizException.ofSupplier(GlobalErrorCode.INVALID_ARGUMENT, "拼团活动不存在"));
    return isInCutLineTime(tranInfoVO.getpCode(), tranInfoVO::getGroupFinishTime);
  }

  /**
   * 根据pDetailCode查询价格体系
   *
   * @param pDetailCode pDetailCode
   * @return PromotionPgPrice
   */
  public PromotionPgPrice selectPromotionPgPriceByDetailCode(String pDetailCode) {
    return promotionPgPriceService.selectPromotionPgPriceByDetailCode(pDetailCode);
  }

  /**
   * 根据活动id和skuId查询价格体系
   * @param promotionId 活动id
   * @param sku skuId
   * @param type 活动类型
   * @return 价格体系
   */
  public PromotionPgPrice selectPromotionPgPriceByPromotionIdAndSkuId(String promotionId, Sku sku, PromotionType type) {
    return promotionPgPriceService.loadByPromotionIdAndSkuId(promotionId, sku, type);
  }

  /**
   * 根据pDetailCode查询拼主价配置
   *
   * @param pDetailCode pDetailCode
   * @return PromotionPgMasterPrice
   */
  public PromotionPgMasterPrice selectPromotionPgMasterPriceByDetailCode(String pDetailCode) {
    return promotionPgMasterPriceService.selectPromotionPgMasterPriceByDetailCode(pDetailCode);
  }

  /**
   * 拼团价格 拼主价、次数获取
   *
   * @param detailCode pDetailCode
   * @param currCpId   currCpId
   * @param currCpId   currCpId
   * @return PiecePrice
   */
  public PiecePrice getPiecePrice(String detailCode, Long currCpId) {
    PromotionPgPrice promotionPgPrice = this.selectPromotionPgPriceByDetailCode(detailCode);
    if (promotionPgPrice == null) {
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "活动价格体系参数不能为空,detailCode:" + detailCode);
    }

    final BigDecimal promotionPrice = promotionPgPrice.getPromotionPrice();
    if (promotionPrice.signum() < 0) {
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "拼团价不能为负数,promotionPrice:" + promotionPrice);
    }

    final PromotionPgMasterPrice promotionPgMasterPrice =
            this.selectPromotionPgMasterPriceByDetailCode(detailCode);

    PiecePrice piecePrice = new PiecePrice();

    if (promotionPgMasterPrice == null) {
      piecePrice.setPromotionPgPrice(promotionPgPrice);
      return piecePrice;
    }

    BigDecimal oPenMemberPrice;
    Integer priceLimit;
    final Integer unifiedSetup = promotionPgPrice.getUnifiedSetup();

    switch (unifiedSetup) {
      case NOTSETUP: //没有设置拼主价
        piecePrice.setPromotionPgPrice(promotionPgPrice);
        return piecePrice;
      case UNIFIEDSETUP: //统一设置拼主价
        BigDecimal memberPrice = promotionPgMasterPrice.getMemberPrice();
        Integer frequency = promotionPgMasterPrice.getFrequency();
        if (memberPrice != null && frequency >= 0) {
          oPenMemberPrice = memberPrice;
          priceLimit = frequency;
          piecePrice.setMemberPrice(oPenMemberPrice);
          piecePrice.setLimit(priceLimit);
        } else {
          throw new BizException(GlobalErrorCode.ERROR_PARAM, "非法参数{" + memberPrice + "," + frequency + "}");
        }
        break;
        //分别设置拼主价
      case PARTSETUP:
        final BigDecimal pureMemberPrice = promotionPgMasterPrice.getPureMemberPrice();
        final BigDecimal whiteMemberPrice = promotionPgMasterPrice.getWhiteMemberPrice();
        final BigDecimal vipMemberPrice = promotionPgMasterPrice.getVipMemberPrice();
        final BigDecimal merchantMemberPrice = promotionPgMasterPrice.getMerchantMemberPrice();
        final Integer pureFrequency = promotionPgMasterPrice.getPureFrequency();
        final Integer whiteFrequency = promotionPgMasterPrice.getWhiteFrequency();
        final Integer vipFrequency = promotionPgMasterPrice.getVipFrequency();
        final Integer merchantFrequency = promotionPgMasterPrice.getMerchantFrequency();

        if (customerProfileService.isNew(currCpId.toString())
                && pureMemberPrice != null && pureFrequency >= 0) {
          //纯白人
          oPenMemberPrice = pureMemberPrice;
          priceLimit = pureFrequency;
          piecePrice.setMemberPrice(oPenMemberPrice);
          piecePrice.setLimit(priceLimit);
        }
        if (customerProfileService.getUplineWeekIgnoreIdentity(currCpId) != null
                && whiteMemberPrice != null && whiteFrequency >= 0) {
          //白人
          oPenMemberPrice = whiteMemberPrice;
          priceLimit = whiteFrequency;
          piecePrice.setMemberPrice(oPenMemberPrice);
          piecePrice.setLimit(priceLimit);
        }
        if (customerProfileService.isVip(currCpId)
                && vipMemberPrice != null && vipFrequency >= 0) {
          //VIP
          oPenMemberPrice = vipMemberPrice;
          priceLimit = vipFrequency;
          piecePrice.setMemberPrice(oPenMemberPrice);
          piecePrice.setLimit(priceLimit);
        }
        if (customerProfileService.isSp(currCpId)
                && merchantMemberPrice != null && merchantFrequency >= 0) {
          //SP
          oPenMemberPrice = merchantMemberPrice;
          priceLimit = merchantFrequency;
          piecePrice.setMemberPrice(oPenMemberPrice);
          piecePrice.setLimit(priceLimit);
        }
        break;
      default:
        throw new BizException(GlobalErrorCode.ERROR_PARAM, "非法的拼主价类型,UnifiedSetup:" + unifiedSetup);
    }
    return piecePrice;
  }

  public List<PromotionPgPrice> listByPromotionIdAndProductId(String promotionId, String productId, PromotionType promotionType) {
    return promotionPgPriceService.listByPromotionId(promotionId, productId, promotionType);
  }

  public List<PromotionTempStock> listTempStock(String pCode) {
    return tempStockMapper.listTempStockByPCode(pCode);
  }

}
