package com.xquark.service.pricing.impl.pricing.impl;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.CouponView;
import com.xquark.service.fullReduce.fullCut.PromotionFullCutService;
import com.xquark.service.pricing.impl.pricing.FullCutViewPromotion;
import com.xquark.service.pricing.impl.pricing.PricingChainParams;
import com.xquark.service.user.UserService;

import java.math.BigDecimal;

import static com.xquark.dal.type.PromotionType.FULLCUT;
import static com.xquark.dal.type.PromotionUserScope.ALL;

/**
 * Created by wangxinhua on 17-12-1. DESC: 活动优惠相关折扣计算链 计算逻辑在父类中实现
 */
public class FullCutDiscountPricingChain extends PromotionDiscountPricingChain {

  private final PromotionFullCutService fullCutService;

  protected FullCutDiscountPricingChain(UserService userService, PgPromotionServiceAdapter pgPromotionServiceAdapter, PromotionFullCutService fullCutService) {
    super(userService, pgPromotionServiceAdapter);
    this.fullCutService = fullCutService;
  }


  @Override
  protected PromotionType calculateScope() {
    return FULLCUT;
  }

  @Override
  protected Promotion loadUseAblePromotion(String productId,
      PricingChainParams params) {
    return fullCutService.selectCanUsePromotion(productId, ALL);
  }

  @Override
  protected CouponView buildCouponView(String promotionId, BigDecimal totalDiscount,
      boolean freeDelivery) {
    return new FullCutViewPromotion(totalDiscount, freeDelivery);
  }
}
