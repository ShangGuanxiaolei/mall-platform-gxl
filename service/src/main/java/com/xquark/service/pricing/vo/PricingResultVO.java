package com.xquark.service.pricing.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Maps;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.type.LogisticDiscountType;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.CouponView;
import com.xquark.dal.vo.UserCouponVo;
import com.xquark.dal.voex.DiscountDetailVO;
import com.xquark.helper.Functions;
import com.xquark.helper.Transformer;
import com.xquark.service.pricing.CouponVO;
import com.xquark.service.pricing.impl.pricing.DiscountPricingResult;
import com.xquark.service.yundou.PointCommDeductionResult;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.MapUtils;

/**
 * @author ahlon
 */
public class PricingResultVO {

  /**
   * 商品总金额
   */
  private BigDecimal goodsFee;
  /**
   * 物流费用
   */
  private BigDecimal logisticsFee;

  private BigDecimal logisticsDiscount;

  private LogisticDiscountType logisticDiscountType;
  /**
   * 折扣金额
   */
  private BigDecimal discountFee;

  private BigDecimal couponDiscountFee;
  /**
   * 买家应该支付金额  = goodsFee +
   */
  private BigDecimal totalFee;

  /**
   * 包含积分信息的应支付金额
   */
  private Long totalYundouFee;

  /**
   * 订单总佣金
   */
  private BigDecimal commissionFee;

  /**
   * 佣金详情
   */
  private Map<String, BigDecimal> commissionDetails;

  /**
   * 参加了何种优惠活动
   */
  private Promotion usingPromotion;

  private CouponVO coupon;

  /**
   * 店铺可用优惠券，按价格倒序排列
   */
  @JsonIgnore
  private List<UserCouponVo> coupons;

  private List<? extends CouponView> couponViews;

  /**
   * 下单所使用的店铺优惠券
   */
  private UserCouponVo confirmedShopCoupon;

  private PointCommDeductionResult pointRet;

  private Map<PromotionType, BigDecimal> discountDetail = new LinkedHashMap<>();

  // 所有优惠相关的计算结果
  @JsonIgnore
  private DiscountPricingResult discountPricingResult;

  private boolean isPayRest;

  /**
   * 是否免单
   */
  private boolean isPayFree;

  /**
   * 前端显示的运费
   */
  private BigDecimal logistics = BigDecimal.ZERO;

  /**
   * 积分实际抵扣的邮费
   */
  private BigDecimal commissionLogisticsDiscount = BigDecimal.ZERO;

  public static PricingResultVO getZeroPricingResult() {
    PricingResultVO r = new PricingResultVO();
    r.setGoodsFee(new BigDecimal(0));
    r.setLogisticsFee(new BigDecimal(0));
    r.setLogisticsDiscount(new BigDecimal(0));
    r.setDiscountFee(new BigDecimal(0));
    r.setTotalFee(new BigDecimal(0));
    r.setCommissionFee(new BigDecimal(0));
    r.setCommissionDetails(new HashMap<String, BigDecimal>());
    r.setTotalYundouFee(0L);
    return r;
  }

  public BigDecimal getGoodsFee() {
    return goodsFee;
  }

  public BigDecimal getLogistics() {
    return logistics;
  }

  public void setLogistics(BigDecimal logistics) {
    this.logistics = logistics;
  }

  public void setGoodsFee(BigDecimal goodsFee) {
    this.goodsFee = goodsFee;
  }

  public BigDecimal getCommissionLogisticsDiscount() {
    return commissionLogisticsDiscount;
  }

  public void setCommissionLogisticsDiscount(BigDecimal commissionLogisticsDiscount) {
    this.commissionLogisticsDiscount = commissionLogisticsDiscount;
  }

  public BigDecimal getLogisticsFee() {
    return logisticsFee;
  }

  public BigDecimal getLogisticsDiscount() {
    return logisticsDiscount;
  }

  public void setLogisticsDiscount(BigDecimal logisticsDiscount) {
    this.logisticsDiscount = logisticsDiscount;
  }

  public void setLogisticsFee(BigDecimal logisticsFee) {
    this.logisticsFee = logisticsFee;
  }

  public BigDecimal getDiscountFee() {
    return discountFee;
  }

  public void setDiscountFee(BigDecimal discountFee) {
    this.discountFee = discountFee;
  }

  public BigDecimal getCouponDiscountFee() {
    return couponDiscountFee;
  }

  public void setCouponDiscountFee(BigDecimal couponDiscountFee) {
    this.couponDiscountFee = couponDiscountFee;
  }

  public BigDecimal getTotalFee() {
    return totalFee;
  }

  public void setTotalFee(BigDecimal totalFee) {
    this.totalFee = totalFee;
  }

  public Long getTotalYundouFee() {
    return totalYundouFee;
  }

  public void setTotalYundouFee(Long totalYundouFee) {
    this.totalYundouFee = totalYundouFee;
  }

  public BigDecimal getCommissionFee() {
    return commissionFee;
  }

  public void setCommissionFee(BigDecimal commissionFee) {
    this.commissionFee = commissionFee;
  }

  public Map<String, BigDecimal> getCommissionDetails() {
    return commissionDetails;
  }

  public void setCommissionDetails(Map<String, BigDecimal> commissionDetails) {
    this.commissionDetails = commissionDetails;
  }

  public CouponVO getCoupon() {
    return coupon;
  }

  public void setCoupon(CouponVO coupon) {
    this.coupon = coupon;
  }

  public List<UserCouponVo> getCoupons() {
    return coupons;
  }

  public void setCoupons(List<UserCouponVo> coupons) {
    this.coupons = coupons;
  }

  public UserCouponVo getConfirmedShopCoupon() {
    return confirmedShopCoupon;
  }

  public void setConfirmedShopCoupon(UserCouponVo confirmedShopCoupon) {
    this.confirmedShopCoupon = confirmedShopCoupon;
  }

  public PointCommDeductionResult getPointRet() {
    return pointRet;
  }

  public void setPointRet(PointCommDeductionResult pointRet) {
    this.pointRet = pointRet;
  }

  public Promotion getUsingPromotion() {
    return usingPromotion;
  }

  public void setUsingPromotion(Promotion usingPromotion) {
    this.usingPromotion = usingPromotion;
  }

  public boolean getIsPayRest() {
    return isPayRest;
  }

  public void setIsPayRest(boolean payRest) {
    isPayRest = payRest;
  }

  public List<? extends CouponView> getCouponViews() {
    return couponViews;
  }

  public void setCouponViews(List<? extends CouponView> couponViews) {
    this.couponViews = couponViews;
  }

  public DiscountPricingResult getDiscountPricingResult() {
    return discountPricingResult;
  }

  public void setDiscountPricingResult(
      DiscountPricingResult discountPricingResult) {
    this.discountPricingResult = discountPricingResult;
  }

  public LogisticDiscountType getLogisticDiscountType() {
    return logisticDiscountType;
  }

  public void setLogisticDiscountType(LogisticDiscountType logisticDiscountType) {
    this.logisticDiscountType = logisticDiscountType;
  }

  public Map<PromotionType, BigDecimal> getDiscountDetail() {
    return discountDetail;
  }

  public void setDiscountDetail(
      Map<PromotionType, BigDecimal> discountDetail) {
    this.discountDetail = discountDetail;
  }

  /**
   * 获取折扣优惠
   */
  public List<DiscountDetailVO> getDiscountDetailStr() {
    Map<PromotionType, BigDecimal> detail = getDiscountDetail();
    if (MapUtils.isEmpty(detail)) {
      return Collections.emptyList();
    }
    // 先过滤
    Map<PromotionType, BigDecimal> filteredRet = new HashMap<>(Maps
        .filterEntries(detail, new DiscountDetailPredicate()));
    // 转换为对象
    List<DiscountDetailVO> ret = Transformer.fromIterable(filteredRet.entrySet(),
        Functions.newEntryToObjFunction(DiscountDetailVO.class));
    Collections.sort(ret);
    return ret;
  }

  public boolean getIsPayFree() {
    return isPayFree;
  }

  public void setIsPayFree(boolean payFree) {
    isPayFree = payFree;
  }
}
