package com.xquark.service.pricing;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.xquark.dal.model.Promotion;

public interface DiscountService {

  BigDecimal calculate(Map<String, Integer> skuMap, String promotionId);

  List<Promotion> listAvailablePromotions(Map<String, Integer> skuMap);


}
