package com.xquark.service.pricing.vo;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ahlon
 */
@Deprecated
public class CartPricingResultVO extends PricingResultVO {

  private Map<String, PricingResultVO> pricesMap;

  public CartPricingResultVO() {
    super();

    this.setGoodsFee(new BigDecimal(0));
    this.setLogisticsFee(new BigDecimal(0));
    this.setDiscountFee(new BigDecimal(0));
    this.setTotalFee(new BigDecimal(0));
    this.setCommissionFee(new BigDecimal(0));
    this.setCommissionDetails(new HashMap<String, BigDecimal>());

    pricesMap = new HashMap<String, PricingResultVO>();
  }

  public Map<String, PricingResultVO> getPricesMap() {
    return pricesMap;
  }

  public void setPricesMap(Map<String, PricingResultVO> pricesMap) {
    this.pricesMap = pricesMap;
  }

  public void putPrices(String shopId, PricingResultVO prices) {
    this.pricesMap.put(shopId, prices);
    this.setGoodsFee(this.getGoodsFee().add(prices.getGoodsFee()));
    this.setLogisticsFee(this.getLogisticsFee().add(prices.getLogisticsFee()));
    this.setDiscountFee(this.getDiscountFee().add(prices.getDiscountFee()));
    this.setTotalFee(this.getTotalFee().add(prices.getTotalFee()));
    this.setCommissionFee(this.getCommissionFee().add(prices.getCommissionFee()));
    this.getCommissionDetails().putAll(prices.getCommissionDetails());
  }
}
