package com.xquark.service.pricing.impl.pricing;

import com.xquark.dal.model.Promotion;

import java.math.BigDecimal;

import static com.xquark.dal.type.PromotionType.RESERVE;

/**
 * @author: yyc
 * @date: 19-4-18 下午11:09
 */
public class ReserveOrderPromotion extends Promotion {

  public ReserveOrderPromotion(String promotionId, BigDecimal discount) {
    setId(promotionId);
    setTitle("立即预约");
    setDiscount(discount);
    setType(RESERVE);
    setIsFreeDelivery(false);
  }
}
