package com.xquark.service.pricing.impl.pricing.impl;

import com.google.common.base.Strings;
import com.xquark.adapter.PgPromotionAdapter;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.model.mypiece.PGMemberInfoVo;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.dal.model.promotion.OrderSkuAmount;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.PieceStatus;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.CouponView;
import com.xquark.helper.Transformer;
import com.xquark.service.cantuan.PieceCacheService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pricing.base.PgPromotionInfo;
import com.xquark.service.pricing.base.PromotionInfo;
import com.xquark.service.pricing.impl.pricing.*;
import com.xquark.service.pricing.impl.pricing.hooks.PieceAfterHook;
import com.xquark.service.user.UserService;
import com.xquark.utils.UUIDGenerator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static com.xquark.dal.type.PromotionType.PIECE_GROUP;

/**
 * 拼团相关优惠计算链
 */
public class PieceGroupDiscountPricingChain extends PromotionDiscountPricingChain<PiecePromotion> {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(PieceGroupDiscountPricingChain.class);

  private final OrderMapper orderMapper;

  private final CustomerProfileService customerProfileService;

  private final PieceCacheService pieceCacheService;

  public PieceGroupDiscountPricingChain(UserService userService, PgPromotionServiceAdapter pgPromotionServiceAdapter, OrderMapper orderMapper, CustomerProfileService customerProfileService, PieceCacheService pieceCacheService) {
    super(userService, pgPromotionServiceAdapter);
    this.orderMapper = orderMapper;
    this.customerProfileService = customerProfileService;
    this.pieceCacheService = pieceCacheService;
  }

  @Override
  protected BigDecimal getRealPrice(
      PiecePromotion canUserPromotion, CartItemVO item, PricingChainParams params) {
    PgPromotionInfo pgPromotionInfo = extractPgPromotion(params);

    PromotionPgPrice promotionPgPrice = getPromotionPgPrice(canUserPromotion, params);
    boolean isNew = canUserPromotion.getIsNew();

    Long cpId = params.getBuyer().getCpId();

    String detailCode = pgPromotionInfo.getDetailCode();
    BigDecimal point = promotionPgPrice.getPoint();
    canUserPromotion.setPoint(point);
    canUserPromotion.setPointDiscount(false);

    // 判断是否有免单活动
    boolean isFreshPromotion = pgPromotionServiceAdapter.loadFreshPromotion(detailCode);
    BigDecimal ret = promotionPgPrice.getPromotionPrice();
    if (isNew && isFreshPromotion) {
      // 如果免单了则不计算德分和立减
      canUserPromotion.setIsPointUsed(false);
      canUserPromotion.setCutDownUsed(false);
      // 提交订单的情况下
      // 如果判断为是纯白人, 则同时确认了现在是参团
      final PricingMode pricingMode = params.getPricingMode();
      boolean isPayFreeUsed =
          pieceCacheService.isPayFreeUesd(
              pgPromotionInfo.getTranCode(), params.getBuyer().getCpId());
      if (!isPayFreeUsed) {
        // 纯白人下单要占用名额
        ret = BigDecimal.ZERO;
        if (pricingMode == PricingMode.CONFIRM) {
          // 设置免单提醒 并且把名额减掉
          pieceCacheService.lockPayFreeMember(pgPromotionInfo.getTranCode(), cpId);
          //设置是否免单字段为true
          canUserPromotion.setPayFree(true);
        }
      }
    }

    // TODO 拼团三期需要设置独立的价格体系，立减服务费等需要替换掉
    item.getDynamicPricing().setReduction(promotionPgPrice.getReduction());
    item.getDynamicPricing().setServerAmt(promotionPgPrice.getServerAmt());
    item.getDynamicPricing().setPromoAmt(promotionPgPrice.getPromoAmt());
    item.getDynamicPricing().setNetWorth(promotionPgPrice.getNetWorth());

    return ret;
  }

  @Override
  protected PiecePromotion loadUseAblePromotion(String productId, PricingChainParams params) {
    PromotionInfo promotionInfo = params.getPromotionInfo();
    User buyer = params.getBuyer();
    if (promotionInfo == null) {
      return null;
    }
    PgPromotionInfo pgPromotionInfo;
    try {
      pgPromotionInfo = (PgPromotionInfo) promotionInfo;
    } catch (ClassCastException e) {
      LOGGER.info(
          "{}:{} - 非拼团活动, 跳过执行", promotionInfo.getPromotionId(), promotionInfo.getPromotionType());
      return null;
    }
    String pCode = pgPromotionInfo.getPromotionId();
    PromotionBaseInfo baseInfo = pgPromotionServiceAdapter.loadBaseInfo(pCode);
    if (baseInfo == null) {
      return null;
    }
    boolean isNew = hasFreePay(buyer, pgPromotionInfo);
    return Transformer.from(baseInfo, new PgPromotionAdapter(null, isNew));
  }

  @Override
  protected CouponView buildCouponView(
      String promotionId, BigDecimal totalDiscount, boolean freeDelivery) {
    return new PieceOrderPromotion(promotionId, totalDiscount);
  }

  @Override
  protected PromotionType calculateScope() {
    return PIECE_GROUP;
  }

  /**
   * 校验拼团前置条件
   *
   * @param params 优惠参数
   * @param promotion 优惠活动对象
   * @param currItem currItem
   */
  @Override
  protected void preCheckExtra(
      PricingChainParams params, PiecePromotion promotion, CartItemVO currItem) {
    PgPromotionInfo pgPromotionInfo = extractPgPromotion(params);

    String tranCode = pgPromotionInfo.getTranCode();
    String detailCode = pgPromotionInfo.getDetailCode();
    String pCode = pgPromotionInfo.getPromotionId();
    CartItemVO cartItemVO = params.getCartItems().get(0);
    String skuCode = cartItemVO.getSku().getSkuCode();
    User buyer = params.getBuyer();
    if (StringUtils.isBlank(detailCode)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拼团参数不正确");
    }

    // 校验用户是否达到参团购买上限
    Integer limit = pgPromotionServiceAdapter.selectBuyLimits(pCode);
    if (StringUtils.isNotBlank(tranCode) && limit != null) {
      // 查询已参团购买件数
      int buyAmounts =
          Optional.ofNullable(pgPromotionServiceAdapter.selectBuyAmounts(pCode, buyer.getId()))
              .orElse(0);
      int thisAmounts = cartItemVO.getAmount();
      if (buyAmounts + thisAmounts > limit) {
        throw new BizException(GlobalErrorCode.PROMOTION_LIMIT, "您已达到参团购买上限");
      }
    }

    // 根据tranCode获取成员购买的商品数量集合
    int sum = 0; // 定义变量，存储当前团已经参团购买的总量
    List<OrderSkuAmount> orderSkuAmounts = pgPromotionServiceAdapter.loadOrderDetail(tranCode);
    for (OrderSkuAmount orderSkuAmount:
         orderSkuAmounts) {
      Long skuId = orderSkuAmount.getSkuId();
      Integer amount = orderSkuAmount.getAmount();
      Sku sku = currItem.getSku();
      if (sku == null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "sku规格信息为空");
      }
      //相同规格的订单商品数量累计
      if (IdTypeHandler.encode(Optional.ofNullable(skuId)
              .orElseThrow(BizException.ofSupplier(GlobalErrorCode.INTERNAL_ERROR, "skuId为空")))
              .equals(sku.getId())) {
        sum = sum + Optional.ofNullable(amount)
                .orElseThrow(BizException.ofSupplier(GlobalErrorCode.INTERNAL_ERROR, "订单商品数量信息错误"));
      }
    }

    // 当前购买数量+已参团成员购买数量（相同规格）
    Integer amount = currItem.getAmount() + sum;
    pgPromotionServiceAdapter.checkPgStatus(buyer.getCpId(), pCode, tranCode, amount, skuCode);
  }

  @Override
  protected void onItemFinish(
      PricingChainParams params,
      PricingContext context,
      DiscountPricingResult ret,
      CartItemVO cartItem,
      PiecePromotion usingPromotion) {
    // 走到这一步时可以确认当前执行的就是拼团链, 且参数、状态均正常, 可以构建参团信息
    super.onItemFinish(params, context, ret, cartItem, usingPromotion);
    // 获取拼团活动信息
    PgPromotionInfo pgPromotionInfo = extractPgPromotion(params);
    String tranCode = pgPromotionInfo.getTranCode();

    // 拼团代码
    boolean isGroupHead = StringUtils.isBlank(tranCode);
    // 约定开团不携带tranCode参数
    PGTranInfoVo tranInfoVo = loadOrBuildTranInfo(params, cartItem);
    if (tranInfoVo == null) {
      // not allowed
      throw new BizException(GlobalErrorCode.PIECE_ERROR, "拼团信息不存在");
    }

    // 构建成员信息, 与拼团信息关联
    PGMemberInfoVo memberInfoVo =
        buildMemberInfo(tranInfoVo, params.getBuyer(), isGroupHead, usingPromotion.getIsNew());

    PieceOrderExtra extra =
        new PieceOrderExtra(tranInfoVo, memberInfoVo, isGroupHead, pgPromotionInfo.getIsCutLine());
    ret.addAfterOrderHook(new PieceAfterHook(extra, pgPromotionServiceAdapter));
    // 将前面计算的是否新人免单返回给客户端
    ret.setIsPayFree(usingPromotion.isPayFree());
  }

  /**
   * 从参数中获取拼团活动对象
   */
  private PgPromotionInfo extractPgPromotion(PricingChainParams params) {
    PromotionInfo promotionInfo = params.getPromotionInfo();
    if (promotionInfo == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "拼团内部错误");
    }
    PgPromotionInfo pgPromotionInfo;
    try {
      pgPromotionInfo = (PgPromotionInfo) promotionInfo;
    } catch (ClassCastException e) {
      LOGGER.info(
          "{}:{} - 非拼团活动, 跳过执行", promotionInfo.getPromotionId(), promotionInfo.getPromotionType());
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "拼团内部错误");
    }
    return pgPromotionInfo;
  }

  /** 根据上下文参数及当前购物车对象 判断当前是开团还是参团, 构建或者查询当前的拼团信息 */
  private PGTranInfoVo loadOrBuildTranInfo(PricingChainParams params, CartItemVO cartItem) {
    PgPromotionInfo pgPromotionInfo = extractPgPromotion(params);
    String tranCode = pgPromotionInfo.getTranCode();
    User buyer = params.getBuyer();
    if (StringUtils.isBlank(tranCode)) {
      // master build
      return buildTranInfo(pgPromotionInfo, buyer, cartItem.getSku());
    } else {
      // member load
      return pgPromotionServiceAdapter.loadPGTranInfo(tranCode);
    }
  }

  /** 构建拼团信息 */
  private PGTranInfoVo buildTranInfo(PgPromotionInfo promotionInfo, User buyer, Sku sku) {
    assert promotionInfo != null && buyer != null && sku != null;
    Long cpId = buyer.getCpId();
    PGTranInfoVo ret = new PGTranInfoVo();
    // TODO 目前不做白人开团及短链接分享
    ret.setPieceGroupTranCode(UUIDGenerator.getUUID());

    // 二期,如是弱关系白人开团,获取上级
    Long upLineId = customerProfileService.getUplineWeak(buyer.getCpId());
    if (!customerProfileService.hasIdentity(buyer.getCpId()) && (upLineId != null)) {
      ret.setGroupHeadId(upLineId);
    } else {
      ret.setGroupHeadId(buyer.getCpId());
    }

    ret.setGroupOpenMemberId(cpId);
    ret.setGroupSkuCode(sku.getId());
    ret.setpCode(promotionInfo.getPromotionId());
    ret.setpDetailCode(promotionInfo.getDetailCode());
    ret.setShareLink("");
    // 已开团, 未支付 - 未成团
    ret.setPieceStatus(PieceStatus.SUBMITTED);
    return ret;
  }

  /** 构建拼团成员信息 */
  private PGMemberInfoVo buildMemberInfo(
      PGTranInfoVo tranInfo, User buyer, boolean isGroupHead, boolean isNew) {
    assert tranInfo != null && buyer != null;
    String tranCode = tranInfo.getPieceGroupTranCode();
    // TODO 一期不做短链接
    PGMemberInfoVo pgMemberInfoVo = new PGMemberInfoVo();
    // 拼团成员唯一标识
    pgMemberInfoVo.setPieceGroupDetailCode(UUIDGenerator.getUUID());
    // 拼团主信息关联
    pgMemberInfoVo.setPieceGroupTranCode(tranCode);
    pgMemberInfoVo.setIsGroupHead(isGroupHead ? 1 : 0);
    pgMemberInfoVo.setShareUrl("");
    pgMemberInfoVo.setStatus(PieceStatus.SUBMITTED);
    pgMemberInfoVo.setIsNew(isNew ? 1 : 0);
    pgMemberInfoVo.setMemberId(buyer.getCpId());
    return pgMemberInfoVo;
  }

  /** 判断是不是纯白人免单 */
  private boolean hasFreePay(User buyer, PgPromotionInfo info) {
    if (info == null) {
      return false;
    }
    // 如果是拼团直接返回
    String tranCode = info.getTranCode();
    if (StringUtils.isBlank(tranCode)) {
      return false;
    }
    // 检查是否有已经0元支付的团员
    // TODO 竞态条件, 可能会存在多个人同时判定为0元支付
    // 付款时再做检查
    // 订单取消的时候同样要归还0元支付的名额
    if (pieceCacheService.isPayFreeUesd(tranCode, buyer.getCpId())) {
      return false;
    }
    return !orderMapper.hasSucceedPay(buyer.getId());
  }

  @Override
  protected boolean skipCurrent(PricingChainParams params, PricingContext context) {
    PromotionInfo promotion = params.getPromotionInfo();
    return promotion == null || promotion.getPromotionType() != PromotionType.PIECE_GROUP;
  }

  protected PromotionPgPrice getPromotionPgPrice(
      Promotion canUserPromotion, PricingChainParams params) {
    final PgPromotionInfo pgPromotionInfo = extractPgPromotion(params);
    assert pgPromotionInfo != null;

    if (null == pgPromotionInfo.getDetailCode()) {
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "detailCode为空");
    }

    final String detailCode = pgPromotionInfo.getDetailCode();
    final String tranCode = pgPromotionInfo.getTranCode();

    PromotionPgPrice promotionPgPrice =
        pgPromotionServiceAdapter.selectPromotionPgPriceByDetailCode(detailCode);
    if (promotionPgPrice == null) {
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "活动价格体系参数不能为空,detailCode:" + detailCode);
    }

    final BigDecimal promotionPrice = promotionPgPrice.getPromotionPrice();
    if (promotionPrice.signum() < 0) {
      throw new BizException(
          GlobalErrorCode.ERROR_PARAM, "拼团价不能为负数,promotionPrice:" + promotionPrice);
    }

    final User user = getCurrentUser();
    if (user == null) {
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "获取用户信息失败");
    }
    if (user.getCpId() == null) {
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "用户cpId为空");
    }
    final Long currCpId = user.getCpId();

    PiecePrice piecePrice = pgPromotionServiceAdapter.getPiecePrice(detailCode, currCpId);

    if (piecePrice.getPromotionPgPrice() != null) {
      return piecePrice.getPromotionPgPrice();
    }

    BigDecimal oPenMemberPrice = piecePrice.getMemberPrice();
    Integer priceLimit = piecePrice.getLimit();

    if (isCanObtainOpenMemberPrice(
        detailCode, tranCode, oPenMemberPrice, params.getBuyer().getCpId(), priceLimit)) {
      // 拼主
      canUserPromotion.setIsPointUsed(false);
      canUserPromotion.setCutDownUsed(false);
      // 设置拼主价
      promotionPgPrice.setPromotionPrice(oPenMemberPrice);
      return promotionPgPrice;
    } else {
      // 拼团三期直接返回拼团活动配置的价格
      return promotionPgPrice;
    }
  }

  private User getCurrentUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof User) {
        User user = (User) principal;
        if (!user.isAnonymous()) {
          return user;
        }
      }
      if (!auth.getClass().getSimpleName().contains("Anonymous")) {
        logger.error("Unknown authentication encountered, ignore it. " + auth);
      }
    }
    throw new BizException(GlobalErrorCode.UNAUTHORIZED, "need login first.");
  }

  private boolean isCanObtainOpenMemberPrice(
      String detailCode, String tranCode, BigDecimal oPenMemberPrice, Long cpid, Integer limit) {
    return oPenMemberPrice != null
        && Strings.isNullOrEmpty(tranCode)
        && pieceCacheService.isCanObtainOpenMemberPrice(String.valueOf(cpid), detailCode, limit);
  }
}
