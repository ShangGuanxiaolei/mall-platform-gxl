package com.xquark.service.pricing.impl.pricing;

import static com.xquark.dal.type.PromotionType.FULLCUT;

import com.xquark.dal.model.Promotion;
import java.math.BigDecimal;

/**
 * Created by wangxinhua on 2018/4/18. DESC: 供订单确认页面展示的全场折扣活动
 */
public class FullCutViewPromotion extends Promotion {

  public FullCutViewPromotion(BigDecimal discount, boolean freeDelivery) {
    setId("");
    setTitle("会员专享优惠");
    setDiscount(discount);
    setType(FULLCUT);
    setIsFreeDelivery(freeDelivery);
  }


}
