package com.xquark.service.pricing;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.User;
import com.xquark.dal.model.CashierItem;
import com.xquark.dal.model.Coupon;
import com.xquark.dal.model.CouponActivity;

public interface CouponService {

  Coupon load(String id);

  CouponVO loadVO(String id);

  /**
   * 可用的优惠券
   */
  List<CouponVO> listValids(BigDecimal price);

  /**
   * openapi zzd
   */
  List<CouponVO> listValids(BigDecimal price, User user);

  /**
   * 处理返回前端的优惠列表
   */
  List<CouponVO> dealCoupons(Map<String, List<CouponVO>> couponMap);

  /**
   * 下单使用
   */
  Coupon use(String couponId, String payNo);

//    /**
//     * 取消订单恢复
//     * @param couponId
//     * @return
//     */
//    Coupon restore(String couponId);

  /**
   * 根据活动CODE，获得优惠活动对象
   */
  CouponActivity loadByActCode(String actCode);

  CouponVO loadByCouponCode(String actCode, String couponCode);

  /**
   * 发放优惠券 此接口只留给想去调用，第三方的优惠券只能调用grantExtCoupon方法
   */
  boolean grantCoupon(Coupon coupon, String userId);

  /**
   * 发放优惠券 此接口只留给想去调用，第三方的优惠券只能调用grantExtCoupon方法
   */
  Coupon grantCoupon(String actCode, String userId, String deviceId);

  Coupon grantExtCoupon(String actCode, String extCouponId,
      BigDecimal amount, String userId);


  /**
   * bos使用
   */
  List<Coupon> listCouponsByAdmin(Map<String, Object> params, Pageable page);

  Long countCouponsByAdmin(Map<String, Object> params);

  /**
   * 生成，目前只在bos里调用
   */
  int create(List<String> listCodes, Map<String, Object> params);

  List<Coupon> listByAdmin(Map<String, Object> params, Pageable pageable);

  List<CouponActivity> listCouponActivityByAdmin();

  Boolean delete(String[] ids);

  Coupon obtainCoupon(String code);

  Map<String, List<CouponVO>> listValidsWithExt(BigDecimal price, String orderNo, String channel);

  List<CashierItem> writeToCashier(List<Coupon> coupons, String bizNo,
      BigDecimal totalFee, String userId, String productId,
      String productName, String partner, String bizNos);

  Coupon lock(String couponId, String payNo);

  Coupon unLock(String couponId, String payNo);

  int autoCloseActivity();

  void autoGrantCoupon(String partner, String userId, String deviceId);

  CouponVO loadExtCouponByExtId(String extCouponId);

}
