package com.xquark.service.pricing.base;

import com.xquark.dal.type.PromotionType;

/**
 * @author wangxinhua
 * @since 18-11-1
 * 活动数据传输对象
 */
public abstract class PromotionInfo {

  private String promotionId;

  private PromotionType promotionType;

  private boolean isCutInLine;

  private String preOrderNo;

  public PromotionInfo() {
  }

  public PromotionInfo(String promotionId, PromotionType promotionType) {
    this.promotionId = promotionId;
    this.promotionType = promotionType;
  }

  public PromotionInfo(String promotionId, PromotionType promotionType,String preOrderNo) {
    this.promotionId = promotionId;
    this.promotionType = promotionType;
    this.preOrderNo = preOrderNo;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public PromotionType getPromotionType() {
    return promotionType;
  }

  public boolean getIsCutLine() {
    return isCutInLine;
  }

  public void setCutInLine(boolean cutInLine) {
    isCutInLine = cutInLine;
  }

  public String getPreOrderNo() {
    return preOrderNo;
  }

  public void setPreOrderNo(String preOrderNo) {
    this.preOrderNo = preOrderNo;
  }
}
