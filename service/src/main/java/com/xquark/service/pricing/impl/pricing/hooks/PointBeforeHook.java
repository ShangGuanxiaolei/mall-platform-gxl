package com.xquark.service.pricing.impl.pricing.hooks;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.service.pricing.impl.pricing.SkuDiscountDetail;
import java.math.BigDecimal;
import java.util.Map;

/**
 * @author wangxinhua.
 * @date 2018/12/18
 */
public class PointBeforeHook extends OrderItemHook {

  /**
   * 使用德分数
   */
  private final BigDecimal using;

  /**
   * 使用红包德分数
   */
  private final BigDecimal usingPacket;

  /**
   * 分摊德分数
   */
  private final Map<String, SkuDiscountDetail> devideDetail;

  /**
   * 分摊红包德分数
   */
  private final Map<String, SkuDiscountDetail> devidePacketDetail;

  public PointBeforeHook(BigDecimal using, BigDecimal usingPacket,
      Map<String, SkuDiscountDetail> devideDetail,
      Map<String, SkuDiscountDetail> devidePacketDetail) {
    this.using = using;
    this.usingPacket = usingPacket;
    this.devideDetail = devideDetail;
    this.devidePacketDetail = devidePacketDetail;
  }

  @Override
  protected void acceptOrder(Order order) {
    order.setPaidPoint(using);
    order.setPaidPointPacket(usingPacket);
  }

  @Override
  protected void acceptItem(Order order, OrderItem item) {
    SkuDiscountDetail pointDetailSku = devideDetail.get(item.getSkuId());
    SkuDiscountDetail pointPacketDetailSku = devidePacketDetail.get(item.getSkuId());
    if (pointDetailSku != null) {
      item.setPaidPoint(pointDetailSku.getDiscount());
    }
    if (pointPacketDetailSku != null) {
      item.setPaidPointPacket(pointPacketDetailSku.getDiscount());
    }
  }
}
