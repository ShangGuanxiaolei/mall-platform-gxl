package com.xquark.service.pricing.base;

/**
 * @author wangxinhua.
 * @date 2018/11/4
 * 表示拼团状态异常
 */
public class PieceStateException extends Exception {

  public PieceStateException(String message) {
    super(message);
  }

  public PieceStateException(String message, Throwable cause) {
    super(message, cause);
  }

}
