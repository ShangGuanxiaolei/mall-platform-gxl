package com.xquark.service.pricing.impl.pricing.impl;

import com.xquark.dal.model.*;
import com.xquark.dal.type.LogisticDiscountType;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.StadiumType;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.freshman.FreshManProductService;
import com.xquark.service.grandSale.GrandSaleStadiumService;
import com.xquark.service.pricing.impl.pricing.DiscountPricingChain;
import com.xquark.service.pricing.impl.pricing.DiscountPricingResult;
import com.xquark.service.pricing.impl.pricing.PricingChainParams;
import com.xquark.service.pricing.impl.pricing.PricingContext;
import com.xquark.service.shop.PostageRet;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.*;

/**
 * 邮费计算链
 */
public class LogisticsPricingChain extends DiscountPricingChain {

  private Logger logger = Logger.getLogger(LogisticsPricingChain.class);

  private ShopTreeService shopTreeService;

  private ShopService shopService;

  private FreshManProductService freshManProductService;

  private GrandSaleStadiumService grandSaleStadiumService;

  public LogisticsPricingChain(ShopTreeService shopTreeService, ShopService shopService, GrandSaleStadiumService grandSaleStadiumService, FreshManProductService freshManProductService){
    this.shopTreeService = shopTreeService;
    this.shopService = shopService;
    this.freshManProductService = freshManProductService;
    this.grandSaleStadiumService = grandSaleStadiumService;
  }

  @Override
  protected DiscountPricingResult calculatePrice(PricingChainParams params, PricingContext context) {
    DiscountPricingResult discountPricingResult = DiscountPricingResult.emptyResult();
    Shop shop = params.getShop();
    Map map = params.getMap();
    List<CartItemVO> cartItems = params.getCartItems();

    // 邮费
    BigDecimal logisticsFee = BigDecimal.ZERO;
    BigDecimal logisticsDiscount = BigDecimal.ZERO;
    LogisticDiscountType logisticDiscountType = null;
    List<Sku> skuList = new ArrayList<>();
    List<String> skuIds = new ArrayList<>();
    String rootShopId = shopTreeService.selectRootShopByShopId(shop.getId()).getRootShopId();
    Address address = (Address) map.get("address");
    Promotion promotion = null;
    boolean flag = false;
    boolean isFreePostage = false;

    //对包含新人专区商品的订单 设置新人专区邮费
    boolean isFreshManOrder = false ;

    for (CartItemVO cartItem : cartItems) {
      //对包含新人专区商品的订单 设置新人专区邮费
      List<FreshManProductVo> freshManProductVos = freshManProductService.selectByProductId(cartItem.getProductId());
      if(CollectionUtils.isNotEmpty(freshManProductVos)){
        isFreshManOrder= true;
      }
      skuIds.add(cartItem.getSku().getId());
      Sku sku = cartItem.getSku();
      sku.setItemAmount(cartItem.getAmount());
      skuList.add(sku);
      Promotion p = context.getCurrUsingPromotion(cartItem.getProductId());
      if (p != null){
        if (!flag)
          flag = p.getIsFreeDelivery();//活动是否包邮

        //查询当前订单是否包含专区
        if(promotion == null && PromotionType.SALEZONE.equals(p.getPromotionType()))
          promotion = p;
      }

      //处理分会场商品邮费逻辑
      if (sku.getPromotionType() != null) {
        String promotionType = sku.getPromotionType();
        StadiumType stadiumType = null;
        try {
          stadiumType = StadiumType.valueOf(sku.getPromotionType());
        } catch (Exception e) {
          logger.error("类型转换失败,promotionType{}" + sku.getPromotionType(), e);
        }
        StadiumType[] stadiumTypes = StadiumType.values();
        if (ArrayUtils.contains(stadiumTypes, stadiumType)) {
          GrandSaleStadium grandSaleStadium = grandSaleStadiumService.selectOneByType(promotionType);
          if (grandSaleStadium == null) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "分会场商品信息不存在");
          }
          isFreePostage = grandSaleStadium.getIsFreePostage();
        }
      }
    }
      //是否是折扣商品
      boolean isSaleOrder = false ;
      for (CartItemVO cartItem : cartItems) {
         int aBoolean = freshManProductService.countBySku(cartItem.getSku().getSkuCode(), cartItem.getProductId());
          //是促销商品设置免邮
          if (0!=aBoolean){
              isSaleOrder=true;
              break;
          }
      }

    //计算邮费
    if (!flag && CollectionUtils.isNotEmpty(skuIds) && address != null) {
      if (StringUtils.isNotBlank(rootShopId)) {
        PostageRet postageRet = shopService.getShopPostageByProduct(rootShopId,
                address.getZoneId(), skuList);
        if(promotionLogisticFee(context.getCurrPromotions().values(), postageRet)) {//用于设置计算
          logisticsFee = logisticsFee.add(postageRet.getLogisticsDiscount());
          logisticsDiscount = postageRet.getLogisticsDiscount();
        }else{
          logisticsFee = logisticsFee.add(postageRet.getLogisticsFee());
          logisticsDiscount = logisticsDiscount.add(postageRet.getLogisticsDiscount());
        }
        logisticDiscountType = postageRet.getLogisticDiscountType();
      }
    }

    /*
     * 活动类型为专区活动
     */
    if (promotion != null && PromotionType.SALEZONE.equals(promotion.getPromotionType())) {
      logisticsFee = Optional.ofNullable(promotion.getLogisticsFee()).orElse(BigDecimal.ZERO);
      logisticsDiscount = BigDecimal.ZERO;
      context.setLogistics(logisticsFee);//需要抵扣的邮费
      context.setLogisticsDiscount(BigDecimal.ZERO);
    }else{
      if (logisticsDiscount.signum() > 0) {
        context.setLogistics(BigDecimal.ZERO);
      } else {
        context.setLogistics(logisticsFee);//需要抵扣的邮费
      }
      context.setLogisticsDiscount(logisticsDiscount);
    }

    //分会场商品免邮
    if (isFreePostage) {
      BigDecimal bigDecimal = Optional.ofNullable(logisticsDiscount).orElse(BigDecimal.ZERO);
      logisticsFee = bigDecimal;
      logisticsDiscount = bigDecimal;
      context.setLogistics(BigDecimal.ZERO);
      context.setLogisticsDiscount(bigDecimal);
    }

    //对包含新人专区商品的订单 设置新人专区邮费
    if(isFreshManOrder){
      logisticsFee = freshManProductService.selectFreshmanLogisticsFee();
      context.setLogistics(logisticsFee);//需要抵扣的邮费
      context.setLogisticsDiscount(BigDecimal.ZERO);
    }
    //对促销商品设置邮费
      if(isSaleOrder){
          context.setLogistics(BigDecimal.ZERO);//需要抵扣的邮费
          context.setLogisticsDiscount(BigDecimal.ZERO);
      }

    context.setLogisticDiscountType(logisticDiscountType);

    Map<String ,Object> logisticsMap = new HashMap<>();
    logisticsMap.put("logisticsFee", logisticsFee);
    logisticsMap.put("logistics", logisticsFee);//用于前端显示的邮费
    logisticsMap.put("commissionLogisticsDiscount", BigDecimal.ZERO);
    logisticsMap.put("logisticsDiscount", logisticsDiscount);//前端显示的减免值
    logisticsMap.put("logisticDiscountType", logisticDiscountType);
    discountPricingResult.setLogisticsMap(logisticsMap);

    return discountPricingResult;
  }

  @Override
  protected PromotionType calculateScope() {
    return null;
  }

  /**
   * 封装的活动邮费减免判断
   * @param promotions 活动
   * @return 是否免邮
   */
  private boolean promotionLogisticFee(Collection<Promotion> promotions, PostageRet postageRet) {
    return promotions.stream().anyMatch(p -> {
      PromotionType promotionType = p.getPromotionType();
      return (PromotionType.isPiece(promotionType)
              || PromotionType.FLASHSALE.equals(promotionType))
              && postageRet.getLogisticsDiscount().compareTo(BigDecimal.ZERO) != 0;
    });
  }

}
