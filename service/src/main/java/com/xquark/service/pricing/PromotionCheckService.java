package com.xquark.service.pricing;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.status.PromotionStatus;
import com.xquark.service.product.vo.ProductVO;
import org.springframework.ui.Model;

/**
 * Created by wangxinhua on 17-7-27. DESC: 检查活动是否有效等
 */
public interface PromotionCheckService {

  /**
   * 检查商品是否在活动中，如果在活动中则添加相关活动信息
   *
   * @param productVO 商品vo
   * @param promotion 活动实例
   * @return 活动状态
   */
  PromotionStatus checkFlashSaleProduct(ProductVO productVO, Promotion promotion,
      String activityGrouponId,
      Model model);

}
