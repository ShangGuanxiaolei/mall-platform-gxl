package com.xquark.service.pricing.impl.pricing;

import com.xquark.dal.model.Promotion;

import java.math.BigDecimal;
import static com.xquark.dal.type.PromotionType.SALEZONE;

public class SaleZoneViewPromotion extends Promotion {
    public SaleZoneViewPromotion(String promotionId, BigDecimal discount) {
        setId(promotionId);
        setTitle("一元专区");
        setDiscount(discount);
        setType(SALEZONE);
        setIsFreeDelivery(false);
    }
}
