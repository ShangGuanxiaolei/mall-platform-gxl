package com.xquark.service.pricing.impl.pricing.impl;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.User;
import com.xquark.dal.type.PromotionType;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pricing.impl.pricing.*;
import com.xquark.utils.DynamicPricingUtil;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.xquark.dal.type.PromotionType.CUT_DOWN;

/**
 * created by
 *
 * @author wangxinhua at 18-6-26 下午1:48 立减优惠链
 */
public class CutDownDiscountPricingChain extends DiscountPricingChain {

  private final CustomerProfileService customerProfileService;

  public CutDownDiscountPricingChain(CustomerProfileService customerProfileService) {
    this.customerProfileService = customerProfileService;
  }

  @Override
  protected DiscountPricingResult calculatePrice(
      PricingChainParams params, PricingContext context) {
    BigDecimal discount = BigDecimal.ZERO;
    List<CartItemVO> cartItems = params.getCartItems();
    Map<String, SkuDiscountDetail> devidedMap = new HashMap<>();

    Long cpId = params.getBuyer().getCpId();

    for (CartItemVO cartItem : cartItems) {
      Promotion currPromotion = context.getCurrUsingPromotion(cartItem.getProductId());
      if (currPromotion != null) {
        if (!currPromotion.getIsCountEarning()) {
          continue;
        }
        if (currPromotion.getCutDownUsed() != null && !currPromotion.getCutDownUsed()) {
          continue;
        }
      }

      BigDecimal val = DynamicPricingUtil.calCutDown(cpId, cartItem);
      discount = discount.add(val);
      devidedMap.put(cartItem.getSku().getId(), new SkuDiscountDetail("", val, CUT_DOWN));
    }
    if (discount == null || discount.signum() == 0) {
      return null;
    }

    BigDecimal currFee = context.getCurrFee();
    BigDecimal discuntResult = currFee.subtract(discount);
    if (discuntResult.signum() < 0) {
      discount = currFee;
    }

    DiscountPricingResult ret = DiscountPricingResult.emptyResult();
    ret.addDiscount(discount, calculateScope(), context);
    ret.addExtraParam(DiscountPricingResult.DEVIDED_CUTDOWN_DETAIL_KEY, devidedMap);
    return ret;
  }

  @Override
  protected PromotionType calculateScope() {
    return CUT_DOWN;
  }

  /** 只有有身份的人可以享受立减优惠 */
  @Override
  protected boolean skipCurrent(PricingChainParams params, PricingContext context) {
    User buyer = params.getBuyer();
    Long cpId = buyer.getCpId();
    return !customerProfileService.hasIdentity(cpId);
  }
}
