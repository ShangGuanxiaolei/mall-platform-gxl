package com.xquark.service.pricing.impl.pricing.impl;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.CouponView;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.pricing.base.PromotionInfo;
import com.xquark.service.pricing.impl.pricing.*;
import com.xquark.service.pricing.impl.pricing.hooks.ReserveAfterHook;
import com.xquark.service.pricing.impl.pricing.hooks.ReserveBeforeHook;
import com.xquark.service.reserve.PromotionReserveService;
import com.xquark.service.user.UserService;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.List;

import static com.xquark.dal.type.PromotionType.RESERVE;

/**
 * 预售相关计算链
 *
 * @author: yyc
 * @date: 19-4-18 下午10:42
 */
public class ReserveDiscountPricingChain extends PromotionDiscountPricingChain {

  private final PromotionReserveService promotionReserveService;

  public ReserveDiscountPricingChain(UserService userService, PgPromotionServiceAdapter pgPromotionServiceAdapter, PromotionReserveService promotionReserveService) {
    super(userService, pgPromotionServiceAdapter);
    this.promotionReserveService = promotionReserveService;
  }

  @Override
  @SuppressWarnings("unchecked")
  protected void preCheckExtra(PricingChainParams params, Promotion promotion, CartItemVO currItem) {
    super.preCheckExtra(params, promotion, currItem);
    final PromotionInfo promotionInfo = params.getPromotionInfo();
    if(promotionInfo != null && params.getPricingMode() == PricingMode.SUBMIT){
      // 检查活动库存并更新
      promotionReserveService.checkPromotion(promotionInfo.getPreOrderNo());
    }
  }

  @Override
  protected Promotion loadUseAblePromotion(String productId, PricingChainParams params) {
    PromotionInfo promotionInfo = params.getPromotionInfo();
    if (promotionInfo == null) {
      return null;
    }

    String promotionId = promotionInfo.getPromotionId();
    String preOrderId = promotionInfo.getPreOrderNo();
    if (StringUtils.isBlank(promotionId) && StringUtils.isNotBlank(preOrderId)) {
      // 如果有预购单号则通过预购单号去恢复活动id
      promotionId = promotionReserveService.loadPromotionIdByPreOrderNo(preOrderId);
    }
    List<CartItemVO> cartItems = params.getCartItems();
    CartItemVO itemVO = cartItems.get(0);
    String skuId = itemVO.getSkuId();
    Promotion promotion = promotionReserveService.loadPromotion(promotionId, skuId);

    // todo something
    promotion.setIsCountEarning(true); // fixme 预售暂时设置默认值
    promotion.setIsPointUsed(true); // fixme 预售暂时设置默认值
    return promotion;
  }

  @Override
  protected BigDecimal getRealPrice(
      Promotion canUserPromotion, CartItemVO item, PricingChainParams params) {
    BigDecimal price = canUserPromotion.getPrice();
    if (price != null) {
      BigDecimal promotionPoint = canUserPromotion.getPoint();
      if (promotionPoint != null) {
        item.getSku().setDeductionDPoint(promotionPoint);
      }
    }
    return price == null ? item.getSku().getPrice() : price;
  }

  @Override
  protected boolean skipCurrent(PricingChainParams params, PricingContext context) {
    PromotionInfo promotion = params.getPromotionInfo();
    return promotion == null || promotion.getPromotionType() != PromotionType.RESERVE;
  }

  @Override
  protected CouponView buildCouponView(
      String promotionId, BigDecimal totalDiscount, boolean freeDelivery) {
    return new ReserveOrderPromotion(promotionId, totalDiscount);
  }

  @Override
  @SuppressWarnings("unchecked")
  protected void onItemFinish(PricingChainParams params, PricingContext context, DiscountPricingResult ret, CartItemVO cartItem, Promotion usingPromotion) {
    super.onItemFinish(params, context, ret, cartItem, usingPromotion);
    final PromotionInfo promotionInfo = params.getPromotionInfo();
    final String preOrderNo = promotionInfo.getPreOrderNo();
    // 在保存订单前将预售信息写入
    ret.addBeforeOrderHook(new ReserveBeforeHook(preOrderNo));
    ret.addAfterOrderHook(new ReserveAfterHook(preOrderNo, promotionReserveService));
  }

  @Override
  protected PromotionType calculateScope() {
    return RESERVE;
  }
}
