package com.xquark.service.pricing.vo;

import com.google.common.base.Preconditions;
import com.xquark.dal.model.Shop;
import com.xquark.service.cart.vo.CartItemVO;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;

/**
 * Created by dongsongjie on 15/11/25.
 */
public class UserSelectedProVO {

  private Map<Shop, String> selShopCouponMap;
  private Map<Shop, String> selShopPromotionMap;
  private Map<CartItemVO, String> selCartItemPromotionMap;
  private Map<CartItemVO, String> selCartItemCouponMap;

  // 是否来自特权码商品
  private String isPrivilege;

  public String getIsPrivilege() {
    return isPrivilege;
  }

  public void setIsPrivilege(String isPrivilege) {
    this.isPrivilege = isPrivilege;
  }

  public Map<Shop, String> getSelShopCouponMap() {
    return selShopCouponMap;
  }

  public void setSelShopCouponMap(Map<Shop, String> selShopCouponMap) {
    this.selShopCouponMap = selShopCouponMap;
  }

  public Map<Shop, String> getSelShopPromotionMap() {
    return selShopPromotionMap;
  }

  public void setSelShopPromotionMap(Map<Shop, String> selShopPromotionMap) {
    this.selShopPromotionMap = selShopPromotionMap;
  }

  public Map<CartItemVO, String> getSelCartItemPromotionMap() {
    return selCartItemPromotionMap;
  }

  public void setSelCartItemPromotionMap(Map<CartItemVO, String> selCartItemPromotionMap) {
    this.selCartItemPromotionMap = selCartItemPromotionMap;
  }

  public Map<CartItemVO, String> getSelCartItemCouponMap() {
    return selCartItemCouponMap;
  }

  public void setSelCartItemCouponMap(Map<CartItemVO, String> selCartItemCouponMap) {
    this.selCartItemCouponMap = selCartItemCouponMap;
  }

  public static UserSelectedProVO build(Shop shop, List<String> couponIds) {
    Preconditions.checkNotNull(shop, "店铺未初始化");
    if (CollectionUtils.isNotEmpty(couponIds)) {
      Map<Shop, String> shopCoupons = new HashMap<>();
      for (String couponId : couponIds) {
        shopCoupons.put(shop, couponId);
      }
      UserSelectedProVO userSelectedProVO = new UserSelectedProVO();
      userSelectedProVO.setSelShopCouponMap(shopCoupons);

      userSelectedProVO.setSelShopPromotionMap(new LinkedHashMap<Shop, String>());
      userSelectedProVO.setSelCartItemPromotionMap(new LinkedHashMap<CartItemVO, String>());
      userSelectedProVO.setSelCartItemCouponMap(new LinkedHashMap<CartItemVO, String>());

      return userSelectedProVO;
    }
    return null;
  }
}

