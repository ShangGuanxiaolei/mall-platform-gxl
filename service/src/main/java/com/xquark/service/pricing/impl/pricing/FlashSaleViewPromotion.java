package com.xquark.service.pricing.impl.pricing;

import static com.xquark.dal.type.PromotionType.FLASHSALE;

import com.xquark.dal.model.Promotion;
import java.math.BigDecimal;

/**
 * created by
 *
 * @author wangxinhua at 18-7-3 下午9:15
 */
public class FlashSaleViewPromotion extends Promotion {

  public FlashSaleViewPromotion(String promotionId, BigDecimal discount) {
    setId(promotionId);
    setTitle("限时抢购优惠");
    setDiscount(discount);
    setType(FLASHSALE);
    setIsFreeDelivery(false);
  }

}
