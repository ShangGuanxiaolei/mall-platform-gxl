package com.xquark.service.pricing.impl.pricing;

import com.google.common.base.Preconditions;
import com.xquark.dal.type.PromotionType;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.error.NonCheckBizException;
import org.apache.commons.collections4.MapUtils;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by wangxinhua on 17-11-30. DESC: 折扣计算链 在serviceConfig中配置计算链
 */
public abstract class DiscountPricingChain {

  private DiscountPricingChain nextChain;

  public DiscountPricingChain setNextChain(DiscountPricingChain nextChain) {
    this.nextChain = nextChain;
    return nextChain;
  }

  protected DiscountPricingChain getNextChain() {
    return nextChain;
  }

  /**
   * 调用 实现类的 {@code calculatePrice} 方法并累加各个链的结果
   * @param params 调用链中需要的所有参数的封装
   * @return {@link DiscountPricingResult} 折扣计算结果
   */
  @Transactional(noRollbackFor = NonCheckBizException.class)
  public DiscountPricingResult calculate(PricingChainParams params, PricingContext context) {
    List<CartItemVO> cartItems = params.getCartItems();
    checkNotNull(cartItems);
    Preconditions.checkState(cartItems.size() > 0, "cart item size is zero.");

    // 否则需要计算全部优惠并取最大值
    DiscountPricingResult result = DiscountPricingResult.emptyResult(params.isAutoSelect());
    if (!skipCurrent(params, context)) {
      result.merge(this.calculatePrice(params, context));
    }
    if (hasNext()) {
      DiscountPricingResult nextResult = getNextChain().calculate(params, context);
      result.merge(nextResult);
    }
    return result;
  }

  /**
   * 模板方法，由子类实现, 计算该链的折扣
   *
   * @param params 参数封装
   * @return 单个链的计算结果
   */
  protected abstract DiscountPricingResult calculatePrice(PricingChainParams params,
      PricingContext context);

  /**
   * 子类实现, 返回单个子类负责计算的优惠范围
   *
   * @return {@link PromotionType} 活动类型
   */
  protected abstract PromotionType calculateScope();

  /**
   * 判断该链后是否还需要继续执行
   *
   * @return true or false
   */
  protected boolean hasNext() {
    return getNextChain() != null;
  }

  /**
   * 判断当前节点是否跳过执行 默认不跳过，由子类实现
   *
   * @return true or false
   */
  protected boolean skipCurrent(PricingChainParams params,
      PricingContext context) {
    return false;
  }

  /**
   * 分摊总优惠到不同的sku
   *
   * @param detailPriceMap skuId - sku比重
   * @param using 实际使用的值
   * @param scala 保留小数位
   * @return skuId - sku对应的优惠
   */
  protected Map<String, SkuDiscountDetail> divideDiscount(String promotionId,
      Map<String, BigDecimal> detailPriceMap, BigDecimal using, int scala) {
    checkNotNull(using);
    if (MapUtils.isEmpty(detailPriceMap)) {
      return Collections.emptyMap();
    }
    BigDecimal total = BigDecimal.ZERO;
    for (BigDecimal val : detailPriceMap.values()) {
      total = total.add(val);
    }
    // 除了最后一个sku的折扣
    BigDecimal discountExpLast = BigDecimal.ZERO;
    Map<String, SkuDiscountDetail> ret = new LinkedHashMap<>();
    int size = detailPriceMap.size();
    int index = 1;
    for (Entry<String, BigDecimal> entry : detailPriceMap.entrySet()) {
      String skuId = entry.getKey();
      BigDecimal discount;
      if (index != size) {
        BigDecimal price = entry.getValue();
        // 单个商品占总价的比例
        BigDecimal percent = price.divide(total, 8, BigDecimal.ROUND_HALF_EVEN);
        // 折扣等于该比例乘以总折扣
        discount = percent.multiply(using).setScale(scala, BigDecimal.ROUND_HALF_EVEN);
        discountExpLast = discountExpLast.add(discount);
      } else {
        discount = using.subtract(discountExpLast);
      }
      index++;
      ret.put(skuId, new SkuDiscountDetail(promotionId, discount, calculateScope()));
    }
    return ret;
  }

}
