package com.xquark.service.pricing.vo;

import com.xquark.dal.model.CartItem;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.Shop;
import com.xquark.dal.vo.UserCouponVo;
import java.util.Map;

/**
 * Created by dongsongjie on 15/11/25.
 */
public class ConfirmOrderPromotionVO {

  private Map<Shop, Promotion> shopPromotion;

  private Map<CartItem, Promotion> cartItemPromotion;

  private Map<Shop, UserCouponVo> shopCoupon;

  private Map<CartItem, UserCouponVo> cartItemCoupon;

  private Map<Shop, PricingResultVO> shopPricingResult;

  private PricingResultVO totalPricingResult;

  public Map<Shop, Promotion> getShopPromotion() {
    return shopPromotion;
  }

  public void setShopPromotion(Map<Shop, Promotion> shopPromotion) {
    this.shopPromotion = shopPromotion;
  }

  public Map<CartItem, Promotion> getCartItemPromotion() {
    return cartItemPromotion;
  }

  public void setCartItemPromotion(Map<CartItem, Promotion> cartItemPromotion) {
    this.cartItemPromotion = cartItemPromotion;
  }

  public Map<Shop, UserCouponVo> getShopCoupon() {
    return shopCoupon;
  }

  public void setShopCoupon(Map<Shop, UserCouponVo> shopCoupon) {
    this.shopCoupon = shopCoupon;
  }

  public Map<CartItem, UserCouponVo> getCartItemCoupon() {
    return cartItemCoupon;
  }

  public void setCartItemCoupon(Map<CartItem, UserCouponVo> cartItemCoupon) {
    this.cartItemCoupon = cartItemCoupon;
  }

  public Map<Shop, PricingResultVO> getShopPricingResult() {
    return shopPricingResult;
  }

  public void setShopPricingResult(Map<Shop, PricingResultVO> shopPricingResult) {
    this.shopPricingResult = shopPricingResult;
  }

  public PricingResultVO getTotalPricingResult() {
    return totalPricingResult;
  }

  public void setTotalPricingResult(PricingResultVO totalPricingResult) {
    this.totalPricingResult = totalPricingResult;
  }
}
