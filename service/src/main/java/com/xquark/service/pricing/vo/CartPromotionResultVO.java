package com.xquark.service.pricing.vo;

import com.xquark.dal.model.CartItem;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.Shop;
import com.xquark.dal.vo.CouponView;
import com.xquark.dal.vo.UserCouponVo;
import java.util.List;
import java.util.Map;

/**
 * @author dongsongjie
 */
public class CartPromotionResultVO {

  private static final long serialVersionUID = 4485622304625973150L;

  private Map<Shop, List<Promotion>> shopPromotions;

  private Map<CartItem, List<Promotion>> cartItemPromotions;

  private Map<Shop, List<? extends CouponView>> shopCoupons;

  private Map<CartItem, List<UserCouponVo>> cartItemCoupons;

  private Map<Shop, PricingResultVO> shopPricingResult;

  private boolean canUseCoupon;

  private PricingResultVO totalPricingResult;

  public Map<Shop, List<Promotion>> getShopPromotions() {
    return shopPromotions;
  }

  public void setShopPromotions(Map<Shop, List<Promotion>> shopPromotions) {
    this.shopPromotions = shopPromotions;
  }

  public Map<CartItem, List<Promotion>> getCartItemPromotions() {
    return cartItemPromotions;
  }

  public void setCartItemPromotions(Map<CartItem, List<Promotion>> cartItemPromotions) {
    this.cartItemPromotions = cartItemPromotions;
  }

  public Map<Shop, List<? extends CouponView>> getShopCoupons() {
    return shopCoupons;
  }

  public void setShopCoupons(Map<Shop, List<? extends CouponView>> shopCoupons) {
    this.shopCoupons = shopCoupons;
  }

  public Map<CartItem, List<UserCouponVo>> getCartItemCoupons() {
    return cartItemCoupons;
  }

  public void setCartItemCoupons(Map<CartItem, List<UserCouponVo>> cartItemCoupons) {
    this.cartItemCoupons = cartItemCoupons;
  }

  public Map<Shop, PricingResultVO> getShopPricingResult() {
    return shopPricingResult;
  }

  public void setShopPricingResult(Map<Shop, PricingResultVO> shopPricingResult) {
    this.shopPricingResult = shopPricingResult;
  }

  public PricingResultVO getTotalPricingResult() {
    return totalPricingResult;
  }

  public void setTotalPricingResult(PricingResultVO totalPricingResult) {
    this.totalPricingResult = totalPricingResult;
  }

  public boolean getCanUseCoupon() {
    return canUseCoupon;
  }

  public void setCanUseCoupon(boolean canUseCoupon) {
    this.canUseCoupon = canUseCoupon;
  }
}
