package com.xquark.service.pricing.impl.pricing.hooks;

import com.xquark.dal.model.Order;
import com.xquark.service.pricing.impl.pricing.OrderUnit;

/**
 * 在保存订单前写入预约活动上下文钩子
 * @author wangxinhua
 * @date 2019-05-07
 * @since 1.0
 */
public class ReserveBeforeHook extends OrderHook {

    private final String perOrderNo;

    public ReserveBeforeHook(String perOrderNo) {
        this.perOrderNo = perOrderNo;
    }

    @Override
    public void accept(OrderUnit orderUnit) {
        // 写入预购单号
        for (Order o : orderUnit.getOrderMap().keySet()) {
            o.setPreOrderNo(perOrderNo);
        }
    }
}
