package com.xquark.service.pricing.impl.pricing.impl;

import com.xquark.dal.model.PromotionPgPrice;

import java.math.BigDecimal;

public class PiecePrice {

  private PromotionPgPrice promotionPgPrice;

  private BigDecimal memberPrice;

  private Integer limit = 0;

  public PromotionPgPrice getPromotionPgPrice() {
    return promotionPgPrice;
  }

  public void setPromotionPgPrice(PromotionPgPrice promotionPgPrice) {
    this.promotionPgPrice = promotionPgPrice;
  }

  public BigDecimal getMemberPrice() {
    return memberPrice;
  }

  public void setMemberPrice(BigDecimal memberPrice) {
    this.memberPrice = memberPrice;
  }

  public Integer getLimit() {
    return limit;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }
}
