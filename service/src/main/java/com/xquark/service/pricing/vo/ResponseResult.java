package com.xquark.service.pricing.vo;

/**
 * @auther liuwei
 * @date 2018/6/16 15:58
 */
import java.io.Serializable;
import org.neo4j.cypher.internal.compiler.v2_1.ast.In;

public class ResponseResult<T> implements Serializable {

  private String code;
  private String message;
  private Integer totalcount;
  private T goodslist;

  public T getGoodslist() {
    return goodslist;
  }

  public void setGoodslist(T goodslist) {
    this.goodslist = goodslist;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Integer getTotalcount() {
    return totalcount;
  }

  public void setTotalcount(Integer totalcount) {
    this.totalcount = totalcount;
  }

  @Override
  public String toString() {
    return "ResponseResult{" +
        "code='" + code + '\'' +
        ", message='" + message + '\'' +
        ", totalcount=" + totalcount +
        ", goodslist=" + goodslist +
        '}';
  }
}
