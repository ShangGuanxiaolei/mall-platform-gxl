package com.xquark.service.pricing.vo;

import com.xquark.dal.model.Product;
import com.xquark.dal.vo.PromotionFullCutVO;
import com.xquark.dal.vo.PromotionFullPiecesVO;
import com.xquark.dal.vo.PromotionFullReduceVO;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by wangxinhua on 17-9-25. DESC: 满减、满件优惠结果
 */
public class FullReducePricingResultVO implements Comparable<FullReducePricingResultVO> {

  // 优惠详情
  private final Map<PromotionFullReduceVO, BigDecimal> details;

  // 赠品
  private Set<Product> giftProducts;

  public static FullReducePricingResultVO initPricing(BigDecimal price, BigDecimal originalPrice,
      Integer amount) {
    return new FullReducePricingResultVO(price, originalPrice, amount);
  }

  public static FullReducePricingResultVO initPricing() {
    return new FullReducePricingResultVO(BigDecimal.ZERO, BigDecimal.ZERO, 0);
  }

  private FullReducePricingResultVO() {
    this.totalDiscountFee = BigDecimal.ZERO;
    this.details = new HashMap<PromotionFullReduceVO, BigDecimal>();
    this.giftProducts = new HashSet<Product>();
  }

  private FullReducePricingResultVO(BigDecimal price, BigDecimal originalPrice, Integer amount) {
    this();
    this.price = price;
    this.originalPrice = originalPrice;
    this.amount = amount;
  }

  private BigDecimal totalDiscountFee;

  private BigDecimal price;

  private BigDecimal originalPrice;

  private Integer amount;

  // 是否包邮
  private boolean freeDelivery;

  public boolean isValid() {
    return this.totalDiscountFee.compareTo(BigDecimal.ZERO) > 0;
  }

  public BigDecimal getTotalDiscountFee() {
    return totalDiscountFee.setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  public void setTotalDiscountFee(BigDecimal totalDiscountFee) {
    this.totalDiscountFee = totalDiscountFee;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getOriginalPrice() {
    return originalPrice;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setOriginalPrice(BigDecimal originalPrice) {
    this.originalPrice = originalPrice;
  }

  public boolean isFreeDelivery() {
    return freeDelivery;
  }

  public void setFreeDelivery(boolean freeDelivery) {
    this.freeDelivery = freeDelivery;
  }

  public Map<PromotionFullReduceVO, BigDecimal> getDetails() {
    return details;
  }

  public Set<Product> getGiftProducts() {
    return giftProducts;
  }

  public void setGiftProducts(Set<Product> giftProducts) {
    this.giftProducts = giftProducts;
  }

  public BigDecimal put(PromotionFullReduceVO reduceVO, BigDecimal value) {
    this.totalDiscountFee = this.totalDiscountFee.add(value);
    return this.details.put(reduceVO, value);
  }

  public BigDecimal remove(String id) {
    BigDecimal value = details.get(id);
    BigDecimal returnValue = null;
    if (value != null) {
      returnValue = details.remove(id);
      totalDiscountFee = totalDiscountFee.subtract(value);
    }
    return returnValue;
  }

  public BigDecimal clearAndPut(PromotionFullReduceVO reduceVO, BigDecimal value) {
    this.details.clear();
    this.totalDiscountFee = BigDecimal.ZERO;
    return this.put(reduceVO, value);
  }

  @Override
  public int compareTo(FullReducePricingResultVO o) {
    if (o == null) {
      return 1;
    }
    return this.totalDiscountFee.compareTo(o.totalDiscountFee);
  }

  public Number getCompareNumber(PromotionFullReduceVO reduceVO) {
    if (reduceVO instanceof PromotionFullCutVO) {
      return reduceVO.getOriginalPriceOnly() ? originalPrice : price;
    } else if (reduceVO instanceof PromotionFullPiecesVO) {
      return amount;
    }
    throw new IllegalArgumentException("参数类型不正确");
  }

  /**
   * 合并两个计算结果
   *
   * @param other 另一个结果
   */
  public void merge(FullReducePricingResultVO other) {
    if (other == null) {
      return;
    }
    this.details.putAll(other.getDetails());
    this.totalDiscountFee = this.totalDiscountFee.add(other.getTotalDiscountFee());
    if (other.isFreeDelivery()) {
      this.freeDelivery = true;
    }
    if (CollectionUtils.isNotEmpty(other.getGiftProducts())) {
      this.getGiftProducts().addAll(other.getGiftProducts());
    }
  }

}
