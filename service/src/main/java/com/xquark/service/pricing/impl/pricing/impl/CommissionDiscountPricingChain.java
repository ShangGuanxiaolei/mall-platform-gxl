package com.xquark.service.pricing.impl.pricing.impl;

import com.hds.xquark.dal.model.CommissionTotal;
import com.hds.xquark.service.point.PointCommService;
import com.xquark.dal.consts.PointConstants;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.Sku;
import com.xquark.dal.type.LogisticDiscountType;
import com.xquark.dal.type.PromotionType;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pricing.impl.pricing.*;
import com.xquark.service.pricing.impl.pricing.hooks.CommissionBeforeHook;
import com.xquark.service.yundou.CommissionDiscountingRet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * created by
 *
 * @author wangxinhua at 18-6-29 下午11:47 佣金抵扣 - 佣金可以直接抵扣现金, 配置时需要配置在计算链的底端, 抵扣其他折扣优惠后的金额
 */
public class CommissionDiscountPricingChain extends DiscountPricingChain {
  private static Logger logger = LoggerFactory.getLogger(CommissionDiscountPricingChain.class);

  private final PointCommService pointCommService;

  public CommissionDiscountPricingChain(
      PointCommService pointCommService) {
    this.pointCommService = pointCommService;
  }

  @Override
  protected DiscountPricingResult calculatePrice(PricingChainParams params,
      PricingContext context) {
    BigDecimal commissionLogistics = context.getLogistics();//需要抵扣的邮费
    BigDecimal commissionLogisticsDiscount = BigDecimal.ZERO;//实际抵扣的邮费
    BigDecimal logisticsDiscount = context.getLogisticsDiscount();//前端显示的邮费减免
    BigDecimal logistics = context.getLogistics();//前端显示的邮费
    LogisticDiscountType logisticDiscountType = context.getLogisticDiscountType();
    BigDecimal currFee = context.getCurrFee();//需要抵扣的商品金额
    // 可以抵扣的佣金
    Long cpId = params.getBuyer().getCpId();
    BigDecimal needCommission = currFee.add(commissionLogistics);//将需要抵扣的积分加上邮费
    CommissionTotal commissionTotal = pointCommService
        .loadCommByCpId(cpId);
    DiscountPricingResult ret = DiscountPricingResult.emptyResult();

    logger.info("=====================当前需要抵扣的佣金：" + needCommission);

    if (commissionTotal == null) {
      commissionTotal = CommissionTotal.emptyInfo(cpId, CommissionTotal.class);
    }
    Map<String, BigDecimal> commissionMaxDeductionMap = new HashMap<>();
    for (CartItemVO cartItem : params.getCartItems()) {
      Sku sku = cartItem.getSku();
      Promotion currUsingPromotion = context.getCurrUsingPromotion(cartItem.getProductId());
      // 如果有活动则按活动价格分摊
      BigDecimal commissionForDivided = Optional.ofNullable(currUsingPromotion)
          .filter(p -> p.getPrice() != null && p.getPrice().compareTo(BigDecimal.ZERO) > 0)
          .map(Promotion::getPrice).orElse(sku.getPrice());
      BigDecimal commission = commissionForDivided.multiply(cartItem.getBAmount());
      commissionMaxDeductionMap.put(sku.getId(), commission);
    }
    // 积分计算值需要考虑折扣在确认时还是下单时计算
    BigDecimal currentCommission = commissionTotal.getTotalUsable();
    BigDecimal usableCommission;
    // 当前有的佣金大于需要使用
    if (currentCommission.compareTo(needCommission) > 0) {
      usableCommission = needCommission;
    } else {
      usableCommission = currentCommission;
    }

    BigDecimal discount = BigDecimal.ZERO;

    //获取德分可抵用价格的值
    BigDecimal usingPoint = context.getPointCommRet().getMaxUsingPoint();
    BigDecimal maxUsingPoint = usingPoint.divide(BigDecimal.TEN, 2, BigDecimal.ROUND_HALF_EVEN);
    //德分加上最大可使用积分
    BigDecimal maxUsableCommission = maxUsingPoint.add(usableCommission);
    BigDecimal maxUsingCommissionAddPoint;
    //用户已有积分大于最大可使用积分
    if(currentCommission.compareTo(maxUsableCommission) > 0){
      maxUsingCommissionAddPoint = maxUsableCommission;
    }else{
      maxUsingCommissionAddPoint = currentCommission;
    }

    logger.info("=====================当前可用usableCommission："+usableCommission);
    logger.info("=====================用户选择使用的积分："+params.getUserSelectCommission());

    BigDecimal userSelectCommission = params.getUserSelectCommission();
    if (userSelectCommission != null) {
      if (userSelectCommission.compareTo(maxUsingCommissionAddPoint) > 0) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "用户选择积分大于实际可使用积分");
      }
      discount = userSelectCommission
          .divide(PointConstants.COMMISSION_SCALA, 2, BigDecimal.ROUND_HALF_EVEN);

      if(logisticsDiscount.compareTo(BigDecimal.ZERO) > 0) {
        commissionLogisticsDiscount = commissionLogistics;
      } else {
        if (userSelectCommission.compareTo(currFee) >= 0){//用户选择使用的积分大于等于需要抵扣的积分
          commissionLogisticsDiscount = userSelectCommission.subtract(currFee);//抵扣掉的邮费
          if (commissionLogisticsDiscount.compareTo(commissionLogistics) > 0) {
            commissionLogisticsDiscount = commissionLogistics;
          }
        } else {
          commissionLogisticsDiscount = BigDecimal.ZERO;
        }
      }
    }
    if (discount.signum() != 0) {
      ret.addDiscount(discount, calculateScope(), context);
      Map<String, SkuDiscountDetail> maxDeductionDetailMap = devideCommissionDiscount(
          commissionMaxDeductionMap,
           discount);
      ret.addBeforeOrderHook(new CommissionBeforeHook(discount, maxDeductionDetailMap));
    }
    context.setCommissionRet(new CommissionDiscountingRet(usableCommission, userSelectCommission,
            discount, maxUsingCommissionAddPoint));

    if (commissionLogistics.compareTo(commissionLogisticsDiscount) >= 0)
      commissionLogistics = commissionLogistics.subtract(commissionLogisticsDiscount);

    Map<String ,Object> logisticsMap = new HashMap<>();
    logisticsMap.put("logisticsFee", commissionLogistics);
    logisticsMap.put("logistics", logistics);//用于前端显示的邮费
    logisticsMap.put("commissionLogisticsDiscount", commissionLogisticsDiscount);
    logisticsMap.put("logisticsDiscount", logisticsDiscount);//前端显示的减免值
    logisticsMap.put("logisticDiscountType", logisticDiscountType);
    ret.setLogisticsMap(logisticsMap);
    return ret;
  }

  /**
   * 摊薄积分到每个sku
   */
  private Map<String, SkuDiscountDetail> devideCommissionDiscount(
      Map<String, BigDecimal> commissionMap,
      BigDecimal using) {
    return divideDiscount("", commissionMap, using, 2);
  }

  @Override
  protected PromotionType calculateScope() {
    return PromotionType.COMMISSION;
  }
}
