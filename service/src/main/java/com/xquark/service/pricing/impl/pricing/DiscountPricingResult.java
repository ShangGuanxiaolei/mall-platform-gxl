package com.xquark.service.pricing.impl.pricing;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.CouponView;
import com.xquark.utils.functional.Consumer;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;
import java.util.Map.Entry;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/** Created by wangxinhua on 17-11-30. DESC: 折扣计算相关 (积分折扣、活动折扣等) 返回对象 */
public class DiscountPricingResult implements Serializable {

  public static final String DEVIDED_CUTDOWN_DETAIL_KEY = "devidedCutDown";

  /** 使用到的活动 - 通过钩子统一处理 */
  @Deprecated
  public static final String USING_PROMOTION_KEY = "usingPromotion";

  /** 是否是预购付尾款操作 */
  public static final String IS_PAY_REST_KEY = "payRest";

  public static final String VALID_COUPONS_KEY = "validCoupons";

  public static final String FINAL_USING_COUPON_KEY = "finalCoupon";

  private static final long serialVersionUID = -1114782892454868376L;

  /** 活动排序器 */
  public static final PromotionComparator PROMOTION_COMPARATOR = new PromotionComparator();

  /** 总优惠明细 */
  private final Map<PromotionType, BigDecimal> discounts;

  /** 根据比例摊分到商品上的总优惠明细 */
  private final Map<PromotionType, Map<String, SkuDiscountDetail>> discountDetails;

  /** 计算过程中的额外参数 TODO 考虑使用threadLocal来存储, merge可能会造成性能问题 */
  private final Map<String, Object> extra;

  /** 保存订单前钩子 */
  private Queue<Consumer<OrderUnit>> beforeOrderSaveHooks = new LinkedList<>();

  /** 保存订单后钩子 */
  private Queue<Consumer<OrderUnit>> afterOrderSaveHooks = new LinkedList<>();

  // 全部可用优惠
  private final List<CouponView> canUsePromotions;

  private Map<String, Promotion> currUsingPromotions;

  // 记录优惠是否已经排序过
  private volatile boolean isSorted;

  private final boolean autoSelect;

  /** 是否免邮 */
  private boolean isFreeDelivery;

  private boolean isPayFree;

  private Map<String, Object> logisticsMap;

  public Map<String, Object> getLogisticsMap() {
    return logisticsMap;
  }

  public void setLogisticsMap(Map<String, Object> logisticsMap) {
    this.logisticsMap = logisticsMap;
  }

  public void setFreeDelivery(boolean freeDelivery) {
    isFreeDelivery = freeDelivery;
  }

  public boolean isFreeDelivery() {
    return isFreeDelivery;
  }

  /** 是否计算收益, 默认为是 */
  private boolean isCountEarning = true;

  private DiscountPricingResult() {
    this(true);
  }

  private DiscountPricingResult(boolean autoSelect) {
    this.autoSelect = autoSelect;
    discounts = new HashMap<>();
    extra = new HashMap<>();
    canUsePromotions = new ArrayList<>();
    discountDetails = new HashMap<>();
    isFreeDelivery = isFreeDelivery();
  }

  /**
   * 增加折扣 若折扣类型相同则累加，若不同则直接放入map 建议使用 {@link #addDiscount(BigDecimal, PromotionType,
   * PricingContext)} 通知context减免折扣
   *
   * @param discount 新的折扣
   * @param type 该折扣类型
   */
  public void addDiscount(final BigDecimal discount, final PromotionType type) {
    checkNotNull(discount);
    checkNotNull(type);
    if (discount.signum() < 0) {
      return;
    }
    BigDecimal newValue = discount;
    if (discounts.containsKey(type)) {
      newValue = discount.add(discounts.get(type));
    }
    discounts.put(type, newValue);
  }

  /**
   * 计算折扣同时通知context
   *
   * @param discount 折扣
   * @param type 折扣类型
   * @param context 需要通知的上下文
   */
  public void addDiscount(BigDecimal discount, PromotionType type, PricingContext context) {
    addDiscount(discount, type);
    context.addDiscount(discount);
  }

  /**
   * 添加根据商品拆分的优惠
   *
   * @param type 优惠类型
   * @param details 优惠详情
   */
  public void addDiscountWithDetail(PromotionType type, Map<String, SkuDiscountDetail> details) {
    checkNotNull(type);
    checkNotNull(details);
    checkState(!getDiscountDetails().containsKey(type), "相同活动重复添加");
    if (MapUtils.isEmpty(details)) {
      return;
    }
    for (Entry<String, SkuDiscountDetail> entry : details.entrySet()) {
      SkuDiscountDetail detail = entry.getValue();
      this.addDiscount(detail.getDiscount(), type);
    }
    this.discountDetails.put(type, details);
  }

  /**
   * 根据类型获取折扣
   *
   * @param type 订单类型
   * @return 类型对应的折扣
   */
  public BigDecimal getDiscount(PromotionType type) {
    return Optional.ofNullable(discounts.get(type)).orElse(BigDecimal.ZERO);
  }

  /**
   * 获取总折扣
   *
   * @return 累加的总折扣
   */
  public BigDecimal getTotalDiscount() {
    BigDecimal total = BigDecimal.ZERO;
    if (MapUtils.isEmpty(discounts)) {
      return total;
    }
    for (Entry<PromotionType, BigDecimal> entry : discounts.entrySet()) {
      total = total.add(entry.getValue());
    }
    return total;
  }

  /** 自动获取最大折扣 */
  public BigDecimal getPromotionDiscount() {
    return getPromotionDiscount(null);
  }

  /**
   * 获取最大折扣
   *
   * @param specify 指定类型则取指定折扣
   * @return 最大折扣或指定折扣
   */
  public BigDecimal getPromotionDiscount(PromotionType specify) {
    if (CollectionUtils.isEmpty(canUsePromotions) || MapUtils.isEmpty(discounts)) {
      return BigDecimal.ZERO;
    }
    // 如果有指定的折扣则取指定折扣
    if (specify != null) {
      return getDiscount(specify);
    }
    // 如果只有一个折扣则直接返回
    if (canUsePromotions.size() == 1) {
      return canUsePromotions.get(0).getDiscount();
    }
    if (autoSelect) {
      // 自动取最大折扣
      return getMaxDiscount();
    }
    return BigDecimal.ZERO;
  }

  public boolean isCountEarning() {
    return isCountEarning;
  }

  public void setCountEarning(boolean countEarning) {
    isCountEarning = countEarning;
  }

  /**
   * 获取最大折扣
   *
   * @return 最大折扣值
   */
  public BigDecimal getMaxDiscount() {
    if (CollectionUtils.isEmpty(canUsePromotions)) {
      return BigDecimal.ZERO;
    }
    // TODO 使用max方法由于排序器是反的
    CouponView maxPromotion = Collections.max(canUsePromotions, PROMOTION_COMPARATOR);
    return maxPromotion.getDiscount();
  }

  /** 供前端展示的优惠详情 */
  public Map<PromotionType, BigDecimal> getDiscountDetail() {
    Map<PromotionType, BigDecimal> ret = new HashMap<>();
    for (PromotionType promotionType : PromotionType.values()) {
      BigDecimal pDiscount = getDiscount(promotionType);
      if (pDiscount == null) {
        ret.put(promotionType, BigDecimal.ZERO);
      } else {
        ret.put(promotionType, pDiscount);
      }
    }
    return ret;
  }

  /**
   * 获取折扣map
   *
   * @return 类新 - 折扣对应的map
   */
  public Map<PromotionType, BigDecimal> getDiscounts() {
    return discounts;
  }

  public void addExtraParam(String key, Object value) {
    extra.put(key, value);
  }

  public Map<String, Object> getExtra() {
    return extra;
  }

  /**
   * 获取附加参数
   *
   * @param key 参数键
   * @param clazz 值类型
   * @param <V> 值类型
   * @return value 或null
   * @throws ClassCastException 类型转换错误
   */
  @SuppressWarnings("unchecked")
  public <V> V getExtraParam(String key, Class<V> clazz) {
    return (V) extra.get(key);
  }

  /**
   * 不支持范型的取值方法
   *
   * @param key 参数键
   * @return value或null
   */
  public Object getExtraParam(String key) {
    return extra.get(key);
  }

  public List<CouponView> getCanUsePromotions() {
    return canUsePromotions;
  }

  public List<CouponView> getSortedCanUsePromotonns() {
    if (!isSorted) {
      Collections.sort(this.getCanUsePromotions(), PROMOTION_COMPARATOR);
    }
    return this.canUsePromotions;
  }

  /**
   * 获取优惠类型-商品-优惠的映射关系
   *
   * @return map or emptyMap
   */
  public Map<PromotionType, Map<String, SkuDiscountDetail>> getDiscountDetails() {
    return discountDetails;
  }

  /**
   * 获取用户选择的优惠详情
   *
   * @param userSelectedPromotion 优惠类型
   * @return 商品-优惠映射关系 map or null
   */
  public Map<String, SkuDiscountDetail> getSpecificDiscountDetails(
      PromotionType userSelectedPromotion) {
    return getDiscountDetails().get(userSelectedPromotion);
  }

  /**
   * 判断是否有某活动的有优惠
   *
   * @param type 活动类型
   * @return true or false
   */
  public boolean isPromotionInUse(PromotionType type) {
    return type != null && discounts.containsKey(type) && discounts.get(type) != null;
  }

  public boolean isSorted() {
    return isSorted;
  }

  /**
   * 将当前对象属性与另一个对象合并 (累加优惠)， 返回合并后的新对象
   *
   * @param another 需要合并的对象
   */
  public void merge(DiscountPricingResult another) {
    if (another == null) {
      return;
    }
    this.extra.putAll(another.getExtra());
    this.discountDetails.putAll(another.getDiscountDetails());
    // 可用优惠
    List<CouponView> anotherPromotions = another.getCanUsePromotions();
    if (CollectionUtils.isEmpty(this.canUsePromotions)) {
      this.isSorted = another.isSorted;
    } else if (CollectionUtils.isNotEmpty(anotherPromotions)) {
      // 双方都非空合并后需要重新排序
      isSorted = false;
    }
    this.canUsePromotions.addAll(anotherPromotions);
    Map<PromotionType, BigDecimal> anotherDiscountMap = another.getDiscounts();
    if (MapUtils.isNotEmpty(anotherDiscountMap)) {
      for (Entry<PromotionType, BigDecimal> entry : anotherDiscountMap.entrySet()) {
        BigDecimal discount = entry.getValue();
        PromotionType key = entry.getKey();
        this.addDiscount(discount, key);
      }
    }

    // 设置是否可以享受收益, 默认都是true,
    this.setCountEarning(isCountEarning() && another.isCountEarning());

    // 只要有一个活动免单则都免单
    this.setIsPayFree(getIsPayFree() || another.getIsPayFree());

    this.setFreeDelivery(isFreeDelivery() || another.isFreeDelivery());

    Queue<Consumer<OrderUnit>> beforeHook = another.getBeforeOrderSaveHooks();
    Queue<Consumer<OrderUnit>> afterHook = another.getAfterOrderSaveHooks();
    if (CollectionUtils.isNotEmpty(beforeHook)) {
      this.getBeforeOrderSaveHooks().addAll(beforeHook);
    }
    if (CollectionUtils.isNotEmpty(afterHook)) {
      this.getAfterOrderSaveHooks().addAll(afterHook);
    }

    if(another.getLogisticsMap() != null)
      this.setLogisticsMap(another.getLogisticsMap());
  }

  public static DiscountPricingResult emptyResult() {
    return new DiscountPricingResult();
  }

  public static DiscountPricingResult emptyResult(boolean autoSelect) {
    return new DiscountPricingResult(autoSelect);
  }

  public boolean getIsPayFree() {
    return isPayFree;
  }

  public void setIsPayFree(boolean payFree) {
    isPayFree = payFree;
  }

  private static class PromotionComparator implements Comparator<CouponView> {

    @Override
    public int compare(CouponView o1, CouponView o2) {
      // 从大到小排序, 返回正常结果的负数
      if (o1 == null) {
        return 1;
      }
      if (o2 == null) {
        return -1;
      }
      PromotionType o1Type = o1.getPromotionType();
      PromotionType o2Type = o2.getPromotionType();
      assert o1Type != null && o2Type != null;
      if (!Objects.equals(o1Type, o2Type)) {
        // 根据类型数字从小到大排序
        return o1Type.getSortNum().compareTo(o2Type.getSortNum());
      }
      BigDecimal o1Discount = o1.getDiscount();
      BigDecimal o2Discount = o2.getDiscount();
      assert o1Discount != null && o2Discount != null;
      // 否则按照优惠大小从大到小排序
      return -o1Discount.compareTo(o2Discount);
    }
  }

  public void addBeforeOrderHook(Consumer<OrderUnit> hook) {
    this.beforeOrderSaveHooks.add(hook);
  }

  public void addAfterOrderHook(Consumer<OrderUnit> hook) {
    this.afterOrderSaveHooks.add(hook);
  }

  public Queue<Consumer<OrderUnit>> getBeforeOrderSaveHooks() {
    return beforeOrderSaveHooks;
  }

  public Queue<Consumer<OrderUnit>> getAfterOrderSaveHooks() {
    return afterOrderSaveHooks;
  }

  /** 保存订单前调用 */
  public void doBeforeOrderHooks(OrderUnit unit) {
    while (!beforeOrderSaveHooks.isEmpty()) {
      Consumer<OrderUnit> hook = beforeOrderSaveHooks.poll();
      hook.accept(unit);
    }
  }

  /** 保存订单后调用 */
  public void doAfterOrderHooks(OrderUnit unit) {
    while (!afterOrderSaveHooks.isEmpty()) {
      Consumer<OrderUnit> hook = afterOrderSaveHooks.poll();
      hook.accept(unit);
    }
  }

  public Map<String, Promotion> getCurrUsingPromotions() {
    return currUsingPromotions;
  }

  public void setCurrUsingPromotions(
      Map<String, Promotion> currUsingPromotions) {
    this.currUsingPromotions = currUsingPromotions;
  }
}
