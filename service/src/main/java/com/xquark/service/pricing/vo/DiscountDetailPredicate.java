package com.xquark.service.pricing.vo;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.xquark.dal.type.PromotionType;

import java.math.BigDecimal;
import java.util.Map.Entry;
import java.util.Set;

import static com.xquark.dal.type.PromotionType.*;

/**
 * @author wangxinhua.
 * @date 2018/11/26
 */
public class DiscountDetailPredicate implements Predicate<Entry<PromotionType, BigDecimal>> {

  private static final Set<PromotionType> WHITE_LIST =
      ImmutableSet.of(CUT_DOWN, LOGISTICS, LOGISTICS_DISCOUNT, OTHERS);

  @Override
  public boolean apply(Entry<PromotionType, BigDecimal> input) {
    PromotionType promotion = input.getKey();
    BigDecimal val = input.getValue();
    if (WHITE_LIST.contains(promotion)) {
      return true;
    }
    return val.signum() > 0;
  }
}
