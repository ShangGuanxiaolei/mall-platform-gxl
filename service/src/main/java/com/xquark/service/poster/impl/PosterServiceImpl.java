package com.xquark.service.poster.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.PosterMapper;
import com.xquark.dal.mapper.PosterTagMapper;
import com.xquark.dal.model.Poster;
import com.xquark.dal.model.PosterTag;
import com.xquark.dal.model.Term;
import com.xquark.service.category.TermService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.poster.PosterService;

@Service("posterService")
public class PosterServiceImpl extends BaseServiceImpl implements PosterService {

  @Autowired
  private PosterMapper posterMapper;

  @Autowired
  private PosterTagMapper posterTagAMapper;

  @Autowired
  private TermService termService;

  @Override
  public Poster save(Poster poster) {
    posterMapper.insert(poster);
    return poster;
  }

  @Override
  public Poster load(String id) {
    return posterMapper.load(id);
  }

  @Override
  public List<Poster> listByTag(String tag) {
    return posterMapper.selectByTag(tag);
  }

  @Override
  public List<Poster> listAll(Pageable pager) {
    return posterMapper.selectAll();
  }

  @Override
  public PosterTag savePosterTag(String posterId, String tag) {
    Term term = termService.loadByName(tag);
    PosterTag pt = new PosterTag();

    pt.setPosterId(posterId);
    pt.setTermId(term.getId());
    pt.setCreatorId(getCurrentUser().getId());
    pt.setCreatedAt(new Date());
    posterTagAMapper.insert(pt);
    return posterTagAMapper.load(pt.getId());
  }

  @Override
  public PosterTag removePosterTag(String posterId, String tag) {
    Term term = termService.loadByName(tag);
    return posterTagAMapper.delete(posterId, term.getId());
  }

}
