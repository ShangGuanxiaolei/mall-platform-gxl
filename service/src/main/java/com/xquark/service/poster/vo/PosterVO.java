package com.xquark.service.poster.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.Poster;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

public class PosterVO extends Poster {

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String imgUrl;

  private String actionType = "WEB";

  public PosterVO() {
    super();
  }

  public PosterVO(String imgCode, String url, String imgUrl) {
    super();
    this.setImgCode(imgCode);
    this.setUrl(url);
    this.imgUrl = imgUrl;
  }

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }

  public String getActionType() {
    return actionType;
  }

  public void setActionType(String actionType) {
    this.actionType = actionType;
  }

}
