package com.xquark.service.poster;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.Poster;
import com.xquark.dal.model.PosterTag;

public interface PosterService {

  Poster save(Poster poster);

  Poster load(String id);

  List<Poster> listAll(Pageable pager);

  List<Poster> listByTag(String tag);

  PosterTag savePosterTag(String posterId, String tag);

  PosterTag removePosterTag(String posterId, String tag);

}