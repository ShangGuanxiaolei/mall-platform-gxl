package com.xquark.service.bargain;

import com.xquark.dal.model.PromotionBargainDetail;
import com.xquark.dal.model.PromotionBargainHistory;
import com.xquark.dal.model.PromotionBargainProduct;
import com.xquark.dal.vo.PromotionBargainPlaceVO;
import com.xquark.dal.vo.PromotionBargainSkuVO;
import com.xquark.service.BaseService;
import io.vavr.control.Either;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

/**
 * 砍价相关接口
 *
 * @author wangxinhua
 * @date 2019-04-06
 * @since 1.0
 */
public interface PromotionBargainService extends BaseService {

  /**
   * 发起砍价
   *
   * @param bargainId 砍价活动id
   * @param skuId 砍价的skuId, 该skuId必须是合法的砍价商品sku之一
   * @return {@link Either} left 错误信息, right 发起结果
   */
  Either<String, PromotionBargainDetail> start(String bargainId, String skuId);

  /**
   * 帮忙砍价
   *
   * @param detailId 发起砍价id
   * @return {@link Either} left 错误信息, right 砍价结果
   */
  Either<String, PromotionBargainHistory> help(String detailId);

  /**
   * 查询商品信息
   *
   * @param bargainId 砍价id
   * @param skuId skuId
   * @return 砍价商品信息对象
   */
  Optional<PromotionBargainProduct> loadProduct(String bargainId, String skuId);

  /**
   * 查询砍价活动
   *
   * @param order 活动排序
   * @param direction 查询顺序
   * @param pageable 分页参数
   * @return 砍价商品列表
   */
  List<PromotionBargainSkuVO> listPromotion(
      String order, Sort.Direction direction, Pageable pageable);

  /**
   * 查询砍价活动总数
   *
   * @return 活动总数
   */
  Long countPromotion();

  /**
   * 进入砍价会场
   *
   * @param order 活动排序
   * @param direction 查询顺序
   * @param pageable 分页参数
   * @return 砍价会场列表
   */
  List<PromotionBargainPlaceVO> listPromotionPlace(
      String order, Sort.Direction direction, Pageable pageable);
}
