package com.xquark.service.bargain;

import com.xquark.dal.model.PromotionBargainRule;
import com.xquark.dal.type.CareerLevelType;

import java.util.Optional;
import java.util.function.Function;

/**
 * @author wangxinhua
 * @date 2019-04-10
 * @since 1.0
 */
public interface PromotionBargainRuleService {

  /**
   * 根据用户身份获取砍价规则
   *
   * @param level 用户身份
   * @return 对应的砍价规则
   */
  Optional<PromotionBargainRule> loadRule(CareerLevelType level);

  /**
   * 根据用户id获取砍价规则
   * @param cpId 用户cpId
   * @param typeFunc 根据用户id来获取身份的方法
   * @return 对应的砍价规则
   */
  Optional<PromotionBargainRule> loadRule(Long cpId, Function<Long, CareerLevelType> typeFunc);
}
