package com.xquark.service.bargain;

import com.xquark.dal.model.PromotionBargainDetail;
import com.xquark.dal.vo.PromotionBargainDetailUserVO;
import com.xquark.dal.vo.PromotionBargainPlaceVO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * 发起砍价相关service
 *
 * @author wangxinhua
 * @date 2019-04-12
 * @since 1.0
 */
public interface PromotionBargainDetailService {

  /**
   * 查询用户是否已经参与过砍价
   *
   * @param bargainId 砍价主表id
   * @param userId 用户id
   * @return true or false
   */
  boolean isJoin(String bargainId, String userId);

  /**
   * 保存发起砍价记录
   *
   * @param detail 发起记录
   * @return true or false
   */
  boolean save(PromotionBargainDetail detail);

  /**
   * 根据id查询
   *
   * @param id 主键
   * @return 发起砍价记录
   */
  PromotionBargainDetail load(String id);

  /**
   * 根据userId查询我发起的砍价
   *
   * @param userId 用户id
   * @param order 活动排序
   * @param direction 查询顺序
   * @param pageable 分页参数
   * @return 用户发起砍价记录集合
   */
  List<PromotionBargainPlaceVO> selectByUser(
      String userId, String order, Sort.Direction direction, Pageable pageable);

  /**
   * 查询我的砍价总记录
   *
   * @return 我的砍价记录
   */
  Long countMyPromotion();

  /**
   * 根据userid,bargainId,skuId,查询用户是否发起了砍价
   *
   * @param skuId 商品skuid
   * @param bargainId 活动id
   * @param userId userId
   * @return 针对指定活动查询我是否发起了砍价
   */
  boolean isExitsByUserIdAndBargainIdAndSkuId(String skuId, String bargainId, String userId);

  /**
   * 查询我的砍价进度,单sku查询
   *
   * @param skuId skuId
   * @param bargainId 活动id
   * @param userId 用户id
   * @return 返回我的砍价进度，单sku
   */
  PromotionBargainDetailUserVO selectMyBargainScheduleByUserId(
          String bargainDetailId);

  /**
   * 通过活动id和skuid查询该活动已发起的数量
   *
   * @param bargainId 活动id
   * @param skuId 商品skuId
   * @return 该活动已发起的数量
   */
  Integer countlaunchNumByBargainIdAndSkuId(String bargainId, String skuId);
}
