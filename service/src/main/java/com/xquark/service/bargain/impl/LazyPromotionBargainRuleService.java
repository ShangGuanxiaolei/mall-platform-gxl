package com.xquark.service.bargain.impl;

import com.xquark.dal.mapper.PromotionBargainRuleMapper;
import com.xquark.dal.model.PromotionBargainRule;
import com.xquark.dal.type.CareerLevelType;
import com.xquark.service.bargain.PromotionBargainRuleService;
import io.vavr.Function0;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 通过懒加载实现的获取规则列表
 *
 * @author wangxinhua
 * @date 2019-04-10
 * @since 1.0
 */
@Service
public class LazyPromotionBargainRuleService
    implements PromotionBargainRuleService, InitializingBean {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(LazyPromotionBargainRuleService.class);

  private final PromotionBargainRuleMapper bargainRuleMapper;

  private Function0<Map<CareerLevelType, List<PromotionBargainRule>>> ruleFunc;

  @Override
  public void afterPropertiesSet() {
    ruleFunc =
        Function0.of(
                () ->
                    bargainRuleMapper.listAll().stream()
                        .collect(Collectors.groupingBy(PromotionBargainRule::getIdentityType)))
            .memoized();
  }

  @Autowired
  public LazyPromotionBargainRuleService(PromotionBargainRuleMapper bargainRuleMapper) {
    this.bargainRuleMapper = bargainRuleMapper;
  }

  @Override
  public Optional<PromotionBargainRule> loadRule(CareerLevelType level) {
    Map<CareerLevelType, List<PromotionBargainRule>> apply = ruleFunc.apply();
    if (MapUtils.isEmpty(apply)) {
      LOGGER.warn("砍价规则未配置");
      return Optional.empty();
    }
    List<PromotionBargainRule> rules = apply.get(level);
    if (CollectionUtils.isEmpty(rules)) {
      LOGGER.warn("身份 {} 对应的砍价规则未配置", level);
      return Optional.empty();
    }
    if (rules.size() > 1) {
      LOGGER.warn("身份 {} 配置了多条砍价规则", level);
    }
    // 默认只取第一条
    return Optional.of(rules.get(0));
  }

  @Override
  public Optional<PromotionBargainRule> loadRule(
      Long cpId, Function<Long, CareerLevelType> typeFunc) {
    final CareerLevelType level = typeFunc.apply(cpId);
    return loadRule(level);
  }
}
