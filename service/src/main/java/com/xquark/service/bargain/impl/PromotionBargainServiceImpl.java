package com.xquark.service.bargain.impl;

import com.xquark.dal.mapper.PromotionBargainHistoryMapper;
import com.xquark.dal.mapper.PromotionBargainMapper;
import com.xquark.dal.mapper.PromotionBargainProductMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.type.CareerLevelType;
import com.xquark.dal.vo.PromotionBargainPlaceVO;
import com.xquark.dal.vo.PromotionBargainProductVO;
import com.xquark.dal.vo.PromotionBargainSku;
import com.xquark.dal.vo.PromotionBargainSkuVO;
import com.xquark.service.bargain.PromotionBargainDetailService;
import com.xquark.service.bargain.PromotionBargainRuleService;
import com.xquark.service.bargain.PromotionBargainService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.promotion.PromotionProductLoader;
import com.xquark.utils.DateUtils;
import io.vavr.control.Either;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * 砍价接口默认实现类
 *
 * @author wangxinhua
 * @date 2019-04-06
 * @since 1.0
 */
@Service
public class PromotionBargainServiceImpl extends BaseServiceImpl
    implements PromotionBargainService, PromotionProductLoader<PromotionBargainProductVO> {

  /** 砍价商品关联mapper */
  private final PromotionBargainProductMapper bargainProductMapper;

  /** 砍价活动关联mapper */
  private final PromotionBargainMapper bargainMapper;

  /** 发起砍价service */
  private final PromotionBargainDetailService bargainDetailService;

  /** 用户service */
  private final CustomerProfileService profileService;

  /** 砍价历史mapper * */
  private final PromotionBargainHistoryMapper bargainHistoryMapper;

  /** 砍价规则service */
  private final PromotionBargainRuleService bargainRuleService;

  /** redis操作 */
  private final RedisTemplate<String, Double> redisTemplate;

  @Autowired
  public PromotionBargainServiceImpl(
      PromotionBargainProductMapper bargainProductMapper,
      PromotionBargainMapper bargainMapper,
      PromotionBargainDetailService bargainDetailService,
      CustomerProfileService profileService,
      PromotionBargainHistoryMapper bargainHistoryMapper,
      PromotionBargainRuleService bargainRuleService,
      RedisTemplate<String, Double> redisTemplate) {
    this.bargainProductMapper = bargainProductMapper;
    this.bargainMapper = bargainMapper;
    this.bargainDetailService = bargainDetailService;
    this.profileService = profileService;
    this.bargainHistoryMapper = bargainHistoryMapper;
    this.bargainRuleService = bargainRuleService;
    this.redisTemplate = redisTemplate;
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public Either<String, PromotionBargainDetail> start(String bargainId, String skuId) {
    // 获取砍价sku
    final PromotionBargainSku bargainSku =
        bargainProductMapper.selectVOByBargainIdAndSkuId(bargainId, skuId);
    if (bargainSku == null) {
      return Either.left("砍价商品不存在");
    }

    final User currUser = getCurrentUser();

    final Optional<PromotionBargainRule> rule =
        bargainRuleService.loadRule(currUser.getCpId(), profileService::getLevelType);
    if (!rule.isPresent()) {
      return Either.left("砍价规则未初始化");
    }

    if (!bargainSku.isValid()) {
      return Either.left("活动已失效");
    }

    // 校验库存
    if (bargainSku.getStock() <= 0) {
      return Either.left("商品库存不足, 无法发起砍价");
    }

    final String userId = currUser.getId();
    final boolean isJoin = bargainDetailService.isJoin(bargainId, userId);
    if (isJoin) {
      return Either.left("您已经发起过砍价, 请不要重复发起哦");
    }

    final PromotionBargainDetail initDetail =
        PromotionBargainDetail.init(currUser.getId(), bargainSku, rule.get()::genDiscountSelf);
    // 保存发起砍价表
    boolean ret = bargainDetailService.save(initDetail);
    if (!ret) {
      return Either.left("抱歉，发起失败; 请稍后再试");
    }
    // 发起成功, 初始化砍价金额
    final String key = genRedisKey(initDetail);
    redisTemplate.opsForValue().set(key, initDetail.getCurrDiscount().doubleValue());
    // 设置活动失效后5天清理redis的值
    final Date expireDate = DateUtils.addDay(bargainSku.getValidTo(), 5);
    redisTemplate.expireAt(key, expireDate);
    return Either.right(initDetail);
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public Either<String, PromotionBargainHistory> help(String detailId) {
    final PromotionBargainDetail detail = bargainDetailService.load(detailId);
    if (detail == null) {
      return Either.left("砍价未发起，无法开始砍价");
    }

    final User currUser = getCurrentUser();
    final String userId = currUser.getId();
    if (StringUtils.equals(detail.getUserId(), userId)) {
      return Either.left("您已经帮自己砍过了哦");
    }

    final PromotionBargainSku bargainSku =
        bargainProductMapper.selectVOByBargainIdAndSkuId(detail.getBargainId(), detail.getSkuId());
    if (bargainSku == null) {
      return Either.left("帮砍商品不存在");
    }

    final CareerLevelType levelType = profileService.getLevelType(currUser.getCpId());
    // 是否只支持新人帮砍
    if (bargainSku.getSupportLimit() && !levelType.isNewUserType()) {
      return Either.left("该商品只有新人可以帮砍哦, 去看看其它宝贝吧~");
    }

    // 是否已经参与过该次砍价
    Boolean isBargained = bargainHistoryMapper.selectExistsByDetailIdAndUser(detailId, userId);
    if (isBargained) {
      return Either.left("您已参与过该次砍价，不能重复帮砍哦");
    }

    final String redisKey = genRedisKey(detail);

    // 当前redis中的剩余砍价金额
    final BigDecimal currDiscount =
        Optional.ofNullable(redisTemplate.opsForValue().get(redisKey))
            .map(BigDecimal::valueOf)
            .orElse(BigDecimal.ONE.negate());
    if (currDiscount.signum() < 0) {
      return Either.left("砍价数据异常, 未发起砍价");
    }

    // 底价
    final BigDecimal priceEnd = bargainSku.getPriceEnd();

    if (currDiscount.compareTo(priceEnd) <= 0) {
      return Either.left("当前已砍至底价, 不能继续砍价");
    }

    final Optional<PromotionBargainRule> rule = bargainRuleService.loadRule(levelType);
    if (!rule.isPresent()) {
      return Either.left("砍价规则未配置");
    }

    final BigDecimal genDiscount = rule.get().genDiscountSupport(bargainSku.getPriceStart());

    // 生成的折扣会导致扣减到负数
    final boolean isOverflow = currDiscount.subtract(genDiscount).compareTo(priceEnd) < 0;
    // 溢出的话直接砍到底价
    final BigDecimal realDiscount = isOverflow ? currDiscount.subtract(priceEnd) : genDiscount;

    // redis扣减砍价金额
    final BigDecimal after =
        BigDecimal.valueOf(
            redisTemplate.opsForValue().increment(redisKey, -realDiscount.doubleValue()));
    if (after.compareTo(priceEnd) < 0) {
      // 扣减后减到负数, 表示别的线程已经扣减了，返回
      return Either.left("您来晚了，商品已砍至底价");
    }

    // 构建砍价历史记录, 砍价成功
    final PromotionBargainHistory history =
        PromotionBargainHistory.init(userId, detailId, realDiscount);

    final boolean ret = bargainHistoryMapper.insert(history) > 0;
    if (!ret) {
      return Either.left("抱歉，砍价失败; 请稍后再试");
    }
    return Either.right(history);
  }

  @Override
  public Optional<PromotionBargainProduct> loadProduct(String bargainId, String skuId) {
    return Optional.ofNullable(bargainProductMapper.selectByBargainIdAndSkuId(bargainId, skuId));
  }

  /**
   * 首页列出砍价活动
   *
   * @param order 活动排序
   * @param direction 查询顺序
   * @param pageable 分页参数
   * @return
   */
  @Override
  public List<PromotionBargainSkuVO> listPromotion(
      String order, Sort.Direction direction, Pageable pageable) {
    return bargainProductMapper.listPromotion(order, direction, pageable);
  }

  /**
   * 计算活动数量
   *
   * @return
   */
  @Override
  public Long countPromotion() {
    return bargainProductMapper.countPromotion();
  }

  /**
   * 通过当前用户的userId查询对应的砍价会场详情
   *
   * @return 砍价会场视图集合
   */
  @Override
  public List<PromotionBargainPlaceVO> listPromotionPlace(
      String order, Sort.Direction direction, Pageable pageable) {
    // 查询所有的活动并放到list中
    List<PromotionBargainPlaceVO> bargainPlaceVOList =
        bargainMapper.selectAllBargain(order, direction, pageable);
    final User currUser = getCurrentUser();
    final String userId = currUser.getId();
    for (PromotionBargainPlaceVO bargainPlaceVO : bargainPlaceVOList) {
      int num = bargainPlaceVOList.indexOf(bargainPlaceVO);
      String skuId = bargainPlaceVO.getSkuId();
      String bargainId = bargainPlaceVO.getBargainId();
      Integer launchNum = bargainDetailService.countlaunchNumByBargainIdAndSkuId(bargainId, skuId);
      bargainPlaceVO.setLaunchNum(launchNum);
      // 查询用户参与的活动，并显示是否发起了订单
      boolean flag =
          bargainDetailService.isExitsByUserIdAndBargainIdAndSkuId(skuId, bargainId, userId);
      if (flag) {
        bargainPlaceVO = bargainMapper.selectAllBargainByUserId(skuId, bargainId, userId);
        bargainPlaceVOList.set(num, bargainPlaceVO);
      }
    }
    return bargainPlaceVOList;
  }

  @Override
  public Optional<PromotionBargainProductVO> load(String promotionId, String productId) {
    return Optional.ofNullable(bargainMapper.selectVOByPIdAndProduct(promotionId, productId));
  }

  /**
   * 从detail中生成redis的key 根据砍价发起表跟用户id表生成redis key
   *
   * @param detail 发起砍价数据
   * @return redis key
   */
  private String genRedisKey(@NotNull PromotionBargainDetail detail) {
    return String.format("BARGAIN_COUNTER:%s:%s", detail.getId(), detail.getUserId());
  }

}
