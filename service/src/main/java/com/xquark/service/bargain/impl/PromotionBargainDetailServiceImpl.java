package com.xquark.service.bargain.impl;

import com.xquark.dal.mapper.PromotionBargainDetailMapper;
import com.xquark.dal.mapper.PromotionBargainMapper;
import com.xquark.dal.model.PromotionBargainDetail;
import com.xquark.dal.vo.PromotionBargainDetailUserVO;
import com.xquark.dal.vo.PromotionBargainPlaceVO;
import com.xquark.service.bargain.PromotionBargainDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wangxinhua
 * @date 2019-04-12
 * @since 1.0
 */
@Service
public class PromotionBargainDetailServiceImpl implements PromotionBargainDetailService {

  private final PromotionBargainMapper bargainMapper;

  private final PromotionBargainDetailMapper bargainDetailMapper;

  @Autowired
  public PromotionBargainDetailServiceImpl(
      PromotionBargainDetailMapper bargainDetailMapper, PromotionBargainMapper bargainMapper) {
    this.bargainDetailMapper = bargainDetailMapper;
    this.bargainMapper = bargainMapper;
  }

  @Override
  public boolean isJoin(String bargainId, String userId) {
    return bargainDetailMapper.selectBargainExists(bargainId, userId);
  }

  @Override
  public boolean save(PromotionBargainDetail detail) {
    return bargainDetailMapper.insert(detail) > 0;
  }

  @Override
  public PromotionBargainDetail load(String id) {
    return bargainDetailMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<PromotionBargainPlaceVO> selectByUser(
      String userId, String order, Sort.Direction direction, Pageable pageable) {
    return bargainMapper.selectMyBargainRecordHelp(userId);
  }

  @Override
  public Long countMyPromotion() {
    return bargainDetailMapper.countMyPromotion();
  }

  @Override
  public boolean isExitsByUserIdAndBargainIdAndSkuId(
      String skuId, String bargainId, String userId) {
    return bargainDetailMapper.isLaunch(skuId, bargainId, userId);
  }

  @Override
  public PromotionBargainDetailUserVO selectMyBargainScheduleByUserId(
      String bargainDetailId) {
    return bargainDetailMapper.loadMySchedule(bargainDetailId);
  }

  @Override
  public Integer countlaunchNumByBargainIdAndSkuId(String bargainId, String skuId) {
    return bargainDetailMapper.selectlaunchNumByBargainIdAndSkuId(bargainId, skuId);
  }
}
