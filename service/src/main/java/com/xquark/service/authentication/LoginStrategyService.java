package com.xquark.service.authentication;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vdlm.common.bus.BusSignalManager;
import com.xquark.dal.mapper.DomainLoginStrategyMapper;
import com.xquark.dal.model.DomainLoginStrategy;

/**
 * @author: chenxi
 */

@Service("loginStrategyService")
public class LoginStrategyService {

  private final static Logger LOG = LoggerFactory.getLogger(LoginStrategyService.class);

  @Autowired
  private BusSignalManager bsm;
  @Autowired
  private DomainLoginStrategyMapper dlsm;

  public List<DomainLoginStrategy> loadLoginStrategies(String profile) {
    return dlsm.listByProfile(profile);
  }

  public List<DomainLoginStrategy> loadLoginStrategies(String profile, String module) {
    return dlsm.listByProfileAndModule(profile, module);
  }

  public void addLoginStrategy(DomainLoginStrategy dls) {
    final int inserts = dlsm.insert(dls);
    if (inserts == 1) {
      bsm.signal(new LoginStrategyEvent(LoginStrategyEventType.ADD, dls));
      LOG.info("added domain login strategy {} successfully", dls);
    } else {
      LOG.warn("failed to add domain login strategy {}", dls);
    }
  }

  public void updateLoginStrategy(DomainLoginStrategy dls) {
    final int updates = dlsm.update(dls);
    if (updates == 1) {
      bsm.signal(new LoginStrategyEvent(LoginStrategyEventType.UPDATE, dls));
      LOG.info("updated domain login strategy {} successfully", dls);
    } else {
      LOG.warn("failed to update domain login strategy {}", dls);
    }
  }

  public void deleteLoginStrategy(DomainLoginStrategy dls) {
    final int deletes = dlsm.delete(dls);
    if (deletes == 1) {
      bsm.signal(new LoginStrategyEvent(LoginStrategyEventType.REMOVE, dls));
      LOG.info("deleted domain login strategy {} successfully", dls);
    } else {
      LOG.warn("failed to delete domain login strategy {}", dls);
    }
  }
}
