package com.xquark.service.authentication;

/**
 * @author: chenxi
 */

public enum LoginStrategyEventType {

  ADD,
  UPDATE,
  REMOVE
}
