package com.xquark.service.authentication;

import com.xquark.dal.model.DomainLoginStrategy;

/**
 * @author: chenxi
 */

public class LoginStrategyEvent {

  private final LoginStrategyEventType type;

  private final DomainLoginStrategy data;

  public LoginStrategyEvent(LoginStrategyEventType type, DomainLoginStrategy data) {
    this.type = type;
    this.data = data;
  }

  public LoginStrategyEventType getType() {
    return type;
  }

  public DomainLoginStrategy getData() {
    return data;
  }
}
