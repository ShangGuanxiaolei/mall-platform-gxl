package com.xquark.service.promotion;

import com.xquark.dal.model.Order;

/**
 * Created by wangxinhua on 17-12-13. DESC:
 */
public interface PromotionOrderObServer {

  void onSubmit(Order order);

  void onCancel(Order order);

}
