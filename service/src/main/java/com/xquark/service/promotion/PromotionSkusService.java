package com.xquark.service.promotion;

import java.util.List;

import com.xquark.dal.vo.PromotionSkuVo;

public interface PromotionSkusService {

	/**
	 * 根据SkuCode查询活动sku范围
	 * @param skuCode
	 * @return
	 */
	List<PromotionSkuVo>  getPromotionSkus(String skuCode);
	/**
	 * 根据活动编码查询活动sku
	 * @param pCode
	 * @return
	 */
	List<PromotionSkuVo>  getPromotionSkusByPcode(String pCode);
	/**
	 * 查询指定活动类型的sku可用库存
	 * @param skuCode
	 * @param pType
	 * @return
	 */
	List<Integer> getPSkuUseableNum (String skuCode, String pType);

	/**
	 * 判断指定的活动商品是否有足够库存
	 * @param promotionSkuIds
	 * @return
	 */
	boolean hasEnoughStock(List<String> promotionSkuIds);
	/**
	 * 根据productid查询skuid
	 */
	String querySkuCodeByProductid(String productId,String pCode);

	List<String> getPromotionSkuIds (List<String> skuIds);

    /**
     * 根据skuCode和pCode查询活动商品信息
     * @param skuCode
     * @param pCode
     * @return
     */
	PromotionSkuVo selectProSkuBySkuCodeAndPcode(String skuCode,String pCode);

	List<String> selectPCodeBySkuCodeList(List<String> promotionskus);

	/**
	 * 根据skuCode和pCode 查询可用库存
	 * @param skuCode
	 * @param pCode
	 * @return
	 */
	Integer getUseableNumBySkuCodeAndPcode(String skuCode, String pCode);

	/**
	 * 根据SkuCode查询活动sku包括失效的
	 * @param skuCode
	 * @return
	 */
	List<PromotionSkuVo>  getPromotionSkusAllStatus(String skuCode);

	/**
	 * 根据商品id查询活动
	 * @param productId
	 * @return
	 */
	List<PromotionSkuVo> getPromotionByProductId(String productId);

	PromotionSkuVo getProductSalesSkus (String skuCode,String pCode);

}
