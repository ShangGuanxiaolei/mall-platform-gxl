package com.xquark.service.promotion;

import java.util.List;
import com.xquark.dal.model.PromotionSkuGift;
/**
 * 赠品信息
 * @author Luxiaoling
 *
 */
public interface PromotionSkuGiftService {
	
	List<PromotionSkuGift> getPromotionSkuGiftList(String pCode,String skuCode);
}
