package com.xquark.service.promotion;



import com.xquark.dal.model.mypiece.PromotionTempStock;

import java.util.List;

/**
 * @author: create by daiweiwei
 * @version: v1.0
 * @description:
 * @date: 2018/10/27 14:55
 */
public interface PromotionTempStockService {
    PromotionTempStock selectByPCodeAndSkuId(String skuCode, String pCode);

    //同步redis大会活动库存到DB
    boolean synchroMeetingStock2DB();

    boolean updatePromotionTempStock(List<PromotionTempStock> tempStockList );
}
