package com.xquark.service.promotion;

import com.xquark.dal.model.PromotionConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * promotionConfig服务
 * @author luxiaoling
 *
 */
public interface PromotionConfigService {
 	
	PromotionConfig selectByConfigType(String configType);

    PromotionConfig selectByConfig(Map map);

    PromotionConfig selectBySkuCodeAndConfigName(String skuCode,String configName);

    String selectPcodeBySku(String skuCode);

    List<PromotionConfig> selectListByConfigType(Map map);

    /**
     * 支付前轮循获取银联支付订单成功的数据的轮循时间，临时方案
     * @return
     */
    int getEspCircleTime();

    /**
     * 强制关闭支付轮循
     * @return
     */
    boolean getEspCircleSwitch();
}
