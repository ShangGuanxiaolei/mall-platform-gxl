package com.xquark.service.promotion.impl;

import com.xquark.dal.mapper.PromotionMapper;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.PromotionType;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.pricing.base.PromotionActivityService;
import com.xquark.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by wangxinhua on 17-11-21. DESC: 基础活动service类
 */
@Service
public class PromotionActivityServiceImpl extends BaseServiceImpl implements
    PromotionActivityService {

  private PromotionMapper promotionMapper;

  private ProductService productService;

  @Autowired
  public PromotionActivityServiceImpl(PromotionMapper promotionMapper,
      ProductService productService) {
    this.promotionMapper = promotionMapper;
    this.productService = productService;
  }

  @Override
  public Promotion load(String id) {
    return promotionMapper.selectByPrimaryKey(id);
  }

  @Override
  public boolean save(Promotion promotion) {
    return promotionMapper.insert(promotion) > 0;
  }

  @Override
  public boolean update(Promotion promotion) {
    return promotionMapper.update(promotion) > 0;
  }

  @Override
  public boolean delete(String id) {
    return promotionMapper.deleteLogically(id) > 0;
  }

  @Override
  public Promotion loadByNameAndType(String name, PromotionType type) {
    return promotionMapper.selectByNameAndType(name, type);
  }

  @Override
  public boolean close(String id) {
    return promotionMapper.close(id) > 0;
  }

  @Override
  public Promotion findDateByBatchNo(String batchNo){
    return promotionMapper.findDateByBatchNo(batchNo);
  }

  @Override
  public Promotion findPromotionByDate(String batchNo){
    return promotionMapper.findPromotionByDate(batchNo);
  }

}
