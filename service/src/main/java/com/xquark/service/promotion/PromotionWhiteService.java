package com.xquark.service.promotion;

import com.xquark.dal.model.PromotionWhite;

public interface PromotionWhiteService {

   PromotionWhite selectByPrimaryKey(String id);

   Boolean isExistWhiteList(Long cpid);

   Boolean isExistByPacket(Long cpId);

   Boolean isExistWhiteListBypCode(Long cpId);

   Boolean isExistsByRealFuckingPCode(Long cpId, String pCode);

   boolean havePacketRule(Long cpId);

   boolean haveLotteryRule(Long cpId);

}
