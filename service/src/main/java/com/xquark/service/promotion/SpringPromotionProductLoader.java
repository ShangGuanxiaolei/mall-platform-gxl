package com.xquark.service.promotion;

import com.google.common.collect.ImmutableMap;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.dal.vo.PromotionProductVO;
import com.xquark.service.bargain.impl.PromotionBargainServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.reserve.PromotionReserveServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;

/**
 * @author wangxinhua
 * @date 2019-04-17
 * @since 1.0
 */
public class SpringPromotionProductLoader {

  private static final Logger LOGGER = LoggerFactory.getLogger(SpringPromotionProductLoader.class);

  private static final Map<PromotionType, Class<? extends PromotionProductLoader>> INNER_MAP =
      ImmutableMap.of(
          PromotionType.BARGAIN, PromotionBargainServiceImpl.class,
          PromotionType.FLASHSALE, FlashSalePromotionProductService.class,
          PromotionType.RESERVE, PromotionReserveServiceImpl.class);

  /**
   * 从spring上下文中去除
   *
   * @param type 活动类型
   * @param promotionId 活动id
   * @param productId 商品id
   * @param <T> 活动商品类型
   * @return 活动商品对象
   */
  public static <T extends PromotionProductVO> Optional<T> load(
      PromotionType type, String promotionId, String productId) {
    if (type == null) {
        type = PromotionType.FLASHSALE;
    }
    Class<? extends PromotionProductLoader> clazz = INNER_MAP.get(type);
    if (clazz == null) {
      LOGGER.error("类型 " + type + " 不支持活动加载");
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "内部错误，活动类型不正确");
    }
    @SuppressWarnings("unchecked")
    PromotionProductLoader<T> loader = SpringContextUtil.getBean(clazz);
    return loader.load(promotionId, productId);
  }
}
