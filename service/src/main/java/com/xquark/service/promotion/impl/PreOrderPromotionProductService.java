package com.xquark.service.promotion.impl;

import com.google.common.base.Preconditions;
import com.xquark.dal.mapper.PromotionApplyMapper;
import com.xquark.dal.mapper.PromotionPreOrderMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.model.promotion.PromotionProduct;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.PromotionStatus;
import com.xquark.dal.type.PreOrderApplyStatus;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PreOrderPromotionOrderVO;
import com.xquark.dal.vo.PreOrderPromotionProductVO;
import com.xquark.dal.vo.PreOrderPromotionUserVO;
import com.xquark.dal.vo.PromotionProductVO;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderService;
import com.xquark.service.pricing.base.PromotionActivityService;
import com.xquark.service.pricing.impl.pricing.DiscountPricingResult;
import com.xquark.service.product.ProductService;
import com.xquark.service.promotion.BasePromotionProductService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

import static com.xquark.dal.type.PromotionType.PREORDER;
import static com.xquark.service.pricing.impl.pricing.DiscountPricingResult.IS_PAY_REST_KEY;

/**
 * Created by wangxinhua on 17-11-21. DESC:
 */
@Service
public class PreOrderPromotionProductService extends BasePromotionProductService {

  private PromotionPreOrderMapper promotionPreOrderMapper;

  private OrderService orderService;

  @Autowired
  public PreOrderPromotionProductService(ProductService productService,
      PromotionPreOrderMapper promotionPreOrderMapper,
      PromotionActivityService promotionService,
      PromotionApplyMapper promotionApplyMapper) {
    super(promotionService, productService, promotionApplyMapper);
    this.promotionPreOrderMapper = promotionPreOrderMapper;
  }

  @Override
  public PromotionStatus isValid(PromotionProductVO product) {
    String userId = getCurrentUser().getId();
    String pPid = product.getId();
    boolean isApplied = isApplied(userId, pPid, PREORDER);
    boolean canPay = promotionApplyMapper.canPayRestPreOrder(pPid, userId);
    if (isApplied && !canPay) {
      return PromotionStatus.CANT_PAY_NOW;
    }
    return PromotionStatus.NORMAL;
  }

  @Override
  public boolean inPromotion(String productId) {
    return promotionPreOrderMapper.inPromotion(productId);
  }

  @Override
  public boolean inPromotion(String promotionId, String productId) {
    return promotionPreOrderMapper.exists(promotionId, productId);
  }

  @Override
  public PromotionProduct loadPromotionProduct(String id) {
    return promotionPreOrderMapper.selectByPrimaryKey(id);
  }

  @Override
  public PromotionProductVO loadPromotionProductByPId(String productId) {
    return null;
  }

  @Override
  public PromotionProductVO loadPromotionProductByProductId(String productId) {
    return null;
  }

  @Override
  public PromotionProduct loadPromotionProduct(String promotionId, String productId) {
    return promotionPreOrderMapper.selectByPromotionIdAndProductId(promotionId, productId);
  }

  @Override
  public PromotionProductVO loadPromotionProductVO(String id) {
    return promotionPreOrderMapper.selectVOByPrimaryKey(id);
  }

  @Override
  public List<PromotionPreOrder> listPromotionProducts(String id, String order,
      Direction direction, Pageable pageable) {
    return promotionPreOrderMapper.listPromotionPreOrder(id, order, direction, pageable);
  }

  @Override
  public List<PreOrderPromotionProductVO> listPromotionProductVOs(String id, String order,
      Direction direction, Pageable pageable) {
    return promotionPreOrderMapper.listPromotionPreOrderVO(id, order, direction, pageable);
  }

  @Override
  public Long countProducts(String promotionId) {
    return promotionPreOrderMapper.count(promotionId);
  }

  @Override
  public DiscountPricingResult calculateForDiscount(CartItemVO cartItemVO,
      PromotionProductVO promotionProductVO) {
    DiscountPricingResult result = DiscountPricingResult.emptyResult();
    String pPid = promotionProductVO.getId();
    PreOrderPromotionProductVO preOrderPromotionProductVO = (PreOrderPromotionProductVO) promotionProductVO;
    boolean canPayRest = promotionApplyMapper
        .canPayRestPreOrder(pPid, getCurrentUser().getId());
    BigDecimal discount;
    BigDecimal preOrderPrice = preOrderPromotionProductVO.getPreOrderPrice();
    if (canPayRest) {
      discount = preOrderPromotionProductVO.getDiscount().subtract(preOrderPrice);
      // 付尾款订单标识
      result.addExtraParam(IS_PAY_REST_KEY, true);
    } else {
      discount = preOrderPrice;
    }
    Sku sku = cartItemVO.getSku();
    // 预购商品将sku原价改为折扣价, 不需要折扣
    sku.setPrice(discount);
    sku.setMarketPrice(discount);
    return result;
  }

  /**
   * 更新预购活动状态
   *
   * @param id 申请id
   * @param status 新状态字符串
   * @return 更新结果
   * @throws IllegalArgumentException 如果 {@link PreOrderApplyStatus} 中没有该状态
   */
  @Override
  public boolean updateStatus(String id, String status) {
    PreOrderApplyStatus statusEnum = PreOrderApplyStatus.valueOf(status);
    return promotionApplyMapper.updatePreOrderStatus(id, statusEnum) > 0;
  }

  /**
   * 通过订单id更新预购活动状态
   *
   * @param id 订单id
   * @param status 新状态字符串
   * @return true or false
   * @throws IllegalArgumentException 如果 {@link PreOrderApplyStatus} 中没有该状态
   */
  @Override
  public boolean updateStatusByOrderId(String id, String status) {
    PreOrderApplyStatus statusEnum = PreOrderApplyStatus.valueOf(status);
    Order order = orderService.load(id);
    String orderId = order.getId();
    if (order.isPayRest()) {
      String promotionId = order.getPromotionId();
      String firstPayId = this.getFirstPayOrderId(promotionId, orderId);
      if (firstPayId == null) {
        logger.error("无法通过订单 {} 查询到预购订单", order);
        return false;
      }
      // 预购则更新预购订单状态
      id = firstPayId;
    }
    return promotionApplyMapper.updatePreOrderStatusByOrderId(id, statusEnum) > 0;
  }

  @Override
  public boolean updateStock(String orderId, int qty) {
    return promotionPreOrderMapper.updateStock(orderId, qty) > 0;
  }

  @Override
  public PromotionApply loadApply(String promotionProductId, String userId) {
    return promotionApplyMapper.selectByPromotionProductIdAndUserId(promotionProductId,
        userId, PromotionType.PREORDER);
  }

  @Override
  public BigDecimal getPromotionPrice(String promotionProductId, String userId) {
    PreOrderPromotionProductVO product = (PreOrderPromotionProductVO)
        this.loadPromotionProductVO(promotionProductId);
    if (product == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "活动商品不存在");
    }
    boolean canPayRest = this.promotionApplyMapper
        .canPayRestPreOrder(promotionProductId, userId);
    // 付尾款订单返回尾款价格, 否则返回预购价格
    if (canPayRest) {
      return product.getDiscount().subtract(product.getPreOrderPrice());
    }
    return product.getPreOrderPrice();
  }

  /**
   * 查询所有商品数量
   *
   * @param isAvailable 是否只查询有效的商品
   * @return 商品数量
   */
  public Long countAllProducts(Boolean isAvailable) {
    return promotionPreOrderMapper.countAll(isAvailable);
  }

  /**
   * 保存活动及活动商品
   *
   * @param promotion 活动
   * @param promotionProduct 活动商品关联对象
   * @return true or false
   */
  @Transactional
  public boolean savePromotionProduct(Promotion promotion,
      PromotionPreOrder promotionProduct) {
    Preconditions.checkNotNull(promotion);
    Preconditions.checkNotNull(promotionProduct);

    boolean promotionSaveResult;
    boolean promotionProductSaveResult;

    String promotionProductId = promotionProduct.getId();
    String productId = promotionProduct.getProductId();
    if (!productService.exists(productId)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品不存在");
    }

    if (StringUtils.isBlank(promotionProductId)) {
      promotion.setType(PromotionType.PREORDER);
      promotionSaveResult = promotionService.save(promotion);
      String promotionId = promotion.getId();
      promotionProduct.setPromotionId(promotionId);
      promotionProductSaveResult = savePreOrder(promotionProduct);
    } else {
      if (promotionService.load(promotionProduct.getPromotionId()) == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "要更新的活动不存在");
      }
      promotionSaveResult = promotionService.update(promotion);
      promotionProductSaveResult = updatePreOrder(promotionProduct);
    }
    return promotionSaveResult && promotionProductSaveResult;
  }

  /**
   * 查询用户vo
   *
   * @param id 活动商品id
   * @param userId 用户id
   * @return 活动商品用户vo
   */
  public PreOrderPromotionUserVO loadUserVO(String id, String userId) {
    return promotionPreOrderMapper.selectUserVOByPrimaryKey(id, userId);
  }

  /**
   * 保存关联对象
   *
   * @param preOrder 活动商品关联对象
   * @return true or false
   */
  public boolean savePreOrder(PromotionPreOrder preOrder) {
    return promotionPreOrderMapper.insert(preOrder) > 0;
  }

  /**
   * 更新关联对象
   *
   * @param preOrder 活动商品关联对象
   * @return true or false
   */
  public boolean updatePreOrder(PromotionPreOrder preOrder) {
    return promotionPreOrderMapper.updateByPrimaryKeySelective(preOrder) > 0;
  }

  /**
   * 查询所有限购活动商品
   *
   * @param order 排序 默认按销量及时间排序
   * @param direction 排序方向
   * @param pageable 分页对象
   * @param isAvailable 是否在当前时间内有效, 过滤掉失效的商品
   * @return 限购商品集合
   */
  public List<PreOrderPromotionProductVO> listAllPromotionProductVOs(String order,
      Direction direction, Pageable pageable, Boolean isAvailable) {
    return promotionPreOrderMapper
        .listAllPromotionPreOrderVO(order, direction, pageable, isAvailable);
  }

  /**
   * 查询所有限购活动商品, 默认按销量及时间排序
   *
   * @param pageable 分页对象
   * @return 限购商品集合
   */
  public List<PreOrderPromotionProductVO> listAllPromotionProductVOs(Pageable pageable,
      Boolean isAvailable) {
    return listAllPromotionProductVOs(null, null, pageable, isAvailable);
  }

  /**
   * 查询所有订单关联的预购活动申请列表
   *
   * @param userId 买家id, 可不传，查询所有订单
   * @param order 排序
   * @param direction 方向
   * @param pageable 分页
   * @return list of {@link PreOrderPromotionOrderVO} or empty list
   */
  public List<PreOrderPromotionOrderVO> listOrderVO(String userId, String order,
      Direction direction, Pageable pageable) {
    return promotionPreOrderMapper.listOrderVO(userId, order, direction, pageable);
  }

  /**
   * 查询所有订单关联的预购活动申请列表
   *
   * @param order 排序
   * @param direction 方向
   * @param pageable 分页
   * @return list of {@link PreOrderPromotionOrderVO} or empty list
   */
  public List<PreOrderPromotionOrderVO> listOrderVO(String order, Direction direction,
      Pageable pageable) {
    return listOrderVO(null, order, direction, pageable);
  }

  /**
   * 查询订单关联的预购活动列表申请数量
   *
   * @param userId 买家id 可不传，查询所有订单
   * @return 订单数量
   */
  public Long countOrderVO(String userId) {
    return promotionPreOrderMapper.countOrderVO(userId);
  }

  /**
   * 查询订单关联的预购活动列表申请数量
   *
   * @return 订单数量
   */
  public Long countOrderVO() {
    return countOrderVO(null);
  }

  /**
   * 查询预购订单id
   *
   * @param promotionId 活动id
   * @param orderId 尾款订单id
   * @return 预购订单id || null
   */
  public String getFirstPayOrderId(String promotionId, String orderId) {
    Long id = promotionApplyMapper.selectFirstPayOrderId(promotionId, orderId);
    return id == null ? null : IdTypeHandler.encode(id);
  }

  @Override
  public PromotionProductVO loadPromotionProductSaleZoneByPId(String productId) {
    return null;
  }
}
