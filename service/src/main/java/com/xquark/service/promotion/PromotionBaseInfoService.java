package com.xquark.service.promotion;

import com.xquark.dal.model.Promotion4ProductSalesVO;
import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.PromotionNameListVO;

import java.util.List;

public interface PromotionBaseInfoService {
	
	List<PromotionBaseInfo> selectBySkuCode(String skuCode);

	List<PromotionBaseInfo> selectBySkuCodeAndType(String skuCode, String pType);

	Integer selectBuyAmounts(String pCode, String userId);

	Integer selectBuyLimits(String pCode);

	List<PromotionBaseInfo> selectByProductId(String productId);

	List<PromotionBaseInfo> selectByProductIdAndType(String productId, String pType);
	/**
	 * 查询上架活动（包括结束的）
	 * @param skuCode
	 * @return
	 */
	List<PromotionBaseInfo> selectEffectiveBySkuCode (String skuCode);

	PromotionBaseInfo selectByPtype(String pType);

	List<PromotionBaseInfo> selectByPush();

	List<PromotionBaseInfo> selectByPushEnd();

	PromotionBaseInfo selectByPCode(String pCode);

	PromotionBaseInfo selectPromotionByPCode(String pCode);

	/**
	 * true表示当前时间小于等于活动结束时间
	 * @param pType
	 * @param pCode
	 * @return
	 */
	Boolean isWhthInTime(String pType,String pCode);

	Integer selectIsWithUsedPoint(String pCode);

	/**
	 * 查看是否超过最大购买限制
	 * @param pCode
	 * @param userId
	 * @return
	 */
    Boolean selectIsOverBuyLimitByPcodeAndUserId(String pCode, String userId);

	/**
	 * 通过skuId校验是否是在白名单中
	 * @param skuId
	 * @return true是白名单
	 */
	boolean checkIsWhiteBySkuId(String skuId, Long cpId);

	/**
	 * 下拉框查询有效活动名和pcode
	 * @return
	 */
    List<PromotionNameListVO> selectPromotionName();

	/**
	 * 查询折扣促销活动
	 * @return
	 */
	List<Promotion4ProductSalesVO> selectSalesPromotion(String pCode);

	List<Promotion4ProductSalesVO> selectSalesPromotionByProductId(String productId);

	List<Promotion4ProductSalesVO> selectExsitSalesPromotionByProductId(String productId);

	List<Promotion4ProductSalesVO> selectSalesPromotionBySkuId(String skuId);


	List<Promotion4ProductSalesVO> selectGrandSalesPromotion(String grandSaleId);

}
