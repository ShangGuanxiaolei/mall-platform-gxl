package com.xquark.service.promotion.impl;

import com.xquark.dal.model.Order;
import com.xquark.dal.status.IntegerOperation;
import com.xquark.dal.type.OrderSortType;
import com.xquark.service.order.OrderService;
import com.xquark.service.promotion.PromotionOrderObServer;
import com.xquark.service.promotion.PromotionProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 17-12-13. DESC:
 */
@Component
public class FlashSaleOrderObserver implements PromotionOrderObServer {

  private static final Logger logger = LoggerFactory.getLogger(FlashSaleOrderObserver.class);

  private OrderService orderService;

  private PromotionProductService flashSalePromotionProductService;

  @Autowired
  public void setOrderService(OrderService orderService) {
    this.orderService = orderService;
  }

  @Autowired
  @Qualifier(value = "flashSalePromotionProductService")
  public void setFlashSalePromotionProductService(
      PromotionProductService flashSalePromotionProductService) {
    this.flashSalePromotionProductService = flashSalePromotionProductService;
  }

  @Override
  public void onSubmit(Order order) {
    if (order.getOrderType() != null && OrderSortType.FLASHSALE == order.getOrderType()) {
      boolean result = flashSalePromotionProductService.updateStock(order.getId(),
          IntegerOperation.PLUS);
      logger.info("订单 {} 提交时修改预购商品库存 {}", order.getOrderNo(), result ? "成功" : "失败");
    }
  }

  @Override
  public void onCancel(Order order) {
    if (order.getOrderType() != null && OrderSortType.FLASHSALE == order.getOrderType()) {
      boolean result = flashSalePromotionProductService.updateStock(order.getId(),
          IntegerOperation.MINUS);
      logger.info("订单 {} 取消时修改预购商品库存 {}", order.getOrderNo(), result ? "成功" : "失败");
    }
  }

  @Override
  public String toString() {
    return "FlashSaleOrderObserver{}";
  }

}
