package com.xquark.service.promotion;
/**
 * 邀请码服务
 * @author LuXiaoLing
 *
 */
public interface PromotionInviteCodeService {
	/**
	 * 邀请码校验
	 * @param code
	 * @param pCode
	 * @return
	 */
	int validate(String code,String pCode);
	/**
	 * 邀请码cpid关系更新
	 * @param code
	 * @param cpid
	 * @param pCode
	 * @return
	 */
	int updateRelation(String code, String cpid , String pCode);

	/**
	 * 查询用户邀请码
	 * @param cpId
	 * @param pCode
	 * @return
	 */
	String selectInviteCode(String cpid,String pCode);
}
