package com.xquark.service.promotion;

import com.xquark.dal.vo.PromotionProductVO;

import java.util.Optional;

/**
 * @author wangxinhua
 * @date 2019-04-16
 * @since 1.0
 */
public interface PromotionProductLoader<T extends PromotionProductVO> {

    /**
     * 根据活动id跟商品id获取活动商品vo对象
     * @param promotionId 活动id
     * @param productId 商品id
     * @return 商品vo对象
     */
    Optional<T> load(String promotionId, String productId);

}
