package com.xquark.service.promotion.impl;

import com.xquark.dal.mapper.PromotionBaseInfoMapper;
import com.xquark.dal.mapper.PromotionWhitelistMapper;
import com.xquark.dal.model.Promotion4ProductSalesVO;
import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.PromotionNameListVO;
import com.xquark.service.promotion.PromotionBaseInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PromotionBaseInfoServiceImpl implements PromotionBaseInfoService {
    private static final Long WHITELIST_FLAG_NUM = 9999999L;
    private static final String WHITELIST_DEFAULT_PCODE = "piece";
  @Autowired private PromotionBaseInfoMapper baseInfoMapper;
  @Autowired
  private PromotionWhitelistMapper promotionWhitelistMapper;
  @Override
  public List<PromotionBaseInfo> selectBySkuCode(String skuCode) {
    return this.selectBySkuCodeAndType(skuCode, null);
  }

  public List<PromotionBaseInfo> selectBySkuCodeAndType(String skuCode, String pType) {
    return baseInfoMapper.selectBySkuCode(skuCode, pType);
  }

  @Override
  public List<PromotionBaseInfo> selectEffectiveBySkuCode(String skuCode) {
    return baseInfoMapper.selectEffectiveBySkuCode(skuCode);
  }

  @Override
  public Integer selectIsWithUsedPoint(String pCode) {
    return baseInfoMapper.selectIsWithUsedPoint(pCode);
  }

  @Override
  public Boolean selectIsOverBuyLimitByPcodeAndUserId(String pCode, String userId) {
    return baseInfoMapper.selectIsOverBuyLimit(pCode, userId);
  }


  @Override
  public boolean checkIsWhiteBySkuId(String skuId, Long cpId) {
    List<PromotionBaseInfo> promotionBaseInfos = this.baseInfoMapper.selectBySkuCode(skuId, null);
    if(promotionBaseInfos == null || promotionBaseInfos.isEmpty()){
      //不是拼团或大会商品
      return true;
    }

    String pCode = promotionBaseInfos.get(0).getpCode();
    if("piece".equals(promotionBaseInfos.get(0).getpType())){
      pCode = WHITELIST_DEFAULT_PCODE;
    }
    boolean isOpen = promotionWhitelistMapper.checkMettingIsOpen(pCode, WHITELIST_FLAG_NUM);
    boolean isWhite = promotionWhitelistMapper.checkIsWhiteExist(pCode, cpId);
    if(isOpen){
      if(isWhite){
        return true;
      }else{
        return false;
      }
    }
    return true;
  }

  @Override
  public Integer selectBuyAmounts(String pCode, String userId) {
    return baseInfoMapper.selectBuyAmounts(pCode, userId);
  }

  @Override
  public Integer selectBuyLimits(String pCode) {
    return baseInfoMapper.selectBuyLimits(pCode);
  }

  @Override
  public List<PromotionBaseInfo> selectByProductId(String productId) {
    return baseInfoMapper.selectByProductId(productId, null);
  }

  public List<PromotionBaseInfo> selectByProductIdAndType(String productId, String pType) {
    return baseInfoMapper.selectByProductId(productId, pType);
  }

  @Override
  public PromotionBaseInfo selectByPtype(String pType) {
    return baseInfoMapper.selectByPtype(pType);
  }

  @Override
  public List<PromotionBaseInfo> selectByPush() {
    return baseInfoMapper.selectByPush();
  }

  @Override
  public List<PromotionBaseInfo> selectByPushEnd() {
    return baseInfoMapper.selectByPushEnd();
  }

  @Override
  public PromotionBaseInfo selectByPCode(String pCode) {
    return baseInfoMapper.selectByPCode(pCode);
  }

	@Override
	public PromotionBaseInfo selectPromotionByPCode(String pCode) {
		return baseInfoMapper.selectByPromotionPCode(pCode);
	}

  @Override
  public Boolean isWhthInTime(String pType, String pCode) {
    return baseInfoMapper.isWhthInTime(pType, pCode);
  }

  @Override
  public List<PromotionNameListVO> selectPromotionName() {
    return baseInfoMapper.selectPromotionName();
  }

  @Override
  public List<Promotion4ProductSalesVO> selectSalesPromotion(String pCode) {
    return baseInfoMapper.selectSalesPromotion(pCode);
  }

  @Override
  public List<Promotion4ProductSalesVO> selectSalesPromotionByProductId(String productId) {
    return baseInfoMapper.selectSalesPromotionByProductId(productId);
  }

  @Override
  public List<Promotion4ProductSalesVO> selectExsitSalesPromotionByProductId(String productId) {
    return baseInfoMapper.selectExsitSalesPromotionByProductId(productId);
  }

  @Override
  public List<Promotion4ProductSalesVO> selectSalesPromotionBySkuId(String skuId) {
    return baseInfoMapper.selectSalesPromotionBySkuId(skuId);
  }

  @Override
  public List<Promotion4ProductSalesVO> selectGrandSalesPromotion(String grandSaleId) {
    return baseInfoMapper.selectGrandSalesPromotion(grandSaleId);
  }

}
