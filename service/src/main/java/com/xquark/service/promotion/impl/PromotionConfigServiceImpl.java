package com.xquark.service.promotion.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.mapper.PromotionConfigMapper;
import com.xquark.dal.model.PromotionConfig;
import com.xquark.service.promotion.PromotionConfigService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
public class PromotionConfigServiceImpl implements PromotionConfigService {
	@Autowired
	private PromotionConfigMapper configMapper;

	@Override
	public PromotionConfig selectByConfigType(String configType) {
		return configMapper.selectByConfigType(configType);
	}

	@Override
	public PromotionConfig selectByConfig(Map map) {

		return configMapper.selectByConfig(map);
	}

	@Override
	public PromotionConfig selectBySkuCodeAndConfigName(String skuCode, String configName) {
		return configMapper.selectBySkuCodeAndConfigName(skuCode,configName);
	}



	@Override
	public String selectPcodeBySku(String skuCode) {
		return configMapper.selectPcodeBySku(skuCode);
	}

	@Override
	public List<PromotionConfig> selectListByConfigType(Map map) {
		return configMapper.selectListByConfigType(map);
	}

	/**
	 * 支付前轮循获取银联支付订单成功的数据的轮循时间，临时方案
	 * @return
	 */
	@Override
	public int getEspCircleTime() {
		//默认轮循时间 秒
		int result = 2;
		RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
		String redisValue = redisUtils.get("hw:pay_esp_circle_time");
		if (StringUtils.isNotBlank(redisValue))
			result = Integer.valueOf(redisValue);

		Map<String,String> map = new HashMap<>();
		map.put("configType", "pay");
		map.put("configName", "pay_esp_circle_time");
		PromotionConfig p = configMapper.selectByConfig(map);

		if(Objects.isNull(p) || StringUtils.isBlank(p.getConfigValue())){
			return result;
		}
		redisUtils.set("hw:pay_esp_circle_time", p.getConfigValue(), 5, TimeUnit.MINUTES);
		result = Integer.valueOf(p.getConfigValue());
		return result;
	}

	/**
	 * 强制关闭支付轮循
	 * @return
	 */
	@Override
	public boolean getEspCircleSwitch() {
		Map<String,String> map = new HashMap<>();
		RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);

		//优先取redis标记
		String redisFlag = redisUtils.get("hw:pay_esp_circle_switch");
		if(StringUtils.isNotBlank(redisFlag))
			return true;

		//查库
		map.put("configType", "pay");
		map.put("configName", "pay_esp_circle_switch");
		PromotionConfig p = configMapper.selectByConfig(map);
		if(Objects.isNull(p) || StringUtils.isBlank(p.getConfigValue()))
			return false;
		if("true".equals(p.getConfigValue())){
			redisUtils.set("hw:pay_esp_circle_switch", "true", 2, TimeUnit.MINUTES);
			return true;
		}
		return false;
	}

}
