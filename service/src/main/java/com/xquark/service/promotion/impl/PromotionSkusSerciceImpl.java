package com.xquark.service.promotion.impl;

import java.util.ArrayList;
import java.util.List;

import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.PromotionConstants;
import com.xquark.dal.mapper.PromotionBaseInfoMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.PromotionConfig;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.SkuExtend;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.promotion.PromotionConfigService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xquark.dal.mapper.PromotionSkusMapper;
import com.xquark.dal.vo.PromotionSkuVo;
import com.xquark.service.promotion.PromotionSkusService;

@Service("promotionSkusSercice")
public class PromotionSkusSerciceImpl implements PromotionSkusService {
	@Autowired
	private PromotionSkusMapper skusMapper;
	@Autowired
	private SkuMapper skuMapper;
	@Autowired
	private PromotionBaseInfoMapper promotionBaseInfoMapper;
	@Autowired
	PromotionBaseInfoService promotionBaseInfoService;
	@Override
	public List<PromotionSkuVo> getPromotionSkus(String skuCode) {

		return skusMapper.getPromotionSkus(skuCode);
	}

	private Logger logger = LoggerFactory.getLogger(PromotionSkusSerciceImpl.class);

	@Override
	public List<PromotionSkuVo> getPromotionSkusByPcode(String pCode) {
		return skusMapper.getPromotionSkusByPcode(pCode);
	}

	@Override
	public List<Integer> getPSkuUseableNum(String skuCode, String pType) {
		return skusMapper.getPSkuUseableNum(skuCode, pType);
	}

	@Override
	public Integer getUseableNumBySkuCodeAndPcode(String skuCode, String pCode) {
		return skusMapper.getUseableNumBySkuCodeAndPcode(skuCode, pCode);
	}



	@Override
	public boolean hasEnoughStock(List<String> promotionSkuIds) {

		boolean isEnough = true;
		if (CollectionUtils.isNotEmpty(promotionSkuIds)) {
			for (String skuId : promotionSkuIds) {
				Sku sku = skuMapper.selectByPrimaryKey(skuId);
				if (null == sku) {
					continue;
				}
				PromotionConfig lock_type_config = promotionConfigService.selectByConfigType("lock_type");
				String lock_type = "";
				if (null == lock_type_config) {
					lock_type = "DB";
				} else {
					lock_type = lock_type_config.getConfigValue();
				}
				if ("DB".equals(lock_type)) {
					return hasEnoughDbStock(sku);

				} else {
					return hasEnoughRedisStock(skuId);
				}
//				//查询大会商品的库存
//				List<Integer> stockLst = this.getPSkuUseableNum(sku.getSkuCode(), "meeting");
//				//没有活动库存记录，该活动商品库存已经被清理
//				if (null == stockLst) {
//					isEnough = false;
//					return isEnough;
//				}
//				//库存不足退出
//				for (Integer intStock : stockLst) {
//					if (intStock <= 0) {
//						isEnough = false;
//						return isEnough;
//					}
//				}
			}
		}
		return isEnough;
	}

	@Autowired
	private PromotionConfigService promotionConfigService;

	private boolean hasEnoughDbStock (Sku sku) {

		boolean isEnough = true;
		//查询大会商品的库存
		List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService.selectBySkuCode(sku.getSkuCode());
		if(CollectionUtils.isNotEmpty(promotionBaseInfos)){
			String pCode = promotionBaseInfos.get(0).getpCode();
			//根据skuCode和pCode查询库存
			Integer useableNum = this.getUseableNumBySkuCodeAndPcode(sku.getSkuCode(), pCode);
			if(null!=useableNum){
				if(useableNum>0){
					return isEnough;
				}else{
					isEnough = false;
					return isEnough;
				}
			}else{
				isEnough = false;
				return isEnough;
			}

		}else{
			//没有活动库存记录，该活动商品库存已经被清理
			isEnough = false;
			return isEnough;
		}

//		List<Integer> stockLst = this.getPSkuUseableNum(sku.getSkuCode(), );
//		//没有活动库存记录，该活动商品库存已经被清理
//		if (null == stockLst) {
//			isEnough = false;
//			return isEnough;
//		}
//		//库存不足退出
//		for (Integer intStock : stockLst) {
//			if (intStock <= 0) {
//				isEnough = false;
//				return isEnough;
//			}
//		}
//		return isEnough;
	}

	private boolean hasEnoughRedisStock (String skuId) {
		Sku sku = skuMapper.selectByPrimaryKey(skuId);
		//PromotionConfig pc = promotionConfigService.selectByConfigType("meeting_pid");
		List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoMapper.selectEffectiveBySkuCode(sku.getSkuCode());
		String promotionId = "meeting";
		if (CollectionUtils.isNotEmpty(promotionBaseInfos)) {
			promotionId = promotionBaseInfos.get(0).getpCode();
		}

		logger.info("hasEnoughRedisStock,promotionId=" + promotionId);
		RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
		String redisKey = PromotionConstants.getCommonAmountKey(promotionId, sku.getSkuCode());
		logger.info("hasEnoughRedisStock,stock=" + redisKey);

		Long stock = 0L;
		if (null != redisUtils.get(redisKey)) {
			stock = (long) redisUtils.get(redisKey);
		}
		logger.info("hasEnoughRedisStock,stock=" + stock);
		if (stock <= 0) {
			return false;
		}
		return true;
	}

	/**
	 * 过滤活动商品
	 * @param skuIds
	 * @return
	 */
	@Override
	public List<String> getPromotionSkuIds (List<String> skuIds) {
		List<String> promotionSkuIds = new ArrayList<String>();
		for (String skuId : skuIds) {
			Sku sku = skuMapper.selectByPrimaryKey(skuId);
			SkuExtend skuEx = new SkuExtend();
			BeanUtils.copyProperties(sku, skuEx);

			//检查这个商品有无活动，没有则认为是没有赠品
			List<PromotionSkuVo> promotionSkuVos = this.getPromotionSkus(skuEx.getSkuCode());
			if (CollectionUtils.isEmpty(promotionSkuVos)) {
				continue;
			}
			promotionSkuIds.add(sku.getId());
		}
		return promotionSkuIds;
	}

	@Override
	public PromotionSkuVo selectProSkuBySkuCodeAndPcode(String skuCode,String pCode) {
		return skusMapper.selectProSkuBySkuCodeAndPcode(skuCode, pCode);
	}

    @Override
    public List<String> selectPCodeBySkuCodeList(List<String> promotionskus) {
        return skusMapper.selectPCodeBySkuCodeList(promotionskus);
    }

    @Override
	public String querySkuCodeByProductid(String productId,String pCode) {
		return skusMapper.querySkuCodeByProductid(productId,pCode);
	}

	@Override
	public List<PromotionSkuVo> getPromotionSkusAllStatus(String skuCode) {
		return skusMapper.getPromotionSkusAllStatus(skuCode);
	}

	@Override
	public List<PromotionSkuVo> getPromotionByProductId(String productId){
		return skusMapper.getPromotionByProductId(productId);
	}

	@Override
	public PromotionSkuVo getProductSalesSkus(String skuCode, String pCode) {
		return skusMapper.getProductSalesSkus(skuCode,pCode);
	}

}
