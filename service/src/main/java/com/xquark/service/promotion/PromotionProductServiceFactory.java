package com.xquark.service.promotion;

import com.xquark.dal.type.PromotionType;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.dal.vo.PromotionProductVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;

/**
 * Created by wangxinhua on 17-11-21. DESC:
 */
public class PromotionProductServiceFactory {

  private static final Logger logger = LoggerFactory
      .getLogger(PromotionProductServiceFactory.class);

  /**
   * 根据类型获取对应活动service实现
   *
   * @param type 活动类型
   * @return 活动Service 若service不存在则返回null
   */
  public static PromotionProductService loadByType(PromotionType type) {
    PromotionProductService service = null;
    try {
      service = (PromotionProductService) SpringContextUtil.getBean(type.getServiceName());
    } catch (BeansException e) {
      logger.error("spring上下文中找不到service: {}", type.getServiceName());
    }
    return service;
  }

  /**
   * 根据工厂获取的service返回商品
   *
   * @param promotionType 活动类型
   * @param promotionProductId 活动商品id
   * @return 查询到的PromotionProduct
   */
  public static PromotionProductVO loadPromotionProduct(PromotionType promotionType,
      String promotionProductId) {
    PromotionProductService service = loadByType(promotionType);
    return service.loadPromotionProductVO(promotionProductId);
  }

}
