package com.xquark.service.promotion.impl;

import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.PromotionConstants;
import com.xquark.dal.mapper.PromotionSkusMapper;
import com.xquark.dal.mapper.StockCheckMapper;
import com.xquark.dal.model.mypiece.PromotionTempStock;
import com.xquark.dal.vo.PromotionSkuVo;
import com.xquark.service.promotion.PromotionTempStockService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: create by daiweiwei
 * @version: v1.0
 * @description:
 * @date: 2018/10/27 14:56
 */
@Service
public class PromotionTempStockServiceImpl implements PromotionTempStockService{
    @Autowired
    private StockCheckMapper stockCheckMapper;

    @Autowired
    private PromotionSkusMapper promotionSkusMapper;
    @Override
    public PromotionTempStock selectByPCodeAndSkuId(String skuCode, String pCode) {
        return null;
    }
    /**
     * 功能描述: 同步活动库存
     * @author Luxiaoling
     * @date 2019/1/2 11:20
     * @param
     * @return  boolean
     */
    @Override
    public boolean synchroMeetingStock2DB() {

        boolean flag = false;
        RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
        //获取需要同步redis库存到db的活动商品
        List<PromotionSkuVo> promotionSkusByPcode = promotionSkusMapper.getPromotionSkuStockToDB();

        for (PromotionSkuVo promotionSkuVo : promotionSkusByPcode) {
            //获取redis的key 规则 pCode_skuCode:amount
            String redisKey = PromotionConstants.getCommonAmountKey(promotionSkuVo.getpCode(), promotionSkuVo.getSkuCode());

            PromotionTempStock vo = new PromotionTempStock();
            vo.setSku_code(promotionSkuVo.getSkuCode());
            vo.setP_code(promotionSkuVo.getpCode());
            if (null != redisUtils.get(redisKey)) {
                vo.setP_usable_sku_num(redisUtils.get(redisKey));
            }else{
                //redis没有值就跳过
                continue;
            }
            if(vo != null && StringUtils.isNotBlank(vo.getSku_code()) && StringUtils.isNotBlank(vo.getP_code())){
                flag = this.stockCheckMapper.updateUsableStock(vo);
            }
        }
        return flag;
    }

    @Override
    public boolean updatePromotionTempStock(List<PromotionTempStock> tempStockList) {
        return stockCheckMapper.updateStockList(tempStockList);
    }
}
