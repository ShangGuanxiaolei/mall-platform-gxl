package com.xquark.service.promotion.impl;

import com.google.common.base.Preconditions;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.PromotionConstants;
import com.xquark.dal.mapper.PromotionApplyMapper;
import com.xquark.dal.mapper.PromotionFlashSaleMapper;
import com.xquark.dal.mapper.PromotionPgPriceMapper;
import com.xquark.dal.mapper.PromotionTitleMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.model.promotion.PromotionProduct;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.PromotionStatus;
import com.xquark.dal.type.CareerLevelType;
import com.xquark.dal.type.FlashSaleApplyStatus;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.FlashSalePromotionOrderVO;
import com.xquark.dal.vo.FlashSalePromotionProductVO;
import com.xquark.dal.vo.FlashSalePromotionUserVO;
import com.xquark.dal.vo.PromotionProductVO;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pricing.base.PromotionActivityService;
import com.xquark.service.pricing.impl.pricing.DiscountPricingResult;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.promotion.BasePromotionProductService;
import com.xquark.service.promotion.PromotionProductLoader;
import com.xquark.utils.DynamicPricingUtil;
import io.vavr.Tuple2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.xquark.dal.type.PromotionType.FLASHSALE;

/**
 * Created by wangxinhua on 17-11-23. DESC:
 */
@Service
public class FlashSalePromotionProductService extends BasePromotionProductService
        implements PromotionProductLoader<FlashSalePromotionProductVO> {

    private PromotionFlashSaleMapper promotionFlashSaleMapper;

    private PromotionTitleMapper promotionTitleMapper;

    private PromotionPgPriceMapper pgPriceMapper;

    @Autowired
    public FlashSalePromotionProductService(
            PromotionActivityService promotionService,
            ProductService productService,
            PromotionFlashSaleMapper promotionFlashSaleMapper,
            PromotionApplyMapper promotionApplyMapper,
            PromotionTitleMapper promotionTitleMapper,
            PromotionPgPriceMapper pgPriceMapper) {
        super(promotionService, productService, promotionApplyMapper);
        this.promotionFlashSaleMapper = promotionFlashSaleMapper;
        this.promotionTitleMapper = promotionTitleMapper;
        this.pgPriceMapper = pgPriceMapper;
    }

    @Override
    public boolean inPromotion(String productId) {
        return promotionFlashSaleMapper.inPromotion(productId);
    }

    @Override
    public boolean inPromotion(String promotionId, String productId) {
        return promotionFlashSaleMapper.exists(promotionId, productId);
    }

    @Override
    public PromotionProduct loadPromotionProduct(String id) {
        return promotionFlashSaleMapper.selectByPrimaryKey(id);
    }

    @Override
    public PromotionProductVO loadPromotionProductByPId(String productId) {
        FlashSalePromotionProductVO result = promotionFlashSaleMapper.selectVOByProductId(productId, true);
        if (result == null) {
            return null;
        }

        syncFromRedis(result, true);

        return result;
    }

    @Override
    public PromotionProductVO loadPromotionProductByProductId(String productId) {
        FlashSalePromotionProductVO result = promotionFlashSaleMapper.selectPromotionProductByProductId(productId, true);
        if (result == null) {
            return null;
        }
        syncFromRedis(result, true);
        return result;
    }

    private void syncFromRedis(FlashSalePromotionProductVO result, boolean dynamic) {
        final String productId = result.getProductId();
        final String promotionId = result.getPromotionId();
        final RedisUtils<Long> redisUtils = RedisUtilFactory.getInstance(Long.class);
        final String flashSaleLimitKey = PromotionConstants.getFlashSaleLimitAmountKey(promotionId, productId);
        final Long limitAmount = redisUtils.get(flashSaleLimitKey);

        final List<PromotionPgPrice> prices =
                pgPriceMapper.listByPromotionIdAndProductId(promotionId, productId, FLASHSALE);
        if (CollectionUtils.isEmpty(prices)) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动价格体系未设置");
        }
        // 设置价格体系中价格最低的
        // TODO 去掉DB层面的关联查询
        prices.stream().min(Comparator.comparing(PromotionPgPrice::getPromotionPrice)
                .thenComparing(PromotionPgPrice::getConversionPrice))
                .ifPresent(result::setPricing);

        PromotionPgPrice pricing = result.getPricing();
        if (pricing != null && dynamic) {
            DynamicPricingUtil.rePricing(pricing, (User) getCurrentUser());
        }

        final String userId = getCurrentUser().getId();
        Long totalBuyCount;
        Tuple2<Long, Long> amt;
        try {
            // TODO redis保存优化为对象，减少redis的读取次数
            // 所有sku的购买数量总和
            List<String> keys = prices.stream().map(PromotionPgPrice::getSkuId)
                    .map(IdTypeHandler::encode)
                    .map(skuId -> PromotionConstants.getUserAmountKey(userId, promotionId, skuId))
                    .collect(Collectors.toList());
            totalBuyCount = redisUtils.multiGet(keys).stream()
                    .reduce(Long::sum).orElse(0L);
            amt = prices.stream()
                    // 映射出key值
                    .map(p -> new Tuple2<>(PromotionConstants.getFlashSaleAmountKey(promotionId, p.getSkuId()),
                            PromotionConstants.getFlashSaleTotalAmountKey(promotionId, p.getSkuId())))
                    // 通过key去redis取值
                    .map(p -> new Tuple2<>(redisUtils.get(p._1), redisUtils.get(p._2)))
                    // 过滤掉卖完了的商品, 并且取出库存最少的值 (销量最大)
                    .filter(p -> p._1 > 0)
                    .min(Comparator.comparing(Tuple2::_1))
                    // 默认值显示剩余库存0件，总库存1件，前端显示百分比为100
                    .orElse(new Tuple2<>(0L, 1L));
        } catch (Exception e) {
            log.error("redis同步库存出错: ", e);
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "出错了，请稍后再试");
        }

        // 取多规格最大值作为商品属性
        Long maxAmt = amt._1;
        Long maxTotalAmt = amt._2;
        if (limitAmount == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "库存配置不同步");
        }
        // 从redis中取库存
        result.setAmount(maxAmt);
        result.setLimitAmount(limitAmount);
        result.setSales(maxTotalAmt - maxAmt);

        result.getPromotion().setBuyLimit(limitAmount.intValue());
        result.getPromotion().setBuyCount(totalBuyCount.intValue());
    }

    @Override
    public PromotionProductVO loadPromotionProductSaleZoneByPId(String productId) {
        FlashSalePromotionProductVO result = promotionFlashSaleMapper.
                selectVOBySaleZoneProductId(productId, true);
        if (result == null) {
            return null;
        }
        syncFromRedis(result, true);
        return result;
    }

    @Override
    public PromotionProduct loadPromotionProduct(String promotionId, String productId) {
        return promotionFlashSaleMapper.selectByPromotionIdAndProductId(promotionId, productId);
    }

    @Override
    public PromotionProductVO loadPromotionProductVO(String id) {
        FlashSalePromotionProductVO result = promotionFlashSaleMapper.selectVOByPrimaryKey(id);
        if (result == null) {
            return null;
        }
        syncFromRedis(result, true);
        return result;
    }

    @Override
    public List<PromotionFlashSale> listPromotionProducts(String id, String order,
                                                          Direction direction, Pageable pageable) {
        return promotionFlashSaleMapper.listPromotionFlashSale(id, order, direction, pageable);
    }

    @Override
    public List<FlashSalePromotionProductVO> listPromotionProductVOs(String id, String order,
                                                                     Direction direction, Pageable pageable) {
        return promotionFlashSaleMapper.listPromotionFlashSaleVO(id, order, direction, pageable);
    }

    @Override
    public Long countProducts(String promotionId) {
        return promotionFlashSaleMapper.count(promotionId);
    }

    @Override
    public PromotionStatus isValid(PromotionProductVO product) {
        return PromotionStatus.NORMAL;
    }

    @Override
    public DiscountPricingResult calculateForDiscount(CartItemVO cartItemVO,
                                                      PromotionProductVO promotionProductVO) {
        FlashSalePromotionProductVO flashSalePromotionProductVO = (FlashSalePromotionProductVO) promotionProductVO;
        DiscountPricingResult result = DiscountPricingResult.emptyResult();
        Sku sku = cartItemVO.getSku();
        BigDecimal price = sku.getPrice();
        // 修改cartItem价格并往结果中加一条记录
        BigDecimal discount = flashSalePromotionProductVO.getDiscount();
        sku.setPrice(discount);
        sku.setMarketPrice(discount);
        result.addDiscount(price.subtract(discount), FLASHSALE);
        return result;
    }

    /**
     * 更新抢购活动状态
     *
     * @param id     申请id
     * @param status 新状态字符串
     * @return true or false
     * @throws IllegalArgumentException 如果 {@link com.xquark.dal.type.FlashSaleApplyStatus} 中没有该状态
     */
    @Override
    public boolean updateStatus(String id, String status) {
        FlashSaleApplyStatus statusEnum = FlashSaleApplyStatus.valueOf(status);
        return promotionApplyMapper.updateFlashSaleStatus(id, statusEnum) > 0;
    }

    /**
     * 通过订单id更新抢购活动状态
     *
     * @param id     订单id
     * @param status 新状态字符串
     * @return true or false
     * @throws IllegalArgumentException 如果 {@link com.xquark.dal.type.FlashSaleApplyStatus} 中没有该状态
     */
    @Override
    public boolean updateStatusByOrderId(String id, String status) {
        FlashSaleApplyStatus statusEnum = FlashSaleApplyStatus.valueOf(status);
        return promotionApplyMapper.updateFlashSaleStatusByOrderId(id, statusEnum) > 0;
    }

    @Override
    public boolean updateStock(String orderId, int qty) {
        return promotionFlashSaleMapper.updateStock(orderId, qty) > 0;
    }

    @Override
    public PromotionApply loadApply(String promotionProductId, String userId) {
        return promotionApplyMapper.selectByPromotionProductIdAndUserId(promotionProductId,
                userId, FLASHSALE);
    }

    @Override
    public BigDecimal getPromotionPrice(String promotionProductId, String userId) {
        FlashSalePromotionProductVO product = (FlashSalePromotionProductVO)
                this.loadPromotionProductVO(promotionProductId);
        if (product == null) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, "活动商品不存在");
        }
        return product.getDiscount();
    }

    /**
     * 保存关联对象
     *
     * @param flashSale 活动商品关联对象
     * @return true or false
     */
    public boolean saveFlashSale(PromotionFlashSale flashSale) {
        String productId = flashSale.getProductId();
        if (inPromotion(productId)) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该商品已经参与了抢购活动");
        }
        return promotionFlashSaleMapper.insert(flashSale) > 0;
    }

    /**
     * 更新活动商品关联对象
     *
     * @param flashSale 活动商品关联对象
     * @return true or false
     */
    public boolean updateFlashSale(PromotionFlashSale flashSale) {
        return promotionFlashSaleMapper.updateByPrimaryKeySelective(flashSale) > 0;
    }


    /**
     * 保存活动及活动商品
     *
     * @param promotion        活动
     * @param promotionProduct 活动商品关联对象
     * @return true or false
     */
    @Transactional
    public boolean savePromotionProduct(Promotion promotion,
                                        PromotionFlashSale promotionProduct) {
        Preconditions.checkNotNull(promotion);
        Preconditions.checkNotNull(promotionProduct);

        boolean promotionSaveResult;
        boolean promotionProductSaveResult;

        String promotionProductId = promotionProduct.getId();
        String productId = promotionProduct.getProductId();
        if (!productService.exists(productId)) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品不存在");
        }

        if (StringUtils.isBlank(promotionProductId)) {
            //一元专区
            if (PromotionType.SALEZONE.equals(promotion.getPromotionType())) {
                Promotion promotionByDate = promotionService.findPromotionByDate(promotion.getBatchNo());
                //批次号不同校验时间
                if (promotionByDate != null && promotionByDate.getValidFrom() != null
                        && promotionByDate.getValidTo() != null) {
                    boolean checkResult = checkValidTime(promotion.getValidFrom(), promotion.getValidTo(),
                            promotionByDate.getValidFrom(), promotionByDate.getValidTo());
                    if (!checkResult)
                        throw new BizException(GlobalErrorCode.ERROR_PARAM, "不同批次的活动时间不能在已生效活动时间内");
                }
                ProductVO productVO = productService.load(productId);
                if (productVO != null && productVO.getPrice() != null &&
                        promotion.getPrice().compareTo(productVO.getPrice()) > 0)
                    throw new BizException(GlobalErrorCode.ERROR_PARAM, "活动价格不能高于商品原价");
                promotionSaveResult = promotionService.save(promotion);
                String promotionId = promotion.getId();
                promotionProduct.setPromotionId(promotionId);
                promotionProduct.setDiscount(BigDecimal.ZERO);//一元购折扣设置为0
                promotionProductSaveResult = saveFlashSale(promotionProduct);
            } else {
                promotion.setType(FLASHSALE);
                promotionSaveResult = promotionService.save(promotion);
                String promotionId = promotion.getId();
                promotionProduct.setPromotionId(promotionId);
                promotionProductSaveResult = saveFlashSale(promotionProduct);
            }
        } else {
            Promotion dbPromotion = promotionService.load(promotionProduct.getPromotionId());
            if (dbPromotion == null) {
                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "要更新的活动不存在");
            }
            Date now = new Date();
            Date validFrom = dbPromotion.getValidFrom();
            Date validTo = dbPromotion.getValidTo();

            if (now.before(validTo) && now.after(validFrom) && !dbPromotion.getClosed()) {
                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动出于进行状态, 无法修改");
            } else {
                promotionSaveResult = promotionService.update(promotion);
                promotionProductSaveResult = updateFlashSale(promotionProduct);
            }
        }
        return promotionSaveResult && promotionProductSaveResult;
    }

    /**
     * 保存活动标题
     *
     * @param type  活动类型
     * @param title 活动标题
     * @return true or false
     */
    @Transactional
    public boolean savePromotionTitle(PromotionType type,
                                      String title) {
        Preconditions.checkNotNull(type);
        Preconditions.checkNotNull(title);
        PromotionTitle promotionTitle = this.selectPromotionTitleByType(type);
        if (promotionTitle != null) {
            promotionTitle.setPromotionTitle(title);
            return promotionTitleMapper.update(promotionTitle) > 0;
        }
        PromotionTitle instance = PromotionTitle.getInstance();
        instance.setPromotionTitle(title);
        instance.setPromotionType(type);
        return promotionTitleMapper.insert(instance) > 0;
    }

    /**
     * 查询活动标题
     *
     * @param promotionType 活动类型
     * @return 活动标题
     */
    public PromotionTitle selectPromotionTitleByType(PromotionType promotionType) {
        return promotionTitleMapper.selectByType(promotionType);
    }

    /**
     * 查询用户vo
     *
     * @param id 活动商品id
     * @return 活动商品用户vo
     */
    public FlashSalePromotionUserVO loadUserVO(String id, String userId) {
        return promotionFlashSaleMapper.selectUserVOByPrimaryKey(id, userId);
    }

    /**
     * 查询所有商品数量
     *
     * @return 商品数量
     */
    public Long countAllProducts() {
        return promotionFlashSaleMapper.countAll();
    }

    public Long countAllByKey(String batchNo, PromotionType promotionType, String promotionStatus) {
        return promotionFlashSaleMapper.countAllByKey(batchNo, promotionType, promotionStatus);
    }

    /**
     * 查询所有商品数量
     *
     * @return 商品数量
     */
    public Long countFlashSaleProducts() {
        return promotionFlashSaleMapper.countFlashSaleAll();
    }


    /**
     * 查询所有抢购活动商品
     *
     * @param pageable    分页对象
     * @return 限购商品集合
     */
    public List<FlashSalePromotionProductVO> listAllPromotionProductVOs(Pageable pageable) {
        return promotionFlashSaleMapper.listAllPromotionFlashSaleVO(pageable);
    }

    /**
     * 根据关键字查询活动
     *
     * @param batchNo         批次号
     * @param promotionType   活动类型
     * @param promotionStatus 活动状态
     * @param pageable        分页
     * @return List<FlashSalePromotionProductVO>
     */
    public List<FlashSalePromotionProductVO> searchByKeyWord(String batchNo,
                                                             PromotionType promotionType,
                                                             String promotionStatus,
                                                             Pageable pageable) {
        return promotionFlashSaleMapper.searchByKeyWord(batchNo, promotionType, promotionStatus, pageable);
    }

    /**
     * 所有秒杀抢购活动商品
     *
     * @param career      身份信息
     * @param grandSaleId 大型促销活动id
     * @return 限购商品集合
     */
    public List<FlashSalePromotionProductVO> listProductVOs(CareerLevelType career, Integer grandSaleId, PromotionType promotionType) {
        final List<FlashSalePromotionProductVO> ret = promotionFlashSaleMapper
                .listFlashSaleVO(career, grandSaleId, promotionType);
        ret.forEach(result -> syncFromRedis(result, false));
        return ret;
    }

    /**
     * 查询一元专区活动商品
     *
     * @return 专区商品集合
     */
    public List<FlashSalePromotionProductVO> listAllSaleZoneProductVOs() {
        return promotionFlashSaleMapper
                .listAllPromotionSaleZoneVO();
    }

    /**
     * 根据批次号查询活动
     *
     * @param batchNo 批次号
     * @return 活动
     */
    public Promotion findDateByBatchNo(String batchNo) {
        return promotionService.findDateByBatchNo(batchNo);
    }

    /**
     * 查询所有订单关联的抢购活动申请列表
     *
     * @param userId    买家id, 可不传，查询所有订单
     * @param order     排序
     * @param direction 方向
     * @param pageable  分页
     * @return list of {@link FlashSalePromotionOrderVO} or empty list
     */
    public List<FlashSalePromotionOrderVO> listOrderVO(String userId, String order,
                                                       Direction direction, Pageable pageable) {
        return promotionFlashSaleMapper.listOrderVO(userId, order, direction, pageable);
    }

    /**
     * 查询所有订单关联的抢购活动申请列表
     *
     * @param order     排序
     * @param direction 方向
     * @param pageable  分页
     * @return list of {@link FlashSalePromotionOrderVO} or empty list
     */
    public List<FlashSalePromotionOrderVO> listOrderVO(String order, Direction direction,
                                                       Pageable pageable) {
        return listOrderVO(null, order, direction, pageable);
    }

    /**
     * 查询订单关联的抢购活动列表申请数量
     *
     * @param userId 买家id 可不传，查询所有订单
     * @return 订单数量
     */
    public Long countOrderVO(String userId) {
        return promotionFlashSaleMapper.countOrderVO(userId);
    }

    /**
     * 查询订单关联的抢购活动列表申请数量
     *
     * @return 订单数量
     */
    public Long countOrderVO() {
        return countOrderVO(null);
    }

    public Date loadPromotionValidFromIfExists(String productId) {
        return promotionFlashSaleMapper.selectPromotionValidFrom(productId);
    }

    public boolean isInPromotion(String productId) {
        return promotionFlashSaleMapper.selectIsInPromotion(productId);
    }

    public Optional<String> loadActivePromotionId(String productId) {
        return Optional.ofNullable(promotionFlashSaleMapper.selectActivePromotionId(productId))
                .map(IdTypeHandler::encode);
    }

    public void checkPromotionStart(String productId) {
        RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
        Integer lockerState = redisUtils.get(PromotionConstants.FLASH_SALE_LOCKER);
        if (lockerState == null || lockerState == 0) {
            return;
        }
        Date promotionValidFrom;
        try {
            promotionValidFrom = this.loadPromotionValidFromIfExists(productId);
        } catch (Exception e) {
            return;
        }
        if (promotionValidFrom == null) {
            return;
        }
        if (new Date().before(promotionValidFrom)) {
            String msg = String.format("限时抢购活动将于%s开始, 请耐心等待", DateFormatUtils.format(promotionValidFrom,
                    "yyyy-MM-dd HH:mm:ss"));
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
        }
    }

    /**
     * 活动时间校验
     *
     * @param validFrom 新建活动开始时间
     * @param validTo   新建活动结束时间
     * @param from      已有活动开始时间
     * @param to        已有活动结束时间
     * @return 活动时间是否合法（true -> 合法，false -> 不合法）
     */
    private static boolean checkValidTime(Date validFrom, Date validTo, Date from, Date to) {
        if (validFrom == null || validTo == null || from == null || to == null)
            throw new BizException(GlobalErrorCode.ERROR_PARAM, "活动时间不能为空");

        //活动开始时间在已生效活动时间内
        boolean a = validFrom.getTime() <= to.getTime();
        boolean a1 = validFrom.getTime() >= from.getTime();

        //活动结束时间在已生效活动时间内
        boolean b = validTo.getTime() <= to.getTime();
        boolean b1 = validTo.getTime() >= from.getTime();

        return !(a && a1) && !(b && b1);
    }

    @Override
    public Optional<FlashSalePromotionProductVO> load(String promotionId, String productId) {
        final FlashSalePromotionProductVO f =
                (FlashSalePromotionProductVO) this.loadPromotionProductByProductId(productId);
        return Optional.ofNullable(f);
    }
}
