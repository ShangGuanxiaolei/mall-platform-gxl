package com.xquark.service.promotion.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xquark.dal.mapper.PromotionInviteCodeMapper;
import com.xquark.dal.mapper.PromotionMapper;
import com.xquark.dal.model.PromotionInviteCode;
import com.xquark.dal.model.User;
import com.xquark.dal.type.InviteCodeStatus;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.product.ProductService;
import com.xquark.service.promotion.PromotionInviteCodeService;

@Service
public class PromotionInviteCodeServiceImpl extends BaseServiceImpl implements PromotionInviteCodeService{
	@Autowired 
	private PromotionInviteCodeMapper inviteCodeMapper;

	/**
	 * code 邀请码
	 * pCode 所属活动编码
	 */
	@Override
	public int validate(String code, String pCode) {
		
		List<PromotionInviteCode> inviteCodeList = inviteCodeMapper.selectByCodeAndPcode(code, pCode);
		if(CollectionUtils.isEmpty(inviteCodeList)) {
			return InviteCodeStatus.NONE.getCode();
		}
		User usr = (User) getCurrentUser();
		String cpId = String.valueOf(usr.getCpId());
		PromotionInviteCode inviteCode ;
		if(inviteCodeList.size()==1) {
			inviteCode = inviteCodeList.get(0);
			if(StringUtils.isBlank(inviteCode.getCpId())) {
				//有邀请码且未绑定
				return InviteCodeStatus.NEVER.getCode();
			}else if(cpId.equals(inviteCode.getCpId())) {
				//有邀请码且绑定自己
				return InviteCodeStatus.ALREADY.getCode();
			}else {
				//有邀请码且绑定他人
				return InviteCodeStatus.USED.getCode();
			}
		}else {
			//无邀请码或者邀请码多条
			return InviteCodeStatus.NONE.getCode();
			}
	}
	/**
	 * 更新邀请码关联
	 * code 邀请码
	 * pCode 所属活动编码
	 * cpId  会员编码
	 */
	@Override
	public int updateRelation(String code, String cpid, String pCode) {
		
		return inviteCodeMapper.updateRelation(code, cpid, pCode);
	}

	@Override
	public String selectInviteCode(String cpid, String pCode) {
		return inviteCodeMapper.selectInviteCodeByCpid(cpid, pCode);
	}

}
