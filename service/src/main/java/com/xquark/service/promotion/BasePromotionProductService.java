package com.xquark.service.promotion;

import com.xquark.dal.mapper.PromotionApplyMapper;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionApply;
import com.xquark.dal.model.promotion.PromotionProduct;
import com.xquark.dal.status.IntegerOperation;
import com.xquark.dal.type.PromotionApplyStatus;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionProductVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderService;
import com.xquark.service.pricing.base.PromotionActivityService;
import com.xquark.service.product.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by wangxinhua on 17-11-23. DESC:
 */
public abstract class BasePromotionProductService extends BaseServiceImpl implements
    PromotionProductService {

  protected final Logger logger = LoggerFactory.getLogger(this.getClass());

  protected final PromotionActivityService promotionService;
  protected final ProductService productService;

  protected final PromotionApplyMapper promotionApplyMapper;

  protected OrderService orderService;

  public BasePromotionProductService(
      PromotionActivityService promotionService, ProductService productService,
      PromotionApplyMapper promotionApplyMapper) {
    this.promotionService = promotionService;
    this.productService = productService;
    this.promotionApplyMapper = promotionApplyMapper;
  }

  @Autowired
  public void setOrderService(OrderService orderService) {
    this.orderService = orderService;
  }

  @Override
  public List<? extends PromotionProduct> listPromotionProducts(String id) {
    return listPromotionProducts(id, null, null, null);
  }

  @Override
  public List<? extends PromotionProductVO> listPromotionProductVOs(String id) {
    return listPromotionProductVOs(id, null, null, null);
  }

  @Override
  @Transactional
  public boolean close(String id) {
    PromotionProduct promotionProduct = this.loadPromotionProduct(id);
    if (promotionProduct == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动商品不存在");
    }
    String promotionId = promotionProduct.getPromotionId();
    Promotion promotion = promotionService.load(promotionId);
    assert promotion != null;
    if (promotion.getClosed()) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动已经为结束状态,请勿重复提交");
    }
    return promotionService.close(promotionId);
  }

  @Override
  public boolean apply(String orderId, String promotionProductId, PromotionType promotionType) {
    PromotionApply promotionApply = new PromotionApply(orderId, promotionProductId,
        promotionType);
    return promotionApplyMapper.insert(promotionApply) > 0;
  }

  @Override
  public boolean isApplied(String userId, String promotionProductId, PromotionType type) {
    return promotionApplyMapper.isApplied(userId, promotionProductId, type);
  }

  @Override
  public boolean updateStock(String orderId) {
    int qty = orderService.countOrderItem(orderId);
    if (qty > 0) {
      return updateStock(orderId, qty);
    }
    return false;
  }

  @Override
  public boolean updateStock(String orderId, IntegerOperation operation) {
    int qty = orderService.countOrderItem(orderId);
    if (qty > 0) {
      int qtyOperation = operation.apply(0, qty);
      return updateStock(orderId, qtyOperation);
    }
    return false;
  }

  @Override
  public BigDecimal getPromotionPrice(String promotionProductId) {
    return getPromotionPrice(promotionProductId, getCurrentUser().getId());
  }

  @Override
  @Transactional
  public boolean auditApply(String applyId, PromotionApplyStatus status) {
    PromotionApply apply = promotionApplyMapper.selectByPrimaryKey(applyId);
    if (apply == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "该申请不存在");
    }
    // 拒绝申请时退款
    if (PromotionApplyStatus.REJECTED == status) {
      String orderId = apply.getOrderId();
      orderService.refund(orderId);
    }
    return this.updateStatus(applyId, status.name());
  }

}
