package com.xquark.service.promotion.impl;

import com.xquark.aop.anno.NotNull;
import com.xquark.dal.model.Order;
import com.xquark.dal.status.IntegerOperation;
import com.xquark.dal.type.OrderSortType;
import com.xquark.service.order.OrderService;
import com.xquark.service.promotion.PromotionOrderObServer;
import com.xquark.service.promotion.PromotionProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 17-12-13. DESC:
 */
@Component
public class PreOrderOrderObserver implements PromotionOrderObServer {

  private OrderService orderService;

  private PromotionProductService preOrderPromotionProductService;

  private static final Logger logger = LoggerFactory.getLogger(PreOrderOrderObserver.class);

  @Autowired
  public void setOrderService(OrderService orderService) {
    this.orderService = orderService;
  }

  @Autowired
  @Qualifier(value = "preOrderPromotionProductService")
  public void setPreOrderPromotionProductService(
      PromotionProductService preOrderPromotionProductService) {
    this.preOrderPromotionProductService = preOrderPromotionProductService;
  }

  @Override
  public void onSubmit(@NotNull Order order) {
    if (order.getOrderType() != null && OrderSortType.PREORDER == order.getOrderType()) {
      boolean result = preOrderPromotionProductService.updateStock(order.getId(),
          IntegerOperation.PLUS);
      logger.info("订单 {} 提交时修改预购商品库存 {}", order.getOrderNo(), result ? "成功" : "失败");
    }
  }

  @Override
  public void onCancel(@NotNull Order order) {
    if (order.getOrderType() != null && OrderSortType.PREORDER == order.getOrderType()) {
      boolean result = preOrderPromotionProductService.updateStock(order.getId(),
          IntegerOperation.MINUS);
      logger.info("订单 {} 取消时修改预购商品库存 {}", order.getOrderNo(), result ? "成功" : "失败");
    }
  }

  @Override
  public String toString() {
    return "PreOrderOrderObserver{}";
  }

}
