package com.xquark.service.promotion.impl;

import com.google.common.collect.ImmutableSet;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.PromotionConstants;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.dal.model.promotion.OrderSkuAmount;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.vo.PromotionSkuVo;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.promotion.PromotionConfigService;
import com.xquark.service.promotion.PromotionOrderDetailService;
import com.xquark.service.promotion.PromotionSkusService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service("promotionOrderDetailService")
public class PromotionOrderDetailServiceImpl implements PromotionOrderDetailService {
	@Autowired
	private PromotionOrderDetailMapper orderDetailMapper;
	@Autowired
	private PromotionGoodsMapper goodsMapper;
	@Autowired
	private SkuMapper skuMapper;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private CartItemMapper cartItemMapper;
	@Autowired
	private PromotionConfigService promotionConfigService;
	@Autowired
	private PromotionBaseInfoService promotionBaseInfoService;
	@Autowired
	private PromotionSkusService promotionSkusService;
	@Autowired
	private PromotionTempStockMapper promotionTempStockMapper;
	@Autowired
	private PromotionSkusMapper promotionSkusMapper;
	@Autowired
	private OrderHeaderMapper orderHeaderMapper;

	@Override
	public boolean checkMoneyIsOver(double thisAmount, double specifiedAmount, String pCode, String cpid) {
		 /**
	     * @param thisAmount 当前订单金额
		 * @param pCode 活动编码
		 * @param cpid 会员编码
		 * @param specifiedAmount 指定金额 
	     */
		double sumMoney = orderDetailMapper.findSumMoneyHitoryOrder(cpid, pCode);
		//true表示此次订单金额加上历史金额小于等于指定金额 可以继续购买
		//false表示此次订单金额加上历史金额大于指定金额 不可继续购买
		if(thisAmount+sumMoney<=specifiedAmount) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * 活动商品库存查询
	 */
	@Override
	public int promotionGoodsCount(String pCode, String skuCode) {
		
		return goodsMapper.promotionGoodsCount(pCode, skuCode);
	}



	/**
	 * 检查参加活动的用户购买总金额是否超过
	 * @return
	 */
	@Override
	public boolean isOverUpValue(List<String> skuCodes, User user,int topValue,String pCode) {
		boolean isOver = false;
		if (CollectionUtils.isNotEmpty(skuCodes)) {
			BigDecimal total = new BigDecimal(0);

			for (String skuCode : skuCodes) {
				//检查sku
				Sku sku = skuMapper.selectBySkuCode(skuCode);
				if (null == sku) {
					continue;
				}
				//取价格和购买数量(1)
				BigDecimal price = sku.getPrice();
				CartItem cartItem = cartItemMapper.selectByUserIdAndSku(user.getId(), sku.getId());
				int amount = 0;
				if (null == cartItem) {
					amount = 1;
				} else {
					amount = cartItem.getAmount();
				}
				PromotionSkuVo promotionSkuVo = promotionSkusService.selectProSkuBySkuCodeAndPcode(skuCode, pCode);
				//如果是当前活动的活动商品
				if (null != promotionSkuVo) {
					if(pCode.equals(promotionSkuVo.getpCode())){
						//累加购买总金额
						total = total.add(price.multiply( new BigDecimal(amount)));
					}
				}
			}
			//精度设置
			//true表示此次订单金额加上历史金额小于等于指定金额 可以继续购买
			//false表示此次订单金额加上历史金额大于指定金额 不可继续购买
//			PromotionConfig promotionConfig = promotionConfigService.selectByConfigType("meeting");
//			Integer max;
//			if (null != promotionConfig) {
//				String value = promotionConfig.getConfigValue();
//				try {
//					max = new Integer(value);
//				} catch (Exception e) {
//					max = 12000;
//				}
//
//			} else {
//				max = 12000;
//			}
//			isOver = !this.checkMoneyIsOver(totalOrdeValue.doubleValue(), new Double(max), null, user.getCpId().toString());

			double sumMoney = orderDetailMapper.findSumMoneyHitoryOrder(user.getCpId().toString(), pCode);
			//true表示此次订单金额加上历史金额小于等于指定金额 可以继续购买
			//false表示此次订单金额加上历史金额大于指定金额 不可继续购买

			BigDecimal bigSumMoney = new BigDecimal(sumMoney+"");
			//true表示此次订单金额加上历史金额小于等于指定金额 可以继续购买
			//false表示此次订单金额加上历史金额大于指定金额 不可继续购买
			//bigdecemal比较大小
			int i = (total.add(bigSumMoney)).compareTo(new BigDecimal(topValue));
			if(i>0) {
				isOver = true;
			}else {
				isOver = false;
			}
		}
		return isOver;
	}


	/**
	 * 检查参加活动的用户购买总金额是否超过
	 * @param skuId
	 * @param user
	 * @return
	 */
	@Override
	public boolean isOverUpValue(String skuId, User user, int qty,int topValue) {
		boolean isOver = false;
		BigDecimal total = new BigDecimal(0);
		//检查sku
		Sku sku = skuMapper.selectByPrimaryKey(skuId);
		if (null == sku) {
			return true;
		}
		//取价格和购买数量(1)
		BigDecimal price = sku.getPrice();
		//累加购买总金额
		total = total.add(price.multiply(new BigDecimal(qty)));

		//精度设置
		//true表示此次订单金额加上历史金额小于等于指定金额 可以继续购买
		//false表示此次订单金额加上历史金额大于指定金额 不可继续购买

//		List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService.selectBySkuCode(skuId);
//		Map map = new HashMap();
//		map.put("configType",promotionBaseInfos.get(0).getpType());
//		map.put("configName","top_value");
//		PromotionConfig promotionConfig = promotionConfigService.selectByConfig(map);
//
//		Integer max;
//		if (null != promotionConfig) {
//			String value = promotionConfig.getConfigValue();
//			try {
//				max = new Integer(value);
//			} catch (Exception e) {
//				max = 12000;
//			}
//
//		} else {
//			max = 12000;
//		}

		List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService.selectBySkuCode(sku.getSkuCode());
		if(CollectionUtils.isEmpty(promotionBaseInfos)){
			return true;
		}
		String pCode = promotionBaseInfos.get(0).getpCode();
		double sumMoney = orderDetailMapper.findSumMoneyHitoryOrder(user.getCpId().toString(), pCode);
		BigDecimal bigSumMoney = new BigDecimal(sumMoney+"");
		//true表示此次订单金额加上历史金额小于等于指定金额 可以继续购买
		//false表示此次订单金额加上历史金额大于指定金额 不可继续购买
		//bigdecemal比较大小
		int i = (total.add(bigSumMoney)).compareTo(new BigDecimal(topValue));
		if(i>0) {
			isOver = true;

		}else {
			isOver = false;
		}

		return isOver;
	}

	/**
	 * 根据订单行判断是否限额
	 * @param orderItemList
	 * @param user
	 * @param topValue
	 * @param pCode
	 * @return
	 */
	@Override
	public boolean isOverUpValueByOrderItem(List<OrderItem> orderItemList, User user,int topValue,String pCode) {
		boolean isOver = false;
		if (CollectionUtils.isNotEmpty(orderItemList)) {
			BigDecimal total = new BigDecimal(0);

			for (OrderItem orderItem : orderItemList) {
				orderItem.getProductId();
				//检查sku
				Sku sku = skuMapper.selectByPrimaryKey(orderItem.getProductId());
				if (null == sku) {
					continue;
				}
				//取价格和购买数量(1)
				BigDecimal price = orderItem.getPrice();

				int amount = orderItem.getAmount();

				PromotionSkuVo promotionSkuVo = promotionSkusService.selectProSkuBySkuCodeAndPcode(sku.getSkuCode(), pCode);
				//如果是当前活动的活动商品
				if (null != promotionSkuVo) {
					if(pCode.equals(promotionSkuVo.getpCode())){
						//累加购买总金额
						total = total.add(price.multiply( new BigDecimal(amount)));
					}
				}
			}

			double sumMoney = orderDetailMapper.findSumMoneyHitoryOrder(user.getCpId().toString(), pCode);
			//true表示此次订单金额加上历史金额小于等于指定金额 可以继续购买
			//false表示此次订单金额加上历史金额大于指定金额 不可继续购买

			BigDecimal bigSumMoney = new BigDecimal(sumMoney+"");
			//true表示此次订单金额加上历史金额小于等于指定金额 可以继续购买
			//false表示此次订单金额加上历史金额大于指定金额 不可继续购买
			//bigdecemal比较大小
			int i = (total.add(bigSumMoney)).compareTo(new BigDecimal(topValue));
			if(i>0) {
				isOver = true;
			}else {
				isOver = false;
			}
		}
		return isOver;
	}

  @Override
  public PromotionOrderDetail loadDetailByOrderNo(String orderNo) {
	  return orderDetailMapper.selectByOrderNo(orderNo);
  }

	@Override
	public PromotionOrderDetail loadDetailByMainOrderNo(String mainOrderNo, String pType) {
		return orderDetailMapper.selectByMainOrderNo(mainOrderNo, pType);
	}

  @Override
	public List<OrderSkuAmount> loadDetailByTranCode(String tranCode){
		return orderDetailMapper.loadDetailByTranCode(tranCode);
	}

	@Override
	public Map<String,String> isOverRestriction(List<String> skuCodes, User user, String pCode) {
		Map<String,String> map = new HashMap<>();
		String isOver = "false";
		if (CollectionUtils.isNotEmpty(skuCodes)) {
			for (String skuCode : skuCodes) {
				//检查sku
				Sku sku = skuMapper.selectBySkuCode(skuCode);
				if (null == sku) {
					continue;
				}

				CartItem cartItem = cartItemMapper.selectByUserIdAndSku(user.getId(), sku.getId());
				int amount = 0;
				if (null == cartItem) {
					amount = 0;
				} else {
					amount = cartItem.getAmount();
				}
//				PromotionTempStock stock = promotionTempStockMapper.selectByPCodeAndSkuId(skuCode,null);
//				if(null==stock){
//					isOver = false;
//					return isOver;
//				}
//				String pCodeOne = stock.getpCode();

				map = isOverRestriction(sku.getId(),user,amount,"");
				if("true".equals(map.get("stock"))){
					return map;
				}

				if("true".equals(map.get("restriction"))){
					return map;
				}
			}
		}
		return map;
	}

	@Override
	public Map<String,String> isOverRestriction(String promotionSkuId, User user, int qty, String pCode) {
		Map<String,String> res = new HashMap<>();
		Sku sku = skuMapper.selectByPrimaryKey(promotionSkuId);

        PromotionTempStock promotionTempStock=promotionTempStockMapper.selectByPCodeAndSkuId(sku.getSkuCode(),null);
		int restriction = promotionTempStock.getRestriction();
		String skuCode = promotionTempStock.getSkuCode();
		pCode = promotionTempStock.getpCode();
        //活动商品库存存储类型 DB/REDIS
        PromotionConfig lock_type_config = promotionConfigService.selectByConfigType("lock_type");
        String lock_type = "";
        int stock=0;
        if (null == lock_type_config) {
            lock_type = "DB";
        }
        lock_type = lock_type_config.getConfigValue();
        if ("DB".equals(lock_type)) {
            //如果是DB库存
            stock= Integer.valueOf(promotionTempStock.getpUsableSkuNum());

        } else {
            //如果是redis库存
            RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
            String totalKey = PromotionConstants.getCommonAmountKey(pCode, sku.getSkuCode());
            stock=redisUtils.get(totalKey);
        }

		if(promotionTempStock == null){
			res.put("restriction","false");
			res.put("stock","true");
			return  res;
		}

		//赠品参数
		Integer gift = promotionSkusMapper.getGift(skuCode,pCode);
		//已购买数量
		Integer amount = orderDetailMapper.findSumAmountHitoryOrder(user.getCpId().toString(), promotionTempStock.getpCode(),promotionSkuId);
		//qty 这次购买数量

		//这次购买总数量(含赠品)
		int shopAmount = 0;
		//本次活动总购买数量
		int total = 0;

		if(gift != 0){
			shopAmount = qty+qty/gift;
		}else{
			shopAmount = qty;
		}

		total = qty + amount;

		if(stock<=0||shopAmount>stock){
			res.put("restriction","false");
			res.put("stock","true");
			return res;
		}

		if(total > restriction && restriction>0){
			res.put("restriction","true");
			res.put("stock","false");
			return res;
		}else {
			res.put("restriction","false");
			res.put("stock","false");
			return res;
		}
	}

  @Override
  @Transactional(rollbackFor = Exception.class)
  public boolean batchUpdateTranOrderStatus(String tranCode) {
	  List<String> toUpdate = orderDetailMapper.selectTranOrderNoList(tranCode);
	  // PAIDNOSTOCK -> PAID 不会走工作流, 需要手动同步order_header表
	  return batchUpdatePgOrderToPaidByOrderNo(toUpdate);
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
	public boolean batchUpdatePgOrderToPaidByOrderNo(String... orderNos) {
		return batchUpdatePgOrderToPaidByOrderNo(Arrays.asList(orderNos));
	}

  private boolean batchUpdatePgOrderToPaidByOrderNo(List<String> orderNoList) {
		Integer ret = orderDetailMapper.batchUpdateTranOrderStatusToPaidByOrderNO(orderNoList);
		orderHeaderMapper.batchUpdateTranOrderStatusToPaidByOrderNo(orderNoList);
		return ret != null && ret > 0;
	}

	private List<String> selectOrderIdByTranCode(String tranCode) {
		return orderDetailMapper.selectOrderIdByTranCode(tranCode)
				.stream().map(s -> IdTypeHandler.encode(Long.valueOf(s)))
				.collect(Collectors.toList());
	}

	private Optional<String> selectOrderIdByDetailCode(String detailCode) {
		return Optional.ofNullable(orderDetailMapper.selectOrderIdByDetailCode(detailCode))
				.map(s -> IdTypeHandler.encode(Long.valueOf(s)));
	}

  @Override
  public Map<String, Integer> sumTranTotalAmount(String tranCode, String orderId) {
		final List<String> ids = this.selectOrderIdByTranCode(tranCode);
		// 去重
		final ImmutableSet<String> idsSet = ImmutableSet.<String>builder()
				.addAll(ids)
				.add(orderId).build();
		List<OrderSkuAmount> orderSkuAmounts = orderDetailMapper.selectItemAmtByOrderId(idsSet.toArray(new String[0]));
		Map<String, Integer> map = new HashMap<>();
		int temp = 0;
		for (OrderSkuAmount orderSkuAmount:
				 orderSkuAmounts) {
			Long skuId = orderSkuAmount.getSkuId();
      Integer amount = Optional.ofNullable(orderSkuAmount.getAmount()).orElse(0);
      if (map.containsKey(IdTypeHandler.encode(skuId))) {
        temp += amount;
      } else {
				temp = amount;
			}
      map.put(IdTypeHandler.encode(skuId), temp);
		}
		return map;
  }

	@Override
	public int selectTrnDetailAmount(String detailCode, String orderId) {
		String id = this.selectOrderIdByDetailCode(detailCode)
				.orElseThrow(BizException.ofSupplier(GlobalErrorCode.INVALID_ARGUMENT, "拼团详情不存在"));
		String[] ids = ImmutableSet.of(id, orderId).toArray(new String[0]);
    List<OrderSkuAmount> orderSkuAmounts = orderDetailMapper.selectItemAmtByOrderId(ids);
    if (orderSkuAmounts == null || orderSkuAmounts.isEmpty()) {
      return 0;
    }
    return Optional.ofNullable(orderSkuAmounts.get(0).getAmount()).orElse(0);
	}

	@Override
  public boolean hasUnPaidOrder(String tranCode, String userId) {
	  return orderDetailMapper.countUnPaidTranOrder(tranCode, userId) > 0;
  }
}
