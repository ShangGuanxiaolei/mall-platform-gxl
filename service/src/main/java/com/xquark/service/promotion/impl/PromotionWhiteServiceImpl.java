package com.xquark.service.promotion.impl;

import com.xquark.dal.mapper.PromotionWhiteMapper;
import com.xquark.dal.model.PromotionWhite;
import com.xquark.service.promotion.PromotionWhiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("promotionWhiteService")
public class PromotionWhiteServiceImpl implements PromotionWhiteService {

  private PromotionWhiteMapper promotionWhiteMapper;

  /** 白名单魔法值 */
  private static final Long MAGIC_NUMBER = 9999999L;

  @Autowired
  public void setPromotionWhiteMapper(PromotionWhiteMapper promotionWhiteMapper) {
    this.promotionWhiteMapper = promotionWhiteMapper;
  }

  @Override
  public PromotionWhite selectByPrimaryKey(String id) {
    return promotionWhiteMapper.selectByPrimaryKey(id);
  }

  @Override
  public Boolean isExistWhiteList(Long cpid) {
    return promotionWhiteMapper.isExistWhiteList(cpid);
  }

  @Override
  public Boolean isExistByPacket(Long cpId) {
    return promotionWhiteMapper.isExistByPacket(cpId);
  }

  @Override
  public Boolean isExistWhiteListBypCode(Long cpId) {
    return promotionWhiteMapper.isExistWhiteListBypCode(cpId);
  }

  @Override
  public Boolean isExistsByRealFuckingPCode(Long cpId, String pCode) {
    return promotionWhiteMapper.isExistsByPCode(cpId, pCode);
  }

  @Override
  public boolean havePacketRule(Long cpId) {
    // 白名单功能关闭，有权限
    if (this.isExistWhiteListBypCode(MAGIC_NUMBER)) {
      return true;
    }
    return this.isExistByPacket(cpId);
  }

  @Override
  public boolean haveLotteryRule(Long cpId) {
      if (this.isExistsByRealFuckingPCode(MAGIC_NUMBER, "lottery")) {
        return true;
      }
      return this.isExistsByRealFuckingPCode(cpId, "lottery");
  }
}
