package com.xquark.service.promotion;

import com.xquark.dal.model.Order;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by wangxinhua on 17-12-13. DESC:
 */
public class PromotionOrderSubject {

  private static final Logger logger = LoggerFactory.getLogger(PromotionOrderSubject.class);

  private List<PromotionOrderObServer> obServers = new ArrayList<>();

  public boolean add(PromotionOrderObServer obServer) {
    logger.info("{} added to promotionOrderSubject", obServer);
    return this.obServers.add(obServer);
  }

  public void onSubmit(Order order) {
    for (PromotionOrderObServer obServer : obServers) {
      obServer.onSubmit(order);
    }
  }

  public void onCancel(Order order) {
    for (PromotionOrderObServer obServer : obServers) {
      obServer.onCancel(order);
    }
  }

}
