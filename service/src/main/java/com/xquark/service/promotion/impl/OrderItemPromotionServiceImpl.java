package com.xquark.service.promotion.impl;

import com.xquark.dal.mapper.OrderItemPromotionMapper;
import com.xquark.dal.mapper.PromotionGrouponMapper;
import com.xquark.dal.model.OrderItemPromotion;
import com.xquark.dal.model.PromotionGroupon;
import com.xquark.dal.vo.PromotionGrouponVO;
import com.xquark.service.groupon.PromotionGrouponService;
import com.xquark.service.promotion.OrderItemPromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by chh on 16-10-14.
 */
@Service("orderItemPromotionService")
public class OrderItemPromotionServiceImpl implements OrderItemPromotionService {

  @Autowired
  private OrderItemPromotionMapper orderItemPromotionMapper;

  /**
   * 根据id查询
   */
  @Override
  public OrderItemPromotion selectByPrimaryKey(String id) {
    return orderItemPromotionMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(OrderItemPromotion orderItemPromotion) {
    // 新增前先将相同promotionid和orderitemid的记录删除掉
    orderItemPromotionMapper.deleteByOrderItemIdAndPromotionId(orderItemPromotion.getPromotionId(),
        orderItemPromotion.getOrderItemId());
    return orderItemPromotionMapper.insert(orderItemPromotion);
  }

  @Override
  public OrderItemPromotion selectByOrderItemId(String orderItemId) {
    return orderItemPromotionMapper.selectByOrderItemId(orderItemId);
  }

  @Override
  public int delete(String id) {
    return orderItemPromotionMapper.delete(id);
  }
}
