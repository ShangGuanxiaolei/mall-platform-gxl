package com.xquark.service.promotion;

import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.User;
import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.dal.model.promotion.OrderSkuAmount;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public interface PromotionOrderDetailService {
	/**
	 *
	 * @param thisAmount 当前订单金额
	 * @param pCode 活动编码
	 * @param cpid 会员编码
	 * @param specifiedAmount 指定金额
	 * true表示此次订单金额加上历史金额小于等于指定金额 可以继续购买
	 * false表示此次订单金额加上历史金额大于指定金额 不可继续购买
	 * @return
	 */
	boolean checkMoneyIsOver(double thisAmount,double specifiedAmount,String pCode,String cpid);
	/**
	 * 活动商品库存查询
	 * @param pCode 活动编码
	 * @param skuCode
	 * @return
	 */
	int promotionGoodsCount(String pCode,String skuCode);

	/**
	 * 检查参加活动的用户购买总金额是否超过
	 * @param promotionSkuIds
	 * @param user
	 * @return
	 */
	boolean isOverUpValue(List<String> promotionSkuIds, User user,int topValue,String pCode);
	boolean isOverUpValue(String promotionSkuIds, User user, int qty,int topValue);

	boolean isOverUpValueByOrderItem(List<OrderItem> orderItemList, User user, int topValue, String pCode);

	/**
	 * 校验商品数量限购（购物车）
	 * @param promotionSkuCodes
	 * @param user
	 * @param pCode
	 * @return
	 */
	Map<String,String> isOverRestriction(List<String> promotionSkuCodes, User user, String pCode);

	/**
	 * 校验商品数量限购（非购物车）
	 * @param promotionSkuId
	 * @param user
	 * @param qty
	 * @param pCode
	 * @return
	 */
	Map<String,String> isOverRestriction(String promotionSkuId, User user, int qty, String pCode);

	PromotionOrderDetail loadDetailByOrderNo(String orderNo);

	PromotionOrderDetail loadDetailByMainOrderNo(String mainOrderNo, String pType);

	List<OrderSkuAmount> loadDetailByTranCode(String tranCode);

	boolean batchUpdateTranOrderStatus(String tranCode);

	@Transactional(rollbackFor = Exception.class)
	boolean batchUpdatePgOrderToPaidByOrderNo(String... orderNos);

	Map<String, Integer> sumTranTotalAmount(String tranCode, String orderId);

	int selectTrnDetailAmount(String tranCode, String orderId);

	boolean hasUnPaidOrder(String tranCode, String userId);

}
