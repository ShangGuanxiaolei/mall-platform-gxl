package com.xquark.service.promotion;

import com.xquark.dal.model.PromotionApply;
import com.xquark.dal.model.promotion.PromotionProduct;
import com.xquark.dal.status.IntegerOperation;
import com.xquark.dal.status.PromotionStatus;
import com.xquark.dal.type.PromotionApplyStatus;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionProductVO;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.pricing.impl.pricing.DiscountPricingResult;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by wangxinhua on 17-11-23. DESC:
 */
public interface PromotionProductService {

  /**
   * 商品是否已经参与了该类型的活动
   *
   * @param productId 商品id
   * @return true or false
   */
  boolean inPromotion(String productId);

  /**
   * 商品是否已经参与了指定的活动
   *
   * @param productId 商品id
   * @return true or false
   */
  boolean inPromotion(String promotionId, String productId);

  /**
   * 根据id查询活动商品
   *
   * @param id 商品id
   * @return 活动商品对象
   */
  PromotionProduct loadPromotionProduct(String id);

  /**
   * 根据单商品查询, 限时抢购只允许配置单商品
   */
  PromotionProductVO loadPromotionProductByPId(String productId);

  /**
   * 根据单商品查询, 限时抢购只允许配置单商品
   */
  PromotionProductVO loadPromotionProductByProductId(String productId);

  /**
   * 根据单商品查询, 限时抢购只允许配置单商品
   */
  PromotionProductVO loadPromotionProductSaleZoneByPId(String productId);

  /**
   * 根据活动id及商品查询活动商品
   *
   * @param promotionId 活动id
   * @param productId 商品id
   * @return 活动商品对象
   */
  PromotionProduct loadPromotionProduct(String promotionId, String productId);

  /**
   * 根据id查询活动商品vo
   *
   * @param id 活动商品id
   * @return 活动商品对象
   */
  PromotionProductVO loadPromotionProductVO(String id);

  /**
   * 根据活动id分页查询对应的活动商品集合 默认按商品销量排序
   *
   * @param id 活动id
   * @param order 排序
   * @param direction 排序方向
   * @param pageable 分页对象
   * @return 活动商品
   */
  List<? extends PromotionProduct> listPromotionProducts(String id, String order,
      Direction direction, Pageable pageable);

  /**
   * 根据活动id分页查询对应的活动商品VO集合 默认按商品销量排序
   *
   * @param id 活动id
   * @param order 排序
   * @param direction 排序方向
   * @param pageable 分页对象
   * @return 活动商品
   */
  List<? extends PromotionProductVO> listPromotionProductVOs(String id, String order,
      Direction direction, Pageable pageable);

  /**
   * 根据活动id分页查询对应的活动商品集合 默认按商品销量排序
   *
   * @param id 活动id
   * @return 活动商品
   */
  List<? extends PromotionProduct> listPromotionProducts(String id);

  /**
   * 根据活动id分页查询对应的活动商品VO集合 默认按商品销量排序
   *
   * @param id 活动id
   * @return 活动商品VO
   */
  List<? extends PromotionProductVO> listPromotionProductVOs(String id);

  /**
   * 查询活动下有多少活动商品
   *
   * @param promotionId 活动id
   * @return 商品数量
   */
  Long countProducts(String promotionId);


  /**
   * 校验活动商品是否合法 (如是否过期等)
   *
   * @param product 活动商品对象
   * @return true or false
   */
  PromotionStatus isValid(PromotionProductVO product);

  /**
   * 结束活动
   *
   * @param id 活动商品id
   * @return true or false
   */
  boolean close(String id);

  /**
   * 计算优惠价格
   *
   * @param cartItemVO 购物车商品
   * @param promotionProductVO 活动商品
   * @return 计算结果
   */
  DiscountPricingResult calculateForDiscount(CartItemVO cartItemVO,
      PromotionProductVO promotionProductVO);

  /**
   * 增加一条活动申请记录
   *
   * @param orderId 订单id
   * @param promotionProductId 活动商品id
   * @param type 活动类型
   * @return true or false
   */
  boolean apply(String orderId, String promotionProductId, PromotionType type);

  /**
   * 判断用户是否已经针对该活动参与过优惠 即该用户下的订单中有一笔已经在申请中
   *
   * @param userId 用户id
   * @param promotionProductId 活动商品id
   * @param type 活动类型
   * @return applied or not
   */
  boolean isApplied(String userId, String promotionProductId, PromotionType type);

  /**
   * 更新活动状态
   *
   * @param id 申请id
   * @param status 新状态字符串
   * @return 更新结果
   */
  boolean updateStatus(String id, String status);

  /**
   * 通过订单id更新活动状态
   *
   * @param id 订单id
   * @param status 新状态字符串
   * @return 更新结果
   */
  boolean updateStatusByOrderId(String id, String status);

  /**
   * 根据订单id更新活动库存
   *
   * @param orderId 订单id
   * @param operation 运算操作类型, 此处只表示加减
   * @return true or false
   */
  boolean updateStock(String orderId, IntegerOperation operation);


  /**
   * 根据订单id更新活动库存
   *
   * @param orderId 订单id
   * @return true or false
   */
  boolean updateStock(String orderId);

  /**
   * 根据订单id更新活动库存
   *
   * @param orderId 订单id
   * @param qty 数量
   * @return true or false
   */
  boolean updateStock(String orderId, int qty);

  /**
   * 根据活动商品和用户id查询用户的活动申请
   *
   * @param promotionProductId 活动商品id
   * @param userId 用户id
   * @return 活动申请对象
   */
  PromotionApply loadApply(String promotionProductId, String userId);

  /**
   * 根据活动商品id及用户信息获取活动价格
   *
   * @param promotionProductId 活动商品id
   * @param userId 用户id
   * @return 活动价格
   */
  BigDecimal getPromotionPrice(String promotionProductId, String userId);

  /**
   * 根据活动商品id及当前用户信息获取活动价格
   *
   * @param promotionProductId 活动商品id
   * @return 活动价格
   */
  BigDecimal getPromotionPrice(String promotionProductId);

  /**
   * 后台审核申请
   *
   * @param applyId 申请id
   * @param status 审核状态
   * @return true or false
   */
  boolean auditApply(String applyId, PromotionApplyStatus status);

}
