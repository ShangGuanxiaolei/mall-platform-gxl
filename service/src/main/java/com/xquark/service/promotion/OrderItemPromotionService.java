package com.xquark.service.promotion;

import com.xquark.dal.model.OrderItemPromotion;

/**
 * Created by chh on 16-10-14. 订单明细优惠关联表Service
 */
public interface OrderItemPromotionService {

  OrderItemPromotion selectByPrimaryKey(String id);

  int insert(OrderItemPromotion orderItemPromotion);

  OrderItemPromotion selectByOrderItemId(String orderItemId);

  int delete(String id);
}
