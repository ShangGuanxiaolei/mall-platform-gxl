package com.xquark.service.promotion;

import com.xquark.dal.model.PromotionApply;
import com.xquark.dal.status.PromotionStatus;
import com.xquark.dal.type.PromotionApplyStatus;
import com.xquark.dal.vo.PromotionProductVO;

import java.util.Date;

/**
 * Created by wangxinhua on 17-12-1. DESC:
 */
public class StatusChecker {

  private final PromotionProductService promotionProductService;

  private final PromotionProductVO target;

  private PromotionStatus status = PromotionStatus.NORMAL;

  public StatusChecker(PromotionProductService service, PromotionProductVO target) {
    this.promotionProductService = service;
    this.target = target;
  }

  public StatusChecker(PromotionProductVO target) {
    this.promotionProductService = PromotionProductServiceFactory
        .loadByType(target.getPromotion().getPromotionType());
    this.target = target;
  }

  /**
   * 校验商品是否存在
   *
   * @return this
   */
  public StatusChecker checkNotNull() {
    if (needContinue()) {
      if (target == null) {
        status = PromotionStatus.NOT_EXISTS;
      }
    }
    return this;
  }

  /**
   * 校验活动日期, 修改状态
   *
   * @return this
   */
  public StatusChecker checkDate() {
    if (needContinue()) {
      if (new Date().after(target.getValidTo())) {
        status = PromotionStatus.END;
      }
    }
    return this;
  }

  /**
   * 校验活动商品及原始商品库存, 修改状态
   *
   * @return this
   */
  public StatusChecker checkAmount() {
    if (needContinue()) {
      if (target.getProduct().getAmount() <= 0) {
        status = PromotionStatus.UNDER_STOCKS;
      }
      if (target.getAmount() <= 0) {
        status = PromotionStatus.SELL_OUT;
      }
    }
    return this;
  }

  /**
   * 校验活动是否已经申请过
   *
   * @param userId 用户id
   * @return this
   */
  public StatusChecker checkApply(String userId) {
    if (needContinue()) {
      String pPid = target.getId();
      PromotionApply apply = promotionProductService.loadApply(pPid, userId);
      if (apply != null) {
        this.status = PromotionStatus.APPLIED;
        // 未审核
        if (apply.getStatus() == PromotionApplyStatus.APPLYING) {
          this.status = PromotionStatus.NOT_AUDIT_YET;
        }
        // 审核拒绝
        if (apply.getStatus() == PromotionApplyStatus.REJECTED) {
          this.status = PromotionStatus.REJECTED;
        }
        // 审核通过
        if (apply.getStatus() == PromotionApplyStatus.PASS) {
          this.status = PromotionStatus.NORMAL;
        }
      }
    }
    return this;
  }

  /**
   * 有子类实现的自定义校验
   *
   * @return this
   */
  public StatusChecker checkCustom() {
    if (needContinue()) {
      status = promotionProductService.isValid(target);
    }
    return this;
  }

  /**
   * 获取最终校验的结果,若之前没有调用过校验方法则返回 {@code PromotionStatus.NORMAL}
   *
   * @return 校验后的状态
   */
  public PromotionStatus getFinalStatus() {
    return status;
  }

  /**
   * 在校验之前判断状态是否还需要继续校验
   *
   * @return true or false
   */
  private boolean needContinue() {
    return status == PromotionStatus.NORMAL;
  }

  /**
   * 封装常用的活动检查
   *
   * @param promotionProduct 活动商品VO
   * @return 活动检查状态
   */
  public static PromotionStatus defaultCheck(PromotionProductVO promotionProduct) {
    return commonCheck(promotionProduct).checkCustom().getFinalStatus();
  }

  /**
   * 封装带用户信息的校验器
   *
   * @param promotionProductVO 活动商品vo
   * @param userId 用户id
   * @return 活动校验状态
   */
  public static PromotionStatus checkWithUser(PromotionProductVO promotionProductVO,
      String userId) {
    return commonCheck(promotionProductVO).checkApply(userId)
        .checkCustom().getFinalStatus();
  }

  /**
   * 基本规则校验器
   *
   * @param promotionProduct 活动商品vo
   * @return 封装校验器
   */
  private static StatusChecker commonCheck(PromotionProductVO promotionProduct) {
    return new StatusChecker(promotionProduct)
        .checkNotNull()
        .checkDate()
        .checkAmount();
  }
}
