package com.xquark.service.promotion.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.PromotionSkuGiftMapper;
import com.xquark.dal.model.PromotionSkuGift;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.promotion.PromotionSkuGiftService;
@Service
public class PromotionSkuGiftServiceImpl extends BaseServiceImpl implements PromotionSkuGiftService{
	@Autowired
	private PromotionSkuGiftMapper skuGiftMapper;
	@Override
	public List<PromotionSkuGift> getPromotionSkuGiftList(String pCode, String skuCode) {
		return skuGiftMapper.getPromotionSkuGiftList(pCode, skuCode);
	}

}
