package com.xquark.service.shop.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xquark.dal.mapper.AccessReportMapper;
import com.xquark.dal.mapper.ShopAccessMapper;
import com.xquark.dal.model.AccessReport;
import com.xquark.dal.model.ShopAccess;
import com.xquark.dal.vo.ShopAccessEx;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.shop.ShopAccessService;
import com.xquark.service.shop.ShopService;

@Service("shopAccessService")
public class ShopAccessServiceImpl extends BaseServiceImpl implements ShopAccessService {

  private static Calendar cal = Calendar.getInstance();

  @Autowired
  ShopAccessMapper shopAccessMapper;

  @Autowired
  AccessReportMapper accessReportMapper;

  @Autowired
  ShopService shopService;

  @Override
  @Transactional
  public int save(ShopAccess shopAccess) {
    int rc = 0;
    cal.setTimeInMillis(System.currentTimeMillis());
    shopAccess.setDate(cal.getTime());
    shopAccess.setHour(cal.get(Calendar.HOUR_OF_DAY));
    rc = shopAccessMapper.updatePvByPrimaryKey(shopAccess);
    if (rc < 1) {
      shopAccess.setPv(1);
      rc = shopAccessMapper.insert(shopAccess);
    }
    return rc;
  }

  @Override
  public List<ShopAccess> findShopAccessByShopId(String shopId, Date date) {
    List<ShopAccess> shopAccesses = shopAccessMapper.selectShopAccessByShopId(shopId, date);
    return shopAccesses;
  }

  @Override
  public AccessReport findReportByShopIdAndDate(String shopId, Date date) {
    return accessReportMapper.selectByShopAndDate(shopId, date);
  }

  @Override
  public int insertReport(AccessReport accessReport) {
    return accessReportMapper.insert(accessReport);
  }

  @Override
  public List<ShopAccessEx> findAccessUvByShopId(String shopId, Date date) {
    return shopAccessMapper.selectAccessUvByShopId(shopId, date);
  }

  @Override
  public Long countReportByDate(Date date) {
    return accessReportMapper.countReportByDate(date);
  }

}
