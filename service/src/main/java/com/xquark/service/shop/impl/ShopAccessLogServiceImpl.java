package com.xquark.service.shop.impl;

import com.xquark.dal.mapper.ShopAccessLogMapper;
import com.xquark.dal.model.ShopAccessLog;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.shop.ShopAccessLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("shopAccessLogService")
public class ShopAccessLogServiceImpl extends BaseServiceImpl implements ShopAccessLogService {

  @Autowired
  ShopAccessLogMapper shopAccessLogMapper;

  @Override
  public int insert(ShopAccessLog team) {
    return shopAccessLogMapper.insert(team);
  }

  @Override
  public int insertOrder(ShopAccessLog shopAccessLog) {
    return shopAccessLogMapper.insert(shopAccessLog);
  }

  @Override
  public ShopAccessLog load(String id) {
    return shopAccessLogMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return shopAccessLogMapper.updateForArchive(id);
  }

  @Override
  public int update(ShopAccessLog record) {
    return shopAccessLogMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 统计某个店铺的总访问量
   */
  @Override
  public Long countByShopId(String shopId) {
    return shopAccessLogMapper.countByShopId(shopId);
  }

  /**
   * 获取用户最后一次访问的店铺
   */
  @Override
  public ShopAccessLog getLastShop(String userId) {
    return shopAccessLogMapper.getLastShop(userId);
  }

  /**
   * 获取用户第一次访问的店铺
   */
  @Override
  public ShopAccessLog getFirstShop(String userId) {
    return shopAccessLogMapper.getFirstShop(userId);
  }

  /**
   * 将店铺访问记录表中的该店铺信息都删掉
   */
  @Override
  public void deleteByShopId(String shopId) {
    shopAccessLogMapper.deleteByShopId(shopId);
  }

}
