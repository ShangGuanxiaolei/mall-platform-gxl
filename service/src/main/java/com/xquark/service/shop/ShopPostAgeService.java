package com.xquark.service.shop;

import com.xquark.dal.model.Carousel;
import com.xquark.dal.model.PostAgeSet;
import com.xquark.dal.vo.ShopPostAge;
import com.xquark.service.BaseService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface ShopPostAgeService extends BaseService {

  /**
   * 服务端分页查询数据
   */
  List<PostAgeSet> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  ShopPostAge getPostAgeByShop(String shopId);

  /**
   * 根据运费模板id获取相关信息
   */
  ShopPostAge getPostAgeById(String id);

  /**
   * 新增运费模板返回默认信息
   */
  ShopPostAge getPostAgeByDefault(String shopId);

  Boolean setPostAgeByShop(ShopPostAge shopPostAge);

  int deleteByPrimaryKey(String id);

}
