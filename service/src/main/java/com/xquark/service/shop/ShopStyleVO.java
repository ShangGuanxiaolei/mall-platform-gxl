package com.xquark.service.shop;

import com.xquark.dal.model.ShopStyle;

public class ShopStyleVO extends ShopStyle {

  private String bannerImg; // 店铺招牌图片
  private String avatar; // 店铺头像

  public String getBannerImg() {
    return bannerImg;
  }

  public void setBannerImg(String bannerImg) {
    this.bannerImg = bannerImg;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

}