package com.xquark.service.shop;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

public class BannerVO {

  private String imgKey;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String imgUrl;

  public String getImgKey() {
    return imgKey;
  }

  public void setImgKey(String imgKey) {
    this.imgKey = imgKey;
  }

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }

}
