package com.xquark.service.shop.impl;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.mapper.PostAgeSetMapper;
import com.xquark.dal.mapper.ShopMapper;
import com.xquark.dal.mapper.ZoneMapper;
import com.xquark.dal.model.PostAgeSet;
import com.xquark.dal.model.PostConf;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.Zone;
import com.xquark.dal.vo.AreaPair;
import com.xquark.dal.vo.AreaPostagePair;
import com.xquark.dal.vo.ShopPostAge;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.shop.ShopPostAgeService;
import com.xquark.service.shop.ShopService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 店铺邮费设置服务实现类
 *
 * @author
 */
@Service("shopPostAgeService")
public class ShopPostAgeServiceImpl extends BaseServiceImpl implements ShopPostAgeService {

  @Autowired
  private PostAgeSetMapper postAgeSetMapper;
  @Autowired
  private ShopService shopService;
  @Autowired
  private ShopMapper shopMapper;
  @Autowired
  private ZoneMapper zoneMapper;

  @Override
  public ShopPostAge getPostAgeByShop(String shopId) {
    if (shopId == null) {
      return null;
    }

    List<PostAgeSet> list = postAgeSetMapper.selectByShopId(shopId);
    if (list == null) {
      return getOrigPostSet(shopId);
    }

    // 新的邮费设置, 从最近更新开始找到一个能正确解析的记录
    for (PostAgeSet pas : list) {
      ShopPostAge spa = resolvePostAge(pas);
      if (spa != null) {
        return spa;
      } else {
        // TODO: 不能正确解析该记录, 可能需要删除该记录?
      }
    }
    // 否则使用老的设置
    return getOrigPostSet(shopId);
  }

  /**
   * 新增运费模板返回默认信息
   */
  @Override
  public ShopPostAge getPostAgeByDefault(String shopId) {
    return getOrigPostSet(shopId);
  }

  @Override
  public ShopPostAge getPostAgeById(String id) {
    if (StringUtils.isEmpty(id)) {
      return null;
    }

    PostAgeSet pas = postAgeSetMapper.selectByPrimaryKey(id);
    if (pas == null) {
      return null;
    }

    ShopPostAge spa = resolvePostAge(pas);
    if (spa != null) {
      return spa;
    } else {
      // TODO: 不能正确解析该记录, 可能需要删除该记录?
    }
    return spa;
  }

  // 如果将来清除shop表中邮费字段, 这里相关代码也需删除
  private ShopPostAge getOrigPostSet(String shopId) {
    ShopPostAge ret = new ShopPostAge();
    ret.setShopId(shopId);
    ret.setPostage(new BigDecimal("0"));
    ret.setPostageStatus(false);
    Shop shop = shopService.load(shopId);
    if (shop != null) {
      ret.setPostageStatus(shop.getPostageStatus());
      ret.setPostage(shop.getPostage());
      ret.setCustomizedPostage(generateCstmPostAge(shop));
    }
    return ret;
  }

  // 如果将来清除shop表中邮费字段, 这里相关代码也需删除
  private List<AreaPostagePair> generateCstmPostAge(Shop shop) {
    List<AreaPair> aps = new ArrayList<AreaPair>();
    List<PostConf> list = zoneMapper.findPostArea(shop.getFreeZone());
    if (list == null || list.isEmpty()) {
      return Collections.emptyList();
    }

    for (PostConf pc : list) {
      Zone zone = zoneMapper.selectByPrimaryKey(pc.getAssociateId());
      if (zone == null) {
        continue;
      }

      AreaPair ap = new AreaPair(zone.getId(), zone.getName());
      aps.add(ap);
    }

    List<AreaPostagePair> ret = new ArrayList<AreaPostagePair>();
    AreaPostagePair pair = new AreaPostagePair();
    pair.setAreas(aps);
    pair.setPostage(new BigDecimal("0"));
    // 因为老版邮费只有包邮, 转为自定义邮费的0元区域
    ret.add(pair);
    return ret;
  }

  @SuppressWarnings("static-access")
  private ShopPostAge resolvePostAge(PostAgeSet postAgeSet) {
    if (postAgeSet == null || postAgeSet.getShopId() == null) {
      return null;
    }

    ShopPostAge ret;
    String jsonStr = postAgeSet.getPostageSet();
    JSONObject object = JSONObject.parseObject(jsonStr);

    try {
      ret = object.parseObject(jsonStr, ShopPostAge.class);
    } catch (Exception e) {
      log.warn("postageset string can not resolve...");
      ret = null;
      // TODO: handle exception
    }

    if (ret != null && ret.getCustomizedPostage() != null &&
        !ret.getCustomizedPostage().isEmpty() &&
        ret.getCustomizedPostage().get(0).getAreas() == null) {
      ret.setCustomizedPostage(null);
    }
    ret.setName(postAgeSet.getName());
    ret.setType(postAgeSet.getType());
    return ret;
  }

  private List<AreaPostagePair> getCustomizedPostage(List<AreaPostagePair> list) {
    // 列表size 为0 或者 第一个自定义区域元素为空或大小为0 都算清空自定义邮费
    if (list == null || list.isEmpty() || list.get(0).getAreas() == null || list.get(0).getAreas()
        .isEmpty()) {
      return Collections.emptyList();
    }

    return list;

  }

  @Override
  @Transactional
  public Boolean setPostAgeByShop(ShopPostAge shopPostAge) {
    if (shopPostAge == null || shopPostAge.getShopId() == null) {
      return false;
    }
    ShopPostAge spa = this.getPostAgeByShop(shopPostAge.getShopId());
    spa.setPostageStatus(shopPostAge.getPostageStatus());
    if (shopPostAge.getPostageStatus() == null || !shopPostAge.getPostageStatus()) {
      // 更新邮费开关
      spa.setPostageStatus(false);
    } else {
      // 更新全局邮费
      if (shopPostAge.getPostage() != null) {  // 可以设置0
        spa.setPostage(shopPostAge.getPostage());
        spa.setDefaultFirst(shopPostAge.getDefaultFirst());
        spa.setDefaultSecond(shopPostAge.getDefaultSecond());
        spa.setDefaultFirstFee(shopPostAge.getDefaultFirstFee());
        spa.setDefaultSecondFee(shopPostAge.getDefaultSecondFee());
        spa.setFreeShippingFirstPay(shopPostAge.getFreeShippingFirstPay());
      }
      // 更新下单超过指定金额包邮
      if (shopPostAge.getFreeShippingPrice() != null) {
        if (shopPostAge.getFreeShippingPrice().compareTo(new BigDecimal("-1")) == 0) {
          spa.setFreeShippingPrice(null);
        } else {
          spa.setFreeShippingPrice(shopPostAge.getFreeShippingPrice());
        }
      }
      // 更新包邮商品
      List<String> goods = shopPostAge.getFreeShippingGoods();
      if (goods != null) {
        if (goods.isEmpty() ||
            (goods.size() == 1 && goods.get(0).equalsIgnoreCase("delete"))) {  // delete
          spa.setFreeShippingGoods(null);
        } else {
          spa.setFreeShippingGoods(goods);
        }
      }
      // 更新自定义包邮设置
      if (shopPostAge.getCustomizedPostage() != null) {
        spa.setCustomizedPostage(getCustomizedPostage(shopPostAge.getCustomizedPostage()));
      }
    }

    String jsonStr = JSONObject.toJSONString(spa);
    // 如果id为空，说明是新增运费模板
    if (StringUtils.isEmpty(shopPostAge.getId())) {
      PostAgeSet pas = new PostAgeSet();
      pas.setShopId(shopPostAge.getShopId());
      pas.setPostageSet(jsonStr);
      pas.setName(shopPostAge.getName());
      pas.setType(shopPostAge.getType());
      postAgeSetMapper.insert(pas);
    }
    // 否则为更新运费模板
    else {
      PostAgeSet pas = new PostAgeSet();
      pas.setId(shopPostAge.getId());
      pas.setPostageSet(jsonStr);
      pas.setName(shopPostAge.getName());
      pas.setType(shopPostAge.getType());
      postAgeSetMapper.update(pas);
    }

    /**List<PostAgeSet> list = postAgeSetMapper.selectByShopId(shopPostAge.getShopId()); // 按创建时间排序
     if (list != null && list.size() > 1) {
     for (int i = 1; i < list.size(); i++) {
     postAgeSetMapper.deleteByPrimaryKey(list.get(i).getId());
     }
     }**/

    if (shopPostAge.getPostage() != null) {
      Shop shop = shopService.load(shopPostAge.getShopId());
      shop.setPostageStatus(shopPostAge.getPostageStatus());  // 不能一直维护老数据, 需要想办法去掉依赖, 说服客户端
      shop.setPostage(shopPostAge.getPostage());
      shopMapper.updateByPrimaryKeySelective(shop);
    }
    return true;
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<PostAgeSet> list(Pageable pager, Map<String, Object> params) {
    return postAgeSetMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return postAgeSetMapper.selectCnt(params);
  }

  @Override
  public int deleteByPrimaryKey(String id) {
    return postAgeSetMapper.deleteByPrimaryKey(id);
  }

}
