package com.xquark.service.shop;

import java.util.Date;
import java.util.List;

import com.xquark.dal.model.AccessReport;
import com.xquark.dal.model.ShopAccess;
import com.xquark.dal.vo.ShopAccessEx;
import com.xquark.service.BaseService;

public interface ShopAccessService extends BaseService {

  int save(ShopAccess shopAccess);

  List<ShopAccess> findShopAccessByShopId(String shopId, Date date);

  AccessReport findReportByShopIdAndDate(String shopId, Date date);

  Long countReportByDate(Date date);

  int insertReport(AccessReport accessReport);

  List<ShopAccessEx> findAccessUvByShopId(String shopId, Date date);
}
