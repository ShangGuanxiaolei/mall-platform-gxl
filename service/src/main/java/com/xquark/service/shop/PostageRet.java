package com.xquark.service.shop;

import com.xquark.dal.type.LogisticDiscountType;
import java.math.BigDecimal;

/**
 * created by
 *
 * @author wangxinhua at 18-7-1 下午2:39
 */
public class PostageRet {

  private final BigDecimal logisticsFee;

  private final BigDecimal logisticsDiscount;

  private final LogisticDiscountType logisticDiscountType;

  public PostageRet(BigDecimal logisticsFee, BigDecimal logisticsDiscount) {
    this(logisticsFee, logisticsDiscount, null);
  }

  public PostageRet(BigDecimal logisticsFee, BigDecimal logisticsDiscount,
      LogisticDiscountType logisticDiscountType) {
    this.logisticsFee = logisticsFee;
    this.logisticsDiscount = logisticsDiscount;
    this.logisticDiscountType = logisticDiscountType;
  }

  public BigDecimal getLogisticsFee() {
    return logisticsFee;
  }

  public BigDecimal getLogisticsDiscount() {
    return logisticsDiscount;
  }

  public LogisticDiscountType getLogisticDiscountType() {
    return logisticDiscountType;
  }

  public static PostageRet empty() {
    return new PostageRet(BigDecimal.ZERO, BigDecimal.ZERO, null);
  }
}
