package com.xquark.service.shop;

import com.xquark.dal.model.Shop;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.Tag;
import com.xquark.dal.model.ThirdCommission;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserTwitter;
import com.xquark.dal.vo.AreaPostagePair;
import com.xquark.dal.vo.ShopAdmin;
import com.xquark.service.ArchivableEntityService;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

public interface ShopService extends ArchivableEntityService<Shop> {

  /**
   * 创建店铺
   */
  Shop create(Shop shop);

  /**
   * 通过 id 查找店铺
   */
  Shop load(String id);

  /**
   * 获取全局唯一的总店
   */
  Shop loadRootShop();

  /**
   * 通过 用户id 查找店铺
   */
  Shop findByUser(String userId);

  /**
   * 通过店铺名称获得店铺
   */
  Shop findByName(String shopName);

  /**
   * 获取当前登录用户的店铺
   */
  Shop mine();

  /**
   * 后台管理 更新店铺
   */
  int updateByAdmin(Shop shop);

  /**
   * 加载店铺的样式信息（通过当前用户）
   */
  ShopStyleVO loadShopStyle();

  /**
   * 通过店铺的Id获取店铺的样式信息
   */
  ShopStyleVO loadShopStyle(String id);

  /**
   * 更新店铺的样式
   */
  boolean updateShopStyles(ShopStyleVO vo);

  /**
   * 店铺统计 今天的订单数，成交数，待付款，待发货，访客
   */
  Map<String, Object> loadStatistics();

  /**
   * 管理员
   */
  Long countShopsByAdmin(Map<String, Object> params);

  Long countShopsByActivity(Map<String, Object> params, String activityId);

  /**
   * 管理员查询店铺
   */
  List<ShopAdmin> listShopsByAdmin(Map<String, Object> params, Pageable pageable);


  List<ShopAdmin> listShopsByActivity(Map<String, Object> params, Pageable pageable,
      String activityid);


  /**
   * 更新店铺
   */
  int update(Shop shop);

  /**
   * 开通担保交易
   */
  Boolean openDanbao();

  /**
   * 关闭担保交易
   */
  Boolean closeDanbao();

  /**
   * 查询店铺
   */
  List<Shop> listAll(Pageable pageable);

  /**
   * 统计店铺的总数量
   */
  Long countByShop();

  // 标签相关的

  /**
   * 通过店铺 Id 查询店铺商品标签
   */
  List<Tag> findTagsByShopId(String shopId, String tag);

  /**
   * 获取用户创建的所有标签
   */
  List<Tag> listUserTags();

  List<Tag> getTagsByUserId(String userId);

  /**
   * 添加当前用户的标签
   */
  Tag saveUserTag(String tag);

  /**
   * 更新标签
   */
  Tag updateUserTag(String id, String tag);

  /**
   * 删除当前用户的标签
   */
  void removeUserTag(String tag);

  /**
   * 获取验证码
   */
  long getRnd();

  /**
   * 店铺搬家
   */
  Map<String, Object> moveThirdShopProducts(User user, long rnd, String itemId, int deviceType,
      int option);

  Map<String, Object> moveThirdShopProducts(User user, String shopUrl, String shopId,
      int deviceTYpe, int option);

  Map<String, Object> moveThirdItem(User user, String itemId);

  /**
   * 新店铺搬家
   */
  /**
   * 手动搬店铺
   */
  Map<String, Object> moveShop(String ouerShopId, String shopUrl);

  /**
   * 手动搬指定商品组图
   *
   * @param shopType 1:taobao, 2:tmall
   */
  Map<String, Object> moveItemGroupImg(String ouerShopId, String itemId, int shopType);

  /**
   * 手动搬指定商品详情
   *
   * @param shopType 1:taobao, 2:tmall
   */
  Map<String, Object> moveItemDesc(String ouerShopId, String itemId, int shopType);

  /**
   * 手动搬指定商品
   *
   * @param shopType 1:taobao, 2:tmall
   */
  Map<String, Object> moveItem(String ouerShopId, String itemId, int shopType);

  /**
   * 手动从newspider库同步到快店库
   *
   * @param type 0:正常搬同步流程, 1:手动搬同步流程, 2:详情同步流程, 3:组图同步流程, 4, sku同步流程 (参见 SpideItemType)
   */
  Map<String, Object> moveSyncFromSpider(String ouerShopId, String itemId, int type);


  /**
   * 获取邮费(是否免邮)
   */
  public BigDecimal getCurPostage(String shopId, String zoneId);

  /**
   * 获取店铺邮费
   *
   * @return not-null
   */
  public BigDecimal getShopPostage(String shopId, String zoneId, List<String> skus,
      BigDecimal totalFee);

  /**
   * 获取店铺邮费 for wechat
   *
   * @return not-null
   */
  BigDecimal getShopPostageForWechat(String shopId, String zoneId, List<Sku> skus,
      BigDecimal totalFee, Integer amount);

  /**
   * 根据订单内每个商品对应的邮费设置计算整个订单邮费
   *
   * @return not-null
   */
  PostageRet getShopPostageByProduct(String shopId, String zoneId, List<Sku> skus);

  boolean isSyncedToXiangqu(String shopId);

  boolean delete(String shopId, String opRemark);

  boolean close(String shopId, String opRemark);

  boolean unClose(String shopId, String opRemark);

  boolean undelete(String shopId, String opRemark);

  boolean saveFragment(boolean fragmentStatus);

  //void autoMoveProductByTask();

  String checkRepeatMoving();

  // 获取自定义邮费（某个地区是否在自定义邮费中设置，有则返回自定义的邮费）
  BigDecimal getCustomizedPostage(List<AreaPostagePair> customizedPostage,
      String zoneId);

  Long selectShopOwner(String shopId);

  List<Shop> listNoCodeShops();

  void addCode(String... ids);

  List<Sku> listNoCodeSkus();

  void addSkuCode(String[] ids);

  boolean updateThirdCommission(ThirdCommission... form);

  /**
   * 优惠券管理获取所有店铺信息
   *
   * @return 所有店铺信息
   */
  List<Shop> listAllShops();

  /**
   * 通过bos开通大B的基本信息
   *
   * @return 所有店铺信息
   */
  Shop createUserAndShopByBos(String phone, String password, String shopName);

  /**
   * 通过我要分销创建分销店铺信息
   *
   * @return 店铺信息
   */
  Shop createDistributeShop(UserTwitter twitterApply);

  /**
   * 我的店铺单品排序
   */
  int sortShopProducts(List<String> sortIds, String shopId);

  Map<String, Object> loadB2BStatistics();
}
