package com.xquark.service.shop.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.mapper.ProdSyncMapper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.mapper.ShopAccessMapper;
import com.xquark.dal.mapper.ShopMapper;
import com.xquark.dal.mapper.ShopStyleMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.mapper.TagMapper;
import com.xquark.dal.mapper.ZoneMapper;
import com.xquark.dal.model.PostConf;
import com.xquark.dal.model.ProdSync;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ShopStyle;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.SystemRegion;
import com.xquark.dal.model.Tag;
import com.xquark.dal.model.ThirdCommission;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserTwitter;
import com.xquark.dal.model.Zone;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.ShopStatus;
import com.xquark.dal.status.SyncAuditStatus;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.type.LogisticDiscountType;
import com.xquark.dal.type.LogisticsType;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.dal.vo.AreaPair;
import com.xquark.dal.vo.AreaPostagePair;
import com.xquark.dal.vo.ShopAdmin;
import com.xquark.dal.vo.ShopPostAge;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.service.account.AccountService;
import com.xquark.service.account.SubAccountLogService;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.commission.CommissionService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.rocketmq.SyncRocketMq;
import com.xquark.service.shop.PostageRet;
import com.xquark.service.shop.ShopPostAgeService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shop.ShopStyleVO;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.syncevent.KDSyncData;
import com.xquark.service.syncevent.SyncEventService;
import com.xquark.service.syncevent.SyncEventType;
import com.xquark.service.systemRegion.SystemRegionService;
import com.xquark.service.user.UserService;
import com.xquark.service.wechat.ShopWechatSettingService;
import com.xquark.utils.MD5Util;
import com.xquark.utils.ResourceResolver;
import com.xquark.utils.ThreadLocalRandom;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("shopService")
public class ShopServiceImpl extends BaseServiceImpl implements ShopService, InitializingBean {

  @Autowired
  private ShopMapper shopMapper;
  @Autowired
  private ShopAccessMapper shopAccessMapper;
  @Autowired
  private ShopStyleMapper shopStyleMapper;
  @Autowired
  private ShopPostAgeService shopPostAgeService;
  @Autowired
  private OrderMapper orderMapper;
  @Autowired
  private TagMapper tagMapper;
  @Autowired
  private UserService userService;
  @Autowired
  private ZoneMapper zoneMapper;
  @Autowired
  private SyncEventService syncEventService;
  @Autowired
  private SubAccountLogService subAccountLogService;
  @Autowired
  private AccountService accountService;
  @Autowired
  private ProdSyncMapper prodSyncMapper;
  @Autowired
  private SkuMapper skuMapper;
  @Autowired
  private ShopWechatSettingService shopWechatSettingService;
  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private CommissionService commissionService;

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private ProductMapper productMapper;

  @Autowired
  private SystemRegionService systemRegionService;

  @Autowired
  private SyncRocketMq syncRocketMq;

  @Autowired
  private CustomerProfileService customerProfileService;

  @Value("${shop.postage.citys}")
  String postageCitys;

  @Value("${sync.scheme.switch}")
  private Integer syncSwitch;

  @Value("${rnd.timeout}")
  private int rndTimeout;
  @Value("${rnd.min}")
  private int rndMin;
  @Value("${rnd.max}")
  private int rndMax;
  @Value("${spider.url}")
  private String url;
  @Value("${site.web.host.name}")
  String siteHost;

  // 缓存全局的shop
  private Shop rootShop;

  @Override
  public int insert(Shop e) {
    e.setStatus(ShopStatus.ACTIVE);
    e.setDanbao(true);
    // 店铺默认头像
    if (StringUtils.isEmpty(e.getImg())) {
      e.setImg("http://images.handeson.com/Fnr5rtXEssim1NVarB-sCoa24ZJf");
    }
    int id = shopMapper.insert(e);
    shopMapper.addCode(e.getId());

    return id;
  }

  @Override
  public int insertOrder(Shop e) {
    e.setStatus(ShopStatus.ACTIVE);
    e.setDanbao(true);
    // 店铺默认头像
    if (StringUtils.isEmpty(e.getImg())) {
      e.setImg("http://images.handeson.com/Fnr5rtXEssim1NVarB-sCoa24ZJf");
    }
    int id = shopMapper.insert(e);
    shopMapper.addCode(e.getId());

    return id;
  }

  @Override
  @Transactional
  public Shop create(Shop shop) {
    //Shop chkShop = findByName(shop.getName());
    /**if (chkShop != null) {
     throw new BizException(GlobalErrorCode.UNKNOWN,
     "商铺已经存在 shopName=[" + shop.getName() + "]");
     }**/

    String userId = shop.getOwnerId();
    if (StringUtils.isEmpty(userId)) {
      userId = ((User) this.getCurrentUser()).getId();
    }

    shop.setOwnerId(userId);
    Shop existShop = shopMapper.selectByUserId(userId);
    if (existShop == null) {
      insert(shop);
      User userUpdate = new User();
      userUpdate.setId(userId);
      userUpdate.setShopId(shop.getId());
      userService.updateByBosUserInfo(userUpdate);
      //user.setShopId(shop.getId());

      // 暂时写死rootshopid modify by chh 2016-12-02
      String rootShopId = "aj6wd1mm";
      // 增加shop的同时，也要维护shoptree对应关系
      ShopTree shopTree = new ShopTree();
      shopTree.setRootShopId(rootShopId);
      shopTree.setAncestorShopId(rootShopId);
      shopTree.setDescendantShopId(shop.getId());
      shopTreeService.insertShopTree(shopTree);

    } else {
      //user.setShopId(existShop.getId());
      shop = existShop;
    }
    return shop;
  }

  @Override
  public Shop load(String id) {
    Shop shop = shopMapper.selectByPrimaryKey(id);
    // 获取店铺banner图片路径
    if (shop != null) {
      String banner = shop.getBanner();
      ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
          .getBean("resourceFacade");
      String url = resourceFacade.resolveUrl(banner);
      shop.setBannerUrl(url);
    }
    return shop;
  }

  @Override
  public Shop loadRootShop() {
    // @see afterPropertiesSet
    return rootShop;
  }

  @Override
  public Shop findByUser(String userId) {
    return shopMapper.selectByUserId(userId);
  }

  @Override
  public Shop findByName(String shopName) {
    return shopMapper.selectByShopName(shopName);
  }

  @Override
  public Shop mine() {
    Shop shop = shopMapper.selectByUserId(getCurrentUser().getId());
    return shop;
  }

  @Override
  @Transactional
  public int update(Shop shop) {
    Shop mine = mine();
    shop.setId(mine.getId());
    if (!shop.getId().equals(mine.getId())) {
      return 0;
    }
    Shop chkShop = findByName(shop.getName());
    if (chkShop != null && !chkShop.getId().equals(shop.getId())) {
      return -1;
    }

    if (shop.getBulletin() != null
        && !StringUtils.defaultString(shop.getBulletin()).equals(
        StringUtils.defaultString(mine.getBulletin()))) {
      shop.setBulletinAt(Calendar.getInstance().getTime());
    }

    if (false == checkPostFreeZone(shop)) {
      return -2;
    }

    if (syncSwitch == 0) {
      KDSyncData ev = new KDSyncData();
      ev.setSiteHost(siteHost);
      ev.setUserPhone(getCurrentUser().getPhone());
      ev.setTimeStamp(new Date());
      ev.setPartnerFlag(KDSyncData.XIANGQU_DST);
      ev.setEvType(SyncEventType.SHOP_UPDATE);
      ev.setShop(chkShop);
      syncEventService.publishEvent(ev);
    } else {
      /**SyncMqEvent event = new SyncMqEvent();
       List<String> ids = new ArrayList<String>();
       ids.add(mine.getId());
       event.setIds(ids);
       event.setEvent(2);
       event.setType(SyncEvType.EV_SHOP.ordinal());
       event.setTimestamp(new Date().getTime());
       syncRocketMq.sendToMQ(event);**/
    }
    return shopMapper.updateByPrimaryKeySelective(shop);
  }

  @Override
  public BigDecimal getCurPostage(String shopId, String zoneId) {
    Shop shop = load(shopId);
    BigDecimal postAge = BigDecimal.ZERO;

    Zone pZone = null;
    Zone ppZone = null;
    pZone = zoneMapper.findParent(zoneId);
    if (pZone != null) {
      ppZone = zoneMapper.findParent(pZone.getId());
    }

    List<String> freeZone = parserPostFreeStr(shop.getFreeZone());
    List<PostConf> freePost = new ArrayList<PostConf>();

    for (String zone : freeZone) {
      // 鍙栧緱搴楅摵璁剧疆鍏嶉偖鍖哄煙瀵瑰簲鐨刬d鍒楄〃
      List<PostConf> aPosts = zoneMapper.findPostArea(zone);
      if (aPosts != null) {
        freePost.addAll(aPosts);
      }
    }

    for (PostConf aFree : freePost) {
      if (aFree.getAssociateId().equals(
          ppZone == null ? "" : ppZone.getId())
          || aFree.getAssociateId().equals(
          pZone == null ? "" : pZone.getId())
          || aFree.getAssociateId().equals(
          zoneId == null ? "" : zoneId)) {
        return postAge;
      }
    }

    if (shop.getPostageStatus()) {
      postAge = shop.getPostage();
    }

    return postAge;
  }

  private Boolean checkPostFreeZone(Shop shop) {
    Boolean ret = true;
    List<String> freeZone = parserPostFreeStr(shop.getFreeZone());
    if (freeZone == null || freeZone.size() == 0) {
      return ret;
    }

    for (String zone : freeZone) {
      if (zone.equals("无")) {
        break;
      }
      List<PostConf> aZones = zoneMapper.findPostArea(zone);
      if (aZones.size() == 0) {
        ret = false;
        break;
      }
    }

    return ret;
  }

  /**
   * <h4>鑾峰彇搴楅摵鐨勫厤閭湴鍖�/h4><br>
   * 濡傛灉鏈夊涓厤閭湴鍖轰互鍧氱嚎鍒嗛殧 '|', 涓�湡鍙細鏈変竴涓厤閭湴鍖�<br> 浣嗗彲鑳戒細鍖呮嫭 娴欐睙娌�闀夸笁瑙掔瓑澶氬湴鍖哄湴鍚�
   */
  private List<String> parserPostFreeStr(String postStr) {
    if (StringUtils.isBlank(postStr)) {
      return Collections.emptyList();
    }

    String[] regions = postStr.split("\\|");
    return Arrays.asList(regions);
  }

  @Override
  public int delete(String id) {
    Shop shop = load(id);
    Shop mine = mine();
    if (!shop.getId().equals(mine.getId())) {
      return 0;
    }
    return shopMapper.deleteByPrimaryKey(id, "鍗栧鑷繁鍏抽棴");
  }

  @Override
  public int undelete(String id) {
    throw new BizException(GlobalErrorCode.UNAUTHORIZED, "a");
    //
    //
    // Shop shop = load(id);
    // Shop mine = mine();
    // if (!shop.getId().equals(mine.getId())) {
    // return 0;
    // }
    // return shopMapper.undeleteByPrimaryKey(id, "");
  }

  @Override
  public List<Tag> findTagsByShopId(String id, String tag) {
    if (StringUtils.isBlank(tag)) {
      tag = null;
    } else {
      tag = "%" + tag + "%";
    }

    return tagMapper.selectByShopId(id, tag);
  }

  @Override
  public List<Shop> listAll(Pageable pageable) {
    return shopMapper.selectAll(pageable);
  }

  @Override
  public Long countByShop() {
    return shopMapper.countByShop();
  }

  @Override
  public Boolean openDanbao() {
    Shop shop = mine();
    shop.setDanbao(true);
    return shopMapper.updateByPrimaryKeySelective(shop) == 1;
  }

  @Override
  public Boolean closeDanbao() {
    Shop shop = mine();
    shop.setDanbao(false);
    return shopMapper.updateByPrimaryKeySelective(shop) == 1;
  }

  static String RND_KEY_FORMAT = "rnd_uid_%s_sid_%s";

  static String getRndKey(User user) {
    return String.format(RND_KEY_FORMAT, user.getId(), user.getShopId());
  }


  @Override
  public long getRnd() {
    final long rnd = ThreadLocalRandom.current().nextLong(this.rndMin,
        this.rndMax);
    final String key = getRndKey((User) this.getCurrentUser());
    try {
//      if (this.memcachedClient.set(key, this.rndTimeout, rnd)) {
//        return rnd;
//      }
      return rnd;
//      log.warn("Failed to add to Memcached, exists key:{}", key);
    } catch (Exception e) {
      log.error("Error to add to Memcached", e);
    }
    return rnd;
  }

  @Override
  public String checkRepeatMoving() {
    final Map<String, String> params = new HashMap<String, String>();
    params.put("ouerUserId", getCurrentUser().getId());
    params.put("ouerShopId", getCurrentUser().getShopId());
    log.info(params.toString());
    final HttpInvokeResult result = PoolingHttpClients.post(url
        + "/shop/exists", params);

    if (!result.isOK()) {
      log.error("Error to moveProduct:" + result, result.getException()
          + " params:" + params.toString());
      return StringUtils.EMPTY;
    }

    final JSONObject obj = JSONObject.parseObject(result.getContent());
    if (obj.getIntValue("errorCode") != 200
        || obj.getJSONObject("data").getIntValue("statusCode") != 200) {
      log.error("Failed to qeury movestatus:" + result + " params:"
          + params.toString());
      return StringUtils.EMPTY;
    }

    JSONArray aJson = obj.getJSONObject("data").getJSONArray("results");
    if (aJson != null && aJson.size() > 0) {
      StringBuffer retString = new StringBuffer();
      for (int i = 0; i < aJson.size(); i++) {
        retString.append(aJson.getJSONObject(i).getString("shopUrl")).append("|");
      }
      return retString.toString();
    }

    log.error("not exists, so do the first move" + params.toString());
    return StringUtils.EMPTY;
  }

  @Override
  public Map<String, Object> moveThirdShopProducts(User user, long rnd,
      String itemId, int deviceType, int option) {
    return this.moveThirdShopProducts(user, rnd, itemId, 3, deviceType, option);
  }

  @Override
  public Map<String, Object> moveThirdShopProducts(User user, String shopUrl,
      String shopId, int deviceType, int option) {
    return this.moveThirdShopProducts(user, shopUrl, 3, deviceType, option);
  }

  public Map<String, Object> moveThirdShopProducts(User user, String shopUrl,
      int shopType, int deviceType, int option) {

    final Map<String, String> params = new HashMap<String, String>();
    params.put("ouerUserId", user.getId());
    params.put("ouerShopId", user.getShopId());
    // params.put("shopType", String.valueOf(shopType));
    params.put("option", String.valueOf(option));
    params.put("url", shopUrl);

    // 鎼
    final Map<String, Object> result = new HashMap<String, Object>();
    log.info(params.toString());
    final HttpInvokeResult rslt = PoolingHttpClients.post(url
        + "/moveProduct/lgnu", params);
    if (!rslt.isOK()) {
      log.error("Error to moveProduct:" + rslt, rslt.getException()
          + "params:" + params.toString());
      result.put("statusCode", "501");
      return result;
    }

    final JSONObject obj = JSONObject.parseObject(rslt.getContent());
    if (obj.getIntValue("errorCode") != 200) {
      log.error("Failed to qeury movestatus:" + rslt + " params:"
          + params.toString());
      result.put("statusCode", "502");
      return result;
    }

    if (obj.getJSONObject("data").getIntValue("statusCode") != 200) {
      if (obj.getJSONObject("data").getIntValue("statusCode") == 601) {
        log.error("shop not exists:" + rslt + " params:"
            + params.toString());
        result.put("statusCode", "601");
        return result;
      } else {
        log.error("unknow error:" + rslt + " params:"
            + params.toString());
        result.put("statusCode", "602");
        return result;
      }
    }

//		// 鏇存柊prop_updated鏃堕棿
//		// 鏄惁鍦ㄥ畾鏃舵惉瀹跺垪琛ㄤ腑
//		if (shopMapper.isPropUpdated(user.getShopId(), shopUrl) > 0 ? true : false) {
//			int ret = shopMapper.updateAutoShopPropAt(user.getShopId());
//			if (ret > 0)
//				log.info("Success to update auto_updated_at");
//			else
//				log.error("Error to update auto_updated_at");
//		}

    result.put("statusCode", "200");
    result.put("shopUrl", shopUrl);
    result.put("shopType", shopType);
    result.put("shopId", user.getShopId());

    return result;
  }

  @Override
  public Map<String, Object> moveThirdItem(User user, String ItemId) {

    final Map<String, String> params = new HashMap<String, String>();
    params.put("ouerUserId", user.getId());
    params.put("ouerShopId", user.getShopId());
    params.put("url", "http://item.taobao.com/item.htm?id=" + ItemId);

    // 鎼
    final Map<String, Object> result = new HashMap<String, Object>();
    log.info(params.toString());
    final HttpInvokeResult rslt = PoolingHttpClients.post(url
        + "/moveItem/url/async", params);
    if (!rslt.isOK()) {
      log.error("Error to moveItem:" + rslt, rslt.getException()
          + "params:" + params.toString());
      result.put("statusCode", "501");
      return result;
    }

    final JSONObject obj = JSONObject.parseObject(rslt.getContent());
    if (obj.getIntValue("errorCode") != 200) {
      log.error("Failed to qeury movestatus:" + rslt + " params:"
          + params.toString());
      result.put("statusCode", "502");
      return result;
    }

    if (obj.getJSONObject("data").getIntValue("statusCode") != 200) {
      if (obj.getJSONObject("data").getIntValue("statusCode") == 601) {
        log.error("shop not exists:" + rslt + " params:"
            + params.toString());
        result.put("statusCode", "601");
        return result;
      } else {
        log.error("unknow error:" + rslt + " params:"
            + params.toString());
        result.put("statusCode", "602");
        return result;
      }
    }

    result.put("statusCode", "200");
    result.put("shopId", user.getShopId());

    return result;
  }

  /**
   * <pre>
   * 绗笁鏂瑰簵閾烘惉瀹�
   * result.statusCode
   * 	200-姝ｅ父
   * 	404-鎵句笉鍒伴殢鏈虹爜
   * 	500-Memcached鏁版嵁鑾峰彇寮傚父
   * 	501-Spider鏁版嵁鑾峰彇寮傚父
   * 	600-Memcached鏁版嵁姣斿閿欒
   * 	601-鏃犳硶纭畾绗笁鏂瑰簵閾�
   * result.shopName
   * 	搴楅摵鍚嶇О
   * result.shopType
   * 	搴楅摵绫诲瀷 1-taobao; 2-tmall; 3-taobao_or_tmall
   * </pre>
   *
   * @param shopType 1-taobao; 2-tmall; 3-taobao_or_tmall
   */
  public Map<String, Object> moveThirdShopProducts(User user, long rnd,
      String itemId, int shopType, int deviceType, int option) {
    final Map<String, Object> result = new HashMap<String, Object>();

    // 涓庣紦瀛樹腑鐨勫�鍋氭瘮瀵�
    final String key = getRndKey(user);
    try {
//      final Object value = this.memcachedClient.get(key);
      final Object value = null;

      if (value == null) {
        result.put("statusCode", 404);
        return result;
      }

      if (Integer.parseInt(value.toString()) != rnd) {
        result.put("statusCode", 600);
        return result;
      }
    } catch (Exception e) {
      log.error("Error to get from Memcached", e);
      result.put("statusCode", 500);
      return result;
    }

    //
    final Map<String, String> params = new HashMap<String, String>();
    params.put("rnd", String.valueOf(rnd));
    params.put("ouerUserId", user.getId());
    params.put("ouerShopId", user.getShopId());
    params.put("shopType", String.valueOf(shopType));
    params.put("itemId", itemId);
    params.put("deviceType", String.valueOf(deviceType));

    // 鎼
    final HttpInvokeResult rslt = PoolingHttpClients.post(url
        + "/moveProduct/rnd", params);
    log.info(params.toString());
    if (!rslt.isOK()) {
      log.error("Error to moveProduct:" + rslt + "params:"
          + params.toString());
      result.put("statusCode", "501");
      return result;
    }

    final JSONObject obj = JSONObject.parseObject(rslt.getContent());
    // 姝ゅ瓧娈靛繀鏈�
    final int errorCode = obj.getIntValue("errorCode");
    if (errorCode != 200) {
      log.error("Failed to moveProduct:" + rslt + " params:"
          + params.toString());
      result.put("statusCode", "502");
      return result;
    }
    // 姝ゅ瓧娈靛繀鏈�
    final int statusCode = obj.getJSONObject("data").getIntValue(
        "statusCode");
    // 鏃犳硶纭畾蹇簵搴楅摵 or 鐢ㄦ埛杈撳叆鐨勫晢鍝両D鏈夎
    if (statusCode != 200) {
      log.error("Failed to confirm Shop:" + rslt + " params:"
          + params.toString());
      result.put("statusCode", "601");
      return result;
    }

    result.put("statusCode", "200");
    result.put("shopName", obj.getJSONObject("data").getString("shopName"));
    result.put("shopType", obj.getJSONObject("data")
        .getIntValue("shopType"));

    return result;
  }

  @Override
  public boolean updateShopStyles(ShopStyleVO vo) {
    // update shop
    Shop shop = load(getCurrentUser().getShopId());
    Shop shop4Update = new Shop();
    shop4Update.setId(shop.getId());
    shop4Update.setImg(vo.getAvatar());
    shop4Update.setBanner(vo.getBannerImg());
    int flag1 = shopMapper.updateByPrimaryKeySelective(shop4Update);

    // update shop settings
    int flag2 = 0;
    ShopStyle ss = shopStyleMapper.selectByPrimaryKey(shop.getId());
    boolean create = false;
    if (ss == null) {
      create = true;
      ss = new ShopStyle();
      ss.setShopId(shop.getId());
    }

    ss.setAvatarStyle(vo.getAvatarStyle());
    ss.setBackgroundColor(vo.getBackgroundColor());
    ss.setFontColor(vo.getFontColor());
    ss.setFontFamily(vo.getFontFamily());
    ss.setListView(vo.getListView());

    if (create) {
      flag2 = shopStyleMapper.insert(ss);
    } else {
      flag2 = shopStyleMapper.update(ss);
    }

    return flag1 > 0 && flag2 > 0;
  }

  @Override
  public ShopStyleVO loadShopStyle() {
    return loadShopStyle(getCurrentUser().getShopId());
  }

  @Override
  public ShopStyleVO loadShopStyle(String id) {
    ShopStyle ss = shopStyleMapper.selectByPrimaryKey(id);
    Shop shop = shopMapper.selectByPrimaryKey(id);

    ShopStyleVO r = new ShopStyleVO();
    if (ss != null) {
      BeanUtils.copyProperties(ss, r);
    } else {
      r.setShopId(shop.getId());
    }
    r.setAvatar(shop.getImg());
    r.setBannerImg(shop.getBanner());
    return r;
  }

  // @Override
  // public int updateImgByPrimaryKey(String id, String img) {
  // if(StringUtils.isBlank(id))
  // return 0;
  // Shop shop = load(id);
  // Shop mine = mine();
  // if (!shop.getId().equals(mine.getId())) {
  // return 0;
  // }
  // return shopMapper.updateImgByPrimaryKey(id, img);
  // }

  // @Override
  // public int updateNameByPrimaryKey(String id, String name) {
  // if(StringUtils.isBlank(id) || StringUtils.isBlank(name))
  // return 0;
  // Shop shop = load(id);
  // Shop mine = mine();
  // if (!shop.getId().equals(mine.getId())) {
  // return 0;
  // }
  // Shop chkShop = findByName(name);
  // if (chkShop != null){
  // return (chkShop.getId().equals(id)?1:0);
  // }
  // return shopMapper.updateNameByPrimaryKey(id, name);
  // }
  //
  // @Override
  // public int updateWechatByPrimaryKey(String id, String wechat) {
  // if(StringUtils.isBlank(id))
  // return 0;
  // Shop shop = load(id);
  // Shop mine = mine();
  // if (!shop.getId().equals(mine.getId())) {
  // return 0;
  // }
  // return shopMapper.updateWechatByPrimaryKey(id, wechat);
  // }
  //
  // @Override
  // public int updateDescByPrimaryKey(String id, String description) {
  // if(StringUtils.isBlank(id))
  // return 0;
  // Shop shop = load(id);
  // Shop mine = mine();
  // if (!shop.getId().equals(mine.getId())) {
  // return 0;
  // }
  // return shopMapper.updateDescByPrimaryKey(id, description);
  // }
  //
  // @Override
  // public int updateBulletinByPrimaryKey(String id, String bulletin) {
  // if(StringUtils.isBlank(id))
  // return 0;
  // Shop shop = load(id);
  // Shop mine = mine();
  // if (!shop.getId().equals(mine.getId())) {
  // return 0;
  // }
  // return shopMapper.updateBulletinByPrimaryKey(id, bulletin);
  // }
  //
  // @Override
  // public int updateLocalByPrimaryKey(String id, Long provinceId, Long
  // cityId) {
  // if(StringUtils.isBlank(id))
  // return 0;
  // Shop shop = load(id);
  // Shop mine = mine();
  // if (!shop.getId().equals(mine.getId())) {
  // return 0;
  // }
  // if(provinceId == null)provinceId = 0L;
  // if(cityId == null)cityId = 0L;
  // return shopMapper.updateLocalByPrimaryKey(id, provinceId, cityId);
  // }
  //
  // @Override
  // public int updatePostageStatusByPrimaryKey(String id, Boolean
  // postageStatus) {
  // if(StringUtils.isBlank(id))
  // return 0;
  // Shop shop = load(id);
  // Shop mine = mine();
  // if (!shop.getId().equals(mine.getId())) {
  // return 0;
  // }
  // return shopMapper.updatePostageStatusByPrimaryKey(id, postageStatus);
  // }
  //
  // @Override
  // public int updatePostageByPrimaryKey(String id, Long freeZoneId,
  // BigDecimal postage) {
  // if(StringUtils.isBlank(id))
  // return 0;
  // Shop shop = load(id);
  // Shop mine = mine();
  // if (!shop.getId().equals(mine.getId())) {
  // return 0;
  // }
  // if(freeZoneId == null)freeZoneId = 0L;
  // return shopMapper.updatePostageByPrimaryKey(id, freeZoneId, postage);
  // }

  @Override
  public List<Tag> listUserTags() {
    return getTagsByUserId(getCurrentUser().getId());
  }

  @Override
  public List<Tag> getTagsByUserId(String userId) {
    if (StringUtils.isBlank(userId)) {
      return Collections.emptyList();
    }

    return tagMapper.selectUserTags(userId);
  }

  @Override
  public Tag saveUserTag(String tagStr) {
    Tag tag = new Tag();
    tag.setTag(tagStr);
    tag.setCreatorId(getCurrentUser().getId());
    tagMapper.insert(tag);
    return tagMapper.selectByPrimaryKey(tag.getId());
  }

  @Override
  public Tag updateUserTag(String id, String tagName) {
    Tag tag = tagMapper.selectUserTag(tagName, getCurrentUser().getId());
    if (tag != null && !tag.getId().equals(id)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "涓嶈兘鏈夐噸澶嶇殑鏍囩");
    }
    tagMapper.updateUserTag(id, tagName, getCurrentUser().getId());
    return tagMapper.selectByPrimaryKey(id);
  }

  @Override
  public void removeUserTag(String tag) {
    int r = tagMapper.deleteTag(tag, getCurrentUser().getId());
    if (r == 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "a");
    }
  }

  @Override
  public Map<String, Object> loadStatistics() {
    Map<String, Object> shopStatis = new HashMap<String, Object>();
    Map<String, Object> params = new HashMap<String, Object>();

    params.put("shopId", getCurrentUser().getShopId());

    Date toDay = DateUtils.truncate(Calendar.getInstance(),
        Calendar.DAY_OF_MONTH).getTime();
    Date yesterday = DateUtils.addDays(toDay, -1);

    Map<String, Object> detail = new HashMap<String, Object>();
    params.put("created1", toDay);
    detail.put("order", orderMapper.countByShopStatistics(params));//fixme 今日订单和今日销售
    detail.put("visitors",
        shopAccessMapper.countByShopId(getCurrentUser().getShopId(), toDay));// 浠婃棩璁垮鏁�

    // and a.account_id = #{params.accountId}
    params.put("accountId", accountService.loadByUserId(getCurrentUser().getId())
        .getId());
    params.put("accountType", AccountType.AVAILABLE);
    params.put("modifyType", "increase");
    detail.put("deal", subAccountLogService.totalAmountByParams(params));// 浠婃棩鎴愪氦
    shopStatis.put("today", detail);
    //TODO 本月销售
    detail = new HashMap<String, Object>();
    detail.put("order", orderMapper.countByShopStatistics(params));//fixme 查询本月销售和本月订单
    shopStatis.put("thisMonth", detail);

    detail = new HashMap<String, Object>();
    params.put("created1", yesterday);
    params.put("created2", toDay);
    detail.put("order", orderMapper.countByShopStatistics(params));// 浠婃棩璁㈠崟鏁�
    detail.put("visitors",
        shopAccessMapper.countByShopId(getCurrentUser().getShopId(), yesterday));// 鏄ㄦ棩璁垮鏁�

    detail.put("deal", subAccountLogService.totalAmountByParams(params))

    ;// 鏄ㄦ棩鎴愪氦
    shopStatis.put("yesterday", detail);

    params.clear();
    params.put("shopId", getCurrentUser().getShopId());
    params.put("status", OrderStatus.SUBMITTED);
    shopStatis.put("orderSubmitted",
        orderMapper.countByShopStatistics(params));// 寰呬粯娆捐鍗曟暟閲�
    params.put("status", OrderStatus.PAID);
    shopStatis.put("orderShipped",
        orderMapper.countByShopStatistics(params));// 寰呭彂璐ц鍗曟暟閲�

    params.clear();
    params.put("created2", toDay);
    params.put("accountId", accountService.loadByUserId(getCurrentUser().getId())
        .getId());
    params.put("accountType", AccountType.AVAILABLE);
    params.put("modifyType", "increase");
    shopStatis.put("income",
        subAccountLogService.totalAmountByParams(params));//fixme 累计收益

    shopStatis.put("visitors",
        shopAccessMapper.countByShopIdUntilDay(getCurrentUser().getShopId(), toDay));//累计访问

    return shopStatis;
  }

  @Override
  public boolean isSyncedToXiangqu(String shopId) {
    return prodSyncMapper.existShop(shopId);
  }

  @Override
  public List<ShopAdmin> listShopsByAdmin(Map<String, Object> params,
      Pageable pageable) {

    return shopMapper.listShopsByAdmin(params, pageable);
  }

  @Override
  public List<ShopAdmin> listShopsByActivity(Map<String, Object> params, Pageable pageable,
      String activityid) {
    return shopMapper.listShopsByNewActivity(params, pageable, activityid);
  }

  @Override
  public Long countShopsByAdmin(Map<String, Object> params) {
    return shopMapper.countShopsByAdmin(params);
  }

  @Override
  public Long countShopsByActivity(Map<String, Object> params, String activityId) {
    return shopMapper.countShopByActivity(params, activityId);
  }

  @Override
  public boolean delete(String shopId, String opRemark) {
    Shop shop = load(shopId);
    if (shop == null) {
      return false;
    }
    boolean rc = shopMapper.deleteByPrimaryKey(shopId, opRemark) == 1;

    if (rc) {
      rc = userService.delete(shop.getOwnerId()) == 1;
    }

    return rc;
  }


  @Override
  public boolean close(String shopId, String opRemark) {
    Shop shop = load(shopId);
    if (shop == null) {
      return false;
    }
    boolean rc = shopMapper.deleteByPrimaryKey(shopId, opRemark) == 1;

    return rc;
  }


  @Override
  public boolean unClose(String shopId, String opRemark) {

    boolean rc = shopMapper.undeleteByPrimaryKey(shopId, opRemark) == 1;

    return rc;
  }

  @Override
  public int updateByAdmin(Shop shop) {
    if (shop == null) {
      return 0;
    }
    Shop chkShop = findByName(shop.getName());
    if (chkShop != null && !chkShop.getId().equals(shop.getId())) {
      return -1;
    }
    return shopMapper.updateByPrimaryKeySelective(shop);
  }

  @Override
  public boolean undelete(String shopId, String opRemark) {
    boolean rc = shopMapper.undeleteByPrimaryKey(shopId, opRemark) == 1;
    if (!rc) {
      return rc;
    }
    Shop shop = load(shopId);

    rc = userService.undelete(shop.getOwnerId()) == 1;

    return rc;
  }

  @Override
  public boolean saveFragment(boolean fragmentStatus) {
    String shopId = getCurrentUser().getShopId();
    return shopMapper.saveFragmentById(shopId, fragmentStatus) == 1;
  }

  private Boolean containtsCity(String zoneId) {
    String[] citys = postageCitys.split(",");
    for (String c : citys) {
      if (c.trim().equals(zoneId)) {
        return true;
      }
    }
    return false;
    //return postageCitys.contains(zoneId);
  }

  /* 寰楀埌鐪佷竴绾х殑鍖哄煙瀵硅薄 */
  private Zone getZone4Cal(String zoneId) {
    if (StringUtils.isBlank(zoneId)) {
      return null;
    }

    try {
      Zone zone = zoneMapper.selectByPrimaryKey(zoneId);
      if (zone == null) {
        return null;
      }
      if (zone.getId().equals("1")) {
        return null; // TODO: 涓嶈兘鐩存帴閫変腑鍥� 鍥介檯鐗堟�涔堝姙?
      }

      if (containtsCity(zone.getId())) { // 甯哥敤鍩庡競, 浼樺厛寰楀埌鍩庡競zone
        return zone;
      } else if (zone.getParentId().equals("1")) { // 涓浗鐪佷竴绾�
        return zone;
      } else if (zone.getParentId().equals("0")) { // 闄や腑鍥戒互澶栫殑鍏跺畠涓�骇(鍥藉)
        return zone;
      } else { // 鎺掗櫎涓浗, (涓浗鐪佷竴绾т互鍐呭悇鍦板尯)
        Zone pZone = zoneMapper.findParent(zone.getId());
        if (pZone != null) {
          return getZone4Cal(pZone.getId());
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  /* 杩囨护鍑洪渶瑕佽绠楅偖璐圭殑鍟嗗搧鍒楄〃 */
  private List<String> prodFilter(List<String> skus, List<String> prods) {
    List<String> ret = new ArrayList<String>();
    for (String e : skus) {
      Sku sku = skuMapper.selectByPrimaryKey(e);
      if (sku != null && prods != null
          && !prods.contains(sku.getProductId())) {
        ret.add(sku.getProductId());
      }
    }
    return ret;
  }

  private List<String> prodFilterForWechat(List<Sku> skus, List<String> prods) {
    List<String> ret = new ArrayList<String>();
    for (Sku sku : skus) {
      if (sku != null && prods != null
          && !prods.contains(sku.getProductId())) {
        ret.add(sku.getProductId());
      }
    }
    return ret;
  }

  private Boolean matchZone(List<AreaPair> aps, String zoneId) {
    if (aps == null || aps.size() == 0 || StringUtils.isBlank(zoneId)) {
      return false;
    }
    for (AreaPair ap : aps) {
      if (ap.getId().equals(zoneId)) {
        return true;
      }
    }

    return false;
  }

  /* 鏍规嵁涔板鏀惰揣鍖哄煙璁＄畻寰楀埌鍗栧鐨勮嚜瀹氫箟閭垂, 杩斿洖绌鸿〃绀轰娇鐢ㄥ叏灞�偖璐�*/
  private BigDecimal calcAreaFee(List<AreaPostagePair> list, String zoneId) {
    if (list == null || list.size() == 0 || StringUtils.isBlank(zoneId)) {
      return null;
    }

    for (AreaPostagePair app : list) {
      if (matchZone(app.getAreas(), zoneId)) {
        return app.getPostage(); // 涓�釜id鍙兘瀛樺湪浜庝竴涓偖璐瑰尯鍩熷唴, 濡傛灉鍑虹幇澶氫釜涔熶互绗竴娆″嚭鐜颁负鍑�
      }
    }

    return null; // 璇oneId娌℃湁鍦ㄨ嚜瀹氫箟閭垂鍖哄煙鍐�
  }


  private BigDecimal calcAreaFeeForWechat(List<AreaPostagePair> list, String zoneId,
      Integer amount) {
    if (list == null || list.size() == 0 || StringUtils.isBlank(zoneId)) {
      return null;
    }
    BigDecimal postage;
    for (AreaPostagePair app : list) {
      if (matchZone(app.getAreas(), zoneId)) {
        int first = app.getFirst();
        int second = app.getSecond();
        if (amount <= first) {
          postage = app.getFirstFee();
        } else {
          double a = amount - first;
          double s = second;
          double i = Math.ceil(a / s);
          postage = app.getFirstFee().add(app.getSecondFee().multiply(new BigDecimal(i)));
        }
        return postage;
      }
    }

    return null;
  }

  @Override
  public BigDecimal getShopPostage(String shopId, String zoneId,
      List<String> skus, BigDecimal totalFee) {
    final BigDecimal zeroFee = BigDecimal.ZERO;// 閭垂榛樿涓�
    if (shopId == null || zoneId == null || totalFee == null) {
      return zeroFee;
    }

    // 鑾峰彇搴楅摵閭垂璁剧疆
    ShopPostAge spa = shopPostAgeService.getPostAgeByShop(shopId);
    if (spa == null || spa.getPostageStatus() == null) {
      return zeroFee;
    }

    // 娌℃湁寮�惎閭垂璁剧疆
    if (!spa.getPostageStatus()) {
      return zeroFee;
    }

    // 鍙栧叏灞�偖璐�
    BigDecimal ret = spa.getPostage();

    // 鎬婚瓒呰繃鎸囧畾棰濆厤閭�
    if (spa.getFreeShippingPrice() != null
        && totalFee.compareTo(spa.getFreeShippingPrice()) >= 0) {
      return zeroFee;
    }

    // 杩囨护鍑洪渶瑕佽绠楅偖璐圭殑鍟嗗搧鍒楄〃,
    // 杩欓噷鏈夊垪琛ㄥ緢鏈夊彲鑳芥湁閲嶅, 涓嶈繃鏆傛椂涓嶉渶瑕佸鐞嗗垪琛�鍙叧蹇冨ぇ灏�
    if (spa.getFreeShippingGoods() != null
        && !spa.getFreeShippingGoods().isEmpty()) {
      List<String> calcList = prodFilter(skus, spa.getFreeShippingGoods());
      if (calcList.isEmpty()) {
        return zeroFee;
      }
    }

    Zone zone = getZone4Cal(zoneId);
    if (zone == null) {
      log.error("getZone4Cal errors");
      return zeroFee;
    }

    BigDecimal areaFee = calcAreaFee(spa.getCustomizedPostage(),
        zone.getId());
    if (areaFee == null && containtsCity(zone.getId())) {
      // 濡傛灉甯哥敤鍩庡競娌¤缃偖璐�浣嗚鍩庡競鎵�湪鐪佽缃簡閭垂,鍒欓偖璐逛负鎵�湪鐪侀偖璐� 姣斿鏉窞10鍧�
      // 娴欐睙璁剧疆15,浣嗘病璁剧疆瀹佹尝,鍒欏畞娉㈠湴鍖洪偖璐逛篃涓�5鍧�
      areaFee = calcAreaFee(spa.getCustomizedPostage(),
          zone.getParentId());
    }
    if (areaFee != null && areaFee.compareTo(BigDecimal.ZERO) == 0) {
      return zeroFee; // 涔板鍦ㄥ寘閭尯
    }

    if (areaFee != null) {
      ret = areaFee; // 涔板鍦ㄥ崠瀹剁殑鑷畾涔夐偖璐瑰尯鍩�
    }

    // all else 浣跨敤鍏ㄥ眬閭垂
    if (ret == null) {
      return zeroFee;
    }

    return ret;
  }

  @Override
  public BigDecimal getShopPostageForWechat(String shopId, String zoneId,
      List<Sku> skus, BigDecimal totalFee, Integer amount) {
    final BigDecimal zeroFee = BigDecimal.ZERO;//
    if (shopId == null || zoneId == null || totalFee == null) {
      return zeroFee;
    }
    ShopPostAge spa = shopPostAgeService.getPostAgeByShop(shopId);
    if (spa == null || spa.getPostageStatus() == null) {
      return zeroFee;
    }
    if (spa.getFreeShippingPrice() != null
        && totalFee.compareTo(spa.getFreeShippingPrice()) != -1) {
      return zeroFee;
    }

    if (!spa.getPostageStatus()) {
      return zeroFee;
    }
    //add 一元c
    BigDecimal ret = new BigDecimal(0);
    int first = spa.getDefaultFirst();
    int second = spa.getDefaultSecond();
    if (amount <= first) {
      ret = spa.getDefaultFirstFee();
    } else if (amount > first) {
      double a = amount - first;
      double s = second;
      double i = Math.ceil(a / s);
      ret = spa.getDefaultFirstFee().add(spa.getDefaultSecondFee().multiply(new BigDecimal(i)));
    }

    if (spa.getFreeShippingPrice() != null
        && totalFee.compareTo(spa.getFreeShippingPrice()) >= 0) {
      return zeroFee;
    }

    if (spa.getFreeShippingGoods() != null
        && !spa.getFreeShippingGoods().isEmpty()) {
      List<String> calcList = prodFilterForWechat(skus, spa.getFreeShippingGoods());
      if (calcList.isEmpty()) {
        return zeroFee;
      }
    }

    Zone zone = getZone4Cal(zoneId);
    if (zone == null) {
      log.error("getZone4Cal errors");
      return zeroFee;
    }

    BigDecimal areaFee = calcAreaFeeForWechat(spa.getCustomizedPostage(),
        zone.getId(), amount);
    if (areaFee == null && containtsCity(zone.getId())) {
      areaFee = calcAreaFee(spa.getCustomizedPostage(),
          zone.getParentId());
    }
    if (areaFee != null && areaFee.compareTo(BigDecimal.ZERO) == 0) {
      return zeroFee;
    }

    if (areaFee != null) {
      ret = areaFee;
    }

    // all else
    if (ret == null) {
      return zeroFee;
    }

    return ret;
  }

  /**
   * 根据订单内每个商品对应的邮费设置计算整个订单邮费
   */
  @Override
  public PostageRet getShopPostageByProduct(String shopId, String zoneId, List<Sku> skus) {
    BigDecimal logisticsFee = BigDecimal.ZERO;
    BigDecimal logisticsDiscount = BigDecimal.ZERO;
    LogisticDiscountType logisticDiscountType = null;
    // 运费模板对应的sku列表，key为运费模板id
    HashMap<String, List<Sku>> templateSku = new HashMap<>();
    for (Sku sku : skus) {
      String productId = sku.getProductId();
      Product product = productMapper.selectByPrimaryKey(productId);
      // 如果该商品运费为统一运费，则直接将商品运费加到总运费中
      if (product.getLogisticsType() == LogisticsType.UNIFORM) {
        logisticsFee = logisticsFee.add(product.getUniformValue());
      }
      // 如果该商品运费为设置的运费模板，将此sku加入到该运费模板对应的列表中
      else if (product.getLogisticsType() == LogisticsType.TEMPLATE) {
        String templateId = product.getTemplateValue();
        if (templateSku.containsKey(templateId)) {
          List<Sku> templateList = templateSku.get(templateId);
          templateList.add(sku);
        } else {
          List<Sku> templateList = new ArrayList<>();
          templateList.add(sku);
          templateSku.put(templateId, templateList);
        }
      }
    }

    // 下单传中台地址表
    // FIXME 统一处理地址映射关系
    SystemRegion systemRegion = systemRegionService.load(zoneId);
    if (systemRegion != null && systemRegion.getAdCode() != null) {
      try {
        Zone realZone = zoneMapper.selectByZipCode(
                String.valueOf(systemRegion.getAdCode()));
        zoneId = realZone.getId();
      }catch (Exception e){
        throw new BizException(GlobalErrorCode.UNKNOWN,"地址异常",e);
      }
    }
    // 计算每个运费模板对应的所有商品的运费
    // TODO 目前仅支持单个运费模板
    for (Entry<String, List<Sku>> entry : templateSku.entrySet()) {
      String templateId = entry.getKey();
      ArrayList templateList = (ArrayList) entry.getValue();
      PostageRet fee = getTemplatePostage(shopId, zoneId, templateList, templateId);
      logisticsFee = logisticsFee.add(fee.getLogisticsFee());
      logisticsDiscount = logisticsDiscount.add(fee.getLogisticsDiscount());
      logisticDiscountType = fee.getLogisticDiscountType();
    }

    return new PostageRet(logisticsFee, logisticsDiscount, logisticDiscountType);
  }

  /**
   * 计算每个运费模板对应的所有商品的总运费
   */
  private PostageRet getTemplatePostage(String shopId, String zoneId, List<Sku> skus,
      String templateId) {
    BigDecimal totalFee = BigDecimal.ZERO;
    // 商品件数总计
    Integer amount = 0;
    // 商品重量总计
    Integer weight = 0;
    for (Sku sku : skus) {
      Integer skuAmount = sku.getItemAmount();
      BigDecimal price = sku.getPrice();
      BigDecimal skuFee = price.multiply(new BigDecimal(skuAmount));
      amount = amount + skuAmount;
      totalFee = totalFee.add(skuFee);
      // 重量累加
      Product product = productMapper.selectByPrimaryKey(sku.getProductId());
      weight = weight +
          ((product.getWeight() == null ? 0 : product.getWeight()) * skuAmount);
    }

    if (shopId == null || zoneId == null || totalFee == null) {
      return PostageRet.empty();
    }
    ShopPostAge spa = shopPostAgeService.getPostAgeById(templateId);
    if (spa == null || spa.getPostageStatus() == null) {
      return PostageRet.empty();
    }

    if (!spa.getPostageStatus()) {
      return PostageRet.empty();
    }
    //add 一元c
    BigDecimal ret = new BigDecimal(0);
    int first = spa.getDefaultFirst();
    int second = spa.getDefaultSecond();

    // 如果该运费模板是按重量计算，则将sku中所有重量作为计算基数
    if ("0".equals(spa.getType())) {
      amount = weight;
    }
    if (amount <= first) {
      ret = spa.getDefaultFirstFee();
    } else if (amount > first) {
      double a = amount - first;
      double s = second;
      double i = Math.ceil(a / s);
      ret = spa.getDefaultFirstFee().add(spa.getDefaultSecondFee().multiply(new BigDecimal(i)));
    }

    if (spa.getFreeShippingGoods() != null
        && !spa.getFreeShippingGoods().isEmpty()) {
      List<String> calcList = prodFilterForWechat(skus, spa.getFreeShippingGoods());
      if (calcList.isEmpty()) {
        return PostageRet.empty();
      }
    }

    Zone zone = getZone4Cal(zoneId);
    if (zone == null) {
      log.error("getZone4Cal errors");
      return PostageRet.empty();
    }

    BigDecimal areaFee = calcAreaFeeForWechat(spa.getCustomizedPostage(),
        zone.getId(), amount);
    if (areaFee == null && containtsCity(zone.getId())) {
      areaFee = calcAreaFee(spa.getCustomizedPostage(),
          zone.getParentId());
    }
    if (areaFee != null && areaFee.compareTo(BigDecimal.ZERO) == 0) {
      return PostageRet.empty();
    }

    if (areaFee != null) {
      ret = areaFee;
    }

    // all else
    if (ret == null) {
      return PostageRet.empty();
    }

    // 如果免邮则把结果赋值给运费折扣, 否则赋值给邮费
    if (spa.getFreeShippingPrice() != null
        && totalFee.compareTo(spa.getFreeShippingPrice()) != -1) {
      return new PostageRet(BigDecimal.ZERO, ret, LogisticDiscountType.FREE_POSTAGE);
    }

    // 首次购买可包邮全局开关
    if (spa.getFreeShippingFirstPay()) {
      // 查看当前用户的免邮资格
      User currUser = (User) getCurrentUser();
      // 根据订单状态及身份确认是否可以免邮
      boolean canUseFreeLogistic = customerProfileService.canUseFreeLogistic(currUser);
      if (canUseFreeLogistic) {
        return new PostageRet(BigDecimal.ZERO, ret, LogisticDiscountType.FREE_FIRST_PAY);
      }
    }

    return new PostageRet(ret, BigDecimal.ZERO);
  }

  @Override
  public BigDecimal getCustomizedPostage(
      List<AreaPostagePair> customizedPostage, String zoneId) {
    // 鑾峰彇鐖剁骇鍒殑鍦板尯
    Zone zone = getZone4Cal(zoneId);
    if (zone == null) {
      log.error("asf");
      return null;
    }

    // 鑾峰彇鏄惁鍦ㄨ嚜瀹氫箟閭垂涓湁璁剧疆锛屾湁鍒欒幏鍙栬缃殑閭垂锛堣嫢涓�釜鍟嗗搧璁剧疆浜嗗涓偖璐硅嚜瀹氫箟锛屽垯鍙幓绗竴涓缃殑锛�
    BigDecimal areaFee = calcAreaFee(customizedPostage, zone.getId());

    // 鑾峰彇甯哥敤鐨勮缃殑閭垂
    if (areaFee != null) {
      return areaFee;
    }

    // 鐢ㄦ埛鎵�湪鐨勭渷浠芥槸甯哥敤鐨勫湴鍖�鑾峰彇璇ョ渷浠界殑鐖朵翰鐨勯偖璐�
    if (containtsCity(zone.getId())) {
      // 濡傛灉甯哥敤鍩庡競娌¤缃偖璐�浣嗚鍩庡競鎵�湪鐪佽缃簡閭垂,鍒欓偖璐逛负鎵�湪鐪侀偖璐� 姣斿鏉窞10鍧� 娴欐睙璁剧疆15,浣嗘病璁剧疆瀹佹尝,鍒欏畞娉㈠湴鍖洪偖璐逛篃涓�5鍧�
      areaFee = calcAreaFee(customizedPostage, zone.getParentId());
    }

    return areaFee;
  }

  @Override
  public Long selectShopOwner(String shopId) {
    return shopMapper.selectShopOwner(shopId);
  }

  @Override
  public List<Shop> listNoCodeShops() {
    return shopMapper.listNoCodeShops();
  }

  @Override
  public void addCode(String... ids) {
    if (ids == null || ArrayUtils.isEmpty(ids)) {
      return;
    }

    for (String p : ids) {
      shopMapper.addCode(p);
    }
  }

  @Override
  public List<Sku> listNoCodeSkus() {
    return skuMapper.listNoCodeSkus();
  }

  @Override
  public void addSkuCode(String[] ids) {
    if (ids == null || ArrayUtils.isEmpty(ids)) {
      return;
    }

    for (String p : ids) {
      skuMapper.addCode(p);
    }
  }

  // mtest?ouerShopId=xxx&url=yyy
  @Override
  public Map<String, Object> moveShop(String ouerShopId, String shopUrl) {
    final Map<String, String> params = new HashMap<String, String>();
    params.put("ouerShopId", ouerShopId);
    params.put("url", shopUrl);

    // 鍒ゆ柇鏄惁宸茬粡鍦╰ask涓�TODO

    final Map<String, Object> result = new HashMap<String, Object>();
    log.info(params.toString());
    final HttpInvokeResult rslt = PoolingHttpClients.post(url + "/mtest", params);
    if (!rslt.isOK()) {
      log.error("Error to moveProduct:" + rslt,
          rslt.getException() + "params:" + params.toString());
      result.put("statusCode", "501");
      return result;
    }

    final JSONObject obj = JSONObject.parseObject(rslt.getContent());
    if (obj.getIntValue("errorCode") != 200) {
      log.error("Failed to qeury movestatus:" + rslt + " params:" + params.toString());
      result.put("statusCode", "502");
      return result;
    }

    if (obj.getJSONObject("data").getIntValue("statusCode") != 200) {
      if (obj.getJSONObject("data").getIntValue("statusCode") == 601) {
        log.error("shop not exists:" + rslt + " params:" + params.toString());
        result.put("statusCode", "601");
        return result;
      } else {
        log.error("unknow error:" + rslt + " params:" + params.toString());
        result.put("statusCode", "602");
        return result;
      }
    }

    result.put("statusCode", "200");
    result.put("shopUrl", shopUrl);
    result.put("shopId", ouerShopId);
    return result;
  }

  // moveImg/group?ouerShopId=xxx&itemId=yyy&shopType=zzz
  @Override
  public Map<String, Object> moveItemGroupImg(String ouerShopId, String itemId, int shopType) {
    final Map<String, String> params = new HashMap<String, String>();
    params.put("ouerShopId", ouerShopId);
    params.put("itemId", itemId);
    params.put("shopType", String.valueOf(shopType));

    final Map<String, Object> result = new HashMap<String, Object>();
    log.info(params.toString());
    final HttpInvokeResult rslt = PoolingHttpClients.post(url + "/moveImg/group", params);
    if (!rslt.isOK()) {
      log.error("Error to moveProduct:" + rslt,
          rslt.getException() + "params:" + params.toString());
      result.put("statusCode", "501");
      return result;
    }

    result.put("statusCode", "200");
    return result;
  }

  // moveDesc/test?ouerShopId=xxx&itemId=yyy&shopType=zzz
  @Override
  public Map<String, Object> moveItemDesc(String ouerShopId, String itemId, int shopType) {
    final Map<String, String> params = new HashMap<String, String>();
    params.put("ouerShopId", ouerShopId);
    params.put("itemId", itemId);
    params.put("shopType", String.valueOf(shopType));

    final Map<String, Object> result = new HashMap<String, Object>();
    log.info(params.toString());
    final HttpInvokeResult rslt = PoolingHttpClients.post(url + "/moveDesc/test", params);
    if (!rslt.isOK()) {
      log.error("Error to moveProduct:" + rslt,
          rslt.getException() + "params:" + params.toString());
      result.put("statusCode", "501");
      return result;
    }

    result.put("statusCode", "200");
    return result;
  }

  //  moveItem/wtest?ouerShopId=xxx&itemId=yyy&shopType=zzz
  @Override
  public Map<String, Object> moveItem(String ouerShopId, String itemId, int shopType) {
    final Map<String, String> params = new HashMap<String, String>();
    params.put("ouerShopId", ouerShopId);
    params.put("itemId", itemId);
    params.put("shopType", String.valueOf(shopType));

    final Map<String, Object> result = new HashMap<String, Object>();
    log.info(params.toString());
    final HttpInvokeResult rslt = PoolingHttpClients.post(url + "/moveItem/wtest", params);
    if (!rslt.isOK()) {
      log.error("Error to moveProduct:" + rslt,
          rslt.getException() + "params:" + params.toString());
      result.put("statusCode", "501");
      return result;
    }

    result.put("statusCode", "200");
    return result;
  }

  // moveItem/mSync?ouerShopId=xxx&itemId=yyy&type=zzz
  @Override
  public Map<String, Object> moveSyncFromSpider(String ouerShopId, String itemId, int type) {
    final Map<String, String> params = new HashMap<String, String>();
    params.put("ouerShopId", ouerShopId);
    params.put("itemId", itemId);
    params.put("type", String.valueOf(type));

    final Map<String, Object> result = new HashMap<String, Object>();
    log.info(params.toString());
    final HttpInvokeResult rslt = PoolingHttpClients.post(url + "/moveItem/mSync", params);
    if (!rslt.isOK()) {
      log.error("Error to moveProduct:" + rslt,
          rslt.getException() + "params:" + params.toString());
      result.put("statusCode", "501");
      return result;
    }

    result.put("statusCode", "200");
    return result;
  }


  @Override
  public boolean updateThirdCommission(ThirdCommission... forms) {
    boolean result = true;
    if (forms == null || ArrayUtils.isEmpty(forms)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "forms.isEmpty");
    }

    String shopId = getCurrentUser().getShopId();
    Shop shop = load(shopId);
    if (shop == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "shop.not.found");
    }

    for (ThirdCommission form : forms) {
      if (!result) {
        log.warn("asdf");
        return result;
      }

      if (form == null || form.getThirdId() == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            "invalid." + form.getThirdId() + " thirdId.isNull");
      }

      BigDecimal thirdCommisionRate = form.getCommissionRate();
      if (thirdCommisionRate == null || thirdCommisionRate.compareTo(BigDecimal.valueOf(0)) < 0 ||
          thirdCommisionRate.compareTo(BigDecimal.valueOf(0.5)) > 0) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            "invalid." + form.getThirdId() + " commisionrate.isNotAllowed");
      }

      // 鑾峰彇搴楅摵鐨勭涓夋柟閰嶇疆淇℃伅
      Map<String, Object> params = new HashMap<String, Object>();
      params.put("shopId", shopId);
      params.put("unionId", form.getThirdId());
      List<ProdSync> aList = prodSyncMapper.findByParmas(params, null);

      if (aList == null || aList.size() == 0) {
        // 娣诲姞
        ProdSync aps = new ProdSync();
        aps.setShopId(getCurrentUser().getShopId());
        aps.setName(shop.getName());
        aps.setSynced(false);
        aps.setAuditSts(SyncAuditStatus.AUDITTING.toString());
        aps.setCommissionRate(form.getCommissionRate());

        User patner = userService.load(form.getThirdId());
        if (patner != null) {
          aps.setUnionId(patner.getId());
        } else {
          aps.setUnionId(IdTypeHandler.encode(16618945L));
        }

        result = prodSyncMapper.insert(aps) == 1;

      } else {
        // 淇敼
        if (aList.size() != 1) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "鏁版嵁搴撲腑绗笁鏂圭殑浣ｉ噾璁剧疆涓嶆涓�潯");
        }

        ProdSync org = aList.get(0);
        org.setAuditSts(SyncAuditStatus.AUDITTING.toString());
        org.setCommissionRate(form.getCommissionRate());
        result = prodSyncMapper.update(org) == 1;
      }
    }

    return result;
  }

  @Override
  public List<Shop> listAllShops() {
    return shopMapper.listAllShops();
  }

  @Override
  @Transactional
  public Shop createUserAndShopByBos(String phone, String password, String shopName) {

    Shop shop = new Shop();
    shop.setName(shopName);
    shop.setStatus(ShopStatus.ACTIVE);
    shop.setArchive(false);
    shop.setCreatedAt(new Date());
    shop.setFragmentStatus(true);

    Shop chkShop = findByName(shopName);
    if (chkShop != null) {
      throw new BizException(GlobalErrorCode.UNKNOWN,
          "新建店铺名 shopName=[" + shop.getName() + "] 已经存在");
    }

    User user = userService.register(phone, MD5Util.md5(password));

    shop = createShop(shop, user.getId());
    //FIXME 商户公众号配置
//		ShopWechatSetting  shopWechatSetting = new ShopWechatSetting();
//		shopWechatSetting.setName(shop.getName());
//		shopWechatSetting.setOwnerId(user.getId());
//		shopWechatSettingService.insert(shopWechatSetting);
    //店铺树关系 根节点的创建
    shopTreeService.insertRootShopTree(shop.getId());

    return shop;
  }

  /**
   * 通过我要分销创建分销店铺信息
   *
   * @return 店铺信息
   */
  @Override
  @Transactional
  public Shop createDistributeShop(UserTwitter userTwitterApply) {

    String userId = userTwitterApply.getUserId();
    Shop existShop = shopMapper.selectByUserId(userId);
    if (existShop == null) {
      existShop = new Shop();
      existShop.setName(userService.load(userTwitterApply.getUserId()).getName());
      existShop.setStatus(ShopStatus.ACTIVE);
      existShop.setArchive(false);
      existShop.setCreatedAt(new Date());
      existShop.setFragmentStatus(true);
      existShop = createShop(existShop, userTwitterApply.getUserId());
    }

    //店铺树关系 子节点店铺的创建
    String parentShopId = userService.load(userTwitterApply.getParentUserId()).getShopId();
    ShopTree parentTree = shopTreeService.selectRootShopByShopId(parentShopId);
    String rootShopId = parentTree.getRootShopId();
    // 先删除掉该shop之前有过的shoptree相关信息
    shopTreeService.deleteByDescendant(existShop.getId());
    ShopTree shopTree = new ShopTree();
    shopTree.setRootShopId(rootShopId);
    shopTree.setAncestorShopId(parentShopId);
    shopTree.setDescendantShopId(existShop.getId());
    shopTreeService.insertShopTree(shopTree);

    return existShop;
  }

  /**
   * 我的店铺单品排序
   */
  @Override
  public int sortShopProducts(List<String> sortIds, String shopId) {
    return shopMapper.sortShopProducts(sortIds, shopId);
  }

  /**
   * app:2b数据统计
   */
  @Override
  public Map<String, Object> loadB2BStatistics() {
    Map<String, Object> shopStatis = new HashMap<String, Object>();
    Map<String, Object> params = new HashMap<String, Object>();
    Map<String, Object> today = new HashMap<String, Object>();
    Map<String, Object> thisMonth = new HashMap<String, Object>();

    String userId = getCurrentUser().getId();
    params.put("userId", userId);

    UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
    if (userAgentVO == null) {//新用户，未提交代理申请
      today.put("order", 0);//今日订单数
      thisMonth.put("order", 0);//本月订单数
      thisMonth.put("agentCount", 0);//本月新增代理数
      thisMonth.put("sale", 0);//本月销售商品数

      shopStatis.put("teamCount", 1);//团队人数
      shopStatis.put("today", today);
      shopStatis.put("thisMonth", thisMonth);
      shopStatis.put("withdrawAll", 0);
      return shopStatis;
    }
    AgentType type = userAgentVO.getType();

    Date toDay = DateUtils.truncate(Calendar.getInstance(), Calendar.DAY_OF_MONTH).getTime();
    Date thisMonthDate = DateUtils.truncate(Calendar.getInstance(), Calendar.MONTH).getTime();

    long todayOrder = 0;//今日订单
    long thisMonthOrder = 0;//本月订单
    long thisMonthIncreaseAgent = 0;//本月新增代理人数
    long thisMonthSale = 0;//本月销售数量

    params.put("day", toDay);
    //今日订单和今日销售
    if (AgentType.DIRECTOR.equals(type) || AgentType.FOUNDER.equals(type)) {//联合创始人,董事
      todayOrder = commissionService.countNumberByDate(params);//获取今日有收益的订单数
    } else if (AgentType.GENERAL.equals(type)) {//总顾问
      todayOrder = orderMapper.countGeneralOrder(params);//获取总顾问直接下级总顾问的订单数
    } else {//一星顾问和二星顾问和特约
      todayOrder = orderMapper.countB2BOrder(params);//获取今日订单数
    }

    params.remove("day");
    params.put("thisMonth", thisMonthDate);
    //本月订单和本月销售
    if (AgentType.DIRECTOR.equals(type) || AgentType.FOUNDER.equals(type)) {
      thisMonthOrder = commissionService.countNumberByDate(params);
      thisMonthSale = orderMapper.getDirectorSaleProductsByDate(params);//获取本月销售商品数
    } else if (AgentType.GENERAL.equals(type)) {
      thisMonthOrder = orderMapper.countGeneralOrder(params);
      thisMonthSale = orderMapper.getGeneralSaleProductsByDate(params);//获取本月销售商品数
    } else {
      thisMonthOrder = orderMapper.countB2BOrder(params);
      thisMonthSale = orderMapper.getFirstAndSecondSaleProductsByDate(params);//获取本月销售商品数
    }

//		params.put("type",type);
//		thisMonthSale=orderMapper.getSaleProductsByDate(params);//获取本月销售商品数

    thisMonthIncreaseAgent = userAgentService.countMonthByB2B(userId);//获取本月新增代理

    today.put("order", todayOrder);//今日订单数
    thisMonth.put("order", thisMonthOrder);//本月订单数
    thisMonth.put("agentCount", thisMonthIncreaseAgent);//本月新增代理数
    thisMonth.put("sale", thisMonthSale);//本月销售商品数

    long teamCount = userAgentService.countTeamByB2B(userId);//获取团队人数

    shopStatis.put("teamCount", teamCount);//团队人数
    shopStatis.put("today", today);
    shopStatis.put("thisMonth", thisMonth);
    return shopStatis;
  }


  public int insertByBos(Shop e, String userId) {
    e.setStatus(ShopStatus.ACTIVE);
    e.setOwnerId(userId);
    e.setDanbao(true);
    int id = shopMapper.insert(e);
    shopMapper.addCode(e.getId());
    return id;
  }

  public Shop createShop(Shop shop, String userId) {
    shop.setOwnerId(userId);
    Shop existShop = shopMapper.selectByUserId(userId);
    if (existShop == null) {
      insertByBos(shop, userId);
      User userUpdate = new User();
      userUpdate.setId(userId);
      userUpdate.setShopId(shop.getId());
      userService.updateByBosUserInfo(userUpdate);
    } else {
      throw new BizException(GlobalErrorCode.UNKNOWN,
          "该手机号已经创建了店铺 existShop=[" + existShop.getName() + "]");
    }
    return shop;
  }


  @Override
  public void afterPropertiesSet() {
      rootShop = shopMapper.selectRootShop();
  }
}
