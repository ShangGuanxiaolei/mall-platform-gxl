package com.xquark.service.shop;

import com.xquark.dal.model.ShopAccessLog;
import com.xquark.service.BaseEntityService;


public interface ShopAccessLogService extends BaseEntityService<ShopAccessLog> {

  ShopAccessLog load(String id);

  int insert(ShopAccessLog team);

  int deleteForArchive(String id);

  int update(ShopAccessLog record);

  /**
   * 统计某个店铺的总访问量
   */
  Long countByShopId(String shopId);

  /**
   * 获取用户最后一次访问的店铺
   */
  ShopAccessLog getLastShop(String userId);

  /**
   * 获取用户第一次访问的店铺
   */
  ShopAccessLog getFirstShop(String userId);

  /**
   * 将店铺访问记录表中的该店铺信息都删掉
   */
  void deleteByShopId(String shopId);

}

