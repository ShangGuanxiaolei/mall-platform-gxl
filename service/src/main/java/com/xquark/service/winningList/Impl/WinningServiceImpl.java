package com.xquark.service.winningList.Impl;

import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.status.MainOrderStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.LogisticDiscountType;
import com.xquark.dal.type.OrderSortType;
import com.xquark.dal.type.OrderType;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.WinningListView;
import com.xquark.dal.vo.WinningProductVO;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderAddressService;
import com.xquark.service.order.impl.MainOrderServiceImpl;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.winningList.WinningService;
import com.xquark.service.winningList.vo.WinningUser;
import com.xquark.utils.UniqueNoUtils;
import io.vavr.control.Either;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class WinningServiceImpl implements WinningService {

  private static Logger logger = LoggerFactory.getLogger(WinningServiceImpl.class);

  private final static String PREFIX = "http://images.handeson.com";

  private final static String SUFFIX = "?imageView2/2/w/640/q/100/@w/$w$@/@h/$h$@";

  private final static String FIRST_AWARD = PREFIX + "/Fm7W0u-LoG270HjDVJHDuOZh7C7B" + SUFFIX;

  private final static String SECOND_AWARD = PREFIX + "/Fg1ST9DkG-ML3PKZPR2D3sgQPXkf" + SUFFIX;

  private final static String THIRD_AWARD = PREFIX + "/FndOct2jE6gD59FxHzb9-rP1hbRH" + SUFFIX;

  @Autowired private WinningMapper winningMapper;

  @Autowired private AddressService addressService;

  @Autowired private UserMapper userMapper;

  @Autowired private PromotionLotteryMapper promotionLotteryMapper;

  @Autowired private PromotionListMapper promotionListMapper;

  @Autowired private PromotionLotteryProbabilityMapper lotteryProbabilityMapper;

  @Autowired private MainOrderServiceImpl mainOrderService;

  @Autowired private OrderAddressService orderAddressService;

  @Autowired private ShopService shopService;



  @Autowired private ProductService productService;

  @Autowired private SkuMapper skuMapper;
  @Override
  public Either<String, List<WinningListView>> getWinningList(Long cpId) {

    final PromotionLottery promotionLottery = promotionLotteryMapper
            .selectClosestLotteryActivityLimitVO();
    if (promotionLottery == null) {
      return Either.left("活动详情不存在");
    }
    final PromotionList promotionList = promotionListMapper.selectByPrimaryKey(promotionLottery
            .getPromotionListId());
    if (promotionList == null) {
      return Either.left("活动不存在");
    }
    final Date start = promotionList.getTriggerStartTime();
    final Date end = promotionList.getTriggerEndTime();
    final List<WinningListView> list = winningMapper.getWinningListByCpId(cpId, start, end)
            .stream()
            .map(w -> {
              final int winningRank = w.getWinningRank();
              final WinningProductVO product = lotteryProbabilityMapper
                      .selectWinningProduct(promotionLottery.getId(), winningRank);
              return new WinningListView(w, product, winningRank, w.getState());
            })
            .collect(Collectors.toList());
    return Either.right(list);
  }

  @Override
  public Either<String, WinningProductVO> getWinningProductVO(String winningId) {
    final PromotionLottery promotionLottery = promotionLotteryMapper
        .selectClosestLotteryActivityLimitVO();
    if (promotionLottery == null) {
      return Either.left("活动详情不存在");
    }
    final WinningList view = winningMapper.getWinningListByPrimaryKey(winningId);
    final int winningRank = view.getWinningRank();
    final WinningProductVO product = lotteryProbabilityMapper.selectWinningProduct(promotionLottery.getId(), winningRank);
    product.setWinningCpId(view.getWinningCpId());
    return Either.right(product);
  }

  /*
   * @Author chp
   * @Description  获取导入商品信息
   * @Date
   * @Param
   * @return
   **/
  @Override
  public Either<String, WinningProductVO> getWinningProductVO2(Long cpid,String skuCode) {

    final WinningProductVO product = lotteryProbabilityMapper.selectWinningProduct2(skuCode);
    product.setWinningCpId(cpid);
    return Either.right(product);
  }



  /**
   * 改变领取的状态
   *
   * @param winningId, addressId
   * @return boolean
   */
  @Override
  @Transactional
  public boolean updateWinningStatus(String winningId, String addressId) {
    if (winningMapper.getUncollectedWinningList(winningId) == null)
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该奖品不存在");
    return winningMapper.updateClaimedStatus(winningId, addressId) > 0;
  }

  @Override
  public WinningDetail getAddressDetail(String winningId, Long cpId) {
    WinningDetail winningDetail = null;
    if (winningId != null) {
      WinningList winningList = winningMapper.getWinningListByPrimaryKey(winningId);

      if (winningList == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "奖品不存在");
      }
      if(winningList.getWinningCpId() == null ||
              !cpId.equals(winningList.getWinningCpId())){
				throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "当前用户和领取奖品用户不一致");
			}

      String addressId = winningList.getAddressId();
      if(addressId == null){
      	throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,"地址ID为空");
			}

      AddressVO addressVO = addressService.loadUserAddress(addressId);
      if (addressVO == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "地址不存在");
      }

      winningDetail = new WinningDetail();
      winningDetail.setPhone(addressVO.getPhone());
      winningDetail.setAddress(addressVO.getDetails());
      winningDetail.setName(addressVO.getConsignee());
    }
    return winningDetail;
  }

	@Override
  @Transactional
	public boolean isValid() {
		return winningMapper.updateValidMapper() > 0;
	}

  @Override
  public List<WinningUser> getWinningUserList() {
    List<WinningList> winningList = winningMapper.getWinningList();
    List<WinningUser> list = new ArrayList<>();
    if(winningList != null){
      for (WinningList winning: winningList) {
        Long cpId = winning.getWinningCpId();
        if (cpId == null) {
          logger.warn("中奖人cpId {} 不存在", cpId);
          continue;
        }
        User user = userMapper.selectByCpId(winning.getWinningCpId());
        if (user == null) {
          logger.warn("中奖人cpId {} 用户不存在", cpId);
          continue;
        }
        WinningUser winningUser = new WinningUser();
        switch (winning.getWinningRank()) {
          case 1:
            winningUser.setLucky("中了一等奖");
            break;
          case 2:
            winningUser.setLucky("中了二等奖");
            break;
          case 3:
            winningUser.setLucky("中了三等奖");
            break;
          case 4:
            winningUser.setLucky("中了四等奖");
            break;
          case 5:
            winningUser.setLucky("中了五等奖");
            break;
          default:
            winningUser.setLucky("中了" + winning.getWinningRank() + "德分");
            break;
        }
        winningUser.setUserName(user.getName());
        list.add(winningUser);
      }
    }else{
      logger.info("暂时没有中奖人");
    }
    return list;
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public Either<String,Boolean> createWinningOrder(String winningId,String addressId) {

    // 状态是立即领取
    final boolean isSuccess = updateWinningStatus(winningId, addressId);
    //生成待发活动订单
    if (!isSuccess) {
      return Either.left("奖品状态无效");
    }

    logger.info("开始领取奖品 winningId：{}，addressId:{}",winningId,addressId);

    // 获取奖品信息
    WinningProductVO wp = getWinningProductVO(winningId)
        .getOrElseThrow(s -> new BizException(GlobalErrorCode.INVALID_ARGUMENT, s));

    Address address = addressService.selectById(addressId);
    final String resourceType = StringUtils.defaultIfBlank(wp.getSkuCodeResources(), "CLIENT");

    if (address == null){
      return Either.left("地址不存在");
    }

    // 通过cpid获取用户信息
    User user = userMapper.selectByCpId(wp.getWinningCpId());

    MainOrder mainOrder = createMainOrder(wp.getPrice(),user);
    final Date now = new Date();
    final String payNo = UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.NPO);
    mainOrder.setPayNo(payNo);
    mainOrder.setPaidAt(now);

    Shop shop = shopService.loadRootShop();

    Order order = new Order();

    order.setGoodsFee(wp.getPrice());
    order.setLogisticsFee(BigDecimal.ZERO);
    order.setLogisticDiscount(BigDecimal.ZERO);
    //新增的积分抵扣掉邮费的字段
    order.setCommissionLogisticsDiscount(BigDecimal.ZERO);
    order.setLogisticDiscountType(LogisticDiscountType.FREE_POSTAGE);
    order.setOrderType(OrderSortType.GIFT);
    order.setDiscountFee(wp.getPrice());
    order.setType(OrderType.DANBAO);
    order.setTotalFee(BigDecimal.ZERO);
    order.setBuyerId(user.getId());
    order.setShopId(shop.getId());
    order.setRootShopId(shop.getId());
    order.setSellerId(shop.getOwnerId());
    order.setNeedInvoice(false);
    order.setStatus(OrderStatus.PAID);
    order.setPaidAt(now);
    order.setPayNo(payNo);
    order.setIsPickup(0);
    order.setDestName(resourceType + "不可退商品");
    order.setDest(resourceType + "_NOT_REFUNDABLE");

    OrderItem orderItem = new OrderItem();
    orderItem.setProductId(wp.getProductId());
    orderItem.setProductName(wp.getName());
    orderItem.setProductImg(wp.getImg());
    orderItem.setPromotionType(PromotionType.GIFT);

    // 修改价格为计算后的价格
    // 特权商品购买不需要花钱
    orderItem.setPrice(wp.getPrice());
    orderItem.setDiscount(wp.getPrice());
    orderItem.setDiscountPrice(BigDecimal.ZERO);
    orderItem.setMarketPrice(BigDecimal.ZERO);

    orderItem.setSkuId(wp.getSkuId());
    orderItem.setSkuStr("无");
    orderItem.setAmount(1);
    orderItem.setPromoAmt(BigDecimal.valueOf(0));
    orderItem.setServerAmt(BigDecimal.valueOf(0));

    OrderAddress oa = new OrderAddress();

    BeanUtils.copyProperties(address,oa);
    mainOrderService.save(mainOrder,Collections.singletonMap(order, Collections.singletonList(orderItem)),oa,null,
            MainOrderStatus.PAID, OrderStatus.PAID);

    return Either.right(true);
  }
   /*
    * @Author chp
    * @Description // 物流导入生成订单
    * @Param
    * @return
    **/
  @Override
  @Transactional(rollbackFor = Exception.class)
  public Either<String,Boolean> createWinningOrder2(Long  cpid,String addressId,ErpImportOrder erp) {

    // 获取奖品信息
    WinningProductVO wp = getWinningProductVO2(cpid,erp.getSkuCode())
             .getOrElseThrow(s -> new BizException(GlobalErrorCode.INVALID_ARGUMENT, s));
//  Address addressq = addressService.selectById(addressId);
    Address address1 = new Address();
    address1.setStreet(erp.getStreet());
    address1.setPhone(erp.getPhone());
    address1.setConsignee(erp.getReceiver());
    address1.setZoneId("1266");
    address1.setCpId(cpid);
    final String resourceType = StringUtils.defaultIfBlank(wp.getSkuCodeResources(), "CLIENT");
//
//    if (address == null){
//      return Either.left("地址不存在");
//    }

    // 通过cpid获取用户信息
    User user = userMapper.selectByCpId(cpid);
    MainOrder mainOrder = createMainOrder2( new BigDecimal(erp.getPayMoney()),user);
    final Date now = new Date();
    final String payNo = UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.NPO);
    mainOrder.setPayNo(payNo);
    mainOrder.setPaidAt(now);
    Shop shop = shopService.loadRootShop();
    OrderImportVo order = new  OrderImportVo();
    order.setGoodsFee(new BigDecimal(erp.getPayMoney()));
    order.setLogisticsFee(BigDecimal.ZERO);
    order.setLogisticDiscount(BigDecimal.ZERO);
    //新增的积分抵扣掉邮费的字段
    order.setCommissionLogisticsDiscount(BigDecimal.ZERO);
    order.setLogisticDiscountType(LogisticDiscountType.FREE_POSTAGE);
    order.setOrderType(OrderSortType.GIFT);
    order.setDiscountFee(new BigDecimal(erp.getPayMoney()));
    order.setType(OrderType.DANBAO);
    order.setTotalFee(BigDecimal.ZERO);
    order.setBuyerId(user.getId());
    order.setShopId(shop.getId());
    order.setRootShopId(shop.getId());
    order.setSellerId(shop.getOwnerId());
    order.setNeedInvoice(false);
    order.setStatus(OrderStatus.PAIDNOSTOCK);
    order.setPaidAt(now);
    order.setPayNo(payNo);
    order.setIsPickup(0);
    order.setDestName(resourceType + "不可退商品");
    order.setDest(resourceType + "_NOT_REFUNDABLE");

    OrderItem orderItem = new OrderItem();
    orderItem.setProductId(wp.getProductId());
    orderItem.setProductName(wp.getName());
    orderItem.setProductImg(wp.getImg());
    orderItem.setPromotionType(PromotionType.GIFT);

    // 修改价格为计算后的价格
    // 特权商品购买不需要花钱
    orderItem.setPrice(wp.getPrice());
    orderItem.setDiscount(wp.getPrice());
    orderItem.setDiscountPrice(BigDecimal.ZERO);
    orderItem.setMarketPrice(BigDecimal.ZERO);
    orderItem.setSkuId(wp.getSkuId());
    orderItem.setSkuStr("无");
    orderItem.setAmount(Integer.parseInt(erp.getCount()));
    orderItem.setPromoAmt(BigDecimal.valueOf(0));
    orderItem.setServerAmt(BigDecimal.valueOf(0));
    OrderAddress oa = new OrderAddress();

    BeanUtils.copyProperties(address1,oa);
    order.setThirdOrderNo(erp.getThirdOrderNo());
    order.setErpOrderNo(erp.getErpOrderNo());
    order.setLot(addressId);
    order.setThirdOrderItemNo(erp.getOrderItemNo());
    order.setFirstOrderNo(erp.getFirstOrderNo());
    order.setIslead("1");

    mainOrderService.save2(mainOrder,Collections.singletonMap(order, Collections.singletonList(orderItem)),oa,null,
            MainOrderStatus.PAID_NO_STOCK, OrderStatus.PAIDNOSTOCK,user);

    return Either.right(true);
  }

  /*
   * @Author chp
   * @Description // 物流导入生成订单
   * @Param
   * @return
   **/
  @Override
  @Transactional(rollbackFor = Exception.class)
  public Either<String,Boolean> createWinningOrder3(Long  cpid,String addressId,NotErpImportOrder erp) {

    // 获取奖品信息
    WinningProductVO wp = getWinningProductVO2(cpid,erp.getSkuCode())
            .getOrElseThrow(s -> new BizException(GlobalErrorCode.INVALID_ARGUMENT, s));
//  Address addressq = addressService.selectById(addressId);
    Address address1 = new Address();
    address1.setStreet(erp.getStreet());
    address1.setPhone(erp.getPhone());
    address1.setConsignee(erp.getReceiver());
    address1.setZoneId("1266");
    address1.setCpId(cpid);
    final String resourceType = StringUtils.defaultIfBlank(wp.getSkuCodeResources(), "CLIENT");
//
//    if (address == null){
//      return Either.left("地址不存在");
//    }

    // 通过cpid获取用户信息
    User user = userMapper.selectByCpId(cpid);
    MainOrder mainOrder = createMainOrder2( new BigDecimal(0),user);
    final Date now = new Date();
    final String payNo = UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.NPO);
    mainOrder.setPayNo(payNo);
    mainOrder.setPaidAt(now);
    Shop shop = shopService.loadRootShop();
    OrderImportVo order = new  OrderImportVo();
    order.setGoodsFee(new BigDecimal(0));
    order.setLogisticsFee(BigDecimal.ZERO);
    order.setLogisticDiscount(BigDecimal.ZERO);
    //新增的积分抵扣掉邮费的字段
    order.setCommissionLogisticsDiscount(BigDecimal.ZERO);
    order.setLogisticDiscountType(LogisticDiscountType.FREE_POSTAGE);
    order.setOrderType(OrderSortType.GIFT);
    order.setDiscountFee(new BigDecimal(0));
    order.setType(OrderType.DANBAO);
    order.setTotalFee(BigDecimal.ZERO);
    order.setBuyerId(user.getId());
    order.setShopId(shop.getId());
    order.setRootShopId(shop.getId());
    order.setSellerId(shop.getOwnerId());
    order.setNeedInvoice(false);
    order.setStatus(OrderStatus.PAIDNOSTOCK);
    order.setPaidAt(now);
    order.setPayNo(payNo);
    order.setIsPickup(0);
    order.setDestName(resourceType + "不可退商品");
    order.setDest(resourceType + "_NOT_REFUNDABLE");

    OrderItem orderItem = new OrderItem();
    orderItem.setProductId(wp.getProductId());
    orderItem.setProductName(wp.getName());
    orderItem.setProductImg(wp.getImg());
    orderItem.setPromotionType(PromotionType.GIFT);

    // 修改价格为计算后的价格
    // 特权商品购买不需要花钱
    orderItem.setPrice(wp.getPrice());
    orderItem.setDiscount(wp.getPrice());
    orderItem.setDiscountPrice(BigDecimal.ZERO);
    orderItem.setMarketPrice(BigDecimal.ZERO);

    orderItem.setSkuId(wp.getSkuId());
    orderItem.setSkuStr("无");
    orderItem.setAmount(Integer.parseInt(erp.getCount()));
    orderItem.setPromoAmt(BigDecimal.valueOf(0));
    orderItem.setServerAmt(BigDecimal.valueOf(0));
    OrderAddress oa = new OrderAddress();

    BeanUtils.copyProperties(address1,oa);
    order.setThirdOrderNo(erp.getOrderNo());
    order.setLot(addressId);
    order.setIslead("2");

    mainOrderService.save2(mainOrder,Collections.singletonMap(order, Collections.singletonList(orderItem)),oa,null,
            MainOrderStatus.PAID_NO_STOCK, OrderStatus.PAIDNOSTOCK,user);

    return Either.right(true);
  }

  private MainOrder createMainOrder(BigDecimal totalFee, User user) {
    MainOrder ret = new MainOrder();
    ret.setTotalFee(BigDecimal.ZERO);
    ret.setDiscountFee(totalFee);
    ret.setPaidFee(new BigDecimal(0));
    ret.setBuyerId(user.getId());
    //默认为担保交易类型
    ret.setType(OrderType.DANBAO);
    ret.setStatus(MainOrderStatus.PAID);
    return ret;
  }
  /*
   * @Author chp
   * @Description //创建导入主订单
   * @Date
   * @Param
   * @return
   **/
  private MainOrder createMainOrder2(BigDecimal totalFee, User user) {
    MainOrder ret = new MainOrder();
    ret.setTotalFee(BigDecimal.ZERO);
    ret.setDiscountFee(totalFee);
    ret.setPaidFee(new BigDecimal(0));
    ret.setBuyerId(user.getId());
    //默认为担保交易类型
    ret.setType(OrderType.DANBAO);
    ret.setStatus(MainOrderStatus.PAID_NO_STOCK);
    return ret;
  }
}