package com.xquark.service.winningList.vo;

public class WinningUser {

  private String userName;

  private String lucky;

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getLucky() {
    return lucky;
  }

  public void setLucky(String lucky) {
    this.lucky = lucky;
  }
}
