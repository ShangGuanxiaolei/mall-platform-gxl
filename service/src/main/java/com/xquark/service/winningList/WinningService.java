package com.xquark.service.winningList;

import com.xquark.dal.model.ErpImportOrder;
import com.xquark.dal.model.NotErpImportOrder;
import com.xquark.dal.model.WinningDetail;
import com.xquark.dal.model.XquarkOrderThirdErp;
import com.xquark.dal.vo.WinningListView;
import com.xquark.dal.vo.WinningProductVO;
import com.xquark.service.winningList.vo.WinningUser;
import io.vavr.control.Either;

import java.util.List;

public interface WinningService {

	//根据cpId来查询奖品列表
	Either<String, List<WinningListView>> getWinningList(Long cpId);

	//改变奖品的状态
	boolean updateWinningStatus(String winningId, String addressId);

	//查看领取详情
	WinningDetail getAddressDetail(String winningId, Long cpId);

	//奖品是否失效
	boolean isValid();

	List<WinningUser> getWinningUserList();

	Either<String,Boolean> createWinningOrder(String winningId,String addressId);
	Either<String,Boolean> createWinningOrder2(Long  cpid, String addressId, ErpImportOrder erp);
	Either<String,Boolean> createWinningOrder3(Long  cpid, String addressId, NotErpImportOrder erp);
	Either<String, WinningProductVO> getWinningProductVO(String winningId);
	Either<String, WinningProductVO> getWinningProductVO2(Long cpid,String skuCode);
}
