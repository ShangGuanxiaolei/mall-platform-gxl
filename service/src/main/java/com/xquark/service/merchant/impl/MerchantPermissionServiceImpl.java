package com.xquark.service.merchant.impl;

import com.xquark.dal.mapper.MerchantPermissionMapper;
import com.xquark.dal.model.MerchantPermission;
import com.xquark.dal.vo.MerchantPermissionVo;
import com.xquark.service.merchant.MerchantPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by quguangming on 16/5/23.
 */
@Service("merchantPermissionService")
public class MerchantPermissionServiceImpl implements MerchantPermissionService {

  @Autowired
  private MerchantPermissionMapper merchantPermissionMapper;

  @Override
  public List<MerchantPermissionVo> list() {
    return merchantPermissionMapper.selectAll();
  }

  @Override
  public MerchantPermissionVo load(String id) {
    return merchantPermissionMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<MerchantPermissionVo> listChildersByParentId(String pId) {
    return merchantPermissionMapper.listByParentId(pId);
  }

  @Override
  public List<MerchantPermissionVo> listByIds(String[] ids) {
    return merchantPermissionMapper.listByIds(ids);
  }

  @Override
  public int update(MerchantPermission permission) {
    return merchantPermissionMapper.updateByPrimaryKeySelective(permission);
  }

  @Override
  public int delete(String id) {
    return merchantPermissionMapper.deleteByPrimaryKey(id);
  }

  @Override
  public Long create(MerchantPermission permission) {
    return merchantPermissionMapper.insert(permission);
  }
}
