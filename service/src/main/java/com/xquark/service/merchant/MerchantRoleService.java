package com.xquark.service.merchant;

import com.xquark.dal.model.MerchantRole;
import com.xquark.dal.vo.MerchantRoleVO;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by quguangming on 16/5/23.
 */
public interface MerchantRoleService {

  MerchantRole load(String id);

  MerchantRole loadByName(String roleName);

  int update(MerchantRole merchantRole);

  List<MerchantRole> listByCreaterId(String createId);

  List<MerchantRole> listAll();

  Long create(MerchantRole merchantRole);

  List<MerchantRole> listRoleByMerchantId(String merchantid);

  List<MerchantRole> listRoleByMerchantName(String name);

  int delete(String id);

  /**
   * 列表查询
   */
  List<MerchantRoleVO> list(Pageable page, String keyword);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(String keyword);

  Long selectNormalCnt();

  /**
   * 根据角色编码获取角色名称
   */
  String getRoleDesc(List name);

}
