package com.xquark.service.merchant;

import com.xquark.dal.model.Merchant;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.annotation.Validated;

/**
 * Created by quguangming on 16/5/23.
 */
@Validated
public interface MerchantService extends UserDetailsService {

  Merchant loadByLoginName(String loginName);

  Merchant load(String id);

  Long create(Merchant merchant);

  List<Merchant> list(Map<String, Object> params, Pageable pageable);

  List<Merchant> listAll();

  int update(Merchant merchant);

  int delete(String id);

  List<Merchant> search(Map<String, Object> params, Pageable pageable);

  Long countMarchants(Map<String, Object> params);

  Merchant getCurrentMerchant();

  /**
   * 用户是否已注册
   */
  boolean isRegistered(String loginname);


  /**
   * 手机号是否已注册
   */
  boolean checkPhone(String phone);

  boolean isPhoneExists(String phone);

  /**
   * 修改密码
   *
   * @param oldPwd can be null
   * @param newPwd not null
   * @return changed?
   */
  boolean changePwd(String oldPwd, String newPwd);
}
