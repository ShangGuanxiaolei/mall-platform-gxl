package com.xquark.service.merchant.impl;

import com.xquark.dal.mapper.MerchantSigninLogMapper;
import com.xquark.dal.model.MerchantSigninLog;
import com.xquark.service.merchant.MerchantSigninLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("merchantSigninLogService")
public class MerchantSigninLogServiceImpl implements MerchantSigninLogService {

  @Autowired
  MerchantSigninLogMapper merchantSigninLogMapper;

  @Override
  public void insert(MerchantSigninLog merchantSigninLog) {
    merchantSigninLogMapper.insert(merchantSigninLog);
  }
}
