package com.xquark.service.merchant.impl;

import com.xquark.dal.mapper.MerchantRoleMapper;
import com.xquark.dal.model.MerchantRole;
import com.xquark.dal.vo.MerchantRoleVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.merchant.MerchantRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by quguangming on 16/5/23.
 */
@Service("merchantRoleService")
public class MerchantRoleServiceImpl implements MerchantRoleService {

  @Autowired
  private MerchantRoleMapper merchantRoleMapper;

  @Override
  public Long create(MerchantRole merchantRole) {
    MerchantRole roleExists = merchantRoleMapper.selectByRoleName(merchantRole.getRoleName());
    if (roleExists != null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "该角色已存在");
    }
    return merchantRoleMapper.insert(merchantRole);
  }

  @Override
  public MerchantRole load(String id) {
    return merchantRoleMapper.selectByPrimaryKey(id);
  }

  @Override
  public MerchantRole loadByName(String roleName) {
    return merchantRoleMapper.selectByRoleName(roleName);
  }

  @Override
  public int update(MerchantRole merchantRole) {
    return merchantRoleMapper.updateByPrimaryKey(merchantRole);
  }

  @Override
  public List<MerchantRole> listByCreaterId(String createId) {
    return merchantRoleMapper.listByCreaterId(createId);
  }

  @Override
  public List<MerchantRole> listAll() {
    return merchantRoleMapper.listAll();
  }

  @Override
  public List<MerchantRole> listRoleByMerchantId(String merchantId) {
    return merchantRoleMapper.listRolesByMerchantId(merchantId);
  }

  @Override
  public List<MerchantRole> listRoleByMerchantName(String name) {
    return merchantRoleMapper.listRolesByMerchantLoginName(name);
  }

  @Override
  public int delete(String id) {
    return merchantRoleMapper.delete(id);
  }

  /**
   * 列表查询
   */
  @Override
  public List<MerchantRoleVO> list(Pageable page, String keyword) {
    return merchantRoleMapper.list(page, keyword);
  }

  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long selectCnt(String keyword) {
    return merchantRoleMapper.selectCnt(keyword);
  }

  /**
   * 取出不包含ROOT和BASIC的角色总数
   */
  @Override
  public Long selectNormalCnt() {
    return merchantRoleMapper.selectNormalCnt();
  }

  /**
   * 根据角色编码获取角色名称
   */
  @Override
  public String getRoleDesc(List name) {
    return merchantRoleMapper.getRoleDesc(name);
  }
}

