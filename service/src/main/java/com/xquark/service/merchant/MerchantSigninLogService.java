package com.xquark.service.merchant;

import com.xquark.dal.model.MerchantSigninLog;

public interface MerchantSigninLogService {

  void insert(MerchantSigninLog marchantSigninLog);

}
