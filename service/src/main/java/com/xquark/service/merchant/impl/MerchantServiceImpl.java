package com.xquark.service.merchant.impl;


import com.xquark.cache.ButtonCache;
import com.xquark.cache.ModuleCache;
import com.xquark.cache.UrlRoleCache;
import com.xquark.dal.mapper.MerchantMapper;
import com.xquark.dal.mapper.MerchantRoleMapper;
import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.MerchantRole;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.merchant.MerchantService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created by quguangming on 16/5/23.
 */

//@Service("merchantService")
//service已在applicationContext-service.xml中定义
public class MerchantServiceImpl implements MerchantService {

  protected Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private MerchantMapper merchantMapper;

  @Autowired
  private MerchantRoleMapper merchantRoleMapper;

  @Autowired
  private ModuleCache moduleCache;

  @Autowired
  private ButtonCache buttonCache;

  @Autowired
  private UrlRoleCache roleCache;

  @Autowired
  private PasswordEncoder pwdEncoder;

  @Override
  public Merchant loadByLoginName(String loginName) {
    return merchantMapper.loadByLoginName(loginName);
  }

  @Override
  public Merchant load(String id) {
    return merchantMapper.selectByPrimaryKey(id);
  }

  @Override
  public Long create(Merchant merchant) {
    Merchant m = merchantMapper.loadByLoginName(merchant.getLoginname());
    if (m != null) {
      return -1L;
    }
    merchant.setPassword(pwdEncoder.encode(merchant.getPassword()));
    return merchantMapper.insert(merchant);
  }

  @Override
  public List<Merchant> list(Map<String, Object> params, Pageable pageable) {
    return merchantMapper.listByMerchantId(params, pageable);
  }

  @Override
  public List<Merchant> listAll() {
    return merchantMapper.listAll();
  }

  @Override
  public int update(Merchant merchant) {

    return merchantMapper.updateByPrimaryKey(merchant);
  }

  @Override
  public int delete(String id) {
    return merchantMapper.deleteByPrimaryKey(id);
  }

  @Override
  public List<Merchant> search(Map<String, Object> params, Pageable pageable) {
    return merchantMapper.listByKeyword(params, pageable);
  }

  @Override
  public Long countMarchants(Map<String, Object> params) {
    return merchantMapper.countMerchants(params);
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Merchant detail = merchantMapper.loadByLoginName(username);
    if (detail == null) {
      throw new UsernameNotFoundException("user not exist");
    } else {
      String prefix = "ROLE_";
      //TODO 缓存用户角色
      List<MerchantRole> merchantRoles = merchantRoleMapper
          .listRolesByMerchantLoginName(username.trim());
      List<String> strRoles = new ArrayList<String>();
      if (merchantRoles != null) {
        for (MerchantRole merchantRole : merchantRoles) {
          String role = merchantRole.getRoleName();
          if (!role.startsWith(prefix)) {
            role = prefix + role;
          }
          strRoles.add(role);
        }
      } else {
        // TODO wangxh 暂时与老的角色兼容
        String oldRoles = detail.getRoles();
        if (oldRoles != null) {
          for (String role : oldRoles.split(",")) {
            strRoles.add(prefix + role);
          }
        } else {
          strRoles.add("ROLE_BASIC");
        }
      }
      detail.setRoleList(strRoles);
      // 用户登录时刷新缓存
      moduleCache.refreshByUser(detail);
      roleCache.refresh();
      return detail;
    }
  }

  /**
   * 获取当前merchant信息 具体逻辑查看：UniqueNoFilter
   */
  @Override
  public Merchant getCurrentMerchant() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof Merchant) {
        Merchant currentMerchant = (Merchant) principal;
        Merchant merchant = this.load(currentMerchant.getId());
        return merchant;
      }
    }

    throw new BizException(GlobalErrorCode.UNAUTHORIZED, "need login first.");
  }

  /**
   * 用户是否已注册
   */
  public boolean isRegistered(String loginname) {
    return merchantMapper.countRegistered(loginname) == 0;
  }

  /**
   * 手机号是否已注册
   */
  public boolean checkPhone(String phone) {
    return merchantMapper.countPhone(phone) == 0;
  }

  @Override
  public boolean isPhoneExists(String phone) {
    return merchantMapper.checkPhoneExits(phone);
  }


  @Override
  public boolean changePwd(String oldPwd, String newPwd) {

    Merchant merchant = getCurrentMerchant();

    merchant = load(merchant.getId());

    if (StringUtils.isNotBlank(oldPwd) && !pwdEncoder.matches(oldPwd, merchant.getPassword())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "原密码不正确");
    }

    if (!StringUtils.isNotBlank(oldPwd) && !StringUtils.isNotBlank(merchant.getPassword())
        || oldPwd != null && merchant.getPassword() != null && pwdEncoder
        .matches(oldPwd, merchant.getPassword())) {
      newPwd = pwdEncoder.encode(newPwd);
      boolean changed = merchantMapper.changePwd(merchant.getId(), newPwd) > 0;
      if (changed) {
        merchant.setPassword(newPwd);
      }
      return changed;
    }
    return false;
  }
}
