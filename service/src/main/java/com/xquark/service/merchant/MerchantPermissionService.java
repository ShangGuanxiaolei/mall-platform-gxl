package com.xquark.service.merchant;

import com.xquark.dal.model.MerchantPermission;
import com.xquark.dal.vo.MerchantPermissionVo;

import java.util.List;

/**
 * Created by quguangming on 16/5/23.
 */
public interface MerchantPermissionService {


  List<MerchantPermissionVo> list();

  MerchantPermissionVo load(String id);

  List<MerchantPermissionVo> listChildersByParentId(String pId);

  List<MerchantPermissionVo> listByIds(String[] ids);

  int update(MerchantPermission permission);

  int delete(String id);

  Long create(MerchantPermission permission);

}
