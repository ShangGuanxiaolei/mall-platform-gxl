package com.xquark.service.zone.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.ZoneMapper;
import com.xquark.dal.model.PostConf;
import com.xquark.dal.model.Zone;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.zone.ZoneService;

@Service("zoneService")
public class ZoneServiceImpl extends BaseServiceImpl implements ZoneService {

  @Autowired
  private ZoneMapper zoneMapper;

  @Override
  public List<Zone> listRoots() {
    return zoneMapper.listRoots();
  }

  @Override
  public List<Zone> listChildren(String zoneId) {
    List<Zone> list = zoneMapper.listChildren(zoneId);
    if (list == null) {
      return Collections.emptyList();
    }

    return list;
  }

  @Override
  public Zone findParent(String zoneId) {
    return zoneMapper.findParent(zoneId);
  }

  @Override
  public Zone load(String id) {
    return zoneMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<Zone> listParents(String zoneId) {
    Zone zone = zoneMapper.selectByPrimaryKey(zoneId);
    if (zone == null) {
      return Collections.emptyList();
    }

    String path = zone.getPath();
    List<Zone> zones = findByIds(path.split(">"));
    zones.add(zone);
    return zones;
  }

  private List<Zone> findByIds(String... zoneIds) {
    if (ArrayUtils.isEmpty(zoneIds)) {
      return Collections.emptyList();
    }

    List<Zone> list = zoneMapper.selectByIds(zoneIds);
    if (list == null) {
      return Collections.emptyList();
    }
    return list;
  }

  @Override
  public Map<String, List<String>> loadPostFreeList() {
    List<PostConf> zoneList = zoneMapper.selectPostConf();
    if (null == zoneList || 0 == zoneList.size()) {
      return null;
    }
    Map<String, List<String>> ret = new HashedMap<String, List<String>>();
    List<String> commonList = new ArrayList<String>();
    List<String> cityList = new ArrayList<String>();
    List<String> provList = new ArrayList<String>();
    for (PostConf aZone : zoneList) {
      if (aZone.getRegionType().equals("0")) {
        commonList.add(aZone.getName());
      } else if (aZone.getRegionType().equals("1")) {
        cityList.add(aZone.getName());
      } else if (aZone.getRegionType().equals("2")) {
        provList.add(aZone.getName());
      } else {
      }
    }
    ret.put("common", commonList);
    ret.put("city", cityList);
    ret.put("province", provList);
    return ret;
  }

  @Override
  public void updateZonePath(String id) {
    Zone zone = zoneMapper.selectByPrimaryKey(id);
    Zone parent = zoneMapper.selectByPrimaryKey(zone.getParentId());
    if (parent != null) {
      zoneMapper.updatePath(id, parent.getPath() + parent.getId() + ">");
    }
    List<Zone> children = zoneMapper.listChildren(id);
    for (Zone z : children) {
      updateZonePath(z.getId());
    }
  }

  @Override
  public List<Zone> listSiblings(String zoneId) {
    return zoneMapper.listSiblings(zoneId);
  }

  @Override
  public List<Zone> listPostageZones() {
    return zoneMapper.listPostAgeArea();
  }

  @Override
  public List<Zone> listPostageCityZones() {
    return zoneMapper.selectCity4PostAge();
  }

  @Override
  public Map<String, String> listAllAddressExceptStreet() {
    List<Zone> zoneList = zoneMapper.listAll();
    Map<String, Zone> zoneMap = new HashMap<String, Zone>();
    for (Zone zone : zoneList) {
      zoneMap.put(zone.getId(), zone);
    }
    Map<String, String> detailMap = new HashMap<String, String>();
    String details = null;
    for (String zoneId : zoneMap.keySet()) {
      Zone zone = zoneMap.get(zoneId);
      details = "";
      if (!zone.getPath().isEmpty()) {
        String[] paths = zone.getPath().split(">");
        for (String path : paths) {
          details += zoneMap.get(path).getName();
        }
      }
      details += zone.getName();
      detailMap.put(zoneId, details);
    }
    return detailMap;
  }

  /**
   * 通过汇购网对应的zoneid，查找代理系统的zone信息
   */
  @Override
  public Zone selectByExtId(String extId) {
    return zoneMapper.selectByExtId(extId);
  }

  /**
   * 查找某个地区下面所有的下级地区，包含自己
   */
  @Override
  public List<Zone> listAllChildrenIncludeSelf(String zoneId, String likeZoneId) {
    return zoneMapper.listAllChildrenIncludeSelf(zoneId, likeZoneId);
  }

  @Override
  public Zone loadByZipCode(String zipCode) {
    return zoneMapper.selectByZipCode(zipCode);
  }

}
