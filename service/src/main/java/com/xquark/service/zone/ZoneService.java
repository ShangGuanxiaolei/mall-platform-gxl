package com.xquark.service.zone;

import java.util.List;
import java.util.Map;

import com.xquark.dal.model.Zone;

public interface ZoneService {

  Zone load(String id);

  List<Zone> listRoots();

  List<Zone> listChildren(String zoneId);

  List<Zone> listSiblings(String zoneId);

  Zone findParent(String zoneId);

  /**
   * 获取地区以及地区的父级地区
   */
  List<Zone> listParents(String zoneId);

  Map<String, List<String>> loadPostFreeList();

  void updateZonePath(String id);

  List<Zone> listPostageZones();

  List<Zone> listPostageCityZones();

  Map<String, String> listAllAddressExceptStreet();

  /**
   * 通过汇购网对应的zoneid，查找代理系统的zone信息
   */
  Zone selectByExtId(String extId);

  /**
   * 查找某个地区下面所有的下级地区，包含自己
   */
  List<Zone> listAllChildrenIncludeSelf(String zoneId, String likeZoneId);

  Zone loadByZipCode(String zipCode);
}
