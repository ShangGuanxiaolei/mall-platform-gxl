package com.xquark.service.smstpl.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.SmsTplMapper;
import com.xquark.dal.mapper.SmsVarTplMapper;
import com.xquark.dal.model.SmsTpl;
import com.xquark.dal.model.SmsVarTpl;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.smstpl.SmsTplService;

@Service("SmsTplService")
public class SmsTplServiceImpl extends BaseServiceImpl implements SmsTplService {

  @Autowired
  private SmsTplMapper smsTplMapper;

  @Autowired
  private SmsVarTplMapper smsVarTplMapper;

  @Override
  public int delete(String id) {
    return smsTplMapper.delete(id);
  }

  @Override
  public int insert(SmsTpl e) {
    return smsTplMapper.insert(e);
  }

  @Override
  public int insertOrder(SmsTpl smsTpl) {
    return smsTplMapper.insert(smsTpl);
  }

  @Override
  public SmsTpl load(String id) {
    return smsTplMapper.load(id);
  }

  @Override
  public int undelete(String id) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public List<SmsTpl> loadByParam(Map<String, String> params) {
    return smsTplMapper.selectByParam(params);
  }

  @Override
  public SmsTpl getUniqueTpl(Map<String, String> params) {
    List<SmsTpl> aList = getSmsTpl(params);
    if (aList != null && aList.size() != 0) {
      return aList.get(0);
    } else {
      return null;
    }
  }

  @Override
  public List<SmsTpl> getSmsTpl(Map<String, String> params) {
    if (!params.containsKey("smsEvent")) {
      return null;
    }
    if (!params.containsKey("countryCode")) {
      params.put("countryCode", "CN");
    }
    if (!params.containsKey("planForm")) {
      params.put("planForm", UserPartnerType.KKKD.toString());
    }
    if (!params.containsKey("status")) {
      params.put("status", "VALID");
    }

    return smsTplMapper.selectByParam(params);
  }


  @Override
  public Boolean checkFmtVars(String[] fmtVars) {
    if (fmtVars == null) {
      return false;
    }
    List<SmsVarTpl> aList = smsVarTplMapper.loadAll();
    if (aList != null && aList.size() != 0) {
      for (String var : fmtVars) {
        if (!findInList(var, aList)) {
          return false;
        }
      }
    } else {
      return false;
    }
    return true;
  }

  private Boolean findInList(String var, List<SmsVarTpl> aList) {
    Boolean ret = false;
    for (SmsVarTpl svt : aList) {
      if (!svt.getVarName().equalsIgnoreCase(var)) {
        ret = false;
      } else {
        ret = true;
        break;
      }
    }
    return ret;
  }

}
