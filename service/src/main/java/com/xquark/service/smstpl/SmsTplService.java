package com.xquark.service.smstpl;

import java.util.List;
import java.util.Map;

import com.xquark.dal.model.SmsTpl;
import com.xquark.service.ArchivableEntityService;

public interface SmsTplService extends ArchivableEntityService<SmsTpl> {

  SmsTpl load(String id);

  List<SmsTpl> loadByParam(Map<String, String> params);

  SmsTpl getUniqueTpl(Map<String, String> params);

  List<SmsTpl> getSmsTpl(Map<String, String> params);

  Boolean checkFmtVars(String[] fmtVars);

}
