package com.xquark.service.ecomho;

import com.xquark.dal.model.Order;
import com.xquark.dal.vo.LogisticInfoVO;
import com.xquark.service.order.vo.OrderEcmohoVO;

public interface EcmohoErpService {

  public boolean sendOrder(OrderEcmohoVO orderEcmohoVO);

  public boolean checkOrderData(OrderEcmohoVO orderEcmohoVO);

  public LogisticInfoVO getLogisticForJKM(String orderNumber);

  public boolean sendOrder(Order order);
}
