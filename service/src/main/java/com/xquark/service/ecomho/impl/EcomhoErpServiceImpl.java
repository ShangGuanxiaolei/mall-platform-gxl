package com.xquark.service.ecomho.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.model.*;
import com.xquark.dal.vo.LogisticInfoVO;
import com.xquark.service.ecomho.EcmohoErpService;
import com.xquark.service.order.OrderAddressService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.vo.OrderEcmohoVO;
import com.xquark.service.order.vo.OrderItemForErpVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.zone.ZoneService;
import com.xquark.utils.EcmohoErpUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("ecmohoErpService")
public class EcomhoErpServiceImpl implements EcmohoErpService {

  private Logger log = LoggerFactory.getLogger(EcomhoErpServiceImpl.class);

  private static String orderXml = "<OWebOrder></OWebOrder>";

  private static String postOrderUrl = "http://openapi.51shop.mobi/M/saveOrUpdateOrder";

  private static String postLogsticUrl = "http://openapi.51shop.mobi/M/OrderDetail";

  private static String method_Name = "swapi.web.order.saveOrUpdateExtra";

  private static String prefix = "@#@";

  @Autowired
  private OrderService orderService;

  @Autowired
  private ProductService productService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private OrderAddressService orderAddressService;

  @Override
  public boolean sendOrder(OrderEcmohoVO orderEcmohoVO) {
    boolean isSuccess = false;
    if (orderEcmohoVO != null) {
      String postXml = getXmlToString(orderEcmohoVO);
      log.info("send xml==" + postXml);
      String orderNo = orderEcmohoVO.getOrderNumber();
      if (StringUtils.isNotBlank(orderEcmohoVO.getWarehouse())) {
        isSuccess = postOrderErp(orderNo, postXml, orderEcmohoVO.getWarehouse());
      }
    }
    return isSuccess;
  }

  @Override
  public boolean checkOrderData(OrderEcmohoVO orderEcmohoVO) {
    boolean isSuccess = false;
    //payment=    price   *（orderCount-giftCount）-discountFee-discount+ postAmount
    //实付金额     =    商品折前价格 *（数量                           -赠送数量            ）-商品优惠金额-    订单优惠金额+ 运费
    if (orderEcmohoVO != null) {
      BigDecimal discountFee = orderEcmohoVO.getDiscount();
      BigDecimal postAmount = orderEcmohoVO.getPostAmount();
      BigDecimal payment = orderEcmohoVO.getPayment();
      BigDecimal isPayment = new BigDecimal(0);
      if (orderEcmohoVO.getOrderItemForErpVO() != null
          && orderEcmohoVO.getOrderItemForErpVO().size() > 0) {
        for (int i = 0; i < orderEcmohoVO.getOrderItemForErpVO().size(); i++) {
          OrderItemForErpVO orderItem = orderEcmohoVO.getOrderItemForErpVO().get(i);
          BigDecimal priceItem = orderItem.getPrice();
          Integer countItem = orderItem.getOrderCount();
          Integer giftCountItem = orderItem.getGiftCount();
          BigDecimal discountFeeItem = orderItem.getDiscountFee();
          isPayment = isPayment.add(priceItem.multiply(new BigDecimal(countItem - giftCountItem))
              .subtract(discountFeeItem));
        }
      }
      isPayment = isPayment.subtract(discountFee).add(postAmount);
      if (payment.compareTo(isPayment) == 0) {
        isSuccess = true;
      }
    }

    return isSuccess;
  }

  @Override
  public LogisticInfoVO getLogisticForJKM(String orderNumber) {
    LogisticInfoVO logisticInfo;
    logisticInfo = postLogistic(orderNumber);
    return logisticInfo;
  }

  private String getXmlToString(OrderEcmohoVO orderForErpVO) {
    SAXBuilder builder = new SAXBuilder();
    StringReader stringReader = new StringReader(orderXml);

    InputSource inputSource = new InputSource(stringReader);
    Document document = null;
    try {
      document = builder.build(inputSource);

      Element goods = document.getRootElement();
      // Element goods = root.getChild("OWebOrder");

      Element orderNumberEl = new Element("orderNumber");
      orderNumberEl.setText(orderForErpVO.getOrderNumber());
      goods.addContent(orderNumberEl);

      Element orderDateEl = new Element("orderDate");
      orderDateEl.setText(orderForErpVO.getOrderDate());
      goods.addContent(orderDateEl);

      Element payTimeEl = new Element("payTime");
      payTimeEl.setText(orderForErpVO.getPayTime());
      goods.addContent(payTimeEl);

      Element buyerNickEl = new Element("buyerNick");
      buyerNickEl.setText(orderForErpVO.getBuyerNick());
      goods.addContent(buyerNickEl);

      Element totalAmountEl = new Element("totalAmount");
      totalAmountEl.setText(orderForErpVO.getTotalAmount());
      goods.addContent(totalAmountEl);

      Element paymentEl = new Element("payment");
      paymentEl.setText(orderForErpVO.getPayment().toPlainString());
      goods.addContent(paymentEl);

      Element factPaymentEl = new Element("factPayment");
      factPaymentEl.setText(orderForErpVO.getFactPayment());
      goods.addContent(factPaymentEl);

      Element discountEl = new Element("discount");
      discountEl.setText(orderForErpVO.getDiscount().toPlainString());
      goods.addContent(discountEl);

      Element postAmountEl = new Element("postAmount");
      postAmountEl.setText(orderForErpVO.getPostAmount().toPlainString());
      goods.addContent(postAmountEl);

      Element invoiceTitleEl = new Element("invoiceTitle");
      invoiceTitleEl.setText(orderForErpVO.getInvoiceTitle());
      goods.addContent(invoiceTitleEl);

      Element invoiceContentEl = new Element("invoiceContent");
      invoiceContentEl.setText(orderForErpVO.getInvoiceContent());
      goods.addContent(invoiceContentEl);

      Element invoiceAmountEl = new Element("invoiceAmount");
      invoiceAmountEl.setText(orderForErpVO.getInvoiceAmount());
      goods.addContent(invoiceAmountEl);

      Element tradeFromEl = new Element("tradeFrom");
      tradeFromEl.setText(orderForErpVO.getTradeFrom());
      goods.addContent(tradeFromEl);

      Element tradeStatusEl = new Element("tradeStatus");
      tradeStatusEl.setText(orderForErpVO.getTradeStatus());
      goods.addContent(tradeStatusEl);

      Element refundStatusEl = new Element("refundStatus");
      refundStatusEl.setText(orderForErpVO.getRefundStatus());
      goods.addContent(refundStatusEl);

      Element paymentTypeEL = new Element("paymentType");
      paymentTypeEL.setText(orderForErpVO.getPaymentType());
      goods.addContent(paymentTypeEL);

      Element postTypeEl = new Element("postType");
      postTypeEl.setText(orderForErpVO.getPostType());
      goods.addContent(postTypeEl);

      Element warehouseEl = new Element("warehouse");
      warehouseEl.setText(orderForErpVO.getWarehouse());
      goods.addContent(warehouseEl);

      Element consigneeEl = new Element("consignee");
      consigneeEl.setText(orderForErpVO.getConsignee());
      goods.addContent(consigneeEl);

      Element provinceEl = new Element("province");
      provinceEl.setText(orderForErpVO.getProvince());
      goods.addContent(provinceEl);

      Element cityEl = new Element("city");
      cityEl.setText(orderForErpVO.getCity());
      goods.addContent(cityEl);

      Element cityareaEl = new Element("cityarea");
      cityareaEl.setText(orderForErpVO.getCityarea());
      goods.addContent(cityareaEl);

      Element addressEl = new Element("address");
      addressEl.setText(orderForErpVO.getAddress());
      goods.addContent(addressEl);

      Element mobilePhoneEl = new Element("mobilePhone");
      mobilePhoneEl.setText(orderForErpVO.getMobilePhone());
      goods.addContent(mobilePhoneEl);

      Element telephoneEl = new Element("telephone");
      telephoneEl.setText(orderForErpVO.getTelephone());
      goods.addContent(telephoneEl);

      Element zipEl = new Element("zip");
      zipEl.setText(orderForErpVO.getZip());
      goods.addContent(zipEl);

      Element sellerMemoEl = new Element("sellerMemo");
      if (orderForErpVO.getIsCrossBorder() == 1) {
        if (orderForErpVO.getIdNumber() != null && !orderForErpVO.getIdNumber().equals("")) {
          String idNumber = orderForErpVO.getIdNumber();
          sellerMemoEl.setText(prefix + idNumber + prefix + orderForErpVO.getSellerMemo());
        }
      } else {
        sellerMemoEl.setText(orderForErpVO.getSellerMemo());
      }
      goods.addContent(sellerMemoEl);

      Element buyerMessageEl = new Element("buyerMessage");
      buyerMessageEl.setText(orderForErpVO.getBuyerMessage());
      goods.addContent(buyerMessageEl);

      Element OWebOrderItemLists = new Element("OWebOrderItemLists");
      OWebOrderItemLists.setText("");
      goods.addContent(OWebOrderItemLists);

      for (int i = 0; i < orderForErpVO.getOrderItemForErpVO().size(); i++) {
        Element OWebOrderItemList = new Element("OWebOrderItemList");
        OWebOrderItemList.setText("");
        OWebOrderItemLists.addContent(OWebOrderItemList);
        // item
        OrderItemForErpVO orderItemForErpVO = orderForErpVO.getOrderItemForErpVO().get(i);

        Element productNumberEl = new Element("productNumber");
        productNumberEl.setText(orderItemForErpVO.getProductNumber());
        OWebOrderItemList.addContent(productNumberEl);

        Element productNameEl = new Element("productName");
        productNameEl.setText(orderItemForErpVO.getProductName());
        OWebOrderItemList.addContent(productNameEl);

        Element skuNumberEl = new Element("skuNumber");
        skuNumberEl.setText(orderItemForErpVO.getSkuNumber());
        OWebOrderItemList.addContent(skuNumberEl);

        Element skuNameEl = new Element("skuName");
        skuNameEl.setText(orderItemForErpVO.getSkuName());
        OWebOrderItemList.addContent(skuNameEl);

        Element priceEl = new Element("price");
        priceEl.setText(orderItemForErpVO.getPrice().toPlainString());
        OWebOrderItemList.addContent(priceEl);

        Element orderCountEl = new Element("orderCount");
        orderCountEl.setText(orderItemForErpVO.getOrderCount().toString());
        OWebOrderItemList.addContent(orderCountEl);

        Element giftCountEl = new Element("giftCount");
        giftCountEl.setText(orderItemForErpVO.getGiftCount().toString());
        OWebOrderItemList.addContent(giftCountEl);

        Element amountEl = new Element("amount");
        amountEl.setText(orderItemForErpVO.getAmount().toString());
        OWebOrderItemList.addContent(amountEl);

        Element memoEl = new Element("memo");
        memoEl.setText(orderItemForErpVO.getMemo());
        OWebOrderItemList.addContent(memoEl);

        Element discountFeeEl = new Element("discountFee");
        discountFeeEl.setText(orderItemForErpVO.getDiscountFee().toPlainString());
        OWebOrderItemList.addContent(discountFeeEl);

        Element barcodeEl = new Element("barcode");
        barcodeEl.setText(orderItemForErpVO.getBarcode());
        OWebOrderItemList.addContent(barcodeEl);
      }
      document.setRootElement(goods);
    } catch (JDOMException e) {
      log.info("erp error =" + e.getMessage());

    } catch (IOException e) {
      log.info("erp error =" + e.getMessage());

    }
    XMLOutputter out = new XMLOutputter();
    ByteArrayOutputStream returnByte = new ByteArrayOutputStream();
    try {
      out.output(document, returnByte);
    } catch (IOException e) {
      log.info("erp error =" + e.getMessage());
    }
    return returnByte.toString();
  }

  private boolean postOrderErp(String orderId, String postXml, String warehouse) {

    DefaultHttpClient httpclient = new DefaultHttpClient();
    HttpPost httpPost;
    httpPost = new HttpPost(postOrderUrl);
    String methodName = method_Name;
    String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        .format(new Date());
    String name = warehouse;
    String nick = warehouse;
    String formats = "xml";
    boolean isSuccess = false;
    String signAfter = "";
    try {
      signAfter = EcmohoErpUtils.getSignMd5(nick, methodName, name, date,
          orderId, formats);
    } catch (NoSuchAlgorithmException e) {
      log.error("ecmho senderror[" + orderId + "] "
          + " error = " + e.getMessage());
    }

    List<NameValuePair> nvps = new ArrayList<NameValuePair>();
    nvps.add(new BasicNameValuePair("nick", nick));
    nvps.add(new BasicNameValuePair("orderData", postXml));
    nvps.add(new BasicNameValuePair("name", name));
    nvps.add(new BasicNameValuePair("date", EcmohoErpUtils
        .StringToTimestamp(date).toString()));
    nvps.add(new BasicNameValuePair("sign", signAfter));
    nvps.add(new BasicNameValuePair("format", formats));
    try {
      UrlEncodedFormEntity entitys = new UrlEncodedFormEntity(nvps,
          "UTF-8");
      httpPost.setEntity(entitys);
      HttpResponse response = httpclient.execute(httpPost);
      HttpEntity entity = response.getEntity();

      if (entity != null) {
        long len = entity.getContentLength();
        if (len != -1 && len < 2048) {
          String resultXml = EntityUtils.toString(entity, "UTF-8");
          log.info("ecmoho receive  [" + orderId + "] " + " result = "
              + resultXml);
          isSuccess = EcmohoErpUtils.getErpPostResult(resultXml);
        }
      }
    } catch (Exception e) {
      log.error("ecmoho send to erp error [" + orderId + "] "
          + " error = " + e.getMessage());
    }

    return isSuccess;

  }

  private LogisticInfoVO postLogistic(String orderNumber) {
    DefaultHttpClient httpclient = new DefaultHttpClient();
    String name = "";
    HttpEntity entity;
    String content;
    LogisticInfoVO logisticInfo = new LogisticInfoVO();

    HttpPost httpPost = new HttpPost(postLogsticUrl);
    List<NameValuePair> nvps = new ArrayList<NameValuePair>();
    nvps.add(new BasicNameValuePair("orderNumber", orderNumber));
    nvps.add(new BasicNameValuePair("name", name));
    try {
      httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
      HttpResponse response = httpclient.execute(httpPost);
      entity = response.getEntity();
      content = EntityUtils.toString(entity, "UTF-8");
      logisticInfo = parseLogisticContent(content);


    } catch (UnsupportedEncodingException e) {
      log.error("get synchro order failed orderId=[" + orderNumber + "]"
          + "  message=" + e.getMessage());

    } catch (ClientProtocolException e) {
      log.error("get synchro order failed orderId=[" + orderNumber + "]"
          + "  message=" + e.getMessage());

    } catch (IOException e) {
      log.error("get synchro order failed orderId=[" + orderNumber + "]"
          + "  message=" + e.getMessage());

    }
    return logisticInfo;
  }

  private LogisticInfoVO parseLogisticContent(String content) {

    JSONObject object = JSON.parseObject(content);
    JSONObject objOrders = object.getJSONObject("erpOrders");
    LogisticInfoVO logisticInfo = new LogisticInfoVO();
    if (null != objOrders) {
      JSONArray arrayOrder = objOrders.getJSONArray("erpOrder");
      if (null != arrayOrder) {
        for (int i = 0; i < arrayOrder.size(); i++) {

          JSONObject arrObject = (JSONObject) arrayOrder.get(i);
          // 只更新3个状态
          String statusERP = arrObject.getString("orderStatus");

          logisticInfo.setStatusERP(statusERP);
          String logisticsCompany = arrObject
              .getString("logisticName");
          logisticInfo.setLogisticsCompany(logisticsCompany);

          String logisticsOrderNo = arrObject
              .getString("expressNumber");
          logisticInfo.setLogisticsNo(logisticsOrderNo);

          String orderNumber = arrObject
              .getString("orderNumber");
          logisticInfo.setOrderNumber(orderNumber);

        }
      }
    }
    return logisticInfo;

  }

  @Override
  public boolean sendOrder(Order order) {
    OrderEcmohoVO orderEcmohoVO = getOrderEcmohoVOFromOrder(order);
    orderEcmohoVO = setEcmohoErpWarehouse(orderEcmohoVO, order);
    sendOrder(orderEcmohoVO);
    return false;
  }

  public OrderEcmohoVO getOrderEcmohoVOFromOrder(Order order) {
    OrderEcmohoVO orderForErpVO = new OrderEcmohoVO();
    OrderAddress orderAddress = orderAddressService.selectByOrderId(order.getId());
    List<Zone> zoneList = zoneService.listParents(orderAddress.getZoneId());
    List<OrderItem> itemList = orderService.listOrderItems(order.getId());
    String province = "";
    String city = "";
    String cityArea = "";
    String phone = orderAddress.getPhone();
    String cardId = "";
    if (StringUtils.isNotBlank(orderAddress.getCertificateId())) {
      cardId = orderAddress.getCertificateId();
    }

    String remark = order.getRemark();
    BigDecimal isPayment = new BigDecimal(0);
    BigDecimal discountFee = order.getDiscountFee();
    BigDecimal postAmount = order.getLogisticsFee();
    if (zoneList.size() > 3) {
      province = zoneList.get(1).getName();
      city = zoneList.get(2).getName();
      cityArea = zoneList.get(3).getName();
    }
    orderForErpVO.setAddress(orderAddress.getStreet());      //地址
    orderForErpVO.setBuyerMessage(""); //买家留言
    orderForErpVO.setBuyerNick("");  //买家帐号
    orderForErpVO.setProvince(province);
    orderForErpVO.setCity(city);       //省
    orderForErpVO.setCityarea(cityArea);   //区
    orderForErpVO.setConsignee(orderAddress.getConsignee());  //收货人
    orderForErpVO.setDiscount(order.getDiscountFee());   //订单优惠金额
    orderForErpVO.setFactPayment(order.getTotalFee().toPlainString()); //订单实付金额
    orderForErpVO.setInvoiceAmount("0");  //发票金额
    orderForErpVO.setInvoiceContent(""); //发票内容|种类
    orderForErpVO.setInvoiceTitle("");  //发票抬头
    orderForErpVO.setMobilePhone(phone);   //手机号
    orderForErpVO.setOrderDate(
        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(order.getCreatedAt()));  //下单日期
    orderForErpVO.setOrderNumber(order.getOrderNo());//订单编号
    orderForErpVO.setPaymentType(order.getPayType().toString());
    orderForErpVO
        .setPayTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));    //付款时间
    orderForErpVO.setPostAmount(order.getLogisticsFee()); //邮费
    //orderForErpVO.setPostAmount("0"); //邮费
    orderForErpVO.setPostType("");   //配送方式(物流公司)
    orderForErpVO.setRefundStatus(""); //退款状态
    orderForErpVO.setTelephone("");    //电话号码
    orderForErpVO.setTotalAmount(order.getTotalFee().toPlainString()); //商品总金额
    orderForErpVO.setTradeFrom("EasyShop");   //订单来源
    orderForErpVO.setTradeStatus("已付款"); //订单状态
    orderForErpVO.setWarehouse("");   //所属仓库
    orderForErpVO.setZip("");         //邮编
    //跨境必填 sellerMemo @#@身份证号@#@
    if (cardId != null && !cardId.equals("")) {
      orderForErpVO.setSellerMemo(prefix + cardId + prefix + remark);
      orderForErpVO.setWarehouse("EasyShop（海外）");
    } else {
      orderForErpVO.setSellerMemo(remark);   //卖家备注
      orderForErpVO.setWarehouse("EasyShop");
    }
    List<OrderItemForErpVO> orderItemList = new ArrayList<OrderItemForErpVO>();
    for (OrderItem orderItem : itemList) {
      BigDecimal price = orderItem.getPrice();
      BigDecimal orderCount = new BigDecimal(orderItem.getAmount());
      BigDecimal giftCount = new BigDecimal(0);
      //BigDecimal discount =  new  BigDecimal(0);
      String productId = orderItem.getProductId();
      String skuId = orderItem.getSkuId();
      Sku sku = productService.loadSku(productId, skuId);
      String code = sku.getSkuCode();
      OrderItemForErpVO orderItemForErpVO = new OrderItemForErpVO();
      orderItemForErpVO.setAmount(orderItem.getAmount());
      orderItemForErpVO.setBarcode("");
      orderItemForErpVO.setDiscountFee(new BigDecimal(0));
      orderItemForErpVO.setGiftCount(0);
      orderItemForErpVO.setMemo("");
      orderItemForErpVO.setOrderCount(orderItem.getAmount());
      orderItemForErpVO.setPrice(orderItem.getPrice());
      orderItemForErpVO.setProductName(orderItem.getProductName());
      orderItemForErpVO.setProductNumber(code);
      orderItemForErpVO.setSkuNumber(code);
      orderItemForErpVO.setSkuName(orderItem.getSkuStr());
      orderItemList.add(orderItemForErpVO);
      isPayment = isPayment.add(price.multiply(orderCount.subtract(giftCount)));
    }
    isPayment = isPayment.subtract(discountFee).add(postAmount);
    orderForErpVO.setOrderItemForErpVO(orderItemList);
    orderForErpVO.setPayment(isPayment);    //订单应付金额
    return orderForErpVO;
  }


  private OrderEcmohoVO setEcmohoErpWarehouse(OrderEcmohoVO orderEcmohoVO, Order order) {
    String warehouse = order.getWarehouseName();
    if (StringUtils.isNotBlank(warehouse)) {
      orderEcmohoVO.setWarehouse(warehouse);
    }
    return orderEcmohoVO;
  }

}

	


	


