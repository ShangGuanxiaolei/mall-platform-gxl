package com.xquark.service.app.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.AppVersionMapper;
import com.xquark.dal.model.AppVersion;
import com.xquark.dal.type.OSType;
import com.xquark.service.app.AppVersionService;
import com.xquark.service.common.BaseServiceImpl;

@Service("appVersionService")
public class AppVersionServiceImpl extends BaseServiceImpl implements AppVersionService {

  @Autowired
  private AppVersionMapper appVersionMapper;

  @Override
  public AppVersion findCurrentVersion(int clientVersion, OSType osType) {
    return appVersionMapper.findCurrentVersion(clientVersion, osType);
  }

  @Override
  public int insert(AppVersion e) {
    return 0;
  }

  @Override
  public int insertOrder(AppVersion appVersion) {
    return 0;
  }

  @Override
  public AppVersion load(String id) {
    return null;
  }
}
