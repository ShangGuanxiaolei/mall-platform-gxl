package com.xquark.service.app;

import com.xquark.dal.model.HomeItem;
import com.xquark.dal.model.Version;
import com.xquark.dal.status.HomeItemBelong;
import com.xquark.dal.vo.HomeItemVO;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by chh on 17-04-01. 首app更新service
 */
public interface VersionService {

  Version selectByPrimaryKey(String id);

  int insert(Version version);

  int modify(Version version);


  /**
   * @param id
   * @return
   */
  int delete(String id);


  /**
   * 服务端分页查询数据
   */
  List<Version> list(Pageable pager, String appType);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(String appType);

  /**
   * 根据传入的app类型与版本号，查询app更新类型
   */
  List<Version> selectByVersion(String appType, String version);


}
