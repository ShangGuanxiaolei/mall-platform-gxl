package com.xquark.service.app.impl;

import com.xquark.dal.mapper.HomeItemMapper;
import com.xquark.dal.mapper.VersionMapper;
import com.xquark.dal.model.HomeItem;
import com.xquark.dal.model.Version;
import com.xquark.dal.status.HomeItemBelong;
import com.xquark.dal.vo.HomeItemVO;
import com.xquark.service.app.VersionService;
import com.xquark.service.homeItem.HomeItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by chh on 17-4-1.
 */
@Service("versionServiceImpl")
public class VersionServiceImpl implements VersionService {

  @Autowired
  private VersionMapper versionMapper;

  @Override
  public Version selectByPrimaryKey(String id) {
    return versionMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(Version version) {
    return versionMapper.insert(version);
  }

  @Override
  public int modify(Version version) {
    return versionMapper.modify(version);
  }


  /**
   * @param id
   * @return
   */
  @Override
  public int delete(String id) {
    return versionMapper.delete(id);
  }


  /**
   * 服务端分页查询数据
   */
  @Override
  public List<Version> list(Pageable pager, String appType) {
    return versionMapper.list(pager, appType);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(String belong) {
    return versionMapper.selectCnt(belong);
  }

  /**
   * 根据传入的app类型与版本号，查询app更新类型
   */
  @Override
  public List<Version> selectByVersion(String appType, String version) {
    return versionMapper.selectByVersion(appType, version);
  }


}
