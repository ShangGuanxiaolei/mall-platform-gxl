package com.xquark.service.app;

import com.xquark.dal.model.AppVersion;
import com.xquark.dal.type.OSType;
import com.xquark.service.BaseEntityService;

public interface AppVersionService extends BaseEntityService<AppVersion> {

  /**
   * 获取app当前版本号
   */
  AppVersion findCurrentVersion(int clientVersion, OSType osType);

}
