package com.xquark.service.domain.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.DomainMapper;
import com.xquark.dal.model.Domain;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.domain.DomainService;

@Service("domainService")
public class DomainServiceImpl extends BaseServiceImpl implements DomainService {

  @Autowired
  private DomainMapper domainMapper;

  @Override
  public int insert(Domain domain) {
    return domainMapper.insert(domain);
  }

  @Override
  public int update(Domain domain) {
    return domainMapper.updateByPrimaryKeySelective(domain);
  }

  @Override
  public Domain load(String id) {
    return domainMapper.selectByPrimaryKey(id);
  }

  @Override
  public Domain loadByCode(String code) {
    return domainMapper.selectByCode(code);
  }

  @Override
  public Domain loadRootDomain() {
    return domainMapper.selectRootDomain();
  }

}
