package com.xquark.service.domain;

import com.xquark.dal.model.Domain;
import com.xquark.service.BaseService;

public interface DomainService extends BaseService {

  int insert(Domain domain);

  int update(Domain domain);

  Domain load(String id);

  Domain loadByCode(String code);

  Domain loadRootDomain();

}
