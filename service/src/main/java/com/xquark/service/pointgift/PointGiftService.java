package com.xquark.service.pointgift;

import com.xquark.dal.model.PointGift;
import com.xquark.dal.model.PointGiftRecord;
import com.xquark.dal.model.UserDTO;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pointgift.dto.PointModifyTO;
import com.xquark.service.pointgift.dto.PointSendType;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface PointGiftService {
    boolean deleteByPrimaryKey(String id);

    boolean insert(PointGift record);

    boolean insertSelective(PointGift record);

    PointGift selectByPrimaryKey(String id);

    boolean updateByPrimaryKeySelective(PointGift record);

    boolean updateByPrimaryKey(PointGift record);

    /**
     * 插入发红包记录并且减少自己的德分
     * @param pointGift
     * @return
     */
    boolean insertPointGiftAndDeduction(PointGift pointGift);

    /**
     * 修改总德分并且添加明细记录
     * @param pointModifyTO
     * @return
     */
    boolean modifyPointAndInsertRecord(PointModifyTO pointModifyTO);


    /**
     * 获取已经过期的红包
     * @return
     */
    List<PointGift> listExpiredPacket();


    boolean checkPointGift(PointGift pointGift, PointGiftRecord pointGiftRecord, Map<String, GlobalErrorCode> mapParam);

    boolean savePointGiftRecord(PointGift pointGift, PointGiftRecord pointGiftRecord);

    PointGift selectBypointGiftNo(String pointGiftNo);
    /**
     * 修改红包状态
     * @param pointGiftNos
     * @param pointGifts
     */
    void updatePointGiftToExpired(List<PointGift> pointGifts);

    /**
     * 通过红包编号找到该信息
     * @param pointGiftNo
     * @return
     */
    PointGift getByPointGiftNo(String pointGiftNo);

    boolean modifyWithDetail(Long cpId, BigDecimal amount, String bizId, PointSendType bizType);

    boolean modifyWithDetailAndGift(Long cpId, BigDecimal amount, String bizId,
                                    PointSendType bizType, Integer peopleNumber, String requireIdentity, Integer packetType, String leaveWord, Integer divisionType, BigDecimal sourceAmount);

    /**
     * 优先消费德分红包, 如果不够再消费德分
     */
    boolean consume(Long cpId, String bizId, BigDecimal amount);

    void insertPointGiftDetailForReceive(PointGift pointGift, PointGiftRecord pointGiftRecord);

    /**
     * 新人
     * @param buyId
     * @return
     */
     boolean isNew(Long buyId);

    UserDTO selectByUserInfoPointGiftNo(String pointGiftNo);

    /**
     * 检查用户是否在红包白名单
     */
    boolean isInWhiteList(Long cpId);

    boolean isPacketEnable();

    /**
     * 发红包
     * @param pointGift
     */
    void sendPacket(PointGift pointGift);

    /**
     * 红包雨消费德分
     * @param id
     * @param consumePoint 消费德分数
     */
    Boolean prePacketRainCheck(String id, Long cpId, Long consumePoint);

    Boolean consumePacketPoint(Long cpId, Long consumePoint);


    /**
     * 发送红包优先消费德分红包, 如果不够再消费德分
     */
    boolean consumePacketPointFirst(Long cpId, BigDecimal amount, String bizId,
                                     Integer peopleNumber, String requireIdentity, Integer packetType, String leaveWord, Integer divisionType);
}
