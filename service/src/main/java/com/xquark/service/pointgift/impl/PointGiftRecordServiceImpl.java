package com.xquark.service.pointgift.impl;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.model.PointTotal;
import com.hds.xquark.service.point.PointServiceApi;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.mapper.PointGiftRecordMapper;
import com.xquark.dal.model.*;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pointgift.PointGiftDetailService;
import com.xquark.service.pointgift.PointGiftRecordService;
import com.xquark.service.pointgift.PointGiftService;
import com.xquark.service.pointgift.dto.PointPacketDTO;
import com.xquark.service.pointgift.dto.PointSendType;
import com.xquark.service.pointgift.dto.UserPointDetailDTO;
import com.xquark.service.userAgent.UserSigninLogService;
import com.xquark.service.weakLink.WeakLinkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
@Service
public class PointGiftRecordServiceImpl implements PointGiftRecordService {

    private static final Logger log = LoggerFactory.getLogger(PointGiftRecordServiceImpl.class);
    private final PointGiftRecordMapper pointGiftRecordMapper;
    private final PointGiftDetailService pointGiftDetailService;
    private final PointServiceApi pointService;
    private final UserSigninLogService userSigninLogService;
    private PointGiftService pointGiftService;
    private final WeakLinkService weakLinkService;


    @Autowired
    public void setPointGiftService(PointGiftService pointGiftService) {
        this.pointGiftService = pointGiftService;
    }

    @Autowired
    public PointGiftRecordServiceImpl(PointGiftRecordMapper pointGiftRecordMapper, PointGiftDetailService pointGiftDetailService, PointContextInitialize pointContextInitialize, UserSigninLogService userSigninLogService, WeakLinkService weakLinkService) {
        this.pointGiftRecordMapper = pointGiftRecordMapper;
        this.pointGiftDetailService = pointGiftDetailService;
        this.pointService = pointContextInitialize.getPointServiceApi();
        this.userSigninLogService = userSigninLogService;
        this.weakLinkService = weakLinkService;
    }

    @Override
    public int deleteByPrimaryKey(String id) {
        return pointGiftRecordMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(PointGiftRecord record) {
        return pointGiftRecordMapper.insert(record);
    }

    @Override
    public int insertSelective(PointGiftRecord record) {
        return pointGiftRecordMapper.insertSelective(record);
    }

    @Override
    public PointGiftRecord selectByPrimaryKey(String id) {
        return pointGiftRecordMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(PointGiftRecord record) {
        return pointGiftRecordMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(PointGiftRecord record) {
        return pointGiftRecordMapper.updateByPrimaryKey(record);
    }

    @Override
    public PointGiftRecord selectByUserGiftNo(PointGiftRecord pointGiftRecord) {
        return pointGiftRecordMapper.selectByUserGiftNo(pointGiftRecord);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public BigDecimal operPointGiftRecord(PointGift pointGift, PointGiftRecord pointGiftRecord) {
        //从redis中取到红包金额
        RedisUtils<BigDecimal> pointByRedis = RedisUtilFactory.getInstance(BigDecimal.class);
        BigDecimal point = null;
        String pointGiftNo = null;
        try {

            pointGiftNo = pointGiftRecord.getPointGiftNo();
            Long lCount = pointByRedis.lSize(pointGiftNo);

            if (0 == lCount) {
                log.error("redis队列中不存在多余红包-->红包编号{}", pointGiftNo);
                return BigDecimal.ZERO;
            } else if (1 == lCount) {
                //3-已领完
                pointGift.setStatus(3);
                pointGiftService.updateByPrimaryKey(pointGift);
            }

            //获取红包积分
            point = pointByRedis.rPop(pointGiftNo);
            if (point == null) {
                throw new BizException(GlobalErrorCode.INTERNAL_ERROR);
            }
            pointGiftRecord.setAmount(point);

            //获取成功后登记红包记录表
            pointGiftRecordMapper.insert(pointGiftRecord);

            //pointTotal更新总得分
            PointTotal pointTotalRec = pointService.initTotal(Long.valueOf(pointGiftRecord.getReceiveCpid()));
            pointTotalRec.setUsablePointPacket(pointTotalRec.getUsablePointPacket().add(point));
            pointService.updateByCpId(pointTotalRec);

            //登记红包明细
            pointGiftService.insertPointGiftDetailForReceive(pointGift, pointGiftRecord);

            //绑定弱关系1-已经绑定
            WeakLink weakLink = new WeakLink(Long.valueOf(pointGiftRecord.getReceiveCpid()),
                    Long.valueOf(pointGiftRecord.getGrantCpid()),
                    1);
            weakLinkService.bound(weakLink);

            return point;
        } catch (RuntimeException runException) {
            //报错恢复
            if (point != null) {
                pointByRedis.lPush(pointGiftNo, point);
            }
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR);
        }
    }

    @Override
    public BigDecimal periodGiftPoint(String userId, String receiveCpid) {

        //从redis中取上次查询时间
        RedisUtils<Long> lastSignTimeKey = RedisUtilFactory.getInstance(Long.class);
        Long lastTime = lastSignTimeKey.get("lastSignTimeKey");
        Long curTime = System.currentTimeMillis() / 1000;

        if (lastTime == null) {
            List<Long> logList = userSigninLogService.selectUserSignInLog(userId);
            if (CollectionUtils.isEmpty(logList)) {
                return BigDecimal.ZERO;
            }
            lastTime = logList.get(0);
        }

        //更新key,并且更新过期时间1天
        lastSignTimeKey.set("lastSignTimeKey", curTime);
        lastSignTimeKey.expire("lastSignTimeKey", 1, TimeUnit.DAYS);

        return pointGiftRecordMapper.periodGiftPoint(lastTime, curTime, receiveCpid);
    }

    @Override
    public PointPacketDTO getPointPacketDetail(Long cpId, String type, PageRequest pageRequest) {
        final List<PointGiftDetailDTO> pointGiftDetailDTOS = pointGiftDetailService.listPointPacketDetailByCpid(cpId, pageRequest, type);
        final ArrayList<UserPointDetailDTO> userPointDetailDTOS = Lists.newArrayList();
        for (PointGiftDetailDTO pointGiftDetailDTO : pointGiftDetailDTOS) {
            userPointDetailDTOS.add(UserPointDetailDTO.valueOf(pointGiftDetailDTO));
        }
        final BigDecimal usablePointPacket = pointService.initTotal(cpId).getUsablePointPacket();
        return new PointPacketDTO(userPointDetailDTOS, usablePointPacket);
    }


    @Override
    public List<UserPointDTO> getPointPacketDetailByPointGiftNo(boolean isBilling, String pointGiftNo, Long cpid,
                                                                Long fromCpid) {
        if (isBilling) {
            return pointGiftRecordMapper.getPointPacketDetailByGiftNo(pointGiftNo);
        } else {
            return getPointReceviceDetailByPointGiftNo(pointGiftNo, cpid, fromCpid) == null ?
                    Collections.<UserPointDTO>emptyList() :
                    Collections.singletonList(getPointReceviceDetailByPointGiftNo(pointGiftNo, cpid, fromCpid));
        }
    }

    @Override
    public List<UserPointDTO> getPointPacketDetailByPointGiftNo(String pointGiftNo) {
        return pointGiftRecordMapper.getPointPacketDetailByGiftNo(pointGiftNo);
    }

    @Override
    public UserPointDTO getPointReceviceDetailByPointGiftNo(String pointGitNo, Long cpid, Long fromCpid) {
        return pointGiftRecordMapper.getPointReceviceDetailByPointGiftNo(pointGitNo, cpid, fromCpid);
    }

    @Override
    public boolean getBillingType(String bizType, String pointGiftNo, Long cpid) {
        boolean billingType;
        if (bizType != null) {
            billingType = PointSendType.valueOf(bizType) == PointSendType.SEND;
            return billingType;
        }

        final PointGift pointGift = pointGiftService.getByPointGiftNo(pointGiftNo);
        if (pointGift == null) {
            return false;
        }
        billingType = Objects.equal(cpid, pointGift.getCpid());
        return billingType;
    }

}
