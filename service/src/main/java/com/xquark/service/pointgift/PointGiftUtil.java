package com.xquark.service.pointgift;

import com.hds.xquark.dal.model.PointTotal;
import java.math.BigDecimal;
import org.apache.commons.lang3.tuple.Pair;

/**
 * @author wangxinhua
 * @since 19-2-1
 */
public class PointGiftUtil {

  /**
   * 计算出使用的德分中普通德分跟红包的明细 {@code Pair.getLeft()} 普通德分使用数 {@code Pair.getRight()} 红包德分使用数
   *
   * @param pointTotal 用户的当前德分
   * @param using 用户当前选择使用的德分
   * @return 德分明细元组
   */
  public static Pair<BigDecimal, BigDecimal> pointDetail(PointTotal pointTotal, BigDecimal using) {
    // 当前红包可用总额
    BigDecimal usablePointPacket = pointTotal.getUsablePointPacket();
    // 减去德分红包后的剩余值
    BigDecimal afterPacket = usablePointPacket.subtract(using);
    if (afterPacket.signum() < 0) {
      // 红包不够扣
      return Pair.of(afterPacket.abs(), usablePointPacket);
    }
    return Pair.of(BigDecimal.ZERO, using);
  }
}
