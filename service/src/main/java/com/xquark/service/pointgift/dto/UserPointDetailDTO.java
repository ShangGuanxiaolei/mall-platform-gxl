package com.xquark.service.pointgift.dto;

import com.xquark.dal.model.PointGiftDetailDTO;

import java.math.BigDecimal;

/**
 * @author Jack Zhu
 * @since 2018/12/13
 */
public class UserPointDetailDTO {
    /**
     * 德分类型
     */
    private PointSendType pointType;
    /**
     * 创建时间
     */
    private Long createTime;
    /**
     * 红包编号
     */
    private String packetNo;
    /**
     * 数量
     */
    private BigDecimal amount;

    /**
     * 定向红包还是非定向红包
     */
    private String packetType;


    public static UserPointDetailDTO valueOf(PointGiftDetailDTO pointGiftDetailDTO) {
        final UserPointDetailDTO userPointDetailDTO = new UserPointDetailDTO();
        userPointDetailDTO.setAmount(pointGiftDetailDTO.getAmount());
        userPointDetailDTO.setCreateTime(pointGiftDetailDTO.getCreatedAt().getTime());
        userPointDetailDTO.setPacketNo(pointGiftDetailDTO.getBizId());
        userPointDetailDTO.setPointType(PointSendType.typeOf(pointGiftDetailDTO.getBizType()));
        userPointDetailDTO.setPacketType(pointGiftDetailDTO.getPacketType());
        return userPointDetailDTO;
    }

    public PointSendType getPointType() {
        return pointType;
    }

    public String getPacketType() {
        return packetType;
    }

    public void setPacketType(String packetType) {
        this.packetType = packetType;
    }

    public void setPointType(PointSendType pointType) {
        this.pointType = pointType;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getPacketNo() {
        return packetNo;
    }

    public void setPacketNo(String packetNo) {
        this.packetNo = packetNo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "UserPointDetailDTO{" +
                "pointType=" + pointType +
                ", createTime=" + createTime +
                ", packetNo='" + packetNo + '\'' +
                ", amount=" + amount +
                '}';
    }
}
