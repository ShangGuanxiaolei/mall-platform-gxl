package com.xquark.service.pointgift;

import com.xquark.dal.model.PointGiftDetail;
import com.xquark.dal.model.PointGiftDetailDTO;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * @author wangxinhua.
 * @date 2018/12/12
 */
public interface PointGiftDetailService {

  int deleteByPrimaryKey(String id);

  boolean insert(PointGiftDetail record);

  PointGiftDetail selectByPrimaryKey(String id);

  boolean updateByPrimaryKeySelective(PointGiftDetail record);

  boolean updateByPrimaryKey(PointGiftDetail record);

  /**
   * 查找用户红包得分支出收入详细信息
   * @param cpId
   * @param pageRequest
   * @param type
   * @return
   */
  List<PointGiftDetailDTO> listPointPacketDetailByCpid(Long cpId, PageRequest pageRequest, String type);

}
