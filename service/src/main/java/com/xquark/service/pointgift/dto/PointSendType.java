package com.xquark.service.pointgift.dto;

import com.google.common.base.Objects;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public enum PointSendType {
    /**
     * 红包发放
     */
    SEND("红包发放"),
    /**
     * 红包领取
     */
    RECEIVE("红包领取"),
    /**
     * 红包退回
     */
    REFUND("红包退款"),
    /**
     * 购物消费
     */
    CONSUMPTION("购物消费"),

    /**
     * 购物消费
     */
    ORDER_REFUND("订单取消"),

    /**
     * 抽奖中奖
     */
    WINNING_POINT("活动德分"),

    /**
     * 红包雨发放
     */
    PACKET_RAIN("活动德分"),

    /**
     * 红包德分发放
     */
    SEND_PACKET_POINT("德分红包发放");

    private static final List<PointSendType> POSITIVE_TYPE_LIST = Arrays.asList(RECEIVE, REFUND,ORDER_REFUND);
    private static final List<PointSendType> NEGATIVE_TYPE_LIST = Arrays.asList(SEND,CONSUMPTION,SEND_PACKET_POINT);

    private String message;

    PointSendType(String message) {
        this.message = message;
    }

    public static PointSendType typeOf(String type) {
        for (PointSendType value : values()) {
            if (Objects.equal(value.name(), type)) {
                return value;
            }
        }
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR);
    }

    public static int computeType(BigDecimal value, PointSendType type) {
        value = value.abs();
        for (PointSendType pointSendType : NEGATIVE_TYPE_LIST) {
            if (Objects.equal(pointSendType, type)) {
                value = value.negate();
                break;
            }
        }
        return value.intValue();
    }
    public String getMessage() {
        return message;
    }}
