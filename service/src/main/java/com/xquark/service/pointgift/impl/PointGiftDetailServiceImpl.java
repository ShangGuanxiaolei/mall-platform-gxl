package com.xquark.service.pointgift.impl;

import com.xquark.dal.mapper.PointGiftDetailMapper;
import com.xquark.dal.mapper.PointGiftMapper;
import com.xquark.dal.model.PointGiftDetail;
import com.xquark.dal.model.PointGiftDetailDTO;
import com.xquark.helper.Transformer;
import com.xquark.service.pointgift.PointGiftDetailService;
import com.xquark.service.pointgift.dto.PointSendType;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author wangxinhua.
 * @date 2018/12/12
 */
@Service
public class PointGiftDetailServiceImpl implements PointGiftDetailService {

  private final PointGiftDetailMapper pointGiftDetailMapper;
  private final PointGiftMapper pointGiftMapper;
  private static final List<String> RED_PACKET_LIST = Arrays.asList(PointSendType.RECEIVE.name(), PointSendType.CONSUMPTION
      .name(), PointSendType.ORDER_REFUND.name(), PointSendType.PACKET_RAIN.name(), PointSendType.WINNING_POINT.name(), PointSendType.SEND_PACKET_POINT.name());
  private static final String RECEIVE_CONSUMPTION = "RECEIVE_CONSUMPTION";

  @Autowired
  public PointGiftDetailServiceImpl(
          PointGiftDetailMapper pointGiftDetailMapper, PointGiftMapper pointGiftMapper) {
    this.pointGiftDetailMapper = pointGiftDetailMapper;
    this.pointGiftMapper = pointGiftMapper;
  }

  @Override
  public int deleteByPrimaryKey(String id) {
    return pointGiftDetailMapper.deleteByPrimaryKey(id);
  }

  @Override
  public boolean insert(PointGiftDetail record) {
    return pointGiftDetailMapper.insert(record) > 0;
  }

  @Override
  public PointGiftDetail selectByPrimaryKey(String id) {
    return pointGiftDetailMapper.selectByPrimaryKey(id);
  }

  @Override
  public boolean updateByPrimaryKeySelective(PointGiftDetail record) {
    return pointGiftDetailMapper.updateByPrimaryKeySelective(record) > 0;
  }

  @Override
  public boolean updateByPrimaryKey(PointGiftDetail record) {
    return pointGiftDetailMapper.updateByPrimaryKey(record) > 0;
  }

  @Override
  public List<PointGiftDetailDTO> listPointPacketDetailByCpid(Long cpId, PageRequest pageRequest, String type) {
    List<PointGiftDetail> pointGiftDetails ;
    if (RECEIVE_CONSUMPTION.equals(type)) {
      pointGiftDetails = pointGiftDetailMapper.listPointPacketDetailByCpid(cpId, RED_PACKET_LIST, pageRequest);
    } else {
      pointGiftDetails = pointGiftDetailMapper.listPointRandomPacketSend(cpId, pageRequest);
    }
    if (CollectionUtils.isEmpty(pointGiftDetails)) {
      return Collections.emptyList();
    }
    return  Transformer.fromIterable(pointGiftDetails, PointGiftDetailDTO.class);
  }

}
