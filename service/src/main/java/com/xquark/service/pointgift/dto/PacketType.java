package com.xquark.service.pointgift.dto;

import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;

/**
 * @author Jack Zhu
 * @since 2018/12/13
 */
public enum PacketType {

    /**
     * 定向红包
     */
    DIRECTIONAL(1),
    /**
     * 随机红包
     */
    RANDOM(2);
    private int code;

    public static PacketType valueOf(int code) {
        for (PacketType value : values()) {
            if (value.code == code) {
                return value;
            }
        }
        throw new BizException(GlobalErrorCode.PACKET_TYPE_ERROR);
    }

    PacketType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }}
