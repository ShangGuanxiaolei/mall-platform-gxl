package com.xquark.service.pointgift.dto;

import java.math.BigDecimal;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public class PointPacketDetailDTO {
    /**
     * 红包编号
     */
    private String pointGiftNo;

    /**
     * 领取金额
     */
    private BigDecimal amount;

    /**
     * 红包发放类型
     */
    private PointSendType pointSendType;
    /**
     * 创建时间
     */
    private Long createTime;


    public String getPointGiftNo() {
        return pointGiftNo;
    }

    public void setPointGiftNo(String pointGiftNo) {
        this.pointGiftNo = pointGiftNo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public PointSendType getPointSendType() {
        return pointSendType;
    }

    public void setPointSendType(PointSendType pointSendType) {
        this.pointSendType = pointSendType;
    }

    @Override
    public String toString() {
        return "PointPacketDetailDTO{" +
                "pointGiftNo='" + pointGiftNo + '\'' +
                ", amount=" + amount +
                ", pointSendType=" + pointSendType +
                ", createTime=" + createTime +
                '}';
    }
}
