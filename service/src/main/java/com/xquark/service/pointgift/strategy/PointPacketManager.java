package com.xquark.service.pointgift.strategy;

import com.xquark.dal.model.PointGift;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pointgift.dto.DivisionType;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public  class PointPacketManager {
    private PointPacketManager() {
    }
    public static void sendPacket(PointGift pointGift) {
        final AbstractPointPacketStrategy pointPacketStrategy;
        final DivisionType type = DivisionType.valueOf(pointGift.getDivisionType());
        switch (type) {
            case NOTEQUAL_AMOUNT:
                pointPacketStrategy = new NotEqualAmountWeChatStrategy();
                break;
            case EQUAL_AMOUNT:
                pointPacketStrategy = new EqualAmountStrategy();
                break;
            default:
                throw new BizException(GlobalErrorCode.ERROR_PARAM);
        }
        //发德分
        pointPacketStrategy.distributionPointPacket(pointGift.getPointGiftNo(), pointGift.getAggregateAmount(), pointGift.getPeopleAmount());
    }
}
