package com.xquark.service.pointgift.dto;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public enum DivisionType {

    /**
     * 非等额红包
     */
    EQUAL_AMOUNT,
    /**
     * 等额红包
     */
    NOTEQUAL_AMOUNT,
    /**
     * 错误类型
     */
    ERROR_TYPE
    ;

    public static DivisionType valueOf(int i) {
        for (DivisionType value : DivisionType.values()) {
            if (value.ordinal() == i) {
                return value;
            }
        }
        return DivisionType.ERROR_TYPE;
    }
}
