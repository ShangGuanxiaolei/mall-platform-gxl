package com.xquark.service.pointgift.strategy;

import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;

import java.math.BigDecimal;
import java.util.Random;

/**
 * 微信红包的随机算法
 *
 * @author Jack Zhu
 * @since 2018/12/25
 */
public class NotEqualAmountWeChatStrategy extends AbstractPointPacketStrategy {

    private Random r = new Random();

    @Override
    protected void preCheck(BigDecimal aggregateAmount, Integer peopleAmount) {
        if (aggregateAmount.intValue() < peopleAmount) {
            throw new BizException(GlobalErrorCode.NOT_DIVIDE_RANDOM_POINT);
        }
    }

    @Override
    protected BigDecimal[] obtainAmount(BigDecimal aggregateAmount, Integer peopleAmount) {
        LeftMoneyPackage leftMoneyPackage = new LeftMoneyPackage(peopleAmount, aggregateAmount.intValue());
        BigDecimal[] amounts = new BigDecimal[leftMoneyPackage.remainSize];
        while (leftMoneyPackage.remainSize != 0) {
            final double randomMoney = getRandomMoney(leftMoneyPackage);
            amounts[leftMoneyPackage.remainSize] = BigDecimal.valueOf(((int) randomMoney));
        }
        return amounts;
    }

    /**
     * 微信红包的具体算法
     *
     * @param leftMoneyPackage
     * @return
     */
    private double getRandomMoney(LeftMoneyPackage leftMoneyPackage) {
        // remainSize 剩余的红包数量
        // remainMoney 剩余的钱
        if (leftMoneyPackage.remainSize == 1) {
            leftMoneyPackage.remainSize--;
            return (double) Math.round(leftMoneyPackage.remainMoney * 1) / 1;
        }
        double min = 1;
        double max = leftMoneyPackage.remainMoney / leftMoneyPackage.remainSize * 2;
        double money = r.nextDouble() * max;
        money = money <= min ? 1 : money;
        money = Math.floor(money * 1) / 1;
        leftMoneyPackage.remainSize--;
        leftMoneyPackage.remainMoney -= money;
        return money;
    }


    private class LeftMoneyPackage {
        private int remainSize;
        private double remainMoney;

        LeftMoneyPackage(int remainSize, double remainMoney) {
            this.remainSize = remainSize;
            this.remainMoney = remainMoney;
        }

        @Override
        public String toString() {
            return "LeftMoneyPackage{" +
                    "remainSize=" + remainSize +
                    ", remainMoney=" + remainMoney +
                    '}';
        }
    }

    public static void main(String[] args) {
        final NotEqualAmountWeChatStrategy notEqualAmountWeChatStrategy = new NotEqualAmountWeChatStrategy();

        for (int i = 0; i < 30; i++) {
            final BigDecimal[] bigDecimals = notEqualAmountWeChatStrategy.obtainAmount(BigDecimal.valueOf(1000), 10);
            int sum = 0;
            for (BigDecimal bigDecimal : bigDecimals) {
                sum += bigDecimal.intValue();
                System.out.print(bigDecimal.intValue()+" ");
            }
            if (sum != 1000) {
                throw new RuntimeException();
            }
            System.out.println();
        }

    }

}
