package com.xquark.service.pointgift.dto;

import com.hds.xquark.dal.type.PlatformType;
import com.hds.xquark.dal.type.Trancd;
import com.hds.xquark.service.point.type.FunctionCodeType;
import com.xquark.dal.model.PointGiftDetail;
import org.apache.commons.lang3.tuple.Pair;

/**
 * @author Jack Zhu
 * @since 2018/12/13
 */
public class PointModifyTO {
    private PointGiftDetail pointGiftDetail;
    private Pair<FunctionCodeType, Trancd> functionCodeTypeTrancdPair;
    private PlatformType platformType;


    public PointModifyTO(PointGiftDetail pointGiftDetail, Pair<FunctionCodeType, Trancd> functionCodeTypeTrancdPair, PlatformType platformType) {
        this.pointGiftDetail = pointGiftDetail;
        this.functionCodeTypeTrancdPair = functionCodeTypeTrancdPair;
        this.platformType = platformType;
    }

    public Pair<FunctionCodeType, Trancd> getFunctionCodeTypeTrancdPair() {
        return functionCodeTypeTrancdPair;
    }

    public void setFunctionCodeTypeTrancdPair(Pair<FunctionCodeType, Trancd> functionCodeTypeTrancdPair) {
        this.functionCodeTypeTrancdPair = functionCodeTypeTrancdPair;
    }

    public PlatformType getPlatformType() {
        return platformType;
    }

    public void setPlatformType(PlatformType platformType) {
        this.platformType = platformType;
    }

    public PointGiftDetail getPointGiftDetail() {
        return pointGiftDetail;
    }

    public void setPointGiftDetail(PointGiftDetail pointGiftDetail) {
        this.pointGiftDetail = pointGiftDetail;
    }
}
