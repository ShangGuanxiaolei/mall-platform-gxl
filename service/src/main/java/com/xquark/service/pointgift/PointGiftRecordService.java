package com.xquark.service.pointgift;

import com.xquark.dal.model.PointGift;
import com.xquark.dal.model.PointGiftRecord;
import com.xquark.dal.model.UserPointDTO;
import com.xquark.service.pointgift.dto.PointPacketDTO;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public interface PointGiftRecordService {
    int deleteByPrimaryKey(String id);

    int insert(PointGiftRecord record);

    int insertSelective(PointGiftRecord record);

    PointGiftRecord selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PointGiftRecord record);

    int updateByPrimaryKey(PointGiftRecord record);

    /**
     *  查看出账入账详情
     * @param cpId
     * @param type
     * @param pageRequest
     * @return
     */
     PointPacketDTO getPointPacketDetail(Long cpId, String type, PageRequest pageRequest);



    PointGiftRecord selectByUserGiftNo(PointGiftRecord pointGiftRecord);

    BigDecimal operPointGiftRecord(PointGift pointGift, PointGiftRecord pointGiftRecord);

    BigDecimal periodGiftPoint(String userId, String receiveCpid);
    /**
     * 通过红包编号找到红包详情
     * @param pointGiftNo
     * @return
     */
    List<UserPointDTO> getPointPacketDetailByPointGiftNo(boolean isBilling, String pointGiftNo, Long cpid, Long fromCpid);

    UserPointDTO getPointReceviceDetailByPointGiftNo(String pointGiftNo, Long cpid, Long fromCpid);

    boolean getBillingType(String bizType,String pointGiftNo,Long cpid);

    /**
     * 查看拿红包详情
     *
     * @param pointGiftNo
     * @return
     */
     List<UserPointDTO> getPointPacketDetailByPointGiftNo(String pointGiftNo);
}