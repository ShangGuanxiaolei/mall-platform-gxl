package com.xquark.service.pointgift.strategy;

import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;

import java.math.BigDecimal;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public abstract class AbstractPointPacketStrategy {

    /**
     * 分发红包
     *
     * @param pointGiftNo 红包编号
     * @param aggregateAmount  总金额
     */
    void distributionPointPacket(String pointGiftNo, BigDecimal aggregateAmount ,Integer peopleAmount) {
        preCheck(aggregateAmount, peopleAmount);
        BigDecimal[] amounts = obtainAmount(aggregateAmount,peopleAmount);
        final RedisUtils<BigDecimal> redisUtils = RedisUtilFactory.getInstance(BigDecimal.class);
        if (amounts != null) {
                redisUtils.leftPushAll(pointGiftNo, amounts);
        }
    }

    /**
     * 检查参数是否正常
     * @param aggregateAmount
     * @param peopleAmount
     */
    protected abstract void preCheck(BigDecimal aggregateAmount, Integer peopleAmount);

    /**
     * 先算出来分发的德分
     *
     * @param aggregateAmount  总金额
     * @param peopleAmount 人数
     * @return 每个人分多少钱
     */
    protected abstract BigDecimal[] obtainAmount(BigDecimal aggregateAmount, Integer peopleAmount);

}
