package com.xquark.service.pointgift.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public class PointPacketDTO {
    private List<UserPointDetailDTO> packetDetailDTOS;
    private BigDecimal availableAmount;

    public PointPacketDTO() {
    }

    public PointPacketDTO(List<UserPointDetailDTO> packetDetailDTOS, BigDecimal availableAmount) {
        this.packetDetailDTOS = packetDetailDTOS;
        this.availableAmount = availableAmount;
    }

    public List<UserPointDetailDTO> getPacketDetailDTOS() {
        return packetDetailDTOS;
    }

    public void setPacketDetailDTOS(List<UserPointDetailDTO> packetDetailDTOS) {
        this.packetDetailDTOS = packetDetailDTOS;
    }

    public BigDecimal getAvailableAmount() {
        return availableAmount;
    }

    public void setAvailableAmount(BigDecimal availableAmount) {
        this.availableAmount = availableAmount;
    }

    @Override
    public String toString() {
        return "PointPacketDTO{" +
                "packetDetailDTOS=" + packetDetailDTOS +
                ", availableAmount=" + availableAmount +
                '}';
    }
}
