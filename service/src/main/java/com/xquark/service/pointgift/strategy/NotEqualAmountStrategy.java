package com.xquark.service.pointgift.strategy;

import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public class NotEqualAmountStrategy extends AbstractPointPacketStrategy {

    private  Random random = new Random();

    @Override
    protected void preCheck(BigDecimal aggregateAmount, Integer peopleAmount) {
        if (aggregateAmount.intValue() < peopleAmount) {
            throw new BizException(GlobalErrorCode.NOT_DIVIDE_RANDOM_POINT);
        }
    }

    @Override
    protected BigDecimal[] obtainAmount(BigDecimal aggregateAmount, Integer peopleAmount) {
        preCheck(aggregateAmount, peopleAmount);
        BigDecimal[] amounts = new BigDecimal[peopleAmount];
        int value = aggregateAmount.intValue();
        // 随机的思路为保留最少每一个人都能分到1德分
        for (Integer i = 0, k = peopleAmount; i < peopleAmount; i++,k--) {
            if (k == 1) {
                amounts[i] = BigDecimal.valueOf(value);
                break;
            }
            //value - (k-1) 剩余德分-保留的德分
            final int distributionValue = random.nextInt(value - (k-1))+1;
            value -= distributionValue;
            amounts[i] = BigDecimal.valueOf(distributionValue);
        }
        //打散顺序
        final List<BigDecimal> decimalList = Arrays.asList(amounts);
        Collections.shuffle(decimalList);
        amounts = decimalList.toArray(new BigDecimal[]{});
        return amounts;
    }
}
