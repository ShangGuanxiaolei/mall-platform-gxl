package com.xquark.service.pointgift.impl;

import com.google.common.collect.Lists;
import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.model.PointTotal;
import com.hds.xquark.dal.type.PlatformType;
import com.hds.xquark.dal.type.Trancd;
import com.hds.xquark.service.point.PointServiceApi;
import com.hds.xquark.service.point.type.FunctionCodeType;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.RainLotteryConstrants;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.mapper.PacketWhitelistMapper;
import com.xquark.dal.mapper.PointGiftMapper;
import com.xquark.dal.mapper.PointGiftRecordMapper;
import com.xquark.dal.model.PointGift;
import com.xquark.dal.model.PointGiftDetail;
import com.xquark.dal.model.PointGiftRecord;
import com.xquark.dal.model.UserDTO;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pointgift.PointGiftDetailService;
import com.xquark.service.pointgift.PointGiftService;
import com.xquark.service.pointgift.PointGiftUtil;
import com.xquark.service.pointgift.dto.PacketType;
import com.xquark.service.pointgift.dto.PointModifyTO;
import com.xquark.service.pointgift.dto.PointSendType;
import com.xquark.service.pointgift.strategy.PointPacketManager;
import com.xquark.utils.DateUtils;
import com.xquark.utils.SnowflakeIdWorker;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
@Service
public class PointGiftServiceImpl implements PointGiftService {

    private final PointGiftMapper pointGiftMapper;
    private final SnowflakeIdWorker snowflakeIdWorker;
    private final PointServiceApi pointService;
    private final PointGiftDetailService pointGiftDetailService;
    private final PointGiftRecordMapper pointGiftRecordMapper;
    private final OrderMapper orderMapper;
    private final PacketWhitelistMapper packetWhitelistMapper;
    private final CustomerProfileService customerProfileService;
    private Logger log = LoggerFactory.getLogger(getClass());

    /**
     * 红包状态
     * 1 - 可领取
     * 2 - 已过期
     * 3 - 已领完
     */
    private final static Integer RECEIVE = 1;
    private final static Integer OVER_DUE = 2;

    @Autowired
    public PointGiftServiceImpl(PointGiftMapper pointGiftMapper,
                                PointGiftRecordMapper pointGiftRecordMapper,
                                SnowflakeIdWorker snowflakeIdWorker,
                                PointGiftDetailService pointGiftDetailService,
                                PointContextInitialize pointContextInitialize, OrderMapper orderMapper,
                                PacketWhitelistMapper packetWhitelistMapper, CustomerProfileService customerProfileService) {
        this.pointGiftMapper = pointGiftMapper;
        this.snowflakeIdWorker = snowflakeIdWorker;
        this.pointService = pointContextInitialize.getPointServiceApi();
        this.pointGiftDetailService = pointGiftDetailService;
        this.pointGiftRecordMapper = pointGiftRecordMapper;
        this.orderMapper = orderMapper;
        this.packetWhitelistMapper = packetWhitelistMapper;
        this.customerProfileService = customerProfileService;
    }

    @Override
    public boolean deleteByPrimaryKey(String id) {
        return pointGiftMapper.deleteByPrimaryKey(id) > 0;
    }

    @Override
    public boolean insert(PointGift record) {
        fullPointGift(record);
        return pointGiftMapper.insert(record) > 0;
    }


    @Override
    public boolean insertSelective(PointGift record) {
        fullPointGift(record);
        return pointGiftMapper.insertSelective(record) > 0;
    }

    @Override
    public PointGift selectByPrimaryKey(String id) {
        return pointGiftMapper.selectByPrimaryKey(id);
    }

    @Override
    public boolean updateByPrimaryKeySelective(PointGift record) {
        return pointGiftMapper.updateByPrimaryKeySelective(record) > 0;
    }

    @Override
    public boolean updateByPrimaryKey(PointGift record) {
        return pointGiftMapper.updateByPrimaryKey(record) > 0;
    }

    /**
     * 往表中插入一条德分红包发放记录并且扣掉当前用户的得分
     *
     * @param pointGift
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean insertPointGiftAndDeduction(PointGift pointGift) {
        pointGift.setPacketType(PacketType.RANDOM.getCode());
        boolean success = insertSelective(pointGift);
        if (success) {
            pointService.modify(pointGift.getCpid(),
                    pointGift.getPointGiftNo(),
                    FunctionCodeType.getPacketSend(),
                    PlatformType.E, pointGift.getAggregateAmount().abs());
        }
        return success;
    }

    @Override
    public boolean modifyPointAndInsertRecord(PointModifyTO pointModifyTO) {
        final PointGiftDetail pointGiftDetail = pointModifyTO.getPointGiftDetail();
        final boolean success = pointGiftDetailService.insert(pointGiftDetail);
        if (success) {
            pointService.modify(pointGiftDetail.getCpId(),
                    pointGiftDetail.getBizId(),
                    pointModifyTO.getFunctionCodeTypeTrancdPair(),
                    pointModifyTO.getPlatformType(), pointGiftDetail.getAmount().abs());
        }
        return success;
    }

    /**
     * 修改红包总额, 同时插入一条明细
     *
     * @param cpId    修改人cpId
     * @param amount  修改数量, 带正负号
     * @param bizId   业务id
     * @param bizType 业务类型
     * @return 修改结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean modifyWithDetail(Long cpId, BigDecimal amount, String bizId,
                                    PointSendType bizType) {
        PointTotal currTotal = pointService.initTotal(cpId);
        BigDecimal afterVal = currTotal.getUsablePointPacket().add(amount);
        if (afterVal.signum() < 0) {
            throw new IllegalStateException("红包总额不够扣减");
        }
        currTotal.setUsablePointPacket(afterVal);
        boolean ret = pointService.updateByCpId(currTotal);
        final PointGiftDetail pointGiftDetail = new PointGiftDetail();
        pointGiftDetail.setCpId(cpId);
        pointGiftDetail.setAmount(amount);
        pointGiftDetail.setBizId(bizId);
        pointGiftDetail.setBizType(bizType.name());
        pointGiftDetailService.insert(pointGiftDetail);
        return ret;
    }

    @Override
    public void insertPointGiftDetailForReceive(PointGift pointGift, PointGiftRecord pointGiftRecord) {
        final PointGiftDetail pointGiftDetail = new PointGiftDetail();
        pointGiftDetail.setCpId(Long.valueOf(pointGiftRecord.getReceiveCpid()));
        pointGiftDetail.setAmount(pointGiftRecord.getAmount());
        pointGiftDetail.setBizId(pointGift.getPointGiftNo());
        pointGiftDetail.setBizType(PointSendType.RECEIVE.name());
        pointGiftDetailService.insert(pointGiftDetail);
    }

    @Override
    public List<PointGift> listExpiredPacket() {
        return pointGiftMapper.selectExpiredPacket();
    }

    @Override
    public void updatePointGiftToExpired(List<PointGift> pointGifts) {
        List<String> pointGiftNos = Lists.newArrayList();
        for (PointGift pointGift : pointGifts) {
            pointGiftNos.add(pointGift.getPointGiftNo());
        }
        pointGiftMapper.updatePointGiftToExpired(pointGiftNos);

    }


    @Override
    public PointGift getByPointGiftNo(String pointGiftNo) {
        return pointGiftMapper.getByPointGiftNo(pointGiftNo);

    }

    /**
     * 填充数据
     *
     * @param record
     */
    private void fullPointGift(PointGift record) {
        Date currentDay = new Date();
        record.setCreatedAt(currentDay);
        record.setExpireAt(DateUtils.addDay(currentDay, 1));
        record.setPointGiftNo(snowflakeIdWorker.nextPacketId());
    }

    @Override
    public boolean checkPointGift(PointGift pointGift, PointGiftRecord pointGiftRecord,
                                  Map<String, GlobalErrorCode> mapParam) {
        //判断红包是否存在
        if (pointGift == null) {
            mapParam.put("ErrorType", GlobalErrorCode.PACKAGE_GIFT_NOT_EXSIT);
            return false;
        }

        //校验领取人是否重复领取
        PointGiftRecord repPointGiftRecord = pointGiftRecordMapper.selectByUserGiftNo(pointGiftRecord);
        if (repPointGiftRecord != null) {
            mapParam.put("ErrorType", GlobalErrorCode.PACKET_REPEAT_ERROR);
            return false;
        }

        //判断红包状态
        if (!RECEIVE.equals(pointGift.getStatus())) {
            mapParam.put("ErrorType", (OVER_DUE.equals(pointGift.getStatus())) ?
                    GlobalErrorCode.PACKAGE_GIFT_OVER_DUE :
                    GlobalErrorCode.PACKAGE_GIFT_BROUGHT_OUT);
            return false;
        }

        return true;
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public boolean savePointGiftRecord(PointGift pointGift, PointGiftRecord pointGiftRecord) {

        PointTotal pointTotalGrant = pointService.initTotal(Long.valueOf(pointGiftRecord.getGrantCpid()));
        BigDecimal usablePoint = pointTotalGrant.getTotalUsable();
        if (pointGift.getAggregateAmount().compareTo(usablePoint) > 0) {
            log.error("用户积分余额不够");
            return false;
        }

        pointGift.setPointGiftNo(snowflakeIdWorker.nextPacketId());
        pointGiftRecord.setPointGiftNo(pointGift.getPointGiftNo());

        //付-德分
        pointService.modify(Long.valueOf(pointGiftRecord.getGrantCpid()),
                pointGift.getPointGiftNo(), FunctionCodeType.getPacketSend(), PlatformType.E,
                pointGift.getAggregateAmount());

        //收-德分
        PointTotal pointTotalRec = pointService.initTotal(Long.valueOf(pointGiftRecord.getReceiveCpid()));
        BigDecimal pointPacketRec = pointTotalRec.getUsablePointPacket();
        pointTotalRec.setUsablePointPacket(pointPacketRec.add(pointGiftRecord.getAmount()));
        pointService.updateByCpId(pointTotalRec);
        insertPointGiftDetailForReceive(pointGift, pointGiftRecord);

        //红包记录表登记
        return ((pointGiftMapper.insert(pointGift) > 0) && (pointGiftRecordMapper.insert(pointGiftRecord) > 0));
    }

    @Override
    public PointGift selectBypointGiftNo(String pointGiftNo) {
        return pointGiftMapper.selectBypointGiftNo(pointGiftNo);
    }

    @Override
    public boolean isNew(Long buyId) {
        return !customerProfileService.getIdentify(buyId);
    }

    @Override
    public UserDTO selectByUserInfoPointGiftNo(String pointGiftNo) {
        return pointGiftMapper.selectByUserInfoPointGiftNo(pointGiftNo);
    }

    @Override
    public boolean isInWhiteList(Long cpId) {
        boolean ignoreWhiteList = isPacketEnable();
        if (ignoreWhiteList) {
            // 忽略白名单, 都返回true
            return true;
        }
        // 否则查看是否在白名单中
        return packetWhitelistMapper.selectExists(cpId);
    }

    /**
     * 判断红包功能是否开放
     */
    @Override
    public boolean isPacketEnable() {
        // 有999999表示禁用白名单
        return packetWhitelistMapper.selectExists(9999999L);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void sendPacket(PointGift pointGift) {
        String bizNO= snowflakeIdWorker.nextPacketId();
        consumePacketPointFirst(pointGift.getCpid(),pointGift.getAggregateAmount(),
                bizNO,pointGift.getPeopleAmount(),
                pointGift.getRequireIdentity(),pointGift.getPacketType(),pointGift.getLeaveWord(),pointGift.getDivisionType());
        //insertPointGiftAndDeduction(pointGift);
        pointGift.setPointGiftNo(bizNO);
        PointPacketManager.sendPacket(pointGift);
    }

    @Override
    public Boolean consumePacketPoint(Long cpId, Long consumePoint) {
        PointTotal pointTotal = pointService.initTotal(cpId);
        BigDecimal usablePoint = pointTotal.getTotalUsableWithPacket();

        if (BigDecimal.valueOf(consumePoint).compareTo(usablePoint) > 0) {
            log.error("[红包雨抽奖]-用户积分余额不够[{}]/[{}]", consumePoint, usablePoint);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public boolean consume(Long cpId, String bizId, BigDecimal amount) {
        PointTotal total = pointService.initTotal(cpId);
        BigDecimal totalUsable = total.getTotalUsableWithPacket();
        if (totalUsable == null || totalUsable.signum() <=0) {
            // 不够消费, return
            return false;
        }
        Pair<BigDecimal, BigDecimal> giftPair = PointGiftUtil
            .pointDetail(total, amount);
        BigDecimal totalUse = giftPair.getLeft();
        BigDecimal packetUse = giftPair.getRight();
        if (packetUse.signum() > 0) {
            modifyWithDetail(cpId, packetUse.negate(), bizId,
                PointSendType.CONSUMPTION);
            log.info("cpId {} 消费德分红包 {}, 业务Id: {}", cpId, packetUse, bizId);
        }
        if (totalUse.signum() > 0) {
            pointService.consume(cpId,
                    bizId,
                    Trancd.DEDUCT_P,
                    PlatformType.E,
                    totalUse);
            log.info("cpId {} 消费德分 {}, 业务Id: {}", cpId, totalUse, bizId);
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean prePacketRainCheck(String id, Long cpId, Long consumePoint) {

        final RedisUtils<Boolean> booleanRedisUtils = RedisUtilFactory.getInstance(Boolean.class);
        final String tokenKey = RainLotteryConstrants.LOTTERY_TOKEN_KEY.apply(id, cpId);

        //如果存在false-没资格,代表没有抽奖可以继续抽奖,
        //true-有资格,代表可以消费但没有消费
        if (!booleanRedisUtils.hasKey(tokenKey)) {
            booleanRedisUtils.set(tokenKey, Boolean.FALSE);
        }

        boolean booleanRedis = booleanRedisUtils.get(tokenKey);
        if (Boolean.TRUE.equals(booleanRedis)) {
            return Boolean.TRUE;
        } else {
            String orderId = String.join("", "LD",
                    snowflakeIdWorker.nextPacketId());
            this.consume(cpId, orderId, BigDecimal.valueOf(consumePoint));

            //消费后改为True
            booleanRedisUtils.set(tokenKey, Boolean.TRUE);

            return Boolean.TRUE;
        }
    }

    private void increaseLottery(String lotteryKey) {
        RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
        //如果存在就直接自增
        if (redisUtils.hasKey(lotteryKey)) {
            redisUtils.incrementOne(lotteryKey);
        } else {
            redisUtils.set(lotteryKey, 1);
            //设置过期时间
            redisUtils.expire(lotteryKey, betWeenToday(), TimeUnit.SECONDS);
        }
    }

    private long betWeenToday() {
        Date date = new Date();
        LocalDateTime midnight = LocalDateTime.ofInstant(date.toInstant(),
                ZoneId.systemDefault()).plusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
        LocalDateTime currentDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        return ChronoUnit.SECONDS.between(currentDateTime, midnight);
    }

    @Override
    public boolean consumePacketPointFirst(Long cpId, BigDecimal amount, String bizId,
                                            Integer peopleNumber,String requireIdentity, Integer packetType, String leaveWord, Integer divisionType) {
        PointTotal total = pointService.initTotal(cpId);
        BigDecimal totalUsable = total.getTotalUsableWithPacket();
        if (totalUsable == null || totalUsable.signum() <=0) {
            // 不够消费, return
            return false;
        }
        Pair<BigDecimal, BigDecimal> giftPair = PointGiftUtil
                .pointDetail(total, amount);
        BigDecimal totalUse = giftPair.getLeft();
        BigDecimal packetUse = giftPair.getRight();
        if (packetUse.signum() > 0) {
            modifyWithDetailAndGift(cpId, packetUse.negate(), bizId,
                    PointSendType.SEND_PACKET_POINT, peopleNumber, requireIdentity, packetType, leaveWord, divisionType,amount);
            log.info("cpId {} 消费德分红包 {}, 业务Id: {}", cpId, packetUse, bizId);
        }
        if (totalUse.signum() > 0) {
            pointService.consume(cpId,
                    bizId,
                    Trancd.PACKET_POINT,
                    PlatformType.E,
                    totalUse);
//            final PointGiftDetail pointGiftDetail = new PointGiftDetail();
//            pointGiftDetail.setCpId(cpId);
//            pointGiftDetail.setAmount(amount);
//            pointGiftDetail.setBizId(bizId);
//            pointGiftDetail.setBizType(PointSendType.SEND_PACKET_POINT.name());
//            pointGiftDetailService.insert(pointGiftDetail);

            //红包记录插入PointGift表
            PointGift pointGift = new PointGift();
            pointGift.setPointGiftNo(bizId);
            pointGift.setAggregateAmount(amount.abs());
            pointGift.setPeopleAmount(peopleNumber);
            pointGift.setRequireIdentity(requireIdentity);
            //pointGift.setPacketType(packetType);
            pointGift.setLeaveWord(leaveWord);
            pointGift.setDivisionType(divisionType);
            pointGift.setPacketType(PacketType.RANDOM.getCode());
            Date currentDay = new Date();
            pointGift.setCreatedAt(currentDay);
            pointGift.setExpireAt(DateUtils.addDay(currentDay, 1));
            pointGift.setCpid(cpId);
            pointGiftMapper.insertSelective(pointGift);
            log.info("cpId {} 消费德分 {}, 业务Id: {}", cpId, totalUse, bizId);
        }
        return true;
    }
    /**
     * 修改红包总额, 同时插入一条明细
     *
     * @param cpId    修改人cpId
     * @param amount  修改数量, 带正负号
     * @param bizId   业务id
     * @param bizType 业务类型
     * @return 修改结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean modifyWithDetailAndGift(Long cpId, BigDecimal amount, String bizId,
                                            PointSendType bizType, Integer peopleNumber,
                                                String requireIdentity, Integer packetType, String leaveWord, Integer divisionType, BigDecimal sourceAmount) {
        PointTotal currTotal = pointService.initTotal(cpId);
        BigDecimal usablePointPacket = currTotal.getUsablePointPacket();
        BigDecimal afterVal = currTotal.getUsablePointPacket().add(amount);
        if (afterVal.signum() < 0) {
            throw new IllegalStateException("红包总额不够扣减");
        }
        currTotal.setUsablePointPacket(afterVal);
        boolean ret = pointService.updateByCpId(currTotal);
        final PointGiftDetail pointGiftDetail = new PointGiftDetail();
        pointGiftDetail.setCpId(cpId);
        pointGiftDetail.setAmount(amount);
        pointGiftDetail.setBizId(bizId);
        pointGiftDetail.setBizType(bizType.name());
        pointGiftDetailService.insert(pointGiftDetail);

        //红包记录插入PointGift表
       //用户拥有红包德分金额
        BigDecimal amo = sourceAmount.abs();//发红包总金额
        if(amo.intValue()<=usablePointPacket.intValue()){
            PointGift pointGift = new PointGift();
            pointGift.setPointGiftNo(bizId);
            pointGift.setAggregateAmount(amount.abs());
            pointGift.setPeopleAmount(peopleNumber);
            pointGift.setRequireIdentity(requireIdentity);
            //pointGift.setPacketType(packetType);
            pointGift.setLeaveWord(leaveWord);
            pointGift.setDivisionType(divisionType);
            pointGift.setPacketType(PacketType.RANDOM.getCode());
            Date currentDay = new Date();
            pointGift.setCreatedAt(currentDay);
            pointGift.setExpireAt(DateUtils.addDay(currentDay, 1));
            pointGift.setCpid(cpId);
            pointGiftMapper.insertSelective(pointGift);
        }
        return ret;
    }
}
