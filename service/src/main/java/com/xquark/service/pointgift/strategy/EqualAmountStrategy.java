package com.xquark.service.pointgift.strategy;

import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;

import java.math.BigDecimal;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public class EqualAmountStrategy extends AbstractPointPacketStrategy {

    @Override
    protected void preCheck(BigDecimal aggregateAmount, Integer peopleAmount) {
        final BigDecimal decimal = aggregateAmount.divide(BigDecimal.valueOf(peopleAmount),BigDecimal.ROUND_HALF_UP);
        if (aggregateAmount.intValue() != peopleAmount * decimal.intValue()) {
            throw new BizException(GlobalErrorCode.NOT_DIVIDE_POINT);
        }
    }

    @Override
    protected BigDecimal[] obtainAmount(BigDecimal aggregateAmount, Integer peopleAmount) {
        final BigDecimal decimal = aggregateAmount.divide(BigDecimal.valueOf(peopleAmount),BigDecimal.ROUND_HALF_UP);
        BigDecimal[] amounts = new BigDecimal[peopleAmount];
        for (Integer i = 0; i < peopleAmount; i++) {
            amounts[i] = decimal;
        }
        return amounts;
    }

}
