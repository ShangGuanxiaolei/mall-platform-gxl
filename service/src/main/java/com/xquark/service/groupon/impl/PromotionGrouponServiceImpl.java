package com.xquark.service.groupon.impl;

import com.xquark.dal.mapper.PromotionGrouponMapper;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionGroupon;
import com.xquark.dal.vo.PromotionGrouponProductVO;
import com.xquark.dal.vo.PromotionGrouponUserVO;
import com.xquark.dal.vo.PromotionGrouponVO;
import com.xquark.service.groupon.PromotionGrouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by chh on 16-10-8.
 */
@Service("promotionGrouponServiceImpl")
public class PromotionGrouponServiceImpl implements PromotionGrouponService {

  @Autowired
  private PromotionGrouponMapper promotionGrouponMapper;

  /**
   * 根据id查询
   */
  @Override
  public PromotionGrouponVO selectByPrimaryKey(String id) {
    return promotionGrouponMapper.selectByPrimaryKey(id);
  }

  /**
   * 新增
   */
  @Override
  public int insert(PromotionGroupon promotionGroupon) {
    return promotionGrouponMapper.insert(promotionGroupon);
  }

  /**
   * 修改
   */
  @Override
  public int modifyPromotionGroupon(PromotionGroupon promotionGroupon) {
    return promotionGrouponMapper.modifyPromotionGroupon(promotionGroupon);
  }

  /**
   * 手动结束团购活动
   */
  @Override
  public int close(String id) {
    return promotionGrouponMapper.close(id);
  }

  /**
   * 列表查询钻石专享活动关联商品
   */
  @Override
  public List<PromotionGrouponProductVO> listPromotionProduct(Pageable page, String id) {
    return promotionGrouponMapper.listPromotionProduct(page, id);
  }

  /**
   * 对分页钻石专享活动关联商品列表接口取总数
   */
  @Override
  public Long selectPromotionProductCnt(String id) {
    return promotionGrouponMapper.selectPromotionProductCnt(id);
  }

  /**
   * 修改活动
   */
  @Override
  public int modifyPromotion(PromotionGrouponVO promotionGroupon) {
    return promotionGrouponMapper.modifyPromotion(promotionGroupon);
  }

  /**
   * 根据团购状态查询某个用户参与的团购活动
   */
  @Override
  public List<PromotionGrouponUserVO> listUserPromotion(String userId, String status,
      Pageable pageable) {
    return promotionGrouponMapper.listUserPromotion(userId, status, pageable);
  }

  /**
   * 列表查询钻石专享活动
   */
  @Override
  public List<Promotion> listPromotion(Pageable pageable, String keyword) {
    return promotionGrouponMapper.listPromotion(pageable, keyword);
  }

  /**
   * 对分页钻石专享活动列表接口取总数
   */
  @Override
  public Long selectPromotionCnt(String keyword) {
    return promotionGrouponMapper.selectPromotionCnt(keyword);
  }

  /**
   * 将id对应的xquark_promotion_Diamond记录archive设置为true
   */
  @Override
  public int delete(String id) {
    return promotionGrouponMapper.delete(id);
  }

  @Override
  public PromotionGrouponVO loadInPromotion(String productId, String promotionId) {
    return promotionGrouponMapper.selectInPromotion(productId, promotionId);
  }
}
