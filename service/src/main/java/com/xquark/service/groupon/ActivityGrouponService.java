package com.xquark.service.groupon;

import com.xquark.dal.model.ActivityGroupon;
import com.xquark.dal.model.ActivityGrouponDetail;
import com.xquark.dal.status.ActivityGrouponStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.vo.ActivityGrouponDetailVO;
import com.xquark.dal.vo.PromotionGrouponVO;

import java.util.List;

/**
 * Created by chh on 16-10-9. 团购活动表Service
 */
public interface ActivityGrouponService {

  /**
   * 根据id查询团购活动表
   */
  ActivityGroupon selectByPrimaryKey(String id);

  ActivityGroupon loadUnique(String userId, String groupOnId);

  /**
   * 新增团购活动表
   */
  int insert(ActivityGroupon activityGroupon);

  /**
   * 修改团购活动表状态
   */
  int updateStatus(String id, ActivityGrouponStatus status);

  /**
   * 根据id查询团购活动明细表
   */
  ActivityGrouponDetail selectDetailByPrimaryKey(String id);

  /**
   * 新增团购活动明细表
   */
  int insertDetail(ActivityGrouponDetail activityGrouponDetail);

  /**
   * 通过订单找到对应的团购活动设置相关信息，如开团时间，开团人数等
   */
  PromotionGrouponVO findPromotionGrouponByOrderId(String orderId);

  /**
   * 根据订单id查询团购活动表
   */
  ActivityGroupon findActivityGrouponByOrderId(String orderId);

  /**
   * 通过订单统计对应的团购活动所有已支付的订单数
   */
  int countPaidOrderByOrderId(String orderId);

  /**
   * 通过id统计对应的团购活动所有已支付的订单数
   */
  int countPaidOrderById(String id);

  /**
   * 通过订单统计对应的团购活动所有指定状态的订单数
   */
  int countOrderByOrderIdAndStatus(String orderId, OrderStatus status);

  /**
   * 团购活动到了结束时间则自动更新未成团活动状态
   */
  int autoGrouponClose();

  /**
   * 根据订单id更新对应团购活动的库存，销量
   */
  int updateStock(String orderId, int qty);

  /**
   * 优惠活动时间到了后自动结束
   */
  int autoPromotionClose();

  List<ActivityGrouponDetailVO> listGrouponUsers(String activityGrouponId);

  boolean isUserInGroupon(String userId);

  /**
   * 通过下订单发起团购
   *
   * @param grouponId 团购商品id
   * @param orderId 订单id
   * @return 是否成功
   */
  boolean startGroupon(String grouponId, String orderId);

  /**
   * 参加某个团购活动
   *
   * @param activityGrouponId 团购活动id
   * @param orderId 订单id
   * @return 是否成功
   */
  boolean joinGroupon(String activityGrouponId, String orderId);

  /**
   * 自动将未成团且已经付款的订单进行退款操作
   */
  int autoGrouponRefund();

  /**
   * 得到该团购活动所有已经参与购买的买家id
   */
  List<String> getPaidUserByOrderId(String orderId);

  /**
   * 结束拼团活动后，需要自动将该活动对应的未成团订单退款
   */
  List<String> needRefundOrdersByPromotionGrouponId(String grouponId);

  /**
   * 得到该团购活动所有已经参与购买或退款的买家id
   */
  List<String> getPaidOrCloseUserByOrderId(String orderId);
}
