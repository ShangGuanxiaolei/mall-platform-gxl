package com.xquark.service.groupon;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionGroupon;
import com.xquark.dal.vo.PromotionGrouponProductVO;
import com.xquark.dal.vo.PromotionGrouponUserVO;
import com.xquark.dal.vo.PromotionGrouponVO;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by chh on 16-10-8. 团购设置表Service
 */
public interface PromotionGrouponService {

  /**
   * 根据id查询
   */
  PromotionGrouponVO selectByPrimaryKey(String id);

  /**
   * 新增
   */
  int insert(PromotionGroupon promotionGroupon);

  /**
   * 修改
   */
  int modifyPromotionGroupon(PromotionGroupon promotionGroupon);

  /**
   * 手动结束团购活动
   */
  int close(String id);

  /**
   * 列表查询钻石专享活动关联商品
   */
  List<PromotionGrouponProductVO> listPromotionProduct(Pageable page, String id);

  /**
   * 对分页钻石专享活动关联商品列表接口取总数
   */
  Long selectPromotionProductCnt(String id);

  /**
   * 修改活动
   */
  int modifyPromotion(PromotionGrouponVO promotionGroupon);

  /**
   * 根据团购状态查询某个用户参与的团购活动
   */
  List<PromotionGrouponUserVO> listUserPromotion(String userId, String status, Pageable pageable);

  /**
   * 列表查询钻石专享活动
   */
  List<Promotion> listPromotion(Pageable pageable, String keyword);

  /**
   * 对分页钻石专享活动列表接口取总数
   */
  Long selectPromotionCnt(String keyword);

  /**
   * 将id对应的xquark_promotion_Diamond记录archive设置为true
   */
  int delete(String id);

  PromotionGrouponVO loadInPromotion(String productId, String promotionId);
}
