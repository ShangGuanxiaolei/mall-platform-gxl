package com.xquark.service.groupon.impl;

import com.xquark.dal.mapper.ActivityGrouponDetailMapper;
import com.xquark.dal.mapper.ActivityGrouponMapper;
import com.xquark.dal.model.ActivityGroupon;
import com.xquark.dal.model.ActivityGrouponDetail;
import com.xquark.dal.model.Order;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.ActivityGrouponStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.vo.ActivityGrouponDetailVO;
import com.xquark.dal.vo.PromotionGrouponVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.groupon.ActivityGrouponService;
import com.xquark.service.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by chh on 16-10-9.
 */
@Service("activityGrouponServiceImpl")
public class ActivityGrouponServiceImpl implements ActivityGrouponService {

  @Autowired
  private ActivityGrouponMapper activityGrouponMapper;

  @Autowired
  private ActivityGrouponDetailMapper activityGrouponDetailMapper;

  @Autowired
  private OrderService orderService;

  /**
   * 根据id查询团购活动表
   */
  @Override
  public ActivityGroupon selectByPrimaryKey(String id) {
    return activityGrouponMapper.selectByPrimaryKey(id);
  }

  @Override
  public ActivityGroupon loadUnique(String userId, String groupOnId) {
    return activityGrouponMapper.selectUnique(userId, groupOnId);
  }

  /**
   * 新增团购活动表
   */
  @Override
  public int insert(ActivityGroupon activityGroupon) {
    return activityGrouponMapper.insert(activityGroupon);
  }

  /**
   * 修改团购活动表状态
   */
  @Override
  public int updateStatus(String id, ActivityGrouponStatus status) {
    return activityGrouponMapper.updateStatus(id, status);
  }

  /**
   * 根据id查询团购活动明细表
   */
  @Override
  public ActivityGrouponDetail selectDetailByPrimaryKey(String id) {
    return activityGrouponDetailMapper.selectByPrimaryKey(id);
  }

  /**
   * 新增团购活动明细表
   */
  @Override
  public int insertDetail(ActivityGrouponDetail activityGrouponDetail) {
    return activityGrouponDetailMapper.insert(activityGrouponDetail);
  }

  /**
   * 通过订单找到对应的团购活动设置相关信息，如开团时间，开团人数等
   */
  @Override
  public PromotionGrouponVO findPromotionGrouponByOrderId(String orderId) {
    return activityGrouponMapper.findPromotionGrouponByOrderId(orderId);
  }

  /**
   * 通过订单统计对应的团购活动所有已支付的订单数
   */
  @Override
  public int countPaidOrderByOrderId(String orderId) {
    return activityGrouponMapper.countPaidOrderByOrderId(orderId);
  }

  /**
   * 通过id统计对应的团购活动所有已支付的订单数
   */
  @Override
  public int countPaidOrderById(String id) {
    return activityGrouponMapper.countPaidOrderById(id);
  }

  /**
   * 根据订单id查询团购活动表
   */
  @Override
  public ActivityGroupon findActivityGrouponByOrderId(String orderId) {
    return activityGrouponMapper.findActivityGrouponByOrderId(orderId);
  }

  /**
   * 通过订单统计对应的团购活动所有指定状态的订单数
   */
  @Override
  public int countOrderByOrderIdAndStatus(String orderId, OrderStatus status) {
    return activityGrouponMapper.countOrderByOrderIdAndStatus(orderId, status);
  }

  /**
   * 团购活动到了结束时间则自动更新未成团活动状态
   */
  @Override
  public int autoGrouponClose() {
    return activityGrouponMapper.autoGrouponClose();
  }

  /**
   * 根据订单id更新对应团购活动的库存，销量
   */
  @Override
  public int updateStock(String orderId, int qty) {
    return activityGrouponMapper.updateStock(orderId, qty);
  }

  /**
   * 优惠活动时间到了后自动结束
   */
  @Override
  public int autoPromotionClose() {
    return activityGrouponMapper.autoPromotionClose();
  }

  @Override
  public List<ActivityGrouponDetailVO> listGrouponUsers(String activityGrouponId) {
    return activityGrouponDetailMapper.selectGrouponUsers(activityGrouponId);
  }

  @Override
  public boolean isUserInGroupon(String userId) {
    return activityGrouponDetailMapper.selectUserInGroupon(userId) > 0;
  }

  @Override
  @Transactional
  public boolean startGroupon(String grouponId, String orderId) {
    // 先发起团购
    Order order = orderService.load(orderId);
    if (order == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "订单不存在");
    }
    String userId = order.getBuyerId();
    ActivityGroupon activityGroupon = new ActivityGroupon();
    activityGroupon.setGrouponId(grouponId);
    activityGroupon.setArchive(false);
    activityGroupon.setUserId(userId);
    activityGroupon.setStatus(ActivityGrouponStatus.CREATED);
    boolean startResult = this.insert(activityGroupon) > 0;
    String activityGrouponId = IdTypeHandler.encode(Long.valueOf(activityGroupon.getId()));

    // 再加入团购
    boolean joinResult = this.joinGroupon(activityGrouponId, orderId);
    return startResult && joinResult;
  }

  @Override
  public boolean joinGroupon(String activityGrouponId, String orderId) {
    ActivityGroupon activityGroupon = this.selectByPrimaryKey(activityGrouponId);
    if (activityGroupon == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "团购活动不存在");
    }
    ActivityGrouponDetail activityGrouponDetail = new ActivityGrouponDetail();
    activityGrouponDetail.setActivityGrouponId(activityGrouponId);
    activityGrouponDetail.setOrderId(orderId);
    activityGrouponDetail.setArchive(false);
    return this.insertDetail(activityGrouponDetail) > 0;
  }

  /**
   * 自动将未成团且已经付款的订单进行退款操作
   */
  @Override
  @Transactional
  public int autoGrouponRefund() {
    List<String> orderIds = activityGrouponMapper.needRefundOrders();
    for (String orderId : orderIds) {
      try {
        orderService.refundSystem(IdTypeHandler.encode(new Long(orderId)));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return orderIds.size();
  }

  /**
   * 得到该团购活动所有已经参与购买的买家id
   */
  @Override
  public List<String> getPaidUserByOrderId(String orderId) {
    return activityGrouponMapper.getPaidUserByOrderId(orderId);
  }

  /**
   * 结束拼团活动后，需要自动将该活动对应的未成团订单退款
   */
  @Override
  public List<String> needRefundOrdersByPromotionGrouponId(String grouponId) {
    return activityGrouponMapper.needRefundOrdersByPromotionGrouponId(grouponId);
  }

  /**
   * 得到该团购活动所有已经参与购买的买家id
   */
  @Override
  public List<String> getPaidOrCloseUserByOrderId(String orderId) {
    return activityGrouponMapper.getPaidOrCloseUserByOrderId(orderId);
  }

}
