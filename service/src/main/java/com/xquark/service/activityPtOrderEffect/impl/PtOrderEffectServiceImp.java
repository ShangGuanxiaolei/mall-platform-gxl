package com.xquark.service.activityPtOrderEffect.impl;

import com.xquark.dal.mapper.ActivityPtOrderStatusMapper;
import com.xquark.dal.mapper.ActivityPtOutTImeMapper;
import com.xquark.service.activityPtOrderEffect.PtOrderEffectService;
import com.xquark.service.cantuan.PieceGroupPayCallBackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liuyandong
 * @date 2018/9/11 10:40
 */
@Service
public class PtOrderEffectServiceImp implements PtOrderEffectService {
    @Autowired
    private ActivityPtOutTImeMapper activityPtOutTImeMapper;
    @Autowired
    private ActivityPtOrderStatusMapper activityPtOrderStatusMapper;
   @Autowired
    private PieceGroupPayCallBackService pieceGroupPayCallBackService;
    @Override
    public void updateOrderStatu() {
        List<String> list = activityPtOrderStatusMapper.selectPtPcode();
        for (int i = 0; i < list.size(); i++) {
            //拼团编码
            List<String> list1 = activityPtOrderStatusMapper.selectPtGroupCode(list.get(i));
            for (int p = 0; p < list1.size(); p++) {
                //订单编号
                pieceGroupPayCallBackService.effectOrder(list1.get(p));
             /*   List<String> list2 = activityPtOutTImeMapper.selectOrderId(list1.get(p), list.get(i));
                for (int o = 0; o < list2.size(); o++) {
                    //订单生效
                    activityPtOrderStatusMapper.updateOrderStatus(list2.get(o));

                }*/
            }

        }


    }
}
