package com.xquark.service.activityEnd.impl;

import com.xquark.dal.mapper.ActivityEndMapper;
import com.xquark.dal.model.Timing;
import com.xquark.service.activityEnd.ActivityEndService;
import com.xquark.service.activityRecycle.ActivityRecycleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class ActivityEndServiceImp implements ActivityEndService {
    @Autowired
    private ActivityEndMapper activityEndMapper;
    @Autowired
    private ActivityRecycleService activityRecycleService;
    private Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public void activityEndService() {
        Timestamp time = new Timestamp((new Date()).getTime());
        //获得拼团活动的活动编码
        log.info("获得失效的活动");
        List<Timing> list = activityEndMapper.selectActivityCode(time);

        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getpType().equals("piece")) {
                // 查看是否还有未结束的拼团团组
                int a = activityEndMapper.selectPtIsLive(list.get(i).getpCode());
                //若无，把活动库存释放到单品库存中
                if (a == 0) {
                    //活动库存释放
                    //activityRecycleService.activitySkuRecycle(list.get(i));库存释放取消
                    //下架活动
                    activityEndMapper.updateActivityStatus(list.get(i).getpCode());
                    activityEndMapper.updateExclusiveStatus(list.get(i).getpCode());
                    activityEndMapper.updateTempStock(list.get(i).getpCode());
                }
                //如果有，库存不释放，活动不下架
            }
            if (list.get(i).getpType().contains("meeting")){
                //下架活动
                activityEndMapper.updateActivityStatus(list.get(i).getpCode());
                activityEndMapper.updateExclusiveStatus(list.get(i).getpCode());
                activityEndMapper.updateTempStock(list.get(i).getpCode());
            }

        }

    }

}