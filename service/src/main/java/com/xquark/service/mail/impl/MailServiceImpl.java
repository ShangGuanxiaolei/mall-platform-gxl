package com.xquark.service.mail.impl;

import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.mail.JavaMailWithAttachment;
import com.xquark.service.mail.MailService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: kong Date: 2018/6/30. Time: 17:53
 */
@Service
@Transactional
public class MailServiceImpl implements MailService {


  @Override
  public Boolean sendMail(File file, String email) {
    Boolean buttum = true;
    try {
      JavaMailWithAttachment se = new JavaMailWithAttachment(true);
      se.doSendHtmlEmail("邮件主题", "邮件内容", email, file);
    } catch (Exception e) {
      e.printStackTrace();
      buttum = false;
    }
    return buttum;
  }


}
