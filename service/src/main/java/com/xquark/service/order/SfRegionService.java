package com.xquark.service.order;

import com.xquark.dal.mapper.RegionDict;

import java.util.List;

/**
 * 顺丰区域服务接口
 */
public interface SfRegionService {

  /**
   * 查询指定region的所有父级及自已
   */
  List<RegionDict> queryAllParentAndSelf(int regionId);

  /**
   * 根据zoneid 查询 sfid
   */

  Long getsfid(Long zoneid);
}
