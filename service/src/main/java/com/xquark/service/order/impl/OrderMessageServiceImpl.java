package com.xquark.service.order.impl;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.model.*;
import com.xquark.dal.status.SmsMessageStatus;
import com.xquark.dal.type.SmsMessageType;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.SellerInfoVO;
import com.xquark.dal.vo.UserInfo;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.msg.SmsMessageService;
import com.xquark.service.order.OrderMessageService;
import com.xquark.service.push.PushService;
import com.xquark.service.smstpl.SmsTplService;
import com.xquark.service.tinyurl.TinyUrlService;
import com.xquark.service.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service("orderMessageService")
public class OrderMessageServiceImpl extends BaseServiceImpl implements OrderMessageService {

  @Autowired
  private PushService pushService;

  @Autowired
  private SmsMessageService smsMessageService;

  @Autowired
  private UserService userService;

  @Autowired
  private TinyUrlService tinyUrlService;

  private static int length = 8;

  @Value("${site.web.host.name}")
  private String domainName;

  @Value("${site.tinyurl.domain.name}")
  private String tinyUrlDomainName;

  @Autowired
  private CashierService cashierService;

  @Autowired
  private SmsTplService smsTplService;

//	private String smsTplSwt = "VALID";

//	private Boolean doSend(OrderVO order, String content, String event, String partner) {
//		Boolean ret= false;
//		if (order == null || content == null) return ret;
//		
//		SmsMessageType smt = SmsMessageType.valueOf(event); 
//		
//		OrderAddress address = order.getOrderAddress();
//		if (address != null && address.getPhone() != null){
//			UserPartnerType userType = UserPartnerType.KKKD;
//			if("xiangqu".equalsIgnoreCase(partner)){
//				userType = UserPartnerType.XIANGQU;
//			} else {
//				userType = UserPartnerType.KKKD;
//			}
//		
//			ret = sendSms(address.getPhone(),content, userType);
//			if (!ret){
//				insertSmsMessage(address.getPhone(),content,smt);
//			}
//		}
//		return ret;
//	}

  private Boolean doSendEngine(OrderVO order, SmsMessageType smt, String partner,
      String paramsDatas) {
    if (order == null || smt == null) {
      return false;
    }

    OrderAddress address = order.getOrderAddress();
    Boolean ret = false;
    if (address != null && address.getPhone() != null) {
      // 哪里的用户群体
      UserPartnerType userType = UserPartnerType.KKKD;
      if ("xiangqu".equalsIgnoreCase(partner)) {
        userType = UserPartnerType.XIANGQU;
      }

      ret = sendSmsEngine(address.getPhone(), smt, userType, paramsDatas);

      if (!ret) {
        insertSmsMessage(address.getPhone(), smt, paramsDatas);
      }
    }

    return ret;
  }

  private Boolean doSendEngine(MainOrderVO order, SmsMessageType smt, String partner,
      String paramsDatas) {
    if (order == null || smt == null) {
      return false;
    }
    OrderAddress address = order.getOrders().get(0).getOrderAddress();
    Boolean ret = false;
    if (address != null && address.getPhone() != null) {
      // 哪里的用户群体
      UserPartnerType userType = UserPartnerType.KKKD;
      if ("xiangqu".equalsIgnoreCase(partner)) {
        userType = UserPartnerType.XIANGQU;
      }

      ret = sendSmsEngine(address.getPhone(), smt, userType, paramsDatas);

      if (!ret) {
        insertSmsMessage(address.getPhone(), smt, paramsDatas);
      }
    }

    return ret;
  }

  private String createParams(OrderVO order, User seller, User buyer,
      Map<String, String> otherParams) {
    JSONObject json = new JSONObject();
    if (order != null) {
      json.put("order", order);
    }

    if (seller != null) {
      UserInfo user = new UserInfo();
      BeanUtils.copyProperties(seller, user);
      json.put("seller", user);
    }

    if (buyer != null) {
      UserInfo user = new UserInfo();
      BeanUtils.copyProperties(buyer, user);
      json.put("buyer", user);
    }

    if (otherParams != null) {
      Iterator<String> it = otherParams.keySet().iterator();
      while (it.hasNext()) {
        String key = it.next();
        String value = otherParams.get(key);
        json.put(key, value);
      }
    }

    return json.toJSONString();
  }

  private String createParams(MainOrder order, User seller, User buyer,
      Map<String, String> otherParams) {
    JSONObject json = new JSONObject();
    if (order != null) {
      json.put("order", order);
    }

    if (seller != null) {
      UserInfo user = new UserInfo();
      BeanUtils.copyProperties(seller, user);
      json.put("seller", user);
    }

    if (buyer != null) {
      UserInfo user = new UserInfo();
      BeanUtils.copyProperties(buyer, user);
      json.put("buyer", user);
    }

    if (otherParams != null) {
      Iterator<String> it = otherParams.keySet().iterator();
      while (it.hasNext()) {
        String key = it.next();
        String value = otherParams.get(key);
        json.put(key, value);
      }
    }

    return json.toJSONString();
  }

  private String createParamsForSeller(OrderVO order, SellerInfoVO seller, User buyer,
      Map<String, String> otherParams) {
    JSONObject json = new JSONObject();
    if (order != null) {
      json.put("order", order);
    }

    if (seller != null) {
      SellerInfoVO user = new SellerInfoVO();
      BeanUtils.copyProperties(seller, user);
      json.put("seller", user);
    }

    if (buyer != null) {
      UserInfo user = new UserInfo();
      BeanUtils.copyProperties(buyer, user);
      json.put("buyer", user);
    }

    if (otherParams != null) {
      Iterator<String> it = otherParams.keySet().iterator();
      while (it.hasNext()) {
        String key = it.next();
        String value = otherParams.get(key);
        json.put(key, value);
      }
    }

    return json.toJSONString();
  }


  private String createParamsForSeller(MainOrderVO order, SellerInfoVO seller, User buyer,
      Map<String, String> otherParams) {
    JSONObject json = new JSONObject();
    if (order != null) {
      json.put("order", order);
    }

    if (seller != null) {
      SellerInfoVO user = new SellerInfoVO();
      BeanUtils.copyProperties(seller, user);
      json.put("seller", user);
    }

    if (buyer != null) {
      UserInfo user = new UserInfo();
      BeanUtils.copyProperties(buyer, user);
      json.put("buyer", user);
    }

    if (otherParams != null) {
      Iterator<String> it = otherParams.keySet().iterator();
      while (it.hasNext()) {
        String key = it.next();
        String value = otherParams.get(key);
        json.put(key, value);
      }
    }

    return json.toJSONString();
  }

  @Override
  public boolean sendBuyerSMSOrderSubmitted(OrderVO order, User seller, User buyer) {
    boolean ret = false;
    if (order == null || seller == null || buyer == null) {
      return ret;
    }

    String prodName = "";
    String orderInfo = getTinyUrl(order.getId());
    List<OrderItem> items = order.getOrderItems();
    if (items != null) {
      prodName = createProductName(items);
    }

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("prodName", prodName);
    otherParams.put("orderInfo", orderInfo);
    String jsonData = createParams(order, seller, buyer, otherParams);

    ret = doSendEngine(order, SmsMessageType.SUBMITTED_BUYER, buyer.getPartner(), jsonData);
    return ret;
  }

  @Override
  public boolean sendBuyerSMSMainOrderSubmitted(MainOrderVO order, User buyer) {
    boolean ret = false;
    if (order == null || buyer == null) {
      return ret;
    }

    String prodName = "";
    String orderInfo = getTinyUrl(order.getId());
    List<OrderVO> orderVOs = order.getOrders();
    for (OrderVO ordervo : orderVOs) {
      List<OrderItem> items = ordervo.getOrderItems();
      if (items != null) {
        prodName = createProductName(items);
      }
    }
    SellerInfoVO seller = new SellerInfoVO();
    BeanUtils.copyProperties(buyer, seller);
    //seller.setLoginname("EasyShop");
    //seller.setUserShopName("EasyShop");
    //seller.setServicePhone("XXXXXXXXXX");
    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("prodName", prodName);
    otherParams.put("orderInfo", orderInfo);
    String jsonData = createParamsForSeller(order, seller, buyer, otherParams);
    ret = doSendEngine(order, SmsMessageType.SUBMITTED_BUYER, buyer.getPartner(), jsonData);
    return ret;
  }

  @Override
  public boolean sendBuyerSMSOrderSubmittedForSeller(OrderVO order, SellerInfoVO seller,
      User buyer) {
    boolean ret = false;
    if (order == null || seller == null || buyer == null) {
      return ret;
    }

    String prodName = "";
    String orderInfo = getTinyUrl(order.getId());
    List<OrderItem> items = order.getOrderItems();
    if (items != null) {
      prodName = createProductName(items);
    }

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("prodName", prodName);
    otherParams.put("orderInfo", orderInfo);
    String jsonData = createParamsForSeller(order, seller, buyer, otherParams);

    ret = doSendEngine(order, SmsMessageType.SUBMITTED_BUYER, buyer.getPartner(), jsonData);
    return ret;
  }

//	@Override
//	public boolean sendBuyerSMSOrderSubmitted(OrderVO order, User seller, User buyer) {
//		boolean ret = false;
//		if (order == null || seller == null || buyer == null) return ret;
//		
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("smsEvent", "SUBMITTED_BUYER");
//		params.put("status", smsTplSwt);
//		SmsTpl aTpl = smsTplService.getUniqueTpl(params);
//		if (aTpl != null) {
//			if ( !smsTplService.checkFmtVars(aTpl.getFormatVar().split(","))) {
//				return false;
//			}
//			String content = null;
//			String prodName = null;
//			String orderInfo  = null;
//			String sellerName = null;
//			List<OrderItem> items = order.getOrderItems();
//			if (items != null)	prodName = createProductName(items);
//			if (seller.getLoginname()!= null)	sellerName = seller.getLoginname();
//			orderInfo = getTinyUrl(order.getId());	
//			
//			Formatter fmt = new Formatter();
//			try {
//				//fmt.format(aTpl.getContent(), aTpl.getFormatVar());
//				fmt.format(aTpl.getContent(), prodName, orderInfo, sellerName, sellerName);
//				content = fmt.toString();
//			} finally {
//				fmt.close();
//			}
//			ret = doSend(order, content, aTpl.getSmsEvent(), buyer.getPartner());
//		} else {  // 20141128版本保留, 没有问题后面版本再替换为异常处理
//			StringBuffer content = new StringBuffer();
//			content.append("恭喜亲，您的宝贝");
//			List<OrderItem> items = order.getOrderItems();
//			if (items != null){
//				content.append(createProductName(items));
//			}
//			content.append("已成功下单，订单地址: ");
//			content.append(getTinyUrl(order.getId()));
//			if (seller.getName()!= null){
//				content.append(" ，卖家: ");
//				content.append(seller.getLoginname());			
//			}
//			content.append(" 。请尽快完成付款，如有疑问可通过订单号联系");
//			content.append(seller.getLoginname());
//			content.append("查询");
//			OrderAddress address = order.getOrderAddress();
//			if (address != null && address.getPhone() != null){
//				UserPartnerType userType = UserPartnerType.KKKD;
//				if("xiangqu".equals(buyer.getPartner())){
//					userType = UserPartnerType.XIANGQU;
//				}
//				ret = sendSms(address.getPhone(),content.toString(), userType);
//				if (!ret){
//					insertSmsMessage(address.getPhone(),content.toString(),SmsMessageType.SUBMITTED_BUYER);
//				}
//			}
//		}
//		return ret;
//	}

  @Override
  public boolean sendBuyerSMSOrderPaied(OrderVO order, User seller, User buyer) {
    boolean ret = false;
    if (order == null || seller == null || buyer == null) {
      return ret;
    }

    String prodName = "";
    String orderInfo = getTinyUrl(order.getId());
    List<OrderItem> items = order.getOrderItems();
    if (items != null) {
      prodName = createProductName(items);
    }

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("prodName", prodName);
    otherParams.put("orderInfo", orderInfo);
    String jsonData = createParams(order, seller, buyer, otherParams);

    ret = doSendEngine(order, SmsMessageType.PAID_BUYER, buyer.getPartner(), jsonData);
    return ret;
  }

  @Override
  public boolean sendBuyerSMSMainOrderPaied(MainOrderVO order, User buyer) {
    boolean ret = false;
    if (order == null || buyer == null) {
      return ret;
    }

    String prodName = "";
    String orderInfo = getTinyUrl(order.getId());
    List<OrderVO> orderVOs = order.getOrders();
    for (OrderVO ordervo : orderVOs) {
      List<OrderItem> items = ordervo.getOrderItems();
      if (items != null) {
        prodName = createProductName(items);
      }
    }
    SellerInfoVO seller = new SellerInfoVO();
    BeanUtils.copyProperties(buyer, seller);
    //seller.setLoginname("EasyShop");
    //seller.setUserShopName("EasyShop");
    //seller.setServicePhone("XXXXXXXXXX");
    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("prodName", prodName);
    otherParams.put("orderInfo", orderInfo);
    String jsonData = createParamsForSeller(order, seller, buyer, otherParams);

    ret = doSendEngine(order, SmsMessageType.PAID_BUYER, buyer.getPartner(), jsonData);
    return ret;
  }

  @Override
  public boolean sendBuyerSMSOrderPaied(OrderVO order, SellerInfoVO seller, User buyer) {
    boolean ret = false;
    if (order == null || seller == null || buyer == null) {
      return ret;
    }

    String prodName = "";
    String orderInfo = getTinyUrl(order.getId());
    List<OrderItem> items = order.getOrderItems();
    if (items != null) {
      prodName = createProductName(items);
    }

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("prodName", prodName);
    otherParams.put("orderInfo", orderInfo);
    String jsonData = createParamsForSeller(order, seller, buyer, otherParams);

    ret = doSendEngine(order, SmsMessageType.PAID_BUYER, buyer.getPartner(), jsonData);
    return ret;
  }

//	@Override
//	public boolean sendBuyerSMSOrderPaied(OrderVO order, User seller,User buyer) {
//		boolean ret = false;
//		if (order == null || seller == null || buyer == null) return ret;
//
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("smsEvent", "PAID_BUYER");
//		params.put("status", smsTplSwt);
//		SmsTpl aTpl = smsTplService.getUniqueTpl(params);
//		if (aTpl != null) {
//			if ( !smsTplService.checkFmtVars(aTpl.getFormatVar().split(","))) {
//				return false;
//			}
//			String content = null;
//			String prodName = null;
//			String sellerName = null;
//			String orderInfo  = null;
//			List<OrderItem> items = order.getOrderItems();
//			if (items != null)	prodName = createProductName(items);
//			if (seller.getLoginname()!= null)	sellerName = seller.getLoginname();
//			orderInfo = getTinyUrl(order.getId());	
//			Formatter fmt = new Formatter();
//			try {
//				fmt.format(aTpl.getContent(), prodName, orderInfo, sellerName, sellerName);
//				content = fmt.toString();
//			} finally {
//				fmt.close();
//			}
//			ret = doSend(order, content, aTpl.getSmsEvent(), buyer.getPartner());
//		} else {
//			StringBuffer content = new StringBuffer();
//			content.append("恭喜亲，您的宝贝");
//			List<OrderItem> items = order.getOrderItems();
//			if (items != null){
//				content.append(createProductName(items));
//			}
//			content.append("购买成功，订单号: ");
//			content.append(getTinyUrl(order.getId()));
//			if (seller.getName()!= null){
//				content.append(" ，卖家: ");
//				content.append(seller.getLoginname());			
//			} else {
//				content.append(" ，卖家");
//			}
//			content.append("将会尽快发货，如有疑问可通过订单号联系");
//			content.append(seller.getLoginname());
//			content.append("查询");
//			OrderAddress address = order.getOrderAddress();
//			if (address != null && address.getPhone() != null){
//				UserPartnerType userType = UserPartnerType.KKKD;
//				if("xiangqu".equals(buyer.getPartner())){
//					userType = UserPartnerType.XIANGQU;
//				}
//				ret = sendSms(address.getPhone(),content.toString(), userType);
//				if (!ret){
//					insertSmsMessage(address.getPhone(),content.toString(),SmsMessageType.PAID_BUYER);
//				}
//			}
//		}
//		return ret;
//	}

  @Override
  public boolean sendSellerSMSOrderPaied(OrderVO order, User seller, User buyer) {
    boolean ret = false;
    if (order == null || seller == null || buyer == null || seller.getLoginname() == null) {
      return ret;
    }

    String prodName = null;
    String orderInfo = getTinyUrl(order.getId());
    List<OrderItem> items = order.getOrderItems();
    if (items != null) {
      prodName = createProductName(items);
    }

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("prodName", prodName);
    otherParams.put("orderInfo", orderInfo);
    String jsonData = createParams(order, seller, buyer, otherParams);

    if (sendSmsEngine(seller.getPhone(), SmsMessageType.PAID_SELLER, jsonData)) {
      insertSmsMessage(seller.getPhone(), SmsMessageType.PAID_SELLER, jsonData);
    }
    return ret;
  }

//	@Override
//	public boolean sendSellerSMSOrderPaied(OrderVO order, User seller,User buyer) {
//		boolean ret = false;
//		if (order == null || seller == null || buyer == null || seller.getLoginname() == null) return ret;
//
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("smsEvent", "PAID_SELLER");
//		params.put("status", smsTplSwt);
//		List<SmsTpl> aTpls = smsTplService.getSmsTpl(params);
//		if (aTpls != null && aTpls.size() != 0) {
//			SmsTpl aTpl = aTpls.get(0);  // TODO: 微信号为空
//			String content = null;
//			String prodName = null;
//			String orderInfo  = null;
//			String buyerWeichat = "";
//			String buyerName = null;
//			List<OrderItem> items = order.getOrderItems();
//			if (items != null)	prodName = createProductName(items);
//			orderInfo = getTinyUrl(order.getId());	
//			OrderAddress address = order.getOrderAddress();
//			if (address != null) {
//				buyerName = address.getConsignee();
//				if (address.getWeixinId() != null) {
//					buyerWeichat = address.getWeixinId();
//				}
//				if ( !smsTplService.checkFmtVars(aTpls.get(0).getFormatVar().split(","))) {
//					return false;
//				}
//				Formatter fmt = new Formatter();
//				try {
//					fmt.format(aTpl.getContent(), prodName, orderInfo, buyerName, buyerWeichat);
//					content = fmt.toString();
//				} finally {
//					fmt.close();
//				}
//				if (sendSms(seller.getLoginname(),content.toString())) {
//					insertSmsMessage(seller.getLoginname(),content.toString(),SmsMessageType.PAID_SELLER);
//				}
//			} else {
//				return ret;
//			}
//		} else {
//			StringBuffer content = new StringBuffer();
//			content.append("恭喜亲，您的宝贝");
//			List<OrderItem> items = order.getOrderItems();
//			OrderAddress address = order.getOrderAddress();
//			if (items != null){
//				content.append(createProductName(items));
//			}
//			content.append("被成功购买，订单号: ");
//			content.append(getTinyUrl(order.getId()));
//			content.append(" ，买家: ");
//			content.append(address.getConsignee());
//			if (address.getWeixinId() != null){
//				content.append("(微信号: ");
//				content.append(address.getWeixinId());
//				content.append(")");
//			}
//			content.append("已付款，请尽快发货。");
//			if (seller != null && seller.getLoginname() != null){
//				ret = sendSms(seller.getLoginname(),content.toString());
//				if (!ret){
//					insertSmsMessage(seller.getLoginname(),content.toString(),SmsMessageType.PAID_SELLER);
//				}
//			}
//		}
//		return ret;
//	}

  @Override
  public boolean sendBuyerSMSOrderShipped(OrderVO order, User seller, User buyer) {
    boolean ret = false;
    if (order == null || seller == null || buyer == null || seller.getLoginname() == null) {
      return ret;
    }

    String prodName = null;
    String orderInfo = getSubOrderTinyUrl(order.getId());
    List<OrderItem> items = order.getOrderItems();
    if (items != null) {
      prodName = createProductName(items);
    }

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("prodName", prodName);
    otherParams.put("orderInfo", orderInfo);
    String jsonData = createParams(order, seller, buyer, otherParams);

    if (doSendEngine(order, SmsMessageType.SHIPPED_BUYER, order.getPartnerType().toString(),
        jsonData)) {
    }
    return ret;
  }

//	@Override
//	public boolean sendBuyerSMSOrderShipped(OrderVO order, User seller, User buyer) {
//		boolean ret = false;
//		if (order == null || seller == null || buyer == null) return ret;
//		
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("smsEvent", "SHIPPED_BUYER");
//		params.put("status", smsTplSwt);
//		List<SmsTpl> aTpls = smsTplService.getSmsTpl(params);
//		if (aTpls != null && aTpls.size() != 0) {
//			SmsTpl aTpl = aTpls.get(0);
//			String content = null;
//			String prodName = null;
//			String orderInfo  = null;
//			String logisticsInc = "";
//			String logisticsOrderNo = "";
//			List<OrderItem> items = order.getOrderItems();
//			if (items != null)	prodName = createProductName(items);
//			orderInfo = getTinyUrl(order.getId());	
//			if ( order.getLogisticsCompany() != null) {
//				logisticsInc = order.getLogisticsCompany().toString();
//				logisticsOrderNo = order.getLogisticsOrderNo();
//				aTpl = aTpls.get(0);  // TODO: 快递公司与运单号
//			} else {
//				aTpl = aTpls.get(1);  // TODO:
//			}
//			OrderAddress address = order.getOrderAddress();
//			if (address != null) {
//				if ( !smsTplService.checkFmtVars(aTpls.get(0).getFormatVar().split(","))) {
//					return false;
//				}
//				Formatter fmt = new Formatter();
//				try {
//					if (order.getLogisticsCompany() != null)
//						fmt.format(aTpl.getContent(), prodName,orderInfo,logisticsInc,logisticsOrderNo);
//					else
//						fmt.format(aTpl.getContent(), prodName,orderInfo);
//					content = fmt.toString();
//				} finally {
//					fmt.close();
//				}
//				ret = doSend(order, content, aTpl.getSmsEvent(), buyer.getPartner());
//			} else {
//				return ret;
//			}
//		} else {
//			StringBuffer content = new StringBuffer();
//			content.append("亲，您已购买的宝贝");
//			List<OrderItem> items = order.getOrderItems();
//			if (items != null){
//				content.append(createProductName(items));
//			}
//			content.append("，订单号: ");
//			content.append(getTinyUrl(order.getId()));
//			
//			if (order.getLogisticsCompany() == null) {
//				content.append(" ，已经发货，稍后请注意查收。");
//			} else {
//				content.append(" ，正由");
//				content.append(order.getLogisticsCompany().toString());
//				content.append("快马加鞭送来，快递单号: ");
//				content.append(order.getLogisticsOrderNo());
//				content.append("，请注意查收。");
//			}
//	
//			OrderAddress address = order.getOrderAddress();
//			if (address != null && address.getPhone() != null){
//				UserPartnerType userType = UserPartnerType.KKKD;
//				if("xiangqu".equals(buyer.getPartner())){
//					userType = UserPartnerType.XIANGQU;
//				}
//				ret = sendSms(address.getPhone(),content.toString(), userType);
//				if (!ret){
//					insertSmsMessage(address.getPhone(),content.toString(),SmsMessageType.SHIPPED_BUYER);
//				}
//			}
//		}
//		
//		return ret;
//	}

  @Override
  public boolean sendBuyerSMSOrderCancelled(OrderVO order, User seller, User buyer) {
    boolean ret = false;
    if (order == null || seller == null || buyer == null || seller.getLoginname() == null) {
      return ret;
    }

    String prodName = null;
    String orderInfo = getTinyUrl(order.getId());
    List<OrderItem> items = order.getOrderItems();
    if (items != null) {
      prodName = createProductName(items);
    }

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("prodName", prodName);
    otherParams.put("orderInfo", orderInfo);
    String jsonData = createParams(order, seller, buyer, otherParams);

    if (doSendEngine(order, SmsMessageType.CANCELLED_BUYER, order.getPartnerType().toString(),
        jsonData)) {
    }
    return ret;
  }

//	@Override
//	public boolean sendBuyerSMSOrderCancelled(OrderVO order, User seller, User buyer) {
//		boolean ret = false;
//		if (order == null || seller == null || buyer == null) return ret;
//		
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("smsEvent", "CANCELLED_BUYER");
//		params.put("status", smsTplSwt);
//		SmsTpl aTpl = smsTplService.getUniqueTpl(params);
//		if (aTpl != null) {
//			if ( !smsTplService.checkFmtVars(aTpl.getFormatVar().split(","))) {
//				return false;
//			}
//			String content = null;
//			String prodName = null;
//			String orderInfo  = null;
//			
//			List<OrderItem> items = order.getOrderItems();
//			if (items != null)	prodName = createProductName(items);
//			orderInfo = getTinyUrl(order.getId());	
//			
//			Formatter fmt = new Formatter();
//			try {
//				fmt.format(aTpl.getContent(), prodName, orderInfo);
//				content = fmt.toString();
//			} finally {
//				fmt.close();
//			}
//			ret = doSend(order, content, aTpl.getSmsEvent(), buyer.getPartner());
//		} else {
//			StringBuffer content = new StringBuffer();
//			content.append("亲，您已拍下的宝贝");
//			List<OrderItem> items = order.getOrderItems();
//			OrderAddress address = order.getOrderAddress();
//			if (items != null){
//				content.append(createProductName(items));
//			}
//			content.append("，订单号: ");
//			content.append(getTinyUrl(order.getId()));
//			content.append(" ，由于超时未付款，已被系统自动取消。");
//			if (address != null && address.getPhone() != null){
//				UserPartnerType userType = UserPartnerType.KKKD;
//				if("xiangqu".equals(buyer.getPartner())){
//					userType = UserPartnerType.XIANGQU;
//				}
//				ret = sendSms(address.getPhone(),content.toString(), userType);
//				if (!ret){
//					insertSmsMessage(address.getPhone(),content.toString(),SmsMessageType.CANCELLED_BUYER);
//				}
//			}
//		}	
//		return ret;
//	}

  @Override
  public boolean sendSellerSMSOrderCancelled(OrderVO order, User seller, User buyer) {
    boolean ret = false;
    if (order == null || seller == null || buyer == null || seller.getLoginname() == null) {
      return ret;
    }

    String prodName = null;
    String orderInfo = getTinyUrl(order.getId());
    List<OrderItem> items = order.getOrderItems();
    if (items != null) {
      prodName = createProductName(items);
    }

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("prodName", prodName);
    otherParams.put("orderInfo", orderInfo);
    String jsonData = createParams(order, seller, buyer, otherParams);

    if (sendSmsEngine(seller.getPhone(), SmsMessageType.CANCELLED_SELLER, jsonData)) {
      insertSmsMessage(seller.getPhone(), SmsMessageType.CANCELLED_SELLER, jsonData);
    }
    return ret;
  }

  @Override
  public boolean sendSellerSMSOrderRefundRequest(OrderVO order) {
    if (order == null) {
      log.debug("发送退货申请相关的消息的时候，订单不存在");
      return false;
    }

    User seller = userService.load(order.getSellerId());
    User buyer = userService.load(order.getBuyerId());

    boolean ret = false;
    if (seller == null || buyer == null || StringUtils.isBlank(seller.getLoginname())) {
      log.debug("发送退货申请相关的消息的时候，买家/卖家信息不存在");
      return false;
    }

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("orderNo", order.getOrderNo());
    String jsonData = createParams(order, seller, buyer, otherParams);
    if (sendSmsEngine(seller.getPhone(), SmsMessageType.ORDER_REFUND_REQUEST_SELLER, jsonData)) {
      insertSmsMessage(seller.getPhone(), SmsMessageType.ORDER_REFUND_REQUEST_SELLER, jsonData);
    }
    return ret;
  }

  @Override
  public boolean sendBuyerSMSOrderRefundSuccess(OrderVO order) {
    User seller = userService.load(order.getSellerId());
    User buyer = userService.load(order.getBuyerId());

    boolean ret = false;
    if (order == null || seller == null || buyer == null || StringUtils
        .isBlank(seller.getLoginname())) {
      return ret;
    }

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("orderNo", order.getOrderNo());
    String jsonData = createParams(order, seller, buyer, otherParams);

    if (doSendEngine(order, SmsMessageType.ORDER_REFUND_SUCCESS_BUYERER,
        order.getPartnerType().toString(), jsonData)) {
    }

    return ret;
  }

  @Override
  public boolean sendBuyerSMSOrderRefundAgreeReturn(OrderVO order) {
    if (order == null) {
      return false;
    }

    User seller = userService.load(order.getSellerId());
    User buyer = userService.load(order.getBuyerId());
    if (seller == null || buyer == null || StringUtils.isBlank(seller.getLoginname())) {
      return false;
    }

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("orderNo", order.getOrderNo());
    String jsonData = createParams(order, seller, buyer, otherParams);
    doSendEngine(order, SmsMessageType.ORDER_REFUND_AGREE_RETURN_BUYERER,
        order.getPartnerType().toString(), jsonData);
    return false;
  }


  @Override
  public boolean sendBuyerSMSOrderRefundReject(OrderVO order) {
    User seller = userService.load(order.getSellerId());
    User buyer = userService.load(order.getBuyerId());

    boolean ret = false;
    if (order == null || seller == null || buyer == null || seller.getLoginname() == null) {
      return ret;
    }

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("orderNo", order.getOrderNo());
//		otherParams.put("orderInfo", orderInfo);
    String jsonData = createParams(order, seller, buyer, otherParams);

//		if (sendSmsEngine(seller.getLoginname(),SmsMessageType.ORDER_REFUND_REJECT_BUYERER, jsonData)) {
//			insertSmsMessage(seller.getLoginname(),SmsMessageType.ORDER_REFUND_REJECT_BUYERER, jsonData);
//		}

    if (doSendEngine(order, SmsMessageType.ORDER_REFUND_REJECT_BUYERER,
        order.getPartnerType().toString(), jsonData)) {
    }
    return ret;
  }

//	@Override
//	public boolean sendSellerSMSOrderCancelled(OrderVO order, User seller, User buyer) {
//		boolean ret = false;
//		if (order == null || seller == null || buyer == null || seller.getLoginname() == null) return ret;
//		
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("smsEvent", "CANCELLED_SELLER");
//		params.put("status", smsTplSwt);
//		SmsTpl aTpl = smsTplService.getUniqueTpl(params);
//		if (aTpl != null) {
//			if ( !smsTplService.checkFmtVars(aTpl.getFormatVar().split(","))) {
//				return false;
//			}
//			String content = null;
//			String prodName = null;
//			String orderInfo  = null;
//			List<OrderItem> items = order.getOrderItems();
//			if (items != null)	prodName = createProductName(items);
//			orderInfo = getTinyUrl(order.getId());	
//			
//			Formatter fmt = new Formatter();
//			try {
//				fmt.format(aTpl.getContent(), prodName, orderInfo);
//				content = fmt.toString();
//			} finally {
//				fmt.close();
//			}
//			if (sendSms(seller.getLoginname(),content.toString()) ) {
//				insertSmsMessage(seller.getLoginname(),content.toString(),SmsMessageType.CANCELLED_SELLER);
//			}
//		} else {
//			StringBuffer content = new StringBuffer();
//			content.append("亲，您已被拍下的宝贝");
//			List<OrderItem> items = order.getOrderItems();
//			if (items != null){
//				content.append(createProductName(items));
//			}
//			content.append("，订单号: ");
//			content.append(getTinyUrl(order.getId()));
//			content.append(" ，由于买家超时未付款，已被系统自动取消。");
//			if (seller != null && seller.getLoginname() != null){
//				ret = sendSms(seller.getLoginname(),content.toString());
//				if (!ret){
//					insertSmsMessage(seller.getLoginname(),content.toString(),SmsMessageType.CANCELLED_SELLER);
//				}
//			}
//		}	
//		return ret;
//	}

  @Override
  public boolean sendBuyerSMSOrderFree(OrderVO order, UserPartnerType userType) {
    boolean ret = false;
    if (order == null) {
      return ret;
    }

    String prodName = null;
    List<OrderItem> items = order.getOrderItems();
    if (items != null) {
      prodName = createProductName(items);
    }
    String orderInfo = getTinyUrl(order.getId());
    String freeFee = cashierService.loadPaidFee(order.getOrderNo()).toString();

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("prodName", prodName);
    otherParams.put("orderInfo", orderInfo);
    otherParams.put("freeFee", freeFee);
    String jsonData = createParams(order, null, null, otherParams);

    if (doSendEngine(order, SmsMessageType.FREE_ORDER, userType.toString(), jsonData)) {
    }
    return ret;
  }

//	@Override
//	public boolean sendBuyerSMSOrderFree(OrderVO order, UserPartnerType userType) {
//		if (order == null) return false;
//		boolean ret = false;
//		
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("smsEvent", "FREE_ORDER");
//		params.put("status", smsTplSwt);
//		SmsTpl aTpl = smsTplService.getUniqueTpl(params);
//		if (aTpl != null) {
//			if ( !smsTplService.checkFmtVars(aTpl.getFormatVar().split(","))) {
//				return false;
//			}
//			String content = null;
//			String orderInfo  = null;
//			String freeFee = null;
//			orderInfo = getTinyUrl(order.getId());	
//			freeFee = cashierService.loadPaidFee(order.getOrderNo()).toString();
//			
//			Formatter fmt = new Formatter();
//			try {
//				fmt.format(aTpl.getContent(), orderInfo, freeFee);
//				content = fmt.toString();
//			} finally {
//				fmt.close();
//			}
//			ret = doSend(order, content, aTpl.getSmsEvent(), UserPartnerType.XIANGQU.toString());
//		} else {
//			StringBuffer content = new StringBuffer();
//			OrderAddress address = order.getOrderAddress();
//			
//			content.append("恭喜你获得了想去免单奖励！免单订单号： ");
//			content.append(order.getOrderNo());
//			content.append(" ，免单金额： ");
//			content.append(cashierService.loadPaidFee(order.getOrderNo()).toString());
//			content.append("元将在7天后打到您的账户！继续购买赢更多惊喜哟！");
//			if (address != null && address.getPhone() != null){
//				ret = sendSms(address.getPhone(), content.toString(), userType);
//				if (!ret){
//					insertSmsMessage(address.getPhone(),content.toString(),SmsMessageType.FREE_ORDER);
//				}
//			}
//		}
//		return ret;
//	}

  @Override
  public boolean sendBuyerSMSOrderFreeFinish(OrderVO order, UserPartnerType userType) {
    boolean ret = false;
    if (order == null) {
      return ret;
    }

    String prodName = null;
    List<OrderItem> items = order.getOrderItems();
    if (items != null) {
      prodName = createProductName(items);
    }
    String orderInfo = getTinyUrl(order.getId());
    String freeFee = cashierService.loadPaidFee(order.getOrderNo()).toString();

    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("prodName", prodName);
    otherParams.put("orderInfo", orderInfo);
    otherParams.put("freeFee", freeFee);
    String jsonData = createParams(order, null, null, otherParams);

    if (doSendEngine(order, SmsMessageType.FREE_ORDER_FINISH, userType.toString(), jsonData)) {
    }
    return ret;
  }

//	@Override
//	public boolean sendBuyerSMSOrderFreeFinish(OrderVO order, UserPartnerType userType) {
//		boolean ret = false;
//		if (order == null) return ret;
//		
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("smsEvent", "SUBMITTED_BUYER");
//		params.put("status", smsTplSwt);
//		SmsTpl aTpl = smsTplService.getUniqueTpl(params);
//		if (aTpl != null) {
//			if ( !smsTplService.checkFmtVars(aTpl.getFormatVar().split(","))) {
//				return false;
//			}
//			String content = null;
//			String orderInfo  = null;
//			String freeFee = null;
//			orderInfo = getTinyUrl(order.getId());	
//			freeFee = cashierService.loadPaidFee(order.getOrderNo()).toString();
//			
//			Formatter fmt = new Formatter();
//			try {
//				fmt.format(aTpl.getContent(), orderInfo, freeFee);
//				content = fmt.toString();
//			} finally {
//				fmt.close();
//			}
//			ret = doSend(order, content, aTpl.getSmsEvent(),  UserPartnerType.XIANGQU.toString());
//		} else {
//			StringBuffer content = new StringBuffer();
//			OrderAddress address = order.getOrderAddress();
//			
//			content.append("你在想去的双十一免单订单号：");
//			content.append(order.getOrderNo());
//			content.append(" ，免单金额： ");
//			content.append(cashierService.loadPaidFee(order.getOrderNo()));
//			content.append("元已打入你的账户，请注意查收哦~");
//			
//			if (address != null && address.getPhone() != null){
//				ret = sendSms(address.getPhone(), content.toString(), userType);
//				if (!ret){
//					insertSmsMessage(address.getPhone(),content.toString(),SmsMessageType.FREE_ORDER_FINISH);
//				}
//			}
//		}
//		return ret;
//	}

//	private boolean sendSms(String mobile,String content){
//		return sendSms(mobile, content, UserPartnerType.KKKD);
//	}
//	
//	private boolean sendSms(String mobile,String content, UserPartnerType userType){
//		return pushService.sendSms(mobile, content, userType);
//	}

  private boolean sendSmsEngine(String mobile, SmsMessageType smt, String paramsDatas) {
    return sendSmsEngine(mobile, smt, UserPartnerType.KKKD, paramsDatas);
  }

  private boolean sendSmsEngine(String mobile, SmsMessageType smt, UserPartnerType userType,
      String paramsDatas) {
    return pushService.sendSmsEngine(mobile, smt.name(), userType, paramsDatas, "");
  }

  /**
   * 获取订单ID的短地址
   */
  private String getTinyUrl(String orderId) {
    String url = domainName + "/order/" + orderId;
    String key = tinyUrlService.insert(url);
    if (domainName.equals(tinyUrlDomainName)) {
      return tinyUrlDomainName + "/t/" + key;
    } else {
      return tinyUrlDomainName + "/t/" + key;
    }
  }

  /**
   * 获取子订单ID的短地址
   */
  private String getSubOrderTinyUrl(String orderId) {
    String url = domainName + "/suborder/" + orderId;
    String key = tinyUrlService.insert(url);
    if (domainName.equals(tinyUrlDomainName)) {
      return tinyUrlDomainName + "/t/" + key;
    } else {
      return tinyUrlDomainName + "/t/" + key;
    }
  }

  private String createProductName(List<OrderItem> items) {
    StringBuffer ret = new StringBuffer();
    String productName = null;
    if (items.size() > 1) {
      productName = items.get(0).getProductName();
      if (productName.length() > length) {
        ret.append(productName.substring(0, length));
        ret.append("...");
      } else {
        ret.append(productName);
      }
      ret.append("等多件");
    } else {
      productName = items.get(0).getProductName();
      if (productName.length() > length) {
        ret.append(productName.substring(0, length));
        ret.append("...");
      } else {
        ret.append(productName);
      }
    }
    return ret.toString();
  }

  private int insertSmsMessage(String phone, SmsMessageType smt, String paramsMap) {
    SmsMessage message = new SmsMessage();
    message.setPhone(phone);
    message.setContent(smt + ":" + paramsMap);
    message.setStatus(SmsMessageStatus.FAIL);
    message.setType(smt);
    message.setCount(0);
    return smsMessageService.insert(message);
  }

  @Override
  public boolean sendBuyerSMSOrderDelayRemind(OrderVO order, User seller, User buyer) {

    boolean ret = false;
    if (order == null || seller == null || buyer == null) {
      return ret;
    }
    String orderInfo = getTinyUrl(order.getId());
    Map<String, String> otherParams = new HashMap<String, String>();
    otherParams.put("orderInfo", orderInfo);
    String jsonData = createParams(order, seller, buyer, otherParams);

    ret = doSendEngine(order, SmsMessageType.DELAYSIGN_EXPIRE_REMIND, buyer.getPartner(), jsonData);
    return ret;
  }


  public static void main(String[] args) {

  }
}
