package com.xquark.service.order.impl;


import static java.math.BigDecimal.ROUND_DOWN;
import static java.math.BigDecimal.ROUND_HALF_UP;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Lists;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderDeliveryDestination;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Sku;
import com.xquark.dal.type.OrderType;
import com.xquark.dal.type.ProductFeaturesKeyType;
import com.xquark.dal.type.SkuCodeResourcesType;
import com.xquark.dal.type.SkuType;
import com.xquark.service.category.CategoryService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderSplitterService;
import com.xquark.service.order.util.cache.FixedTimeGuavaCache;
import com.xquark.service.product.ProductService;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/** 目前仅允许单店铺内拆分订单 Created by dongsongjie on 15/11/16. */
@Service("orderSplitterService")
@SuppressWarnings("Duplicates")
public class OrderSplitterServiceImpl extends BaseServiceImpl implements OrderSplitterService {

  private ImmutableSortedSet<OrderDeliveryCondition> conditions;

  @Value("${wdt.warehouseNO}")
  private String warehouseNO;

  @Value("${wdt.shopName}")
  private String shopName;

  @Value("${order.send.erp.on-off}")
  private boolean sendErpSwitchOn;

  @Autowired private ProductService productService;

  @Autowired private CategoryService categoryService;

  @Autowired
  private FixedTimeGuavaCache fixedTimeGuavaCache;


  @PostConstruct
  public void init() {
    //noinspection unchecked
    conditions = fixedTimeGuavaCache.getOrderDeliveryConditionProvider();
    log.info("init conditions : {}",conditions);
  }


  @Override
  public Map<Order, List<OrderItem>> split(Map<Order, List<OrderItem>> orders) {
    Preconditions.checkNotNull(orders);
    Preconditions.checkArgument(orders.size() > 0, "splitting orders is empty.");

    Map<Order, List<OrderItem>> ret = new HashMap<>();
    Map<Order, List<OrderItem>> splittedOrders;
    Iterator<Map.Entry<Order, List<OrderItem>>> iterator = orders.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry<Order, List<OrderItem>> entry = iterator.next();
      Order order = entry.getKey();
      List<OrderItem> orderItems = entry.getValue();
      // 按店铺内orderItems拆分订单
      // insource shop go to another path
      if (sendErpSwitchOn) {
        splittedOrders = split0(order, orderItems);
      } else {
        splittedOrders = orders;
      }
      ret.putAll(splittedOrders);
    }
    return ret;
  }

  private Map<Order, List<OrderItem>> split0(Order orgi, List<OrderItem> orgiOrderItems) {
    //noinspection unchecked
    conditions = fixedTimeGuavaCache.getOrderDeliveryConditionProvider();
    Preconditions.checkArgument(conditions != null,
        "dest conditions collection initialized with error.");
    Preconditions.checkArgument(orgi != null);
    Preconditions.checkArgument(orgiOrderItems.size() > 0, "orgi item size equals zero.");

    Map<OrderDeliveryDestination, List<OrderItem>> destOrderItems = new HashMap<>();
    Map<Order, List<OrderItem>> ret = new HashMap<>();

    for (OrderItem orderItem : orgiOrderItems) {
      MappingContext context = new MappingContext();
      Sku sku = productService.loadSku(orderItem.getSkuId());

      Product product = productService.load(orderItem.getProductId());
      orderItem.setProduct(product);
      context.setNotNeedToSend(product.getFeature(ProductFeaturesKeyType.NOT_SEND_TO_ERP));
      context.setIsCrossBorder(product.getIsCrossBorder() == null ? 0 : product.getIsCrossBorder());
      context.setOrderType(OrderType.DANBAO);
      context.setShopId(product.getShopId());
      context.setSkuCodeResourcesType(SkuCodeResourcesType
          .valueResOf(sku.getSkuCodeResources()));
      context.setRefundAble(product.getSupportRefund());

      if (categoryService.loadCategoryByProductId(product.getId()) != null) {
        context.setCategoryId(categoryService.loadCategoryByProductId(product.getId()).getId());
      } else {
        context.setCategoryId("");
      }

      OrderDeliveryDestination dest = null;

      for (OrderDeliveryCondition condition : conditions) {
        dest = condition.meet(context);
        if (dest != null) {
          break;
        }
      }
      if (dest == null) {
        log.info("no such context , please check db {} existed",context);
        throw new BizException(
            GlobalErrorCode.UNKNOWN, "orgi destination not found for item:" + orderItem.getSkuId());
      }
      log.info("dest after split " + dest.getSkuCodeResourcesType());

      List<OrderItem> items = destOrderItems.get(dest);
      if (items == null) {
        destOrderItems.put(dest, Lists.newArrayList(orderItem));
      } else {
        items.add(orderItem);
      }
    }

    // 重新生成order并计算所有fee
    int size = destOrderItems.size();
    int index = 0;

    if (size == 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "destOrderItems size is zero.");
    }

    if (size == 1) {
      OrderDeliveryDestination dest = destOrderItems.keySet().iterator().next();
      orgi.setDestName(dest.getName());
      orgi.setDest(dest.getDest());
      orgi.setWarehouseName(dest.getWarehouseName());
      orgi.setShopName(dest.getShopName());
      orgi.setSource(dest.getSource());
      ret.put(orgi, orgiOrderItems);
    } else {
      BigDecimal discountFee = BigDecimal.ZERO;
      BigDecimal logisticFee = BigDecimal.ZERO;
      BigDecimal logisticDiscount = BigDecimal.ZERO;
      BigDecimal commissionLogisticsDiscount = BigDecimal.ZERO;
      for (Map.Entry<OrderDeliveryDestination, List<OrderItem>> entry : destOrderItems.entrySet()) {
        OrderDeliveryDestination d = entry.getKey();
        List<OrderItem> i = entry.getValue();
        index++;

//        Order splittedOrder = new Order();
        Order splittedOrder = (Order) orgi.clone();
        splittedOrder.setTotalFee(null);
        splittedOrder.setGoodsFee(null);
        splittedOrder.setLogisticsFee(null);
        splittedOrder.setDiscountFee(null);
//        BeanUtils.copyProperties(
//            orgi,
//            splittedOrder,
//            "totalFee",
//            "goodsFee",
//            "logisticsFee",
//            "discountFee"); // ignore orginal fee for security reason
        if (index != size) {
          reCalculate(
              splittedOrder,
              orgi,
              i,
              BigDecimal.ZERO,
              BigDecimal.ZERO,
              BigDecimal.ZERO,
              BigDecimal.ZERO);
          discountFee = discountFee.add(splittedOrder.getDiscountFee()); // 计算除最后一个order以外的优惠值
        } else {
          // 最后一笔精度补偿
          reCalculate(
              splittedOrder,
              orgi,
              i,
              discountFee,
              logisticFee,
              logisticDiscount,
              commissionLogisticsDiscount);
        }
        splittedOrder.setSource(d.getSource());
        splittedOrder.setDestName(d.getName());
        splittedOrder.setDest(d.getDest());
        splittedOrder.setWarehouseName(d.getWarehouseName());
        splittedOrder.setShopName(d.getShopName());
        ret.put(splittedOrder, i);
      }
      // 检查拆单结果
      checkResult(orgi, orgiOrderItems, ret);
    }
    return ret;
  }

  private void checkResult(
      Order orgi, List<OrderItem> orgiOrderItems, Map<Order, List<OrderItem>> ret) {
    // 金额校验
    BigDecimal spTotalFee = new BigDecimal(0);
    BigDecimal spDiscountFee = new BigDecimal(0);
    BigDecimal spGoodsFee = new BigDecimal(0);

    for (Order order : ret.keySet()) {
      spTotalFee = spTotalFee.add(order.getTotalFee());
      spDiscountFee = spDiscountFee.add(order.getDiscountFee());
      spGoodsFee = spGoodsFee.add(order.getGoodsFee());
    }

    if (spTotalFee.compareTo(orgi.getTotalFee()) != 0
        || spDiscountFee.compareTo(orgi.getDiscountFee()) != 0
        || spGoodsFee.compareTo(orgi.getGoodsFee()) != 0) {
      log.warn(
          "订单费用计算错误:"
              + spTotalFee
              + ":"
              + orgi.getTotalFee()
              + "|"
              + spDiscountFee
              + ":"
              + orgi.getDiscountFee()
              + "|"
              + spGoodsFee
              + ":"
              + orgi.getGoodsFee());

      checkResult(spTotalFee, orgi.getTotalFee());
      checkResult(spDiscountFee, orgi.getDiscountFee());
      checkResult(spGoodsFee, orgi.getGoodsFee());
    }
  }

  private void checkResult(BigDecimal target, BigDecimal source) {
    if (target.subtract(source).abs().compareTo(BigDecimal.valueOf(0.02)) > 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "订单费用计算错误:" + target + ":" + source);
    }
  }

  /**
   * 目前仅考虑店铺级优惠,优惠券等等，根据拆分订单goodsfee占比重新计算totalFee, discountFee logisticsFee
   * 一般是全场包邮，暂不考虑此费用，拆单时复制该值，可能导致最终价格高于用户在下单页面看到的价格
   */
  private void reCalculate(
      Order splittedOrder,
      Order orgi,
      List<OrderItem> i,
      BigDecimal discountFeeExpLast,
      BigDecimal logisticsFeeExpLast,
      BigDecimal logisticsDiscountExpLast,
      BigDecimal commLogiDiscountExpLast) {
    BigDecimal orgiGoodsFee = orgi.getGoodsFee();
    BigDecimal soGoodsFee = BigDecimal.ZERO;
    // 拆单后item德分
    BigDecimal paidPoint = BigDecimal.ZERO;
    // 拆单后item德分
    BigDecimal paidPointPacket = BigDecimal.ZERO;
    // 拆单后item积分
    BigDecimal paidCommission = BigDecimal.ZERO;
    // 拆单后item立减
    BigDecimal reduction = BigDecimal.ZERO;

    for (OrderItem orderItem : i) {
      soGoodsFee =
          soGoodsFee.add(orderItem.getPrice().multiply(new BigDecimal(orderItem.getAmount())));
      // 重新计算积分、德分
      paidPoint = paidPoint.add(orderItem.getPaidPoint());
      paidPointPacket = paidPointPacket.add(orderItem.getPaidPointPacket());
      paidCommission = paidCommission.add(orderItem.getPaidCommission());
      reduction = reduction.add(orderItem.getReduction());
    }

    BigDecimal proportion = soGoodsFee.divide(orgiGoodsFee, 8, ROUND_DOWN);
    splittedOrder.setGoodsFee(soGoodsFee);
    splittedOrder.setPaidPoint(paidPoint);
    splittedOrder.setPaidPointPacket(paidPointPacket);
    splittedOrder.setPaidCommission(paidCommission);
    splittedOrder.setReduction(reduction);

    if (BigDecimal.ZERO.compareTo(discountFeeExpLast) == 0) {
      splittedOrder.setDiscountFee(
          orgi.getDiscountFee().multiply(proportion).setScale(2, ROUND_HALF_UP));
    } else {
      splittedOrder.setDiscountFee(
          orgi.getDiscountFee().subtract(discountFeeExpLast)); // 在最后一笔拆单中补偿可能丢失的精度
    }

    if (BigDecimal.ZERO.compareTo(logisticsFeeExpLast) == 0) {
      splittedOrder.setLogisticsFee(
          orgi.getLogisticsFee().multiply(proportion).setScale(2, ROUND_HALF_UP));
    } else {
      splittedOrder.setLogisticsFee(orgi.getLogisticsFee().subtract(logisticsFeeExpLast));
    }

    if (BigDecimal.ZERO.compareTo(logisticsDiscountExpLast) == 0) {
      splittedOrder.setLogisticDiscount(
          orgi.getLogisticDiscount().multiply(proportion).setScale(2, ROUND_HALF_UP));
    } else {
      splittedOrder.setLogisticDiscount(
          orgi.getLogisticDiscount().subtract(logisticsDiscountExpLast));
    }

    if (BigDecimal.ZERO.compareTo(commLogiDiscountExpLast) == 0) {
      splittedOrder.setCommissionLogisticsDiscount(
          orgi.getCommissionLogisticsDiscount().multiply(proportion).setScale(2, ROUND_HALF_UP));
    } else {
      splittedOrder.setCommissionLogisticsDiscount(
          orgi.getCommissionLogisticsDiscount().subtract(commLogiDiscountExpLast));
    }

    splittedOrder.setTotalFee(
        soGoodsFee
            .add(splittedOrder.getLogisticsFee())
            .subtract(splittedOrder.getDiscountFee())
            .add(splittedOrder.getCommissionLogisticsDiscount()));
  }

  private boolean checkInSourceRule(
          String shopId, String categoryName, SkuType skuCodeResourcesType) {
    if (null != shopId && null != categoryName) {
      if (shopId.equals("8s1ukmmd")
          && categoryName.equals("次品")
          && "ECMOHO".equals(skuCodeResourcesType.getName())) {
        return true;
      }
    }
    return false;
  }

  public String getWarehouseNO() {
    return warehouseNO;
  }

  public void setWarehouseNO(String warehouseNO) {
    this.warehouseNO = warehouseNO;
  }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  /** 判断推送订单的上下文 */
  public static class MappingContext {

    private boolean isCrossBorder;

    private String shopId;

    private OrderType orderType;

    private SkuType skuCodeResourcesType;

    private String categoryId;
    // special rule to inSource shop
    private boolean notNeedToSend;

    private boolean refundAble;

    public boolean getNotNeedToSend() {
      return notNeedToSend;
    }

    public void setNotNeedToSend(boolean notNeedToSend) {
      this.notNeedToSend = notNeedToSend;
    }

    public boolean isRefundAble() {
      return refundAble;
    }

    public void setRefundAble(boolean refundAble) {
      this.refundAble = refundAble;
    }

    public boolean isCrossBorder() {
      return isCrossBorder;
    }

    public void setIsCrossBorder(Integer isCrossBorder) {
      switch (isCrossBorder) {
        case 0:
          this.isCrossBorder = false;
          break;
        case 1:
          this.isCrossBorder = true;
          break;
        default:
          throw new IllegalArgumentException("isCrossBorder status not accepted:" + isCrossBorder);
      }
    }

    public String getShopId() {
      return shopId;
    }

    public void setShopId(String shopId) {
      this.shopId = shopId;
    }

    public OrderType getOrderType() {
      return orderType;
    }

    public void setOrderType(OrderType orderType) {
      this.orderType = orderType;
    }

    public SkuType getSkuCodeResourcesType() {
      return skuCodeResourcesType;
    }

    public void setSkuCodeResourcesType(SkuType skuCodeResourcesType) {
      this.skuCodeResourcesType = skuCodeResourcesType;
    }

    public String getCategoryId() {
      return categoryId;
    }

    public void setCategoryId(String categoryId) {
      this.categoryId = categoryId;
    }

    @Override
    public String toString() {
      return "MappingContext{" +
              "isCrossBorder=" + isCrossBorder +
              ", shopId='" + shopId + '\'' +
              ", orderType=" + orderType +
              ", skuCodeResourcesType=" + skuCodeResourcesType +
              ", categoryId='" + categoryId + '\'' +
              ", notNeedToSend=" + notNeedToSend +
              ", refundAble=" + refundAble +
              '}';
    }
  }

  public  static interface OrderDeliveryCondition {

    OrderDeliveryDestination meet(MappingContext context);

    Integer order();
  }

  public static   class OrderDeliveryConditionImpl implements OrderDeliveryCondition {

    @Override
    public OrderDeliveryDestination meet(MappingContext context) {
      return null;
    }

    @Override
    public Integer order() {
      return null;
    }

  }
}






