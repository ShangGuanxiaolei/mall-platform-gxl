package com.xquark.service.order;

import com.xquark.dal.model.OrderUserDevice;

public interface OrderUserDeviceService {

  OrderUserDevice save(OrderUserDevice orderUserDevice);

}
