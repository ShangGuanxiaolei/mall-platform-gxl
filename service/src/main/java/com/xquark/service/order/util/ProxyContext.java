package com.xquark.service.order.util;

import com.xquark.dal.model.Supplier;
import java.lang.reflect.Method;

/**
 * @author jackzhu
 * @since 2018/11/16
 **/
public class ProxyContext {

  private Object proxy;
  private Method method;
  private Object[] args;
  private Supplier supplier;
  private String backAble;

  public ProxyContext(Object proxy, Method method, Object[] args,
      Supplier supplier, String backAble) {
    this.proxy = proxy;
    this.method = method;
    this.args = args;
    this.supplier = supplier;
    this.backAble = backAble;
  }

  public Supplier getSupplier() {
    return supplier;
  }

  public void setSupplier(Supplier supplier) {
    this.supplier = supplier;
  }

  public String getBackAble() {
    return backAble;
  }

  public void setBackAble(String backAble) {
    this.backAble = backAble;
  }

  public Object getProxy() {
    return proxy;
  }

  public void setProxy(Object proxy) {
    this.proxy = proxy;
  }

  public Method getMethod() {
    return method;
  }

  public void setMethod(Method method) {
    this.method = method;
  }

  public Object[] getArgs() {
    return args;
  }

  public void setArgs(Object[] args) {
    this.args = args;
  }
}
