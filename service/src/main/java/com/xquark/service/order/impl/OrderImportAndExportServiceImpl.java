package com.xquark.service.order.impl;

import com.xquark.dal.mapper.OrderItemMapper;
import com.xquark.dal.model.Customerluckylist;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sound.midi.SoundbankResource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/5/24
 * Time: 11:11
 * Description: 订单导入导出
 */
@Service
public class OrderImportAndExportServiceImpl {
     @Autowired
     private OrderItemMapper  orderItemMapper;


     /*
      * @Author chp
      * @Description //获取订单列表
      * @Date
      * @Param
      * @return
      **/
     public List<Map<String,Object>>   getOrderList(
             String   orderStatus, //订单状态
             String   thirdOrderNo,//第三方订单号
             String   hvOrderNo,//hv订单号
             String   source ,//source
             Date startTime,//查询起始时间
             Date   endTime , //查询结束时间
             Pageable page){
         return    orderItemMapper.getOrderList(orderStatus,thirdOrderNo,hvOrderNo,source,startTime,endTime,page);
     }

      public int getOrderListCount(
              String   orderStatus, //订单状态
              String   thirdOrderNo,//第三方订单号
              String   hvOrderNo,//hv订单号
              String   source ,//source
              Date startTime,//查询起始时间
              Date   endTime , //查询结束时间
              Pageable page

      ){
         return  orderItemMapper.getOrderListCount(orderStatus,thirdOrderNo,hvOrderNo,source,startTime,endTime,page);
      }


    /*
     * @Author chp
     * @Description //查询erp物流详情
     * @Date
     * @Param
     * @return
     **/
    public   List<Map<String,Object>> getInfoListErp(
         String lot,Pageable page
    ){
         return orderItemMapper.getInfoListErp(lot, page);
    };

    /*
     * @Author chp
     * @Description //查询erp物流详情数量
     * @Date
     * @Param
     * @return
     **/
     public int  getInfoListErpCount(
            String lot
    ){
        return orderItemMapper.getInfoListErpCount(lot);
    };


     /*
      * @Author chp
      * @Description //查询导入详情过期
      * @Date
      * @Param
      * @return
      **/
     public List<Map<String,Object>>  getInfoDetail(String lot){
          return orderItemMapper.getInfoDetail(lot);
     }


     public  List<Map<String,Object>> getCheckList(
             String   orderStatus, //订单状态
             String   source ,//source
             Date startTime,//查询起始时间
             Date   endTime , //查询结束时间
             Pageable page
     ){

          return  orderItemMapper. getCheckList(
                  orderStatus,
                  source,
                  startTime,
                  endTime,
                  page);
     }
    public  int getCheckListCount(
            String   orderStatus, //订单状态
            String   source ,//source
            Date startTime,//查询起始时间
            Date   endTime , //查询结束时间
            Pageable page
    ){

        return  orderItemMapper. getCheckListCount(
                orderStatus,
                source,
                startTime,
                endTime,
                page);
    }

     public int updateCheck(String[] lot,String status){
       return    orderItemMapper.updateCheck(lot, status);
     }


     /*
      * @Author chp
      * @Description  查询日志列表
      * @Date  
      * @Param 
      * @return 
      **/
     public List<Map<String,Object>> getInfoList(String result,Date startTime,Date endTime,Pageable page){
         System.out.println("状态");
     return   orderItemMapper.getInfoList(result,startTime, endTime, page);
     }
     public int getInfoListCount(String result,Date startTime,Date endTime,Pageable page){
        return   orderItemMapper.getInfoListCount(result, startTime, endTime, page);
    }


     /*
   * @Author chp
   * @Description // 第三方物流erp导出
   * @Date
   * @Param
   * @return
   **/
    public List<Map<String,Object>> getErpWl(String[] orderList){
       return   orderItemMapper.getErpWl(orderList);
    }

   /*
    * @Author chp
    * @Description  查询订单是否有重复记录
    * @Date
    * @Param
    * @return
    **/
    public  List<String> getThirdItemList(List<String> orderList){
        return  orderItemMapper.getThirdItemList(orderList);
    }
    /*
     * @Author chp
     * @Description //查询sku是否存在
     * @Date
     * @Param
     * @return
     **/
    public  List<String> getSkuList(List<String> skuList){

        return  orderItemMapper.getSkuList(skuList);

    }
    public int updateStatus(String[] lots) {
        int i = orderItemMapper.updateOrderErp(lots);
        int i1 = orderItemMapper.updateOrderErpMain(lots);
    if(0==i||0==i1){
        return 0;
    }

    return  1;
    }

}
