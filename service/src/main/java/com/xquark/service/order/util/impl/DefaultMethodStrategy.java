package com.xquark.service.order.util.impl;

import com.xquark.service.order.util.AbstractProxyMethodStrategy;
import com.xquark.service.order.util.ProxyContext;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author jackzhu
 * @since 2018/11/16
 **/
public class DefaultMethodStrategy extends AbstractProxyMethodStrategy {

  public DefaultMethodStrategy(ProxyContext proxyContext) {
    super(proxyContext);
  }

  @Override
  public Object execute() {
    final Method method = proxyContext.getMethod();
    final Object[] args = proxyContext.getArgs();
    //其他方法
    Object result = null;
    try {
      result = method.invoke(ORDER_DELIVERY_CONDITION, args);
    } catch (InvocationTargetException | IllegalAccessException e) {
      logger.info(e.getMessage());
    }
    return result;
  }
}
