package com.xquark.service.order.util.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;
import com.xquark.dal.mapper.SupplierMapper;
import com.xquark.dal.model.Supplier;
import com.xquark.service.order.impl.OrderSplitterServiceImpl.OrderDeliveryCondition;
import com.xquark.service.order.util.OrderDeliveryConditionProxyFactory;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Jack Zhu
 * @since 2018-10-19
 **/
@Component
public class FixedTimeGuavaCache {

  private final Logger logger = LoggerFactory.getLogger(FixedTimeGuavaCache.class);
  @Autowired
  private SupplierMapper supplierMapper;

  @SuppressWarnings("UnstableApiUsage")
  private final LoadingCache<Object, ImmutableSortedSet> loadingCache = CacheBuilder.newBuilder()
      .refreshAfterWrite(5, TimeUnit.SECONDS)
      .build(CacheLoader.from(this::getProvider));

  public ImmutableSortedSet getOrderDeliveryConditionProvider() {
    try {
      return loadingCache.get("");

    } catch (ExecutionException e) {
      logger.debug("获取的值异常:{}",e.getMessage());
    }
    logger.info("供应商列表:{}" + loadingCache);
    return null;
  }

  public ImmutableSortedSet getProvider() {
    Set<OrderDeliveryCondition> set = Sets.newHashSet();
    //从数据库中查出所有的供应商
    List<Supplier> suppliers = supplierMapper.selectAll();
    for (Supplier supplier : suppliers) {
      //通过代理类生成可退条件判断
      OrderDeliveryCondition enableBackOderDeliveryCondition = OrderDeliveryConditionProxyFactory
          .getEnableBackOderDeliveryCondition(supplier);
      //通过代理类生成不可退条件判断
      OrderDeliveryCondition unableBackOderDeliveryCondition = OrderDeliveryConditionProxyFactory
          .getUnableBackOderDeliveryCondition(supplier);
      set.add(enableBackOderDeliveryCondition);
      set.add(unableBackOderDeliveryCondition);
    }

    return ImmutableSortedSet.orderedBy((Comparator<OrderDeliveryCondition>) (o1, o2) -> o1.order() - o2.order()).addAll(set).build();
  }

}