package com.xquark.service.order;

import java.util.List;

import com.xquark.dal.model.OrderRefundAttach;
import com.xquark.service.BaseEntityService;

public interface OrderRefundAttachService extends BaseEntityService<OrderRefundAttach> {

  List<OrderRefundAttach> listByRefundId(String refundId);

}
