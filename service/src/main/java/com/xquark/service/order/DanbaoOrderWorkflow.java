package com.xquark.service.order;

import com.xquark.dal.mapper.OrderItemMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.User;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.OrderActionType;
import com.xquark.dal.type.PromotionType;
import com.xquark.event.OrderActionEvent;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.groupon.ActivityGrouponService;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 担保交易工作流
 *
 * @author odin
 * @author ahlon
 */
public class DanbaoOrderWorkflow extends OrderWorkflow {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ActivityGrouponService activityGrouponService;

    @Autowired
    private OrderService orderService;

    private Logger log = LoggerFactory.getLogger(getClass());

    private OrderStatus[] statuses = new OrderStatus[]{
            OrderStatus.SUBMITTED,
            OrderStatus.CANCELLED,
            OrderStatus.PAID,
            OrderStatus.SHIPPED,
            OrderStatus.SUCCESS,
            OrderStatus.CHANGING,
            OrderStatus.REFUNDING,
            OrderStatus.REISSUING,
            OrderStatus.CLOSED
    };

    private OrderActionType[] actions = new OrderActionType[]{
            OrderActionType.SUBMIT,
            OrderActionType.CANCEL,
            OrderActionType.PAY,
            OrderActionType.SHIP,
            OrderActionType.SIGN,
            OrderActionType.REQUEST_REFUND,
            OrderActionType.ACCEPT_REFUND,
            OrderActionType.REJECT_REFUND,
            OrderActionType.REQUEST_CHANGE,
            OrderActionType.ACCEPT_CHANGE,
            OrderActionType.REJECT_CHANGE,
            OrderActionType.REQUEST_REISSUE,
            OrderActionType.ACCEPT_REISSUE,
            OrderActionType.REJECT_REISSUE,
    };

    @Override
    public OrderActionType[] getNextAction(Order order, User user) {
        OrderStatus status = order.getStatus();
        switch (status) {
            case SUBMITTED: // 已提交
                if (isBuyer(order, user)) {
                    return new OrderActionType[]{OrderActionType.CANCEL, OrderActionType.PAY};
                }
                break;
            case CANCELLED: // 取消
                break;
            case PAID: // 已付款
                if (isSeller(order, user)) {
                    return new OrderActionType[]{OrderActionType.SHIP, OrderActionType.REFUND};
                }
                break;
            case SHIPPED: // 已发货
                if (isBuyer(order, user)) {
                    return new OrderActionType[]{OrderActionType.SIGN, OrderActionType.REQUEST_REFUND,
                            OrderActionType.REFUND};
                }
                break;
            case REFUNDING: // 交易成功
                if (isSeller(order, user)) {
                    return new OrderActionType[]{OrderActionType.ACCEPT_REFUND,
                            OrderActionType.REJECT_REFUND};
                }
                break;
            case SUCCESS: // 交易成功
                break;
            case CLOSED: // 交易成功
                break;
            default:
                break;
        }
        return new OrderActionType[0];
    }

    @Override
    @Transactional
    public void execute(Order order, User user, OrderActionType action, Map<String, Object> params) {
        // 1. check privileges
        checkPrivileges(order, user, action);
        // 2. check pre conditions, like order status, etc.
        checkPreConditions(order, action);

        // 3. result handle
        switch (action) {
            case CANCEL:
                cancel(order);
                break;
            case PAY:
                if (!pay(order)) {
                    log.warn("订单[" + order.getId() + "]已经支付，无需重新支付");
                    return;
                }
                break;
            case SHIP:
                if (params.get(LOGISTICS_COMPANY_PROPERTY) != null) {
                    order.setLogisticsCompany(params.get(LOGISTICS_COMPANY_PROPERTY).toString());
                    order.setLogisticsOrderNo((String) params.get(LOGISTICS_ORDERNO_PROPERTY));
                }
                ship(order);
                break;
            case SIGN:
                if (params != null && params.get("delaySign") != null) {
                    delaySign(order);
                    return;
                } else {
                    sign(order);
                }
                break;
            case REFUND:
                if (params != null) {
                    check(order, params);
                } else {
                    order.setRefundFee(order.getPaidFee());
                }
                refund(order);
                break;
            case REQUEST_REFUND:
                requestRefund(order);
                break;
            case CANCEL_REFUND:
                cancelRefund(order, params);
                break;
            case ACCEPT_REFUND:
                acceptRefund(order);
                break;
            case REJECT_REFUND:
                rejectRefund(order);
                break;
            case CHANGE:
                change(order);
                break;
            case REQUEST_CHANGE:
                //值不为空执行换货请求
                if(params.get(CHANGE_GOODS_PROPERTY) != null){
                    requestChange(order, params);
                }else{
                    throw new RuntimeException("换货商品为空！");
                }
                break;
            case CANCEL_CHANGE:
                cancelChange(order, params);
                break;
            case ACCEPT_CHANGE:
                acceptChange(order);
                break;
            case REJECT_CHANGE:
                rejectChange(order);
                break;
            case REQUEST_REISSUE:
                //值不为空执行补货请求
                if(params.get(REISSUE_GOODS_PROPERTY) != null){
                    requestReissue(order, params);
                }else{
                    throw new RuntimeException("补货商品为空！");
                }
                break;
            case ACCEPT_REISSUE:
                cancelReissue(order, params);
                break;
            case REJECT_REISSUE:
                acceptReissue(order);
                break;
            case CANCEL_REISSUE:
                rejectReissue(order);
                break;
            default:
                unkownAction(order, action);
                break;
        }

        // 4. fire events
        ApplicationEvent event = new OrderActionEvent(action, order);
        applicationContext.publishEvent(event);
        log.info("fire event[" + action.name() + "] for order[" + order.getOrderNo() + "]");
    }

    private void check(Order order, Map<String, Object> params){
        if (params.containsKey(REFUND_GOODS_FEE_PROPERTY)) {
            order.setRefundGoodsFee((BigDecimal) params.get(REFUND_GOODS_FEE_PROPERTY));
            order.setRefundLogisticsFee((BigDecimal) params.get(REFUND_LOGISTICS_FEE_PROPERTY));
            if (order.getRefundGoodsFee().compareTo(NumberUtils.createBigDecimal("-1")) == 0) {
                order.setRefundGoodsFee(order.getPaidFee().subtract(order.getLogisticsFee()).add(order.getDiscountFee()));
            }
            if (order.getRefundLogisticsFee().compareTo(NumberUtils.createBigDecimal("-1")) == 0) {
                order.setRefundLogisticsFee(order.getLogisticsFee());
            }
            order.setRefundFee(order.getRefundGoodsFee().add(order.getRefundLogisticsFee()));
        } else if (params.containsKey(REFUND_FEE_PROPERTY)) {
            order.setRefundFee((BigDecimal) params.get(REFUND_FEE_PROPERTY));
        }
    }

    private BigDecimal findRefundableFee(Order order, OrderStatus orderStatus) {
        BigDecimal refundableFee = null;

//		if(orderStatus == OrderStatus.PAID || orderStatus == OrderStatus.SHIPPED){
//			refundableFee = order.getPaidFee();
////			refundableFee = order.getTotalFee().add(order.getDiscountFee());
//			if(orderStatus == OrderStatus.SHIPPED)
//				refundableFee = refundableFee.subtract(order.getLogisticsFee());
//		}

        refundableFee = order.getTotalFee();
        if (orderStatus != OrderStatus.PAID) {
            refundableFee = refundableFee.subtract(order.getLogisticsFee());
        }
        return refundableFee;
    }

    @Override
    @Transactional
    public void executeBySystem(Order order, OrderActionType action, Map<String, Object> params) {

        // 1. check privileges and status
        boolean valid = checkPreConditions(order, action);
        if (!valid) {
            log.error("订单[" + order.getId() + "]的该操作已经做过了，忽略该操作");
        }

        // 2. result handle
        switch (action) {
            case CANCEL:
                cancel(order);
                break;
            case REFUND:
                if (params == null) {
                    order.setRefundFee(order.getPaidFee());
                    refund(order);
                    break;
                }
                check(order, params);
                refund(order);
                break;

            case PAY:
                if (!pay(order)) {
                    log.warn("订单[" + order.getId() + "]已经支付，无需重新支付");
                    return;
                }
                break;
            case SHIP:
                if (params.get(LOGISTICS_COMPANY_PROPERTY) != null) {
                    order.setLogisticsCompany(params.get(LOGISTICS_COMPANY_PROPERTY).toString());
                    order.setLogisticsOrderNo((String) params.get(LOGISTICS_ORDERNO_PROPERTY));
                }
                ship(order);
                break;
            case CANCEL_REFUND:
                cancelRefund(order, params);
                break;
            case CANCEL_CHANGE:
                cancelChange(order, params);
                break;
            case CANCEL_REISSUE:
                cancelReissue(order, params);
                break;
            case SIGN:
                sign(order);
                break;
            case DELAYSIGN:
                delaySign(order);
                break;
            default:
                unkownAction(order, action);
                break;
        }

        // 3. fire event
        ApplicationEvent event = new OrderActionEvent(action, order);
        applicationContext.publishEvent(event);
        log.info("fire event[" + action.name() + "] for order[" + order.getOrderNo() + "] by system autoexec");
    }

    private void requestRefund(Order order) {
        Order record = new Order();
        record.setId(order.getId());
        record.setStatus(OrderStatus.REFUNDING);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", order.getId());


        if (orderMapper.updateOrderByRequestRefund(order.getId()) == 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "request order refund occurs error");
        }
        log.debug("order[" + order.getOrderNo() + "] is refunding");

    }

    private void cancelRefund(Order order, Map<String, Object> params) {
//		Map<String, Object> params = new HashMap<String, Object>();
//		params.put("id", order.getId());
//		params.put("status", OrderStatus.SHIPPED);

        if (orderMapper.updateOrderByCancelRefund(order.getId(), (OrderStatus) params.get("orderStatus")) == 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "request order refund occurs error");
        }
        log.debug("order[" + order.getOrderNo() + "] is cancel refund");

    }


    /**
     * 拒绝补货
     * @param order
     */
    private void rejectReissue(Order order) {
        Order record = new Order();
        record.setId(order.getId());
        record.setStatus(OrderStatus.SHIPPED);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", order.getId());
        params.put("status", OrderStatus.REISSUING);

        if (orderMapper.updateOrderStatusWithPreCondition(params, record) == 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "order reject reissue occurs error");
        }
        log.debug("order[" + order.getOrderNo() + "] reject reissue");
    }

    /**
     * 同意补货
     * @param order
     */
    private void acceptReissue(Order order) {
        Order record = new Order();
        record.setId(order.getId());
        record.setStatus(OrderStatus.CLOSED);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", order.getId());
        params.put("status", OrderStatus.REISSUING);

        if (orderMapper.updateOrderStatusWithPreCondition(params, record) == 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "order accept reissue occurs error");
        }
        log.debug("order[" + order.getOrderNo() + "] accept reissue");
    }

    /**
     * 取消补货
     * @param order
     * @param params
     */
    private void cancelReissue(Order order, Map<String, Object> params) {
        if (orderMapper.updateOrderByCancelReissue(order.getId(), (OrderStatus) params.get("orderStatus")) == 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "request order reissue occurs error");
        }
        log.debug("order[" + order.getOrderNo() + "] is cancel reissue");
    }

    /**
     * 请求补货
     * @param order
     */
    private void requestReissue(Order order, Map<String, Object> params) {
      //修改订单状态为补货中
      if (orderMapper.updateOrderByRequestReissue(order.getId()) == 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "request order reissue occurs error");
      }
      log.debug("order[" + order.getOrderNo() + "] is reissuing");
      //将用户选择补发的订单商品插入数据库，特殊标记
      List<OrderItem> orderItemList = (List<OrderItem>) params.get("reissueGoods");
      for (OrderItem orderItem:
              orderItemList) {
        OrderItem item = orderItemMapper.selectByPrimaryKey(orderItem.getId());//查询出请求补货的商品
        item.setAmount(orderItem.getAmount());//设置前端传递的数量
        item.setPromotionType(PromotionType.APPLY_REISSUE_GOODS);//特殊标记后插入数据库
        orderItemMapper.insertChangeGoods(item);//使用插入换货商品的接口
      }
      log.debug("orderItemList[" + orderItemList + "] is insert ");
    }

    /**
     * 拒绝换货
     * @param order
     */
    private void rejectChange(Order order) {
        Order record = new Order();
        record.setId(order.getId());
        record.setStatus(OrderStatus.SHIPPED);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", order.getId());
        params.put("status", OrderStatus.CHANGING);

        if (orderMapper.updateOrderStatusWithPreCondition(params, record) == 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "order reject change occurs error");
        }
        log.debug("order[" + order.getOrderNo() + "] reject change");
    }

    /**
     * 同意换货
     * @param order
     */
    private void acceptChange(Order order) {
        Order record = new Order();
        record.setId(order.getId());
        record.setStatus(OrderStatus.CLOSED);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", order.getId());
        params.put("status", OrderStatus.CHANGING);

        if (orderMapper.updateOrderStatusWithPreCondition(params, record) == 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "order accept change occurs error");
        }
        log.debug("order[" + order.getOrderNo() + "] accept change");
    }

    /**
     * 取消换货
     * @param order
     * @param params
     */
    private void cancelChange(Order order, Map<String, Object> params) {
        if (orderMapper.updateOrderByCancelChange(order.getId(), (OrderStatus) params.get("orderStatus")) == 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "request order change occurs error");
        }
        log.debug("order[" + order.getOrderNo() + "] is cancel change");

    }

    @Autowired
    private OrderItemMapper orderItemMapper;

    /**
     * 请求换货
     * @param order
     */
    private void requestChange(Order order, Map<String, Object> params) {
      //修改订单状态为换货中
      if (orderMapper.updateOrderByRequestChange(order.getId()) == 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "request order change occurs error");
      }
      log.debug("order[" + order.getOrderNo() + "] is changing");

      //将用户选择更换的订单商品插入数据库，特殊标记，供换货下单使用
      List<OrderItem> orderItemList = (List<OrderItem>) params.get("changeGoods");
      for (OrderItem orderItem:
           orderItemList) {
        OrderItem item = orderItemMapper.selectByPrimaryKey(orderItem.getId());//查询出请求换货的商品
        item.setAmount(orderItem.getAmount());//设置前端传递的数量
        item.setPromotionType(PromotionType.APPLY_CHANGE_GOODS);//特殊标记后插入数据库
        orderItemMapper.insertChangeGoods(item);
      }
      log.debug("orderItemList[" + orderItemList + "] is insert ");
    }

    /**
     * 拒绝退货
     * @param order
     */
    private void rejectRefund(Order order) {
        Order record = new Order();
        record.setId(order.getId());
        record.setStatus(OrderStatus.SHIPPED);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", order.getId());
        params.put("status", OrderStatus.REFUNDING);

        if (orderMapper.updateOrderStatusWithPreCondition(params, record) == 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "order reject refund occurs error");
        }
        log.debug("order[" + order.getOrderNo() + "] reject refund");
    }

    /**
     * 同意退货
     * @param order
     */
    private void acceptRefund(Order order) {
        Order record = new Order();
        record.setId(order.getId());
        record.setStatus(OrderStatus.CLOSED);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", order.getId());
        params.put("status", OrderStatus.REFUNDING);

        if (orderMapper.updateOrderStatusWithPreCondition(params, record) == 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "order accept refund occurs error");
        }
        log.debug("order[" + order.getOrderNo() + "] accept refund");
    }

    protected void checkPrivileges(Order order, User user, OrderActionType action) {
        log.info("check user[" + user.getLoginname() + "]'s privileges of order[" + order.getOrderNo()
                + "] by action[" + action.name() + "]");

        switch (action) {
            case CANCEL:
                // 卖家取消订单，买家也可以
                if (!isBuyer(order, user) && !isSeller(order, user)) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "user has no privileges to do this action orderNo=[" + order.getOrderNo() + "]");
                }

                break;
            case PAY:
                if (!isBuyer(order, user)) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "user has no privileges to do this action orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case SHIP:
                if (!isSeller(order, user)) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "user has no privileges to do this action orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case SIGN:
                if (!isBuyer(order, user)) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "user has no privileges to do this action orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case REFUND:
                if (!isSeller(order, user)) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "user has no privileges to do this action orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case REQUEST_REFUND:
                if (!isBuyer(order, user )) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "user has no privileges to do this action orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case CANCEL_REFUND:
                if (!isBuyer(order, user)) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "user has no privileges to do this action orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case ACCEPT_REFUND:
                if (!isSeller(order, user)) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "user has no privileges to do this action orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case REJECT_REFUND:
                if (!isSeller(order, user)) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "user has no privileges to do this action orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case REQUEST_CHANGE:
                if (!isBuyer(order, user)) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "user has no privileges to do this action orderNo=[" + order.getOrderNo() + "]");
                }
                break;
          case REQUEST_REISSUE:
                if (!isBuyer(order, user)) {
                  throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                          "user has no privileges to do this action orderNo=[" + order.getOrderNo() + "]");
                }
            break;
            default:
                unkownAction(order, action);
                break;
        }
    }

    protected boolean checkPreConditions(Order order, OrderActionType action) {
        log.info("check precondition for order[" + order.getOrderNo() + "] by action[" + action.name()
                + "] orderNo=[" + order.getOrderNo() + "]");

        switch (action) {
            case CANCEL:
                if (!OrderStatus.SUBMITTED.equals(order.getStatus())) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case PAY:
                if (OrderStatus.CANCELLED.equals(order.getStatus())) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                } else if (!OrderStatus.SUBMITTED.equals(order.getStatus())) {
                    log.warn(
                            "this action[" + action.name() + "] has already been paid with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                    return false;
                }
                break;
            case SHIP:
                if (!OrderStatus.PAID.equals(order.getStatus())) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case SIGN:
                if (!OrderStatus.SHIPPED.equals(order.getStatus())) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case REFUND:
                //如果订单有佣金，则不能退款
//				List<Commission> commissions = unionService.listByOrderId(order.getId());
//				if(commissions.size() > 0)
//					throw new BizException(GlobalErrorCode.UNKNOWN, "this action[" + action.name() + "] can not be done because of the exist commention");

                List<OrderStatus> statuses = new ArrayList<OrderStatus>();
                statuses.add(OrderStatus.PAIDNOSTOCK);//liuyandong增加代码

                statuses.add(OrderStatus.PAID);
                statuses.add(OrderStatus.SHIPPED);
                statuses.add(OrderStatus.REFUNDING);
                if (statuses.indexOf(order.getStatus()) < 0) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case REQUEST_REFUND:
                if (!OrderStatus.SHIPPED.equals(order.getStatus()) && !OrderStatus.PAID
                        .equals(order.getStatus()) && !OrderStatus.SUCCESS.equals(order.getStatus())) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case CANCEL_REFUND:
                if (OrderStatus.REFUNDING != order.getStatus()) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case CANCEL_CHANGE:
                if (OrderStatus.CHANGING != order.getStatus()) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case CANCEL_REISSUE:
                if (OrderStatus.REISSUING != order.getStatus()) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case ACCEPT_REFUND:
                if (!OrderStatus.REFUNDING.equals(order.getStatus())) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case REJECT_REFUND:
                if (!OrderStatus.REFUNDING.equals(order.getStatus())) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case ACCEPT_CHANGE:
                if (!OrderStatus.CHANGING.equals(order.getStatus())) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case REJECT_CHANGE:
                if (!OrderStatus.CHANGING.equals(order.getStatus())) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case ACCEPT_REISSUE:
                if (!OrderStatus.REISSUING.equals(order.getStatus())) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case REJECT_REISSUE:
                if (!OrderStatus.REISSUING.equals(order.getStatus())) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            case REQUEST_CHANGE:
                if (!OrderStatus.SHIPPED.equals(order.getStatus()) && !OrderStatus.PAID
                        .equals(order.getStatus()) && !OrderStatus.SUCCESS.equals(order.getStatus())) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                            "this action[" + action.name() + "] can not be done action with order status[" + order
                                    .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
          case REQUEST_REISSUE:
                if (!OrderStatus.SHIPPED.equals(order.getStatus()) && !OrderStatus.PAID
                        .equals(order.getStatus()) && !OrderStatus.SUCCESS.equals(order.getStatus())) {
                  throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
                          "this action[" + action.name() + "] can not be done action with order status[" + order
                                  .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
                }
                break;
            default:
                unkownAction(order, action);
                break;
        }
        return true;
    }

    @Override
    public OrderStatus[] getOrderStatuses() {
        return this.statuses;
    }

    @Override
    public OrderActionType[] getOrderActions() {
        return this.actions;
    }

}
