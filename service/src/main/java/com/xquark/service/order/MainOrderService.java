package com.xquark.service.order;

import com.xquark.dal.model.Address;
import com.xquark.dal.model.MainOrder;
import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.status.MainOrderStatus;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.service.BaseEntityService;
import com.xquark.service.pricing.base.PromotionInfo;
import com.xquark.service.pricing.vo.UserSelectedProVO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * @author ahlon
 */
public interface MainOrderService extends BaseEntityService<MainOrder> {

  MainOrderVO loadVOWithStatus(String orderId, MainOrderStatus... status);

  MainOrderVO loadVO(String orderId);

  List<MainOrder> listPendingByCashierItem(int delaySeconds);
  
  List<MainOrder> listPending(int delaySeconds);

  Supplier<List<MainOrder>> listPendingSupplier(int delaySeconds);

  int update(MainOrder mainOrder);

  int updateFrom(MainOrder mainOrder, MainOrderStatus... originStatus);

  int updatePayNo(MainOrderVO mainOrderVO, String payNo, PaymentMode mode);

  MainOrderVO loadByOrderNo(String batchBizNo);

  boolean pending(MainOrderVO mainOrder);

  void pay(String orderNo, PaymentMode mode, String payNo);

    int insert(MainOrder mainOrder, MainOrderStatus status);

    MainOrder submitBySkuId(String skuId, OrderAddress oa, String remark,
                            UserSelectedProVO userSelectedProVO, Address address, Boolean danbao, int qty,
                            String realShopId, String orderType, boolean useDeduction,
                            String isPrivilege, boolean needInvoice,
                            BigDecimal selectPoint, BigDecimal selectCommission, Long upLine,
                            PromotionInfo promotionInfo);

  MainOrder submitBySkuIds(List<String> skuIds, OrderAddress oa, Map<String, String> shopRemarks,
                           UserSelectedProVO userSelectedProVO,
                           PromotionType userSelectedPromotion, Address address,
                           Boolean danbao, String realShopId,
                           String orderType,
                           Boolean useDeduction, Boolean needInvoice, BigDecimal selectPoint,
                           BigDecimal selectCommission, Long upLine);

  List<String> getOrderNoByMain(String orderNo);

  void updateStatusByOrderNo(String orderNo,MainOrderStatus status);

  MainOrder queryMainOrderByPayNo(String bizNo);

  Long loadUnPaidOrder(String payNo);
  /**
   * 所有子单取消，主单取消
   * @param id
   * @return
   */
  int cancelMainOrderIfOrderCancelled(String id);
}
