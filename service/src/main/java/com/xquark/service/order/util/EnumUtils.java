package com.xquark.service.order.util;


import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import sun.reflect.ConstructorAccessor;
import sun.reflect.FieldAccessor;
import sun.reflect.ReflectionFactory;

/**
 * 动态增加枚举类
 *
 * @author jackzhu
 * @since 2018/11/14
 */
public abstract class EnumUtils {

  private static ReflectionFactory reflectionFactory = ReflectionFactory.getReflectionFactory();
  /**
   * Add an enum instance to the enum class given as argument
   *
   * the type of the enum (implicit)
   *
   * @param enumType the class of the enum to be modified
   * @param enumName the name of the new enum instance to be added to the class.
   */
  @SuppressWarnings("unchecked")
  public static <T extends Enum<?>> void addEnum(Class<T> enumType, String... enumName) {

    // 0. Sanity checks
    if (!Enum.class.isAssignableFrom(enumType)) {
      throw new RuntimeException("class " + enumType + " is not an instance of Enum");
    }
    // 1. Lookup "$VALUES" holder in enum class and get previous enum instances
    Field valuesField = null;
    Field[] fields = enumType.getDeclaredFields();
    for (Field field : fields) {
      if (field.getName().contains("$VALUES")) {
        valuesField = field;
        break;
      }
    }
    AccessibleObject.setAccessible(new Field[] { valuesField }, true);

    try {
      // 2. Copy it
      T[] previousValues = (T[]) valuesField.get(enumType);
      List values = new ArrayList(Arrays.asList(previousValues));

      // 3. build new enum
      T newValue = (T) makeEnum(
          // The target enum class
          enumType,
          // THE NEW ENUM INSTANCE TO BE DYNAMICALLY ADDED
          values.size(),
          // can be used to pass values to the enum constuctor
          new Class[] {},
          // can be used to pass values to the enum constuctor
          new Object[] {},enumName);

      // 4. add new value
      values.add(newValue);

      // 5. Set new values field
      setFailsafeFieldValue(valuesField, null,
          values.toArray((T[]) Array.newInstance(enumType, 0)));

      // 6. Clean enum cache
      cleanEnumCache(enumType);

    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException(e.getMessage(), e);
    }
  }




  private static Object makeEnum(Class<?> enumClass, int ordinal,
      Class<?>[] additionalTypes, Object[] additionalValues,String... value) throws Exception {
    int length = value.length == 1 ? 0 : value.length;
    Object[] parms = new Object[additionalValues.length + length+2];
    parms[0] = value[0];
    parms[1] = Integer.valueOf(ordinal);
    for (int i = 2; i < parms.length; i++) {
      parms[i] = value[i-2];
    }
    System.arraycopy(additionalValues, 0, parms, length+1, additionalValues.length);
    return enumClass.cast(getConstructorAccessor(enumClass, additionalTypes,length).newInstance(parms));
  }

  private static ConstructorAccessor getConstructorAccessor(Class<?> enumClass,
      Class<?>[] additionalParameterTypes,int valuesLength) throws NoSuchMethodException {
    Class<?>[] parameterTypes = new Class[additionalParameterTypes.length + valuesLength+2];
    parameterTypes[0] = String.class;
    parameterTypes[1] = int.class;
    for (int i = 2; i < parameterTypes.length; i++) {
      parameterTypes[i] = String.class;
    }
    System.arraycopy(additionalParameterTypes, 0,
        parameterTypes, valuesLength, additionalParameterTypes.length);
    return reflectionFactory.newConstructorAccessor(
        enumClass.getDeclaredConstructor(parameterTypes));
  }



  private static void setFailsafeFieldValue(Field field, Object target, Object value) throws NoSuchFieldException,
          IllegalAccessException {
    // let's make the field accessible
    field.setAccessible(true);
    // next we change the modifier in the Field instance to
    // not be final anymore, thus tricking reflection into
    // letting us modify the static final field
    Field modifiersField = Field.class.getDeclaredField("modifiers");
    modifiersField.setAccessible(true);
    int modifiers = modifiersField.getInt(field);
    // blank out the final bit in the modifiers int
    modifiers &= ~Modifier.FINAL;
    modifiersField.setInt(field, modifiers);
    FieldAccessor fa = reflectionFactory.newFieldAccessor(field, false);
    fa.set(target, value);
  }

  private static void cleanEnumCache(Class<?> enumClass)
      throws NoSuchFieldException, IllegalAccessException {
    // Sun (Oracle?!?) JDK 1.5/6
    blankField(enumClass, "enumConstantDirectory");
    // IBM JDK
    blankField(enumClass, "enumConstants");
  }

  private static void blankField(Class<?> enumClass, String fieldName)
      throws NoSuchFieldException, IllegalAccessException {
    for (Field field : Class.class.getDeclaredFields()) {
      if (field.getName().contains(fieldName)) {
        AccessibleObject.setAccessible(new Field[] { field }, true);
        setFailsafeFieldValue(field, enumClass, null);
        break;
      }
    }
  }


}
