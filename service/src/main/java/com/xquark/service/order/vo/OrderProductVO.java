package com.xquark.service.order.vo;

/**
 * 订单商品信息
 */
public class OrderProductVO {

  private String productSn;//商品编号
  private int productNum;//商品数量
  private int productType;//商品类型
  private int sellPrice;//商品售价

  public String getProductSn() {
    return productSn;
  }

  public void setProductSn(String productSn) {
    this.productSn = productSn;
  }

  public int getProductNum() {
    return productNum;
  }

  public void setProductNum(int productNum) {
    this.productNum = productNum;
  }

  public int getProductType() {
    return productType;
  }

  public void setProductType(int productType) {
    this.productType = productType;
  }

  public int getSellPrice() {
    return sellPrice;
  }

  public void setSellPrice(int sellPrice) {
    this.sellPrice = sellPrice;
  }
}
