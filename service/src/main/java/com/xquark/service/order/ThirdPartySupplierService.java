package com.xquark.service.order;

import com.xquark.dal.vo.OrderVO;
import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 *@auther liuwei
 * @date 2018/6/28 11:09
 */
public interface ThirdPartySupplierService {

  /**
   * 按照表格内容查询订单
   * @param
   * @return
   */
  List<OrderVO> selectOrders4WxExport(String params);

  /**
   * 更新sku
   */
  Boolean importExcelToInsert(InputStream inputStream) throws Exception;

  /**
   * 更新物流信息
   * @param inputStream
   * @return
   * @throws Exception
   */
  Boolean importExcelToUpdate(InputStream inputStream) throws Exception;

  /**
   * 买家，卖家获取订单（导入订单使用）
   */
  OrderVO loadByOrderNo(String orderNo);

}
