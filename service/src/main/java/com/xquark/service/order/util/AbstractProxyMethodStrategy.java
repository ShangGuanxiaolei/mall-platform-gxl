package com.xquark.service.order.util;

import com.xquark.dal.model.Supplier;
import java.lang.String;
import com.xquark.dal.type.ErpServiceDestinationFactory;
import com.xquark.dal.type.SkuCodeResourcesType;
import com.xquark.dal.type.SkuType;
import com.xquark.service.order.impl.OrderSplitterServiceImpl.OrderDeliveryConditionImpl;

import org.apache.ws.commons.schema.constants.Enum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jackzhu
 * @since 2018/11/16
 **/
public abstract class AbstractProxyMethodStrategy {

  protected ProxyContext proxyContext;

  public AbstractProxyMethodStrategy(ProxyContext proxyContext) {
    this.proxyContext = proxyContext;
  }

  protected static final Logger logger = LoggerFactory
      .getLogger(AbstractProxyMethodStrategy.class);
  protected static final OrderDeliveryConditionImpl ORDER_DELIVERY_CONDITION = new OrderDeliveryConditionImpl();
  protected static final String REFUNDABLE = "_REFUNDABLE";
  protected static final String NOT_REFUNDABLE = "_NOT_REFUNDABLE";
  protected static final String BACK_ENABLE = "可退商品";
  protected static final String BACK_UNABLE = "不可退商品";

  public abstract Object execute();


  protected String initErcEnum(Supplier supplier, String backAble) {
    return obtainErcEnumType(REFUNDABLE.equals(backAble), supplier.getCode());
  }

  protected String initSkuCode(Supplier supplier) {
    SkuCodeResourcesType
            .getSkuCodeResourceBySupplierCode(supplier.getCode()).orElse(obtainSkuEnumType(supplier.getCode()));
    return supplier.getCode();
  }

  protected String obtainErcEnumType(boolean refundAble,
                                     String supplierType) {
    final String supplierCode =
        supplierType + (refundAble ? REFUNDABLE : NOT_REFUNDABLE);
    String string =  ErpServiceDestinationFactory.getErp(supplierCode);
    //没有该枚举类型就创建
    if (string == null) {
      ErpServiceDestinationFactory.put(supplierCode,supplierCode);
      string = ErpServiceDestinationFactory.getErp(supplierCode);
    }

    if (string == null) {
      throw new Enum.EnumValueException("string :"+supplierCode+" is null");
    }
    return string;
  }

  protected SkuType obtainSkuType(Supplier supplier) {
    return SkuCodeResourcesType
            .getSkuCodeResourceBySupplierCode(supplier.getCode())
            .orElse(obtainSkuEnumType(supplier.getCode()));
  }

  protected SkuType obtainSkuEnumType(
          String supplierType) {
    SkuType skuCodeResourcesType = null;

    skuCodeResourcesType = SkuCodeResourcesType.getSkuCodeResourceBySupplierCode(supplierType).orElse(null);
    //如果没有该枚举类型就加入该枚举
    if (skuCodeResourcesType == null) {
      //构造枚举
      SkuType skuType = SkuCodeResourcesType.valueSupplierCode(supplierType);
      if (skuType == null) {
        SkuCodeResourcesType.put(supplierType,new SkuType(supplierType,supplierType, "A", supplierType));
      }
      skuCodeResourcesType = SkuCodeResourcesType.getSkuCodeResourceBySupplierCode(supplierType)
              .orElseThrow(() -> new Enum.EnumValueException("skuCodeResourcesType " + supplierType + "is null"));
    }
    return skuCodeResourcesType;
  }
}
