package com.xquark.service.order.vo;

public class OrderForErpVO {

  private String orderNumber;

  private String orderDate;

  private String payTime;

  private String buyerNick;

  private String totalAmount;

  private String payment;

  private String factPayment;

  private String discount;

  private String postAmount;

  private String invoiceTitle;

  private String invoiceContent;

  private String invoiceAmount;

  private String tradeFrom;

  private String tradeStatus;

  private String refundStatus;

  private String paymentType;

  private String postType;

  private String warehouse;

  private String consignee;

  private String province;

  private String city;

  private String cityarea;

  private String address;

  private String mobilePhone;

  private String telephone;

  private String zip;

  private String sellerMemo;

  private String buyerMessage;

  private int isCrossBorder;


  public int getIsCrossBorder() {
    return isCrossBorder;
  }

  public void setIsCrossBorder(int isCrossBorder) {
    this.isCrossBorder = isCrossBorder;
  }

  public String getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public String getOrderDate() {
    return orderDate;
  }

  public void setOrderDate(String orderDate) {
    this.orderDate = orderDate;
  }

  public String getPayTime() {
    return payTime;
  }

  public void setPayTime(String payTime) {
    this.payTime = payTime;
  }

  public String getBuyerNick() {
    return buyerNick;
  }

  public void setBuyerNick(String buyerNick) {
    this.buyerNick = buyerNick;
  }

  public String getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(String totalAmount) {
    this.totalAmount = totalAmount;
  }

  public String getPayment() {
    return payment;
  }

  public void setPayment(String payment) {
    this.payment = payment;
  }

  public String getFactPayment() {
    return factPayment;
  }

  public void setFactPayment(String factPayment) {
    this.factPayment = factPayment;
  }

  public String getDiscount() {
    return discount;
  }

  public void setDiscount(String discount) {
    this.discount = discount;
  }

  public String getPostAmount() {
    return postAmount;
  }

  public void setPostAmount(String postAmount) {
    this.postAmount = postAmount;
  }

  public String getInvoiceTitle() {
    return invoiceTitle;
  }

  public void setInvoiceTitle(String invoiceTitle) {
    this.invoiceTitle = invoiceTitle;
  }

  public String getInvoiceContent() {
    return invoiceContent;
  }

  public void setInvoiceContent(String invoiceContent) {
    this.invoiceContent = invoiceContent;
  }

  public String getInvoiceAmount() {
    return invoiceAmount;
  }

  public void setInvoiceAmount(String invoiceAmount) {
    this.invoiceAmount = invoiceAmount;
  }

  public String getTradeFrom() {
    return tradeFrom;
  }

  public void setTradeFrom(String tradeFrom) {
    this.tradeFrom = tradeFrom;
  }

  public String getTradeStatus() {
    return tradeStatus;
  }

  public void setTradeStatus(String tradeStatus) {
    this.tradeStatus = tradeStatus;
  }

  public String getRefundStatus() {
    return refundStatus;
  }

  public void setRefundStatus(String refundStatus) {
    this.refundStatus = refundStatus;
  }

  public String getPaymentType() {
    return paymentType;
  }

  public void setPaymentType(String paymentType) {
    this.paymentType = paymentType;
  }

  public String getPostType() {
    return postType;
  }

  public void setPostType(String postType) {
    this.postType = postType;
  }

  public String getWarehouse() {
    return warehouse;
  }

  public void setWarehouse(String warehouse) {
    this.warehouse = warehouse;
  }

  public String getConsignee() {
    return consignee;
  }

  public void setConsignee(String consignee) {
    this.consignee = consignee;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCityarea() {
    return cityarea;
  }

  public void setCityarea(String cityarea) {
    this.cityarea = cityarea;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getMobilePhone() {
    return mobilePhone;
  }

  public void setMobilePhone(String mobilePhone) {
    this.mobilePhone = mobilePhone;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public String getSellerMemo() {
    return sellerMemo;
  }

  public void setSellerMemo(String sellerMemo) {
    this.sellerMemo = sellerMemo;
  }

  public String getBuyerMessage() {
    return buyerMessage;
  }

  public void setBuyerMessage(String buyerMessage) {
    this.buyerMessage = buyerMessage;
  }


}
