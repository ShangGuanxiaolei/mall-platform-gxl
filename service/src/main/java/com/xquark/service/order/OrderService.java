package com.xquark.service.order;

import com.xquark.dal.model.*;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.*;
import com.xquark.dal.vo.*;
import com.xquark.service.BaseEntityService;
import com.xquark.service.order.vo.CustomerVO;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * @author ahlon
 */
public interface OrderService extends BaseEntityService<Order> {


  /**
   * 买家提交订单
   */
  @Deprecated
  Order submitByShop(String shopId, OrderAddress orderAddress,
                     String remark, String unionId, String tuId, Boolean danbao);

  /**
   * 买家提交订单
   */
  @Deprecated
  Order submitBySkuId(String skuId, OrderAddress orderAddress,
                      String remark, String unionId, String tuId, Boolean danbao);

  @Deprecated
  Order submitBySkuIds(List<String> skuIds, OrderAddress orderAddress,
                       String remark, String unionId, String tuId, Boolean danbao);

  @Deprecated
  List<Order> submitBySkuIds(List<String> skuIds, OrderAddress orderAddress,
                             Map<String, String> remarks, String unionId, String tuId, Boolean danbao);

  /**
   * 买家直接提交订单，不走购物车流程
   *
   * @param amount 购买数量
   */
  @Deprecated
  Order submitBySkuId(String skuId, String proLiteralId, OrderAddress orderAddress,
                      String remark, String unionId, String tuId, Boolean danbao, int amount);

  /**
   * 买家取消订单，扩展以后卖家可以取消订单
   */
  Order cancel(String orderId);

  /**
   * 买家取消整个订单
   */
  void cancelMainOrder(String orderId);


  /**
   * 管理员进行关闭订单 zzd
   */
  Order sysCancel(String orderId);

  /**
   * 系统自动任务取消订单 买家未付款超过1天就自动取消订单
   */
  int autoCancel();

  /**
   * 系统自动提醒签收即将到期用户 自动签收前一天系统提醒买家
   */
  int autoRemindSign();

  /**
   * 直接到帐/担保交易支付订单，第三方支付平台返回调用设置订单为已支付状态 第三方支付返回有哪些可能，每种情况的对接考虑
   */
  void pay(String orderId, PaymentMode mode, String payNo);
  void payForPg(String orderNo, PaymentMode mode, String payNo);
  /**
   * 卖家发货
   */
  boolean ship(String orderId, String logisticsCompany, String logisticsOrderNo);

  /**
   * erp发货
   */
  boolean shipByErp(String orderId, String logisticsCompany, String logisticsOrderNo);

  /**
   * 签收，自动签收
   */
  void sign(String orderId);

  /**
   * 延迟签收
   */
  void delaySign(String orderId);

  /**
   * 提醒卖家发货
   */
  void remindShip(String orderId);

  /**
   * 卖家全额退款
   */
  void refund(String orderId);

  /**
   * 卖家部分退款
   */
  void refund(String orderId, BigDecimal returnFree);

  /**
   * 卖家部分退款
   */
  void refundSystem(String orderId, BigDecimal refundment);

  /**
   * 卖家部分退款
   */
  void refundSystem(String orderId);

  /**
   * 买家申请退款
   */
  void requestRefund(String orderId);

  /**
   * 卖家同意退款
   */
  void acceptRefund(String orderId);

  /**
   * 卖家拒绝退款
   */
  void rejectRefund(String orderId);

  int autoSign();

  int update(Order order);

  int updateFrom(OrderStatus from, Order order);

  // ==================== 以上接口已经整理 =================

  int insert(Order e, OrderStatus status);

  int insert2(Order e, OrderStatus status);

  /**
   * 获取订单
   */
  Order load(String id);

  /**
   * 获取订单扩展类
   */
  OrderVO loadVO(String id);

  /**
   * 买家，卖家获取订单
   */
  OrderVO loadByOrderNo(String orderNo);

  /**
   * 列出下一步订单用户可以做什么
   */
  OrderActionType[] listUserNextActions(String orderId);

  /**
   * 订单流程执行
   * @param orderId
   * @param action
   * @param params
   * @return
   */
  // int execute(Order order,OrderActionType action, Map<String, Object> params);

  /**
   * 订单流程执行(由系统执行)
   */
  // int executeBySystem(Order order,OrderActionType action, Map<String, Object> params);

  List<Order> adminSearchOrders(AdminSearchConditions searchConditions);

  /**
   * 获取指定用户的某个状态的订单信息
   */
  List<OrderVO> listByStatus4Buyer(String userId, OrderStatus status, Pageable pageable);

  /**
   * 卖家根据状态列出订单列表
   */
  List<OrderVO> listByStatus4Seller(OrderStatus status, Pageable pageable);

  List<OrderVO> listByStatus4Seller(Supplier<List<OrderVO>> supplier);

  /**
   * @param status
   * @param pageable
   * @return
   */
  List<OrderVO> listByStatus(OrderStatus status, Pageable pageable, String userId,
                             String dateStr);

  List<OrderVO> listByStatus2(OrderStatus status, Pageable page, String userId,
                              String dateStr);

  List<OrderVO> listUnCommentOrder(Pageable pageable, String userId);

  /**
   * 查询某订单是否已评论
   *
   * @param orderId 订单id
   * @return true or false
   */
  Boolean isOrderComment(String orderId, String userId);

  /**
   * 卖家根据状态列出订单列表
   */
  List<OrderVO> listByStatus4Seller(OrderStatus[] status, Pageable pageable);

  /**
   * 卖家根据状态搜索订单列表
   */
  List<OrderVO> listByStatusKey4Seller(OrderStatus status, String key);

  /**
   * 卖家获取客户列表
   */
  List<Customer> listCustomers(Pageable pageable);

  /**
   * 卖家取vip客户列表
   */
  List<Customer> listVipCustomers();

  /**
   * 卖家搜索客户列表
   */
  List<Customer> listCustomersByKey(String key, Pageable pageable);


  /**
   * 卖家获取某客户的订单列表
   */
  CustomerVO listOrdersByCustomer(String customerId);

  CustomerVO listOrdersByCustomer(String name, String phone);

  List<OrderItem> listOrderItems(String orderId);

  Long countByAdmin(Map<String, Object> params);

  Map<String, Object> countMapByAdmin(Map<String, Object> params);

  Long countBuyerOrdersByStatus(OrderStatus status);

  Long countSellerOrdersByStatus(OrderStatus status);

  List<OrderVO> listByAdmin(Map<String, Object> params, Pageable pageable);

  List<OrderVO> listByAdmin4Export(Map<String, Object> params, Pageable pageable);

  Long countByMerchant(Map<String, Object> params);

  List<OrderVO> listByMerchant(Map<String, Object> params, Pageable pageable);

  int updateOrderByRefund1(String orderId);

  Long selectOrderSeqByShopId(String shopId);

  int updateOrderRefundByAdmin(String orderId);

  /**
   * 批量退款
   */
  void updateOrderRefundByAdmin(List<String> orderId);

  Boolean updateVip(String name, String phone, Boolean vip);

  List<Order> searchByBuyer(Map<String, Object> params);

  List<Order> listBuyerOrders(String buyerUserId, Pageable pager);

  Long countBuyerOrders(String buyerUserId);

  /**
   * 获取订单佣金数据
   */
  //Promotion loadOrderPromotion(String order);

  Boolean countFirstStatus4Seller(Order order, OrderStatus status);

  /**
   * 更新订单价格(老接口)
   */
  void updateTotalPrice(String orderId, BigDecimal totalPrice);

  /**
   * 更新订单价格(新接口R141230)
   */
  void updatePrice(String orderId, BigDecimal goodsFee, BigDecimal bLogisticsFee);

  /**
   * 修改订单价格（不需要传入uerid，系统管理员修改金额）
   */
  void updatePriceSystem(String orderId, BigDecimal goodsFee);

  /**
   * 统计当前用户某个状态的订单个数
   */
  Long getCountByStatus4Buyer(OrderStatus os);

  Long getCountByStatus4Buyer(OrderStatus os, String userId);

  Long getCountByStatus4Buyer(OrderStatus os, String userId, String shopId);

  Long getCountByMoreStatus4Buyer(String[] orderStatusArr, String userId, String shopId);

  // 订单留言
  Boolean saveMessage(OrderMessage form);

  // 快店订单留言列表 (快店)
  List<MsgOrdersVO> listMsgOrders(Pageable pageable);

  // 查看订单留言
  List<OrderMessage> viewMessages(String orderId);

  int setMsgRead(String orderId);

  /**
   * 查看当前用户所有留言信息 (xq)
   */
  List<OrderMessage> viewReplyMsgs(Pageable pageable);

  List<OrderMessage> viewReplyMsgs(String userId, Pageable page);

  OrderMessage selectOrderMsgById(String msgId);

  int countNoVisitOrderBySellerId(String userId, Date lastVisitTime);

  MsgProdInfoVO selectProdInfoByOrderId(String orderId);

  /**
   * 获取订单的服务费 订单>=3000 的时候收取2%的技术手续费
   */
  BigDecimal loadTechServiceFee(Order order);

  BigDecimal loadTechServiceFee(BigDecimal fee);

  List<Order> randomFetchOrderForActivity(Date startTime);

  List<OrderFeeVO> findOrderFees(OrderVO order);

  /**
   * 管理端编辑订单备注
   */
  int updRemarkByAdmin(String id, String remark);

  /**
   * 新退款接口
   */
  void refund(String orderId, BigDecimal goodsFee, BigDecimal logisticsFee);

  /**
   * 换货申请接口
   *
   * @param orderId
   * @param orderItemList
   */
  void requestChange(String orderId, List<OrderItem> orderItemList);

  /**
   * 补货申请接口
   * @param orderId
   * @param orderItemList
   */
  void requestReissue(String orderId, List<OrderItem> orderItemList);

  String obtainPaymentChannel(String orderNo);

    int updateFromByMainOrderId(String mainOrderId, PaymentMode payType, OrderStatus to, OrderStatus... from);

    int delete(String id);

  Boolean deleteByBuyer(String orderId);

  void cancelRefund(String orderId, OrderStatus orderStatus);

  /**
   * 系统取消退货申请
   */
  void cancelRefundSystem(String orderId, OrderStatus orderStatus);

  int autoCancelPiece();

  int autoRefund(String orderId, BigDecimal refundFee);

  List<OrderVO> listByStatus4Buyer(OrderStatus status, Pageable pageable);

  /**
   * 获取会员订单信息列表
   */
  List<OrderVO> listByStatusAndMember(String[] orderStatusArr, String userId, String shopId,
                                      Pageable pageable, Map<String, Object> params);

  /**
   * 获取会员卖出的订单信息列表
   */
  List<OrderVO> listSellerByStatusAndMember(String[] orderStatusArr, String userId, String shopId,
                                            Pageable pageable, Map<String, Object> params);

  public void executeBySystem(String orderId, OrderActionType action,
                              Map<String, Object> params);//by zzd

  List<OrderFeeVO> findOrderFees(OrderVO order, BigDecimal cmFee);

  /**
   * 确认收货
   */
  void sign(String orderId, User user);

  void updatePrice(String id, BigDecimal bGoodsFee, BigDecimal bLogisticsFee,
                   String userId);

  void updateTotalPrice(String id, BigDecimal bPrice, String userId);

  boolean cancel(String orderId, String userId);

  /**
   * 模糊查询订单信息的分页方法
   */
  List<OrderVO> listByStatus4SellerWithLike(OrderStatus status,
                                            Pageable pageable, String searchStr, Map<String, Object> params, String userId);

  /**
   * 模糊查询记录总数
   */
  Long countSellerOrdersByStatusWithLike(OrderStatus status, String searchStr);

  List<OrderVO> listOrders4Export(Map<String, Object> params);

  /**
   * 系统自动任务同步订单状态
   */
  int autoRefreshOrder();

  /**
   * 获得未发送给珊瑚云erp的跨境订单集合
   */
  List<Order> selectOrderList2Jooge(Pageable pager);

  /**
   * 更新订单发送erp状态
   */
  int updateOrderSendErpStatus(String id, Date sendErpAt, SendErpStatus sendErpStatus,
                               SendErpDest sendErpDest, String sendErpLog);

  /**
   * 更新订单物流接受状态
   */
  int updateOrderLogisticsErpStatus(String id, Date sysnErpAt, SysnErpStatus sysnErpStatus,
                                    String sysnErpLog);


  void saveMessages(List<OrderMessage> orderMessages);

  int selectOrderCountByMainOrderId(String mainOrderId, String orderStatus);

  /**
   *  测试分佣
   */
  int autoShipAndSignForTest();

  List<Order> selectStatusByMainOrderId(String mainOrderId);

  Long countByPartner(Map<String, Object> params);

  List<OrderVO> listByPartner(Map<String, Object> params, Pageable pageable);

  List<OrderVO> selectOrders4WxExport(Map<String, Object> params);

  Order selectLastMonthTotalFeeByShopId(String shopId);

  List<OrderVO> selectLastMonthOrderByShop(String rootShopId, String shopId);

  List<OrderVO> listByStatusKey4Seller(OrderStatus status, String key, Pageable pageable,
                                       Map<String, Object> params, String userId);

  List<OrderItemVO> selectVOByOrderIdAndUserId(String orderId, String userId);

  /**
   * b2b进货订单审核操作
   */
  void audit(String orderId);

  /**
   * 卖家审核发货接口（b2b经销商使用）
   */
  boolean auditAndship(String orderId, String logisticsCompany, String logisticsOrderNo);

  /**
   * 获取待审核订单数
   */
  long countOrder(Map<String, Object> userId);

  /**
   * 查询是否是该用户第一笔成交成功的订单
   */
  long countBeforeSuccessOrder(String id, String userId);

  /**
   * 团队订单查询
   */
  List<OrderVO> listByTeam(Map<String, Object> params, Pageable pageable);

  Long countByTeam(Map<String, Object> params);

  /**
   * 统计某个店铺的总会员数，即在这个店铺下过订单的用户数
   */
  Long countMemberByShopId(String shopId);

  /**
   * 查询某个店铺所有卖出的商品总金额
   */
  BigDecimal sumBySellerShopId(String shopId);

  /**
   * 确认订单
   */
  int confirmOrder(String orderId);

  long countByStatusAndMember(String[] orderStatusArr, String userId, String shopId,
                              Map<String, Object> params);

  List<OrderVO> listByPromotionTypeAndUserId(PromotionType type, String userId, String shopId,
                                             Pageable pageable);

  Long countByPromotionTypeAndUserId(PromotionType type, String userId, String shopId);

  int countOrderItem(String orderId);

  long countTypeOrderCount(String userId, String status);

  /**
   * 检查并更新订单的售后入口
   *
   * @param order 要检查的订单
   */
  boolean checkAndUpdateCapableToRefund(Order order);


  /**
   * 更新售后入口的开启与关闭
   *
   * @param capableToRefund 开启或关闭
   * @param id              订单id
   * @param openTime
   */
  boolean updateCapableToRefundById(boolean capableToRefund, String id, Date openTime);

  /**
   * 根据OrderNo更新数据
   */
  int updateByOrderNo(WmsExpress wmsExpress);

  /**
   * 获取Wms发货信息
   * @param orderNo
   */
  List<WmsOrderNtsProduct> getWmsOrderNtsProduct(String orderNo);

  /**
   * 获取Wms订单包装箱信息
   * @param orderNo
   */
  List<WmsOrderPackage> getWmsOrderPackage(String orderNo);

  /**
   * 获取Wms订单信息
   */
  List<WmsOrder> getWmsOrder();
  List<WmsOrder> getWmsOrder2();
  List<WmsSku> getWmsOrderSku(String starttime,String skuCode);

  boolean updateStatusByOrderNo(OrderStatus status, String orderNo);

  boolean updateStatusByOrderId(OrderStatus status, String orderId);

  void updateOrderStatusByOrderNo(OrderStatus status, String orderNo);

  void updatePayNoByOrderNo(String orderNo, String payNo);

  void updateOrderByOrderNo(Order order);

  List<Order> queryOrderCanPush();

  List<Order> loadByPayNo(String bizNo);

  void sfOrderCreateTask();

  List<Order> queryOrderByMainOrderId(String id);

  Integer nextval();
  Long  nextval2();

  void confirmChange(String orderId);

  //新增补货下单逻辑
  boolean confirmReissue(String orderId);

  void submitChange(String orderId, String productIdAndAmount);

  boolean updateOrderAddress(String orderId, Address address);

  List<OrderVO> selectByBuyerAndStatusList(String userId, List<String> statusList, Pageable pageable, String type);

  Boolean checkSaleZone(String buyerId);

  /**
   * 循环调取数据库状态
   * @param orderId
   * @return
   */
  boolean getPayCallBackStatus(String orderId);

  List<OrderVO> listByStatus4Seller2(OrderStatus status, Pageable page);

  /**
   * 验证顺丰推送过来的订单号是否存在
   * @param orderNo
   * @return
   */
  int checkOrderNo(String orderNo);

  boolean saveIdentityLogOnSubmit(String orderNo, Long cpId, CareerLevelType careerType);

  boolean saveIdentityLogOnSubmit(String orderNo, Long cpId);

  boolean updateIdentityLogOnPay(String orderNo, CareerLevelType careerType);

  boolean updateIdentityLogOnPay(String orderNo);

  List<WmsSku> selectWmsOrder(String skuId);

  List<String> selectWmsSku(String orderNo);
}
