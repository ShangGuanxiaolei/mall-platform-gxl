package com.xquark.service.order.impl;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.collect.Lists;
import com.vdlm.common.lang.CollectionUtil;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.dal.status.MainOrderStatus;
import com.xquark.dal.status.OrderHeaderStatusMapping;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.PayStatus;
import com.xquark.dal.type.*;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.PromotionSkuVo;
import com.xquark.dal.vo.UserCouponVo;
import com.xquark.event.MainOrderActionEvent;
import com.xquark.service.address.AddressService;
import com.xquark.service.cart.CartService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.cashier.impl.CashierServiceImpl;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.domain.DomainService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.freshman.FreshManProductService;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.grandSale.GrandSaleStadiumService;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderAddressService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.OrderSplitterService;
import com.xquark.service.outpay.ThirdPaymentQueryService;
import com.xquark.service.outpay.impl.ThirdPaymentQueryRes;
import com.xquark.service.pintuan.PromotionPgPriceService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.pricing.base.PromotionInfo;
import com.xquark.service.pricing.impl.pricing.DiscountPricingResult;
import com.xquark.service.pricing.impl.pricing.OrderUnit;
import com.xquark.service.pricing.impl.pricing.PricingMode;
import com.xquark.service.pricing.impl.pricing.SkuDiscountDetail;
import com.xquark.service.pricing.vo.ConfirmOrderPromotionVO;
import com.xquark.service.pricing.vo.PricingResultVO;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductSkuVO;
import com.xquark.service.promotion.OrderItemPromotionService;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.promotion.PromotionSkuGiftService;
import com.xquark.service.promotion.impl.PromotionSkusSerciceImpl;
import com.xquark.service.reserve.PromotionReserveService;
import com.xquark.service.shop.ShopService;
import com.xquark.utils.DateUtils;
import com.xquark.utils.DynamicPricingUtil;
import com.xquark.utils.UniqueNoUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static com.xquark.service.pricing.impl.pricing.DiscountPricingResult.DEVIDED_CUTDOWN_DETAIL_KEY;

/**
 * Created by dongsongjie on 15/11/9.
 */
@Service("mainOrderService")
public class MainOrderServiceImpl extends BaseServiceImpl implements MainOrderService {

  @Autowired
  private MainOrderMapper mainOrderMapper;

  @Autowired
  private OrderMapper orderMapper;

  @Autowired
  private OrderService orderService;

  @Autowired
  private CartService cartService;

  @Autowired
  private ProductService productService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private PromotionService promotionService;

  @Autowired
  private DomainService domainService;

  @Autowired
  private OrderAddressService orderAddressService;

  @Autowired
  private AddressService addressService;

  @Autowired
  private OrderItemMapper orderItemMapper;

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private OrderSplitterService orderSplitterService;

  @Autowired
  private OrderItemPromotionService orderItemPromotionService;

  @Autowired
  private OrderDetailMapper orderDetailMapper;

  @Autowired
  private OrderHeaderMapper orderHeaderMapper;

  @Autowired
  private CustomerProfileService customerProfileService;

  @Autowired
  private PromotionSkusSerciceImpl promotionSkusService;

  @Autowired
  private PromotionBaseInfoService promotionBaseInfoService;

  @Autowired
  private PromotionSkuGiftService promotionSkuGiftService;

  @Autowired
  private PromotionOrderDetailMapper promotionOrderDetailMapper;

  @Autowired
  private SkuMapper skuMapper;

  @Autowired
  private PromotionPgPriceService promotionPgPriceService;

  @Autowired
  private FreshManProductService freshManProductService;

  @Autowired
  private CashierServiceImpl cashierService;

  @Autowired
  private FreshManService freshManService;

  @Autowired
  private ThirdPaymentQueryService thirdPaymentQueryService;

  @Autowired
  private PromotionReserveService promotionReserveService;

  @Autowired
  private GrandSaleStadiumService grandSaleStadiumService;


  @Value("${sf.api.token.profile}")
  private String profile;

  @Value("${sf.api.host.url}")
  private String host;

  @Override
  public int insert(MainOrder mainOrder) {
      return insert(mainOrder, MainOrderStatus.SUBMITTED);
  }

  @Override
  public int insert(MainOrder mainOrder, MainOrderStatus status) {
    if (status == null) {
      status = MainOrderStatus.SUBMITTED;
    }
    mainOrder.setOrderNo(UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.ES));
    mainOrder.setStatus(status);
    log.error(mainOrder.getOrderNo() + " ****ES****");
    return mainOrderMapper.insert(mainOrder);
  }

  @Override
  public int insertOrder(MainOrder mainOrder) {
    mainOrder.setOrderNo(UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.ES));
    mainOrder.setStatus(MainOrderStatus.PAID);
    log.error(mainOrder.getOrderNo() + " ****ES****");
    return mainOrderMapper.insert(mainOrder);
  }

  @Override
  public MainOrder load(String id) {
    return mainOrderMapper.selectByPrimaryKey(id);
  }

  /**
   * @param realShopId for wechat shop only
   * @param orderType  区分进货类型的订单
   */
  @Override
  @Transactional
  public MainOrder submitBySkuId(String skuId, OrderAddress oa, String remark,
                                 UserSelectedProVO userSelectedProVO, Address address, Boolean danbao,
                                 int qty, String realShopId, String orderType, boolean useDeduction,
                                 String isPrivilege, boolean needInvoice,
                                 BigDecimal selectPoint, BigDecimal selectCommission, Long cpId,
                                 PromotionInfo promotionInfo) {

    User buyer = (User) getCurrentUser();
    Long fromCpId;
    if (cpId != null) {
      fromCpId = cpId;
    } else {
      fromCpId = buyer.getUpline();
    }

    Long upline = customerProfileService.getUpline(buyer.getCpId(), fromCpId, promotionInfo);

    PromotionType userSelectedPromotion = promotionInfo == null ? null :
            promotionInfo.getPromotionType();

    //FIXME refactor dongsongjie
    ProductSkuVO productSkuVO = productService
            .load(productService.loadSku(skuId).getProductId(), skuId);
    //Shop shop = shopService.load(productSkuVO.getShopId());
    Shop shop = shopService.loadRootShop();
    Sku sku = productSkuVO.getSku();

    CartItemVO directBuyCartItem = new CartItemVO();
    directBuyCartItem.setProduct(productService.load(sku.getProductId()));
    directBuyCartItem.setShop(shop);
    directBuyCartItem.setSku(productSkuVO.getSku());
    directBuyCartItem.setUser(buyer);
    directBuyCartItem.setSkuId(skuId);
    directBuyCartItem.setProductId(sku.getProductId());
    directBuyCartItem.setAmount(qty);

    MainOrder ret = createMainOrder(remark, buyer);
    ret.setOrderSingleType(OrderSingleType.SUBMIT);
//        ret.setActivityGrouponId(activityGrouponId);
    Order order = new Order();
    order.setShopId(shop.getId());
    order.setSellerId(shop.getOwnerId());
    order.setBuyerId(buyer.getId());
    order.setMainOrderId(ret.getId()); // 在子订单中记录主订单ID
    order.setRemark(remark);
    order.setRootShopId(productSkuVO.getShopId()); // 从当前商品中读取root shop id
    order.setNeedInvoice(needInvoice);
    setOrderTypeInfo(danbao, order);
    Domain domain = loadDomain(buyer);
    order.setPartner(domain.getCode());
    order.setPartnerType(UserPartnerType.KKKD);
    order.setShareCpId(cpId);

    order.setOrderType(OrderSortType.NORMAL);
    if (PromotionType.VIP_RIGHT.equals(userSelectedPromotion)) {
      order.setOrderType(OrderSortType.VIP_RIGHT);
    }

    // 特权活动订单
    if ("1".equals(isPrivilege)) {
      order.setOrderType(OrderSortType.PRIVILEGE);
      if (userSelectedProVO == null) {
        userSelectedProVO = new UserSelectedProVO();
      }
      userSelectedProVO.setIsPrivilege(isPrivilege);
    }

    // 是否自提
    if (address == null) {
      order.setIsPickup(1);
    } else {
      order.setIsPickup(0);
    }

    // 订单计价
    Map<Shop, List<CartItemVO>> cartItemMap = new HashMap<Shop, List<CartItemVO>>();
    cartItemMap.put(shop, Lists.newArrayList(directBuyCartItem));
    ConfirmOrderPromotionVO confirmOrderPromotionVO = promotionService
            .calculate4ConfirmOrder(cartItemMap, buyer, userSelectedProVO, address,
                    useDeduction,
                    selectPoint, selectCommission, promotionInfo, PricingMode.SUBMIT);
    PricingResultVO orderPrice = confirmOrderPromotionVO.getShopPricingResult().get(shop);
    Map<Shop, UserCouponVo> shopCoupon = confirmOrderPromotionVO.getShopCoupon();

    DiscountPricingResult discountPricingResult = orderPrice.getDiscountPricingResult();

    log.info(
            "===========================当前用户下单的fromCoId=" + fromCpId + "=========================");

    BigDecimal cutDownDiscount = discountPricingResult.getDiscount(PromotionType.CUT_DOWN);
    if (upline != null) {
      order.setFromCpId(upline);
    }
//    if (null == upline && null != fromCpId && fromCpId > 0) {
//      order.setFromCpId(fromCpId);
//    }
    if (null == upline && (null == fromCpId || 0 == fromCpId) &&
            PromotionType.VIP_RIGHT.equals(userSelectedPromotion)) {
      order.setFromCpId(2000002L);
    }
    order.setReduction(cutDownDiscount == null ? BigDecimal.ZERO : cutDownDiscount);

    OrderItem orderItem = new OrderItem();
    orderItem.setProductId(productSkuVO.getId());
    orderItem.setProductName(productSkuVO.getName());
    orderItem.setProductImg(productSkuVO.getImg());

    // 修改价格为计算后的价格
    // 特权商品购买不需要花钱
    if ("1".equals(isPrivilege)) {
      orderItem.setPrice(BigDecimal.ZERO);
      orderItem.setMarketPrice(BigDecimal.ZERO);
    } else {
      orderItem.setPrice(directBuyCartItem.getSku().getPrice());
      orderItem.setMarketPrice(directBuyCartItem.getSku().getMarketPrice());
    }

    orderItem.setSkuId(sku.getId());
    orderItem.setSkuStr(sku.getSpec());
    orderItem.setAmount(qty);
    BigDecimal bAmount = BigDecimal.valueOf(qty);
    orderItem.setPromoAmt(sku.getPromoAmt().multiply(bAmount));
    orderItem.setServerAmt(sku.getServerAmt().multiply(bAmount));

    List<OrderItem> orderItems = new ArrayList<>(1);
    orderItems.add(orderItem);

    Map<String, SkuDiscountDetail> couponDetail = null;
    if (userSelectedPromotion != null && discountPricingResult
            .isPromotionInUse(userSelectedPromotion)) {
    }

    @SuppressWarnings("unchecked")
    Map<String, SkuDiscountDetail> cutDownDetail = (Map<String, SkuDiscountDetail>) discountPricingResult
            .getExtraParam(DEVIDED_CUTDOWN_DETAIL_KEY);

    if (MapUtils.isNotEmpty(cutDownDetail)) {
      SkuDiscountDetail cutDownDetailSku = cutDownDetail.get(skuId);
      if (cutDownDetailSku != null) {
        orderItem.setReduction(cutDownDetailSku.getDiscount());
      } else {
        orderItem.setReduction(BigDecimal.ZERO);
      }
    }

    order.setGoodsFee(orderPrice.getGoodsFee());
    order.setLogisticsFee(orderPrice.getLogisticsFee());
    order.setLogisticDiscount(orderPrice.getLogisticsDiscount());
    order.setCommissionLogisticsDiscount(orderPrice.getCommissionLogisticsDiscount());//新增的积分抵扣掉邮费的字段
    order.setLogisticDiscountType(orderPrice.getLogisticDiscountType());
    order.setDiscountFee(orderPrice.getDiscountFee());
    order.setTotalFee(orderPrice.getTotalFee());
    // 是否为付尾款订单 在计算预购价格时放入
    order.setIsPayRest(orderPrice.getIsPayRest());

    //记录优惠信息
    if (MapUtils.isNotEmpty(shopCoupon) && shopCoupon.get(shop) != null) {
      order.setCouponId(shopCoupon.get(shop).getId());
    }

    ret.setTotalFee(confirmOrderPromotionVO.getTotalPricingResult().getTotalFee());
    ret.setDiscountFee(confirmOrderPromotionVO.getTotalPricingResult().getDiscountFee());

    // 保存订单到数据库
    Map<Order, List<OrderItem>> orders = new HashMap<>(1);
    orders.put(order, orderItems);

    OrderUnit orderUnit = new OrderUnit(buyer, ret, orders);

    // 分摊之前处理优惠相关
    discountPricingResult.doBeforeOrderHooks(orderUnit);

    // 订单对象创建完成，优惠计算完毕，开始订单拆分
    Map<Order, List<OrderItem>> splitedOrders = orderSplitterService.split(orders);

    save(ret, splitedOrders, oa, couponDetail, MainOrderStatus.SUBMITTED, OrderStatus.SUBMITTED);
    // 保存之后调用优惠相关
    discountPricingResult.doAfterOrderHooks(orderUnit);
    // 获取每个sku是否计算收益
    Map<String, Boolean> countEarningMap = discountPricingResult.getCurrUsingPromotions()
            .entrySet().stream()
            .collect(Collectors.toMap(Entry::getKey, e -> e.getValue().getIsCountEarning()));
    //加入分会场类型订单是否计算收益逻辑
    setStadiumCountEarning(countEarningMap, sku);
    saveToOrder(buyer.getCpId(), splitedOrders,
            PromotionType.VIP_RIGHT.equals(userSelectedPromotion) ? 2 : null, countEarningMap);
    applicationContext.publishEvent(new MainOrderActionEvent(
            MainOrderActionType.SUBMIT, ret));
    return ret;
  }

  /**
   * * @param orderType 区分进货类型的订单
   */
  @Override
  @Transactional
  public MainOrder submitBySkuIds(List<String> skuIds, OrderAddress oa,
                                  Map<String, String> shopRemarks,
                                  UserSelectedProVO userSelectedProVO,
                                  PromotionType userSelectedPromotion, Address address,
                                  Boolean danbao, String realShopId,
                                  String orderType,
                                  Boolean useDeduction, Boolean needInvoice, BigDecimal selectPoint,
                                  BigDecimal selectCommission, Long cpId) {
    User buyer = (User) getCurrentUser();

    Long fromCpId;
    if (cpId != null) {
      fromCpId = cpId;
    } else {
      fromCpId = buyer.getUpline();
    }

    Long upline = customerProfileService.getUpline(buyer.getCpId(), fromCpId);

    // 创建主订单对象
    MainOrder ret = createMainOrder(null, buyer);
    ret.setOrderSingleType(OrderSingleType.CART);

    List<CartItemVO> cartItems = cartService.checkout(new HashSet<>(skuIds), buyer, realShopId, userSelectedPromotion);

    Map<String, Boolean> stadiumMap = new HashMap<>();

    //按店铺初始化购物车集合, 检查商品是否下架
    Map<Shop, List<CartItemVO>> cartItemsMap = new HashMap<>(4);
    for (CartItemVO cartItem : cartItems) {
      List<CartItemVO> list = cartItemsMap.get(cartItem.getShop());
      if (list == null) {
        list = new ArrayList<>(8);
        cartItemsMap.put(cartItem.getShop(), list);
      }
      list.add(cartItem);
    }

    ConfirmOrderPromotionVO confirmOrderPromotionVO = promotionService
            .calculate4ConfirmOrder(cartItemsMap, buyer, userSelectedProVO, address,
                    useDeduction, selectPoint, selectCommission, null, PricingMode.SUBMIT);
    Map<Shop, UserCouponVo> shopCoupon = confirmOrderPromotionVO.getShopCoupon();
    Map<Shop, PricingResultVO> shopPrices = confirmOrderPromotionVO.getShopPricingResult();
    PricingResultVO totalPrice = confirmOrderPromotionVO.getTotalPricingResult();

    DiscountPricingResult discountPricingResult = totalPrice.getDiscountPricingResult();
    Map<String, SkuDiscountDetail> discountDetails = null;
    boolean isPromotionInUse = discountPricingResult.isPromotionInUse(userSelectedPromotion);

    BigDecimal cutDownDiscount = discountPricingResult.getDiscount(PromotionType.CUT_DOWN);

    //创建子订单
    Map<Order, List<OrderItem>> orders = new HashMap<>();
    for (Map.Entry<Shop, List<CartItemVO>> entry : cartItemsMap.entrySet()) {
      Shop shop = entry.getKey();
      Order order = new Order();
      if (upline != null) {
        order.setFromCpId(upline);
      }
      order.setShopId(shop.getId());
      order.setSellerId(shop.getOwnerId());
      order.setBuyerId(buyer.getId());
      setOrderTypeInfo(danbao, order);
      Domain domain = loadDomain(buyer);
      order.setPartner(domain.getCode());
      order.setReduction(cutDownDiscount == null ? BigDecimal.ZERO : cutDownDiscount);
      order.setPartnerType(UserPartnerType.KKKD);
      order.setOrderType(OrderSortType.NORMAL);
      order.setRemark(shopRemarks.get(shop.getId()));
      order.setPaidYundou(0L);
      order.setFullYundou(false);
      order.setNeedInvoice(needInvoice);
      order.setShareCpId(cpId);
      if (isPromotionInUse) {
        OrderSortType orderPromotionType = OrderSortType
                .valueOf(userSelectedPromotion.toString());
        order.setOrderType(orderPromotionType);
        discountDetails = discountPricingResult
                .getSpecificDiscountDetails(userSelectedPromotion);
      }
      // 是否自提
      if (address == null) {
        order.setIsPickup(1);
      } else {
        order.setIsPickup(0);
      }

      List<OrderItem> orderItems = new ArrayList<>();

      // 立减分摊
      @SuppressWarnings("unchecked")
      Map<String, SkuDiscountDetail> cutDownDetail = (Map<String, SkuDiscountDetail>) discountPricingResult
              .getExtraParam(DEVIDED_CUTDOWN_DETAIL_KEY);

      for (CartItemVO cartItem : entry.getValue()) {
        String skuId = cartItem.getSkuId();
        ProductSkuVO productSkuVO = productService
                .load(cartItem.getProductId(), cartItem.getSkuId());
        if (StringUtils.isEmpty(order.getRootShopId())) {
          order.setRootShopId(productSkuVO.getShopId()); // 从当前商品中读取root shop id
        }

        Sku sku = productSkuVO.getSku();

        OrderItem orderItem = new OrderItem();
        orderItem.setProductId(productSkuVO.getId());
        orderItem.setProductName(productSkuVO.getName());
        orderItem.setProductImg(productSkuVO.getImg());

        //价格暂时读取原商品价格
        orderItem.setPrice(productSkuVO.getSkuPrice());
        orderItem.setMarketPrice(productSkuVO.getSkuMarketPrice());

        orderItem.setSkuId(sku.getId());
        orderItem.setSkuStr(sku.getSpec());
        orderItem.setAmount(cartItem.getAmount());

        orderItem.setPromoAmt(sku.getPromoAmt().multiply(cartItem.getBAmount()));
        orderItem.setServerAmt(sku.getServerAmt().multiply(cartItem.getBAmount()));
        orderItems.add(orderItem);

        if (MapUtils.isNotEmpty(cutDownDetail)) {
          SkuDiscountDetail cutDownDetailSku = cutDownDetail.get(skuId);
          if (cutDownDetailSku != null) {
            orderItem.setReduction(cutDownDetailSku.getDiscount());
          } else {
            orderItem.setReduction(BigDecimal.ZERO);
          }
        }

        //设置分会场是否收益计算
        setStadiumCountEarning(stadiumMap, sku);
      }

      PricingResultVO prices = shopPrices.get(shop);
      order.setGoodsFee(prices.getGoodsFee());
      order.setLogisticsFee(prices.getLogisticsFee());
      order.setLogisticDiscount(prices.getLogisticsDiscount());
      order.setCommissionLogisticsDiscount(prices.getCommissionLogisticsDiscount());//新增的积分抵扣掉邮费的字段
      order.setLogisticDiscountType(prices.getLogisticDiscountType());
      order.setDiscountFee(prices.getDiscountFee());
      order.setTotalFee(prices.getTotalFee());

      //记录优惠信息
      if (MapUtils.isNotEmpty(shopCoupon) && shopCoupon.get(shop) != null) {
        order.setCouponId(shopCoupon.get(shop).getId());
      }

//      orders.put(order, newOrderItems);
      orders.put(order, orderItems);
    }
    ret.setTotalFee(totalPrice.getTotalFee());
    ret.setDiscountFee(totalPrice.getDiscountFee());

    OrderUnit orderUnit = new OrderUnit(buyer, ret, orders);

    // 分摊之前处理优惠相关
    discountPricingResult.doBeforeOrderHooks(orderUnit);
    // 订单对象创建完成，优惠计算完毕，开始订单拆分
    Map<Order, List<OrderItem>> splitedOrders = orderSplitterService.split(orders);

    // 保存订单
    save(ret, splitedOrders, oa, discountDetails, MainOrderStatus.SUBMITTED, OrderStatus.SUBMITTED);
    discountPricingResult.doAfterOrderHooks(orderUnit);

    // 获取每个sku是否计算收益
    Map<String, Boolean> countEarningMap = discountPricingResult.getCurrUsingPromotions()
            .entrySet().stream()
            .collect(Collectors.toMap(Entry::getKey, e -> e.getValue().getIsCountEarning()));
    countEarningMap.putAll(stadiumMap);
    //保存到第三方库
    saveToOrder(buyer.getCpId(), splitedOrders,
            PromotionType.VIP_RIGHT.equals(userSelectedPromotion) ? 2 : null, countEarningMap);
    applicationContext.publishEvent(new MainOrderActionEvent(
            MainOrderActionType.SUBMIT, ret));
    return ret;
  }

  private void setStadiumCountEarning(Map<String, Boolean> stadiumMap, Sku sku) {
    if (sku.getPromotionType() != null) {
      String promotionType = sku.getPromotionType();
      StadiumType stadiumType = null;
      try {
        stadiumType = StadiumType.valueOf(promotionType);
      } catch (Exception e) {
        log.error("类型转换失败,promotionType{}", promotionType, e);
      }
      StadiumType[] stadiumTypes = StadiumType.values();
      if (ArrayUtils.contains(stadiumTypes, stadiumType)) {
        GrandSaleStadium grandSaleStadium = grandSaleStadiumService.selectOneByType(promotionType);
        if (grandSaleStadium == null) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "分会场商品信息不存在");
        }
        Boolean isCountEarning = grandSaleStadium.getIsCountEarning();
        stadiumMap.put(sku.getProductId(), isCountEarning);
      }
    }
  }

  @Override
  public List<String> getOrderNoByMain(String orderNo) {
    return mainOrderMapper.getOrderNoByMain(orderNo);
  }

  @Override
  public void updateStatusByOrderNo(String orderNo, MainOrderStatus status) {
    mainOrderMapper.updateStatusByOrderNo(orderNo, status);
  }

  @Override
  public MainOrder queryMainOrderByPayNo(String bizNo) {
    MainOrder order = mainOrderMapper.queryMainOrderByPayNo(bizNo);
    return order;
  }

  @Override
  public MainOrderVO loadVOWithStatus(String orderId, MainOrderStatus... status) {
    MainOrder mainOrder = mainOrderMapper.selectByPrimaryKeyAndStatus(orderId, status);
    if (mainOrder == null) {
      return null;
    }
    List<Order> orders = orderMapper.selectByMainOrderId(orderId);
    List<OrderVO> orderVOs = new ArrayList<OrderVO>();
    for (Order order : orders) {
      orderVOs.add(orderService.loadVO(order.getId()));
    }

    if (mainOrder != null && !CollectionUtils.isEmpty(orders)) {
      return new MainOrderVO(mainOrder, orderVOs);
    }
    return null;
  }

  @Override
  public MainOrderVO loadVO(String orderId) {
    return loadVOWithStatus(orderId);
  }
  
  @Override
  public List<MainOrder> listPendingByCashierItem(int delaySeconds){
	  return mainOrderMapper.listPendingByCashierItem(delaySeconds);
  }

  @Override
  public List<MainOrder> listPending(int delaySeconds) {
      return mainOrderMapper.listPending(delaySeconds);
  }

  @Override
  public java.util.function.Supplier<List<MainOrder>> listPendingSupplier(int delaySeconds) {
      return () -> listPending(delaySeconds);
  }

  @Override
  public int update(MainOrder mainOrder) {
    checkMoney(mainOrder.getTotalFee());
    return mainOrderMapper.updateByPrimaryKeySelective(mainOrder);
  }

  /**
   * 带乐观锁的状态更新
   */
  @Override
  public int updateFrom(MainOrder mainOrder, MainOrderStatus... originStatus) {
    checkMoney(mainOrder.getTotalFee());
    return mainOrderMapper.updateByPrimaryKeyAndStatus(mainOrder, originStatus);
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public int updatePayNo(MainOrderVO mainOrderVO, String payNo, PaymentMode mode) {
    Preconditions.checkNotNull(mainOrderVO, "主订单不能为空");
    String orderId = mainOrderVO.getId();
    Preconditions.checkState(StringUtils.isNotBlank(orderId), "主订单id不能为空");
    mainOrderVO.setPayNo(payNo);
    mainOrderVO.setPayType(mode);
    int ret = mainOrderMapper.updateByPrimaryKeySelective(mainOrderVO);
    List<OrderVO> subOrders = mainOrderVO.getOrders();
    if (CollectionUtils.isEmpty(subOrders)) {
      log.warn("主订单 {} 没有对应的子订单", orderId);
      return ret;
    }
    subOrders.forEach(o -> {
      Order toUpdate = new Order();
      toUpdate.setId(o.getId());
      toUpdate.setPayNo(payNo);
      toUpdate.setPayType(mode);
      orderMapper.updateByPrimaryKeySelective(toUpdate);
    });
    return ret;
  }

  @Override
  public MainOrderVO loadByOrderNo(String batchBizNo) {
    MainOrder mainOrder = mainOrderMapper.selectByOrderNo(batchBizNo);
    List<Order> orders = orderMapper.selectByMainOrderId(mainOrder.getId());
    List<OrderVO> orderVOs = new ArrayList<>();
    for (Order order : orders) {
      orderVOs.add(orderService.loadVO(order.getId()));
    }

    if (!CollectionUtils.isEmpty(orders)) {
      return new MainOrderVO(mainOrder, orderVOs);
    }
    return null;
  }

  private void checkMoney(BigDecimal price) {
    if (price == null) {
      return;
    }

    if (String.valueOf(price).length() > 9) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成订单的价格"
              + price + "不合理，超出数据库");
    }
  }

  /**
   * 获取订单的下单方式
   */
  private void setOrderTypeInfo(Boolean danbao, Order order) {
    OrderType orderType = OrderType.DIRECT;

    if (Boolean.TRUE.equals(danbao)) {
      orderType = OrderType.DANBAO;
    } else {
      String shopId = order.getShopId();
      if (StringUtils.isBlank(shopId)) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "设置订单的交易方式的时候，订单所属的店铺不存在");
      }

      Shop shop = shopService.load(shopId);
      if (shop == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "设置订单的交易方式的时候，店铺" + shopId + "不存在");
      }

      if (shop.getDanbao() != null && shop.getDanbao()) {
        orderType = OrderType.DANBAO;
      }
    }
    order.setType(orderType);
  }

  private Domain loadDomain(User user) {
    Domain domain = null;
    if (StringUtils.isNotBlank(user.getPartner())) {
      domain = domainService.loadByCode(user.getPartner());
    } else {
      domain = domainService.loadRootDomain();
    }

    if (domain == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "用户错误 partner=[" + user.getPartner() + "]");
    }
    return domain;
  }

  private MainOrder createMainOrder(String remark, User user) {
    MainOrder ret = new MainOrder();
    ret.setTotalFee(new BigDecimal(0));
    ret.setDiscountFee(new BigDecimal(0));
    ret.setPaidFee(new BigDecimal(0));
    ret.setBuyerId(user.getId());
    ret.setType(OrderType.DANBAO); //默认为担保交易类型
    ret.setRemark(remark);

    return ret;
  }

  public void save(MainOrder mainOrder, Map<Order, List<OrderItem>> orders,
                   OrderAddress orderAddress,
                   Map<String, SkuDiscountDetail> skuDiscountDetails, MainOrderStatus mainOrderStatus,
                   OrderStatus status) {

    //保存主订单
    insert(mainOrder, mainOrderStatus);

    Iterator<Map.Entry<Order, List<OrderItem>>> iterator = orders.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry<Order, List<OrderItem>> entry = iterator.next();
      Order order = entry.getKey();
      List<OrderItem> orderItems = entry.getValue();
      order.setMainOrderId(mainOrder.getId()); // 设置主订单ID到子订单

      //判断是否是大会活动订单 2018-10-08
      boolean isStadium =false;
      for (OrderItem orderItem : orderItems) {
        String skuId = orderItem.getSkuId();
        Sku sku = skuMapper.selectByPrimaryKey(skuId);
        //是否分会场商品
        String type = sku.getPromotionType();
        if (type != null) {
          StadiumType stadiumType = null;
          try {
            stadiumType = StadiumType.valueOf(type);
          } catch (Exception e) {
            log.error("分会场类型转换失败,type:{}", type, e);
          }
          StadiumType[] stadiumTypes = StadiumType.values();
          boolean contains = ArrayUtils.contains(stadiumTypes, stadiumType);
          if(contains ){
            isStadium = true;
          }
        }
        if(isStadium && OrderSortType.NORMAL.equals(order.getOrderType())){
           order.setOrderType(OrderSortType.STADIUM);
        }

        Integer amount = orderItem.getAmount();
        if (amount == null || amount <= 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "下单数量不正确, 请重试");
        }
        List<PromotionBaseInfo> promotionSkuVos = promotionBaseInfoService
                .selectBySkuCodeAndType(sku.getSkuCode(), "meeting");
        if (CollectionUtils.isNotEmpty(promotionSkuVos)) {
          order.setOrderType(OrderSortType.MEETING);
          break;
        }
      }

      // 是否是促销活动商品订单
      for (OrderItem orderItem : orderItems) {
        String skuId = orderItem.getSkuId();
        Sku sku = skuMapper.selectByPrimaryKey(skuId);
        Integer amount = orderItem.getAmount();
        if (amount == null || amount <= 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "下单数量不正确, 请重试");
        }
        List<PromotionBaseInfo> promotionSkuVos = promotionBaseInfoService
                .selectBySkuCodeAndType(sku.getSkuCode(), "PRODUCT_SALES");

//        if(order.getOrderType().equals(OrderSortType.FLASHSALE)&& CollectionUtils.isNotEmpty(promotionSkuVos)){
//          order.setOrderType(OrderSortType.MULTIPLE);
//          break;
//        }
        if (CollectionUtils.isNotEmpty(promotionSkuVos) && OrderSortType.NORMAL.equals(order.getOrderType())) {
          order.setOrderType(OrderSortType.PRODUCT_SALES);
          break;
        }
      }



      //判断是否是大会活动订单 2018-10-08

      //判断是否存在新人专区商品 设置订单类型
      for (OrderItem orderItem : orderItems) {
        Integer amount = orderItem.getAmount();
        if (amount == null || amount <= 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "下单数量不正确, 请重试");
        }
        List<FreshManProductVo> freshManProductVos = freshManProductService.selectByProductId(orderItem.getProductId());
        if(CollectionUtil.isNotEmpty(freshManProductVos)&& OrderSortType.NORMAL.equals(order.getOrderType())){
          order.setOrderType(OrderSortType.FRESHMAN);
          break;
        }
      }

      orderService.insert(order, status);

      if (orderAddress != null) {
        orderAddress.setOrderId(order.getId());
        Address address = new Address();
        BeanUtils.copyProperties(orderAddress, address);
        address.setCommon(false);
        address.setUserId(getCurrentUser().getId());
        if (address.getWeixinId() == null) {
          address.setWeixinId("");
        }

        orderAddressService.insert(orderAddress);
        addressService.saveUserAddress(address, true);
      }

      // 保存订单项
      //增加1013杭州大会赠品订单行-2018-10-07 by Chenpeng
      List<OrderItem> newOrderItems = this.addGiftOrderItem(orderItems, order);
      //增加1013杭州大会赠品订单行-2018-10-07 by Chenpeng

      for (OrderItem orderItem : newOrderItems) {
        orderItem.setOrderId(order.getId());
        // 活动订单保存前相关逻辑处理
//                if (StringUtils.isNotBlank(yundouProductId)) {
//                    BigDecimal discountPrice = yundouProductService.getDiscountPrice(orderItem.getPrice(), yundouProductId);
//                    orderItem.setPrice(discountPrice);
//                    orderItem.setMarketPrice(discountPrice);
//                } else {
//                    promotionCheckService.checkOrderItem(orderItem, order.getOrderType(), promotionProduct, activityGrouponId);
//                }
        //新人专区商品 对订单行设置type
        List<FreshManProductVo> freshManProductVos = freshManProductService.selectByProductId(orderItem.getProductId());
        if(CollectionUtil.isNotEmpty(freshManProductVos)){
          orderItem.setPromotionType(PromotionType.FRESHMAN);
        }

         // 促销商品对订单行设置type

        List<Promotion4ProductSalesVO> promotion4ProductSalesVOS = promotionBaseInfoService.selectSalesPromotionByProductId(orderItem.getProductId());
        if (CollectionUtil.isNotEmpty(promotion4ProductSalesVOS )) {
          orderItem.setPromotionType(PromotionType.PRODUCT_SALES);
        }


        orderItemMapper.insert(orderItem);
        // 限时抢购订单保存后相关逻辑处理
//                checkFlashsaleAfterSave(orderItem);
        User user = (User) getCurrentUser();

        // TODO 暂时只处理大会跟拼团的活动
        List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService
                .selectByProductIdAndType(orderItem.getProductId(), "meeting");
        // 保存promotion_order_detail的活动信息
        Supplier<PromotionOrderDetail> orderDetailSupplier;
        if (CollectionUtils.isNotEmpty(promotionBaseInfos)) {
          String pCode = promotionBaseInfos.get(0).getpCode();
          orderDetailSupplier = new MeetingDetailSupplier(mainOrder.getOrderNo(),
                  order.getOrderNo(), orderItem.getId(), user.getCpId(), pCode);
          this.savePromotionOrderDetail(orderDetailSupplier);
        }
      }

      // 根据订单类型保存订单活动或积分的明细
      saveOrderDetail(order, orderItems, skuDiscountDetails);

    }

  }
  /*
   * @Author chp
   * @Description  物流导入生成订单
   * @Date
   * @Param
   * @return
   **/
  public void save2(MainOrder mainOrder, Map<Order, List<OrderItem>> orders,
                   OrderAddress orderAddress,
                   Map<String, SkuDiscountDetail> skuDiscountDetails, MainOrderStatus mainOrderStatus,
                   OrderStatus status,User user) {

    //保存主订单
    insert(mainOrder, mainOrderStatus);

    Iterator<Map.Entry<Order, List<OrderItem>>> iterator = orders.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry<Order, List<OrderItem>> entry = iterator.next();
      Order order = entry.getKey();
      List<OrderItem> orderItems = entry.getValue();
      order.setMainOrderId(mainOrder.getId()); // 设置主订单ID到子订单

      //判断是否是大会活动订单 2018-10-08
      for (OrderItem orderItem : orderItems) {
        String skuId = orderItem.getSkuId();
        Sku sku = skuMapper.selectByPrimaryKey(skuId);
        Integer amount = orderItem.getAmount();
        if (amount == null || amount <= 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "下单数量不正确, 请重试");
        }
        List<PromotionBaseInfo> promotionSkuVos = promotionBaseInfoService
                .selectBySkuCodeAndType(sku.getSkuCode(), "meeting");
        if (CollectionUtils.isNotEmpty(promotionSkuVos)) {
          order.setOrderType(OrderSortType.MEETING);
          break;
        }
      }

      // 是否是促销活动商品订单
      for (OrderItem orderItem : orderItems) {
        String skuId = orderItem.getSkuId();
        Sku sku = skuMapper.selectByPrimaryKey(skuId);
        Integer amount = orderItem.getAmount();
        if (amount == null || amount <= 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "下单数量不正确, 请重试");
        }
        List<PromotionBaseInfo> promotionSkuVos = promotionBaseInfoService
                .selectBySkuCodeAndType(sku.getSkuCode(), "PRODUCT_SALES");
        if (CollectionUtils.isNotEmpty(promotionSkuVos)) {
          order.setOrderType(OrderSortType.PRODUCT_SALES);
          break;
        }
      }



      //判断是否是大会活动订单 2018-10-08

      //判断是否存在新人专区商品 设置订单类型
      for (OrderItem orderItem : orderItems) {
        Integer amount = orderItem.getAmount();
        if (amount == null || amount <= 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "下单数量不正确, 请重试");
        }
        List<FreshManProductVo> freshManProductVos = freshManProductService.selectByProductId(orderItem.getProductId());
        if(CollectionUtil.isNotEmpty(freshManProductVos)){
          order.setOrderType(OrderSortType.FRESHMAN);
          break;
        }
      }
//      orderService.insert(order, status);
     orderService.insert2(order, status);

      if (orderAddress != null) {
        orderAddress.setOrderId(order.getId());
        Address address = new Address();
        BeanUtils.copyProperties(orderAddress, address);
        address.setCommon(false);
        address.setUserId(user.getId());
        if (address.getWeixinId() == null) {
          address.setWeixinId("");
        }

           orderAddressService.insert(orderAddress);
//        addressService.saveUserAddress(address, true);
      }

      // 保存订单项
      //增加1013杭州大会赠品订单行-2018-10-07 by Chenpeng
      List<OrderItem> newOrderItems = this.addGiftOrderItem(orderItems, order);
      //增加1013杭州大会赠品订单行-2018-10-07 by Chenpeng

      for (OrderItem orderItem : newOrderItems) {
        orderItem.setOrderId(order.getId());
        // 活动订单保存前相关逻辑处理
//                if (StringUtils.isNotBlank(yundouProductId)) {
//                    BigDecimal discountPrice = yundouProductService.getDiscountPrice(orderItem.getPrice(), yundouProductId);
//                    orderItem.setPrice(discountPrice);
//                    orderItem.setMarketPrice(discountPrice);
//                } else {
//                    promotionCheckService.checkOrderItem(orderItem, order.getOrderType(), promotionProduct, activityGrouponId);
//                }
        //新人专区商品 对订单行设置type
        List<FreshManProductVo> freshManProductVos = freshManProductService.selectByProductId(orderItem.getProductId());
        if(CollectionUtil.isNotEmpty(freshManProductVos)){
          orderItem.setPromotionType(PromotionType.FRESHMAN);
        }

        // 促销商品对订单行设置type

        List<Promotion4ProductSalesVO> promotion4ProductSalesVOS = promotionBaseInfoService.selectSalesPromotionByProductId(orderItem.getProductId());
        if (CollectionUtil.isNotEmpty(promotion4ProductSalesVOS )) {
          orderItem.setPromotionType(PromotionType.PRODUCT_SALES);
        }


        orderItemMapper.insert(orderItem);
        // 限时抢购订单保存后相关逻辑处理
//                checkFlashsaleAfterSave(orderItem);
//        User user = (User) getCurrentUser();

        // TODO 暂时只处理大会跟拼团的活动
        List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService
                .selectByProductIdAndType(orderItem.getProductId(), "meeting");
        // 保存promotion_order_detail的活动信息
        Supplier<PromotionOrderDetail> orderDetailSupplier;
        if (CollectionUtils.isNotEmpty(promotionBaseInfos)) {
          String pCode = promotionBaseInfos.get(0).getpCode();
          orderDetailSupplier = new MeetingDetailSupplier(mainOrder.getOrderNo(),
                  order.getOrderNo(), orderItem.getId(), user.getCpId(), pCode);
          this.savePromotionOrderDetail(orderDetailSupplier);
        }
      }

      // 根据订单类型保存订单活动或积分的明细
      saveOrderDetail(order, orderItems, skuDiscountDetails);

    }

  }



  //判断当前商品productId获取新人专区商品信息
  public List<FreshManProductVo> checkIsFreshManProduct(String productId){
    return freshManProductService.selectByProductId(productId);

  }



  /**
   * 保存每个订单项的优惠明细
   *
   * @param order                订单对象
   * @param orderItems           订单对应的订单项
   * @param skuDiscountDetailMap sku对应的优惠明细
   */
  private void saveOrderDetail(Order order, List<OrderItem> orderItems,
                               Map<String, SkuDiscountDetail> skuDiscountDetailMap) {
    if (MapUtils.isNotEmpty(skuDiscountDetailMap)) {
      for (OrderItem orderItem : orderItems) {
        String skuId = orderItem.getSkuId();
        SkuDiscountDetail detail = skuDiscountDetailMap.get(skuId);
        if (detail != null) {
          String promotionId = detail.getPromotionId();
          PromotionType type = detail.getPromotionType();
          OrderItemPromotion orderItemPromotion = new OrderItemPromotion();
          orderItemPromotion.setArchive(false);
          orderItemPromotion.setPromotionId(promotionId);
          orderItemPromotion.setType(type);
          orderItemPromotion.setOrderItemId(orderItem.getId());
          orderItemPromotionService.insert(orderItemPromotion);
        }
      }
    }
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public boolean pending(MainOrderVO mo) {
    ThirdPaymentQueryRes res = null;
    try{
      final Optional<ThirdPaymentQueryRes> query = thirdPaymentQueryService.query(mo);
      if (query.isPresent()) {
        res = query.get();
      }
    } catch (Exception e) {
      log.error("查询第三方支付状态错误", e);
    }
    // 第三方支付返回已支付, 修改订单状态为PAID
    if (res != null && res.isPaid()) {
      cashierService.handleThirdRes(mo, res);
      return true;
    }
    // 否则修改订单状态为PENDING
    final MainOrder mainOrderToUpdate = new MainOrder();
    mainOrderToUpdate.setId(mo.getId());
    mainOrderToUpdate.setStatus(MainOrderStatus.PENDING);
    final List<OrderVO> subOrders = mo.getOrders();
    final String mainOrderNo = mo.getOrderNo();
    final boolean mainRet = updateFrom(mainOrderToUpdate, MainOrderStatus.SUBMITTED) > 0;
    if (CollectionUtils.isEmpty(subOrders)) {
      log.warn("主订单 {} 没有对应的子订单", mainOrderNo);
      return true;
    }
    final int sum =
        subOrders.stream()
            .map(
                o -> {
                  final Order orderToUpdate = new Order();
                  orderToUpdate.setId(o.getId());
                  orderToUpdate.setStatus(OrderStatus.PENDING);
                  return orderService.updateFrom(OrderStatus.SUBMITTED, orderToUpdate);
                })
            .mapToInt(x -> x)
            .sum();
    if (sum < subOrders.size()) {
      log.warn("主订单 {} 部分子订单状态已变更为PENDING", mainOrderNo);
    }
    return mainRet;
  }

  @Override
  public void pay(String orderNo, PaymentMode mode, String payNo) {
    MainOrderVO order = loadByOrderNo(orderNo);
    if (order.getStatus() == MainOrderStatus.PAID) {
      // 新人引导发放德分, 拆单可能出现子单数据不同步问题, 所以德分发放在主订单更新之后.所有子单更新之前
      freshManService.freshManGrantPoint(order.getOrders().get(0));
      return;
    }
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("payNo", payNo);

    // 更新支付单号,支付单号和支付方式以当前的为准
    MainOrder updateOrder = new MainOrder();
    updateOrder.setId(order.getId());
    updateOrder.setPayNo(payNo);
    updateOrder.setStatus(MainOrderStatus.PAID);
    updateOrder.setPaidStatus(PayStatus.SUCCESS);
    updateOrder.setPaidAt(new Date());
    updateOrder.setPayType(mode);//更新支付方式
    updateFrom(updateOrder, MainOrderStatus.SUBMITTED, MainOrderStatus.PENDING);

    freshManService.freshManGrantPoint(order.getOrders().get(0));
  }

  private List<OrderItem> buildGiftOrderItems(Set<Product> giftProducts) {
    List<OrderItem> result = new ArrayList<OrderItem>();
    for (Product product : giftProducts) {
      List<Sku> skus = productService.listSkus(product);
      if (CollectionUtils.isNotEmpty(skus)) {
        Sku sku = skus.get(0);
        OrderItem giftItem = new OrderItem();
        giftItem.setProductId(product.getId());
        giftItem.setProductName(product.getName());
        giftItem.setProductImg(product.getImg());

        //价格暂时读取原商品价格
        giftItem.setPrice(BigDecimal.ZERO);
        giftItem.setMarketPrice(BigDecimal.ZERO);

        giftItem.setSkuId(sku.getId());
        giftItem.setSkuStr(sku.getSpec());
        giftItem.setAmount(1);
        result.add(giftItem);
      }
    }
    return result;
  }

  /**
   * 保存订单到结算中心
   *
   * @param cpId              用户cpId
   * @param order             订单明细
   * @param jtype             joinType
   * @param isCountEarningMap 是否计算收益, 为false则不同步收益给结算中心
   */
  private void saveToOrder(Long cpId, Map<Order, List<OrderItem>> order, Integer jtype,
                           Map<String, Boolean> isCountEarningMap) {
    boolean isFirstFreshmanOrder = this.checkIsFirstFreshmanOrder(cpId);
    // 部分属性需要根据身份变更
    List<OrderDetail> orderDetails = new ArrayList<>();
    List<OrderHeader> orderHeaders = new ArrayList<>();
    Set<Entry<Order, List<OrderItem>>> entrySet = order.entrySet();
    //标记新人礼单用
    String freshmanMarkNo = "";
    for (Entry<Order, List<OrderItem>> e : entrySet) {

      List<OrderItem> orderItems = e.getValue();
      OrderHeader orderHeader = new OrderHeader();
      Order key = e.getKey();
      String orderNo = key.getOrderNo();

      orderHeader.setIscalculated(0);
      orderHeader.setIsdeleted(0);
      orderHeader.setFromCpId(key.getFromCpId());
      //created_at创建时间
      if (key.getCreatedAt() == null) {
        key.setCreatedAt(new Date());
      }
      orderHeader.setIsActive(0);
      //cpid资格证号
      orderHeader.setCpid(cpId);
      //orderid订单号，种子表生成
      orderHeader.setOrderid(key.getOrderNo());
      //ordermonth订单月
      orderHeader.setOrdermonth(getMonth(key.getCreatedAt()));
      //freightamt运费
      orderHeader.setFreightamt(key.getLogisticsFee());
      //totaldue应付
      orderHeader.setTotaldue(key.getTotalFee());
      //totalpaid实付
      orderHeader.setTotalpaid(key.getPaidFee());
      //status状态
      orderHeader.setStatus(getStatus(OrderStatus.SUBMITTED));
      //commissionProductamt计酬金额
      if (key.getPaidPoint() == null) {
        key.setPaidPoint(new BigDecimal(0));
      }
      orderHeader.setCommissionProductamt(null);
      //createtime订单创建时间
      orderHeader.setCreatetime(key.getCreatedAt());
      //deadline订单过期时间
      // 订单过期时间, 半个小时
      // FIXME 处理硬编码
      Date deadLineTime = DateUtils.addSeconds(new Date(), 30 * 60);
      orderHeader.setDeadline(deadLineTime);
      //paytime申请支付时间
      orderHeader.setPaytime(null);
      //paytimeFinish支付完成时间
      orderHeader.setPaytimeFinish(null);
      //sendtimeFinish发货完成时间
      orderHeader.setSendtimeFinish(null);
      //joinType默认值设为0
      orderHeader.setJoinType(0);
      if (null != jtype) {
        orderHeader.setJoinType(jtype);
      }

      //finishtime完成时间
      orderHeader.setFinishtime(null);
      //storeId外键ID，店铺表
      orderHeader.setStoreId(key.getShopId());
      //orderinguserId外键ID，用户表
      orderHeader.setOrderinguserId(key.getBuyerId());
      //totalFreight配送费,保险费 + 运费
      orderHeader.setTotalFreight(key.getLogisticsFee());
      //remark备注
      orderHeader.setRemark(key.getRemark());
      //source来源平台 1 hds, 2 vivilife, 3 ecommerce
      orderHeader.setSource((byte) PlatformType.E.getCode());
      //createdAt创建时间
      orderHeader.setCreatedAt(new Date());
      //updatedAt更新时间
      orderHeader.setUpdatedAt(key.getUpdatedAt());
      //productamt产品金额
      orderHeader.setProductamt(key.getGoodsFee());
      //discountamt折扣金额
      orderHeader.setDiscountamt(key.getDiscountFee());
      //detailCount订单明细记录数，用于核对header和detail数据
      orderHeader.setDetailCount(orderItems.size());
      orderHeader.setUsePoints(key.getPaidCommission());
      orderHeader.setUseDePoints(key.getPaidPoint());
      BigDecimal vpTotal = BigDecimal.ZERO;
      BigDecimal reductionTotal = BigDecimal.ZERO;
      //vp总VP值


      for (OrderItem o : orderItems) {

        OrderDetail orderDetail = new OrderDetail();
        Sku sku = skuMapper.selectByPrimaryKey(o.getSkuId());
        PromotionType promotionType = o.getPromotionType();
        if (sku == null) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "sku不存在");
        }

        //判断是否存在新人专区商品 设置orderHeader表的joinType为5 只更新最后一个子单的数据
        List<FreshManProductVo> freshManProductVos = freshManProductService.selectByProductId(o.getProductId());
        if(CollectionUtil.isNotEmpty(freshManProductVos)){
            freshmanMarkNo=key.getOrderNo();
        }

        Integer amount = o.getAmount();
        BigDecimal bAmount = BigDecimal.valueOf(amount);
        orderDetail.setIsdeleted(0);
        //orderid订单号，种子表生成
        orderDetail.setOrderid(key.getOrderNo());
        orderDetail.setCreatedAt(key.getCreatedAt());
        //updated_at更新时间
        orderDetail.setUpdatedAt(key.getUpdatedAt());
        //product_source产品来源
        orderDetail.setProductSource(3L);
        //quantity数量
        orderDetail.setQuantity(amount);
        //unitprice购买时单价
        orderDetail.setUnitprice(o.getPrice());
        //detailid订单明细主键
        orderDetail.setDetailid(o.getId());
        //product_sku产品SKU编号
        orderDetail.setProductSku(sku.getSkuCode());
        orderDetail.setTaxrate(BigDecimal.valueOf(0.17));
        // 更新后立减只在order_header表
        BigDecimal promoAmt = BigDecimal.ZERO;
        BigDecimal serverAmt = BigDecimal.ZERO;
        BigDecimal savingAmt = BigDecimal.ZERO;
        BigDecimal vp = BigDecimal.ZERO;
        // 可以计算收益才计算
        // 没有活动则默认计算收益
        Boolean isCountEarning = Optional.ofNullable(isCountEarningMap.get(sku.getProductId()))
                .orElse(true);
        if (isCountEarning) {
          // 立减
          savingAmt = DynamicPricingUtil.calCutDown(cpId, sku).multiply(bAmount);
          // vp
          vp = sku.getNetWorth().multiply(bAmount);
          // 推广费
          promoAmt = sku.getPromoAmt().multiply(bAmount);
          // 服务费
          serverAmt = sku.getServerAmt().multiply(bAmount);
        }

        // 处理价格体系
        PromotionPgPrice pgPrice = null;
        if (PromotionType.isPiece(promotionType)) {
          PromotionOrderDetail promotionOrderDetail = promotionOrderDetailMapper.selectByOrderNo(orderNo);
          if (null == promotionOrderDetail || null == promotionOrderDetail.getP_detail_code()) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "拼团活动详情编码为空");
          }

          String pDetailCode = promotionOrderDetail.getP_detail_code();
          pgPrice = promotionPgPriceService.selectPromotionPgPriceByDetailCode(pDetailCode);
        } else if (Objects.equals(promotionType, PromotionType.FLASHSALE)) {
          pgPrice = promotionPgPriceService.loadByPromotionIdAndSkuId(o.getPromotionId(), sku,
                  promotionType);
        }

        if (pgPrice != null) {
          sku.setPoint(pgPrice.getReduction());
          sku.setServerAmt(pgPrice.getServerAmt());
          // vp 净值
          vp = pgPrice.getNetWorth().multiply(bAmount);
          // 推广费
          promoAmt = pgPrice.getPromoAmt().multiply(bAmount);
          // 服务费
          serverAmt = pgPrice.getServerAmt().multiply(bAmount);
          // 立减
          savingAmt = DynamicPricingUtil.calCutDown(cpId, sku).multiply(bAmount);
        }

        //传递到中台的值需要将所有商品的值累加后传递
        reductionTotal = reductionTotal.add(savingAmt);
        vpTotal = vpTotal.add(vp);
        orderDetail.setPromoAmt(promoAmt);
        orderDetail.setServerAmt(serverAmt);
        orderDetail.setSavingAmt(savingAmt);
        orderDetail.setVp(vp);
        orderDetails.add(orderDetail);
      }

      // vp值
      orderHeader.setVp(vpTotal);
      // 返利
      orderHeader.setPromoAmt(reductionTotal);
      orderHeader.setUsePoints(key.getPaidCommission());
      orderHeaders.add(orderHeader);
    }
      //设置新人单 jointype 最后一个新人子订单 当前单是第一单有效新人单 并且 没有jtype
      for (OrderHeader orderHeader : orderHeaders) {
          if(StringUtils.isNotBlank(freshmanMarkNo)
                  && freshmanMarkNo.equals(orderHeader.getOrderid())
                  && isFirstFreshmanOrder && (null == jtype || 0 ==jtype)){
              orderHeader.setJoinType(5);
          }
      }

    addOrder(orderDetails, orderHeaders);
  }
  //校验是不是首单新人单
  private boolean checkIsFirstFreshmanOrder(Long cpid){
    //查询该用户有没有下过有效新人单
    Integer freshmanOrderCount = freshManService.selectFreshmanOrder(cpid);
    if(0==freshmanOrderCount){
      return true;
    }else{
      return false;

    }
  }

  private Integer getStatus(OrderStatus orderStatus) {
    return OrderHeaderStatusMapping.mapping(orderStatus);
  }

  public Integer getMonth(Date date) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMM");
    Integer month = Integer.parseInt(simpleDateFormat.format(date));
    return month;
  }

  @Transactional
  public void addOrder(List<OrderDetail> orderDetails, List<OrderHeader> orderHeaders) {
    orderDetailMapper.batchInsert(orderDetails);
    orderHeaderMapper.batchInsert(orderHeaders);
  }

  @Override
  public Long loadUnPaidOrder(String payNo) {
    return mainOrderMapper.selectUnPaid(payNo);
  }

  /**
   * 增加大会赠品
   */
  private List<OrderItem> addGiftOrderItem(List<OrderItem> srcOrderItems, Order order) {
    if (CollectionUtil.isEmpty(srcOrderItems)) {
      return srcOrderItems;
    }
    List<OrderItem> newItems = new ArrayList<>();
    for (OrderItem item : srcOrderItems) {
      newItems.add(item);
      if (!OrderSortType.MEETING.equals(order.getOrderType())) {
        continue;
      }
      //查询该商品Code
      Sku sku = skuMapper.selectByPrimaryKey(item.getSkuId());
      if (null == sku) {
        continue;
      }
      //检查这个商品有无活动，没有则认为是没有赠品
      List<PromotionSkuVo> promotionSkuVos = promotionSkusService
              .getPromotionSkus(sku.getSkuCode());
      if (org.apache.commons.collections.CollectionUtils.isEmpty(promotionSkuVos)) {

        continue;
      }
      //普通活动商品设置meeting
      item.setPromotionType(PromotionType.MEETING);
      PromotionSkuVo promotionSkuVo = promotionSkuVos.get(0);//TODO 活动判断
      //检查这个活动商品有没有赠品，
      List<PromotionSkuGift> giftList = promotionSkuGiftService
              .getPromotionSkuGiftList(promotionSkuVo.getpCode(), promotionSkuVo.getSkuCode());
      if (org.apache.commons.collections.CollectionUtils.isEmpty(giftList)) {

        continue;
      }
      //买赠信息 （目前为买gift赠1）
      int gift = promotionSkuVos.get(0).getGift();
      if (gift <= 0) {
        continue;
      }
      int giftCount = 0;
      //计算赠品数量
      if (gift > 0) {
        //舍掉小数取整
        giftCount = (int) Math.floor(item.getAmount() / gift);
        if (giftCount < 1) {
          continue;
        }
      }
      OrderItem newItem = copyOrderItem(item);
      String skuName = newItem.getProductName();
      newItem.setAmount(giftCount);
      newItem.setProductName("【赠品】" + skuName);
      newItems.add(newItem);
    }
    return newItems;
  }

  /**
   * 增加大会赠品
   */
  private OrderItem copyOrderItem(OrderItem srcOrderItem) {
    OrderItem destOrderItem = new OrderItem();
    BeanUtils.copyProperties(srcOrderItem, destOrderItem);

    //`price` decimal(12,2) NOT NULL COMMENT '折扣价',
    destOrderItem.setPrice(new BigDecimal(0));
    //`market_price` decimal(12,2) DEFAULT NULL COMMENT '原价',
    destOrderItem.setMarketPrice(new BigDecimal(0));
    //`discount` decimal(18,2) DEFAULT '0.00' COMMENT '优惠金额',
    destOrderItem.setDiscount(new BigDecimal(0));
    //`discount_price` decimal(18,2) DEFAULT '0.00' COMMENT '优惠价',
    destOrderItem.setDiscountPrice(new BigDecimal(0));
    //`paid_point` decimal(10,2) DEFAULT '0.00',
    destOrderItem.setPaidPoint(new BigDecimal(0));
    //`paid_commission` decimal(10,2) DEFAULT '0.00',
    destOrderItem.setPaidCommission(new BigDecimal(0));
    //`reduction` decimal(18,2) NOT NULL DEFAULT '0.00' COMMENT '立减',
    destOrderItem.setReduction(new BigDecimal(0));
    //`promo_amt` decimal(18,2) NOT NULL DEFAULT '0.00' COMMENT '推广费',
    destOrderItem.setPromoAmt(new BigDecimal(0));
    //`server_amt` decimal(18,2) NOT NULL DEFAULT '0.00' COMMENT '服务费',
    destOrderItem.setServerAmt(new BigDecimal(0));
    destOrderItem.setPromotionType(PromotionType.MEETING_GIFT);
    return destOrderItem;
  }

  /**
   * 保存活动订单
   */
  private void savePromotionOrderDetail(Supplier<PromotionOrderDetail> detailSupplier) {
    PromotionOrderDetail detail = detailSupplier.get();
    if (detail != null) {
      promotionOrderDetailMapper.insert(detail);
    }
  }
  
  @Override
  @Transactional
  public int cancelMainOrderIfOrderCancelled(String id) {
	  return mainOrderMapper.cancelMainOrderIfOrderCancelled(id);
  }

}
