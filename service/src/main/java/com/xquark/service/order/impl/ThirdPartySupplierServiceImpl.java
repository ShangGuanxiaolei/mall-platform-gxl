package com.xquark.service.order.impl;

import com.xquark.dal.mapper.ThirdPartySupplierMapper;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.SupplierLogisticsExport;
import com.xquark.dal.vo.SupplierSkuExportVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.order.ThirdPartySupplierService;
import com.xquark.utils.ExcelUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.xquark.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("thirdPartySupplierOrderService")
public class ThirdPartySupplierServiceImpl extends BaseServiceImpl implements
    ThirdPartySupplierService {

  @Autowired
  private ThirdPartySupplierMapper thirdPartySupplierMapper;

  public List<OrderVO> selectOrders4WxExport(String params) {
    return thirdPartySupplierMapper.selectOrders4WxExport(params);
  }

  //=======================================以下是excel导入============================================================

  /**
   * 该部分接受文件路径的方式
   * @param inputStream
   * @return
   */
  public Boolean importExcelToInsert(InputStream inputStream){
    String[][] line = null;
    try {
      line=ExcelUtils.excelImport(inputStream);//把excel的数据都作为二维数组读取出来
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (line == null || line.length == 0) {
      throw new RuntimeException("excelData is null");
    }
    List<SupplierSkuExportVO> supplierSkuExports = new ArrayList<>();
    String Message="";
    try {
      for (int j=1;j<line.length;j++) {
        SupplierSkuExportVO supplierSkuExport = new SupplierSkuExportVO();
        for (int i=0;i<line[j].length;i++) {
          if(line[j][i]==null||line[j][i].equals("")){
            continue;
          }
          switch (i){
            case 0:
              Message="第"+(j+1)+"行第"+(i+1)+"列的编码规格出现错误";
              supplierSkuExport.setSkuCode(line[j][i]);
              break;
            case 1:
              Message="第"+(j+1)+"行第"+(i+1)+"列的规格条码出现错误";
              supplierSkuExport.setBarCode(line[j][i]);
              break;
            case 2:
              Message="第"+(j+1)+"行第"+(i+1)+"列的规格名称出现错误";
              supplierSkuExport.setProductName(line[j][i]);
              break;
            case 3:
              Message="第"+(j+1)+"行第"+(i+1)+"列的库存出现错误";
              if(line[j][i]==null){
                line[j][i]="";
              }
              supplierSkuExport.setAmount(Integer.parseInt(line[j][i]));
              break;
          }
        }
        supplierSkuExports.add(supplierSkuExport);
      }
      Message="success";
      importExcel(supplierSkuExports);
    }catch (Exception e){
      if(!Message.equals("success")){
        throw new RuntimeException(Message);
      }else{
        e.printStackTrace();
      }
    }
    return null;
  }

  /**
   * 把execl中读出来的数据插入到数据库中
   * @param supplierSkuExports
   */
  @Transactional
  public void importExcel(List<SupplierSkuExportVO> supplierSkuExports){
    for (SupplierSkuExportVO sku : supplierSkuExports) {
      //mapper的更新方法
      thirdPartySupplierMapper.updateSkuAmount(sku.getSkuCode(),sku.getBarCode(),sku.getAmount());
    }
  }

  /**
   * 物流信息导入
   * @param inputStream
   * @return
   */
  public Boolean importExcelToUpdate(InputStream inputStream){
    String[][] line =null;
    try {
      line=ExcelUtils.excelImport(inputStream);//把excel的数据都作为二维数组读取出来
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (line == null || line.length == 0) {
      throw new RuntimeException("excelData is null");
    }
    //新模板有10列
    if(line[0].length<10){
      throw new RuntimeException("导入失败，请尝试更新模板");
    }

    List<SupplierLogisticsExport> supplierLogisticsExports = new ArrayList<>();
    String Message="";
    try {
      for (int j=1;j<line.length;j++) {//从第二行开始
        SupplierLogisticsExport logistics = new SupplierLogisticsExport();
        String multiWaybill = "";

        for (int i=0;i<line[j].length;i++) {
//          if(line[j][i]==null||line[j][i].equals("") || line[j][i].isEmpty()){
//            continue;
//          }

          switch (i){
            case 0:
              Message="第"+(j+1)+"行第"+(i+1)+"列的订单编号出现错误";
              logistics.setOrderNo(line[j][i]);
              break;
            case 1:
              Message="第"+(j+1)+"行第"+(i+1)+"列的下单时间出现错误";
              break;
            case 2:
              Message="第"+(j+1)+"行第"+(i+1)+"列的收件人姓名出现错误";
              break;
            case 3:
              Message="第"+(j+1)+"行第"+(i+1)+"列的收件人电话出现错误";
              break;
            case 4:
              Message="第"+(j+1)+"行第"+(i+1)+"列的快递公司名称出现错误";
              logistics.setLogisticsCompany(line[j][i]);
              break;
            case 5:
              Message="第"+(j+1)+"行第"+(i+1)+"列的快递单号1出现错误";
              if(null!=line[j][i] && !"".equals(line[j][i])){
                multiWaybill = multiWaybill+line[j][i];
              }
              break;
            case 6:
              Message="第"+(j+1)+"行第"+(i+1)+"列的快递单号2出现错误";
              if(null!=line[j][i] && !"".equals(line[j][i])){
                multiWaybill = multiWaybill+","+line[j][i];
              }
              break;
            case 7:
              Message="第"+(j+1)+"行第"+(i+1)+"列的快递单号3出现错误";
              if(null!=line[j][i] && !"".equals(line[j][i])){
                multiWaybill = multiWaybill+","+line[j][i];
              }
              break;
            case 8:
              Message="第"+(j+1)+"行第"+(i+1)+"列的快递单号4出现错误";
              if(null!=line[j][i] && !"".equals(line[j][i])){
                multiWaybill = multiWaybill+","+line[j][i];
              }
              break;

            case 9:
              Message="第"+(j+1)+"行第"+(i+1)+"列的快递单号5出现错误";
              if(null!=line[j][i] && !"".equals(line[j][i])){
                multiWaybill = multiWaybill+","+line[j][i];
              }
              logistics.setLogisticsOrderNo(multiWaybill);
              break;
          }
        }
        if (StringUtil.isNotNull(logistics.getLogisticsOrderNo()) && StringUtil.isNotNull(logistics.getLogisticsOrderNo())) {
          supplierLogisticsExports.add(logistics);
        }
      }
      Message="success";
      importLogisticsExcel(supplierLogisticsExports);
    }catch (Exception e){
      if(!Message.equals("success")){
        throw new RuntimeException(Message);
      }else{
        e.printStackTrace();
      }
    }
    return null;
  }

  /**
   * 把execl中读出来的数据插入到数据库中
   * @param logisticsList
   */
  @Transactional
  public void importLogisticsExcel(List<SupplierLogisticsExport> logisticsList){
    for (SupplierLogisticsExport logistics : logisticsList) {
      //mapper的更新方法,注意对应关系
      thirdPartySupplierMapper.updateLogisticsInfo(logistics.getOrderNo(),
          logistics.getLogisticsCompany(),logistics.getLogisticsOrderNo());
    }
  }
  /**
   *baseServiceImpl中的方法，暂时不用
   * @param orderNo
   * @return
   */
  @Override
  public OrderVO loadByOrderNo(String orderNo) {
    return null;
  }
}
