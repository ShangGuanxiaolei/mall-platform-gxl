package com.xquark.service.order.util;


import com.xquark.dal.model.Supplier;
import com.xquark.service.order.impl.OrderSplitterServiceImpl.OrderDeliveryCondition;
import com.xquark.service.order.impl.OrderSplitterServiceImpl.OrderDeliveryConditionImpl;
import com.xquark.service.order.util.impl.*;

import java.lang.reflect.Proxy;


/**
 * 代理工厂
 *
 * @author jackzhu
 * @since 2018/11/14
 */
public class OrderDeliveryConditionProxyFactory {

    private OrderDeliveryConditionProxyFactory() {
    }

    private static final String MEET_METHOD = "meet";
    private static final String ORDER_METHOD = "order";
    private static final String HASH_CODE_METHOD = "hashCode";

    public static final String BACK_ENABLE = "_REFUNDABLE";
    public static final String BACK_UNABLE = "_NOT_REFUNDABLE";


    public static OrderDeliveryCondition getEnableBackOderDeliveryCondition(final Supplier supplier) {

        return getOrderDeliveryCondition(supplier, BACK_ENABLE);

    }

    public static OrderDeliveryCondition getUnableBackOderDeliveryCondition(final Supplier supplier) {

        return getOrderDeliveryCondition(supplier, BACK_UNABLE);

    }

    private static OrderDeliveryCondition getOrderDeliveryCondition(final Supplier supplier,
                                                                    final String backAble) {
        return (OrderDeliveryCondition) Proxy
                .newProxyInstance(OrderDeliveryConditionImpl.class.getClassLoader(),
                        new Class[]{OrderDeliveryCondition.class},
                        (proxy, method, args) -> {
                            final AbstractProxyMethodStrategy proxyMethodStrategy;
                            final ProxyContext proxyContext = new ProxyContext(proxy, method, args, supplier,
                                    backAble);

                            switch (method.getName()) {
                                case MEET_METHOD:
                                    proxyMethodStrategy = new MeetMethodStrategy(proxyContext);
                                    break;
                                case ORDER_METHOD:
                                    proxyMethodStrategy = new OrderMethodStrategy(proxyContext);
                                    break;
                                case HASH_CODE_METHOD:
                                    proxyMethodStrategy = new HashCodeMethodStrategy(proxyContext);
                                    break;
                                default:
                                    proxyMethodStrategy = new DefaultMethodStrategy(proxyContext);
                                    break;
                            }
                            return proxyMethodStrategy.execute();
                        });
    }

}

