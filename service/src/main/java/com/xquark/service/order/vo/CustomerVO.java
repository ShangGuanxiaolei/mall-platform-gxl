package com.xquark.service.order.vo;

import java.util.List;

import com.xquark.dal.vo.Customer;
import com.xquark.dal.vo.OrderVO;

public class CustomerVO extends Customer {

  private List<OrderVO> orders;

  public List<OrderVO> getOrders() {
    return orders;
  }

  public void setOrders(List<OrderVO> orders) {
    this.orders = orders;
  }

}
