package com.xquark.service.order.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.consts.SFApiConstants;
import com.xquark.dal.mapper.SfAppConfigMapper;
import com.xquark.dal.mapper.SystemRegionMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.ProductSource;
import com.xquark.service.http.pojo.sf.product.SFProductWrapper;
import com.xquark.service.logistics.LogisticsGoodsService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.SfRegionService;
import com.xquark.service.order.vo.OrderForThirdVO;
import com.xquark.service.order.vo.OrderProductVO;
import com.xquark.service.product.impl.SFProductSign;
import com.xquark.service.vo.ShippingGoodsAllParam;
import com.xquark.service.vo.ShippingGoodsParam;
import com.xquark.utils.HttpClientUtil;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * 异步提交订单到第三方 例如顺丰
 */
@Component
public class AsyncSubmitOrder {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Async
  public void asyncSubmit(Map<Order, List<OrderItem>> splitedOrders, OrderAddress orderAddress,
      SfAppConfigMapper sfConfigMapper, String profile, String host,
      SfRegionService sfRegionService, SystemRegionMapper systemRegionMapper,
      OrderService orderService,LogisticsGoodsService logisticsGoodsService) {
    for (Map.Entry<Order, List<OrderItem>> entry : splitedOrders.entrySet()) {
      Order order = entry.getKey();
      ProductSource productSource = order.getSource();

      List<OrderItem> orderItemList = entry.getValue();
      if (productSource == ProductSource.SF) {
        sfInfo(order, orderItemList, orderAddress, sfConfigMapper, profile, host, sfRegionService,
            systemRegionMapper, orderService,logisticsGoodsService);
      }
    }
  }

  private void sfInfo(Order order, List<OrderItem> orderItemList, OrderAddress orderAddress,
      SfAppConfigMapper sfConfigMapper, String profile, String host,
      SfRegionService sfRegionService, SystemRegionMapper systemRegionMapper,
      OrderService orderService,LogisticsGoodsService logisticsGoodsService) {
    OrderForThirdVO orderForThirdVO = new OrderForThirdVO();
    orderForThirdVO.setOuterId(order.getOrderNo());
    orderForThirdVO.setPayId(1);
    orderForThirdVO.setDevice(28);
    orderForThirdVO.setOrderSource(28);
    orderForThirdVO.setAddress(orderAddress.getStreet());
    orderForThirdVO.setMobile(orderAddress.getPhone());
    orderForThirdVO.setConsignee(orderAddress.getConsignee());

    int payTime = Integer.parseInt(String.valueOf(order.getPaidAt().getTime() / 1000));
    orderForThirdVO.setPayTime(payTime);

    orderForThirdVO.setMoneyPaid(order.getTotalFee().intValue() * 100);

    int addTime = Integer.parseInt(String.valueOf(System.currentTimeMillis() / 1000));
    orderForThirdVO.setAddTime(addTime);
    orderForThirdVO.setPreSale(false);

    orderForThirdVO.setShippingFee(order.getLogisticsFee().multiply(new BigDecimal(100)).intValue());

    List<SystemRegion> regionDictList = systemRegionMapper
            .listParents(orderAddress.getZoneId());
    if (regionDictList != null && !regionDictList.isEmpty()) {
      int len = regionDictList.size();
      Long regionId = 0l;
      List<Long> regionList = new ArrayList<Long>();
      for (int i = 0; i < len; i++) {
        regionId = regionDictList.get(i).getId();
        regionList.add(regionId);
      }
      Long Province = sfRegionService.getsfid(regionList.get(0));
      Long City = sfRegionService.getsfid(regionList.get(1));
      Long District = sfRegionService.getsfid(regionList.get(2));
      //select * from xquark_system_sf_region where id in (3004,3005,3006)
      orderForThirdVO.setProvince(Province.intValue());
      orderForThirdVO.setCity(City.intValue());
      orderForThirdVO.setDistrict(District.intValue());
      orderForThirdVO.setArea(0);
    }

    List<ShippingGoodsParam> shippingGoodsParams = new ArrayList<ShippingGoodsParam>();

    List<OrderProductVO> orderProductVOList = new ArrayList<OrderProductVO>();
    BigDecimal productAmount = new BigDecimal(0);
    for (OrderItem orderItem : orderItemList) {
      OrderProductVO orderProductVO = new OrderProductVO();
      orderProductVO.setProductSn(orderItem.getProduct().getEncode());
      orderProductVO.setProductNum(orderItem.getAmount());
      orderProductVO.setProductType(0);

      BigDecimal partnerProductPrice = new BigDecimal(0);
      if (null != orderItem.getSku() && null != orderItem.getSku().getPartnerProductPrice()) {
          partnerProductPrice = orderItem.getSku().getPartnerProductPrice();
      }

      orderProductVO
          .setSellPrice(
                  partnerProductPrice.multiply(new BigDecimal(100)).intValue());
      productAmount = productAmount
          .add(partnerProductPrice
              .multiply(new BigDecimal(orderItem.getAmount())));
      orderProductVOList.add(orderProductVO);

      ShippingGoodsParam shippingGoodsParam = new ShippingGoodsParam();
      shippingGoodsParam.setSellPrice(Double.parseDouble(String.valueOf(orderProductVO.getSellPrice())));
      shippingGoodsParam.setProductSn(orderItem.getProduct().getEncode());
      shippingGoodsParam.setProductIndex(orderItem.getProduct().getEncode());
      shippingGoodsParam.setWeight(Double.parseDouble(String.valueOf(orderItem.getProduct().getWeight())));
      shippingGoodsParam.setProductId(IdTypeHandler.decode(orderItem.getProductId()));
      shippingGoodsParam.setRegionId(orderForThirdVO.getProvince());
      shippingGoodsParam.setCityId(orderForThirdVO.getCity());
      shippingGoodsParam.setSfshipping(String.valueOf(orderItem.getProduct().getSfshipping()));
      shippingGoodsParam.setMerchantNumber(orderItem.getProduct().getMerchantNumber());

      shippingGoodsParams.add(shippingGoodsParam);
    }

    ShippingGoodsAllParam shippingGoodsAllParam = new ShippingGoodsAllParam();
    shippingGoodsAllParam.setShippingGoodsParams(shippingGoodsParams);
    shippingGoodsAllParam.setUserrank(10);
    //计算运费 创建订单1.0版规则更改,大于等于99元运费为0,小于99元运费为10元,单位：分
    log.info("商品总价：{}",productAmount.intValue());
    if(productAmount.intValue() >= 99){
      orderForThirdVO.setShippingFee(0);
    }else {
      orderForThirdVO.setShippingFee(1000);
    }
    log.info("顺丰商品运费：{}",orderForThirdVO.getShippingFee());

//    ShippingGoodsAll shippingGoodsAll = logisticsGoodsService.getShippingFee(shippingGoodsAllParam);
//    if (null != shippingGoodsAll && null != shippingGoodsAll.getTotalshippingfee()) {
//      orderForThirdVO.setShippingFee(shippingGoodsAll.getTotalshippingfee());
//    }

    orderForThirdVO.setProductAmount(productAmount.multiply(new BigDecimal(100)).intValue());
    orderForThirdVO.setOrderAmount(orderForThirdVO.getProductAmount() + orderForThirdVO.getShippingFee());

    orderForThirdVO.setOrderProducts(orderProductVOList);
    orderForThirdVO.setMoneyPaid(orderForThirdVO.getOrderAmount());
    orderForThirdVO.setIsList(0);

    SfAppConfig appConfig = sfConfigMapper.selectByProfile(profile);


    String json = JSON.toJSONString(orderForThirdVO);
    //提交订单升级2.0
    String pushOrderUrl = this.basePushOrderUrl(appConfig, json,host);

    String res = this.requestProducts(pushOrderUrl, json);

//    String orderMsg = res.getContent();
    JSONObject jsonObject = JSON.parseObject(res);
    if (null != jsonObject) {
      JSONObject value = jsonObject.getJSONObject("data");
      if ("0".equals(jsonObject.getString("code"))) {
        Order orderNext = new Order();
        orderNext.setOrderNo(order.getOrderNo());
        orderNext.setLogisticsCompany("顺丰");
        orderNext.setPartnerOrderNo(value.getString("orderSn"));
        orderNext.setStatus(OrderStatus.DELIVERY);
        orderService.updateOrderByOrderNo(orderNext);
        log.info("顺丰订单参数：{}，返回值：{}", orderForThirdVO.toString(), res);
      } else {
//      orderService.refund(order.getId());
        log.error("order push error:" + jsonObject.get("msg"));
//      throw new BizException(GlobalErrorCode.PRODUCT_INVENTORY, "商品库存不足，请重新下单");
      }
    }
  }

    /**
     * 获取顺丰商品2.0版
     * @param url
     * @return
     */
    private String requestProducts(String url, String json) {
        log.info("请求地址 {}", url);
        log.info("请求参数 {}", json);
        String result = HttpClientUtil.doPost(url, json, "utf-8");
        JSONObject jsonObject = JSONObject.parseObject(result);
        log.info("请求顺丰返回结果： {}", jsonObject);
        return result;
    }

  /**
   * 生成SF公共参数 2.0签名
   * @param config
   * @return
   */
  public String generateOrderSign(SfAppConfig config,String json){
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("format=json");
    stringBuilder.append("&timestamp="+System.currentTimeMillis()/1000);
    stringBuilder.append("&version=1.0");
    stringBuilder.append("&"+config.getClientId());
    stringBuilder.append("&"+config.getAccessToken());
    stringBuilder.append("&" + json);
    log.info("顺丰签名URL: " + stringBuilder);
    MessageDigest md = null;
    try {
      md = MessageDigest.getInstance("MD5");
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    String sign = Base64.getUrlEncoder().encodeToString(md.digest(stringBuilder.toString().getBytes(StandardCharsets.UTF_8)));
    return sign;
  }

  /**
   * 获取顺丰商品公共参数URL，使用商品编码查询商品信息
   * @param config
   * @return
   */
  public String basePushOrderUrl(SfAppConfig config,String json,String host){

    String pushOrderUrl = String.format("%s?app_key=%s&access_token=%s&timestamp=%s&sign=%s&format=json&version=1.0",
            host + SFApiConstants.CREATE_ORDER, config.getClientId(),
            config.getAccessToken(),
            String.valueOf(System.currentTimeMillis()/1000),
            this.generateOrderSign(config,json));
    log.info("请求顺丰获取商品URL:" + pushOrderUrl);
    return pushOrderUrl;
  }

}
