package com.xquark.service.order;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.OrderItemComment;
import com.xquark.service.BaseEntityService;

public interface OrderItemCommentService extends BaseEntityService<OrderItemComment> {

  int sellerReply(String id, String reply);

  /**
   * get comments by product
   */
  List<OrderItemComment> selectByProductId(String productId, Pageable pager);

  /**
   * count comments by product
   */
  Long countByProductId(String productId);

  OrderItemComment selectLastestByProductId(String productId);

  Long countByBuyerIdAndSellerId(String buyerId, String sellerId);

  List<OrderItemComment> selectByEntityWithPage(OrderItemComment orderItemComment, Pageable pager);

  List<OrderItemComment> selectByOrderItemId(String orderId);

  Long countNewOrderItemReply(String sellerId, Date startTime);

  BigDecimal selectProcuctScoreAvg(String productId);

  Boolean checkExistsByOrderId(String orderId);

  List<OrderItemComment> selectUnreadBySellerId(String sellerId, Pageable pager);

  /**
   * 删除评论
   */
  int delete(String id);

  /**
   * 删除评论的回复信息
   */
  int emptyReply(String id);


}
