package com.xquark.service.order.vo;

import java.util.List;

import org.springframework.beans.BeanUtils;

import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.model.Zone;

public class OrderAddressVO extends OrderAddress {

  private static final long serialVersionUID = -1565907675400698132L;

  private String details;

  private List<Zone> zones;

  public OrderAddressVO() {
  }

  public OrderAddressVO(OrderAddress address, String details) {
    BeanUtils.copyProperties(address, this);
    this.details = details;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }

  public List<Zone> getZones() {
    return zones;
  }

  public void setZones(List<Zone> zones) {
    this.zones = zones;
  }
}
