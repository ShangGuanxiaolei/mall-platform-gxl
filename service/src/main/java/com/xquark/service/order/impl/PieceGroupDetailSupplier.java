package com.xquark.service.order.impl;

import com.google.common.base.Supplier;
import com.xquark.dal.model.mypiece.PGMemberInfoVo;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.service.pricing.impl.pricing.PieceOrderExtra;

/**
 * @author wangxinhua.
 * @date 2018/11/3
 */
public class PieceGroupDetailSupplier implements Supplier<PromotionOrderDetail> {

  private final String orderNo;
  private final String subOrderNo;
  private final String orderItemNo;
  private final Long cpId;
  private final PieceOrderExtra pieceOrderExtra;

  public PieceGroupDetailSupplier(String orderNo, String subOrderNo, String orderItemNo,
      Long cpId, PieceOrderExtra pieceOrderExtra) {
    this.orderNo = orderNo;
    this.subOrderNo = subOrderNo;
    this.orderItemNo = orderItemNo;
    this.cpId = cpId;
    this.pieceOrderExtra = pieceOrderExtra;
  }

  @Override
  public PromotionOrderDetail get() {
    if (pieceOrderExtra == null) {
      return null;
    }
    PGTranInfoVo pgTranInfoVo = pieceOrderExtra.getPgTranInfoVo();
    PGMemberInfoVo pgMemberInfoVo = pieceOrderExtra.getPgMemberInfoVo();
    PromotionOrderDetail promotionOrderDetail = new PromotionOrderDetail();
    promotionOrderDetail.setOrder_head_code(orderNo);
    promotionOrderDetail.setSub_order_no(subOrderNo);
    promotionOrderDetail.setOrder_detail_code(String.valueOf(IdTypeHandler.decode(orderItemNo)));
    //活动编码
    promotionOrderDetail.setP_code(pgTranInfoVo.getpCode());
    //活动详情编码
    promotionOrderDetail.setP_detail_code(pgTranInfoVo.getpDetailCode());
    promotionOrderDetail.setPiece_group_tran_code(pgTranInfoVo.getPieceGroupTranCode());
    promotionOrderDetail.setPiece_group_detail_code(pgMemberInfoVo.getPieceGroupDetailCode());
    promotionOrderDetail.setMember_id(cpId);
    //活动类型
    promotionOrderDetail.setP_type("piece");
    promotionOrderDetail.setIs_cut_line(pieceOrderExtra.getIsCutInLine());
    return promotionOrderDetail;
  }

}
