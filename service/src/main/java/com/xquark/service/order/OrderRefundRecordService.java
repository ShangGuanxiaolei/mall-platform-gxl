package com.xquark.service.order;

import java.util.List;

import com.xquark.dal.model.OrderRefundRecord;

public interface OrderRefundRecordService {


  List<OrderRefundRecord> listOrderByBatchNo(String batchNo);

  void saveOrderRecord(OrderRefundRecord alipayOrder);

  void saveOrderRecord(List<OrderRefundRecord> lsOrder);

  List<OrderRefundRecord> listOrderByBatchNoAndTradeNo(List<OrderRefundRecord> lsRecord,
      String tradeNo);

  List<String> listBatchNoByOrderNo(String orderNo);

}
