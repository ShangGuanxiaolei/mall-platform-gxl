package com.xquark.service.order.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.OrderAddressMapper;
import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.model.Zone;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderAddressService;
import com.xquark.service.order.vo.OrderAddressVO;
import com.xquark.service.zone.ZoneService;

@Service("orderAddressService")
public class OrderAddressServiceImpl extends BaseServiceImpl implements OrderAddressService {

  @Autowired
  private OrderAddressMapper orderAddressMapper;

  @Autowired
  private ZoneService zoneService;


  @Override
  public int insert(OrderAddress e) {
    return orderAddressMapper.insert(e);
  }

  @Override
  public int insertOrder(OrderAddress orderAddress) {
    return orderAddressMapper.insert(orderAddress);
  }

  @Override
  public OrderAddress load(String id) {
    return orderAddressMapper.selectByPrimaryKey(id);
  }

	/*@Override
	public int archiveAddress(String addressId) {
		return orderAddressMapper.updateForArchive(addressId);
	}*/

  @Override
  public OrderAddressVO selectByOrderId(String orderId) {
    OrderAddress address = orderAddressMapper.selectByOrderId(orderId);
    if (address == null) {
      return null;
    }
    List<Zone> zoneList = zoneService.listParents(address.getZoneId());
    String details = "";
    for (Zone zone : zoneList) {
      details += zone.getName();
    }
    details += address.getStreet();
    return new OrderAddressVO(address, details);
  }

  @Override
  public int update(OrderAddress orderAddress) {
    if (StringUtils.isBlank(orderAddress.getId())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "订单地址的id不能为空");
    }

    OrderAddress db = orderAddressMapper.selectByPrimaryKey(orderAddress.getId());
    if (db == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "订单地址不存在");
    }

    if (!StringUtils.isBlank(orderAddress.getConsignee())) {
      db.setConsignee(orderAddress.getConsignee());
    }

    if (!StringUtils.isBlank(orderAddress.getPhone())) {
      db.setPhone(orderAddress.getPhone());
    }

    if (!StringUtils.isBlank(orderAddress.getStreet())) {
      db.setStreet(orderAddress.getStreet());
    }

    if (!StringUtils.isBlank(orderAddress.getWeixinId())) {
      db.setWeixinId(orderAddress.getWeixinId());
    }

    if (!StringUtils.isBlank(orderAddress.getZoneId())) {
      db.setZoneId(orderAddress.getZoneId());
    }
    return orderAddressMapper.update(db);
  }

}
