package com.xquark.service.order.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 与第三方对接订单
 */
public class OrderForThirdVO {

  private String outerId;//系统订单唯一标识
  private int payId;//支付方式
  private int device;//订单来源一直，默认为28
  private int orderSource;//订单来源 28：开放平台订单
  private int province;//省ID
  private int city;//市ID
  private int district;//县、区ID
  private int area;//街道ID
  private String address;//收货地址
  private String telNumber;//座机号
  private String mobile;//用户手机号
  private String consignee;//收货人
  private int payTime;//支付时间
  private int moneyPaid;//已支付金额
  private int addTime;//订单生成时间
  private Boolean preSale;//是否预售
  private int orderAmount;//订单总金额
  private int productAmount;//商品总金额
  private int shippingFee;//运费
  private int isList;//是否显示购物清单
  private BigDecimal partnerProductPrice;//供货价格
  private List<OrderProductVO> orderProducts;//订单商品信息

  public String getOuterId() {
    return outerId;
  }

  public void setOuterId(String outerId) {
    this.outerId = outerId;
  }

  public int getPayId() {
    return payId;
  }

  public void setPayId(int payId) {
    this.payId = payId;
  }

  public int getDevice() {
    return device;
  }

  public void setDevice(int device) {
    this.device = device;
  }

  public int getOrderSource() {
    return orderSource;
  }

  public void setOrderSource(int orderSource) {
    this.orderSource = orderSource;
  }

  public int getProvince() {
    return province;
  }

  public void setProvince(int province) {
    this.province = province;
  }

  public int getCity() {
    return city;
  }

  public void setCity(int city) {
    this.city = city;
  }

  public int getDistrict() {
    return district;
  }

  public void setDistrict(int district) {
    this.district = district;
  }

  public int getArea() {
    return area;
  }

  public void setArea(int area) {
    this.area = area;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getTelNumber() {
    return telNumber;
  }

  public void setTelNumber(String telNumber) {
    this.telNumber = telNumber;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getConsignee() {
    return consignee;
  }

  public void setConsignee(String consignee) {
    this.consignee = consignee;
  }

  public int getPayTime() {
    return payTime;
  }

  public void setPayTime(int payTime) {
    this.payTime = payTime;
  }

  public int getMoneyPaid() {
    return moneyPaid;
  }

  public void setMoneyPaid(int moneyPaid) {
    this.moneyPaid = moneyPaid;
  }

  public int getAddTime() {
    return addTime;
  }

  public void setAddTime(int addTime) {
    this.addTime = addTime;
  }

  public Boolean getPreSale() {
    return preSale;
  }

  public void setPreSale(Boolean preSale) {
    this.preSale = preSale;
  }

  public int getOrderAmount() {
    return orderAmount;
  }

  public void setOrderAmount(int orderAmount) {
    this.orderAmount = orderAmount;
  }

  public int getProductAmount() {
    return productAmount;
  }

  public void setProductAmount(int productAmount) {
    this.productAmount = productAmount;
  }

  public int getShippingFee() {
    return shippingFee;
  }

  public void setShippingFee(int shippingFee) {
    this.shippingFee = shippingFee;
  }

  public int getIsList() {
    return isList;
  }

  public void setIsList(int isList) {
    this.isList = isList;
  }

  public List<OrderProductVO> getOrderProducts() {
    return orderProducts;
  }

  public void setOrderProducts(List<OrderProductVO> orderProducts) {
    this.orderProducts = orderProducts;
  }

  public BigDecimal getPartnerProductPrice() {
    return partnerProductPrice;
  }

  public void setPartnerProductPrice(BigDecimal partnerProductPrice) {
    this.partnerProductPrice = partnerProductPrice;
  }

  @Override
  public String toString() {
    return "OrderForThirdVO{" +
        "outerId='" + outerId + '\'' +
        ", payId=" + payId +
        ", device=" + device +
        ", orderSource=" + orderSource +
        ", province=" + province +
        ", city=" + city +
        ", district=" + district +
        ", area=" + area +
        ", address='" + address + '\'' +
        ", telNumber='" + telNumber + '\'' +
        ", mobile='" + mobile + '\'' +
        ", consignee='" + consignee + '\'' +
        ", payTime=" + payTime +
        ", moneyPaid=" + moneyPaid +
        ", addTime=" + addTime +
        ", preSale=" + preSale +
        ", orderAmount=" + orderAmount +
        ", productAmount=" + productAmount +
        ", shippingFee=" + shippingFee +
        ", isList=" + isList +
        ", partnerProductPrice=" + partnerProductPrice +
        ", orderProducts=" + orderProducts +
        '}';
  }
}
