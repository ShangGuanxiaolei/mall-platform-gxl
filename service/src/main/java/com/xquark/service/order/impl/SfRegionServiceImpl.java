package com.xquark.service.order.impl;

import com.xquark.dal.mapper.RegionDict;
import com.xquark.dal.mapper.RegionMapper;
import com.xquark.service.order.SfRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 顺丰服务
 */
@Service("sfRegionService")
public class SfRegionServiceImpl implements SfRegionService {

  @Autowired
  private RegionMapper regionMapper;

  public List<RegionDict> queryAllParentAndSelf(int regionId) {
    return regionMapper.queryAllParentAndSelf(regionId);
  }

  @Override
  public Long getsfid(Long zoneid) {
    return regionMapper.selectsfid(zoneid);
  }
}