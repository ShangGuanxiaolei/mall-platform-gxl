package com.xquark.service.order.util.impl;

import com.xquark.dal.model.Supplier;
import com.xquark.service.order.util.AbstractProxyMethodStrategy;
import com.xquark.service.order.util.OrderDeliveryConditionProxyFactory;
import com.xquark.service.order.util.ProxyContext;

/**
 * @author Jack Zhu
 * @date 2019/02/27
 */
public class InitMethodStrategy  extends AbstractProxyMethodStrategy {

    public InitMethodStrategy(ProxyContext proxyContext) {
        super(proxyContext);
    }

    @Override
    public Object execute() {
        final Supplier supplier = proxyContext.getSupplier();

        final String supplierName = initSkuCode(supplier);
        final String enableCodeResourceName = initErcEnum(supplier, OrderDeliveryConditionProxyFactory.BACK_ENABLE);
        final String unableCodeResourceName = initErcEnum(supplier, OrderDeliveryConditionProxyFactory.BACK_UNABLE);
        logger.info("动态加载枚举类型: {}\n", supplierName);
        logger.info("动态加载枚举类型: {}\n", enableCodeResourceName);
        logger.info("动态加载枚举类型: {}\n", unableCodeResourceName);
        return null;
    }
}
