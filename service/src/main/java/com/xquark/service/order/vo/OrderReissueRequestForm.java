package com.xquark.service.order.vo;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.validation.group.order.ApplyForRefundGroup;
import com.xquark.dal.validation.group.order.ApplyForReissueGroup;
import com.xquark.dal.validation.group.order.ProceedReissueGroup;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.util.List;

public class OrderReissueRequestForm {

  private String id;

  @ApiModelProperty(value = "订单id")
  @NotBlank(groups = {ApplyForReissueGroup.class, ProceedReissueGroup.class})
  private String orderId;

  @ApiModelProperty(value = "1代表仅退款，2代表退款退货,3代表换货,4代表补货")
  private Integer buyerRequire = 4;  //买家要求

  private Integer buyerReceived;  //买家是否已收到货,无需传入

  @NotBlank(groups = {ApplyForReissueGroup.class}, message = "补货原因不能为空")
  private String reissueReason;  //换货原因

  @Digits(integer = 10, fraction = 2, groups = {ApplyForRefundGroup.class}, message = "退款金额格式不正确")
  private BigDecimal refundFee;    //退款金额,此字段在换货中不需要使用，为避免报错添加

  @NotBlank(groups = {ProceedReissueGroup.class}, message = "补货商品不能为空")
  private List<OrderItem> orderItemList; //换货商品集合

  @Length(max = 100, groups = {ApplyForReissueGroup.class}, message = "补货说明的最大长度为100")
  private String reissueMemo;    //换货说明

  private String reissueImg; // 换货凭证（多张图片用,号区分）

  @NotBlank(groups = {ProceedReissueGroup.class}, message = "物流公司不能为空")
  private String logisticsCompany; // 物流公司

  @NotBlank(groups = {ProceedReissueGroup.class}, message = "物流编号不能为空")
  private String logisticsNo; // 物流编号

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public Integer getBuyerRequire() {
    return buyerRequire;
  }

  public void setBuyerRequire(Integer buyerRequire) {
    this.buyerRequire = buyerRequire;
  }

  public Integer getBuyerReceived() {
    return buyerReceived;
  }

  public void setBuyerReceived(Integer buyerReceived) {
    this.buyerReceived = buyerReceived;
  }

  public String getReissueReason() {
    return reissueReason;
  }

  public void setReissueReason(String reissueReason) {
    this.reissueReason = reissueReason;
  }

  public String getReissueMemo() {
    return reissueMemo;
  }

  public void setReissueMemo(String reissueMemo) {
    this.reissueMemo = reissueMemo;
  }

  public List<OrderItem> getOrderItemList() {
    return orderItemList;
  }

  public void setOrderItemList(List<OrderItem> orderItemList) {
    this.orderItemList = orderItemList;
  }

  public BigDecimal getRefundFee() {
    return refundFee;
  }

  public void setRefundFee(BigDecimal refundFee) {
    this.refundFee = refundFee;
  }

  public String getReissueImg() {
    return reissueImg;
  }

  public void setReissueImg(String reissueImg) {
    this.reissueImg = reissueImg;
  }

  public String getLogisticsCompany() {
    return logisticsCompany;
  }

  public void setLogisticsCompany(String logisticsCompany) {
    this.logisticsCompany = logisticsCompany;
  }

  public String getLogisticsNo() {
    return logisticsNo;
  }

  public void setLogisticsNo(String logisticsNo) {
    this.logisticsNo = logisticsNo;
  }
}
