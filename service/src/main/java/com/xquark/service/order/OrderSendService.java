package com.xquark.service.order;

import com.xquark.dal.model.OrderSend;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: a9175
 * Date: 2018/6/17.
 * Time: 15:04
 * ${DESCRIPTION}
 */
public interface OrderSendService {

    /**
     * 返回全部列表
     *
     * @return
     */
//    public List<OrderSend> findAll();


    /**
     * 增加
     */
    public void add(OrderSend orderSend);


    /**
     * 修改
     */
//    public void update(OrderSend orderSend);


    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */
//    public OrderSend findOne(Long id);


    /**
     * 批量删除
     *
     * @param ids
     */
//    public void delete(Long[] ids);


}
