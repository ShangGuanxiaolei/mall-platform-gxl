package com.xquark.service.order;

import com.google.common.base.Optional;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.ActivityGrouponStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.PayStatus;
import com.xquark.dal.type.*;
import com.xquark.dal.vo.OrderItemVO;
import com.xquark.dal.vo.PromotionGrouponVO;
import com.xquark.event.MessageNotifyEvent;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.groupon.ActivityGrouponService;
import com.xquark.service.msg.MessageService;
import com.xquark.service.ownerAdress.CustomerOwnerAdress;
import com.xquark.service.pricing.impl.pricing.impl.PgPromotionServiceAdapter;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.user.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.parboiled.common.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

public abstract class OrderWorkflow {

  /**
   * 订单流程返回值 0 失败 1 成功 2 成功（已经执行过）
   */
  public static int FAIL = 0;
  public static int SUCCESS = 1;
  public static int SUCCESSED = 2;

  public static final String LOGISTICS_COMPANY_PROPERTY = "logisticsCompany";
  public static final String LOGISTICS_ORDERNO_PROPERTY = "logisticsOrderNo";

  public static final String REFUND_FEE_PROPERTY = "refundFee";
  public static final String REFUND_GOODS_FEE_PROPERTY = "refundGoodsFee";
  public static final String REFUND_LOGISTICS_FEE_PROPERTY = "refundLogisticsFee";

  public static final String CHANGE_GOODS_PROPERTY = "changeGoods";
  public static final String REISSUE_GOODS_PROPERTY = "reissueGoods";

  @Autowired
  protected OrderMapper orderMapper;

  @Autowired
  protected OrderItemMapper orderItemMapper;

  @Autowired
  protected SkuMapper skuMapper;

  @Autowired
  private ActivityGrouponService activityGrouponService;

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private MessageService messageService;

  @Autowired
  private UserMessageMapper userMessageMapper;

  @Autowired
  private MainOrderMapper mainOrderMapper;

  @Autowired
  private UserService userService;

  @Autowired
  private PromotionBaseInfoService promotionBaseInfoService;

  @Autowired
  private PgPromotionServiceAdapter pgPromotionServiceAdapter;

  @Autowired
  private PromotionOrderDetailMapper promotionOrderDetailMapper;

  @Autowired
  private PromotionConfigMapper promotionConfigMapper;

  @Autowired
  private ProductMapper productMapper;

//  @Autowired
//  private MainOrderService mainOrderService;

  private Logger log = LoggerFactory.getLogger(getClass());

  @Value("${order.delaysign.cnt}")
  private int delaySignCnt;

  @Value("${order.delaysign.date}")
  private int defDelayDate;

  @Value("${tech.serviceFee.standard}")
  private String serviceFeethreshold;

  @Autowired
  private CustomerOwnerAdress customerOwnerAdress;
  @Autowired
  private FreshManService freshManService;
  /**
   * shared common action
   */
  @Transactional
  protected void cancel(Order order) {
    Order r = new Order();
    r.setId(order.getId());
    r.setCancelledAt(new Date());
    r.setStatus(OrderStatus.CANCELLED);

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("id", order.getId());
    params.put("status", order.getStatus());

    if (orderMapper.updateOrderByCancel(params, r) == 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "order[id=" + order.getId() + "] cancel action has not been done");
    }
    log.debug("order[" + order.getOrderNo() + "] is cancelled");
    //如果所有子单全部取消，去取消主单
    int a = mainOrderMapper.cancelMainOrderIfOrderCancelled(order.getMainOrderId());
    if( a > 0 )
    	log.info("{}取消时，发现所有子单已取消，执行取消主单成功！" , order.getOrderNo());


  }

  /**
   * 检查是否有活动订单
   */
  private boolean hadMeetingOrderItem(Order order) {
    boolean hadMeetingSku = false;
    //
      if (!OrderSortType.MEETING.equals(order.getOrderType())) {
          return hadMeetingSku;
      }

    String orderId = order.getId();
    List<OrderItemVO> orderItemVOS = orderItemMapper.selectVOByOrderId(orderId);
    if (CollectionUtils.isEmpty(orderItemVOS)) {
      return false;
    }
    //循环检查所有订单行是否包含活动商品
    for (int i = 0; i < orderItemVOS.size(); i++) {
      OrderItemVO orderItemVO = orderItemVOS.get(i);
      String skuId = orderItemVO.getSkuId();
      if (StringUtils.isEmpty(skuId)) {
        continue;
      }
      Sku sku = skuMapper.selectByPrimaryKey(skuId);
      if (null == sku) {
        continue;
      }

      //检查该商品是否参加活动，如果不是活动商品continue；否则退出，返回
      List<PromotionBaseInfo> lstPb = promotionBaseInfoService.selectBySkuCode(sku.getSkuCode());
      if (CollectionUtils.isEmpty(lstPb)) {
        continue;
      }
      hadMeetingSku = true;
      break;
    }
    return hadMeetingSku;
  }

  /**
   * 检查是否需要推送
   * @param order
   * @return
   */
  private boolean hadWMS(Order order) {
    //是否需要推送
    boolean hadWMS = false;
    //orderId
    String mainOrderId = order.getMainOrderId();

    MainOrder mainOder = mainOrderMapper.selectByPrimaryKey(mainOrderId);
    String orderNo = mainOder.getOrderNo();

    Map promotionOrderDetail = promotionOrderDetailMapper.findPgOrderInfo(orderNo, order.getOrderNo());
    String pCode = promotionOrderDetail.get("p_code").toString();
    PromotionBaseInfo baseInfo = promotionBaseInfoService.selectByPCode(pCode);
    if (null == baseInfo) {
        return false;
    }

    Map map = new HashMap();
    map.put("configType",baseInfo.getpType());
    map.put("configName","isWMS");
    PromotionConfig config = promotionConfigMapper.selectByConfig(map);
    if(config != null){
      String isWMS = config.getConfigValue();
      if(("on").equals(isWMS)){
        hadWMS = true;
      }

    }
    return hadWMS;
  }

  /**
   * 全部付款(直接到帐交易/担保交易)
   */
  @Transactional
  protected boolean pay(Order order) {
    // 在不同的流程中，这一步的前置条件都不一样
    Order record = new Order();
    // 判断订单是否已经处理 TODO 竞争问题
    if (order.getPaidAt() != null && order.getPaidStatus() == PayStatus.SUCCESS) {
      return true;
    }
    record.setId(order.getId());
    // 默认支付金额等于应付金额
    // 默认支付金额等于应付金额
    record.setPaidFee(order.getTotalFee());
    // 时间使用应用服务器时间
    // TODO: 确认第三方支付接口是否返回支付时间
    record.setPaidAt(new Date());

    //杭州大会商品，支付状态修改，2018-10-08

    if (hadMeetingOrderItem(order)) {
      if(hadWMS(order)){
        record.setStatus(OrderStatus.PAID);
      }else {
        record.setStatus(OrderStatus.PAIDNOSTOCK);
      }
    } else if (OrderSortType.isPiece(order.getOrderType())) {
      record.setStatus(OrderStatus.PAIDNOSTOCK);
    } else {
      record.setStatus(OrderStatus.PAID);
    }
    //杭州大会商品，支付状态修改，2018-10-08
    record.setPaidStatus(PayStatus.SUCCESS);

    //直接交易设置对账时间
    if (order.getType() == OrderType.DIRECT) {
      record.setCheckingAt(new Date(System.currentTimeMillis()));
    }

    Map<String, Object> params = new HashMap<>();
    params.put("id", order.getId());
    params.put("status", new OrderStatus[]{OrderStatus.SUBMITTED, OrderStatus.PENDING});

    /**
     * if 当前主单之前的累计消费金额为 consumedPay, 当前 order 子单的应付金额为 goodsFee, 子单数量为 amount
     * 现象1:
     *  如果 consumedPay + goodsFee >= 200 总是成立, 并且发放德分事物未提交, 可能造成同时间出现多次发放德分现象
     * 现象2:
     *  如果 consumedPay + goodsFee < 200 && consumedPay + goodsFee * amount >=200 总是成立
     *    1. 更新订单线程在统计金额之前, 不发送德分(不正常)
     *    2. 更新订单线程在统计金额之后, 发送德分(正常, 但存在线程先后问题)
     *
     * 此项操作调整, 在主订单支付成功且更新后执行
     */
    //新人引导发放德分 在更新订单状态之前操作
//    freshManService.freshManGrantPoint(order);

    if (orderMapper.updateOrderByPay(params, record) == 0) {
      Order o = orderMapper.selectByPrimaryKey(order.getId());
      if (OrderStatus.PAID.equals(o.getStatus())) {
        log.debug("order[" + order.getOrderNo() + "] has already been paid");
        return false;
      } else {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "order[id=" + order.getId() + "] pay action has not been done");
      }
    }


    //2018-12-14 Yarm设置归属城市和归属店铺
    this.customerOwnerAdress.setOwnerAdress(order.getBuyerId(),order.getOrderNo(), null);
    log.debug("order[" + order.getOrderNo() + "] is paid");
    return true;
  }

  @Transactional
  protected void ship(Order order) {
    Order record = new Order();
    Date now = new Date();
    record.setId(order.getId());
    record.setStatus(OrderStatus.SHIPPED);
    record.setShippedAt(now);
    record.setLogisticsCompany(order.getLogisticsCompany());
    record.setLogisticsOrderNo(order.getLogisticsOrderNo());
    boolean capableToRefund = capableToRefund(order);
    orderMapper.updateCapableToRefundById(capableToRefund, order.getId(), null);

    Calendar c = Calendar.getInstance();
    c.setTime(now);
    c.add(Calendar.DATE, this.defDelayDate);
    record.setLatestSignAt(c.getTime());

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("id", order.getId());
    params.put("status", OrderStatus.PAID);

    if (orderMapper.updateOrderByShip(params, record) == 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "order[id=" + order.getId() + "] ship action has not been done");
    }
    log.debug("order[" + order.getOrderNo() + "] is shipped");

  }

  @Transactional
  protected void delaySign(Order order) {
    Order record = new Order();
    record.setId(order.getId());

    Calendar calendar = Calendar.getInstance();
    if (order.getLatestSignAt() == null) {
      calendar.setTime(order.getShippedAt());
      calendar.add(Calendar.DATE, this.defDelayDate);
      order.setLatestSignAt(order.getShippedAt());
    }
    calendar.setTime(order.getShippedAt());
    calendar.add(Calendar.DATE, this.defDelayDate * (this.delaySignCnt + 1) - 1);

    if (order.getLatestSignAt().after(calendar.getTime())) { // 超出最迟签收时间
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "您已延迟收货两次，订单有问题可联系客服qq哦");
    } else { //
      calendar.setTime(order.getLatestSignAt());
      calendar.add(Calendar.DATE, this.defDelayDate);
      Map<String, Object> params = new HashMap<String, Object>();
      params.put("id", order.getId());
      params.put("status", OrderStatus.SHIPPED);
      record.setLatestSignAt(calendar.getTime());
      if (orderMapper.updateOrderBySign(params, record) == 0) {
        throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "用户延迟确认收货失败");
      }
    }

    log.debug("order[" + order.getOrderNo() + "] is delay signed");

  }

  @Transactional
  protected void sign(Order order) {
    Order record = new Order();
    record.setId(order.getId());
    record.setStatus(OrderStatus.SUCCESS);
    record.setSucceedAt(new Date());
    boolean capableToRefund = capableToRefund(order);
    record.setCapableToRefund(capableToRefund);//确认收货后，订单可退，默认开放售后入口
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("id", order.getId());
    params.put("status", OrderStatus.SHIPPED);

//		Calendar calendar = Calendar.getInstance();
//		calendar.add(Calendar.DATE, -7);
//		params.put("deadline", calendar.getTime());

    //担保交易设置对账时间
    if (order.getType() == OrderType.DANBAO) {
      record.setCheckingAt(new Date(System.currentTimeMillis()));
    }

    if (orderMapper.updateOrderBySign(params, record) == 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "order[id=" + order.getId() + "] sign action has not been done");
    }
    log.debug("order[" + order.getOrderNo() + "] is signed");

  }

  private boolean capableToRefund(Order order){
    List<OrderItem> orderItems = orderItemMapper.selectByOrderId(order.getId());
    if(orderItems != null && orderItems.size() >0 ){
      for (OrderItem orderItem:
              orderItems) {//遍历订单商品，如果包含不可退商品，则设置订单不可退
        Product product = productMapper.selectByPrimaryKey(orderItem.getProductId());
        if(product != null){
          return product.getSupportRefund();
        }
      }
    }else{
      throw new RuntimeException("订单商品不存在");
    }
    return false;
  }

  @Transactional
  protected void refund(Order order) {
    Order record = new Order();
    record.setId(order.getId());
    record.setRefundFee(order.getRefundFee());
    record.setRefundGoodsFee(order.getRefundGoodsFee());
    record.setRefundLogisticsFee(order.getRefundLogisticsFee());
    Optional<Date> dateOptional = Optional.fromNullable(order.getPaidAt());
    if(OrderSortType.isPiece(order.getOrderType())){
      record.setStatus(OrderStatus.CANCELLED);
    }else{
      record.setStatus(order.isInOneHour() ? OrderStatus.CANCELLED : OrderStatus.CLOSED);
    }

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("id", order.getId());
    params.put("status", new OrderStatus[]{OrderStatus.PAID, OrderStatus.SHIPPED,
        OrderStatus.PAIDNOSTOCK, OrderStatus.REFUNDING});
    //扣減德分 需要在更新订单状态之前处理
    freshManService.freshManeductPoint(order);

    //拼团订单处理
    if (orderMapper.updateOrderByRefund(params, record) == 0) {
        log.error("order[id=" + order.getId() + "] refund action has not been done");
    }


    //2018-12-14 Yarm设置归属城市和归属店铺
    this.customerOwnerAdress.setOwnerAdress(order.getBuyerId(),order.getOrderNo(), null);
    log.debug("order[" + order.getOrderNo() + "] is refunded");

  }

  /**
   * 卖家换货
   */
  @Transactional
  protected void change(Order order) {
    Order record = new Order();
    record.setId(order.getId());//没有设置费用等信息，可能会报错
    record.setStatus(order.isInOneHour() ? OrderStatus.CANCELLED : OrderStatus.CLOSED);

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("id", order.getId());
    params.put("status",
            new OrderStatus[]{OrderStatus.PAID, OrderStatus.SHIPPED, OrderStatus.CHANGING});

    //通过record和params更新订单信息，更新失败则抛出异常
    if (orderMapper.updateOrderByRefund(params, record) == 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "order[id=" + order.getId() + "] change action has not been done");
    }
    log.debug("order[" + order.getOrderNo() + "] is changed");

  }

  private void pushMessage(Long msgId, String userId, String url, Integer type, String relatId,
                           String data, String desc) {
    Message message = messageService.loadMessage(IdTypeHandler.encode(msgId));
    message.setContent(desc);
    message.setData(data);
    MessageNotifyEvent event = new MessageNotifyEvent(message, userId, url, type, relatId);
    applicationContext.publishEvent(event);
  }

  /**
   * 团购订单逻辑处理
   */
  private void checkGroupon(Order order, OrderStatus type) {
    // 如果是团购订单
    if (order.getOrderType() == OrderSortType.GROUPON) {
      String orderId = order.getId();
      PromotionGrouponVO promotionGrouponVO = activityGrouponService
              .findPromotionGrouponByOrderId(orderId);

      if (OrderStatus.PAID == type) {
        // 团购支付后，如果发起者，则提醒发起拼团成功，参与者提醒参与拼团成功
        ActivityGroupon activityGroupon = activityGrouponService
                .findActivityGrouponByOrderId(orderId);
        String userId = activityGroupon.getUserId();
        userId = IdTypeHandler.encode(new Long(userId));
        // 如果买家是开团人，则提醒已经开团成功，否则提醒有新团员加入
        String message = "";
        if (userId.equals(order.getBuyerId())) {
          message = "您已成功开团啦，快去邀请小伙伴吧！";
          pushMessage(PushMsgId.GROUPON_PAY.getId(), order.getBuyerId(),
                  Long.toString(new Date().getTime()), PushMsgType.MSG_GROUPON_PAY.getValue(), null,
                  activityGroupon.getId(), message);
        } else {
          message = "参与拼团成功！";
          pushMessage(PushMsgId.GROUPON_PAY.getId(), order.getBuyerId(),
                  Long.toString(new Date().getTime()), PushMsgType.MSG_GROUPON_PAY.getValue(), null,
                  activityGroupon.getId(), message);
        }

        int paidCount = activityGrouponService.countPaidOrderByOrderId(orderId);
        int needNumbers = promotionGrouponVO.getNumbers() - paidCount;
        // 有人参团成功，提示开团人及已参团人“某某成功参团！”
        List<String> userIds = activityGrouponService.getPaidUserByOrderId(orderId);
        for (String inuserId : userIds) {
          inuserId = IdTypeHandler.encode(new Long(inuserId));
          if (!inuserId.equals(order.getBuyerId())) {
            User buyerUser = userService.load(order.getBuyerId());
            String buyerName = buyerUser.getName();
            String msg;
            if (needNumbers > 0) {
              msg = String.format("%s加入了您的拼团，还剩%d人，加油哦", buyerName, needNumbers);
            } else {
              msg = String.format("%s加入了您的拼团，您的拼团已成功，请耐心等待发货！", buyerName);
            }
            pushMessage(PushMsgId.GROUPON_PAY.getId(), inuserId,
                    Long.toString(new Date().getTime()),
                    PushMsgType.MSG_GROUPON_PAY.getValue(), null, activityGroupon.getId(), msg);
          }
        }

        // 支付完成后处理，如果达到成团人数，则更新团购活动状态为PAID
        // 判断团购人数是否达到上限
        if (paidCount >= promotionGrouponVO.getNumbers()) {
          // 如果团购活动处于退款申请中，就算达到满员也不能发货，因此不改变状态，待退款申请处理后会改变状态
          if (activityGroupon.getStatus() != ActivityGrouponStatus.REFUNDING) {
            try {
              Thread.sleep(1000);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            // 更新团购活动状态为PAID
            activityGrouponService
                    .updateStatus(activityGroupon.getId(), ActivityGrouponStatus.PAID);
            // 拼团成功后在消息栏里提示拼团成功
            for (String inuserId : userIds) {
              inuserId = IdTypeHandler.encode(new Long(inuserId));
              pushMessage(PushMsgId.GROUPON_PAY.getId(), inuserId,
                      Long.toString(new Date().getTime()), PushMsgType.MSG_GROUPON_PAY.getValue(), null,
                      activityGroupon.getId(), "拼团已成功，待发货！");
            }
          }
        }
      } else if (OrderStatus.REFUNDING == type) {
        // 退款后更新团购活动状态
        // 判断团购人数是否达到上限
        int paidCount = activityGrouponService.countPaidOrderByOrderId(orderId);
        if (paidCount < promotionGrouponVO.getNumbers()) {
          ActivityGroupon activityGroupon = activityGrouponService
                  .findActivityGrouponByOrderId(orderId);
          // 更新团购活动状态为CREATED,团购活动结束后的退款这种情况不需要更新活动状态
          if (activityGroupon.getStatus() != ActivityGrouponStatus.CLOSED) {
            activityGrouponService
                    .updateStatus(activityGroupon.getId(), ActivityGrouponStatus.CREATED);
          }
        }
        ActivityGroupon activityGroupon = activityGrouponService
                .findActivityGrouponByOrderId(orderId);
        String userId = activityGroupon.getUserId();
        userId = IdTypeHandler.encode(new Long(userId));
        // 如果开团人退款，直接提示所有参团人参团失败
        if (userId.equals(order.getBuyerId())) {
          List<String> userIds = activityGrouponService.getPaidOrCloseUserByOrderId(orderId);
          for (String inuserId : userIds) {
            inuserId = IdTypeHandler.encode(new Long(inuserId));
            // 发送退款消息前，判断该用户是否已经发送过退款消息
            String content = "很遗憾，您的拼团没有成功，我们会在1-3个工作日内原路给您退款，如有疑问请联系客服！";
            if (userMessageMapper
                    .countMessageByUser(inuserId, "" + PushMsgId.GROUPON_PAY.getId(), content) == 0) {
              pushMessage(PushMsgId.GROUPON_PAY.getId(), inuserId,
                      Long.toString(new Date().getTime()), PushMsgType.MSG_GROUPON_PAY.getValue(), null,
                      activityGroupon.getId(), content);
            }
          }
        }
      } else if (OrderStatus.SHIPPED == type) {
        // 发货后更新团购活动状态
        // 判断团购人数是否达到上限
        int successCount = activityGrouponService
                .countOrderByOrderIdAndStatus(orderId, OrderStatus.SHIPPED);
        if (successCount >= promotionGrouponVO.getNumbers()) {
          ActivityGroupon activityGroupon = activityGrouponService
                  .findActivityGrouponByOrderId(orderId);
          // 更新团购活动状态为SHIPPED
          activityGrouponService
                  .updateStatus(activityGroupon.getId(), ActivityGrouponStatus.SHIPPED);
        }
        // 发送消息给团员团购订单已发货
        /**ActivityGroupon activityGroupon = activityGrouponService.findActivityGrouponByOrderId(orderId);
         pushMessage(PushMsgId.GROUPON_PAY.getId(), order.getBuyerId(),  Long.toString(new Date().getTime()), PushMsgType.MSG_GROUPON_PAY.getValue(), null, activityGroupon.getId(), "已发货");**/
      } else if (OrderStatus.SUCCESS == type) {
        // 签收后更新团购活动状态
        // 判断团购人数是否达到上限
        int successCount = activityGrouponService
                .countOrderByOrderIdAndStatus(orderId, OrderStatus.SUCCESS);
        if (successCount >= promotionGrouponVO.getNumbers()) {
          ActivityGroupon activityGroupon = activityGrouponService
                  .findActivityGrouponByOrderId(orderId);
          // 更新团购活动状态为SUCCESS
          activityGrouponService
                  .updateStatus(activityGroupon.getId(), ActivityGrouponStatus.SUCCESS);
          // 拼团成功后在消息栏里提示拼团已完成
          /**List<String> userIds = activityGrouponService.getPaidUserByOrderId(orderId);
           for(String userId : userIds){
           pushMessage(PushMsgId.GROUPON_PAY.getId(), IdTypeHandler.encode(new Long(userId)),  Long.toString(new Date().getTime()), PushMsgType.MSG_GROUPON_PAY.getValue(), null, activityGroupon.getId(), "已完成");
           }**/
        }
      }

    }
  }



  public void unkownAction(Order order, OrderActionType action) {
    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            "unsupported action[" + action + "] on order["
                    + order.getOrderNo() + "]");
  }

  public boolean isBuyer(Order order, User user) {//增加后台用户申请换货的权限，供以后使用
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      Object principal = auth.getPrincipal();
      return order.getBuyerId().equals(user.getId()) || (principal instanceof Merchant);
    } else {
      return false;
    }
  }

  public boolean isSeller(Order order, User user) {
    //FIXME dongsongjie 判断订单是否是当前店铺或者店铺中推客的订单
    return true;
//		return user.hasRole(1) || order.getSellerId().equals(user.getId());
  }

  public abstract void execute(Order order, User user, OrderActionType action,
                               Map<String, Object> params);

  public abstract void executeBySystem(Order order, OrderActionType action,
                                       Map<String, Object> params);

  public abstract OrderStatus[] getOrderStatuses();

  public abstract OrderActionType[] getOrderActions();

  public abstract OrderActionType[] getNextAction(Order order, User user);

  protected abstract void checkPrivileges(Order order, User user, OrderActionType action);

  protected abstract boolean checkPreConditions(Order order, OrderActionType action);
}
