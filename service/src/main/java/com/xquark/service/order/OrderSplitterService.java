package com.xquark.service.order;

import com.xquark.dal.model.*;
import com.xquark.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * 主子订单按物流要求拆分
 *
 * @author dongsongjie
 */

public interface OrderSplitterService extends BaseService {

  /**
   * 订单拆分
   *
   * @param orders 按店铺划分的订单对象及其订单项
   * @return 按物流要求拆分成order，一个order对应一个ERP订单及一个包裹
   */
  Map<Order, List<OrderItem>> split(Map<Order, List<OrderItem>> orders);
}
