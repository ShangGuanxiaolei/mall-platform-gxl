package com.xquark.service.order.util.impl;

import com.xquark.dal.model.Supplier;
import com.xquark.service.order.util.AbstractProxyMethodStrategy;
import com.xquark.service.order.util.ProxyContext;
import java.lang.reflect.InvocationTargetException;

/**
 * @author jackzhu
 * @since 2018/11/16
 **/
public class HashCodeMethodStrategy extends AbstractProxyMethodStrategy {


  public HashCodeMethodStrategy(ProxyContext proxyContext) {
    super(proxyContext);
  }


  @Override
  public Object execute() {
  //在hashcode的时候调用初始化枚举方法
    new InitMethodStrategy(proxyContext).execute();
    try {
      return proxyContext.getMethod().invoke(ORDER_DELIVERY_CONDITION, proxyContext.getArgs());
    } catch (InvocationTargetException | IllegalAccessException e) {
      logger.debug("方法执行失败： {}", e.getMessage());
    }
    return null;
  }


}
