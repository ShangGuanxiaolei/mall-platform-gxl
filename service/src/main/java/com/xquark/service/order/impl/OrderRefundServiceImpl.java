package com.xquark.service.order.impl;

import com.xquark.aop.anno.NotNull;
import com.xquark.dal.mapper.OrderRefundMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.OrderRefundStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.LogisticsCompany;
import com.xquark.dal.type.OrderRefundActionType;
import com.xquark.dal.type.PushMsgId;
import com.xquark.dal.type.PushMsgType;
import com.xquark.dal.vo.*;
import com.xquark.event.MessageNotifyEvent;
import com.xquark.event.XQMessageNotifyEvent;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.msg.MessageService;
import com.xquark.service.order.OrderMessageService;
import com.xquark.service.order.OrderRefundAttachService;
import com.xquark.service.order.OrderRefundService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.vo.OrderChangeRequestForm;
import com.xquark.service.order.vo.OrderRefundRequestForm;
import com.xquark.service.order.vo.OrderReissueRequestForm;
import com.xquark.service.orderHeader.OrderHeaderService;
import com.xquark.service.user.UserService;
import com.xquark.thirds.umpay.api.util.StringUtil;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("orderRefundService")
public

class OrderRefundServiceImpl extends BaseServiceImpl implements OrderRefundService {

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private MessageService messageService;

  @Autowired
  private OrderRefundMapper orderRefundMapper;

  @Autowired
  private OrderMessageService orderMessageService;

  @Autowired
  private UserService userService;

  @Autowired
  private OrderRefundAttachService orderRefundAttachService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private OrderHeaderService orderHeaderService;

  @Value("${order.refund.autoConfirm.day}")
  private int autoConfirmDay;

  @Value("${order.refund.autoSuccessWithBuyerNoShip.day}")
  private int autoSuccessWithBuyerNoShipDay;

  @Value("${order.refund.autoClosedWithSellerNoSign.day}")
  private int autoClosedWithSellerNoSignDay;

  @Override
  public List<PropertyDict> loadRefundReasonDict(OrderStatus orderStatus) {
    return OrderRefundVO.loadRefundReasonDict(orderStatus);
  }


  @Override
  public List<OrderRefundVO> list(String sellerId, Pageable pageable) {
    return orderRefundMapper.list(sellerId, pageable);
  }

  public boolean isBuyer(Order order, User user) {
    return order.getBuyerId().equals(user.getId());
  }

  public boolean isSeller(Order order, User user) {
    return user.hasRole(1) || order.getSellerId().equals(user.getId());
  }

  protected void checkPrivileges(Order order, OrderRefund orderRefund, User user,
      OrderRefundActionType action) {
    log.info(
        "check user[" + user.getLoginname() + "]'s privileges of orderRefund[" + orderRefund.getId()
            + "] by action[" + action.name() + "]");

    switch (action) {
      case CONFIRM:
        if (!isSeller(order, user)) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "order refund user has no privileges to do this action=[" + action + "] orderNo=["
                  + order.getOrderNo() + "]");
        }
        break;
      case CANCEL:
//			cancel(orderRefund);
        break;
      case REJECT:
        if (!isSeller(order, user)) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "order refund user has no privileges to do this action=[" + action + "] orderNo=["
                  + order.getOrderNo() + "]");
        }
        break;
      case SHIP:
        if (!isBuyer(order, user)) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "order refund user has no privileges to do this action=[" + action + "] orderNo=["
                  + order.getOrderNo() + "]");
        }
        break;
      case SIGN:
        if (!isSeller(order, user)) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "order refund user has no privileges to do this action=[" + action + "] orderNo=["
                  + order.getOrderNo() + "]");
        }
        break;
      case REJECT_SIGN:
        if (!isSeller(order, user)) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "order refund user has no privileges to do this action=[" + action + "] orderNo=["
                  + order.getOrderNo() + "]");
        }
        break;
      default:
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "不支持的操作类型");
    }
  }

  @Override
  public void execute(OrderRefundActionType action, OrderRefund orderRefund,
      Map<String, Object> params) {
    User user = (User) getCurrentUser();
    Order order = orderService.load(orderRefund.getOrderId());
    checkPrivileges(order, orderRefund, user, action);

    switch (action) {
      case CONFIRM:
        confirm(orderRefund, false);
        break;
      case CANCEL:
        cancel(orderRefund);
        break;
      case REJECT:
        reject(orderRefund, params);
        break;
      case SHIP:
        ship(orderRefund);
        break;
      case SIGN:
        sign(orderRefund);
        break;
      case REJECT_SIGN:
        rejectSign(orderRefund);
        break;
      default:
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "不支持的操作类型");
    }
  }

  @Override
  public void executeBySystem(OrderRefundActionType action, OrderRefund orderRefund,
      Map<String, Object> params) {
    switch (action) {
      case CONFIRM:
        confirm(orderRefund, true);
        break;
      case CANCEL:
        cancel(orderRefund, true); // 取消
        break;
      case SIGN:
        sign(orderRefund, true); // 完成
        break;
      default:
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "不支持的操作类型");
    }
  }

  @Override
  @Transactional
  public OrderRefund submit(OrderRefund orderRefund) {
    OrderVO order = orderService.loadVO(orderRefund.getOrderId());
    OrderStatus orderStatus = order.getStatus();
    if (StringUtils.isNotBlank(orderRefund.getId())) {
      orderStatus = load(orderRefund.getId()).getOrderStatus();
    }

    BigDecimal refundFee = findRefundableFee(order, orderStatus);
    if (orderRefund.getRefundFee().compareTo(refundFee) > 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "退款金额不能大于订单能退的金额" + refundFee);
    }

    if (StringUtils.isBlank(orderRefund.getId())) {
      if (listByOrderId(orderRefund.getOrderId()).size() > 0) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "申请已提交，请勿重复提交");
      }

      orderRefund.setApplyNo(UniqueNoUtils.next(UniqueNoType.R));
      orderRefund.setOrderStatus(order.getStatus());
      orderRefund.setStatus(OrderRefundStatus.SUBMITTED);
      OrderItem orderItem = order.getOrderItems().get(0);
      orderRefund.setProductName(orderItem.getProductName());
      orderRefund.setImg(orderItem.getProductImg());
      insert(orderRefund);
      orderService.requestRefund(order.getId());

      //TODO须移出
      //发送消息
      orderMessageService.sendSellerSMSOrderRefundRequest(order);

      pushMessage(PushMsgId.REFUND_REQUEST.getId(), order.getSellerId(),
          Long.toString(new Date().getTime()), PushMsgType.MSG_REFUND_REQUEST_S.getValue(),
          order.getId(), order.getOrderNo());

    } else {
      updateByModifyRequest(orderRefund);
    }

//		//先简单的修改状态
//		OrderRefund updateOrderRefund = new OrderRefund();
//		updateOrderRefund.setId(orderRefund.getId());
//		updateOrderRefund.setStatus(OrderRefundStatus.SUBMITTED);
//		orderRefundMapper.updateByPrimaryKeySelective(updateOrderRefund);

    return orderRefund;
  }

  private void pushMessage(Long msgId, String baiduTag, String url, Integer type, String relatId,
      String orderNo) {
    Message message = messageService.loadMessage(IdTypeHandler.encode(msgId));
    message.setContent(message.getContent().replace("{orderNo}", orderNo));
//		message.setContent(modifyContent(message.getContent(), msgId));  
    MessageNotifyEvent event = new MessageNotifyEvent(message, baiduTag, url, type, relatId);
    applicationContext.publishEvent(event);
  }


  private void pushXQMsg(Long msgId, String baiduTag, Integer type, String relatId,
      String orderNo) {
    Message message = messageService.loadMessage(IdTypeHandler.encode(msgId));
    message.setContent(message.getContent().replace("{orderNo}", orderNo));
    XQMessageNotifyEvent event = new XQMessageNotifyEvent(message, baiduTag, null, type, relatId);
    event.setExtDate(createExtData(relatId));
    applicationContext.publishEvent(event);
  }

  private ThirdExtPushData createExtData(String orderId) {
    ThirdExtPushData data = new ThirdExtPushData();
    List<OrderItem> ois = orderService.listOrderItems(orderId);
    Order order = orderService.load(orderId);
    if (ois != null && ois.size() > 0) {
      data.setThirdId(ois.get(0).getProductId());
      data.setOrderStatus(order.getStatus().toString());
    }
    return data;
  }

  /**
   * 卖家收货
   */
  @Transactional
  private void sign(OrderRefund orderRefund, boolean isSystem) {
    if (orderRefundMapper.sign(orderRefund) != 1) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "更新失败");
    }

    if (isSystem) {
      orderService.refundSystem(orderRefund.getOrderId(), orderRefund.getRefundFee());
      return;
    }

    orderService.refund(orderRefund.getOrderId(), orderRefund.getRefundFee());
  }

  /**
   * 取消退货申请--订单的状态改为发货中
   */
  @Transactional
  private void cancel(OrderRefund orderRefund, boolean isSystem) {
    int rc = orderRefundMapper.cancel(orderRefund);
    if (rc != 1) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "更新失败");
    }

    if (isSystem) {
      orderService.cancelRefundSystem(orderRefund.getOrderId(), orderRefund.getOrderStatus());
      String id = orderRefund.getId();
      orderRefundMapper.deleteByPrimaryKey2(id);
      return;
    }

    orderService.cancelRefund(orderRefund.getOrderId(), orderRefund.getOrderStatus());
  }

  @Transactional
  private void confirm(OrderRefund orderRefund, boolean admin) {
    int rc = 0;

    if (admin) {
      log.debug("auto confirm refund apply orderRefund=[" + orderRefund.getId() + "]");
    }

    if (admin && orderRefund.getBuyerRequire() != 1) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "buyer require must be only refund fee ");
    }

    OrderVO order = orderService.loadVO(orderRefund.getOrderId());
    if (orderRefund.getBuyerRequire() == 1) {
      rc = orderRefundMapper.success(orderRefund);
      orderService.autoRefund(orderRefund.getOrderId(), orderRefund.getRefundFee());

      if (!admin) {

        //send sms
        orderMessageService.sendBuyerSMSOrderRefundSuccess(order);

        //send push
        User buyerUser = userService.load(order.getBuyerId());
        if (isXQUser(buyerUser)) {
          pushXQMsg(PushMsgId.REFUND_SUCCESS.getId(), buyerUser.getExtUserId(),
              PushMsgType.MSG_REFUND_SUCCESS_B.getValue(), order.getId(), order.getOrderNo());
        }
      }

    } else {
      rc = orderRefundMapper.confirm(orderRefund);

      //send sms
      orderMessageService.sendBuyerSMSOrderRefundAgreeReturn(order);

      //send push
      User buyerUser = userService.load(order.getBuyerId());
      if (isXQUser(buyerUser)) {
        pushXQMsg(PushMsgId.REFUND_AGREE_RETURN.getId(), buyerUser.getExtUserId(),
            PushMsgType.MSG_REFUND_AGREE_RETURN_B.getValue(), order.getId(), order.getOrderNo());
      }
    }

    if (rc != 1) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "更新失败");
    }
  }

  @SuppressWarnings("unchecked")
  @Transactional
  private void reject(OrderRefund orderRefund, Map<String, Object> params) {
    //保存附件信息
    OrderRefundAttach attach = null;
    List<String> attachKeys = (List<String>) params.get("attachs");
    if (attachKeys != null) {
      for (String attachKey : attachKeys) {
        attach = new OrderRefundAttach();
        attach.setRefundId(orderRefund.getId());
        attach.setAttachKey(attachKey);
        attach.setType(1);
        orderRefundAttachService.insert(attach);
      }
    }

    if (orderRefundMapper.reject(orderRefund) != 1) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "更新失败");
    }

    OrderVO order = orderService.loadVO(orderRefund.getOrderId());
    orderMessageService.sendBuyerSMSOrderRefundReject(order);

    User buyerUser = userService.load(order.getBuyerId());
    if (isXQUser(buyerUser)) {
      pushXQMsg(PushMsgId.REFUND_REJECT.getId(), buyerUser.getExtUserId(),
          PushMsgType.MSG_REFUND_REJECT_B.getValue(), order.getId(), order.getOrderNo());
    }
  }

  private Boolean isXQUser(User user) {
    if (user != null && user.getPartner() != null && user.getPartner().equalsIgnoreCase("xiangqu")
        && StringUtils.isNotEmpty(user.getExtUserId())) {
      return true;
    }
    return false;
  }

  @Transactional
  private void ship(OrderRefund orderRefund) {
    if (orderRefundMapper.ship(orderRefund) != 1) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "更新失败");
    }
  }

  @Transactional
  private void cancel(OrderRefund orderRefund) {

    int rc = 0;
    rc = orderRefundMapper.cancel(orderRefund);

    if (rc != 1) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "更新失败");
    }

    orderService.cancelRefund(orderRefund.getOrderId(), orderRefund.getOrderStatus());
  }

  @Transactional
  private void sign(OrderRefund orderRefund) {
    if (orderRefundMapper.sign(orderRefund) != 1) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "更新失败");
    }
    orderService.refund(orderRefund.getOrderId(), orderRefund.getRefundFee());
  }


  @Transactional
  private void rejectSign(OrderRefund orderRefund) {
    if (orderRefundMapper.rejectSign(orderRefund) != 1) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "更新失败");
    }
//		OrderVO order = orderService.loadVO(orderRefund.getOrderId());
//		orderMessageService.sendBuyerSMSOrderRefundSuccess(order);
//		User buyerUser = userService.load(order.getBuyerId());
//		if (isXQUser(buyerUser)) {
//			pushXQMsg(PushMsgId.REFUND_REJECT.getId(), buyerUser.getExtUserId(), PushMsgType.MSG_REFUND_REJECT_B.getValue(), order.getId());
//		}
  }

  @Override
  @Transactional
  public int insert(OrderRefund orderRefund) {
    int result = orderRefundMapper.insert(orderRefund);
    if (StringUtils.isNotBlank(orderRefund.getRefundImg())) {
      String[] images = orderRefund.getRefundImg().split(",");
      for (String img : images) {
        orderRefundMapper
            .insertRefundImg(orderRefund.getId(), img);
      }
    }
    return result;
  }

  @Override
  @Transactional
  public int insertOrder(OrderRefund orderRefund) {
    int result = orderRefundMapper.insert(orderRefund);
    if (StringUtils.isNotBlank(orderRefund.getRefundImg())) {
      String[] images = orderRefund.getRefundImg().split(",");
      for (String img : images) {
        orderRefundMapper
                .insertRefundImg(orderRefund.getId(), img);
      }
    }
    return result;
  }

  @Override
  public OrderRefund load(String id) {
    return orderRefundMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<OrderRefund> listByOrderId(String orderId) {
    List<OrderRefund> orderRefunds = orderRefundMapper.listByOrderId(orderId);
    for (OrderRefund orderRefund : orderRefunds) {
      List<OrderRefundImg> refundImgList = orderRefundMapper.listRefundImg(orderRefund.getId());
      orderRefund.setRefundImgList(refundImgList);
    }

    return orderRefunds;
  }

  @Override
  @Transactional
  public Boolean updateStatusByOrderId(@NotBlank String id, @NotNull OrderRefundStatus status,
      Map<String, Object> params) {
    return orderRefundMapper.updateStatusByOrderId(id, status, params) > 0;
  }

  @Override
  public OrderRefundVO loadVO(String id) {
    return orderRefundMapper.selectVOByPrimaryKey(id);
  }

  @Override
  public OrderVO loadOrderVO(String orderId, String id) {
    OrderVO order = new OrderVO(orderService.load(orderId));
    OrderStatus orderStatus = order.getStatus();
    if (StringUtils.isNoneBlank(id)) {
      OrderRefund orderRefund = load(id);
      orderStatus = orderRefund.getOrderStatus();
    }
    order.setRefundableFee(findRefundableFee(order, orderStatus));
    return order;
  }

  private BigDecimal findRefundableFee(Order order, OrderStatus orderStatus) {
    BigDecimal refundableFee = null;

    refundableFee = order.getTotalFee();
    if (orderStatus != OrderStatus.PAID) {
      refundableFee = refundableFee.subtract(order.getLogisticsFee());
    }
    return refundableFee;
  }

  @Override
  public int update(OrderRefund orderRefund) {
    return orderRefundMapper.updateByPrimaryKeySelective(orderRefund);
  }

  @Override
  public int updateByModifyRequest(OrderRefund orderRefund) {
    return orderRefundMapper.updateByModifyRequest(orderRefund);
  }

  @Override
  public List<OrderRefundOperateDetail> initOpDetail(OrderRefund orderRefund, String from) {
    List<OrderRefundOperateDetail> opDetails = new ArrayList<OrderRefundOperateDetail>();
    orderService.load(orderRefund.getOrderId());

    //仅退款
    OrderRefundOperateDetail op = new OrderRefundOperateDetail();
    op.setType(0);
    op.setTime(orderRefund.getCreatedAt());
    op.setFrom(2);
    op.setTitle((from.equals("3") ? "买家" : "") + "创建了退款申请");
    StringBuffer sbf = new StringBuffer();
    sbf.append("买家要求：" + (orderRefund.getBuyerRequire() == 1 ? "仅退款\n" : "退货并退款\n"));
    if (orderRefund.getOrderStatus() == OrderStatus.SHIPPED) {
      sbf.append(
          "货物状态：" + ((orderRefund.getBuyerReceived() == null || orderRefund.getBuyerReceived() == 0)
              ? "买家未收到货\n" : "买家已收到货\n"));
    }
    sbf.append("退款金额：" + orderRefund.getRefundFee() + "\n");

    String refundReason = null;

    List<PropertyDict> refundReasonDict = loadRefundReasonDict(orderRefund.getOrderStatus());
    for (PropertyDict dict : refundReasonDict) {
      if (dict.getKey().equals(orderRefund.getRefundReason())) {
        refundReason = dict.getValue();
        break;
      }
    }

    sbf.append("退款原因：" + refundReason + "\n");
    sbf.append("退款说明：" + orderRefund.getRefundMemo());

    op.setContent(sbf.toString());
    opDetails.add(op);

    if (from.equals("3")) {
      //卖家端显示
      String content = "您可以同意或拒绝退款，拒绝退款后需要提供拒绝退款的凭证";
      op = new OrderRefundOperateDetail(0, 1, orderRefund.getCreatedAt(), "请及时处理退款协议", content);
      opDetails.add(op);
    } else {
      String content = "待卖家处理";
      op = new OrderRefundOperateDetail(0, 1, orderRefund.getCreatedAt(), "待卖家处理", content);
      opDetails.add(op);
    }

    if (orderRefund.getRejectTime() != null) {
      String content =
          "拒绝原因：" + orderRefund.getRefuseReason() + "\n  拒绝理由： " + orderRefund.getRefuseDetail();
      if (from.equals("3")) {
        //卖家端显示
        op = new OrderRefundOperateDetail(0, 3, orderRefund.getRejectTime(), "您拒绝了买家的退款要求",
            content);
        opDetails.add(op);
      } else {
        op = new OrderRefundOperateDetail(0, 3, orderRefund.getRejectTime(), "卖家拒绝了您的退款要求",
            content);
        opDetails.add(op);
      }

      List<OrderRefundAttach> attachs = orderRefundAttachService
          .listByRefundId(orderRefund.getId());
      for (OrderRefundAttach attach : attachs) {
//				op = new OrderRefundOperateDetail(1, 3, orderRefund.getRejectTime(), "凭证", resourceFacade.resolveUrl(attach.getAttachKey()));
        op = new OrderRefundOperateDetail(1, 3, orderRefund.getRejectTime(), "凭证",
            attach.getAttachKey());
        opDetails.add(op);
      }
    }

    if (orderRefund.getConfirmTime() != null) {
      if (from.equals("3")) {
        //卖家端显示
        String content = "同意了买家的退款协议";
        op = new OrderRefundOperateDetail(0, 3, orderRefund.getConfirmTime(), "同意了买家的退款协议",
            content);
        opDetails.add(op);
      } else {
        String content = "卖家同意了退款协议";
        op = new OrderRefundOperateDetail(0, 3, orderRefund.getConfirmTime(), "卖家同意了退款协议", content);
        opDetails.add(op);
      }

      if (orderRefund.getBuyerRequire() == 2) {
        String content = "";
        content += "退货收货人：" + orderRefund.getReturnName() + "\n";
        content += "联系电话：" + orderRefund.getReturnPhone() + "\n";
        content += "退货地址：" + orderRefund.getReturnAddress() + "\n";
        if (StringUtils.isNotBlank(orderRefund.getReturnMemo())) {
          content += "退货备注：" + orderRefund.getReturnMemo();
        }
        op = new OrderRefundOperateDetail(0, 3, orderRefund.getCreatedAt(), "退货信息", content);
        opDetails.add(op);

      }
    }

    if (orderRefund.getShipTime() != null) {
      String logisticsCompanyName = orderRefund.getLogisticsCompany();
      for (LogisticsCompany logistics : LogisticsCompany.values()) {
        if (logistics.name().equals(logisticsCompanyName)) {
          logisticsCompanyName = logistics.getName();
          break;
        }
      }

      String content = "物流公司：" + logisticsCompanyName + "\n";
      content += "运单编号：" + orderRefund.getLogisticsNo() + "\n";
      content += "备注：" + StringUtils.defaultIfBlank(orderRefund.getLogisticsMemo(), "无");
      if (from.equals("3")) {
        //卖家端显示
        op = new OrderRefundOperateDetail(0, 2, orderRefund.getShipTime(), "买家已发货", content);
        opDetails.add(op);
      } else {
        op = new OrderRefundOperateDetail(0, 2, orderRefund.getShipTime(), "已发货", content);
        opDetails.add(op);
      }
    }

    if (orderRefund.getSignTime() != null) {
      String content = "已确认收货";
      if (from.equals("3")) {
        //卖家端显示
        op = new OrderRefundOperateDetail(0, 3, orderRefund.getSignTime(), "已确认收货", content);
        opDetails.add(op);
      } else {
        op = new OrderRefundOperateDetail(0, 3, orderRefund.getSignTime(), "卖家已确认收货", content);
        opDetails.add(op);
      }
    }

    if (orderRefund.getStatus() == OrderRefundStatus.SUCCESS) {
      op = new OrderRefundOperateDetail(0, 3, orderRefund.getUpdatedAt(), "卖家已同意退款", "卖家已同意退款");
      opDetails.add(op);
    } else if (orderRefund.getStatus() == OrderRefundStatus.CANCELLED) {
      op = new OrderRefundOperateDetail(0, 2, orderRefund.getUpdatedAt(), "退款申请取消", "买家取消了退款申请");
      opDetails.add(op);
    }

    if (orderRefund.getStatus() == OrderRefundStatus.REJECT_RETURN) {
      op = new OrderRefundOperateDetail(0, 3, orderRefund.getRejectReturnTime(), "卖家没有收到货",
          "卖家没有收到货");
      opDetails.add(op);
    }

//		op = new OrderRefundOperateDetail();
//		op.setType(1);
//		op.setTime(Calendar.getInstance().getTime());
//		op.setFrom(3);
//		op.setTitle("凭证");
//		op.setContent("http://img-prod.kkkd.com/FmazE7eNjMZutzVQU-GcoHFTinC-?imageView2/2/w/480/q/100");
//		opDetails.add(op);
    return opDetails;
  }

  @Override
  public List<OrderRefundVO> listOrderRefundByAdmin(Map<String, Object> params, Pageable page) {
    return orderRefundMapper.listOrderRefundByAdmin(params, page);
  }

  @Override
  public Long countOrderRefundByAdmin(Map<String, Object> params) {
    return orderRefundMapper.countOrderRefundByAdmin(params);
  }

  @Override
  @Transactional
  public int confirmSellerByAdmin(String id, String opType, String adminRemark) {
    OrderRefund orderRefund = load(id);
    Order order = orderService.load(orderRefund.getOrderId());
    int rc = orderRefundMapper.confirmSellerByAdmin(id, opType, adminRemark);
    if (rc != 1 || order.getStatus() != OrderStatus.REFUNDING) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, order.getOrderNo() + " is failed ");
    }
    if ("rejectSeller".equals(opType)) {
      //不同意卖家操作，使退款申请继续进行
      OrderRefund updateOrderRefund = new OrderRefund();
      updateOrderRefund.setId(id);
      updateOrderRefund.setStatus(OrderRefundStatus.SUBMITTED);
      rc = orderRefundMapper.updateByPrimaryKeySelective(updateOrderRefund);
    } else if ("acceptSeller".equals(opType)) {
      //同意卖家的操作，使订单还原成原状态
      Order updateOrder = new Order();
      updateOrder.setId(order.getId());
      updateOrder.setStatus(orderRefund.getOrderStatus());
      rc = orderService.update(updateOrder);
    }
    if (rc != 1) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, order.getOrderNo() + " is failed ");
    }
    return 1;
  }

  @Override
  public int autoConfirm() {
    int retVal = 0;
    List<OrderRefund> orderRefunds = orderRefundMapper.selectAutoSign(autoConfirmDay);
    for (OrderRefund orderRefund : orderRefunds) {
      try {
        log.debug("auto confirm refund refundId=[" + orderRefund.getId() + "]");
        executeBySystem(OrderRefundActionType.CONFIRM, orderRefund, null);
        retVal++;
      } catch (Exception e) {
        log.error("autoConfirm failed refundId=[" + orderRefund.getId() + "]" + "  message=" + e
            .getMessage());
        continue;
      }
    }
    return retVal;
  }


  @Override
  public int autoSuccessWithBuyerNoShip() {
    // 同意申请，但是买家7天内没有发货的申请
    List<OrderRefund> orderRefunds = orderRefundMapper
        .selectAutoSuccessWithBuyerNoShip(autoSuccessWithBuyerNoShipDay);
    if (orderRefunds == null || orderRefunds.isEmpty()) {
      return 0;
    }

    int count = 0;
    for (OrderRefund orderRefund : orderRefunds) {
      try {
        log.debug("auto Success With Buyer No Ship, refundId=[" + orderRefund.getId() + "]");
        //保持订单的原来状态，有task处理状态转换
        //orderRefund.setOrderStatus(OrderStatus.SUCCESS);
        //退款状态为取消
        executeBySystem(OrderRefundActionType.CANCEL, orderRefund, null);
        count++;
      } catch (Exception e) {
        log.error("auto Success With Buyer No Ship failed, refundId=[" + orderRefund.getId() + "]"
            + "  message=" + e.getMessage());
        continue;
      }
    }
    return count;
  }


  @Override
  public int autoClosedWithSellerNoSign() {
    // 获取卖家10天了还没有确认收货的退货申请
    List<OrderRefund> orderRefunds = orderRefundMapper
        .selectAutoClosedWithSellerNoSign(autoClosedWithSellerNoSignDay);
    if (orderRefunds == null || orderRefunds.isEmpty()) {
      return 0;
    }

    int retVal = 0;
    for (OrderRefund orderRefund : orderRefunds) {
      try {
        log.debug("autoClosed With Seller No Sign, refundId=[" + orderRefund.getId() + "]");
        executeBySystem(OrderRefundActionType.SIGN, orderRefund, null);
        retVal++;
      } catch (Exception e) {
        log.error("autoClosed With Seller No Sign failed, refundId=[" + orderRefund.getId() + "]"
            + "  message=" + e.getMessage());
        continue;
      }
    }
    return retVal;
  }

  @Override
  public int successByOrderId(String orderid) {
    return orderRefundMapper.successByOrderId(orderid);
  }

  @Override
  public OrderRefund loadOrderRefund(String orderId){
    return orderRefundMapper.loadOrderRefund(orderId);
  }

  @Override
  @Transactional
  public void applyChangeForAdmin(String orderId, String changeReason, String orderItems){
    // 获取订单相关信息
    OrderVO order = orderService.loadVO(orderId);
    OrderRefund orderRefund = new OrderRefund();
    orderRefund.setOrderId(orderId);
    orderRefund.setRefundFee(BigDecimal.ZERO);

    //处理业务逻辑
    orderRefund.setBuyerRequire(3);
    orderRefund.setApplyNo(UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.R));
    orderRefund.setOrderStatus(order.getStatus());
    orderRefund.setStatus(OrderRefundStatus.APPLY_FOR_CHANGE);
    orderRefund.setRefundReason(changeReason);
    OrderItem orderItem = order.getOrderItems().get(0);//取出第一条数据
    orderRefund.setProductName(orderItem.getProductName());
    orderRefund.setImg(orderItem.getProductImg());//设置订单商品图片

    String[] split = orderItems.split(",");
    List<String> orderItemIds = new ArrayList<>();
    List<String> amount = new ArrayList<>();
    for(int i =0; i < split.length; i++){
      if(StringUtil.isNotEmpty(split[i])) {
        if(i == 0) {
          orderItemIds.add(split[i]);
          continue;
        }
        if(i == 1) {
          amount.add(split[i]);
          continue;
        }
        if(i % 2 == 0){
          orderItemIds.add(split[i]);
        }else{
          amount.add(split[i]);
        }
      }
    }

    if(!(orderItemIds.size() >0)){
      throw new RuntimeException("换货商品信息获取异常");
    }
    List<OrderItem> orderItemList = new ArrayList<>();
    for (int i = 0; i < orderItemIds.size(); i++) {
      if(StringUtil.isNotEmpty(orderItemIds.get(i))){
        OrderItem item = new OrderItem();
        item.setId(orderItemIds.get(i));//设置换货商品id
        //数量设置
        if(!(amount.size() > 0) || StringUtil.isEmpty(amount.get(i))){
          throw new RuntimeException("换货商品数量异常");
        }
        orderItem.setAmount(Integer.parseInt(amount.get(i)));
        //加入商品列表
        orderItemList.add(orderItem);
      }else{
        throw new RuntimeException("换货商品信息获取异常");
      }
    }

    if (orderItemList.size() > 0) {
      orderService.requestChange(order.getId(), orderItemList);//调用服务层的请求换货的逻辑
    } else {
      throw new BizException(GlobalErrorCode.REPEAT_SUBMIT, "未选择换货商品");
    }
    this.insert(orderRefund);//插入数据到退货表
  }

  @Override
  public boolean updateWmsCheckStatus(String orderId, String logiCompany, String logiNumber){
    OrderRefund refund =  loadOrderRefund(orderId);
    refund.setLogisticsCompany(logiCompany);
    refund.setLogisticsNo(logiNumber);
    refund.setStatus(OrderRefundStatus.SUBMITTED);//状态设置为已提交
    return update(refund) > 0;
  }

  @Override
  @Transactional
  public void applyForRefund(OrderRefundRequestForm form){
    // 获取订单相关信息
    OrderVO order = orderService.loadVO(form.getOrderId());
    OrderRefund orderRefund = new OrderRefund();
    BeanUtils.copyProperties(form, orderRefund);
    BigDecimal refundFee = order.getTotalFee().setScale(2, BigDecimal.ROUND_DOWN);
    orderRefund.setRefundFee(refundFee);

    //处理业务逻辑
    orderRefund.setApplyNo(UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.R));
    orderRefund.setOrderStatus(order.getStatus());
    orderRefund.setStatus(OrderRefundStatus.APPLY_FOR_REFUND);
    OrderItem orderItem = order.getOrderItems().get(0);
    orderRefund.setProductName(orderItem.getProductName());
    orderRefund.setImg(orderItem.getProductImg());
    orderService.requestRefund(order.getId());
    insert(orderRefund);
    //更新中台订单表的状态
    orderHeaderService.updateStatusByOrderNo(Integer.toString(6), order.getOrderNo());
  }

  @Override
  @Transactional
  public boolean applyForChange(OrderChangeRequestForm form){
    // 获取订单相关信息
    OrderVO order = orderService.loadVO(form.getOrderId());
    OrderRefund orderRefund = new OrderRefund();

    BeanUtils.copyProperties(form, orderRefund);

    //处理业务逻辑
    orderRefund.setRefundFee(BigDecimal.ZERO);
    orderRefund.setApplyNo(UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.R));
    orderRefund.setOrderStatus(order.getStatus());
    orderRefund.setStatus(OrderRefundStatus.APPLY_FOR_CHANGE);
    orderRefund.setRefundReason(form.getChangeReason());
    orderRefund.setRefundMemo(form.getChangeMemo());
    orderRefund.setRefundImg(form.getChangeImg());
    OrderItem orderItem = order.getOrderItems().get(0);//FIXME 待优化
    orderRefund.setProductName(orderItem.getProductName());
    orderRefund.setImg(orderItem.getProductImg());

    List<OrderItem> orderItemList = form.getOrderItemList();//获取用户选择的商品
    if (orderItemList != null && orderItemList.size() > 0) {
      orderService.requestChange(order.getId(), orderItemList);//进入请求换货工作流
    } else {
      throw new BizException(GlobalErrorCode.REPEAT_SUBMIT, "用户未选择换货商品");
    }
    //插入数据到退货表
    return this.insert(orderRefund) > 0;
  }

  @Override
  @Transactional
  public boolean applyReissue(OrderReissueRequestForm form){
    // 获取订单相关信息
    OrderVO order = orderService.loadVO(form.getOrderId());
    OrderRefund orderRefund = new OrderRefund();
    BeanUtils.copyProperties(form, orderRefund);
    orderRefund.setRefundFee(BigDecimal.ZERO);//金额置零

    //处理业务逻辑
    orderRefund.setApplyNo(UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.R));
    orderRefund.setOrderStatus(order.getStatus());
    orderRefund.setStatus(OrderRefundStatus.APPLY_FOR_REISSUE);//申请补货
    orderRefund.setRefundReason(form.getReissueReason());//设置补货原因
    orderRefund.setRefundMemo(form.getReissueMemo());//设置补货说明
    orderRefund.setRefundImg(form.getReissueImg());//设置补货凭证
    OrderItem orderItem = order.getOrderItems().get(0);//取出第一条数据
    orderRefund.setProductName(orderItem.getProductName());
    orderRefund.setImg(orderItem.getProductImg());//设置订单商品图片
    List<OrderItem> orderItemList = form.getOrderItemList();//获取用户选择的商品(可能多个)
    if (orderItemList != null && orderItemList.size() > 0) {
      orderService.requestReissue(order.getId(), orderItemList);//调用服务层的请求补货的逻辑
    } else {
      throw new BizException(GlobalErrorCode.REPEAT2_SUBMIT, "用户未选择补货商品");
    }
    return this.insert(orderRefund) > 0;//插入数据到退货表
  }

}
