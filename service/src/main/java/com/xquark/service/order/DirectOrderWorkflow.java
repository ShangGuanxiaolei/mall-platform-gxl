package com.xquark.service.order;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.transaction.annotation.Transactional;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.User;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.OrderActionType;
import com.xquark.event.OrderActionEvent;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;

/**
 * 即时到帐交易：卖家发货后交易就完成
 *
 * @author odin
 * @author ahlon
 */
public class DirectOrderWorkflow extends OrderWorkflow {

  @Autowired
  private ApplicationContext applicationContext;

  private Logger log = LoggerFactory.getLogger(getClass());

  private OrderStatus[] statuses = new OrderStatus[]{
      OrderStatus.SUBMITTED,
      OrderStatus.CANCELLED,
      OrderStatus.PAID,
      OrderStatus.SHIPPED,
      OrderStatus.SUCCESS
  };

  private OrderActionType[] actions = new OrderActionType[]{
      OrderActionType.SUBMIT,
      OrderActionType.CANCEL,
      OrderActionType.PAY,
      OrderActionType.SHIP,
      OrderActionType.SIGN
  };

  @Override
  public OrderActionType[] getNextAction(Order order, User user) {
    OrderStatus status = order.getStatus();
    switch (status) {
      case SUBMITTED: // 已提交
        if (isBuyer(order, user)) {
          return new OrderActionType[]{OrderActionType.CANCEL, OrderActionType.PAY};
        }
        break;
      case CANCELLED: // 取消
        break;
      case PAID: // 已付款
        if (isSeller(order, user)) {
          return new OrderActionType[]{OrderActionType.SHIP};
        }
        break;
      case SHIPPED: // 已发货
        if (isBuyer(order, user)) {
          return new OrderActionType[]{OrderActionType.SIGN};
        }
        break;
      case SUCCESS: // 交易成功
        break;
      default:
        break;
    }
    return new OrderActionType[0];
  }

  @Override
  @Transactional
  public void execute(Order order, User user, OrderActionType action, Map<String, Object> params) {
    // 1. check privileges
    checkPrivileges(order, user, action);

    // 2. check pre conditions, like order status, etc.
    checkPreConditions(order, action);

    // 3. result handle
    switch (action) {
      case CANCEL:
        cancel(order);
        break;
      case PAY:
        if (!pay(order)) {
          log.warn("订单已经支付，无需重新支付");
          return;
        }
        break;
      case SHIP:
        String logisticCompany =
            params.get(LOGISTICS_COMPANY_PROPERTY) != null ? params.get(LOGISTICS_COMPANY_PROPERTY)
                .toString() : "";
        order.setLogisticsCompany(logisticCompany);
        String logisticOrderNo =
            params.get(LOGISTICS_ORDERNO_PROPERTY) != null ? params.get(LOGISTICS_ORDERNO_PROPERTY)
                .toString() : "";
        order.setLogisticsOrderNo(logisticOrderNo);
        ship(order);
        break;
      case SIGN:
        // do nothing
        break;
      default:
        unkownAction(order, action);
        break;
    }

    // 4. fire events
    ApplicationEvent event = new OrderActionEvent(action, order);
    applicationContext.publishEvent(event);
    log.info("fire event[" + action.name() + "] for order[" + order.getOrderNo() + "]");
  }

  @Override
  @Transactional
  public void executeBySystem(Order order, OrderActionType action, Map<String, Object> params) {
    // 1. check privileges and status
    checkPreConditions(order, action);

    // 2. result handle
    switch (action) {
      case CANCEL:
        cancel(order);
        break;
      case PAY:
        if (!pay(order)) {
          log.warn("订单已经支付，无需重新支付");
          return;
        }
        break;
      default:
        unkownAction(order, action);
        break;
    }

    // 3. fire event
    ApplicationEvent event = new OrderActionEvent(action, order);
    applicationContext.publishEvent(event);
    log.info("fire event[" + action.name() + "] for order[" + order.getOrderNo()
        + "] by system autoexec");
  }

  protected void checkPrivileges(Order order, User user, OrderActionType action) {
    log.info("check user[" + user.getLoginname() + "]'s privileges of order[" + order.getOrderNo()
        + "] by action[" + action.name() + "]");

    switch (action) {
      case CANCEL:
      case PAY:
        if (!isBuyer(order, user)) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
              "user has no privileges to do this action");
        }
        break;
      case SHIP:
        if (!isSeller(order, user)) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
              "user has no privileges to do this action");
        }
        break;
      default:
        break;
    }
  }

  protected boolean checkPreConditions(Order order, OrderActionType action) {
    log.info("check precondition for order[" + order.getOrderNo() + "] by action[" + action.name()
        + "]");

    switch (action) {
      case CANCEL:
        if (!OrderStatus.SUBMITTED.equals(order.getStatus())) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
              "this action[" + action.name() + "] can not be done with order status[" + order
                  .getStatus() + "]");
        }
        break;
      case PAY:
        if (!OrderStatus.SUBMITTED.equals(order.getStatus())) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
              "this action[" + action.name() + "] can not be done with order status[" + order
                  .getStatus() + "]");
        } else if (!OrderStatus.SUBMITTED.equals(order.getStatus())) {
          log.warn(
              "this action[" + action.name() + "] has already been paid with order status[" + order
                  .getStatus() + "] orderNo=[" + order.getOrderNo() + "]");
          return false;
        }
        break;
      case SHIP:
        if (!OrderStatus.PAID.equals(order.getStatus())) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
              "this action[" + action.name() + "] can not be done action with order status[" + order
                  .getStatus() + "]");
        }
        break;
      default:
        break;
    }

    return true;
  }

  @Transactional
  protected void ship(Order order) {
    Order record = new Order();
    record.setId(order.getId());
    record.setStatus(OrderStatus.SUCCESS);
    record.setShippedAt(new Date());
    record.setLogisticsCompany(order.getLogisticsCompany());
    record.setLogisticsOrderNo(order.getLogisticsOrderNo());

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("id", order.getId());
    params.put("status", OrderStatus.PAID);

    if (orderMapper.updateOrderByShip(params, record) == 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "order ship action has not been done");
    }
    log.debug("order[" + order.getOrderNo() + "] is shipped");
  }

  @Override
  public OrderStatus[] getOrderStatuses() {
    return this.statuses;
  }

  @Override
  public OrderActionType[] getOrderActions() {
    return this.actions;
  }

}
