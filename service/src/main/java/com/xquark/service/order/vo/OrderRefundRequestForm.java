package com.xquark.service.order.vo;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.validation.group.order.ApplyForRefundGroup;
import com.xquark.dal.validation.group.order.ProceedRefundGroup;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

public class OrderRefundRequestForm {

  String id;
  @ApiModelProperty(value = "订单id")
  @NotBlank(groups = {ApplyForRefundGroup.class, ProceedRefundGroup.class})
  String orderId;
  @ApiModelProperty(value = "1代表仅退款，2代表退款退货,3代表换货")
  Integer buyerRequire = 1;  //买家要求
  Integer buyerReceived;  //买家是否已收到货,无需传入
  @NotBlank(groups = {ApplyForRefundGroup.class}, message = "退款原因不能为空")
  String refundReason;  //退款原因
  @Digits(integer = 10, fraction = 2, groups = {ApplyForRefundGroup.class}, message = "退款金额格式不正确")
  BigDecimal refundFee;    //退款金额
  @Length(max = 100, groups = {ApplyForRefundGroup.class}, message = "退款说明的最大长度为100")
  String refundMemo;    //退款说明
  private String refundImg; // 退款凭证（多张图片用,号区分）

  @NotBlank(groups = {ProceedRefundGroup.class}, message = "物流公司不能为空")
  private String logisticsCompany; // 物流公司
  @NotBlank(groups = {ProceedRefundGroup.class}, message = "物流编号不能为空")
  private String logisticsNo; // 物流编号


  public String getRefundImg() {
    return refundImg;
  }

  public void setRefundImg(String refundImg) {
    this.refundImg = refundImg;
  }

  public String getLogisticsCompany() {
    return logisticsCompany;
  }

  public void setLogisticsCompany(String logisticsCompany) {
    this.logisticsCompany = logisticsCompany;
  }

  public String getLogisticsNo() {
    return logisticsNo;
  }

  public void setLogisticsNo(String logisticsNo) {
    this.logisticsNo = logisticsNo;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public Integer getBuyerRequire() {
    return buyerRequire;
  }

  public void setBuyerRequire(Integer buyerRequire) {
    this.buyerRequire = buyerRequire;
  }

  public Integer getBuyerReceived() {
    return buyerReceived;
  }

  public void setBuyerReceived(Integer buyerReceived) {
    this.buyerReceived = buyerReceived;
  }

  public String getRefundReason() {
    return refundReason;
  }

  public void setRefundReason(String refundReason) {
    this.refundReason = refundReason;
  }

  public BigDecimal getRefundFee() {
    return refundFee;
  }

  public void setRefundFee(BigDecimal refundFee) {
    this.refundFee = refundFee;
  }

  public String getRefundMemo() {
    return refundMemo;
  }

  public void setRefundMemo(String refundMemo) {
    this.refundMemo = refundMemo;
  }


}
