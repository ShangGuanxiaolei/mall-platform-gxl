package com.xquark.service.order.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.OrderItemCommentMapper;
import com.xquark.dal.model.OrderItemComment;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.order.OrderItemCommentService;

@Service("orderItemCommentService")
public class OrderItemCommentServiceImpl extends BaseServiceImpl implements
    OrderItemCommentService {

  @Autowired
  private OrderItemCommentMapper orderItemCommentMapper;

  @Override
  public int insert(OrderItemComment record) {
    return orderItemCommentMapper.insert(record);
  }

  @Override
  public int insertOrder(OrderItemComment orderItemComment) {
    return orderItemCommentMapper.insert(orderItemComment);
  }

  @Override
  public OrderItemComment load(String id) {
    return orderItemCommentMapper.selectByPrimaryKey(id);
  }

  @Override
  public int sellerReply(String id, String reply) {
    OrderItemComment record = new OrderItemComment();
    record.setId(id);
    record.setReply(reply);
    return orderItemCommentMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public List<OrderItemComment> selectByProductId(String productId, Pageable pager) {
    return orderItemCommentMapper.selectByProductId(productId, pager);
  }

  @Override
  public Long countByProductId(String productId) {
    return orderItemCommentMapper.countByProductId(productId);
  }

  @Override
  public OrderItemComment selectLastestByProductId(String productId) {
    return orderItemCommentMapper.selectLastestByProductId(productId);
  }

  @Override
  public Long countByBuyerIdAndSellerId(String buyerId, String sellerId) {
    return orderItemCommentMapper.countByBuyerIdAndSellerId(buyerId, sellerId);
  }

  @Override
  public List<OrderItemComment> selectByEntityWithPage(OrderItemComment orderItemComment,
      Pageable pager) {
    return orderItemCommentMapper.selectByEntityWithPage(orderItemComment, pager);
  }

  @Override
  public List<OrderItemComment> selectUnreadBySellerId(String sellerId, Pageable pager) {
    return orderItemCommentMapper.selectUnreadBySellerId(sellerId, pager);
  }

  @Override
  public Long countNewOrderItemReply(String sellerId, Date startTime) {
    return orderItemCommentMapper.countNewOrderItemReplyBySellerId(sellerId, startTime);
  }

  @Override
  public List<OrderItemComment> selectByOrderItemId(String orderId) {
    return orderItemCommentMapper.selectByOrderItemId(orderId);
  }

  @Override
  public BigDecimal selectProcuctScoreAvg(String productId) {
    return orderItemCommentMapper.selectProcuctScoreAvg(productId);
  }

  @Override
  public Boolean checkExistsByOrderId(String orderId) {
    return orderItemCommentMapper.checkExistsByOrderId(orderId) > 0;
  }

  @Override
  public int delete(String id) {
    if (org.apache.commons.lang3.StringUtils.isBlank(id)) {
      return 0;
    }

    return orderItemCommentMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int emptyReply(String id) {
    int rc = orderItemCommentMapper.emptyReply(id);
    return rc;
  }
}
