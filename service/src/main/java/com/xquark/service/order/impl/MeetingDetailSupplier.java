package com.xquark.service.order.impl;

import com.google.common.base.Supplier;
import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.dal.mybatis.IdTypeHandler;

/**
 * @author wangxinhua.
 * @date 2018/11/3
 * 大会订单supplier
 */
public class MeetingDetailSupplier implements Supplier<PromotionOrderDetail> {

  private final String orderNo;
  private final String subOrderNo;
  private final String orderItemNo;
  private final Long cpId;
  private final String pCode;

  public MeetingDetailSupplier(String orderNo, String subOrderNo, String orderItemNo, Long cpId,String pCode) {
    this.orderNo = orderNo;
    this.subOrderNo = subOrderNo;
    this.orderItemNo = orderItemNo;
    this.cpId = cpId;
    this.pCode= pCode;
  }


  @Override
  public PromotionOrderDetail get() {
    PromotionOrderDetail promotionOrderDetail = new PromotionOrderDetail();
    promotionOrderDetail.setOrder_head_code(orderNo);
    promotionOrderDetail.setOrder_detail_code(String.valueOf(IdTypeHandler.decode(orderItemNo)));
    //活动详情编码
    promotionOrderDetail.setP_detail_code(pCode);
    promotionOrderDetail.setPiece_group_tran_code(null);
    promotionOrderDetail.setPiece_group_detail_code(null);
    promotionOrderDetail.setMember_id(cpId);
    promotionOrderDetail.setSub_order_no(subOrderNo);
    //活动类型
    promotionOrderDetail.setP_type("meeting");
    promotionOrderDetail.setP_code(pCode);
    return promotionOrderDetail;
  }

}
