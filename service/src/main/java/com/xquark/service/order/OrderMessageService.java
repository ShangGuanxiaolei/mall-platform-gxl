package com.xquark.service.order;

import com.xquark.dal.model.User;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.SellerInfoVO;

public interface OrderMessageService {


  boolean sendBuyerSMSOrderDelayRemind(OrderVO order, User seller, User buyer);

  boolean sendBuyerSMSOrderSubmittedForSeller(OrderVO order, SellerInfoVO seller, User buyer);

  boolean sendBuyerSMSOrderSubmitted(OrderVO order, User seller, User buyer);


  boolean sendBuyerSMSMainOrderSubmitted(MainOrderVO order, User buyer);

  /**
   * 买家付款后发短信消息给买家
   */
  boolean sendBuyerSMSOrderPaied(OrderVO order, User seller, User buyer);

  boolean sendBuyerSMSOrderPaied(OrderVO order, SellerInfoVO seller, User buyer);

  boolean sendBuyerSMSMainOrderPaied(MainOrderVO order, User buyer);

  /**
   * 买家付款后发短信消息给卖家
   */
  boolean sendSellerSMSOrderPaied(OrderVO order, User seller,
      User buyer);

  /**
   * 卖家发货后发短信消息给买家
   */
  boolean sendBuyerSMSOrderShipped(OrderVO order, User seller,
      User buyer);

  /**
   * 系统自动取消后发短信消息给买家
   */
  boolean sendBuyerSMSOrderCancelled(OrderVO order, User seller,
      User buyer);

  /**
   * 系统自动取消后发短信消息给卖家
   */
  boolean sendSellerSMSOrderCancelled(OrderVO order, User seller,
      User buyer);

  /**
   * 活动中奖通知买家
   */
  boolean sendBuyerSMSOrderFree(OrderVO order, UserPartnerType userType);

  boolean sendBuyerSMSOrderFreeFinish(OrderVO order, UserPartnerType userType);

  /**
   * 发送订单退货申请的相关审核信息
   */
  boolean sendSellerSMSOrderRefundRequest(OrderVO order);

  boolean sendBuyerSMSOrderRefundSuccess(OrderVO order);

  boolean sendBuyerSMSOrderRefundReject(OrderVO order);

  boolean sendBuyerSMSOrderRefundAgreeReturn(OrderVO order);


}
