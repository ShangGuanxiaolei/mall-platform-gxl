package com.xquark.service.order.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xquark.dal.mapper.OrderRefundRecordMapper;
import com.xquark.dal.model.OrderRefundRecord;
import com.xquark.service.order.OrderRefundRecordService;

@Service("alipayBatchService")
public class OrderRefundRecordServiceImpl implements OrderRefundRecordService {

  @Autowired
  public OrderRefundRecordMapper refundRecoedMapper;

  @Override
  public List<OrderRefundRecord> listOrderByBatchNo(String batchNo) {
    return refundRecoedMapper.listOrderByBatchNo(batchNo);
  }

  @Override
  public void saveOrderRecord(OrderRefundRecord batchOrder) {

    refundRecoedMapper.saveOrderByOrderNo(batchOrder);
  }

  @Override
  @Transactional
  public void saveOrderRecord(List<OrderRefundRecord> lsOrder) {
    for (OrderRefundRecord batchOrder : lsOrder) {
      this.saveOrderRecord(batchOrder);
    }

  }

  public List<String> getOldBatchNOByOrderNo(String orderNo) {
    return this.refundRecoedMapper.listBatchNoByOrderNo(orderNo);
  }


  @Override
  public List<OrderRefundRecord> listOrderByBatchNoAndTradeNo(List<OrderRefundRecord> lsRecord,
      String trade_no) {
    List<OrderRefundRecord> lsOrderRecord = new ArrayList<OrderRefundRecord>();
    for (OrderRefundRecord record : lsRecord) {
      if (record.getTrade_no().equals(trade_no)) {
        lsOrderRecord.add(record);
      }
    }
    return lsOrderRecord;
  }

  @Override
  public List<String> listBatchNoByOrderNo(String orderNo) {
    return this.refundRecoedMapper.listBatchNoByOrderNo(orderNo);
  }


}
