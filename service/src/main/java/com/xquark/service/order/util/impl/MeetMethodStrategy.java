package com.xquark.service.order.util.impl;


import com.xquark.dal.model.OrderDeliveryDestination;
import com.xquark.dal.model.Supplier;
import java.lang.String;

import com.xquark.dal.type.SkuCodeResourcesType;
import com.xquark.dal.type.SkuType;
import com.xquark.service.order.impl.OrderSplitterServiceImpl.MappingContext;
import com.xquark.service.order.util.AbstractProxyMethodStrategy;
import com.xquark.service.order.util.ProxyContext;

/**
 * meet方法
 *
 * @author jackzhu
 * @since 2018/11/16
 **/
public class MeetMethodStrategy extends AbstractProxyMethodStrategy {


  public MeetMethodStrategy(ProxyContext proxyContext) {
    super(proxyContext);
  }

  @Override
  public Object execute() {
    final String backAble = proxyContext.getBackAble();
    final Supplier supplier = proxyContext.getSupplier();
    //拿到传进来的第一个参数
    MappingContext context = (MappingContext) proxyContext.getArgs()[0];
    //拿到供应商名和Sku名
    final SkuType codeResourcesType = SkuCodeResourcesType.valueSupplierCode(supplier.getCode());
    final String enumType = obtainErcEnumType(context.isRefundAble(),
        supplier.getCode());
    final String supplierType = codeResourcesType.getName();
    final String resourceType = context.getSkuCodeResourcesType().getName();
    // 判断可退不可退
    final boolean isRefundAble =
            REFUNDABLE.equals(backAble) == context.isRefundAble();
    //判断供应商类型和来源类型时候匹配
    if (supplierType.equals(resourceType) && isRefundAble) {
      return new OrderDeliveryDestination(
          supplier.getName() + (REFUNDABLE.equals(backAble)?BACK_ENABLE:BACK_UNABLE), codeResourcesType, enumType, "", "");
    }
    return null;
  }
}
