package com.xquark.service.order.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.OrderUserDeviceMapper;
import com.xquark.dal.model.OrderUserDevice;
import com.xquark.service.order.OrderUserDeviceService;

@Service("orderUserDeviceService")
public class OrderUserDeviceServiceImpl implements OrderUserDeviceService {

  @Autowired
  private OrderUserDeviceMapper orderUserDeviceMapper;

  @Override
  public OrderUserDevice save(OrderUserDevice orderUserDevice) {
    orderUserDeviceMapper.insert(orderUserDevice);
    return orderUserDevice;
  }

}
