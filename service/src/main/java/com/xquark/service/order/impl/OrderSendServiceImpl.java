package com.xquark.service.order.impl;

import com.xquark.dal.mapper.OrderSendMapper;
import com.xquark.dal.model.OrderSend;
import com.xquark.service.order.OrderSendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * User: a9175
 * Date: 2018/6/17.
 * Time: 15:08
 */
@Service
public class OrderSendServiceImpl implements OrderSendService {
    @Autowired
    private OrderSendMapper orderSendMapper;

    public List<OrderSend> findAll() {
        return null;
    }

    /**
     * 添加 菠萝派的发货订单
     * @param orderSend
     */
    @Override
    public void add(OrderSend orderSend) {
        orderSendMapper.insert(orderSend);
    }

    //添加操作
    public void update(OrderSend orderSend) {

    }

    public OrderSend findOne(Long id) {
        return null;
    }

    public void delete(Long[] ids) {

    }
}
