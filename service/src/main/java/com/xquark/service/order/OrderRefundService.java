package com.xquark.service.order;

import com.xquark.dal.status.OrderRefundStatus;
import java.util.List;
import java.util.Map;

import com.xquark.service.order.vo.OrderChangeRequestForm;
import com.xquark.service.order.vo.OrderRefundRequestForm;
import com.xquark.service.order.vo.OrderReissueRequestForm;
import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.OrderRefund;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.OrderRefundActionType;
import com.xquark.dal.vo.OrderRefundOperateDetail;
import com.xquark.dal.vo.OrderRefundVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.PropertyDict;
import com.xquark.service.BaseEntityService;

public interface OrderRefundService extends BaseEntityService<OrderRefund> {

  List<OrderRefundVO> list(String sellerId, Pageable pageable);

  int update(OrderRefund orderRefund);

  OrderRefund submit(OrderRefund orderRefund);

  OrderRefundVO loadVO(String id);

  List<OrderRefund> listByOrderId(String orderId);

  Boolean updateStatusByOrderId(String orderId, OrderRefundStatus status,
      Map<String, Object> params);

  List<OrderRefundOperateDetail> initOpDetail(OrderRefund orderRefund,
      String from);

  List<PropertyDict> loadRefundReasonDict(OrderStatus orderStatus);

  void execute(OrderRefundActionType action, OrderRefund orderRefund,
      Map<String, Object> params);

  OrderVO loadOrderVO(String orderId, String id);

  int updateByModifyRequest(OrderRefund orderRefund);

  List<OrderRefundVO> listOrderRefundByAdmin(Map<String, Object> params, Pageable page);

  Long countOrderRefundByAdmin(Map<String, Object> params);

  int confirmSellerByAdmin(String id, String opType, String adminRemark);

  /**
   * 买家提交申请，卖家超过3天未操作时自动同意
   */
  int autoConfirm();

  void executeBySystem(OrderRefundActionType action, OrderRefund orderRefund,
      Map<String, Object> params);

  int autoSuccessWithBuyerNoShip();

  int autoClosedWithSellerNoSign();

  /**
   * 根据orderid将退款申请记录状态更新为success
   */
  int successByOrderId(String orderId);

  OrderRefund loadOrderRefund(String orderId);

  void applyChangeForAdmin(String orderId, String changeReason, String orderItems);

  boolean updateWmsCheckStatus(String orderId, String logiCompany, String logiNumber);

  void applyForRefund(OrderRefundRequestForm form);

  boolean applyForChange(OrderChangeRequestForm form);

  boolean applyReissue(OrderReissueRequestForm form);

}
