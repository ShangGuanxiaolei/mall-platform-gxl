package com.xquark.service.order.util.impl;

import com.xquark.service.order.util.AbstractProxyMethodStrategy;
import com.xquark.service.order.util.ProxyContext;

/**
 * @author jackzhu
 * @since 2018/11/16
 **/
public class OrderMethodStrategy extends AbstractProxyMethodStrategy {

  private static int order = 3000;

  public OrderMethodStrategy(ProxyContext proxyContext) {
    super(proxyContext);
  }

  @Override
  public Object execute() {
    final int incrementNum = BACK_ENABLE.equals(proxyContext.getBackAble()) ? 1 : 1000;
    order += incrementNum;
    return order;
  }
}
