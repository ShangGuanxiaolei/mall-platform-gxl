package com.xquark.service.order;


import com.xquark.dal.model.OrderAddress;
import com.xquark.service.BaseEntityService;
import com.xquark.service.order.vo.OrderAddressVO;

public interface OrderAddressService extends BaseEntityService<OrderAddress> {
  /**
   * 用户删除订单地址
   * @param userId
   * @return
   */
  //int archiveAddress(String addressId);

  /**
   * 查询订单的订单地址
   *
   * @return OrderAddress
   */
  OrderAddressVO selectByOrderId(String orderId);

  int update(OrderAddress orderAddress);

}
