package com.xquark.service.order.impl;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.IUser;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.model.pay.PaymentConfig;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.MainOrderStatus;
import com.xquark.dal.status.OrderRefundStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.*;
import com.xquark.dal.vo.*;
import com.xquark.event.MessageNotifyEvent;
import com.xquark.event.OrderActionEvent;
import com.xquark.event.XQMessageNotifyEvent;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.address.AddressService;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.cart.CartService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.cuservice.vo.Constant;
import com.xquark.service.domain.DomainService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.logistics.LogisticsGoodsService;
import com.xquark.service.msg.MessageService;
import com.xquark.service.order.*;
import com.xquark.service.order.vo.CustomerVO;
import com.xquark.service.order.vo.OrderAddressVO;
import com.xquark.service.outpay.ThirdPaymentQueryService;
import com.xquark.service.outpay.impl.OrderQuery;
import com.xquark.service.outpay.impl.PaymentFacade;
import com.xquark.service.outpay.impl.ThirdPaymentQueryRes;
import com.xquark.service.ownerAdress.CustomerOwnerAdress;
import com.xquark.service.pay.OutPayService;
import com.xquark.service.pay.PayRequestApiService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pricing.PricingService;
import com.xquark.service.pricing.vo.PricingResultVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductSkuVO;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.promotion.PromotionConfigService;
import com.xquark.service.sequence.IdSequenceService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.systemRegion.SystemRegionService;
import com.xquark.service.union.UnionService;
import com.xquark.service.user.RolePriceService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.thirds.umpay.api.util.StringUtil;
import com.xquark.utils.DateUtils;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.xquark.dal.status.OrderStatus.*;

public class OrderServiceImpl extends BaseServiceImpl implements OrderService {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private CartService cartService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private SkuMapper skuMapper;

  @Autowired
  private OrderMapper orderMapper;

  @Autowired
  private PromotionReserveMapper promotionReserveMapper;

  @Autowired
  private UserService userService;

  @Autowired
  private OrderItemMapper orderItemMapper;

  @Autowired
  private PricingService pricingService;

  @Autowired
  private PayRequestApiService payRequestApiService;

  @Autowired
  private AccountApiService accountApiService;

  @Autowired
  private AddressService addressService;

  @Autowired
  private CartItemMapper cartItemMapper;

  @Autowired
  private OrderMessageMapper orderMessageMapper;

  @Autowired
  private MessageService messageService;

  @Autowired
  private UnionService unionService;

  @Autowired
  private OrderMessageService orderMessageService;

  @Autowired
  private DomainService domainService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private MainOrderService mainOrderService;

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private RolePriceService rolePriceService;

  @Autowired
  private PromotionCouponService promotionCouponService;

  @Autowired
  private OrderRefundMapper orderRefundMapper;

  @Autowired
  private OrderItemCommentService orderItemCommentService;

  @Autowired
  private SystemRegionService systemRegionService;

  @Autowired
  private SfAppConfigMapper sfConfigMapper;

  @Autowired
  private SfRegionService sfRegionService;

  @Autowired
  private SystemRegionMapper systemRegionMapper;

  @Autowired
  private OrderAddressService orderAddressService;

  @Autowired
  private ProductService productService;

  @Autowired
  private LogisticsGoodsService logisticsGoodsService;

  @Autowired
  private SkuCombineMapper skuCombineMapper;

  @Autowired
  private SkuCombineExtraMapper skuCombineExtraMapper;

  @Autowired
  private OrderRefundService orderRefundService;

  @Autowired
  private CustomerOwnerAdress customerOwnerAdress;

  @Autowired
  private OrderQuery orderQuery;

  @Autowired
  private CashierService cashierService;

  @Autowired
  private PromotionConfigService promotionConfigService;

  @Autowired
  private OutPayService outPayService;

  @Autowired
  private OutpayMapper outpayMapper;

  @Autowired
  private PaymentFacade paymentFacade;

  @Autowired
  private OrderIdentityLogMapper orderIdentityLogMapper;

  @Autowired
  private IdSequenceService idSequenceService;

  @Autowired
  private CustomerProfileService customerProfileService;

  @Value("${site.web.host.name}")
  private String hostName;

  @Value("${site.web.host.name}")
  private String domainName;

  @Value("${tech.serviceFee.standard}")
  private BigDecimal serviceFeeStandard;

  @Value("${tech.serviceFee.rate}")
  private BigDecimal serviceFeeRate;

  @Value("${order.delaysign.date}")
  private String delayDay;

  @Value("${sf.api.token.profile}")
  private String profile;

  @Value("${sf.api.host.url}")
  private String host;
  @Value("${task.wms.interval}")
  private String wmsInterval;

  @Autowired
  private ThirdPaymentQueryService thirdPaymentQueryService;

  private Map<OrderType, OrderWorkflow> workflows;

  private static final String XIANGQU = "xiangqu";

  private static final String WMS_TYPE = "STADIUM";

  @Override
  @Transactional
  @Deprecated
  public Order submitByShop(String shopId, OrderAddress orderAddress,
                            String remark, String unionId, String tuId, Boolean danbao) {
    User buyer = (User) getCurrentUser();

    Order order = new Order();
    order.setShopId(shopId);
    // TODO handle if shop does not exist
    order.setSellerId(shopService.load(shopId).getOwnerId());
    order.setBuyerId(buyer.getId());
    // order.setPayType(payType);
    order.setRemark(remark);
    setOrderPartnerType(buyer, order);

    Domain domain = loadOrderDomain(buyer);
    order.setPartner(domain.getCode());
    order.setPartnerType(findPartnerTypeByPartner(domain.getCode()));

    setOrderTypeInfo(danbao, order);

    order.setUnionId(StringUtils.defaultIfBlank(unionId, null));
    order.setTuId(StringUtils.defaultIfBlank(tuId, null));

    List<CartItemVO> cartItems = cartService.listCartItems(shopId);
    Map<String , Integer> map = new HashMap<String , Integer>(
        cartItems.size());
    List<OrderItem> orderItems = new ArrayList<OrderItem>(cartItems.size());
    for (CartItem cartItem : cartItems) {
      OrderItem orderItem = new OrderItem();
      ProductSkuVO product = productService.load(cartItem.getProductId(),
          cartItem.getSkuId());
      if (product == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            "商品规格[" + cartItem.getSkuId() + "]不存在或者已经下架");
      }
      if (cartItem.getAmount() > product.getSku().getAmount()) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品["
            + product.getName() + "]规格["
            + product.getSku().getSpec() + "]库存仅剩"
            + product.getSku().getAmount() + "件，不能满足您需要的"
            + cartItem.getAmount() + "件。");
      }
      orderItem.setProductId(product.getId());
      orderItem.setProductName(product.getName());
      orderItem.setProductImg(product.getImg());
      orderItem.setPrice(product.getSku().getPrice());
      orderItem.setSkuId(product.getSku().getId());
      orderItem.setSkuStr(product.getSku().getSpec());
      orderItem.setAmount(cartItem.getAmount());
      orderItems.add(orderItem);
      map.put(orderItem.getSkuId(), orderItem.getAmount());
    }

    // 订单计价
    PricingResultVO prices = pricingService.calculate(map,
        orderAddress.getZoneId(), "");
    order.setGoodsFee(prices.getGoodsFee());
    order.setLogisticsFee(prices.getLogisticsFee());
    order.setDiscountFee(prices.getDiscountFee());
    order.setTotalFee(prices.getTotalFee());

    if (prices.getUsingPromotion() != null) {
      order.setPromotionId(prices.getUsingPromotion().getId());
    }
    setCouponInfo(prices, order);

    // 保存订单到数据库
    return save(order, orderAddress, orderItems, OrderStatus.SUBMITTED);
  }

  @Override
  @Transactional
  @Deprecated
  public Order submitBySkuId(String skuId, OrderAddress orderAddress,
                             String remark, String unionId, String tuId, Boolean danbao) {
    User buyer = (User) getCurrentUser();

    CartItem cartItem = cartItemMapper.selectByUserIdAndSku(buyer.getId(),
        skuId);
    if (cartItem == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          buyer.getLoginname() + ": 商品规格[" + skuId + "]不存在或者已经下架");
    }
    Sku sku = productService.loadSku(cartItem.getSkuId());
    Product product = productService.load(sku.getProductId());

    Order order = new Order();
    order.setShopId(product.getShopId());
    order.setSellerId(product.getUserId());
    order.setBuyerId(buyer.getId());
    setOrderTypeInfo(danbao, order);
    setOrderPartnerType(buyer, order);

    Domain domain = loadOrderDomain(buyer);
    order.setPartner(domain.getCode());
    order.setPartnerType(findPartnerTypeByPartner(domain.getCode()));

    order.setRemark(remark);
    order.setUnionId(StringUtils.defaultIfBlank(unionId, null));
    order.setTuId(StringUtils.defaultIfBlank(tuId, null));

    OrderItem orderItem = new OrderItem();
    orderItem.setProductId(product.getId());
    orderItem.setProductName(product.getName());
    orderItem.setProductImg(product.getImg());
    orderItem.setPrice(sku.getPrice());
    orderItem.setMarketPrice(sku.getMarketPrice());
    orderItem.setSkuId(sku.getId());
    // orderItem.setSkuStr(sku.getSpec());
    orderItem.setSkuStr(productService.findSkuSpec(sku, true));
    orderItem.setAmount(cartItem.getAmount());

    List<OrderItem> orderItems = new ArrayList<OrderItem>();
    orderItems.add(orderItem);

    Map<String , Integer> map = new HashMap<String , Integer>();
    map.put(orderItem.getSkuId(), orderItem.getAmount());

    // 订单计价
    PricingResultVO prices = pricingService.calculate(map,
        orderAddress.getZoneId(), "");
    order.setGoodsFee(prices.getGoodsFee());
    order.setLogisticsFee(prices.getLogisticsFee());
    order.setDiscountFee(prices.getDiscountFee());
    order.setTotalFee(prices.getTotalFee());

    if (prices.getUsingPromotion() != null) {
      order.setPromotionId(prices.getUsingPromotion().getId());
    }

    setCouponInfo(prices, order);

    // 保存订单到数据库
    return save(order, orderAddress, orderItems, OrderStatus.SUBMITTED);
  }

  @Override
  @Transactional
  @Deprecated
  public Order submitBySkuIds(List<String > skuIds, OrderAddress oa,
                              String remark, String unionId, String tuId, Boolean danbao) {
    User user = (User) getCurrentUser();
    CartItem ci = cartItemMapper.selectByUserIdAndSku(user.getId(),
        skuIds.get(0));
    Map<String , String > remarks = new HashMap<String , String >();
    remarks.put(ci.getShopId(), remark);

    List<Order> orders = submitBySkuIds(skuIds, oa, remarks, unionId, tuId,
        danbao);
    return orders.get(0);
  }

  @Override
  @Transactional
  @Deprecated
  public List<Order> submitBySkuIds(List<String > skuIds, OrderAddress oa,
                                    Map<String , String > remarks, String unionId, String tuId,
                                    Boolean danbao) {
    User user = (User) getCurrentUser();

    Map<String , List<CartItem>> cartItemsMap = new HashMap<String , List<CartItem>>();
    for (String skuId : skuIds) {
      CartItem cartItem = cartItemMapper.selectByUserIdAndSku(
          user.getId(), skuId);
      if (cartItem == null) {
        throw new BizException(GlobalErrorCode.NOT_FOUND,
            user.getLoginname() + ": 商品规格[" + skuId + "]不存在或者已经下架");
      }
      List<CartItem> list = cartItemsMap.get(cartItem.getShopId());
      if (list == null) {
        list = new ArrayList<CartItem>();
        cartItemsMap.put(cartItem.getShopId(), list);
      }
      list.add(cartItem);
    }

    List<Order> result = new ArrayList<Order>();
    for (Entry<String , List<CartItem>> entry : cartItemsMap.entrySet()) {
      String shopId = entry.getKey();
      Shop shop = shopService.load(shopId);

      Order order = new Order();
      order.setShopId(shopId);
      order.setSellerId(shop.getOwnerId());
      order.setBuyerId(user.getId());
      setOrderPartnerType(user, order);
      setOrderTypeInfo(danbao, order);

      Domain domain = loadOrderDomain(user);
      order.setPartner(domain.getCode());
      order.setPartnerType(findPartnerTypeByPartner(domain.getCode()));

      order.setRemark(remarks.get(shopId));

      order.setUnionId(StringUtils.defaultIfBlank(unionId, null));
      order.setTuId(StringUtils.defaultIfBlank(tuId, null));

      List<OrderItem> orderItems = new ArrayList<OrderItem>();
      Map<String , Integer> map = new HashMap<String , Integer>();
      for (CartItem cartItem : entry.getValue()) {
        Sku sku = productService.loadSku(cartItem.getSkuId());
        Product product = productService.load(cartItem.getProductId());
        OrderItem orderItem = new OrderItem();
        orderItem.setProductId(product.getId());
        orderItem.setProductName(product.getName());
        orderItem.setProductImg(product.getImg());
        orderItem.setPrice(sku.getPrice());
        orderItem.setMarketPrice(sku.getMarketPrice());
        orderItem.setSkuId(sku.getId());
        orderItem.setSkuStr(sku.getSpec());
        orderItem.setAmount(cartItem.getAmount());
        orderItems.add(orderItem);

        map.put(orderItem.getSkuId(), orderItem.getAmount());
      }

      PricingResultVO prices = pricingService.calculate(map,
          oa.getZoneId(), "");
      order.setGoodsFee(prices.getGoodsFee());
      order.setLogisticsFee(prices.getLogisticsFee());
      order.setDiscountFee(prices.getDiscountFee());
      order.setTotalFee(prices.getTotalFee());

      if (prices.getUsingPromotion() != null) {
        order.setPromotionId(prices.getUsingPromotion().getId());
      }
      setCouponInfo(prices, order);
      // 保存订单到数据库
      Order odr = save(order, oa, orderItems, OrderStatus.SUBMITTED);
      result.add(odr);
    }

    return result;
  }

  /**
   * 买家直接提交订单，不走购物车流程
   *
   * @param qty 购买数量
   */
  @Override
  @Transactional
  @Deprecated
  public Order submitBySkuId(String skuId, String proLiteralId, OrderAddress oa,
                             String remark, String unionId, String tuId, Boolean danbao, int qty) {
    User user = (User) getCurrentUser();

    Sku sku = productService.loadSku(skuId);
    Product product = productService.load(sku.getProductId());
    Shop shop = shopService.load(product.getShopId());

    Order order = new Order();
    order.setOrderSingleType(OrderSingleType.SUBMIT);//下单方式，直接购买，不走购物车流程
    order.setShopId(shop.getId());
    order.setSellerId(shop.getOwnerId());
    order.setBuyerId(user.getId());
    order.setRemark(remark);
    setOrderPartnerType(user, order);
    setOrderTypeInfo(danbao, order);

    Domain domain = loadOrderDomain(user);
    order.setPartner(domain.getCode());
    order.setPartnerType(findPartnerTypeByPartner(domain.getCode()));

    order.setUnionId(StringUtils.defaultIfBlank(unionId, null));
    order.setTuId(StringUtils.defaultIfBlank(tuId, null));

    OrderItem orderItem = new OrderItem();
    orderItem.setProductId(product.getId());
    orderItem.setProLiteralId(proLiteralId); //代销商品，将代销商品id计入此字段
    orderItem.setProductName(product.getName());
    orderItem.setProductImg(product.getImg());
    orderItem.setPrice(sku.getPrice());
    orderItem.setMarketPrice(sku.getMarketPrice());
    orderItem.setSkuId(sku.getId());
    orderItem.setSkuStr(sku.getSpec());
    orderItem.setAmount(qty);

    List<OrderItem> orderItems = new ArrayList<OrderItem>();
    orderItems.add(orderItem);

    Map<String , Integer> map = new HashMap<String , Integer>();
    map.put(orderItem.getSkuId(), orderItem.getAmount());

    // 订单计价
    PricingResultVO prices = pricingService.calculate(map, oa.getZoneId(),
        "");
    order.setGoodsFee(prices.getGoodsFee());
    order.setLogisticsFee(prices.getLogisticsFee());
    order.setDiscountFee(prices.getDiscountFee());
    order.setTotalFee(prices.getTotalFee());

    if (prices.getUsingPromotion() != null) {
      order.setPromotionId(prices.getUsingPromotion().getId());
    }

    // 保存订单到数据库
    return save(order, oa, orderItems, OrderStatus.SUBMITTED);
  }

  @Override
  @Transactional
  public Order cancel(String orderId) {
    execute(orderId, OrderActionType.CANCEL, null);
    return load(orderId);
  }

  /**
   * 根据orderId，取消整个订单
   */
  @Override
  @Transactional
  public void cancelMainOrder(@NotBlank String orderId) {
    MainOrderVO mainOrderVO = mainOrderService.loadVO(orderId);
    List<OrderVO> orders = mainOrderVO.getOrders();
    for (OrderVO order : orders) {
      execute(order.getId(), OrderActionType.CANCEL, null);
    }
    //更新主单状态
    mainOrderService.updateStatusByOrderNo(mainOrderVO.getOrderNo(), MainOrderStatus.CANCELLED);
  }

  @Override
  @Transactional
  public Order sysCancel(String orderId) {
    executeBySystem(orderId, OrderActionType.CANCEL, null);
    return load(orderId);
  }

  /**
   * for循环没有事务，否则单个事务太大
   */
  @Override
  public int autoCancel() {
    int retVal = 0;
    // 普通订单30分钟取消
    List<Order> orders = orderMapper.selectAutoCancel(30);
    cancelOrder(orders, retVal);
    return retVal;
  }

  /**
   * 循环取消拼团订单
   */
  @Override
  public int autoCancelPiece() {
    int retVal = 0;
    // 拼团订单3分钟取消
    List<Order> orders = orderMapper.selectPieceAutoCancel(3);
    for (Order order : orders) {
      executeBySystem(order.getId(), OrderActionType.CANCEL, null);
      retVal++;
    }
    return retVal;
  }

  @Override
  public int autoRefund(String orderId, BigDecimal refundFee) {
    Map<String , Object> params = new HashMap<String , Object>();
    params.put(OrderWorkflow.REFUND_FEE_PROPERTY, refundFee);
    executeBySystem(orderId, OrderActionType.REFUND, params);
    return 1;
  }

  @Override
  public void pay(String orderNo, PaymentMode mode, String payNo) {
    OrderVO order = loadByOrderNo(orderNo);
    Map<String , Object> params = new HashMap<String , Object>();
    params.put("payNo", payNo);

    // 更新支付单号
    Order updateOrder = new Order();
    updateOrder.setId(order.getId());
    updateOrder.setPayNo(payNo);
    updateOrder.setPayType(mode);
    update(updateOrder);

    // if(order.getTotalFee().add(order.getDiscountFee()).compareTo(BigDecimal.ZERO)
    // != 1){
    // log.error("订单总额不能为0  orderNo=[" + orderNo + "]");
    // throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "订单总额不能为0");
    // }
    executeBySystem(order.getId(), OrderActionType.PAY, null);
  }

  @Override
  public void payForPg(String orderNo, PaymentMode mode, String payNo) {
    OrderVO order = loadByOrderNo(orderNo);
    Map<String , Object> params = new HashMap<String , Object>();
    params.put("payNo", payNo);

    // 更新支付单号
    Order updateOrder = new Order();
    updateOrder.setId(order.getId());
    updateOrder.setPayNo(payNo);
    update(updateOrder);

    // if(order.getTotalFee().add(order.getDiscountFee()).compareTo(BigDecimal.ZERO)
    // != 1){
    // log.error("订单总额不能为0  orderNo=[" + orderNo + "]");
    // throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "订单总额不能为0");
    // }
    executeBySystem(order.getId(), OrderActionType.PAY_NO_STOCK, null);
  }

  /**
   * b2b进货订单审核操作
   */
  @Override
  public void audit(String orderId) {
    // b2b进货订单审核，直接调用pay逻辑，订单变成付款待发货状态
    executeBySystem(orderId, OrderActionType.PAY, null);
  }


  @Override
  public void refund(String orderId) {
    execute(orderId, OrderActionType.REFUND, null);
  }

  @Override
  public void refund(String orderId, BigDecimal returnFree) {
    Map<String , Object> params = new HashMap<String , Object>();
    params.put(OrderWorkflow.REFUND_FEE_PROPERTY, returnFree);
    execute(orderId, OrderActionType.REFUND, params);
  }

  /**
   * 系统返回买家钱款
   */
  @Override
  public void refundSystem(String orderId) {
    executeBySystem(orderId, OrderActionType.REFUND, null);
  }

  /**
   * 系统返回买家钱款
   */
  @Override
  public void refundSystem(String orderId, BigDecimal refundment) {
    Map<String , Object> params = new HashMap<String , Object>();
    params.put(OrderWorkflow.REFUND_FEE_PROPERTY, refundment);
    executeBySystem(orderId, OrderActionType.REFUND, params);
  }

  @Override
  public void refund(String orderId, BigDecimal goodsFee,
                     BigDecimal logisticsFee) {
    Map<String , Object> params = new HashMap<String , Object>();
    params.put(OrderWorkflow.REFUND_GOODS_FEE_PROPERTY, goodsFee);
    params.put(OrderWorkflow.REFUND_LOGISTICS_FEE_PROPERTY, logisticsFee);
    execute(orderId, OrderActionType.REFUND, params);
  }

  /**
   * 换货请求业务
   */
  @Override
  public void requestChange(String orderId, List<OrderItem> orderItemList) {
    Map<String , Object> params = new HashMap<String , Object>();
    params.put(OrderWorkflow.CHANGE_GOODS_PROPERTY, orderItemList);
    execute(orderId, OrderActionType.REQUEST_CHANGE, params);
  }

  /**
   * 补货请求
   */
  @Override
  public void requestReissue(String orderId, List<OrderItem> orderItemList) {
    Map<String , Object> params = new HashMap<String , Object>();
    params.put(OrderWorkflow.REISSUE_GOODS_PROPERTY, orderItemList);
    execute(orderId, OrderActionType.REQUEST_REISSUE, params);
  }

  @Override
  public int autoSign() {
    int retVal = 0;
    List<Order> orders = orderMapper.selectAutoSign();
    for (Order order : orders) {
      try {
        executeBySystem(order.getId(), OrderActionType.SIGN, null);
        retVal++;
      } catch (Exception e) {
        log.error("autoSign failed orderId=[" + order.getId() + "]"
            + "  message=" + e.getMessage());
        continue;
      }
    }
    return retVal;
  }

  @Override
  public int autoShipAndSignForTest() {
    int retVal = 0;
    List<Order> orders = orderMapper.selectAutoPay();
    for (Order order : orders) {
      try {
        Map<String , Object> params = new HashMap<>();
        params.put("LOGISTICS_COMPANY_PROPERTY", "test");
        params.put("LOGISTICS_ORDERNO_PROPERTY", order.getOrderNo());
        executeBySystem(order.getId(), OrderActionType.SHIP, params);
        executeBySystem(order.getId(), OrderActionType.SIGN, null);
        retVal++;
      } catch (Exception e) {
        log.error("autoShipAndSignForTest failed orderId=[" + order.getId() + "]"
            + "  message=" + e.getMessage());
        continue;
      }
    }

    return retVal;
  }

  @Override
  public int update(Order order) {
    // 修改订单之前验证价格
    checkMoney(order.getTotalFee());
    return orderMapper.updateByPrimaryKeySelective(order);
  }

  @Override
  public int updateFrom(OrderStatus from, Order order) {
    checkMoney(order.getTotalFee());
    return orderMapper.updateByPrimaryKeyAndStatus(order, from);
  }

  @Override
  public int updateFromByMainOrderId(String mainOrderId, PaymentMode payType, OrderStatus to, OrderStatus... from) {
    return orderMapper.updateStatusByMainOrderId(mainOrderId, payType, to, from);
  }

  @Override
  public int delete(String id) {
    String [] ids = {id};
    return orderMapper.delete(ids);
  }

  @Override
  public Boolean deleteByBuyer(String orderId) {
    return orderMapper.deleteByBuyer(orderId) > 0 ? true : false;
  }

  @Override
  public void requestRefund(String orderId) {
    execute(orderId, OrderActionType.REQUEST_REFUND, null);
  }

  @Override
  public void cancelRefund(String orderId, OrderStatus orderStatus) {
    Map<String , Object> params = new HashMap<String , Object>();
    params.put("orderStatus", orderStatus);
    execute(orderId, OrderActionType.CANCEL_REFUND, params);
  }

  @Override
  public void cancelRefundSystem(String orderId, OrderStatus orderStatus) {
    Map<String , Object> params = new HashMap<String , Object>();
    params.put("orderStatus", orderStatus);
    Order order = load(orderId);
    if (order.getStatus() == OrderStatus.CHANGING) {
      executeBySystem(orderId, OrderActionType.CANCEL_CHANGE, params);
    } else if (order.getStatus() == OrderStatus.REFUNDING) {
      executeBySystem(orderId, OrderActionType.CANCEL_REFUND, params);
    } else if (order.getStatus() == OrderStatus.REISSUING) {
      executeBySystem(orderId, OrderActionType.CANCEL_REISSUE, params);
    }

  }

  @Override
  public void acceptRefund(String orderId) {
    execute(orderId, OrderActionType.ACCEPT_REFUND, null);
  }

  @Override
  public void rejectRefund(String orderId) {
    execute(orderId, OrderActionType.REJECT_REFUND, null);
  }

  private Order save(Order order, OrderAddress orderAddress,
                     List<OrderItem> orderItems, OrderStatus status) {
    // 保存订单头信�?
    insert(order, status);

    // 保存订单地址信息
    orderAddress.setOrderId(order.getId());

    Address address = new Address();
    BeanUtils.copyProperties(orderAddress, address);
    address.setCommon(false);
    address.setUserId(getCurrentUser().getId());
    if (address.getWeixinId() == null) {
      address.setWeixinId("");
    }

    orderAddressService.insert(orderAddress);
    addressService.saveUserAddress(address, true);

    // 保存订单项数�?
    for (OrderItem orderItem : orderItems) {
      orderItem.setOrderId(order.getId());
      orderItemMapper.insert(orderItem);
    }

    if (isXQUser(userService.load(order.getBuyerId()))) { // 想去买家订单remark也直接记录到订单消息中
      if (StringUtils.isNotBlank(order.getRemark())) {
        OrderMessage message = new OrderMessage();
        message.setBuyerId(order.getBuyerId());
        message.setSellerId(order.getSellerId());
        message.setContent(order.getRemark());
        message.setGroupId("0");
        message.setOrderId(order.getId());
        saveMessage(message);
      }
    }
    // TODO 该部分的逻辑统一转移到workflow中，保持订单的工作流的独立�?
    applicationContext.publishEvent(new OrderActionEvent(
        OrderActionType.SUBMIT, order));
    return order;
  }

  @Override
  public void updateTotalPrice(String orderId, BigDecimal totalPrice) {
    updateTotalPrice(orderId, totalPrice, getCurrentUser().getId());
  }

  @Override
  public void updateTotalPrice(String orderId, BigDecimal totalPrice,
                               String userId) {
    Order order = orderMapper.selectBySeller(orderId, userId);
    if (order == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "订单不存在");
    }

    if (!SUBMITTED.equals(order.getStatus())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "只有[待付款]的订单才可以进行价格编辑操作");
    }

    checkMoney(totalPrice);
    orderMapper.updateTotalPrice(orderId, totalPrice, userId);
  }

  @Override
  public void updatePrice(String orderId, BigDecimal goodsFee,
                          BigDecimal logisticsFee, String userId) {
    Order order = orderMapper.selectBySeller(orderId, userId);
    if (order == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "订单不存在");
    }

    if (!SUBMITTED.equals(order.getStatus())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "只有[待付款]的订单才可以进行价格编辑操作");
    }

    if (!order.getSellerId().equals(userId)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "您没有权限进行价格编辑操作");
    }

    if (goodsFee == null) { // 不修改
      goodsFee = order.getTotalFee().subtract(order.getLogisticsFee());
    }

    if (logisticsFee == null) { // 不修改
      logisticsFee = order.getLogisticsFee();
    }

    // 总金额
    BigDecimal totalFee = goodsFee.add(logisticsFee);
    checkMoney(totalFee);
    orderMapper.updatePrice(orderId, totalFee, logisticsFee, userId);

    log.info("update price orderId=[" + orderId + "] newGoodsFee=["
        + goodsFee + "] newLogisticsFee=[" + logisticsFee + "] "
        + "oldTotalFee=[" + order.getTotalFee() + "] oldLogisticsFee=["
        + order.getLogisticsFee() + "]");
  }

  /**
   * 修改订单价格（不需要传入uerid，系统管理员修改金额）
   */
  @Override
  public void updatePriceSystem(String orderId, BigDecimal goodsFee) {
    Order order = orderMapper.selectByPrimaryKey(orderId);
    if (order == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "订单不存在");
    }

    BigDecimal logisticsFee = order.getLogisticsFee();
    // 总金额
    BigDecimal totalFee = goodsFee.add(logisticsFee);
    orderMapper.updatePriceSystem(orderId, totalFee, goodsFee);

  }

  @Override
  public void updatePrice(String orderId, BigDecimal goodsFee,
                          BigDecimal logisticsFee) {
    updatePrice(orderId, goodsFee, logisticsFee, getCurrentUser().getId());
  }

  // ============ 以上是整理过的代�?==============

  @Override
  public OrderVO loadVO(String orderId) {
    Order order = load(orderId);
    // User user = getCurrentUser();
    // if (!user.getId().equals(order.getBuyerId())
    // )
    // return null;

    OrderVO vo = new OrderVO();
    BeanUtils.copyProperties(order, vo);

    // 查找订单的退款退货信息
    List<OrderRefund> orderRefunds = orderRefundMapper.listByOrderId(orderId);
    if (orderRefunds != null) {
      for (OrderRefund orderRefund : orderRefunds) {
        if (orderRefund.getStatus() == OrderRefundStatus.SUBMITTED
            || orderRefund.getStatus() == OrderRefundStatus.SUCCESS) {
          vo.setRefundAt(orderRefund.getCreatedAt());
          vo.setRefundFee(orderRefund.getRefundFee());
          vo.setRefundLogisticsCompany(orderRefund.getLogisticsCompany());
          vo.setRefundLogisticsNo(orderRefund.getLogisticsNo());
          vo.setRefundMemo(orderRefund.getRefundMemo());
          vo.setRefundReason(orderRefund.getRefundReason());
          vo.setBuyerRequire("" + orderRefund.getBuyerRequire());
          vo.setRefundImgList(orderRefund.getRefundImgList());
        }
      }
    }

    List<OrderItem> orderItems = orderItemMapper.selectByOrderId(orderId);
    if (null != orderItems && orderItems.size() > 0) {
      ProductVO pv = productService
          .load(orderItems.get(0).getProductId());
      if (null != pv) {
        vo.setIsCrossBorder(pv.getIsCrossBorder());
      }
    }

    // 放入商品vo信息
    for (OrderItem item : orderItems) {
      String productId = item.getProductId();
      Product product = productService.load(productId);
      item.setProduct(product);
      String skuId = item.getSkuId();
      if (StringUtils.isNotBlank(skuId)) {
        Sku sku = skuMapper.selectByPrimaryKey(skuId);
        item.setSku(sku);
      }
    }

    log.info("current orderId = {},  buyerId = {}",IdTypeHandler.decode(order.getId()), order.getBuyerId());
    List<Address> addresses = addressService.listUserAddresses(order
        .getBuyerId());
    vo.setCertificateId(null != addresses && addresses.size() > 0 ? addresses
        .get(0).getCertificateId() : null);
    vo.setOrderItems(orderItems);

    OrderAddress orderAddress = orderAddressService
        .selectByOrderId(orderId);
    vo.setOrderAddress(orderAddress);

    if (orderAddress != null) {
      List<SystemRegion> zoneList = systemRegionService.listParents(orderAddress.getZoneId());

      StringBuilder addressDetails = new StringBuilder();
      for (SystemRegion zone : zoneList) {
        addressDetails.append(zone.getName());
      }
      addressDetails.append(orderAddress.getStreet());
      ((OrderAddressVO) orderAddress).setDetails(addressDetails.toString());
      vo.setAddressDetails(addressDetails.toString());
    } else {
      vo.setAddressDetails("自提");
    }

    String couponId = vo.getCouponId();
    if (StringUtils.isNotEmpty(couponId)) {
      PromotionCouponVo promotionCouponVo = promotionCouponService.load(couponId);
      vo.setPromotionCouponVo(promotionCouponVo);
    }

    User seller = userService.load(order.getSellerId());
    if (seller != null) {
      vo.setSellerPhone(seller.getPhone());
      vo.setShopId(vo.getShopId());
    }
    vo.setDefDelayDate(Integer.parseInt(delayDay));

    return vo;
  }

  @Override
  public OrderVO loadByOrderNo(String orderNo) {
    Order order = orderMapper.selectByOrderNo(orderNo);
    if (order == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "订单[" + orderNo
          + "]不存在");
    }
    OrderVO result = new OrderVO();
    BeanUtils.copyProperties(order, result);

    List<OrderItem> orderItems = orderItemMapper.selectByOrderId(order
        .getId());
    result.setOrderItems(orderItems);

    OrderAddress orderAddress = orderAddressService.selectByOrderId(order
        .getId());
    result.setOrderAddress(orderAddress);
    return result;
  }

  @Override
  public boolean ship(String orderId, String logisticsCompany,
                      String logisticsOrderNo) {
    Map<String , Object> params = new HashMap<String , Object>();
    params.put(OrderWorkflow.LOGISTICS_COMPANY_PROPERTY, logisticsCompany);
    params.put(OrderWorkflow.LOGISTICS_ORDERNO_PROPERTY, logisticsOrderNo);
    execute(orderId, OrderActionType.SHIP, params);
    return true;
  }


  @Override
  public void sign(String orderId) {
    sign(orderId, (User) getCurrentUser());
  }

  @Override
  public void sign(String orderId, User user) {
    execute(orderId, OrderActionType.SIGN, null, user);
  }

  @Override
  public void delaySign(String orderId) {
    Map<String , Object> params = new HashMap<String , Object>();
    params.put("delaySign", "delaySign");
    execute(orderId, OrderActionType.SIGN, params);

    Order order = this.load(orderId);
    if (order != null) {
      User aSellerUser = userService.load(order.getSellerId());
      if (aSellerUser != null) {
        pushMessage(PushMsgId.OrderDelaySign.getId(),
            aSellerUser.getId(),
            Long.toString(new Date().getTime()),
            PushMsgType.MSG_ORDER_DELAYSIGN_S.getValue(), orderId);
      }
    }
  }

  private int getIntervalDays(Date from, Date to) {
    if (from == null || to == null) {
      return 0;
    }
    long intervalMilli = from.getTime() - to.getTime();
    int ret = (int) (intervalMilli / (24 * 60 * 60 * 1000));
    return ret + 1;
  }

  @Override
  public void remindShip(String orderId) {
    Order order = this.load(orderId);
    if (order == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "您要提醒的订单不存在");
    }

    User seller = userService.load(order.getSellerId());
    Date now = new Date();
    int intval = getIntervalDays(now, order.getRemindShipAt());
    if (seller != null && (intval == 0 || intval > 1)) {
      pushMessage(PushMsgId.OrderRemindShip.getId(), seller.getId(),
          Long.toString(new Date().getTime()),
          PushMsgType.MSG_ORDER_REMINDSHIP_S.getValue(),
          order.getId());
    } else {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "您已提醒过卖家发货");
    }
    order.setRemindShipAt(now);
    this.update(order);
  }

  // @Override
  // public int orderPaid(String orderId) {
  // Order order = orderMapper.selectByPrimaryKey(orderId);
  // User user = this.getCurrentUser();
  //
  // if (!order.getBuyerId().equals(user.getId()) ||
  // !OrderStatus.SUBMITTED.equals(OrderStatus.SUBMITTED)) {
  // return 0;
  // }
  //
  // return orderMapper.updateOrderStatus4Buyer(orderId, OrderStatus.PAID,
  // user.getId());
  // }
  //
  // @Override
  // public int orderComplete(String orderId) {
  // Order order = orderMapper.selectByPrimaryKey(orderId);
  // User user = this.getCurrentUser();
  //
  // if (!order.getSellerId().equals(user.getId()) ||
  // !OrderStatus.SUBMITTED.equals(OrderStatus.SUBMITTED)) {
  // return 0;
  // }
  //
  // return orderMapper.updateOrderStatus4Seller(orderId, OrderStatus.SUCCESS,
  // user.getId());
  // }

  @Override
  public List<OrderVO> listByStatus4Buyer(OrderStatus status,
      Pageable pageable) {
    return listByStatus4Buyer(getCurrentUser().getId(), status, pageable);
  }

  @Override
  public List<OrderVO> listByStatus4Buyer(String userId, OrderStatus status,
                                          Pageable pageable) {
    List<OrderVO> orders = orderMapper.selectByBuyerAndStatus(userId,
        status, pageable);
    for (OrderVO order : orders) {
      // zzd 一次性从db中获取出来
      // order.setOrderItems(orderItemMapper.selectByOrderId(order.getId()));
      User seller = userService.loadByAdmin(order.getSellerId());
      order.setSellerPhone(seller.getPhone());
      Shop shop = shopService.load(seller.getShopId());
      order.setShopName(shop == null ? "" : shop.getName());
      order.setDefDelayDate(Integer.parseInt(delayDay));
    }

    return orders;
  }

  @Override
  public List<OrderVO> listByStatusAndMember(String [] orderStatusArr, String userId,
                                             String shopId, Pageable pageable, Map<String , Object> params) {

    List<OrderVO> orders = orderMapper
        .selectByMemberIdAndStatus(orderStatusArr, userId, shopId, pageable, params);

    for (OrderVO order : orders) {
      if (shopId == null) {
        User seller = userService.loadByAdmin(order.getSellerId());
        order.setSellerPhone(seller.getPhone());
        Shop shop = shopService.load(seller.getShopId());
        order.setShopName(shop == null ? "" : shop.getName());
      }
      order.setDefDelayDate(Integer.parseInt(delayDay));
      // 判断订单item是否已经评价过
      List<OrderItem> orderItems = order.getOrderItems();
      for (OrderItem itemVO : orderItems) {
        List<OrderItemComment> comments = orderItemCommentService
            .selectByOrderItemId(itemVO.getId());
        itemVO.setIsComment(comments != null && comments.size() > 0);
      }
    }

    return orders;
  }

  @Override
  public long countByStatusAndMember(String [] orderStatusArr, String userId, String shopId,
                                     Map<String , Object> params) {
    long count = orderMapper.countByMemberIdAndStatus(orderStatusArr, userId, shopId, params);
    return count;
  }

  @Override
  public List<OrderVO> listSellerByStatusAndMember(String [] orderStatusArr, String sellerShopId,
                                                   String shopId, Pageable pageable, Map<String , Object> params) {

    List<OrderVO> orders = orderMapper
        .selectSellerByMemberIdAndStatus(orderStatusArr, sellerShopId, shopId, pageable,
            params);

    for (OrderVO order : orders) {
      if (shopId == null) {
        User seller = userService.loadByAdmin(order.getSellerId());
        order.setSellerPhone(seller.getPhone());
        Shop shop = shopService.load(seller.getShopId());
        order.setShopName(shop == null ? "" : shop.getName());
      }
      order.setDefDelayDate(Integer.parseInt(delayDay));
    }

    return orders;
  }


  @Override
  public List<OrderVO> listByStatus4Seller(OrderStatus status, Pageable page) {
    return this.listByStatus4Seller(() -> orderMapper.selectBySellerAndStatus(
        getCurrentUser().getId(), status, page));
  }

  @Override
  public List<OrderVO> listByStatus4Seller2(OrderStatus status, Pageable page) {
    return this.listByStatus4Seller(
        () -> orderMapper.selectBySellerAndStatus2(getCurrentUser().getId(), status, page));
  }

  @Override
  public int checkOrderNo(String orderNo) {
    if(StringUtils.isEmpty(orderNo)){
      throw new BizException(GlobalErrorCode.NOT_FOUND,"订单号不存在");
    }
    return orderMapper.checkOrderNo(orderNo);
  }

  @Override
  public List<OrderVO> listByStatus4Seller(Supplier<List<OrderVO>> supplier) {
    List<OrderVO> orders = supplier.get();
    for (OrderVO order : orders) {
      order.setOrderItems(orderItemMapper.selectByOrderId(order.getId()));
      order.setDefDelayDate(Integer.parseInt(delayDay));
    }
    return orders;
  }

  @Override
  public List<OrderVO> listByStatus(OrderStatus status, Pageable page, String userId,
                                    String dateStr) {
    // 如果传递了userId，则是查该userId对应的订单，否则则是查询当前用户的订单
    if (StringUtils.isEmpty(userId)) {
      userId = getCurrentUser().getId();
    }
    List<OrderVO> orders = orderMapper.selectBySellerAndStatusAll(userId, status, page, "");
    setOrderDetail(orders, userId);
    return orders;
  }

  /**
   * 2019-04-19 增加PENDING处理
   */
  @Override
  public List<OrderVO> listByStatus2(OrderStatus status, Pageable page, String userId,
                                    String dateStr) {
// 如果传递了userId，则是查该userId对应的订单，否则则是查询当前用户的订单
    if (StringUtils.isEmpty(userId)) {
      userId = getCurrentUser().getId();
    }
    List<OrderVO> orders = orderMapper.selectBySellerAndStatusAll2(userId, status, page, "");
    setOrderDetail(orders, userId);
    return orders;
  }

  @Override
  public List<OrderVO> listUnCommentOrder(Pageable pageable, String userId) {
    if (StringUtils.isEmpty(userId)) {
      userId = getCurrentUser().getId();
    }
    List<OrderVO> orders = orderMapper.selectUnCommentOrders(userId,
        SUCCESS, pageable);
    setOrderDetail(orders, userId);
    return orders;
  }

  @Override
  public Boolean isOrderComment(String orderId, String userId) {
    return orderMapper.selectIsOrderCommented(orderId, userId);
  }

  private void setOrderDetail(List<OrderVO> orders, String userId) {
    for (OrderVO order : orders) {
      if (userId.equals(order.getBuyerId())) {
        order.setFromType("in");
      } else {
        order.setFromType("out");
      }
      List<OrderItemVO> itemVOs = orderItemMapper
          .selectVOByOrderIdAndUserId(order.getId(), userId);
      //进货类型订单省赚逻辑处理
      if (order.getOrderType() == OrderSortType.PURCHASE) {
        for (OrderItemVO itemVO : itemVOs) {
          BigDecimal marketPrice = itemVO.getMarketPrice();
          BigDecimal price = itemVO.getPrice();
          int amount = itemVO.getAmount();
          // 买入的省用商品原价减去商品买入的价格
          if ("in".equals(order.getFromType())) {
            itemVO.setCommission(
                marketPrice.subtract(price).multiply(new BigDecimal(amount)));
          }
          // 卖出的赚用商品买入的价格减去此用户设置的经销商价格
          else {
            //判断当前用户是否在代理角色中
            UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
            if (userAgentVO != null) {
              String role = userAgentVO.getRole();
              RolePriceVO rolePriceVO = rolePriceService
                  .selectByProductAndRole(itemVO.getProductId(), role);
              if (rolePriceVO != null) {
                BigDecimal rolePrice = rolePriceVO.getPrice();
                itemVO.setCommission(
                    price.subtract(rolePrice).multiply(new BigDecimal(amount)));
              }
            }

          }
        }
      } else if(order.getOrderType() == OrderSortType.RESERVE
              && StringUtils.isNotBlank(order.getOrderNo())
              && order.getOrderNo().contains(UniqueNoType.PRO.name())){
        // 预约单的订单编号和订单号相同
        order.setOrderStatus(order.getStatus());
        try{
          convertOrderItem(order);
        }catch (Exception e){
          log.error("预约单转化失败",e);
        }
      }else{
        order.setOrderItemVOs(itemVOs);
        order.setOrderItems(orderItemMapper.selectByOrderId(order.getId()));
        order.setDefDelayDate(Integer.parseInt(delayDay));
      }
    }

  }

  /**
   * 查询预约的子订单
   * @param vo
   */
  private void convertOrderItem(OrderVO vo) {
    List<PromotionReserveOrderItemVO> reserveOrderItems =
            promotionReserveMapper.selectReserveOrderItems(vo.getId());
    List<OrderItem> orderItems =
            reserveOrderItems.stream()
                    .map(
                            item -> {
                              OrderItem orderItem = new OrderItem();
                              orderItem.setOrderId(item.getOrderId());
                              orderItem.setPromotionType(PromotionType.RESERVE);
                              orderItem.setAmount(item.getAmount());
                              orderItem.setPrice(item.getPrice());
                              orderItem.setDiscountPrice(item.getPromotionPrice());
                              orderItem.setProductId(item.getProductId());
                              orderItem.setSkuId(item.getSkuId());
                              orderItem.setSkuStr(item.getSkuStr());
                              orderItem.setProductName(item.getProductName());
                              orderItem.setCanPay(item.getCanPay());
                              orderItem.setPromotionValidFrom(item.getValidFrom());
                              orderItem.setPromotionValidTo(item.getValidTo());
                              orderItem.setPromotionPayFrom(item.getPayFrom());
                              orderItem.setPromotionPayTo(item.getPayTo());
                              orderItem.setProductImg(item.getProductImg());
                              return orderItem;
                            })
                    .collect(Collectors.toList());
    vo.setOrderItems(orderItems);
    vo.setPromotionId(reserveOrderItems.get(0).getPromotionId());
  }

  @Override
  public List<OrderVO> listByStatus4Seller(OrderStatus[] status, Pageable page) {
    List<OrderVO> orders = orderMapper.selectBySellerAndStatusArr(
        getCurrentUser().getId(), status, page);
    for (OrderVO order : orders) {
      order.setOrderItems(orderItemMapper.selectByOrderId(order.getId()));
      order.setDefDelayDate(Integer.parseInt(delayDay));
    }
    return orders;
  }

  @Override
  public Long countSellerOrdersByStatusWithLike(OrderStatus status, String key) {
    if (StringUtils.isNotBlank(key)) {
      key = "%" + key + "%";
    }

    IUser user = this.getCurrentUser();
    if (user == null) {
      return 0L;
    }

    List<OrderStatus> oos = new ArrayList<OrderStatus>();
    if (status != null) {
      if (status.equals(CLOSED)) {
        oos.add(CANCELLED);
        oos.add(SUCCESS);
        oos.add(CLOSED);
      } else {
        oos.add(status);
      }
    }

    return orderMapper.countBySellerAndStatus(user.getId(), key,
        oos.toArray(new OrderStatus[oos.size()]));
  }

  @Override
  public Long countSellerOrdersByStatus(OrderStatus status) {
    return countSellerOrdersByStatusWithLike(status, null);
  }

  @Override
  public List<Customer> listCustomers(Pageable pageable) {
    return listCustomersByKey(null, pageable);
  }

  @Override
  public List<Customer> listCustomersByKey(String key, Pageable pageable) {
    Shop shop = shopService.findByUser(getCurrentUser().getId());
    if (shop == null) {
      return new ArrayList<Customer>();
    }

    List<String > allowedSortFields = Arrays.asList("last_paid_at",
        "order_count", "sum_consumption");
    List<String > sortList = new ArrayList<String >();
    String sortStr = null;
    if (pageable.getSort() != null) {
      Iterator<Sort.Order> iter = pageable
          .getSort().iterator();
      while (iter.hasNext()) {
        Sort.Order o = iter.next();
        if (allowedSortFields.contains(o.getProperty())) {
          sortList.add(o.getProperty() + " " + o.getDirection());
        }
      }
      if (sortList.size() > 0) {
        sortStr = StringUtils.join(sortList.toArray(), ",");
      }
    }

    if (StringUtils.isNotBlank(key)) {
      key = "%" + key + "%";
    }

    List<Customer> customers = orderMapper.selectCustomersByShopIdAndKey(
        shop.getId(), key, sortStr, pageable);
    return customers;
  }

  @Override
  public CustomerVO listOrdersByCustomer(String customerId) {
    Shop shop = shopService.findByUser(getCurrentUser().getId());
    if (shop == null) {
      return null;
    }
    CustomerVO customer = null;
    List<OrderVO> orders = orderMapper.selectByShopIdAndCustomerId(
        shop.getId(), customerId);
    if (orders.size() > 0) {
      customer = new CustomerVO();
      Customer buyer = orderMapper.selectCustomerByShopIdAndBuyerId(
          shop.getId(), customerId);
      BeanUtils.copyProperties(buyer, customer);
      customer.setOrders(orders);

      for (OrderVO order : orders) {
        List<OrderItem> orderItems = orderItemMapper
            .selectByOrderId(order.getId());
        order.setOrderItems(orderItems);
      }
    }
    return customer;
  }

  @Override
  public CustomerVO listOrdersByCustomer(String name, String phone) {
    Shop shop = shopService.findByUser(getCurrentUser().getId());
    if (shop == null) {
      return null;
    }
    CustomerVO customer = null;
    List<Order> orders = orderMapper.selectByShopIdAndConsignee(
        shop.getId(), name, phone);
    BigDecimal totalFee = new BigDecimal(0.00);
    if (orders.size() > 0) {
      customer = new CustomerVO();
      Customer buyer = orderMapper.selectCustomerByShopIdAndConsignee(
          shop.getId(), name, phone);
      BeanUtils.copyProperties(buyer, customer);
      List<OrderVO> orderList = new ArrayList<OrderVO>();
      for (Order order : orders) {
        OrderVO vo = new OrderVO();
        BeanUtils.copyProperties(order, vo);

        List<OrderItem> items = orderItemMapper.selectByOrderId(order
            .getId());
        vo.setImgUrl("");
        vo.setOrderItems(items);
        vo.setOrderAddress(orderAddressService.selectByOrderId(order
            .getId()));
        orderList.add(vo);
        totalFee = totalFee.add(order.getTotalFee());
      }
      customer.setOrderCount(orders.size());
      customer.setSumConsumption(totalFee);
      customer.setOrders(orderList);
      customer.setDeliveryAddr(getFullAddr(buyer.getZoneId(),
          buyer.getStreet()));
    }
    return customer;
  }

  private String getFullAddr(String zoneId, String street) {
    String provString = "";
    String cityString = "";
    Zone cityZone = zoneService.findParent(zoneId);
    if (cityZone != null) {
      cityString = cityZone.getName();
      Zone provZone = zoneService.findParent(cityZone.getId());
      if (provZone != null) {
        provString = provZone.getName();
      }
    }

    return provString + cityString + street;
  }

  @Override
  public OrderActionType[] listUserNextActions(String orderId) {
    return null;
  }

  private OrderWorkflow getOrderWorkflow(OrderType orderType) {
    OrderType type = null;
    if (orderType.equals(OrderType.PIECE)) {
      type = OrderType.DANBAO;
    } else {
      type = orderType;
    }
    return workflows.get(type);
  }

  @Override
  public List<OrderItem> listOrderItems(String orderId) {
    List<OrderItem> result = orderItemMapper.selectByOrderId(orderId);
    if (result == null) {
      return Collections.emptyList();
    }
    return result;
  }

  public Map<OrderType, OrderWorkflow> getWorkflows() {
    return workflows;
  }

  public void setWorkflows(Map<OrderType, OrderWorkflow> workflows) {
    this.workflows = workflows;
  }

  /**
   * 插入订单数据到数据库
   */
  @Override
  public int insert(Order e) {
    e.setOrderNo(UniqueNoUtils.next(UniqueNoType.ESO));
    e.setStatus(SUBMITTED);
    // 订单生成之前，验证订单的金额是否合理--订单检查总和即可，这样商品，物流都不可能超过了
    checkMoney(e.getTotalFee());
    return orderMapper.insert(e);
  }

  @Override
  public int insert(Order e, OrderStatus status) {
    if (status == null) {
      status = SUBMITTED;
    }
    e.setOrderNo(UniqueNoUtils.next(UniqueNoType.ESO));
    e.setStatus(status);
    // 订单生成之前，验证订单的金额是否合理--订单检查总和即可，这样商品，物流都不可能超过了
    checkMoney(e.getTotalFee());
    return orderMapper.insert(e);
  }
  @Override
  public int insert2(Order e, OrderStatus status) {
    if (status == null) {
      status = SUBMITTED;
    }
    e.setOrderNo(UniqueNoUtils.next(UniqueNoType.ESO));
    e.setStatus(status);
    // 订单生成之前，验证订单的金额是否合理--订单检查总和即可，这样商品，物流都不可能超过了
    checkMoney(e.getTotalFee());
    return orderMapper.insert2(e);
  }
  /**
   * 换货插入订单到数据库
   */
  @Override
  public int insertOrder(Order order) {
    order.setOrderNo(UniqueNoUtils.next(UniqueNoType.ESO));
    order.setStatus(PAID);
    // 订单生成之前，验证订单的金额是否合理--订单检查总和即可，这样商品，物流都不可能超过了
    checkMoney(order.getTotalFee());
    return orderMapper.insertOrder(order);
  }

  private void checkMoney(BigDecimal price) {
    if (price == null) {
      return;
    }

    if (String .valueOf(price).length() > 9) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成订单的价格"
          + price + "不合理，超出数据库");
    }
  }

  @Override
  public Order load(String orderId) {
    return orderMapper.selectByPrimaryKey(orderId);
  }

  @Override
  public List<Order> adminSearchOrders(AdminSearchConditions searchConditions) {
    // TODO Auto-generated method stub
    return null;
  }

  // ============= private 方法==================
  private void execute(String orderId, OrderActionType action,
                       Map<String , Object> params, User user) {
    Order order = load(orderId);
    // 当前订单处于哪个工作流
    OrderWorkflow workflow = getOrderWorkflow(order.getType());

    // 工作流执行本次状态修改
    // user可以为空，比如付款回调、系统关闭订单
    workflow.execute(order, user, action, params);
  }

  /**
   * 用户推进执行
   */
  private void execute(String orderId, OrderActionType action,
                       Map<String , Object> params) {
    execute(orderId, action, params, (User) getCurrentUser());
  }

  /**
   * 系统task执行或者来自第三方服务器回调
   */
  @Override
  public void executeBySystem(String orderId, OrderActionType action,
                              Map<String , Object> params) {
    Order order = load(orderId);
    OrderWorkflow workflow = this.getOrderWorkflow(order.getType());
    workflow.executeBySystem(order, action, params);
  }

  @Override
  public Long countByAdmin(Map<String , Object> params) {
    // String partner = getCurrentUser().getPartner();
    // if(!partner.equals(rootPartner))
    // params.put("partner", getCurrentUser().getPartner());
    return orderMapper.countByAdmin(params);
  }

  @Override
  public Map<String , Object> countMapByAdmin(Map<String , Object> params) {
    // String partner = getCurrentUser().getPartner();
    // if(!partner.equals(rootPartner))
    // params.put("partner", getCurrentUser().getPartner());
    return orderMapper.countMapByAdmin(params);
  }

  @Override
  public List<OrderVO> listByAdmin(Map<String , Object> params,
      Pageable pageable) {
    // String partner = getCurrentUser().getPartner();
    // if(!partner.equals(rootPartner))
    // params.put("partner", getCurrentUser().getPartner());
    List<OrderVO> listOrder = orderMapper.selectByAdmin(params, pageable);
    for (OrderVO ordervo : listOrder) {
      if (StringUtils.isNotBlank(ordervo.getMainOrderId())) {
        if (null != mainOrderService.load(ordervo.getMainOrderId())) {
          if (StringUtils.isNotBlank(
              mainOrderService.load(ordervo.getMainOrderId()).getOrderNo())) {
            ordervo.setMainOrderNo(
                mainOrderService.load(ordervo.getMainOrderId()).getOrderNo());
          }
        }
      }
    }
    return listOrder;
  }

  @Override
  public Long countByMerchant(Map<String , Object> params) {
    List<OutPay> outPays = new ArrayList<>();
    if (StringUtils.isNotBlank(String.valueOf(params.get("tradeNo"))))
      outPays = outpayMapper.selectByTradeNo(String.valueOf(params.get("tradeNo")));
    if (CollectionUtils.isNotEmpty(outPays))
      params.put("orderNo", "%" + outPays.get(0).getBillNo() + "%");
      return orderMapper.countByAdmin(params);
  }

  @Override
  public List<OrderVO> listByMerchant(Map<String , Object> params, Pageable pageable) {
    List<OutPay> outPays = new ArrayList<>();
  if (StringUtils.isNotBlank((String) params.get("tradeNo"))) {
      outPays = outpayMapper.selectByTradeNo(String.valueOf(params.get("tradeNo")));
      if (CollectionUtils.isNotEmpty(outPays))
          params.put("orderNo", "%" + outPays.get(0).getBillNo() + "%");
  }

    List<OrderVO> listOrder = orderMapper.selectByAdmin(params, pageable);
/*
    if (CollectionUtils.isNotEmpty(listOrder)) {
      listOrder.parallelStream()
              .forEach(o -> {
                o.setOrderItems(ord erItemMapper.selectByOrderId(o.getId()));
                o.setDefDelayDate(Integer.parseInt(delayDay));
              });
    }
*/
    for (OrderVO order : listOrder) {
      order.setOrderItems(orderItemMapper.selectByOrderId(order.getId()));
      order.setDefDelayDate(Integer.parseInt(delayDay));
      if (CollectionUtils.isNotEmpty(outPays))
        order.setOutTradeNo(outPays.get(0).getTradeNo());
      else {
          List<OutPay> billNos = outpayMapper.selectByBillNO(order.getPayNo());
          if (CollectionUtils.isNotEmpty(billNos))
              order.setOutTradeNo(billNos.get(0).getTradeNo());
      }
    }
    return listOrder;
  }

  @Override
  public List<OrderVO> listByAdmin4Export(Map<String , Object> params,
      Pageable pageable) {
    List<OrderVO> list = orderMapper.selectByAdmin4Export(params, pageable);
    return list;
  }

  @Override
  public int updateOrderByRefund1(String orderId) {
    Order dbOrder = load(orderId);
    if (dbOrder == null) {
      log.warn("您要修改的订单" + orderId + "不存在");
      return 0;
    }

    String payNo = payRequestApiService.generatePayNo();
    SubAccount fromAccount = accountApiService.findSubAccountByUserId(
        dbOrder.getSellerId(), AccountType.DANBAO);
    SubAccount toAccount = accountApiService.findSubAccountByUserId(
        dbOrder.getBuyerId(), AccountType.CONSUME);
    PayRequest request = new PayRequest(payNo, dbOrder.getOrderNo(),
        PayRequestBizType.REFUND, PayRequestPayType.REFUND,
        dbOrder.getPaidFee(), fromAccount.getId(), toAccount.getId(),
        null);

    int flag3 = 0;
    int flag1 = payRequestApiService.payRequest(request) ? 1 : 0;
    if (flag1 == 1) {
      payNo = payRequestApiService.generatePayNo();
      fromAccount = accountApiService.findSubAccountByUserId(
          dbOrder.getBuyerId(), AccountType.CONSUME);
      toAccount = accountApiService.findSubAccountByUserId(
          dbOrder.getBuyerId(), AccountType.AVAILABLE);
      request = new PayRequest(payNo, dbOrder.getOrderNo(),
          PayRequestBizType.REFUND,
          PayRequestPayType.CONSUME2AVAILABLE, dbOrder.getPaidFee(),
          fromAccount.getId(), toAccount.getId(), null);

      int flag2 = payRequestApiService.payRequest(request) ? 1 : 0;
      if (flag2 == 1) {
        Map<String , Object> params = new HashMap<String , Object>();
        params.put("orderId", orderId);
        params.put("status", new OrderStatus[]{PAID,
            SHIPPED});

        Order order = new Order();
        order.setId(dbOrder.getId());
        order.setStatus(CLOSED);
        flag3 = orderMapper.updateOrderByRefund(params, order);
      }
    }

    if (flag3 == 0) {
      log.error("refund error orderId=[" + orderId + "] flag1=[" + flag1
          + "]  flag2=[" + flag1 + "]");
    }

    return flag3;
  }

  @Override
  public Long selectOrderSeqByShopId(String shopId) {
    return orderMapper.selectOrderSeqByShopId(shopId);
  }

  @Override
  public int updateOrderRefundByAdmin(String orderId) {
    return orderMapper
        .updateOrderRefundByAdmin(orderId, RefundType.SUCCESS);
  }

  @Override
  public List<Customer> listVipCustomers() {
    Shop shop = shopService.findByUser(getCurrentUser().getId());
    return orderMapper.selectCustomersByVip(shop.getId(), true);
  }

  /*
   * private Order loadLatestByBuyerId(String buyerId) { String shopId =
   * this.getCurrentUser().getShopId(); if (shopId== null) { return null; }
   * else { return orderMapper.selectLatestByBuyerId(shopId, buyerId); } }
   */

  @Override
  public Boolean updateVip(String name, String phone, Boolean vip) {
    int ret = 0;
    if (name == null) {
      return false;
    }
    ret = orderMapper.updateVipByCustomer(name, phone, getCurrentUser().getShopId(),
        vip);
    return ret == 1 ? true : false;
  }

  @Override
  public List<Order> searchByBuyer(Map<String , Object> params) {
    return orderMapper.selectByBuyer(params, getCurrentUser().getId());
  }

  /*
   * @Override public Promotion loadOrderPromotion(String order) { // TODO
   * Auto-generated method stub return null; }
   */

  @Override
  public Boolean countFirstStatus4Seller(Order order, OrderStatus status) {
    Boolean ret = false;
    Long totalCnt = orderMapper.countBySellerAndStatus(order.getSellerId(),
        null, null);
    switch (status) {
      case SUBMITTED: // 第一次有买家下单, 通知卖家
        if (totalCnt > 1) {
          break;
        }
        if (1 == countOrder(order.getSellerId(), null,
            SUBMITTED)) {
          ret = true;
        }
        break;
      case PAID: // 第一次直接付�?通知卖家
        if (order.getType().equals(OrderType.DIRECT)) {
          if (countOrder(order.getSellerId(), null, SUCCESS) > 1) {
            break;
          }

          if (countOrder(order.getSellerId(), null, SHIPPED) > 1) {
            break;
          }
          if (countOrder(order.getSellerId(), null, PAID) == 1) {
            ret = true;
          }
        }
        break;
      case SUCCESS: // 第一次担保交易成功后卖家收到第一比款, 通知卖家
        if (order.getType().equals(OrderType.DANBAO)) {
          if (1 == countOrder(order.getSellerId(), null,
              SUCCESS)) {
            ret = true;
          }
        }
        break;
      default:
        break;
    }
    return ret;
  }

  private Long countOrder(String sellerId, String key, OrderStatus orderStatus) {
    return orderMapper.countBySellerAndStatus(sellerId, key, orderStatus);
  }

  @Override
  public Long getCountByStatus4Buyer(OrderStatus os) {
    return getCountByStatus4Buyer(os, getCurrentUser().getId());
  }

  @Override
  public Long getCountByStatus4Buyer(OrderStatus os, String userId) {
    return orderMapper.selectCountByStatus4Buyer(userId, os, null);
  }

  @Override
  public Long getCountByStatus4Buyer(OrderStatus os, String userId, String shopId) {
    return orderMapper.selectCountByStatus4Buyer(userId, os, shopId);
  }

  @Override
  public Long getCountByMoreStatus4Buyer(String [] orderStatusArr, String userId, String shopId) {
    return orderMapper.selectCountByMoreStatus4Buyer(userId, orderStatusArr, shopId);
  }

  @SuppressWarnings("resource")
  private void pushMessage(Long msgId, String baiduTag, String url,
                           Integer type, String relatId) {
    if (msgId == null) {
      log.warn("pushMessage msgId is null, type:" + type + ", relatId:"
          + relatId);
      return;
    }
    Message message = messageService.loadMessage(IdTypeHandler
        .encode(msgId));
    if (msgId.equals(PushMsgId.OrderDelaySign.getId())) {
      Formatter fmt = new Formatter();
      try {
        fmt.format(message.getContent(), delayDay);
      } finally {
        message.setContent(fmt.toString());
      }
    } else if (msgId.equals(PushMsgId.OrderDelaySignRemind.getId())
        || msgId.equals(PushMsgId.OrderRemindShip.getId())) {
      Formatter fmt = new Formatter();
      try {
        Order order = load(relatId);
        if (order != null) {
          fmt.format(message.getContent(), order.getOrderNo());
        } else {
          fmt.format(message.getContent(), "");
        }
      } finally {
        message.setContent(fmt.toString());
      }
    }
    MessageNotifyEvent event = new MessageNotifyEvent(message, baiduTag,
        url, type, relatId);
    applicationContext.publishEvent(event);
  }

  private void pushXQMsg(Long msgId, String baiduTag, Integer type,
                         String relatId) {
    Message message = messageService.loadMessage(IdTypeHandler
        .encode(msgId));
    String orderId = relatId;
    XQMessageNotifyEvent event = new XQMessageNotifyEvent(message,
        baiduTag, null, type, relatId);
    OrderMessage om = orderMessageMapper
        .selectLatestSellerReplyByOrderId(orderId);
    event.setExtDate(om);
    applicationContext.publishEvent(event);
  }

  @Override
  public Boolean saveMessage(OrderMessage message) {
    // 获取订单
    Order aOrder = load(message.getOrderId());
    if (aOrder == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "订单不存在");
    }

    String groupId = message.getGroupId();
    if (aOrder == null || groupId == null) {
      log.debug("请选择您要回复的消息");
      return false;
    }

    if (!groupId.equals("0")) { // 0:买家留言, 1:卖家回复
      message.setHasRead(true);
      OrderMessage msg = orderMessageMapper.selectByPrimaryKey(message
          .getGroupId());
      if (msg == null) { // 卖家没有指定回复哪一条买家留言
        log.warn("卖家回复买家的留言不存在，订单[" + aOrder.getId() + "]");
        return false;
      }
    } else {
      message.setHasRead(false);
    }

    message.setBuyerId(aOrder.getBuyerId());
    message.setSellerId(aOrder.getSellerId());

    boolean insertStat = orderMessageMapper.insert(message) == 1 ? true
        : false;

    // 发消息
    if (groupId.equals("0")) {
      // 买家留言通知卖家
      User aSellerUser = userService.load(aOrder.getSellerId());
      if (aSellerUser != null) {
        pushMessage(PushMsgId.MessageNotify.getId(),
            aSellerUser.getId(),
            Long.toString(new Date().getTime()),
            PushMsgType.MSG_LEAVEMSGS.getValue(), aOrder.getId());
      } else {
        return false;
      }
    } else {
      // 卖家回复通知买家
      User aBuyerUser = userService.load(aOrder.getBuyerId());
      if (aBuyerUser != null && aBuyerUser.getPartner() != null
          && aBuyerUser.getPartner().equalsIgnoreCase("xiangqu")
          && StringUtils.isNotEmpty(aBuyerUser.getExtUserId())) {
        pushXQMsg(PushMsgId.MessageNotify_XQ.getId(),
            aBuyerUser.getExtUserId(),
            PushMsgType.MSG_LEAVEMSGS.getValue(), aOrder.getId());
      } else {
        return false;
      }
    }

    return insertStat;
  }

  // 快店订单留言列表 (快店)
  @Override
  public List<MsgOrdersVO> listMsgOrders(Pageable pageable) {
    List<OrderMessage> aList = orderMessageMapper.selectOrdersBySeller(
        getCurrentUser().getId(), pageable);
    List<MsgOrdersVO> retList = new ArrayList<MsgOrdersVO>();
    if (aList != null) {
      for (OrderMessage om : aList) {
        MsgOrdersVO aVo = new MsgOrdersVO();
        aVo.setOrderId(om.getOrderId());
        aVo.setBuyerId(om.getBuyerId());
        aVo.setHasRead(om.getHasRead());
        aVo.setLastMsgCont(om.getContent());
        User aUser = userService.load(om.getBuyerId());
        if (aUser != null) {
          aVo.setBuyerNick(aUser.getName());
        }
        List<OrderItem> oimList = orderItemMapper.selectByOrderId(om
            .getOrderId());
        if (oimList != null && oimList.size() != 0) {
          aVo.setProductImg(oimList.get(0).getProductImg());
        }
        aVo.setLastMsgTime(om.getCreatedAt());
        retList.add(aVo);
      }
    }
    return retList;
  }

  // 查看订单留言
  @Override
  public List<OrderMessage> viewMessages(String orderId) {
    List<OrderMessage> result = orderMessageMapper.selectByOrderId(orderId);
    if (result == null) {
      return Collections.emptyList();
    }

    return result;
  }

  @Override
  public int countNoVisitOrderBySellerId(String userId, Date lastVisitTime) {
    return orderMapper.countNoVisitOrderBySellerId(userId, lastVisitTime);
  }

  @Override
  public OrderMessage selectOrderMsgById(String msgId) {
    return orderMessageMapper.selectByPrimaryKey(msgId);
  }

  @Override
  public MsgProdInfoVO selectProdInfoByOrderId(String orderId) {
    return orderMessageMapper.selectProdInfoByOrderId(orderId);
  }

  @Override
  public int setMsgRead(String orderId) {
    return orderMessageMapper.updateMsgReadByOrderId(orderId);
  }

  @Override
  public BigDecimal loadTechServiceFee(Order order) {
    return loadTechServiceFee(order.getTotalFee());
  }

  @Override
  public BigDecimal loadTechServiceFee(BigDecimal baseFee) {
    if (baseFee != null && baseFee.compareTo(serviceFeeStandard) == 1) {
      BigDecimal fee = baseFee.subtract(serviceFeeStandard);
      return fee.multiply(serviceFeeRate);
    }

    return BigDecimal.ZERO;
  }

  @Override
  public List<Order> randomFetchOrderForActivity(Date startTime) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<OrderFeeVO> findOrderFees(OrderVO order, BigDecimal cmFee) {
    // 实收金额 totalFee+discount-cmFee
    BigDecimal ss = order.getTotalFee().subtract(cmFee);
    if (ss.compareTo(BigDecimal.ZERO) < 0) {
      ss = BigDecimal.ZERO;
    }

    List<OrderFeeVO> fees = new ArrayList<OrderFeeVO>();
    fees.add(new OrderFeeVO("goods", "合计", ObjectUtils.defaultIfNull(
        order.getGoodsFee(), BigDecimal.ZERO), "a"));
    fees.add(new OrderFeeVO("logistics", "运费", ObjectUtils.defaultIfNull(
        order.getLogisticsFee(), BigDecimal.ZERO), "a"));
    fees.add(new OrderFeeVO("commission", "推广金额", cmFee, "b"));
    fees.add(new OrderFeeVO("receiving", "实收金额", ss, "b"));
    fees.add(new OrderFeeVO("payAmount", "实付金额", ObjectUtils.defaultIfNull(
        order.getPaidFee(), order.getTotalFee()), "c"));
    return fees;
  }

  @Override
  public List<OrderFeeVO> findOrderFees(OrderVO order) {
    // 推广金额
    List<Commission> commissions = unionService
        .listByOrderId(order.getId());
    BigDecimal cmFee = BigDecimal.ZERO;
    for (Commission cm : commissions) {
      cmFee = cmFee.add(cm.getFee());
    }

    return findOrderFees(order, cmFee);
  }

  private Boolean isXQUser(User user) {
    if (user != null && user.getPartner() != null
        && user.getPartner().equalsIgnoreCase("xiangqu")
        && StringUtils.isNotEmpty(user.getExtUserId())) {
      return true;
    }
    return false;
  }

  @Override
  public int autoRemindSign() {
    int ret = 0;
    List<Order> orders = orderMapper.selectAutoSignRemind();
    if (orders == null || orders.size() == 0) {
      return ret;
    }

    for (Order order : orders) {
      User buyerUser = userService.load(order.getBuyerId());
      if (isXQUser(buyerUser)) {
        pushMessage(PushMsgId.OrderDelaySignRemind.getId(),
            buyerUser.getId(), Long.toString(new Date().getTime()),
            PushMsgType.MSG_ORDER_DELAYSIGN_B.getValue(),
            order.getId());
        ret++;

        OrderVO orderVO = new OrderVO(order);
        User seller = userService.load(order.getSellerId());
        User buyer = userService.load(order.getBuyerId());
        orderMessageService.sendBuyerSMSOrderDelayRemind(orderVO,
            seller, buyer);
      }
    }
    return ret;
  }

  @Override
  public int updRemarkByAdmin(String id, String remark) {
    return orderMapper.updRemarkByAdmin(id, remark);
  }

  @Override
  public String obtainPaymentChannel(String orderNo) {
    return orderMapper.obtainPaymentChannel(orderNo);
  }

  @Override
  public void updateOrderRefundByAdmin(List<String > orderIds) {
    if (orderIds != null && orderIds.size() > 0) {
      for (String orderId : orderIds) {
        this.updateOrderRefundByAdmin(orderId);
      }
    }
  }

  @Override
  public List<Order> listBuyerOrders(String buyerUserId, Pageable pager) {
    return orderMapper.selectByBuyer(buyerUserId, pager);
  }

  @Override
  public Long countBuyerOrders(String buyerUserId) {
    return orderMapper.countByBuyer(buyerUserId);
  }

  @Override
  public Long countBuyerOrdersByStatus(OrderStatus status) {
    String userId = getCurrentUser().getId();
    if (StringUtils.isBlank(userId)) {
      return 0L;
    }

    if (status.equals(CLOSED)) {
      Long cnt = 0L;
      cnt += orderMapper.countByBuyerAndStatus(userId,
          CANCELLED, null, null);
      cnt += orderMapper.countByBuyerAndStatus(userId,
          SUCCESS, null, null);
      cnt += orderMapper.countByBuyerAndStatus(userId,
          CLOSED, null, null);
      return cnt;
    }

    return orderMapper.countByBuyerAndStatus(userId, status, null, null);
  }

  @Override
  public List<OrderMessage> viewReplyMsgs(String userId, Pageable page) {
    if (StringUtils.isBlank(userId)) {
      return Collections.emptyList();
    }

    List<OrderMessage> result = orderMessageMapper.selectRepsMsgBySeller(
        userId, page);
    if (result == null) {
      return Collections.emptyList();
    }

    return result;
  }

  @Override
  public List<OrderMessage> viewReplyMsgs(Pageable pageable) {
    return viewReplyMsgs(getCurrentUser().getId(), pageable);
  }

  @Override
  public boolean cancel(String orderId, String userId) {
    Order o = load(orderId);
    if (o == null) {
      log.warn("您要取消的订单不存在");
      return false;
    }

    /*
     * if(!o.getSellerId().equals(userId)){ log.warn("您没有权限取消订单"); return
     * false; }
     */

    //2018-12-14 Yarm设置归属城市和归属店铺
    this.customerOwnerAdress.setOwnerAdress(userId,orderId, null);
    cancel(orderId);
    return true;
  }

  @Override
  public List<OrderVO> listByStatus4SellerWithLike(OrderStatus status,
                                                   Pageable page, String key, Map<String , Object> params, String userId) {
    if (StringUtils.isNotBlank(key)) {
      key = "%" + key + "%";
    }

    List<OrderItemWithOrderVO> orderItemWithOrderVOs = orderMapper
        .selectBySellerAndStatusKey(userId, status, key, page, params);
    List<OrderVO> orders = new ArrayList<OrderVO>();
    Map<String , OrderVO> orderIdMap = new HashMap<String , OrderVO>();
    for (int i = 0; i < orderItemWithOrderVOs.size(); i++) {
      OrderItemWithOrderVO vo = orderItemWithOrderVOs.get(i);
      OrderVO orderVo = vo.getOrderVO();
      OrderItem orderItem = vo.getOrderItem();
      if (orderIdMap.containsKey(orderVo.getId())) {
        OrderVO bean = orderIdMap.get(orderVo.getId());
        if (bean.getOrderItems() == null) {
          List<OrderItem> list = new ArrayList<OrderItem>();
          list.add(orderItem);
          bean.setOrderItems(list);
        } else {
          bean.getOrderItems().add(orderItem);
        }
      } else {
        List<OrderItem> list = new ArrayList<OrderItem>();
        list.add(orderItem);
        orderVo.setOrderItems(list);
        orderIdMap.put(orderVo.getId(), orderVo);
        orders.add(orderVo);
      }
    }

    for (OrderVO order : orders) {
      order.setOrderItemVOs(orderItemMapper.selectVOByOrderId(order.getId()));
    }

    return orders;
  }

  @Override
  public List<OrderVO> listByStatusKey4Seller(OrderStatus status, String key) {

    String userId = getCurrentUser().getId();
    List<OrderVO> orders = listByStatus4SellerWithLike(status, null, key, null, userId);
    for (OrderVO order : orders) {
      if (userId.equals(order.getBuyerId())) {
        order.setFromType("in");
      } else {
        order.setFromType("out");
      }
      order.setOrderItemVOs(orderItemMapper.selectVOByOrderId(order.getId()));
      order.setOrderItems(orderItemMapper.selectByOrderId(order.getId()));
      order.setDefDelayDate(Integer.parseInt(delayDay));
    }
    return orders;
  }

  @Override
  public List<OrderVO> listByStatusKey4Seller(OrderStatus status, String key, Pageable pageable,
                                              Map<String , Object> params, String userId) {
    // 如果传递了userId，则是查该userId对应的订单，否则则是查询当前用户的订单
    if (StringUtils.isEmpty(userId)) {
      userId = getCurrentUser().getId();
    }
    List<OrderVO> orders = listByStatus4SellerWithLike(status, pageable, key, params, userId);
    for (OrderVO order : orders) {
      if (userId.equals(order.getBuyerId())) {
        order.setFromType("in");
      } else {
        order.setFromType("out");
      }
      order.setOrderItemVOs(
          orderItemMapper.selectVOByOrderIdAndUserId(order.getId(), userId));
      order.setOrderItems(orderItemMapper.selectByOrderId(order.getId()));
      order.setDefDelayDate(Integer.parseInt(delayDay));
    }
    return orders;
  }

  @Override
  public List<OrderItemVO> selectVOByOrderIdAndUserId(String orderId, String userId) {
    return orderItemMapper.selectVOByOrderIdAndUserId(orderId, userId);
  }


  @Override
  public List<OrderVO> listOrders4Export(Map<String , Object> params) {
    return orderMapper.selectOrders4Export(params);
  }

  // ===================私有方法============================
  private void setCouponInfo(PricingResultVO prices, Order order) {
    /*
     * if (prices.getCoupon() != null) {
     * order.setCouponId(prices.getCoupon().getId());
     * couponServcie.use(prices.getCoupon().getId()); }
     */

  }

  /**
   * 去掉想去硬编码， 先注释掉，有问题再放开
   **/
  private void setOrderPartnerType(User user, Order order) {
    // UserPartnerType type = findPartnerTypeByPartner(user.getPartner());
    // order.setPartnerType(type);
  }

  // 发现很多地方还在用该type
  private UserPartnerType findPartnerTypeByPartner(String partner) {
    if (XIANGQU.equals(partner)) {
      return UserPartnerType.XIANGQU;
    }

    if ("kkkd".equals(partner)) {
      return UserPartnerType.KKKD;
    }

    return null;
  }

  private Domain loadOrderDomain(User user) {
    Domain domain = null;
    if (StringUtils.isNotBlank(user.getPartner())) {
      domain = domainService.loadByCode(user.getPartner());
    } else {
      domain = domainService.loadRootDomain();
    }

    if (domain == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "用户错误 partner=[" + user.getPartner() + "]");
    }
    return domain;
  }

  /**
   * 获取订单的下单方式
   */
  private void setOrderTypeInfo(Boolean danbao, Order order) {
    OrderType orderType = OrderType.DIRECT;

    if (Boolean.TRUE.equals(danbao)) {
      orderType = OrderType.DANBAO;
    } else {
      String shopId = order.getShopId();
      if (StringUtils.isBlank(shopId)) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            "设置订单的交易方式的时候，订单所属的店铺不存在");
      }

      Shop shop = shopService.load(shopId);
      if (shop == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            "设置订单的交易方式的时候，店铺" + shopId + "不存在");
      }

      if (shop.getDanbao() != null && shop.getDanbao()) {
        orderType = OrderType.DANBAO;
      }
    }

    order.setType(orderType);
  }

  @Override
  public int autoRefreshOrder() {

    int retVal = 0;
    return retVal;

  }

  private boolean getStatusMapping(String orderid, String statusERP, String logisticsCompany,
                                   String logisticsOrderNo) {

    boolean result = false;
    // 1=未付款订单，3=客审订单，4=财审订单，5=打印快递单，6=订单配货
    // 7=订单出库，8=途中订单，9=订单结算，10=订单成功，13=订单取消，14=缺货订单
    // SUBMITTED, // 已提交 ，未付款
    // CANCELLED, // 取消
    // PAID, // 已付款 未发货
    // SHIPPED, // 已发货
    // SUCCESS, // 交易成功
    // REFUNDING, // 退款申请中
    // CLOSED // 交易关闭
    if (statusERP != null && !statusERP.equals("")) {
      Date now = new Date();
      if (statusERP.equals("订单出库")) {
        result = shipByErp(orderid, logisticsCompany, logisticsOrderNo);
        if (result) {
          log.info("旺店通 回传订单成功 : orderid=" + orderid);
          orderMapper.updateOrderLogisticsErpStatus(orderid, now, SysnErpStatus.SUCCESS,
              "SUCCESS");

        } else {
          log.info("旺店通 回传订单失败 : orderid=" + orderid);
          orderMapper.updateOrderLogisticsErpStatus(orderid, now, SysnErpStatus.FAIL,
              "FAIL");
        }
      } else if (statusERP.equals("途中订单")) {
        result = shipByErp(orderid, logisticsCompany, logisticsOrderNo);
        if (result) {
          log.info("旺店通 回传订单成功 : orderid=" + orderid);
          orderMapper.updateOrderLogisticsErpStatus(orderid, now, SysnErpStatus.SUCCESS,
              "SUCCESS");

        } else {
          log.info("旺店通 回传订单失败 : orderid=" + orderid);
          orderMapper.updateOrderLogisticsErpStatus(orderid, now, SysnErpStatus.FAIL,
              "FAIL");
        }

      } else if (statusERP.equals("缺货订单")) {
        log.info("旺店通 回传订单失败 : orderid=" + orderid);
        orderMapper.updateOrderLogisticsErpStatus(orderid, now, SysnErpStatus.FAIL, "缺货订单");
      }
//			else if (statusERP.equals("订单成功")) {
//				//orderStatus = OrderStatus.SUCCESS;
//				result = execute(orderid, OrderActionType.SIGN, null);
//			}
//			else if (statusERP.equals("客审订单")) {
//				orderStatus = OrderStatus.PAID;
//		    }else if (statusERP.equals("财审订单")) {
//				orderStatus = OrderStatus.PAID;
//		    }else if (statusERP.equals("订单取消")) {
//				orderStatus = OrderStatus.CANCELLED;
//			}else if (statusERP.equals("缺货订单")) {
//				orderStatus = OrderStatus.PAID;
//			}else if (statusERP.equals("打印快递单")) {
//				orderStatus = OrderStatus.PAID;
//			}else if (statusERP.equals("订单配货")) {
//				orderStatus = OrderStatus.PAID;
//			}else if (statusERP.equals("订单结算")) {
//			orderStatus = OrderStatus.SHIPPED;
//		    }else if (statusERP.equals("未付款订单")) {
//			orderStatus = OrderStatus.SUBMITTED;
//		    }

    }

    return result;

  }

  @Override
  public boolean shipByErp(String orderId, String logisticsCompany,
                           String logisticsOrderNo) {
    Map<String , Object> params = new HashMap<String , Object>();
    params.put(OrderWorkflow.LOGISTICS_COMPANY_PROPERTY, logisticsCompany);
    params.put(OrderWorkflow.LOGISTICS_ORDERNO_PROPERTY, logisticsOrderNo);
    executeBySystem(orderId, OrderActionType.SHIP, params);
    return true;
  }

  @Override
  public List<Order> selectOrderList2Jooge(Pageable pager) {
    return orderMapper.selectOrderList2Jooge(pager);
  }

  @Override
  public int updateOrderSendErpStatus(String id, Date sendErpAt,
                                      SendErpStatus sendErpStatus, SendErpDest sendErpDest,
                                      String sendErpLog) {
    return orderMapper
        .updateOrderSendErpStatus(id, sendErpAt, sendErpStatus, sendErpDest, sendErpLog);
  }

  @Override
  public int updateOrderLogisticsErpStatus(String id, Date sysnErpAt,
                                           SysnErpStatus sysnErpStatus, String sysnErpLog) {
    return orderMapper.updateOrderLogisticsErpStatus(id, sysnErpAt, sysnErpStatus, sysnErpLog);
  }

  /**
   * 将买家对卖家的留言批量保存
   */
  @Override
  @Transactional
  public void saveMessages(List<OrderMessage> orderMessages) {
    for (OrderMessage orderMessage : orderMessages) {
      saveMessage(orderMessage);
    }
  }

  @Override
  public int selectOrderCountByMainOrderId(String mainOrderId, String orderStatus) {
    return orderMapper.selectOrderCountByMainOrderId(mainOrderId, orderStatus);
  }

  @Override
  public List<Order> selectStatusByMainOrderId(String mainOrderId) {
    return orderMapper.selectStatusByMainOrderId(mainOrderId);
  }

  @Override
  public Long countByPartner(Map<String , Object> params) {
    return orderMapper.countPartnerByAdmin(params);
  }

  @Override
  public List<OrderVO> listByPartner(Map<String , Object> params, Pageable pageable) {
    List<OrderVO> listOrder = orderMapper.selectPartnerByAdmin(params, pageable);
    for (OrderVO order : listOrder) {
      order.setOrderItems(orderItemMapper.selectByOrderId(order.getId()));
      order.setDefDelayDate(Integer.parseInt(delayDay));
    }
    return listOrder;
  }

  @Override
  public List<OrderVO> selectOrders4WxExport(Map<String , Object> params) {
    return orderMapper.selectOrders4WxExport(params);
  }

  @Override
  public Order selectLastMonthTotalFeeByShopId(String shopId) {
    return orderMapper.selectLastMonthTotalFeeByShopId(shopId);
  }

  @Override
  public List<OrderVO> selectLastMonthOrderByShop(String rootShopId, String shopId) {
    return orderMapper.selectLastMonthOrderByShop(rootShopId, shopId);
  }


  @Override
  @Transactional
  public boolean auditAndship(String orderId, String logisticsCompany,
                              String logisticsOrderNo) {

    // 先调用付款逻辑，然后调用发货逻辑
    executeBySystem(orderId, OrderActionType.PAY, null);

    Map<String , Object> params = new HashMap<String , Object>();
    params.put(OrderWorkflow.LOGISTICS_COMPANY_PROPERTY, logisticsCompany);
    params.put(OrderWorkflow.LOGISTICS_ORDERNO_PROPERTY, logisticsOrderNo);
    execute(orderId, OrderActionType.SHIP, params);
    return true;
  }

  @Override
  public long countOrder(Map<String , Object> params) {
    return orderMapper.countOrder(params);
  }

  /**
   * 查询是否是该用户第一笔成交成功的订单
   */
  @Override
  public long countBeforeSuccessOrder(String id, String userId) {
    return orderMapper.countBeforeSuccessOrder(id, userId);
  }


  @Override
  public Long countByTeam(Map<String , Object> params) {
    return orderMapper.countTeamByAdmin(params);
  }

  /**
   * 团队订单查询
   */
  @Override
  public List<OrderVO> listByTeam(Map<String , Object> params, Pageable pageable) {
    List<OrderVO> listOrder = orderMapper.selectTeamByAdmin(params, pageable);
    for (OrderVO order : listOrder) {
      order.setOrderItems(orderItemMapper.selectByOrderId(order.getId()));
      order.setDefDelayDate(Integer.parseInt(delayDay));
    }
    return listOrder;
  }

  /**
   * 统计某个店铺的总会员数，即在这个店铺下过订单的用户数
   */
  @Override
  public Long countMemberByShopId(String shopId) {
    return orderMapper.countMemberByShopId(shopId);
  }

  /**
   * 查询某个店铺所有卖出的商品总金额
   */
  @Override
  public BigDecimal sumBySellerShopId(String shopId) {
    return orderMapper.sumBySellerShopId(shopId);
  }

  /**
   * 确认订单
   */
  @Override
  public int confirmOrder(String orderId) {
    return orderMapper.confirmOrder(orderId);
  }

  /**
   * 根据类型与用户id查询活动订单
   */
  @Override
  public List<OrderVO> listByPromotionTypeAndUserId(PromotionType type, String userId,
                                                    String shopId, Pageable pageable) {
    return orderMapper.listByPromotionTypeAndUserId(type, userId, shopId, pageable);
  }

  /**
   * 根据活动与用户id count活动订单数
   */
  @Override
  public Long countByPromotionTypeAndUserId(PromotionType type, String userId, String shopId) {
    return orderMapper.countByPromotionTypeAndUserId(type, userId, shopId);
  }

  /**
   * 查询订单下orderItem数量
   *
   * @return orderItem数量
   */
  @Override
  public int countOrderItem(String orderId) {
    return orderMapper.countOrderItem(orderId);
  }

  /**
   * 待收货订单数
   */
  @Override
  public long countTypeOrderCount(String userId, String status) {
    return orderMapper.countTypeOrderCount(userId, status);
  }


  /**
   * 订单dest中不可退部分
   */
  private static String NOT_REFUNDABLE = "NOT_REFUNDABLE";

  @Override
  public boolean checkAndUpdateCapableToRefund(Order order) {
    boolean capableToRefund = order.getCapableToRefund();
    boolean isOpen = capableToRefund;
    Date openTime = null;
    if (capableToRefund) {
      //如果开放售后，检查是否过期
      Optional<Date> updatedAt = Optional.fromNullable(order.getUpdateRefundEntryAt());
      isOpen = updatedAt.transform(new Function<Date, Boolean>() {
        @Override
        public Boolean apply(Date input) {
          long t = System.currentTimeMillis() - input.getTime();
          if (t >= (3600000 * 24 * 7)) {//7天后售后的入口自动关闭
            return false;
          }
          return true;
        }
      }).or(false);
    } else {//如果没开放售后，检查是否应该应该开放
      //先检查订单是否为不可退订单
      Optional<String> dest = Optional
          .fromNullable(order.getDest());
      if (!dest.isPresent()) {
        log.error("order id:{},{}", order.getId(),
            GlobalErrorCode.ORDER_DEST_CAN_NOT_BE_NULL.getError());
      }
      boolean refundable = dest.transform(new Function<String, Boolean>() {
        @Override
        public Boolean apply(String input) {
          return !input.toString().contains(NOT_REFUNDABLE);
        }
      }).or(false);
      if (refundable) {//只有在订单是可退的情况下做进一步判断
        OrderStatus status = order.getStatus();
        if (status.equals(SUCCESS)) {
          //如果订单已经收货,则需满足两个条件才可开启售后:1.距离收货没超过七天，2.且没开启过售后
          Optional<Date> successDate = Optional.fromNullable(order.getSucceedAt());
          boolean isOver = successDate.transform(new Function<Date, Boolean>() {
            @Override
            public Boolean apply(Date input) {
              long t = System.currentTimeMillis() - input.getTime();
              if (t >= (3600000 * 24 * 7)) {
                return true;
              }
              return false;
            }
          }).or(false);
          if (!isOver && null == order.getUpdateRefundEntryAt()) {
            //设置开放时间,收货时间
            if (successDate.isPresent()) {
              openTime = successDate.get();
              isOpen = true;
            }
          }
        }
        if (status.equals(SHIPPED)) {
          //如果订单已经发货，则判断距离下单时间是否超过14*24
          Optional<Date> dateOptional = Optional.fromNullable(order.getPaidAt());
          isOpen = dateOptional.transform(new Function<Date, Boolean>() {
            @Override
            public Boolean apply(Date input) {
              long t = System.currentTimeMillis() - input.getTime();
              if (t <= (3600000 * 24 * 14) && t >= (3600000 * 24 * 7)) {
                return true;
              }
              return false;
            }
          }).or(false);
          if (isOpen) {
            openTime = new Date(dateOptional.get().getTime() + 24 * 3600000 * 7);
          }
        }
        //其他状态下不允许申请售后
      }
    }
    if (isOpen != capableToRefund) {
      updateCapableToRefundById(isOpen, order.getId(), openTime);
      order.setCapableToRefund(isOpen);
    }
    return isOpen;
  }

  @Override
  public boolean updateCapableToRefundById(boolean capableToRefund, String id,
                                           Date openTime) {
    return orderMapper.updateCapableToRefundById(capableToRefund, id, null) > 0;
  }

  /**
   * 根据OrderNo更新数据
   */
  @Override
  @Transactional
  public int updateByOrderNo(WmsExpress wmsExpress) {
    Integer num = orderMapper.updateByOrderNo(wmsExpress);
    return num;
  }

  @Override
  @Transactional
  public List<WmsOrderNtsProduct> getWmsOrderNtsProduct(String orderNo) {
    List<WmsOrderNtsProduct> list = orderMapper.getWmsOrderNtsProduct(Integer.parseInt(wmsInterval),
        orderNo);
    Iterator<WmsOrderNtsProduct> iterator = list.iterator();
    List<WmsOrderNtsProduct> combineSlaves = new ArrayList<>();
    while (iterator.hasNext()) {
      WmsOrderNtsProduct w = iterator.next();

      // 促销活动增加  生成的product 需要设置 源productId
      Product productById = productService.findProductById(w.getProductId());
      if(null!=productById && null!=productById.getSourceId()){
        w.setProductId(productById.getSourceId());
        Sku sku = skuMapper.selectByPrimaryKey(w.getSkuId());
        if(null!= sku && StringUtils.isNotBlank(sku.getSourceSkuCode())){
          Sku bySkuCode = skuMapper.selectBySkuCode(sku.getSourceSkuCode());
          w.setSkuId(IdTypeHandler.decode(bySkuCode.getId()));
          w.setStockSku(sku.getSourceSkuCode());
        }
      }


      String skuId = w.getSkuId();
      String productId = w.getProductId();
      w.setEdiStatus(1);
      if (skuId != null && productId != null) {

        boolean isCombine = skuCombineMapper.isCombineProduct(productId);
        // 套装商品, 特殊处理
        if (isCombine) {
          List<SkuCombineExtra> extras = skuCombineExtraMapper.listSlaveBySkuId(skuId);
          List<WmsOrderNtsProduct> slaveWmsProduct = buildCombineWmsProduct(w, extras);
          //如果是大会活动订单对子商品设置活动类型
          if(PromotionType.MEETING.toString().equals(w.getType())||PromotionType.MEETING_GIFT.toString().equals(w.getType())){
            for (WmsOrderNtsProduct wmsOrderNtsProduct : slaveWmsProduct) {
              wmsOrderNtsProduct.setType(w.getType());
            }
          }
          // 如果是套装商品则删除, 并将子商品添加到列表
          if (CollectionUtils.isNotEmpty(slaveWmsProduct)) {
            iterator.remove();
            combineSlaves.addAll(slaveWmsProduct);
          }
        }
      }
    }
    list.addAll(combineSlaves);
    return list;
  }

  /**
   * 将套餐中的子商品单独构建为wms商品推送 返回格式兼容原来的查询格式
   */
  private List<WmsOrderNtsProduct> buildCombineWmsProduct(WmsOrderNtsProduct master,
      List<SkuCombineExtra> extras) {
    if (master == null || CollectionUtils.isEmpty(extras)) {
      return Collections.emptyList();
    }
    List<WmsOrderNtsProduct> ret = new ArrayList<>();
    for (SkuCombineExtra extra : extras) {
      // 构造值与原sql查询中的保持兼容
      SkuBasicVO sku = skuMapper.selectBasicVOByPrimaryKey(extra.getSlaveId());
      WmsOrderNtsProduct wms = new WmsOrderNtsProduct();
      // 套装应发货件数等于主商品购买件数 * 每套件数
      Long amount = Long.valueOf(extra.getAmount()) * master.getAmount();
      BigDecimal price = sku.getPrice();
      wms.setOrderNo(master.getOrderNo());
      wms.setRemark(master.getRemark());
      wms.setProductId(IdTypeHandler.decode(sku.getProductId()));
      wms.setProductName(sku.getProductName());
      wms.setAmount(amount);
      // 加个字段在wms中应该无意义, 此处重新计算item价格, 保持兼容
      //更改为单价 2018-12-19
      wms.setMarketPrice(price);
      wms.setDiscount(master.getDiscount());
      wms.setStockSku(sku.getSkuCode());
      wms.setSkuId(IdTypeHandler.decode(sku.getId()));
      wms.setCreatedAt(master.getCreatedAt());
      wms.setEdiStatus(1);

      // 设置套装商品属性
      wms.setIsCombination(1);
      wms.setCombinationName(master.getProductName());
      wms.setCombinationNo(master.getStockSku());
      wms.setCombinationNum(master.getAmount());
      wms.setType(master.getType());
      ret.add(wms);
    }
    return ret;
  }

  @Override
  public List<WmsOrderPackage> getWmsOrderPackage(String orderNo) {
    List<WmsOrderPackage> list = orderMapper.getWmsOrderPackage(Integer.parseInt(wmsInterval), orderNo);
    for (WmsOrderPackage w : list) {
      w.setEdiStatus(1);
      w.setOrderingQty(1L);
      w.setPackageName("");
      w.setPackageType("");
      w.setWarehouseQty(1L);
    }
    return list;
  }

  /**
   * 判断订单是否是同步给WMS的
   *
   * @return true 是
   */
  private boolean isToWms(String orderNo) {
    boolean ret = false;
    try {
      if (StringUtils.isNotBlank(orderNo)) {
        OrderVO vo = loadByOrderNo(orderNo);
        List<OrderItem> items = vo.getOrderItems();
        if (CollectionUtils.isNotEmpty(items)) {
          OrderItem it = items.get(0);
          String skuId = it.getSkuId();
          if (StringUtils.isNotBlank(skuId)) {
            Optional<Sku> sku = Optional.fromNullable(skuMapper.selectByPrimaryKey(skuId));
            if (sku.isPresent() && sku.get().getSkuCodeResources()
                .equals(SkuCodeResourcesType.getSkuCodeResourceBySupplierCode("CLIENT").orElseThrow(RuntimeException::new).getRes())) {
              ret = true;
            }
          }
        }
      }
    } catch (Exception e) {
      log.error("判断订单所属供应商失败", e);
    }
    return ret;
  }

  @Override
  @Transactional
  public List<WmsOrder> getWmsOrder() {
    List<WmsOrder> list = orderMapper.getWmsOrder(Integer.parseInt(wmsInterval));
    Iterator<WmsOrder> iterator = list.iterator();
    while (iterator.hasNext()) {
      try {
        WmsOrder w = iterator.next();
        w.setLocalEdiStatus(1);
        w.setWmsEdiStatus(0);
        w.setWarehouseCode("01");
        w.setFreightDiscount(new BigDecimal(1));
        w.setWeight(new BigDecimal(1));
        w.setVolume(new BigDecimal(1));
        w.setStatus(0L);
        w.setOtherAdjustments(new BigDecimal(0));
        String zoneId = w.getZipcode();
        if (zoneId != null) {
          List<SystemRegion> regionList = systemRegionService.listParents(zoneId);
          if (CollectionUtils.isNotEmpty(regionList)) {
            StringBuilder address = new StringBuilder();
            for (SystemRegion region : regionList) {
              address.append(region.getName());
            }
            w.setAddress(address + w.getAddress());
          } else {
            w.setAddress(w.getAddress());
          }
        }

        //设置分会场的wms类型
        String orderNo = w.getOrderNo();
        Order order = java.util.Optional.ofNullable(orderMapper.selectByOrderNo(orderNo))
                .orElseThrow(BizException.ofSupplier(GlobalErrorCode.INTERNAL_ERROR, "订单不存在"));
        List<OrderItem> orderItems = orderItemMapper.selectByOrderId(order.getId());
        for (OrderItem orderItem:
             orderItems) {
          String skuId = java.util.Optional.ofNullable(orderItem.getSkuId())
                  .orElseThrow(BizException.ofSupplier(GlobalErrorCode.INTERNAL_ERROR, "skuId为空"));
          Sku sku = skuMapper.selectByPrimaryKey(skuId);
          Assert.notNull(sku, "商品不存在或已删除, skuId = " + skuId);
          log.error("SKUID+++:"+skuId);
          if (sku!=null&&sku.getPromotionType() != null) {
            String promotionType = sku.getPromotionType();
            StadiumType stadiumType = null;
            try {
              stadiumType = StadiumType.valueOf(promotionType);
            } catch (Exception e) {
              log.error("分会场类型转换失败,type:{}", promotionType, e);
            }
            StadiumType[] stadiumTypes = StadiumType.values();
            if (ArrayUtils.contains(stadiumTypes, stadiumType)) {
              w.setType(WMS_TYPE);
            }
          }
        }
      } catch (Exception e) {
        log.error("同步订单失败,order no:{}", e);
      }
    }
    return list;
  }
  @Override
  @Transactional
  public List<WmsOrder> getWmsOrder2() {
    List<WmsOrder> list = orderMapper.getWmsOrder2(Integer.parseInt(wmsInterval));
    Iterator<WmsOrder> iterator = list.iterator();
    while (iterator.hasNext()) {
      try {
        WmsOrder w = iterator.next();
        w.setLocalEdiStatus(1);
        w.setWmsEdiStatus(0);
        w.setWarehouseCode("01");
        w.setFreightDiscount(new BigDecimal(1));
        w.setWeight(new BigDecimal(1));
        w.setVolume(new BigDecimal(1));
        w.setStatus(0L);
        w.setOtherAdjustments(new BigDecimal(0));
        String zoneId = w.getZipcode();
        if (zoneId != null) {
          List<SystemRegion> regionList = systemRegionService.listParents(zoneId);
          if (CollectionUtils.isNotEmpty(regionList)) {
            StringBuilder address = new StringBuilder();
            for (SystemRegion region : regionList) {
              address.append(region.getName());
            }
            w.setAddress(address + w.getAddress());
          } else {
            w.setAddress(w.getAddress());
          }
        }

        //设置分会场的wms类型
        String orderNo = w.getOrderNo();
        Order order = java.util.Optional.ofNullable(orderMapper.selectByOrderNo(orderNo))
                .orElseThrow(BizException.ofSupplier(GlobalErrorCode.INTERNAL_ERROR, "订单不存在"));
        List<OrderItem> orderItems = orderItemMapper.selectByOrderId(order.getId());
        for (OrderItem orderItem:
                orderItems) {
          String skuId = java.util.Optional.ofNullable(orderItem.getSkuId())
                  .orElseThrow(BizException.ofSupplier(GlobalErrorCode.INTERNAL_ERROR, "skuId为空"));
          Sku sku = skuMapper.selectByPrimaryKey(skuId);
          Assert.notNull(sku, "商品不存在或已删除, skuId = " + skuId);
          log.error("SKUID+++:"+skuId);
          if (sku!=null&&sku.getPromotionType() != null) {
            String promotionType = sku.getPromotionType();
            StadiumType stadiumType = null;
            try {
              stadiumType = StadiumType.valueOf(promotionType);
            } catch (Exception e) {
              log.error("分会场类型转换失败,type:{}", promotionType, e);
            }
            StadiumType[] stadiumTypes = StadiumType.values();
            if (ArrayUtils.contains(stadiumTypes, stadiumType)) {
              w.setType(WMS_TYPE);
            }
          }
        }
      } catch (Exception e) {
        log.error("同步订单失败,order no:{}", e);
      }
    }
    return list;
  }
  @Override
  public List<WmsSku> getWmsOrderSku(String starttime,String skuCode) {
    List<WmsSku> list = orderMapper.getWmsOrderSku(starttime,skuCode);
    return list;
  }

  @Override
  public boolean updateStatusByOrderNo(OrderStatus status, String orderNo) {
    return orderMapper.updateStatusByOrderNo(status.toString(), orderNo) > 0;
  }

  @Override
  public boolean updateStatusByOrderId(OrderStatus status, String orderId) {
    return orderMapper.updateStatusByOrderId(status.toString(), orderId) > 0;
  }

  @Override
  public void updateOrderStatusByOrderNo(OrderStatus status, String orderNo) {
    orderMapper.updateOrderStatusByOrderNo(status.toString(), orderNo);
  }

  @Override
  public void updatePayNoByOrderNo(String orderNo, String payNo) {
    orderMapper.updatePayNoByOrderNo(orderNo, payNo);
  }

  @Override
  public void updateOrderByOrderNo(Order order) {
    orderMapper.updateOrderByOrderNo(order);
  }

  @Override
  public List<Order> queryOrderCanPush() {
    return orderMapper.queryOrderCanPush();
  }

  @Override
  public List<Order> loadByPayNo(String bizNo) {
    return orderMapper.loadByPayNo(bizNo);
  }

  @Override
  public void sfOrderCreateTask() {
    AsyncSubmitOrder asyncSubmitOrder = new AsyncSubmitOrder();
    List<Order> splitedOrders = queryOrderCanPush();

    for (Order order : splitedOrders) {
      List<OrderItem> orderItemList = listOrderItems(order.getId());
      OrderAddressVO orderAddressVO = orderAddressService.selectByOrderId(order.getId());
      if (orderItemList != null && !orderItemList.isEmpty()) {
        for (OrderItem orderItem : orderItemList) {
          Product product = productService.load(orderItem.getProductId());

          orderItem.setProduct(product);

          Sku sku = productService.loadSku(orderItem.getSkuId());
          orderItem.setSku(sku);

          order.setSource(ProductSource.SF);
        }

        Map<Order, List<OrderItem>> orderListMap = new HashMap<Order, List<OrderItem>>();
        orderListMap.put(order, orderItemList);

        asyncSubmitOrder.asyncSubmit(orderListMap, orderAddressVO, sfConfigMapper, profile, host,
            sfRegionService,
            systemRegionMapper, this, logisticsGoodsService);
      }
    }
  }

  @Override
  public synchronized Integer nextval() {
    return orderMapper.nextval();
  }
  
  /*
   * @Author chp
   * @Description //使用redis生成订单号
   * @Date  
   * @Param 
   * @return 
   **/
  @Override
  public synchronized Long nextval2() {
    return idSequenceService.generateIdByType2(IdSequenceType.ORDER);
  }
  
  
  @Override
  public List<Order> queryOrderByMainOrderId(String id) {
    return orderMapper.queryOrderByMainOrderId(id);
  }

  @Override
  @Transactional
  public void confirmChange(String orderId){
    log.info("begin /order/change/confirm");
    //根据orderId获取订单信息
    Order order = this.load(orderId);
    //获取订单编号用于修改订单状态
    String orderNo = order.getOrderNo();

    order.setOrderType(OrderSortType.EXCHANGE);//设置订单类型为换货订单，特殊标记
    order.setCapableToRefund(false);//设置不可申请售后
    this.insertOrder(order);//添加换货订单

    order.setId("");//id置空
    //添加address、orderItem
    OrderAddressVO orderAddressVO = orderAddressService.selectByOrderId(orderId);//先获取原地址信息
    orderAddressVO.setOrderId(order.getId());//重新设置orderId
    orderAddressService.insert(orderAddressVO);

    List<OrderItem> orderItemList = orderItemMapper.selectByOrderIdAndPT(orderId);//先获取订单商品信息
    for (OrderItem orderItem : orderItemList) {
      OrderItem item = orderItemMapper.selectByPrimaryKey(orderItem.getId());
      item.setOrderId(order.getId());//重新设置orderId
      orderItemMapper.insert(item);
    }

    OrderRefund orderRefund = orderRefundMapper.loadOrderRefund(orderId);
    //修改订单状态
    if(orderRefund != null && orderRefund.getOrderStatus() != null){
      this.updateOrderStatusByOrderNo(orderRefund.getOrderStatus(), orderNo);
    }else{
      this.updateOrderStatusByOrderNo(OrderStatus.SUCCESS, orderNo);
    }

    //修改换货申请状态
    orderRefundService.updateStatusByOrderId(orderId, OrderRefundStatus.SUCCESS, null);
  }


  @Override
  @Transactional
  public boolean confirmReissue(String orderId){
    log.info("begin /order/reissue/confirm");
    //根据orderId获取订单信息
    Order order = this.load(orderId);
    //获取订单编号用于修改订单状态
    String orderNo = order.getOrderNo();

    order.setOrderType(OrderSortType.REISSUE);//设置订单类型为补货订单，特殊标记
    order.setCapableToRefund(false);//设置不可申请售后
    this.insertOrder(order);//添加补货订单

    order.setId("");//id置空
    //添加address、orderItem
    OrderAddressVO orderAddressVO = orderAddressService.selectByOrderId(orderId);//先获取原地址信息
    orderAddressVO.setOrderId(order.getId());//重新设置orderId
    orderAddressService.insert(orderAddressVO);

    List<OrderItem> orderItemList = orderItemMapper.selectByOrderIdAndPT(orderId);//先获取订单商品信息
    for (OrderItem orderItem : orderItemList) {
      OrderItem item = orderItemMapper.selectByPrimaryKey(orderItem.getId());

      productService.updateSkuStock(item.getSkuId(), item.getAmount());//补货出仓扣减库存
      productService.updateAmountForRefund(item.getProductId(), item.getAmount());

      item.setOrderId(order.getId());//重新设置orderId
      orderItemMapper.insert(item);
    }

    OrderRefund orderRefund = orderRefundMapper.loadOrderRefund(orderId);
    orderRefundService.updateStatusByOrderId(orderId, OrderRefundStatus.SUCCESS, null);//修改售后订单为success
    if(orderRefund != null && orderRefund.getOrderStatus() != null){
      return orderMapper.updateStatusByOrderNo(orderRefund.getOrderStatus().toString(), orderNo) > 0;
    }else{
      return orderMapper.updateStatusByOrderNo(OrderStatus.SUCCESS.toString(), orderNo) > 0;
    }
  }

  // 未发货订单30分钟内，支持修改地址(针对非顺丰订单)
  @Override
  public boolean updateOrderAddress(String orderId, Address address) {
    Order order = orderMapper.selectByPrimaryKey(orderId);
    if (null == order) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "订单不存在");
    }
    if (null == order.getPaidAt() || "".equals(order.getPaidAt())) {
      throw new RuntimeException("当前订单[" + order.getId() + "]的支付时间为空");
    }
    boolean timeOut = (System.currentTimeMillis() - order.getPaidAt().getTime()) > 1800000;
    if (timeOut) {
      log.info("抱歉，您支付已超过30分钟，不可再修改地址");
      return false;
//      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "抱歉，您支付已超过30分钟，不可再修改地址");
    } else {
        OrderAddressVO orderAddressVO = orderAddressService.selectByOrderId(orderId);
        if (null == orderAddressVO) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, "订单地址不存在");
        }
        log.info("order_address id is: " + orderAddressVO.getId());
        orderAddressVO.setZoneId(address.getZoneId());
        orderAddressVO.setStreet(address.getStreet());
        orderAddressVO.setConsignee(address.getConsignee());
        orderAddressVO.setPhone(address.getPhone());

        OrderAddress orderAddress = new OrderAddress();
        BeanUtils.copyProperties(orderAddressVO, orderAddress);
        return orderAddressService.update(orderAddress) == 1;
    }
  }

  @Override
  @Transactional
  public void submitChange(String orderId, String productIdAndAmount){
    log.info("begin /order/change/submit");
    //根据orderId获取订单信息
    Order order = load(orderId);
    //获取订单编号用于修改订单状态
    String orderNo = order.getOrderNo();
    order.setOrderType(OrderSortType.EXCHANGE);//设置订单类型为换货订单，特殊标记
    insertOrder(order);//执行插入操作
    order.setId("");//id置空
    order.setBuyerId("");//客服下单，将buyerId置空
    //除了需要插入订单记录外，还需要插入address、orderItem等，否则会影响项目运行
    //下面的orderId需要获取刚插入订单的orderId
    OrderAddressVO orderAddressVO = orderAddressService.selectByOrderId(orderId);//先获取原地址信息
    orderAddressVO.setOrderId(order.getId());//重新设置orderId
    orderAddressService.insert(orderAddressVO);//执行插入操作

    String [] split = productIdAndAmount.split(",");
    List<String> productIds = new ArrayList<>();
    List<String> amount = new ArrayList<>();
    for(int i =0; i < split.length; i++){
      if(StringUtil.isNotEmpty(split[i])) {
        if(i == 0) {
          productIds.add(split[i]);
          continue;
        }
        if(i == 1) {
          amount.add(split[i]);
          continue;
        }
        if(i % 2 == 0){
          productIds.add(split[i]);
        }else{
          amount.add(split[i]);
        }
      }
    }

    if(!(productIds.size() > 0)){
      throw new RuntimeException("换货商品信息获取异常");
    }
    List<OrderItem> orderItemList = new ArrayList<>();
    for (int i = 0; i < productIds.size(); i++) {
      if(StringUtil.isNotEmpty(productIds.get(i))){
        OrderItem orderItem = new OrderItem();
        ProductVO productVO = productService.load(productIds.get(i));
        orderItem.setOrderId(order.getId());//重新设置orderId
        orderItem.setProductId(productVO.getId());
        orderItem.setSkuId(productVO.getSkus().get(0).getId());
        orderItem.setProductName(productVO.getName());
        orderItem.setProductImg(productVO.getImg());
        orderItem.setProductImgUrl(productVO.getImgUrl());
        orderItem.setPrice(productVO.getPrice());
        orderItem.setMarketPrice(productVO.getMarketPrice());
        BigDecimal disCount = new BigDecimal(productVO.getDiscount() == null ? 0 : productVO.getDiscount());
        orderItem.setDiscount(disCount);
        orderItem.setDiscountPrice(order.getDiscountFee());
        orderItem.setPaidPoint(order.getPaidPoint());
        orderItem.setPaidCommission(order.getPaidCommission());
        orderItem.setPromoAmt(productVO.getPromoAmt());
        orderItem.setServerAmt(productVO.getServerAmt());
        //数量设置
        if(!(amount.size() > 0) || StringUtil.isEmpty(amount.get(i))){
          throw new RuntimeException("换货商品数量异常");
        }
        orderItem.setAmount(Integer.parseInt(amount.get(i)));
        //加入商品列表
        orderItemList.add(orderItem);
      }else{
        throw new RuntimeException("换货商品信息获取异常");
      }
    }

    //遍历商品列表，执行插入操作
    for (OrderItem item : orderItemList) {//对订单商品进行遍历
      orderItemMapper.insert(item);//执行插入操作
    }

    OrderRefund orderRefund = orderRefundMapper.loadOrderRefund(orderId);
    //订单插入完成后，修改原订单为完成状态、修改换货订单为已完成
    //修改状态的同时，还需要设置订单完成时间，此操作在数据库更新时进行
    if(orderRefund != null && orderRefund.getOrderStatus() != null){
      updateOrderStatusByOrderNo(orderRefund.getOrderStatus(), orderNo);
    }else{
      updateOrderStatusByOrderNo(OrderStatus.SUCCESS, orderNo);
    }

    orderRefundService.updateStatusByOrderId(orderId, OrderRefundStatus.SUCCESS, null);
  }

  @Override
  public List<OrderVO> selectByBuyerAndStatusList(String userId, List<String> statusList, Pageable pageable, String type) {
    Integer modifyAdressDateFlag = null; //30分钟内是否修改地址
    Integer consultApplyDateFlag = null; //14天内可以申请售后

    if(Constant.MODIFY_ADRESS.name().equalsIgnoreCase(type)){
      modifyAdressDateFlag = 30;//30分钟
    }
    if(Constant.CONSULT_APPLY.name().equalsIgnoreCase(type)){
      consultApplyDateFlag = 14;//14天
    }

    // 如果传递了userId，则是查该userId对应的订单，否则则是查询当前用户的订单
    if (StringUtils.isEmpty(userId)) {
      userId = getCurrentUser().getId();
    }

    List<OrderVO> list = orderMapper.selectByBuyerAndStatusList(userId, statusList, pageable,modifyAdressDateFlag,consultApplyDateFlag);
    return list;
  }

  @Override
  public Boolean checkSaleZone(String buyerId) {
      return orderMapper.checkSaleZone(buyerId);
  }

  /**
   * 解决生产环境重复支付的问题的临时方案，后续修复后要做相应的改动，不要轮循调用 TODO
   * @param orderId
   * @return
   */
  @Override
  public boolean getPayCallBackStatus(String orderId) {
    boolean result = true;
    RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
    //强制关闭支付轮循
    boolean b = this.promotionConfigService.getEspCircleSwitch();
    if(b)
      return true;

    //优先取redis标记
    String redisFlag = redisUtils.get(new StringBuffer().append("hw:pay_esp_circle_result")
    .append(orderId).toString());
    if(StringUtils.isNotBlank(redisFlag))
      return false;

    //获取order表的id
    Set<Boolean> set = new HashSet<>();
    List<Long> oderIdList = this.orderMapper.selectIdByMainOrderId(orderId);
    for (Long id : oderIdList){
      boolean isPaid = this.orderMapper.selectCallBackStatus(id);
      set.add(isPaid);
    }

    //子订单是否都回调，若银联包含一个false则说明银联回调失败
    if(set != null && set.contains(false))
      result = false;

      if (!result) {
          // 二次确认如果第三方支付已成功，则返回成功
          MainOrder mainOrder = mainOrderService.load(orderId);
          if (mainOrder != null) {
              result = thirdPaymentQueryService.query(mainOrder)
                      .map(ThirdPaymentQueryRes::isPaid).orElse(false);
          }
      }

    if(!result)
      redisUtils.set(new StringBuffer().append("hw:pay_esp_circle_result")
              .append(orderId).toString(), "1", 1, TimeUnit.SECONDS);
    return result;
  }

  /**
   * 退款时增加订单查询校验
   * @param orders 订单
   */
  private void cancelOrder(List<Order> orders, int retVal) {
    for (Order order : orders) {
      try {
        //取消主动查询订单状态的逻辑
//        tryCancelOrder(order);
        executeBySystem(order.getId(), OrderActionType.CANCEL, null);
      } catch (Exception e) {
        log.error("取消订单失败" + order.getOrderNo(), e.getMessage(), e);
      }
      retVal++;
    }
  }

  private void tryCancelOrder(Order order) {
    if (null == order.getMainOrderId()) {
      log.error("主订单编号为空，不能查询订单是否已支付");
      return;
    }
    MainOrder mainOrder = mainOrderService.load(order.getMainOrderId());
    boolean queryByEpsResult = false;
    if (StringUtils.isNotBlank(mainOrder.getPayNo())) {
      // 设置支付编号后，依次进行查询
      order.setPayNo(mainOrder.getPayNo());
      order.setPayType(PaymentMode.EPS);
      Map<String, String> map = orderQuery.queryByEps(order, TradeType.APP);
      if (queryResult(map, order)) {
        queryByEpsResult = true;
        //保存outPay
        String transactionId = map.get("transaction_id");
        this.savePayResult(order, TradeType.APP, transactionId);
      } else {
        Map<String, String> map1 = orderQuery.queryByEps(order, TradeType.MINIPROGRAM);
        if (queryResult(map1, order)) {
          queryByEpsResult = true;
          //保存outPay
          String transactionId = map.get("transaction_id");
          this.savePayResult(order, TradeType.MINIPROGRAM, transactionId);
        }
      }
    }
    if (queryByEpsResult) {//订单从eps查询出已支付
      cashierService.paid(mainOrder.getPayNo(), "order", mainOrder.getPayType());
    } else {
      log.error("支付编号为空，不能查询订单是否已支付, 执行原生退款操作 订单号: {} ", order.getOrderNo());
      executeBySystem(order.getId(), OrderActionType.CANCEL, null);
    }
  }

  private boolean queryResult(Map<String, String> res, Order order) {
    String status = res.get("status");
    String resultCode = res.get("result_code");

    if (StringUtils.isBlank(status) || StringUtils.isBlank(resultCode)) {
      log.error("订单接口查询失败，请求单号[" + order.getOrderNo() + "]");
      return false;
    }

    if ("0".equals(status) && "0".equals(resultCode)){
      log.info("查询订单成功, 订单号为[" + order.getOrderNo() + "]");
      if ("SUCCESS".equals(res.get("trade_state"))) {
        return true;
      } else {
        log.error("该订单未支付，order_no:" + order.getOrderNo());
        return false;
      }
    }else{//没有在易办事查询到订单
      String errorCode = res.get("err_code");
      String errorMsg = res.get("err_msg");
      if(errorCode != null){
        log.info("查询订单返回成功，resultCode返回失败，错误码[" + errorCode + "], 错误消息[" + errorMsg + "]" );
        return false;
      }
    }

    return false;
  }

  private void savePayResult(Order order, TradeType tradeType, String transactionId) {
    if (order == null || tradeType == null) {
      log.error("order或tradeType为null");
      return;
    }
    //处理outPay添加业务
    PaymentMode paymentMode = order.getPayType();
    PaymentConfig paymentConfig = paymentFacade.loadPayment(profile, paymentMode, tradeType.name());
    if (paymentConfig == null) {
      log.error("paymentConfig配置不存在，tradeType:" + tradeType);
      return;
    }
    OutPay aOp = new OutPay();
    aOp.setOutId(order.getPayType().toString());
    aOp.setpOutpayId("0"); // 父亲支付ID，用于批量打款和批量退款中的子支付记录
    aOp.setRequestId("0"); //
    aOp.setForOutpayId("0"); // 仅用于原路退回的操作，关联原始支付的OutPayId
    aOp.setOutStatus("SUCCESS"); // 支付请求的状态，这个状态不是必须的，对于支付宝就是一个字符串描述
    aOp.setOutstatusex("");  // 外部交易扩展状态
    aOp.setBillNo(order.getPayNo()); // 商户订单号
    aOp.setTradeNo(transactionId); // 第三方交易号
    aOp.setPaymentMerchantId(IdTypeHandler.encode(Long.valueOf(StringUtils.defaultIfBlank(paymentConfig.getMchId(), "0"))));
    aOp.setStatus(PaymentStatus.SUCCESS); // 系统内部的支付状态，SUBMITTED提交  FAILED支付失败  SUCCESS支付完成 CANCEL取消
    aOp.setOutpayType(PayRequestBizType.ORDER.toString());
    aOp.setOutpayTypeEx("USER_PAY"); // 支付类型，USER_PAY用户即时到账
    aOp.setAmount(order.getTotalFee());
    aOp.setUpdatedAt(new Date());

    List<OutPay> aOutPays = outPayService.listByTradeNo(order.getPayNo());
    OutPay outPay = null;
    for (OutPay pay : aOutPays) {
      if (order.getPayType().toString().equals(pay.getOutId())) {
        outPay = pay;
        aOp.setId(pay.getId());
        outPayService.updateByPrimaryKey(aOp);
        break;
      }
    }
    if (outPay == null) {
      aOp.setCreatedAt(new Date());
      outPayService.insertSelective(aOp);
    }
  }

  @Override
  public boolean saveIdentityLogOnSubmit(String orderNo, Long cpId, CareerLevelType careerType) {
    final OrderIdentityLog identityLog = new OrderIdentityLog(orderNo, cpId, careerType);
    if (!orderIdentityLogMapper.selectOrderNoExists(orderNo)) {
      return orderIdentityLogMapper.insert(identityLog) > 0;
    }
    log.warn("订单 {} 身份日志已存在", orderNo);
    return false;
  }

  @Override
  public boolean saveIdentityLogOnSubmit(String orderNo, Long cpId) {
    return saveIdentityLogOnSubmit(orderNo, cpId, customerProfileService.getCurrLevelType());
  }

  @Override
  public boolean updateIdentityLogOnPay(String orderNo, CareerLevelType careerType) {
    final OrderIdentityLog identityLog = new OrderIdentityLog();
    identityLog.setOrderNo(orderNo);
    identityLog.setOnPayIdentity(careerType);
    return orderIdentityLogMapper.updateByOrderNo(identityLog) > 0;
  }

  @Override
  public boolean updateIdentityLogOnPay(String orderNo) {
    return updateIdentityLogOnPay(orderNo, customerProfileService.getCurrLevelType());
  }

  @Override
  public List<WmsSku> selectWmsOrder(String skuId) {
    List<WmsSku> wmsSkus = orderMapper.selectWmsOrderNo(skuId);
    return wmsSkus;
  }

  @Override
  public List<String> selectWmsSku(String orderNo) {
    List<String> strings = orderMapper.selectWmsSku(orderNo);
    return strings;
  }
}
