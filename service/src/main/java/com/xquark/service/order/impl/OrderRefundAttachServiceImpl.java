package com.xquark.service.order.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.OrderRefundAttachMapper;
import com.xquark.dal.model.OrderRefundAttach;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.order.OrderRefundAttachService;
import com.xquark.service.order.OrderService;

@Service("orderRefundAttachService")
public class OrderRefundAttachServiceImpl extends BaseServiceImpl implements
    OrderRefundAttachService {

  @Autowired
  private OrderRefundAttachMapper orderRefundAttachMapper;

  @Autowired
  private OrderService orderService;

  @Override
  public int insert(OrderRefundAttach e) {
    return orderRefundAttachMapper.insert(e);
  }

  @Override
  public int insertOrder(OrderRefundAttach orderRefundAttach) {
    return orderRefundAttachMapper.insert(orderRefundAttach);
  }

  @Override
  public OrderRefundAttach load(String id) {
    return orderRefundAttachMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<OrderRefundAttach> listByRefundId(String refundId) {
    return orderRefundAttachMapper.listByRefundId(refundId);
  }

}
