package com.xquark.service.alipay;

import com.xquark.dal.model.UserAlipay;
import com.xquark.service.ArchivableEntityService;

public interface UserAlipayService extends ArchivableEntityService<UserAlipay> {

  int update(UserAlipay alipay);

  UserAlipay loadByUserId(String userId);
}
