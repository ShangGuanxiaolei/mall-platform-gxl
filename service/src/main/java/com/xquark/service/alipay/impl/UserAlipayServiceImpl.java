package com.xquark.service.alipay.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.UserAlipayMapper;
import com.xquark.dal.model.UserAlipay;
import com.xquark.service.alipay.UserAlipayService;
import com.xquark.service.common.BaseServiceImpl;

@Service("userAlipayService")
public class UserAlipayServiceImpl extends BaseServiceImpl implements UserAlipayService {

  @Autowired
  private UserAlipayMapper userAlipayMapper;

  @Override
  public int delete(String id) {
    return userAlipayMapper.updateForArchive(id);
  }

  @Override
  public int undelete(String id) {
    return userAlipayMapper.updateForUnArchive(id);
  }

  @Override
  public int insert(UserAlipay alipay) {
    return userAlipayMapper.insert(alipay);
  }

  @Override
  public int insertOrder(UserAlipay userAlipay) {
    return userAlipayMapper.insert(userAlipay);
  }

  @Override
  public UserAlipay load(String id) {
    return userAlipayMapper.selectByPrimaryKey(id);
  }

  @Override
  public UserAlipay loadByUserId(String userId) {
    return userAlipayMapper.loadByUserId(userId);
  }

  @Override
  public int update(UserAlipay alipay) {
    return userAlipayMapper.updateByPrimaryKeySelective(alipay);
  }

}
