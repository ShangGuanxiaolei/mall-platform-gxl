package com.xquark.service.wms;

import com.xquark.dal.model.WmsOrder;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: byy Date: 18-6-9. Time: 下午5:29
 */
public interface WmsOrderService {

  /**
   * 添加
   */
  boolean insert(WmsOrder obj);

  /**
   * 通过id查找
   */
  WmsOrder getByPrimaryKey(String id);

  /**
   * 更新
   */
  boolean update(WmsOrder obj);


  /**
   * 通过id删除
   */
  boolean delete(String id);

  /**
   * 批量删除
   */
  boolean batchDelete(List<String> ids);

  /**
   * 计算数据的数量
   */
  Long count(Map<String, Object> params);


  /**
   * 分页查询
   */
  List<WmsOrder> list(Pageable page, Map<String, Object> params);

  /**
   * 根据orderNo获取订单信息
   */
  WmsOrder getByOrderNo(String orderNo);

  /**
   * 通过EdiStatus查询
   */
  List<WmsOrder> getByWmsEdiStatus(Integer wmsEdiStatus);

  /**
   * 获取订单数据到Wms订单相关表中
   */
  Boolean getWmsOrderAndOthers();

  boolean syncWmsOrderInfo();

  Set<String> loadWmsOrderSet();

  @Transactional(rollbackFor = Exception.class)
  void handleWmsOrder(WmsOrder o, Set<String> wmsOrders);

  /**
   * 获取到Wms订单表
   */
  Integer getWmsOrder();

}