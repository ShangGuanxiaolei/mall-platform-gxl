package com.xquark.service.wms;

import com.xquark.dal.model.WmsOrderNtsProduct;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

/**
 * User: byy Date: 18-6-9. Time: 下午5:30
 */
public interface WmsOrderNtsProductService {

  /**
   * 添加
   */
  boolean insert(WmsOrderNtsProduct obj);

  /**
   * 通过id查找
   */
  WmsOrderNtsProduct getByPrimaryKey(String id);

  /**
   * 更新
   */
  boolean update(WmsOrderNtsProduct obj);


  /**
   * 通过id删除
   */
  boolean delete(String id);

  /**
   * 批量删除
   */
  boolean batchDelete(List<String> ids);

  /**
   * 计算数据的数量
   */
  Long count(Map<String, Object> params);


  /**
   * 分页查询
   */
  List<WmsOrderNtsProduct> list(Pageable page, Map<String, Object> params);


  WmsOrderNtsProduct getByOrderNo(String orderNo);

  /**
   * 获取Wms订单产品发货表
   * @param orderNo
   */
  Integer getWmsOrderNtsProduct(String orderNo);
}