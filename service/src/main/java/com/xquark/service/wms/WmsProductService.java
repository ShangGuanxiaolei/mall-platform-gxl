package com.xquark.service.wms;

import com.xquark.dal.model.WmsProduct;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.data.domain.Pageable;

/**
 * User: kong Date: 18-6-9. Time: 下午5:01
 */
public interface WmsProductService {

  /**
   * 添加
   */
  boolean insert(WmsProduct obj);

  /**
   * 通过id查找
   */
  WmsProduct getByPrimaryKey(String id);

  /**
   * 更新
   */
  boolean update(WmsProduct obj);


  /**
   * 通过id删除
   */
  boolean delete(String id);

  /**
   * 批量删除
   */
  boolean batchDelete(List<String> ids);

  /**
   * 计算数据的数量
   */
  Long count(Map<String, Object> params);


  /**
   * 分页查询
   */
  List<WmsProduct> list(Pageable page, Map<String, Object> params);

  /**
   * 根据code获取产品
   */
  WmsProduct getByCode(String code);

  /**
   * 获取code集合
   */
  Set<String> getCodeSet();
}