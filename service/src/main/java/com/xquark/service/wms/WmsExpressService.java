package com.xquark.service.wms;

import com.xquark.dal.model.WmsExpress;
import com.xquark.dal.vo.SFOrderLogDal;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

/**
 * User: byy Date: 18-6-9. Time: 下午5:31
 */
public interface WmsExpressService {

  /**
   * 添加
   */
  boolean insert(WmsExpress obj);

  /**
   * 通过id查找
   */
  WmsExpress getByPrimaryKey(String id);

  /**
   * 更新
   */
  boolean update(WmsExpress obj);


  /**
   * 通过id删除
   */
  boolean delete(String id);

  /**
   * 批量删除
   */
  boolean batchDelete(List<String> ids);

  /**
   * 计算数据的数量
   */
  Long count(Map<String, Object> params);


  /**
   * 分页查询
   */
  List<WmsExpress> list(Pageable page, Map<String, Object> params);


  /**
   * 根据edi状态获取
   */
  List<WmsExpress> getByEdiStatusAndStatus(Integer ediStatus, Integer status);

  List<SFOrderLogDal> getRouteByOrderNo(String orderNo);
}