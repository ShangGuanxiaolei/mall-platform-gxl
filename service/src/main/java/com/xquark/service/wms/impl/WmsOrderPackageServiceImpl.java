package com.xquark.service.wms.impl;

import com.google.common.base.Preconditions;
import com.xquark.dal.mapper.WmsOrderPackageMapper;
import com.xquark.dal.model.WmsOrderPackage;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.order.OrderService;
import com.xquark.service.wms.WmsOrderPackageService;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/** User: byy Date: 18-6-9. Time: 下午5:35 */
@Service
@Transactional
public class WmsOrderPackageServiceImpl extends BaseServiceImpl implements WmsOrderPackageService {

  @Autowired private WmsOrderPackageMapper baseMapper;

  @Autowired private OrderService orderService;

  /** 根据id查询 */
  @Override
  public WmsOrderPackage getByPrimaryKey(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.getByPrimaryKey(id);
  }

  /** 插入数据 */
  @Override
  public boolean insert(WmsOrderPackage obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.insert(obj) > 0;
  }

  /** 修改数据 */
  @Override
  public boolean update(WmsOrderPackage obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.update(obj) > 0;
  }

  @Override
  public boolean delete(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.delete(id) > 0;
  }

  /**
   * 批量删除
   *
   * @param ids id集合
   */
  @Override
  public boolean batchDelete(List<String> ids) {
    Preconditions.checkNotNull(ids, "param cant be null");
    final boolean ret = baseMapper.batchDelete(ids) > 0;
    return ret;
  }

  /** 分页查询 */
  @Override
  public List<WmsOrderPackage> list(Pageable page, Map<String, Object> params) {
    return baseMapper.list(page, params);
  }

  @Override
  public WmsOrderPackage getByOrderNo(String orderNo) {
    return baseMapper.getByOrderNo(orderNo);
  }

  @Override
  public Integer getWmsOrderPackage(String orderNo) {
    List<WmsOrderPackage> list = orderService.getWmsOrderPackage(orderNo);
    AddOrUpdate(list);
    return list.size();
  }

  /** 对分页页面列表接口取总数 */
  @Override
  public Long count(Map<String, Object> params) {
    return baseMapper.count(params);
  }

  public void AddOrUpdate(List<WmsOrderPackage> list) {
    Set<String> orders = baseMapper.getOrderNo();
    for (WmsOrderPackage w : list) {
      try {
        //        Date date = w.getCreatedAt();
        //        Date afterDate = DateUtils.addSeconds(date, 60 * 60);
        //        date = new Date();
        //        if (!date.after(afterDate)) {
        //          continue;
        //        }
        if (orders.contains(w.getOrderNo())) {
          //          baseMapper.update(w);
          continue;
        }
        baseMapper.insert(w);
      } catch (Exception e) {
        e.printStackTrace();
        System.out.println("Wms订单包装箱表订单编号" + w.getOrderNo() + "发生错误");
      }
    }
  }
}
