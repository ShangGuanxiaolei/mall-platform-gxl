package com.xquark.service.wms;

import com.xquark.dal.model.WmsInventory;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

/**
 * User: kong Date: 18-6-9. Time: 下午5:21
 */
public interface WmsInventoryService {

  /**
   * 添加
   */
  boolean insert(WmsInventory obj);

  /**
   * 通过id查找
   */
  WmsInventory getByPrimaryKey(String id);

  /**
   * 更新
   */
  boolean update(WmsInventory obj);

  /**
   * 批量更新
   */
  boolean updateByIds(String ids, int status);

  boolean updateBeforeDate(int status, Date start, String code);

  /**
   * 批量根据code更新状态
   */
  boolean updateByCode(String code, int status);


  /**
   * 通过id删除
   */
  boolean delete(String id);

  /**
   * 批量删除
   */
  boolean batchDelete(List<String> ids);

  /**
   * 计算数据的数量
   */
  Long count(Map<String, Object> params);


  /**
   * 分页查询
   */
  List<WmsInventory> list(Pageable page, Map<String, Object> params);


  /**
   * 根据edi状态查询
   */
  List<WmsInventory> getByEdiStatus(Integer ediStatus, Date start);

  void changeEdiStatusAndUpdate(List<WmsInventory> list, Date start);
}