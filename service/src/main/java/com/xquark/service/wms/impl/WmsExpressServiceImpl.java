package com.xquark.service.wms.impl;

import com.google.common.base.Preconditions;
import com.xquark.dal.mapper.WmsExpressMapper;
import com.xquark.dal.model.WmsExpress;
import com.xquark.dal.vo.SFOrderLogDal;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.wms.WmsExpressService;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: kong Date: 18-6-9. Time: 下午5:36
 */
@Service
@Transactional
public class WmsExpressServiceImpl extends BaseServiceImpl implements WmsExpressService {

  @Autowired
  private WmsExpressMapper baseMapper;

  /**
   * 根据id查询
   */
  @Override
  public WmsExpress getByPrimaryKey(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.getByPrimaryKey(id);
  }

  /**
   * 插入数据
   */
  @Override
  public boolean insert(WmsExpress obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.insert(obj) > 0;
  }

  /**
   * 修改数据
   */
  @Override
  public boolean update(WmsExpress obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.update(obj) > 0;
  }

  @Override
  public boolean delete(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.delete(id) > 0;
  }

  /**
   * 批量删除
   *
   * @param ids id集合
   */
  @Override
  public boolean batchDelete(List<String> ids) {
    Preconditions.checkNotNull(ids, "param cant be null");
    final boolean ret = baseMapper.batchDelete(ids) > 0;
    return ret;
  }


  /**
   * 分页查询
   */
  @Override
  public List<WmsExpress> list(Pageable page, Map<String, Object> params) {
    return baseMapper.list(page, params);
  }

  /**
   * 根据edi状态获取
   */
  @Override
  public List<WmsExpress> getByEdiStatusAndStatus(Integer ediStatus, Integer status) {
    List<WmsExpress> list = baseMapper.getByEdiStatusAndStatus(ediStatus, status);
    return list;
  }

  @Override
  public List<SFOrderLogDal> getRouteByOrderNo(String orderNo) {
    return baseMapper.getRouteByOrderNo(orderNo);
  }


  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long count(Map<String, Object> params) {
    return baseMapper.count(params);
  }


}
