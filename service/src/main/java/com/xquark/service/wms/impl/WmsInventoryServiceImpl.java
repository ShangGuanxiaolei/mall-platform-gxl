package com.xquark.service.wms.impl;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.mapper.WmsInventoryMapper;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.WmsInventory;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.wms.WmsInventoryService;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * User: kong Date: 18-6-9. Time: 下午5:23
 */
@Service
public class WmsInventoryServiceImpl extends BaseServiceImpl implements WmsInventoryService {

  @Autowired
  private WmsInventoryMapper baseMapper;

  @Autowired
  private SkuMapper skuMapper;

  @Autowired
  private ProductMapper productMapper;

  private static final String HW_CODE = "01";

  /**
   * 根据id查询
   */
  @Override
  public WmsInventory getByPrimaryKey(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.getByPrimaryKey(id);
  }

  /**
   * 插入数据
   */
  @Override
  public boolean insert(WmsInventory obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.insert(obj) > 0;
  }

  /**
   * 修改数据
   */
  @Override
  public boolean update(WmsInventory obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.update(obj) > 0;
  }

  @Override
  public boolean updateByIds(String ids, int status) {
    return baseMapper.updateByIds(ids, status) > 0;
  }

  @Override
  public boolean updateBeforeDate(int status, Date start, String code) {
    return baseMapper.updateBeforeDate(status, code, start) > 0;
  }

  @Override
  public boolean updateByCode(String code, int status) {
    return baseMapper.updateByCode(code, status) > 0;
  }

  @Override
  public boolean delete(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.delete(id) > 0;
  }

  /**
   * 批量删除
   *
   * @param ids id集合
   */
  @Override
  public boolean batchDelete(List<String> ids) {
    Preconditions.checkNotNull(ids, "param cant be null");
    final boolean ret = baseMapper.batchDelete(ids) > 0;
    return ret;
  }


  /**
   * 分页查询
   */
  @Override
  public List<WmsInventory> list(Pageable page, Map<String, Object> params) {
    return baseMapper.list(page, params);
  }

  @Override
  public List<WmsInventory> getByEdiStatus(Integer ediStatus, Date start) {
    return baseMapper.getByEdiStatus(ediStatus, start);
  }


  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long count(Map<String, Object> params) {
    return baseMapper.count(params);
  }

  @Override
  public void changeEdiStatusAndUpdate(List<WmsInventory> list, Date start) {
    for (WmsInventory w : list) {
      try {
        Optional<Sku> skuOptional = Optional.fromNullable(skuMapper.selectBySkuCode(w.getCode()));
        if (skuOptional.isPresent()) {
          String code = productMapper.getSupplierCodeByIdHandler(skuOptional.get().getProductId());
          if (HW_CODE.equals(code)) {
            int status = 3;
            try {
              skuMapper.updateByCode(w);
              productMapper.updateProductAmountByMaxSkuAmount(skuOptional.get().getProductId());
            } catch (Exception e) {
              status = 2;
              log.error("code {} edi状态更新出错", code, e);
            } finally {
              w.setEdiStatus(status);
              // 批量更新edi状态
              this.updateBeforeDate(status, start, w.getCode());
            }
          }
        }
      } catch (Exception e) {
        log.error("库存编码发生错误:{},trace:{}", w.getCode(), e);
      }
    }
  }


}
