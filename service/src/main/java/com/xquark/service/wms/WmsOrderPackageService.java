package com.xquark.service.wms;

import com.xquark.dal.model.WmsOrderPackage;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

/**
 * User: byy Date: 18-6-9. Time: 下午5:30
 */
public interface WmsOrderPackageService {

  /**
   * 添加
   */
  boolean insert(WmsOrderPackage obj);

  /**
   * 通过id查找
   */
  WmsOrderPackage getByPrimaryKey(String id);

  /**
   * 更新
   */
  boolean update(WmsOrderPackage obj);


  /**
   * 通过id删除
   */
  boolean delete(String id);

  /**
   * 批量删除
   */
  boolean batchDelete(List<String> ids);

  /**
   * 计算数据的数量
   */
  Long count(Map<String, Object> params);


  /**
   * 分页查询
   */
  List<WmsOrderPackage> list(Pageable page, Map<String, Object> params);


  /**
   * 根据orderNo查询
   */
  WmsOrderPackage getByOrderNo(String orderNo);

  /**
   * 获取到Wms订单包装表
   * @param orderNo
   */
  Integer getWmsOrderPackage(String orderNo);
}