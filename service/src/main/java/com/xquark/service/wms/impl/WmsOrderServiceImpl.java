package com.xquark.service.wms.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.xquark.dal.mapper.WmsOrderMapper;
import com.xquark.dal.model.WmsOrder;
import com.xquark.dal.type.PromotionType;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.order.OrderService;
import com.xquark.service.wms.WmsOrderNtsProductService;
import com.xquark.service.wms.WmsOrderPackageService;
import com.xquark.service.wms.WmsOrderService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;

/** User: byy Date: 18-6-9. Time: 下午5:33 */
@Service
@Transactional
public class WmsOrderServiceImpl extends BaseServiceImpl implements WmsOrderService {

  @Autowired private WmsOrderMapper wmsOrderMapper;

  @Autowired private OrderService orderService;

  @Autowired private WmsOrderPackageService wmsOrderPackageService;

  @Autowired private WmsOrderNtsProductService wmsOrderNtsProductService;

  private final static Set<String> SPECIAL_TYPE = ImmutableSet.of(
      PromotionType.FLASHSALE.name(),
      PromotionType.SALEZONE.name(),
      PromotionType.PIECE.name(),
      PromotionType.FRESHMAN.name(),
      PromotionType.PRODUCT_SALES.name(),
      PromotionType.MULTIPLE.name()

  );

  private Logger log = LoggerFactory.getLogger(getClass());

  /** 根据id查询 */
  @Override
  public WmsOrder getByPrimaryKey(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return wmsOrderMapper.getByPrimaryKey(id);
  }

  /** 插入数据 */
  @Override
  public boolean insert(WmsOrder obj) {
    Preconditions.checkNotNull(obj);
    return wmsOrderMapper.insert(obj) > 0;
  }

  /** 修改数据 */
  @Override
  public boolean update(WmsOrder obj) {
    Preconditions.checkNotNull(obj);
    return wmsOrderMapper.update(obj) > 0;
  }

  @Override
  public boolean delete(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return wmsOrderMapper.delete(id) > 0;
  }

  /**
   * 批量删除
   *
   * @param ids id集合
   */
  @Override
  public boolean batchDelete(List<String> ids) {
    Preconditions.checkNotNull(ids, "param cant be null");
    final boolean ret = wmsOrderMapper.batchDelete(ids) > 0;
    return ret;
  }

  /** 分页查询 */
  @Override
  public List<WmsOrder> list(Pageable page, Map<String, Object> params) {
    return wmsOrderMapper.list(page, params);
  }

  /** 根据orderNo获取订单信息 */
  @Override
  public WmsOrder getByOrderNo(String orderNo) {
    return wmsOrderMapper.getByOrderNo(orderNo);
  }

  @Override
  public List<WmsOrder> getByWmsEdiStatus(Integer ediStatus) {
    return wmsOrderMapper.getByWmsEdiStatus(ediStatus);
  }

  @Override
  public Boolean getWmsOrderAndOthers() {
    StringBuffer info = new StringBuffer();
    Integer num1 = getWmsOrder();
    Integer num2 = wmsOrderNtsProductService.getWmsOrderNtsProduct(null);
    Integer num3 = wmsOrderPackageService.getWmsOrderPackage(null);
    info.append("Wms订单表一共获取数据" + num1 + "条，进货单表" + num2 + "条，包装箱表" + num3 + "条");
    log.info(info.toString());
    return num1 > 0 && num2 > 0 & num3 > 0;
  }

  @Override
  public boolean syncWmsOrderInfo() {
    List<WmsOrder> wmsOrders = orderService.getWmsOrder();
    Set<String> orderNos = wmsOrderMapper.getOrderNo();
    log.info("开始同步wms订单数据");
    for (WmsOrder o : wmsOrders) {
      handleWmsOrder(o, orderNos);
    }
    log.info("开始同步wms订单数据");
    return false;
  }

  @Override
  public Set<String> loadWmsOrderSet() {
    return wmsOrderMapper.getOrderNo();
  }

  /**
   * 处理单个wms订单
   */
  @Override
  @Transactional(rollbackFor = Exception.class, isolation = Isolation.READ_COMMITTED)
  public void handleWmsOrder(WmsOrder o, Set<String> wmsOrders) {
    if (SPECIAL_TYPE.contains(o.getOrderType())) {
      o.setType(o.getOrderType());
    }
    String orderNo = o.getOrderNo();
    try {
      if (wmsOrders.contains(o.getOrderNo())) {
        log.info("订单 {} 在wms中已存在, 跳过执行", orderNo);
        return;
      }
      log.info("开始同步订单 {} ", orderNo);
      // 初始化到0
      o.setName("");//在edi库支持表情前暂时不插入用户名称
      wmsOrderMapper.insert(o);

      log.info("开始同步订单 {} 的orderItem", orderNo);
      // 执行orderItem
      int itemNum = wmsOrderNtsProductService.getWmsOrderNtsProduct(o.getOrderNo());
      log.info("订单 {} orderItem同步完成, 共处理 {} 条", orderNo, itemNum);

      log.info("开始同步订单 {} 的package", orderNo);
      // 执行package
      int packageNum = wmsOrderPackageService.getWmsOrderPackage(o.getOrderNo());
      log.info("订单 {} package同步完成, 共处理 {} 条", orderNo, packageNum);
    } catch (Exception e) {
      log.error("获取Wms订单表编号:{},trace:{}", o.getOrderNo(), e);
      throw new RuntimeException("订单 " + orderNo + " wms状态同步出错");
    }
  }

  @Override
  public Integer getWmsOrder() {
    List<WmsOrder> list = orderService.getWmsOrder();
    AddOrUpdate(list);
    return list.size();
  }

  /** 对分页页面列表接口取总数 */
  @Override
  public Long count(Map<String, Object> params) {
    return wmsOrderMapper.count(params);
  }

  public void AddOrUpdate(List<WmsOrder> list) {

    Set<String> orderNos = wmsOrderMapper.getOrderNo();
    for (WmsOrder w : list) {
      if ("PIECE".equals(w.getOrderType())) {
        w.setType(w.getOrderType());
      }
      try {
        if (orderNos.contains(w.getOrderNo())) {
          continue;
        } else {
          wmsOrderMapper.insert(w);
        }
        w.setLocalEdiStatus(1);
        wmsOrderMapper.update(w);
      } catch (Exception e) {
        log.error("获取Wms订单表编号:{},trace:{}", w.getOrderNo(), e);
      }
    }
  }
}
