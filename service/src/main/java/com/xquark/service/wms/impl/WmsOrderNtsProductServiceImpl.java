package com.xquark.service.wms.impl;

import com.google.common.base.Preconditions;
import com.xquark.dal.mapper.SkuCombineMapper;
import com.xquark.dal.mapper.WmsOrderNtsProductMapper;
import com.xquark.dal.model.WmsOrderNtsProduct;
import com.xquark.dal.type.PromotionType;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.order.OrderService;
import com.xquark.service.wms.WmsOrderNtsProductService;

import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/** User: byy Date: 18-6-9. Time: 下午5:34 */
@Service
@Transactional
public class WmsOrderNtsProductServiceImpl extends BaseServiceImpl
    implements WmsOrderNtsProductService {

  @Autowired private WmsOrderNtsProductMapper baseMapper;

  @Autowired private OrderService orderService;
  @Autowired private SkuCombineMapper skuCombineMapper;
  /** 根据id查询 */
  @Override
  public WmsOrderNtsProduct getByPrimaryKey(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.getByPrimaryKey(id);
  }

  /** 插入数据 */
  @Override
  public boolean insert(WmsOrderNtsProduct obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.insert(obj) > 0;
  }

  /** 修改数据 */
  @Override
  public boolean update(WmsOrderNtsProduct obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.update(obj) > 0;
  }

  @Override
  public boolean delete(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.delete(id) > 0;
  }

  /**
   * 批量删除
   *
   * @param ids id集合
   */
  @Override
  public boolean batchDelete(List<String> ids) {
    Preconditions.checkNotNull(ids, "param cant be null");
    final boolean ret = baseMapper.batchDelete(ids) > 0;
    return ret;
  }

  /** 分页查询 */
  @Override
  public List<WmsOrderNtsProduct> list(Pageable page, Map<String, Object> params) {
    return baseMapper.list(page, params);
  }

  @Override
  public WmsOrderNtsProduct getByOrderNo(String orderNo) {
    return baseMapper.getByOrderNo(orderNo);
  }

  @Override
  public Integer getWmsOrderNtsProduct(String orderNo) {
    List<WmsOrderNtsProduct> list = orderService.getWmsOrderNtsProduct(orderNo);
    // 将订单中的PIECE_GROUP类型转换为PIECE类型，给WMS推送的是PIECE类型
    for (WmsOrderNtsProduct wmsOrder : list) {
      if (PromotionType.PIECE_GROUP.name().equals(wmsOrder.getType())) {
        wmsOrder.setType(PromotionType.PIECE.name());
      }
    }
    // 过滤掉赠品，将赠品的数量增加到主商品的数量上
    List<WmsOrderNtsProduct> prmotionfilter = prmotionfilter(list);
    AddAndUpdate(prmotionfilter);
    return prmotionfilter.size();
  }

  /**
   * 过滤掉赠品，将赠品的数量增加到主商品的数量上
   *
   * @param wmsOrderNtsProductList
   * @return
   */
  List<WmsOrderNtsProduct> prmotionfilter(List<WmsOrderNtsProduct> wmsOrderNtsProductList) {
    List<WmsOrderNtsProduct> result = new ArrayList<>();
    Map<String, WmsOrderNtsProduct> temp = new HashMap<>();
    // 默認主商品在赠品之前
    for (WmsOrderNtsProduct wmsOrderNtsProduct : wmsOrderNtsProductList) {
      if (PromotionType.MEETING.toString().equals(wmsOrderNtsProduct.getType())) {
        temp.put(
            wmsOrderNtsProduct.getOrderNo() + wmsOrderNtsProduct.getStockSku(), wmsOrderNtsProduct);
        result.add(wmsOrderNtsProduct);
      } else if (PromotionType.MEETING_GIFT.toString().equals(wmsOrderNtsProduct.getType())) {
        WmsOrderNtsProduct amount =
            temp.get(wmsOrderNtsProduct.getOrderNo() + wmsOrderNtsProduct.getStockSku());
        if (null != amount) {
          amount.setAmount(amount.getAmount() + wmsOrderNtsProduct.getAmount());
          // 如果是套裝商品， 數量 主商品與贈品累加
          if (null != amount.getIsCombination() && 1 == amount.getIsCombination()) {
            amount.setCombinationNum(
                amount.getCombinationNum() + wmsOrderNtsProduct.getCombinationNum());
          }
        }
      } else {
        result.add(wmsOrderNtsProduct);
      }
    }
    // 单品数量 与 相同单品订单行的数量累加
    Map<String, WmsOrderNtsProduct> countTemp = new HashMap<>();
    for (WmsOrderNtsProduct wmsOrderProduct : result) {
      WmsOrderNtsProduct wmsOrderTemp =
          countTemp.get(wmsOrderProduct.getOrderNo() + wmsOrderProduct.getStockSku());
      if (null != wmsOrderTemp) {
        wmsOrderProduct.setAmount(wmsOrderProduct.getAmount() + wmsOrderTemp.getAmount());
      }
      countTemp.put(wmsOrderProduct.getOrderNo() + wmsOrderProduct.getStockSku(), wmsOrderProduct);
    }
    List<WmsOrderNtsProduct> responseResult = new ArrayList<WmsOrderNtsProduct>(countTemp.values());

//    //合并一品多价的skuCode
//    List<WmsOrderNtsProduct> skuCoderesult = new ArrayList<>();
//    Map<String, WmsOrderNtsProduct> skuCodetemp = new HashMap<>();
//
//    for (WmsOrderNtsProduct wmsOrderNtsProduct : responseResult) {
//      if(null==skuCodetemp.get(wmsOrderNtsProduct.getOrderNo() + wmsOrderNtsProduct.getStockSku())){
//        skuCodetemp.put(wmsOrderNtsProduct.getOrderNo() + wmsOrderNtsProduct.getStockSku(), wmsOrderNtsProduct);
//        skuCoderesult.add(wmsOrderNtsProduct);
//      }else{
//        WmsOrderNtsProduct wmsOrderProductSkuCode = skuCodetemp.get(wmsOrderNtsProduct.getOrderNo() + wmsOrderNtsProduct.getStockSku());
//        wmsOrderProductSkuCode.setAmount(wmsOrderProductSkuCode.getAmount()+wmsOrderNtsProduct.getAmount());
//      }
//    }
    return responseResult;
  }

  /** 对分页页面列表接口取总数 */
  @Override
  public Long count(Map<String, Object> params) {
    return baseMapper.count(params);
  }

  public void AddAndUpdate(List<WmsOrderNtsProduct> list) {
    for (WmsOrderNtsProduct w : list) {
      try {
        // FIXME 特殊处理多sku
        w.setProductName(w.getName());
        long count = baseMapper.countByOrderNoAndSku(w.getOrderNo(), w.getStockSku());
        if (count > 0) {
          //          baseMapper.update(w);
          continue;
        }
        log.info(
            "insert no:{},name:{},sku:{},count:{}",
            w.getOrderNo(),
            w.getProductName(),
            w.getStockSku(),
            count);
        baseMapper.insert(w);
      } catch (Exception e) {
        log.error("Wms订单发货表订单编号:{},trace:{}", w.getOrderNo(), e);
      }
    }
  }
}
