package com.xquark.service.wms.impl;

import com.xquark.dal.mapper.WmsProductMapper;
import com.xquark.dal.model.WmsProduct;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.wms.WmsProductService;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.parboiled.common.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: kong Date: 18-6-9. Time: 下午5:06
 */
@Service
@Transactional
public class WmsProductServiceImpl extends BaseServiceImpl implements WmsProductService {

  @Autowired
  private WmsProductMapper baseMapper;

  /**
   * 根据id查询
   */
  @Override
  public WmsProduct getByPrimaryKey(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.getByPrimaryKey(id);
  }

  /**
   * 插入数据
   */
  @Override
  public boolean insert(WmsProduct obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.insert(obj) > 0;
  }

  /**
   * 修改数据
   */
  @Override
  public boolean update(WmsProduct obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.update(obj) > 0;
  }

  @Override
  public boolean delete(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.delete(id) > 0;
  }

  /**
   * 批量删除
   *
   * @param ids id集合
   */
  @Override
  public boolean batchDelete(List<String> ids) {
    Preconditions.checkNotNull(ids, "param cant be null");
    final boolean ret = baseMapper.batchDelete(ids) > 0;
    return ret;
  }


  /**
   * 分页查询
   */
  @Override
  public List<WmsProduct> list(Pageable page, Map<String, Object> params) {
    return baseMapper.list(page, params);
  }

  @Override
  public WmsProduct getByCode(String code) {
    WmsProduct wmsProduct = baseMapper.getByCode(code);
    return wmsProduct;
  }

  @Override
  public Set<String> getCodeSet() {
    return baseMapper.getCodeSet();
  }


  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long count(Map<String, Object> params) {
    return baseMapper.count(params);
  }


}
