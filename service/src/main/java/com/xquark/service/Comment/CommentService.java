package com.xquark.service.Comment;

import com.xquark.dal.model.Comment;
import com.xquark.dal.model.CommentLike;
import com.xquark.dal.model.CommentReply;
import com.xquark.dal.type.CommentType;
import com.xquark.dal.vo.CommentManagment;
import com.xquark.dal.vo.CommentVO;
import com.xquark.dal.vo.ReplyManagment;
import com.xquark.service.BaseEntityService;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

/**
 * Created by jitre on 11/9/17. 评论的服务接口
 */
public interface CommentService extends BaseEntityService<Comment> {

  /**
   * 添加评论
   */
  public int addComment(Comment comment);

  /**
   * 批量添加评论
   */
  public boolean addComments(List<Comment> comments);

  /**
   * 通过id删除评论
   */
  public void deleteCommentById(String id);


  /**
   * 通过id更新一条评论
   */
  public void updateCommentById(Comment comment);


  /***
   * 通过id获取评论
   * @param id
   */
  public Comment getCommentById(String id);


  /**
   * 获取界面上的评论数据
   *
   * @param objId 评论对象的id
   * @param start 哪一条开始(不包括这条)
   * @param offset 偏移量
   */
  public List<CommentVO> getCommentsVO(String objId, String userId, int start, int offset);

  /**
   * 添加评论回复
   */
  public int addCommentReply(CommentReply reply);

  /**
   * 通过id删除回复
   */
  public void deleteCommentReplyById(String id);


  /**
   * 修改回复
   */
  public void updateCommentReplyById(CommentReply reply);


  /***
   * 通过id获取回复
   * @param id
   */
  public CommentReply getCommentReplyById(String id);


  /**
   * 点赞一个评论
   */
  public int likeComment(CommentLike like);


  /**
   * 取消对评论的点赞
   */
  public int cancelLikeByCommentIdAndUserId(CommentLike like);


  /**
   * 屏蔽评论
   */
  public void blockComment(String id);

  /**
   * 解除对评论的屏蔽
   */
  public void unblockComment(String id);


  /**
   * 屏蔽回复
   */
  public void blockReply(String id);


  /**
   * 解除对评论的屏蔽
   */
  public void unblockReply(String id);

  /**
   * 获取后台使用的评论数据
   */

  public List<CommentManagment> getCommentList(CommentType type, Boolean blocked,
      Boolean descByTime, Pageable pageable);

  /**
   * 获取后台使用的评论数据的条数
   */

  public Integer getCommentCount(CommentType type, Boolean blocked, Boolean descByTime,
      Pageable pageable);

  /**
   * 获取后台使用的回复数据
   */
  public List<ReplyManagment> getReplysList(String commentId);


  /**
   * 获取后台使用的回复数据的条数
   */
  public Integer getReplysCount(String commentId);


  /**
   * 批量删除评论数据
   */
  public void batchDeleteComment(String[] ids);

  /**
   * 批量屏蔽评论
   */
  public void batchBlockComment(String[] ids);

  /**
   * 批量撤销对评论的屏蔽
   */
  public void batchUnBlockComment(String[] ids);

  List<CommentVO> listCommentByProductIdAndStar(String productId, Integer star,
      Pageable pageable);

  Map<String, Long> commentTotalMap(String productId);

  Long countCommentByProductIdAndStar(String productId, Integer star);

}
