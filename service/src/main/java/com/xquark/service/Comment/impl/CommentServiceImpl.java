package com.xquark.service.Comment.impl;

import com.xquark.dal.mapper.CommentLikeMapper;
import com.xquark.dal.mapper.CommentMapper;
import com.xquark.dal.mapper.CommentReplyMapper;
import com.xquark.dal.model.Comment;
import com.xquark.dal.model.CommentLike;
import com.xquark.dal.model.CommentLikeExample;
import com.xquark.dal.model.CommentReply;
import com.xquark.dal.model.CommentReplyExample;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.CommentType;
import com.xquark.dal.vo.CommentManagment;
import com.xquark.dal.vo.CommentVO;
import com.xquark.dal.vo.ReplyManagment;
import com.xquark.service.Comment.CommentService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by jitre on 11/9/17.
 */
@Service("commentService")
@Transactional
public class CommentServiceImpl extends BaseServiceImpl implements CommentService {

  private final static Map<String, Long> USELESS_COMMENT_MAP;

  static {
    USELESS_COMMENT_MAP = new LinkedHashMap<>();
    USELESS_COMMENT_MAP.put("total", 0L);
    USELESS_COMMENT_MAP.put("one_star", 0L);
    USELESS_COMMENT_MAP.put("two_star", 0L);
    USELESS_COMMENT_MAP.put("three_star", 0L);
    USELESS_COMMENT_MAP.put("four_star", 0L);
    USELESS_COMMENT_MAP.put("five_star", 0L);
  }

  @Autowired
  private CommentMapper commentMapper;

  @Autowired
  private CommentReplyMapper commentReplyMapper;

  @Autowired
  private CommentLikeMapper commentLikeMapper;


  /**
   * 添加评论
   */
  @Override
  public int addComment(Comment comment) {
    int ret = commentMapper.insert(comment);
    comment.setId(IdTypeHandler.encode(Long.valueOf(comment.getId())));

    return ret;
  }

  @Override
  @Transactional
  public boolean addComments(List<Comment> comments) {
    for (Comment comment : comments) {
      if (this.addComment(comment) <= 0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "评论保存失败");
      }
    }
    return true;
  }

  /**
   * 删除评论
   */
  @Override
  public void deleteCommentById(String id) {
    commentMapper.deleteByPrimaryKey(id);

    //同时删除所有点赞
    CommentLikeExample likeExample = new CommentLikeExample();
    likeExample.createCriteria().andCommentIdEqualTo(IdTypeHandler.decode(id));
    commentLikeMapper.deleteByExample(likeExample);

    //删除所有回复
    CommentReplyExample replyExample = new CommentReplyExample();
    replyExample.createCriteria().andCommentIdEqualTo(IdTypeHandler.decode(id));
  }


  /**
   * 更新一条评论
   */
  @Override
  public void updateCommentById(Comment comment) {
    commentMapper.updateByPrimaryKeySelective(comment);
  }


  /***
   * 通过id获取评论
   * @param id
   */
  @Override
  public Comment getCommentById(String id) {
    return commentMapper.selectByPrimaryKey(id);
  }

  /**
   * 获取界面上的评论数据
   *
   * @param objId 评论对象的id
   * @param start 哪一条开始(不包括这条)
   * @param offset 偏移量
   */
  @Override
  public List<CommentVO> getCommentsVO(String objId, String userId, int start, int offset) {
    return commentMapper.getCommentVOS(objId, userId, start, offset);
  }

  /**
   * 添加评论回复
   */
  @Override
  public int addCommentReply(CommentReply reply) {
    int ret = commentReplyMapper.insert(reply);
    reply.setId(IdTypeHandler.encode(Long.valueOf(reply.getId())));
    return ret;
  }

  /**
   * 通过id删除回复
   */
  @Override
  public void deleteCommentReplyById(String id) {
    commentReplyMapper.deleteByPrimaryKey(id);
  }


  /**
   * 通过replyId修改回复
   */
  @Override
  public void updateCommentReplyById(CommentReply reply) {
    commentReplyMapper.updateByPrimaryKeySelective(reply);
  }

  /***
   * 通过id获取回复

   * @param id
   */
  @Override
  public CommentReply getCommentReplyById(String id) {
    return commentReplyMapper.selectByPrimaryKey(id);
  }

  /**
   * 点赞一个评论
   *
   * @return 1表示成功,-1表示失败,点赞了无法再次点赞
   */
  @Override
  public int likeComment(CommentLike like) {
    //构造动态sql
    CommentLikeExample example = new CommentLikeExample();
    example.createCriteria().andCommentIdEqualTo(IdTypeHandler.decode(like.getCommentId()))
        .andUserIdEqualTo(IdTypeHandler.decode(like.getUserId()));

    //检查用户是否已经点赞
    List<CommentLike> commentLikes = commentLikeMapper.selectByExample(example);
    if (commentLikes.size() >= 1) {
      return -1;
    }

    //评论的总点赞数＋１
    commentMapper.plusLikeCount(like.getCommentId());

    //添加点赞的数据
    commentLikeMapper.insert(like);

    return 1;
  }

  /**
   * 取消对评论的点赞
   *
   * @return 1表示成功,-1表示失败,还没点赞无法取消点赞
   */
  @Override
  public int cancelLikeByCommentIdAndUserId(CommentLike like) {

    //构造动态sql
    CommentLikeExample example = new CommentLikeExample();
    example.createCriteria().andCommentIdEqualTo(IdTypeHandler.decode(like.getCommentId()))
        .andUserIdEqualTo(IdTypeHandler.decode(like.getUserId()));

    //检查用户是否已经点赞
    List<CommentLike> commentLikes = commentLikeMapper.selectByExample(example);
    if (commentLikes.size() == 0) {
      return -1;
    }

    //评论的总点赞数减一
    commentMapper.subtractLikeCount(like.getCommentId());

    //文档化点赞的记录
    example.clear();
    example.createCriteria().andCommentIdEqualTo(IdTypeHandler.decode(like.getCommentId()))
        .andUserIdEqualTo(IdTypeHandler.decode(like.getUserId()));
    commentLikeMapper.deleteByExample(example);

    return 1;
  }

  /**
   * 屏蔽评论
   */
  @Override
  public void blockComment(String id) {
    commentMapper.blockComment(id, 1);
  }

  /**
   * 解除对评论的屏蔽
   */
  @Override
  public void unblockComment(String id) {
    commentMapper.blockComment(id, 0);
  }

  /**
   * 屏蔽回复
   */
  @Override
  public void blockReply(String id) {
    commentReplyMapper.blockReply(id, 1);
  }

  /**
   * 解除对评论的屏蔽
   */
  @Override
  public void unblockReply(String id) {
    commentReplyMapper.blockReply(id, 0);
  }

  /**
   * 获取后台使用的评论数据
   *
   * @param pageable 分页
   */
  @Override
  public List<CommentManagment> getCommentList(CommentType type, Boolean blocked,
      Boolean descByTime, Pageable pageable) {

    return commentMapper.getCommentList(type, blocked, descByTime, pageable);
  }

  /**
   * 获取后台使用的评论数据的条数
   */
  @Override
  public Integer getCommentCount(CommentType type, Boolean blocked, Boolean descByTime,
      Pageable pageable) {
    return commentMapper.getCommentCount(type, blocked, descByTime, pageable);
  }

  /**
   * 获取后台使用的回复数据
   */
  @Override
  public List<ReplyManagment> getReplysList(String commentId) {
    return commentReplyMapper.getReplysList(commentId);
  }

  /**
   * 获取后台使用的回复数据的条数
   */
  @Override
  public Integer getReplysCount(String commentId) {
    return commentReplyMapper.getReplysCount(commentId);
  }

  /**
   * 批量删除评论数据
   */
  @Override
  public void batchDeleteComment(String[] ids) {
    for (String id : ids) {
      deleteCommentById(id);
    }
  }

  /**
   * 批量屏蔽评论
   */
  @Override
  public void batchBlockComment(String[] ids) {
    for (String id : ids) {
      blockComment(id);
    }
  }

  /**
   * 批量撤销对评论的屏蔽
   */
  @Override
  public void batchUnBlockComment(String[] ids) {
    for (String id : ids) {
      unblockComment(id);
    }
  }

  @Override
  public List<CommentVO> listCommentByProductIdAndStar(String productId, Integer star,
      Pageable pageable) {
    return commentMapper.listCommentByProductIdAndStar(productId, star, pageable);
  }

  @Override
  public Map<String, Long> commentTotalMap(String productId) {
    return USELESS_COMMENT_MAP;
  }

  @Override
  public Long countCommentByProductIdAndStar(String productId, Integer star) {
    return commentMapper.countCommentByProductIdAndStar(productId, star);
  }

  /**
   * 检查默认的not null，和default value等约束要求，
   */
  @Override
  public int insert(Comment comment) {
    return this.addComment(comment);
  }

  @Override
  public int insertOrder(Comment comment) {
    return this.addComment(comment);
  }

  @Override
  public Comment load(String id) {
    return this.getCommentById(id);
  }

}
