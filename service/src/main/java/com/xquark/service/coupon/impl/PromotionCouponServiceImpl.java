package com.xquark.service.coupon.impl;


import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.xquark.dal.mapper.PromotionCouponMapper;
import com.xquark.dal.mapper.TermRelationshipMapper;
import com.xquark.dal.mapper.UserCouponMapper;
import com.xquark.dal.model.UserCoupon;
import com.xquark.dal.model.promotion.Coupon;
import com.xquark.dal.status.CouponStatus;
import com.xquark.dal.type.UserCouponType;
import com.xquark.dal.vo.BasePromotionCouponVO;
import com.xquark.dal.vo.CategoryVO;
import com.xquark.dal.vo.PromotionCouponVo;
import com.xquark.service.cart.vo.CartPromotionInfo;
import com.xquark.service.category.CategoryService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by  on 15-11-5.
 */
@Service("promotionCouponService")
public class PromotionCouponServiceImpl extends BaseServiceImpl implements PromotionCouponService {

    @Autowired
    private PromotionCouponMapper promotionCouponMapper;
    @Autowired
    private UserCouponMapper userCouponMapper;
    @Autowired
    private TermRelationshipMapper termRelationshipMapper;

    @Autowired
    private CategoryService categoryService;

    @Override
    public PromotionCouponVo load(String id) {
        //转换原有时间格式和数据格式
        PromotionCouponVo pcVo = promotionCouponMapper.selectByPrimaryKey(id);
        if (pcVo != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
            pcVo.setStrValidFrom(simpleDateFormat.format(pcVo.getValidFrom()));
            pcVo.setStrValidTo(simpleDateFormat.format(pcVo.getValidTo()));

            pcVo.setIntApplyAbove(pcVo.getApplyAbove().intValue());
            pcVo.setIntDiscount(pcVo.getDiscount().intValue());
        }
        return pcVo;
    }

    @Override
    public Long countPromotionCoupon(Map<String, Object> params) {
        return promotionCouponMapper.countPromotionCoupon(params);
    }

    @Override
    public int create(Map<String, Object> params) {
        return promotionCouponMapper.insert(params);
    }

    @Override
    public List<PromotionCouponVo> list(Map<String, Object> params, Pageable pageable) {
        return promotionCouponMapper.listPromotionCoupon(params, pageable);
    }

    /**
     * 查询基础优惠券信息
     */
    @Override
    public List<BasePromotionCouponVO> listBaseCouponVO(Map<String, ?> params, String order,
                                                        Direction direction, Pageable pageable) {
        order = StringUtils.defaultIfEmpty(order, "created_at");
        direction = Optional.fromNullable(direction).or(Direction.DESC);
        return promotionCouponMapper.listBasePromotionCouponVO(params, order, direction, pageable);
    }

    @Override
    @Transactional
    public int modifyPromotionCoupon(Map<String, Object> params, String id) {
        String status = null;
        if (params != null) {
            status = params.get("status").toString();
        }
        if (status != null && (CouponStatus.CLOSED.toString().equals(status) || CouponStatus.OVERDUE
                .toString().equals(status))) {
            userCouponMapper
                    .closeByPromotionId(id, status, new String[]{CouponStatus.VALID.toString()});
        }
        return promotionCouponMapper.modifyPromotionCoupon(params, id);
    }


    @Override
    public List<PromotionCouponVo> loadShopPromotionCoupon(String shopId, String status) {

        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(shopId)) {
            params.put("shopId", shopId);
        }
        if (StringUtils.isNotBlank(status)) {
            params.put("status", status);
        }
        List<PromotionCouponVo> couponList = promotionCouponMapper.loadShopPromotionCoupon(params);
        //转换原有时间格式和数据格式
        SimpleDateFormat simpleDateFormat = null;
        if (couponList != null) {
            if (simpleDateFormat == null) {
                simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
            }
            for (PromotionCouponVo pcVo : couponList) {
                pcVo.setStrValidFrom(simpleDateFormat.format(pcVo.getValidFrom()));
                pcVo.setStrValidTo(simpleDateFormat.format(pcVo.getValidTo()));

                pcVo.setIntApplyAbove(pcVo.getApplyAbove().intValue());
                pcVo.setIntDiscount(pcVo.getDiscount().intValue());
            }
        }
        return couponList;
    }

    @Override
    public Long countPromotionCouponByShopId(Map<String, Object> params) {
        return promotionCouponMapper.countCouponByShopId(params);
    }

    @Override
    public List<PromotionCouponVo> listPromotionCouponByShopId(Map<String, Object> params,
                                                               Pageable pageable) {

        List<PromotionCouponVo> couponList = promotionCouponMapper
                .listCouponByShopId(params, pageable);
        //转换原有时间格式和数据格式
        SimpleDateFormat simpleDateFormat = null;
        if (couponList != null) {
            if (simpleDateFormat == null) {
                simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
            }
            for (PromotionCouponVo pcVo : couponList) {
                pcVo.setStrValidFrom(simpleDateFormat.format(pcVo.getValidFrom()));
                pcVo.setStrValidTo(simpleDateFormat.format(pcVo.getValidTo()));

                pcVo.setIntApplyAbove(pcVo.getApplyAbove().intValue());
                pcVo.setIntDiscount(pcVo.getDiscount().intValue());
            }
        }
        return couponList;
    }

    @Override
    public List<BasePromotionCouponVO> listBasePromotionCoupon(Map<String, ?> params, String order,
                                                               Direction direction, Pageable pageable) {
        return promotionCouponMapper.listBasePromotionCouponVO(params, order, direction, pageable);
    }


    public List<BasePromotionCouponVO> listBasePromotionCouponByProduct2(final String productId, String userId,
                                                                        String order, Direction direction, Pageable pageable) {
        Map<String, ?> params = ImmutableMap.of("status", CouponStatus.VALID,
                "useNumber", 0, "flag", "gt");
        List<BasePromotionCouponVO> couponList = this.listBasePromotionCoupon(params, "amount",
                Direction.DESC, null);
        // 去掉不符合条件的优惠券
        CollectionUtils.filter(couponList,
                new Predicate<BasePromotionCouponVO>() {
                    @Override
                    public boolean evaluate(BasePromotionCouponVO object) {
                        UserCouponType scope = object.getScope();
                        // 数据异常逻辑
                        if (scope == null) {
                            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "优惠券数据未初始化");
                        }
                        CategoryVO category = categoryService.loadCategoryByProductId(productId);
                        String categoryId = null;
                        if (category != null) {
                            categoryId = category.getId();
                        }
                        // 比较数据库中商品、分类, 过滤掉不符合条件的优惠券
                        return scope.inScope(object, productId, categoryId);
                    }
                });
        for (BasePromotionCouponVO coupon : couponList) {
            int acquireLimit = coupon.getAcquireLimit();
            Long acquiredCoupon = userCouponMapper.countAcquirePromotionCoupons(userId, coupon.getId());
            // 达到领取上限后则设置为已领取
            coupon.setAcquiredNum(acquiredCoupon);
        }
        return couponList;
    }

    @Override
    public List<BasePromotionCouponVO> listBasePromotionCouponByProduct(final String productId,
                                                                        String order, Direction direction, Pageable pageable) {
        Map<String, ?> params = ImmutableMap.of("status", CouponStatus.VALID,
                "useNumber", 0, "flag", "gt");
        String userId = getCurrentUser().getId();
        List<BasePromotionCouponVO> couponList = this.listBasePromotionCoupon(params, "amount",
                Direction.DESC, null);
        // 去掉不符合条件的优惠券
        CollectionUtils.filter(couponList,
                new Predicate<BasePromotionCouponVO>() {
                    @Override
                    public boolean evaluate(BasePromotionCouponVO object) {
                        UserCouponType scope = object.getScope();
                        // 数据异常逻辑
                        if (scope == null) {
                            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "优惠券数据未初始化");
                        }
                        CategoryVO category = categoryService.loadCategoryByProductId(productId);
                        String categoryId = null;
                        if (category != null) {
                            categoryId = category.getId();
                        }
                        // 比较数据库中商品、分类, 过滤掉不符合条件的优惠券
                        return scope.inScope(object, productId, categoryId);
                    }
                });
        for (BasePromotionCouponVO coupon : couponList) {
            int acquireLimit = coupon.getAcquireLimit();
            Long acquiredCoupon = userCouponMapper.countAcquirePromotionCoupons(userId, coupon.getId());
            // 达到领取上限后则设置为已领取
            coupon.setAcquiredNum(acquiredCoupon);
        }
        return couponList;
    }

    @Override
    public List<BasePromotionCouponVO> listBasePromotionCouponByProduct(String productId) {
        return listBasePromotionCouponByProduct(productId, "amount",
                Direction.DESC, null);
    }

    @Override
    public List<BasePromotionCouponVO> listBasePromotionCouponByProductAndUserId(String productId,String userId) {
        return listBasePromotionCouponByProduct2(productId, userId,"amount",
                Direction.DESC, null);
    }

    @Override
    public Long selectRemainder(String promotionCouponId) {
        return promotionCouponMapper.selectRemainder(promotionCouponId);
    }

    @Override
    @Transactional
    public int autoClosePromotionCoupon() {

        List<Coupon> coupons = promotionCouponMapper.selectShouldCloseCoupon();
        if (coupons != null) {
            for (Coupon coupon : coupons) {
                promotionCouponMapper.closeCoupon(coupon.getId());
                userCouponMapper.closeByPromotionId(coupon.getId(), CouponStatus.OVERDUE.toString(),
                        new String[]{CouponStatus.VALID.toString()});
            }
            return coupons.size();
        }

        return 0;
    }

    /**
     * 確定订单结算后更新优惠券使用数量
     *
     * @param couponIds 优惠券ID
     * @return 修改行数
     */
    @Transactional
    @Override
    public int updateUsedCount(String... couponIds) {
        if (couponIds == null || ArrayUtils.isEmpty(couponIds)) {
            return 0;
        }
        int nUpdate = 0;
        for (String cpId : couponIds) {
            UserCoupon cp = userCouponMapper.selectByPrimaryKey(cpId);
            //更新优惠券状态为已使用
            cp.setStatus(CouponStatus.USED);
            cp.setAcquiredAt(new Date());
            userCouponMapper.updateUserCouponById(cp);
            //更新优惠券使用数量
            int n = promotionCouponMapper.updateUsedCountById(cp.getCouponId(), 1);
            nUpdate += n;
        }
        return nUpdate;
    }

    /**
     * 查询当前可用的优惠券，供web端调用
     */
    @Override
    public List<PromotionCouponVo> listForApp(Pageable pageable, String shopId) {
        return promotionCouponMapper.listForApp(pageable, shopId);
    }

    /**
     * 优惠券作废后更新使用该优惠券的未支付订单
     */
    @Override
    public int updateOrderDiscount(String couponId, BigDecimal discount) {
        return promotionCouponMapper.updateOrderDiscount(couponId, discount);
    }

    /**
     * 优惠券作废后更新使用该优惠券的未支付主订单
     */
    @Override
    public int updateMainOrderDiscount(String couponId, BigDecimal discount) {
        return promotionCouponMapper.updateMainOrderDiscount(couponId, discount);
    }

    @Override
    public boolean inPromotion(String productId, String excludePromotionId, String shopId) {
        List<String> categoryIds = termRelationshipMapper.getCategoryByProduct(productId);
        List<Coupon> selectShopCoupon = promotionCouponMapper.selectShopCoupon(shopId);
        boolean flag = false;
        // 找到该店铺内能使用到的优惠券
        for (Coupon coupon : selectShopCoupon) {
            String pId = coupon.getProductId();
            String cId = coupon.getCategoryId();
            // 如果该优惠券所有商品，所有品类都能使用，则该商品在优惠券优惠中
            if (StringUtils.isEmpty(pId) && StringUtils.isEmpty(cId)) {
                flag = true;
            }
            // 如果该优惠券指定了商品可用，则判断指定的商品是否包含该商品
            else if (StringUtils.isNotEmpty(pId)) {
                if (pId.indexOf(productId) != -1) {
                    flag = true;
                }
            }
            // 如果该优惠券指定了商品分类可用，则判断指定的商品分类是否包含该商品的分类
            else if (StringUtils.isNotEmpty(cId)) {
                for (String categoryId : categoryIds) {
                    if (cId.indexOf(categoryId) != -1) {
                        flag = true;
                    }
                }
            }
        }
        return flag;
    }

    @Override
    public void changeStatus(CartPromotionInfo info, String productId, String shopId) {
        info.setInCoupon(this.inPromotion(productId, null, shopId));
    }

}

