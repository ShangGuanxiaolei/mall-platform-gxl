package com.xquark.service.coupon;

import com.xquark.dal.model.UserCoupon;
import com.xquark.dal.vo.UserCouponVo;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

/**
 * Created by  on 15-11-5.
 */
public interface UserCouponService {

  List<UserCouponVo> list(Map<String, Object> params, Pageable pageable);

  Long countPromotionCoupons(String[] statusArr, String promotionCouponId);

  Long countUserCoupons(Map<String, Object> params);

  List<UserCoupon> selectCouponListByUserId(String uId);

  Long countAcquirePromotionCoupons(String userId, String promotionCouponId);

  int acquireCoupon(UserCoupon userCoupon);

  List<UserCouponVo> validUserCouponList(String userId, String shopId,
      String rootShopId, List<String> productIds);

  int updateUserCouponById(UserCoupon userCoupon);

  UserCoupon load(String couponId);

  List<UserCouponVo> selectMyCouponList(String userId, String phone, String shopId);

  List<UserCouponVo> selectMyCouponListPage(Map params, Pageable pageable, String[] statusArr);

  Long updateStatusByCouponId(String promotionCouponId, String status);

}
