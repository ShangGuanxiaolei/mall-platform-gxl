package com.xquark.service.coupon.impl;

import com.google.common.base.Optional;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.mapper.PromotionCouponMapper;
import com.xquark.dal.mapper.UserCouponMapper;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.UserCoupon;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.CouponStatus;
import com.xquark.dal.vo.UserCouponVo;
import com.xquark.service.coupon.UserCouponService;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by  on 15-11-5.
 */
@Service("userCouponServiceImpl")
public class UserCouponServiceImpl implements UserCouponService {

  @Autowired
  private UserCouponMapper userCouponMapper;

  @Autowired
  private PromotionCouponMapper promotionCouponMapper;

  @Override
  public Long countPromotionCoupons(String[] statusArr, String promotionCouponId) {
    return userCouponMapper.countPromotionCoupons(statusArr, promotionCouponId);
  }

  @Override
  public Long countUserCoupons(Map<String, Object> params) {
    return userCouponMapper.countUserCoupons(params);
  }

  @Override
  public List<UserCouponVo> list(Map<String, Object> params, Pageable pageable) {
    return userCouponMapper.listUserCoupon(params, pageable);
  }

  @Override
  public List<UserCoupon> selectCouponListByUserId(String uId) {
    return userCouponMapper.selectCouponListByUserId(uId);
  }

  @Override
  public Long countAcquirePromotionCoupons(String userId, String promotionCouponId) {
    return userCouponMapper.countAcquirePromotionCoupons(userId, promotionCouponId);
  }

  @Autowired
  private ProductMapper productMapper;

  @Override
  @Transactional
  public int acquireCoupon(UserCoupon userCoupon) {
    int result = userCouponMapper.insert(userCoupon);
    userCouponMapper
        .addCode(userCoupon.getId(), IdTypeHandler.encode(new Integer(userCoupon.getId())));
    if (result > 0) {
      promotionCouponMapper.updateRemainderById(1, userCoupon.getCouponId());
    }
    return result;
  }

  /**
   * 获取可用的用户优惠 （用户ID,电话号码二选一）
   *
   * @param userId 用户ID
   * @param shopId 店铺ID
   * @param rootShopId 总店铺ID
   * @return 可用的用户优惠卷列表
   */
  @Override
  public List<UserCouponVo> validUserCouponList(String userId, String shopId,
      String rootShopId, List<String> productIds) {

    Map<String, Object> params = new HashMap<String, Object>();
    if (StringUtils.isNotBlank(userId)) {
      params.put("uId", userId);
    }
    /**if (StringUtils.isNotBlank(phone)){
     params.put("phone",phone);
     }**/
    if (StringUtils.isNotBlank(shopId)) {
      params.put("shopId", shopId);
    }
    // 加入总店id过滤条件，如果优惠券是发放到总店，则全店铺均可用
    if (StringUtils.isNotBlank(rootShopId)) {
      params.put("rootShopId", rootShopId);
    }
    params.put("status", CouponStatus.VALID);
    params.put("productIds", productIds);
    List<UserCouponVo> couponList = userCouponMapper.selectValidCouponList(params);

    SimpleDateFormat simpleDateFormat = null;
    if (couponList != null) {
      if (simpleDateFormat == null) {
        simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
      }
      for (UserCouponVo ucVo : couponList) {
        ucVo.setStrValidFrom(simpleDateFormat.format(ucVo.getValidFrom()));
        ucVo.setStrValidTo(simpleDateFormat.format(ucVo.getValidTo()));

        ucVo.setIntApplyAbove(ucVo.getApplyAbove().intValue());
        ucVo.setIntDiscount(ucVo.getDiscount().intValue());

        // 如果查询出总店的优惠券，需要将总店的shopid设置为该小店对应的shopid，因为前端判断都是以小店的shopid对应取优惠信息
        if (ucVo.getShopId().equals(rootShopId)) {
          ucVo.setShopId(shopId);
        }
      }
    }
    return couponList;
  }

  /**
   * 获取用户可以使用的最优优惠券
   */
  public Optional<UserCouponVo> calculateBestOfferCoupon(String userId, String shopId,
      String rootShopId,
      List<String> productIds) {
    List<UserCouponVo> coupons = this.validUserCouponList(userId, shopId, rootShopId, productIds);
    if (CollectionUtils.isEmpty(coupons)) {
      return Optional.absent();
    }
    UserCouponVo bestOfferCoupon;
    // TODO wangxinhua 重构
    return Optional.absent();
  }

  @Override
  public List<UserCouponVo> selectMyCouponList(String userId, String phone, String shopId) {
    Map<String, Object> params = new HashMap<String, Object>();
    if (StringUtils.isNotBlank(userId)) {
      params.put("uId", userId);
    }
    if (StringUtils.isNotBlank(phone)) {
      params.put("phone", phone);
    }
    if (StringUtils.isNotBlank(shopId)) {
      params.put("shopId", shopId);
    }
    String[] statusArr = new String[]{CouponStatus.VALID.toString(), CouponStatus.USED.toString(),
        CouponStatus.OVERDUE.toString(), CouponStatus.CLOSED.toString()};
    List<UserCouponVo> couponList = userCouponMapper.selectUserCouponList(params, statusArr);
    // 优惠券商品支持多选，因此商品名称，商品图片需要做特殊处理
    for (UserCouponVo vo : couponList) {
      String productIds = vo.getProductId();
      String[] ids = productIds.split(",");
      StringBuffer productName = new StringBuffer();
      String productImg = "";
      for (String id : ids) {
        Product product = productMapper.selectByPrimaryKey(id);
        if (product != null) {
          productName.append(product.getName());
          productImg = product.getImg();
        }
      }
      vo.setProductImg(productImg);
      vo.setProductName(productName.toString());
    }

    SimpleDateFormat simpleDateFormat = null;
    if (couponList != null) {
      if (simpleDateFormat == null) {
        simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
      }
      for (UserCouponVo ucVo : couponList) {
        ucVo.setStrValidFrom(simpleDateFormat.format(ucVo.getValidFrom()));
        ucVo.setStrValidTo(simpleDateFormat.format(ucVo.getValidTo()));

        ucVo.setIntApplyAbove(ucVo.getApplyAbove().intValue());
        ucVo.setIntDiscount(ucVo.getDiscount().intValue());
      }
    }
    return couponList;
  }

  @Override
  public List<UserCouponVo> selectMyCouponListPage(Map params, Pageable pageable,
      String[] statusArr) {

    List<UserCouponVo> couponList = userCouponMapper
        .selectUserCouponListPage(pageable, params, statusArr);
    // 优惠券商品支持多选，因此商品名称，商品图片需要做特殊处理
    for (UserCouponVo vo : couponList) {
      String productIds = vo.getProductId();
      if (StringUtils.isNotEmpty(productIds)) {
        String[] ids = productIds.split(",");
        StringBuffer productName = new StringBuffer();
        String productImg = "";
        for (String id : ids) {
          Product product = productMapper.selectByPrimaryKey(id);
          if (product != null) {
            productName.append(product.getName());
            productImg = product.getImg();
          }
        }
        vo.setProductImg(productImg);
        vo.setProductName(productName.toString());
      }
    }
    SimpleDateFormat simpleDateFormat = null;
    if (couponList != null) {
      if (simpleDateFormat == null) {
        simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
      }
      for (UserCouponVo ucVo : couponList) {
        ucVo.setStrValidFrom(simpleDateFormat.format(ucVo.getValidFrom()));
        ucVo.setStrValidTo(simpleDateFormat.format(ucVo.getValidTo()));

        ucVo.setIntApplyAbove(ucVo.getApplyAbove().intValue());
        ucVo.setIntDiscount(ucVo.getDiscount().intValue());
      }
    }
    return couponList;
  }

  /**
   * 更新用户优惠券数据
   *
   * @param userCoupon 结合具体业务逻辑设置相应的值
   * @return 返回修改后的行数
   */
  public int updateUserCouponById(UserCoupon userCoupon) {
    return userCouponMapper.updateUserCouponById(userCoupon);
  }

  /**
   * 通过CouponId获取优惠券
   *
   * @param couponId 用户优惠券ID
   * @return 用户优惠券
   */
  @Override
  public UserCoupon load(String couponId) {
    return userCouponMapper.selectByPrimaryKey(couponId);
  }

  @Override
  public Long updateStatusByCouponId(String promotionCouponId, String status) {
    return userCouponMapper.updateStatusByCouponId(promotionCouponId, status);
  }

}
