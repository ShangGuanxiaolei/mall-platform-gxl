package com.xquark.service.coupon;

import com.xquark.dal.vo.BasePromotionCouponVO;
import com.xquark.dal.vo.PromotionCouponVo;
import com.xquark.service.product.ProductPromotion;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

/**
 * Created by  on 15-11-5.
 */
public interface PromotionCouponService extends ProductPromotion {

  PromotionCouponVo load(String id);

  int create(Map<String, Object> params);

  List<PromotionCouponVo> list(Map<String, Object> params, Pageable pageable);

  List<BasePromotionCouponVO> listBaseCouponVO(Map<String, ?> params, String order,
      Direction direction, Pageable pageable);

  int modifyPromotionCoupon(Map<String, Object> params, String id);

  Long countPromotionCoupon(Map<String, Object> params);

  List<PromotionCouponVo> loadShopPromotionCoupon(String shopId, String status);

  Long selectRemainder(String promotionCouponId);

  int autoClosePromotionCoupon();

  int updateUsedCount(String... couponIds);

  Long countPromotionCouponByShopId(Map<String, Object> params);

  List<PromotionCouponVo> listPromotionCouponByShopId(Map<String, Object> params,
      Pageable pageable);

  List<BasePromotionCouponVO> listBasePromotionCoupon(Map<String, ?> params, String order,
      Direction direction, Pageable pageable);

  List<BasePromotionCouponVO> listBasePromotionCouponByProduct(String productId, String order,
      Direction direction, Pageable pageable);

  List<BasePromotionCouponVO> listBasePromotionCouponByProduct(String productId);

  List<BasePromotionCouponVO> listBasePromotionCouponByProductAndUserId(String productId,String userId);

  List<PromotionCouponVo> listForApp(Pageable pageable, String shopId);

  /**
   * 优惠券作废后更新使用该优惠券的未支付订单
   */
  int updateOrderDiscount(String couponId, BigDecimal discount);

  /**
   * 优惠券作废后更新使用该优惠券的未支付订单
   */
  int updateMainOrderDiscount(String couponId, BigDecimal discount);
}
