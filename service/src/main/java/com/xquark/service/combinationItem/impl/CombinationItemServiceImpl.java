package com.xquark.service.combinationItem.impl;

import com.google.common.base.Preconditions;
import com.xquark.dal.mapper.CombinationItemMapper;
import com.xquark.dal.model.CombinationItem;
import com.xquark.service.combinationItem.CombinationItemService;
import com.xquark.service.common.BaseServiceImpl;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: byy Date: 18-6-1. Time: 上午9:27 组合商品明细服务层实现类
 */
@Service
@Transactional
public class CombinationItemServiceImpl extends BaseServiceImpl implements CombinationItemService {

  @Autowired
  private CombinationItemMapper baseMapper;

  /**
   * 根据id查询
   */
  @Override
  public CombinationItem getByPrimaryKey(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.getByPrimaryKey(id);
  }

  /**
   * 插入数据
   */
  @Override
  public boolean insert(CombinationItem obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.insert(obj) > 0;
  }

  /**
   * 修改数据
   */
  @Override
  public boolean update(CombinationItem obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.update(obj) > 0;
  }

  @Override
  public boolean delete(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.delete(id) > 0;
  }

  /**
   * 批量删除
   *
   * @param ids id集合
   */
  @Override
  public boolean batchDelete(List<String> ids) {
    Preconditions.checkNotNull(ids, "param cant be null");
    final boolean ret = baseMapper.batchDelete(ids) > 0;
    return ret;
  }


  /**
   * 分页查询
   */
  @Override
  public List<CombinationItem> list(Pageable page, Map<String, Object> params) {
    return baseMapper.list(page, params);
  }


  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long count(Map<String, Object> params) {
    return baseMapper.count(params);
  }


}
