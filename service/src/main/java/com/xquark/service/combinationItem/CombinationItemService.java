package com.xquark.service.combinationItem;

import com.xquark.dal.model.CombinationItem;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

/**
 * User: byy Date: 18-6-1. Time: 上午9:25 组合商品明细服务层
 */
public interface CombinationItemService {

  /**
   * 添加
   */
  boolean insert(CombinationItem obj);

  /**
   * 通过id查找
   */
  CombinationItem getByPrimaryKey(String id);

  /**
   * 更新
   */
  boolean update(CombinationItem obj);


  /**
   * 通过id删除
   */
  boolean delete(String id);

  /**
   * 批量删除
   */
  boolean batchDelete(List<String> ids);

  /**
   * 计算数据的数量
   */
  Long count(Map<String, Object> params);


  /**
   * 分页查询
   */
  List<CombinationItem> list(Pageable page, Map<String, Object> params);


}