package com.xquark.service.merchantSupplier;

import com.xquark.aop.anno.NotNull;
import com.xquark.dal.mapper.MerchantSupplierMapper;
import com.xquark.dal.model.MerchantSupplier;
import com.xquark.service.base.imp.TemplateBaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: huangjie Date: 2018/7/2. Time: 下午8:43
 */
@Service
@Transactional
public class MerchantSupplierServiceImpl extends TemplateBaseServiceImpl<MerchantSupplier,
    MerchantSupplierMapper> implements  MerchantSupplierService {

  private MerchantSupplierMapper mapper;
  @Autowired
  public void setMerchantSupplierMapper(MerchantSupplierMapper mapper) {
      this.mapper = mapper;
  }

  @Override
  protected MerchantSupplierMapper getMapper() {
    return mapper;
  }

  @Override
  public MerchantSupplier getByMerchantId(@NotNull String id) {
    return mapper.getByMerchantId(id);
  }
}
