package com.xquark.service.merchantSupplier;

import com.xquark.dal.model.MerchantSupplier;
import com.xquark.service.base.TemplateBaseService;

/**
 * Created by IntelliJ IDEA. User: huangjie Date: 2018/7/2. Time: 下午8:56 ${DESCRIPTION}
 */
public interface MerchantSupplierService extends TemplateBaseService<MerchantSupplier>{

  MerchantSupplier getByMerchantId(String id);
}
