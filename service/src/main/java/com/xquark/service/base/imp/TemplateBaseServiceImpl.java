package com.xquark.service.base.imp;

import com.xquark.dal.BaseEntity;
import com.xquark.dal.mapper.BaseMapper;
import com.xquark.service.base.TemplateBaseService;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * User: huangjie Date: 2018/7/2. Time: 下午8:31 模板service接口的实现
 */
public abstract class TemplateBaseServiceImpl <T extends BaseEntity,M extends BaseMapper<T>>  implements TemplateBaseService<T> {

  /***
   * hook,调用子类依赖的mapper
   * @return
   */
  abstract protected M getMapper();

  @Override
  public boolean delete(@NotBlank  String id) {
    return getMapper().delete(id) > 0;
  }

  @Override
  public boolean batchDelete(@NotNull @NotEmpty List<String> ids) {
    return getMapper().batchDelete(ids)>0;
  }

  @Override
  public T getByPrimaryKey(@NotNull String id) {
    return getMapper().getByPrimaryKey(id);
  }

  @Override
  public Long count(Map<String, Object> params) {
    return getMapper().count(params);
  }

  @Override
  public List<? extends T> list(Map<String, Object> params) {
    return getMapper().list(params);
  }

  @Override
  public Boolean insert(@NotNull  T record) {
    return getMapper().insert(record) > 0;
  }

  @Override
  public Boolean update(@NotNull T record) {
    return getMapper().update(record) > 0;
  }
}
