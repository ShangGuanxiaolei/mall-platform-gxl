package com.xquark.service.base;

import com.xquark.dal.mapper.PromotionListMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

/**
 * @author wangxinhua
 * @date 2019-04-29
 * @since 1.0
 */
public abstract class RainLotteryJobService {

    protected PromotionListMapper promotionListMapper;

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public void setPromotionListMapper(PromotionListMapper promotionListMapper) {
        this.promotionListMapper = promotionListMapper;
    }

    /**
     * 今天是否有活动配置
     *
     * @param type 活动类型
     */
    public boolean isNotActivity(Integer type) {
        boolean ret = promotionListMapper.havePromotionToday(type);
        if (!ret) {
            log.info("未找到今日[{}]活动配置跳过...", LocalDateTime.now());
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

}
