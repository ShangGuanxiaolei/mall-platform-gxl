package com.xquark.service.base;

import com.xquark.dal.BaseEntity;
import java.util.List;
import java.util.Map;

/**
 * User: huangjie Date: 2018/6/26. Time: 下午6:50
 */
public interface TemplateBaseService<T extends BaseEntity> {

  boolean delete(String id);

  boolean batchDelete(List<String> ids);

  T getByPrimaryKey(String id);

  Long count(Map<String, Object> params);

  List<? extends T> list(Map<String, Object> params);

  Boolean insert(T record);

  Boolean update(T record);
}
