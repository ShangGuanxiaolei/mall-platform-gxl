package com.xquark.service.mypiece;

import com.xquark.dal.model.Address;
import com.xquark.dal.model.MainOrder;
import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.model.Shop;
import com.xquark.dal.type.PromotionType;
import com.xquark.service.BaseEntityService;
import com.xquark.service.pricing.vo.UserSelectedProVO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author gx
 */
public interface PieceMainOrderByCartService extends BaseEntityService<MainOrder> {

  MainOrder submitBySkuIds2(List<String> skuIds, OrderAddress oa, Map<String, String> shopRemarks,
                            UserSelectedProVO userSelectedProVO,
                            PromotionType userSelectedPromotion, Address address,
                            Boolean piece, Shop shop,
                            String orderType,
                            Boolean useDeduction, Boolean needInvoice, BigDecimal selectPoint,
                            BigDecimal selectCommission, Long upLine, String pCode);

}
