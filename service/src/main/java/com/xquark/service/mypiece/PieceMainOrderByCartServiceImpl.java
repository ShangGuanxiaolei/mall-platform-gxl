package com.xquark.service.mypiece;

import com.xquark.dal.consts.PointConstants;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.status.MainOrderStatus;
import com.xquark.dal.type.*;
import com.xquark.dal.vo.UserCouponVo;
import com.xquark.service.address.AddressService;
import com.xquark.service.cart.CartService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.domain.DomainService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderAddressService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.OrderSplitterService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.pricing.impl.pricing.DiscountPricingResult;
import com.xquark.service.pricing.impl.pricing.SkuDiscountDetail;
import com.xquark.service.pricing.vo.ConfirmOrderPromotionVO;
import com.xquark.service.pricing.vo.PricingResultVO;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductSkuVO;
import com.xquark.service.promotion.OrderItemPromotionService;
import com.xquark.service.yundou.PointCommDeductionResult;
import com.xquark.utils.UniqueNoUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.Map.Entry;

import static com.xquark.service.pricing.impl.pricing.DiscountPricingResult.*;

/**
 * @author gx
 */
@Service("pieceMainOrderByCartService")
public class PieceMainOrderByCartServiceImpl extends PieceMainOrderServiceImpl implements PieceMainOrderByCartService {

  @Autowired
  private MainOrderMapper mainOrderMapper;

  @Autowired
  private OrderMapper orderMapper;

  @Autowired
  private OrderService orderService;

  @Autowired
  private CartService cartService;

  @Autowired
  private ProductService productService;

  @Autowired
  private PromotionService promotionService;

  @Autowired
  private DomainService domainService;

  @Autowired
  private OrderAddressService orderAddressService;

  @Autowired
  private AddressService addressService;

  @Autowired
  private OrderItemMapper orderItemMapper;

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private OrderSplitterService orderSplitterService;

  @Autowired
  private OrderItemPromotionService orderItemPromotionService;

  @Autowired
  private CustomerProfileService customerProfileService;

  @Autowired
  private OrderDetailMapper orderDetailMapper;

  @Autowired
  private OrderHeaderMapper orderHeaderMapper;

  @Autowired
  private SkuMapper skuMapper;

  @Autowired
  private RecordPromotionPgTranInfoMapper recordPromotionPgTranInfoMapper;
  @Autowired
  private PromotionPgMemberInfoMapper promotionPgMemberInfoMapper;

  @Value("${sf.api.token.profile}")
  private String profile;

  @Value("${sf.api.host.url}")
  private String host;

  @Override
  public int insert(MainOrder mainOrder) {
    mainOrder.setOrderNo(UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.ES));
    mainOrder.setStatus(MainOrderStatus.SUBMITTED);
    log.error(mainOrder.getOrderNo() + " ****ES****");
    return mainOrderMapper.insert(mainOrder);
  }

  @Override
  public MainOrder load(String id) {
    return null;
  }

  @Transactional
  public void addOrder(List<OrderDetail> orderDetails, List<OrderHeader> orderHeaders) {
    orderDetailMapper.batchInsert(orderDetails);
    orderHeaderMapper.batchInsert(orderHeaders);
  }


  Domain loadDomain(User user) {
    Domain domain = null;
    if (StringUtils.isNotBlank(user.getPartner())) {
      domain = domainService.loadByCode(user.getPartner());
    } else {
      domain = domainService.loadRootDomain();
    }

    if (domain == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "用户错误 partner=[" + user.getPartner() + "]");
    }
    return domain;
  }
}
