package com.xquark.service.mypiece;

import com.xquark.dal.model.*;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.service.BaseEntityService;
import com.xquark.service.pricing.vo.UserSelectedProVO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author gx
 */
public interface PieceMainOrderService extends BaseEntityService<MainOrder> {

  MainOrderVO loadVO(String orderId);

  void pay(String orderNo, PaymentMode mode, String payNo);

  MainOrder submitBySkuId2(String skuId, OrderAddress oa, String remark,
                          UserSelectedProVO userSelectedProVO, Address address, Boolean piece, int qty,
                          Shop shop, String orderType, boolean useDeduction,
                          PromotionType userSelectedPromotion, String isPrivilege, boolean needInvoice,
                          BigDecimal selectPoint, BigDecimal selectCommission, Long upLine, String pCode,Integer pgDiscount
                          ,Integer isGroupHead);
  MainOrder submitBySkuIds2(List<String> skuIds, OrderAddress oa, Map<String, String> shopRemarks,
                            UserSelectedProVO userSelectedProVO,
                            PromotionType userSelectedPromotion, Address address,
                            Boolean piece, Shop shop,
                            String orderType,
                            Boolean useDeduction, Boolean needInvoice, BigDecimal selectPoint,
                            BigDecimal selectCommission, Long upLine, String pCode);
  void updatePiecePayStatus(MainOrderVO order,PaymentMode paymentMode,String bizNo);

  /**
   * 拼团订单生效
   * @param pCode
   * @param pTranCode
   */
  public void effectOrders (String pCode, String pTranCode);

  boolean hadDoubleDiscount (User user, String pTranCode);

//  MainOrder submitBySkuId1(String skuId, OrderAddress oa, String remark,
//                          UserSelectedProVO userSelectedProVO, Address address, Boolean danbao, int qty,
//                          boolean useDeduction, String pCode);
//
//  MainOrder submitBySkuIds1(List<String> skuIds, OrderAddress oa, Map<String, String> shopRemarks,
//                           UserSelectedProVO userSelectedProVO, PromotionType userSelectedPromotion,
//                           Address address, Boolean danbao,
//                           Boolean useDeduction, String pCode);
//  MainOrder submitBySkuId3(String skuId, OrderAddress oa, String remark,
//                          UserSelectedProVO userSelectedProVO, Address address, Boolean piece, int qty,
//                          Shop shop, String orderType, Boolean useDeduction, String pCode);


//  List<String> getOrderNoByMain(String orderNo);
//  void updateStatusByOrderNo(String orderNo, MainOrderStatus status);
//
//    MainOrder queryMainOrderByPayNo(String bizNo);
}
