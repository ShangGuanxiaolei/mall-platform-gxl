package com.xquark.service.mypiece;

import com.xquark.dal.model.mypiece.PromotionTempStock;
import com.xquark.dal.model.mypiece.PromotionTempStockVo;
import com.xquark.dal.model.mypiece.StockCheckBean;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author gx
 * @date 2018-09-04
 */
@Service
public class StockCheckService {
  private static Logger logger = LoggerFactory.getLogger(StockCheckService.class);
  @Autowired
  private PieceStockCheckService stockCheckService;

  /**
   * 库存检查,针对拼团根据skuCode查询库存
   *
   * @param stockCheckBean stockCheckBean
   * @return List<PromotionTempStockVo>
   */
  public List<PromotionTempStockVo> checkStockForPiece(StockCheckBean stockCheckBean) {
    List<PromotionTempStockVo> promotionTempStockVo = null;
    if (null != stockCheckBean) {
      PromotionTempStock promotionTempStock = new PromotionTempStock();
      //sku编码
      if (StringUtils.isNoneBlank(stockCheckBean.getSku_code())) {
        promotionTempStock.setSku_code(stockCheckBean.getSku_code());
      }
      promotionTempStock.setIs_deleted(1);
      promotionTempStockVo = stockCheckService.findStock(promotionTempStock);
    }
    return promotionTempStockVo;
  }

  /**
   * 库存扣减
   *
   * @param stockCheckBean stockCheckBean
   * @return boolean
   */
  public boolean deduceStock(StockCheckBean stockCheckBean) {
    List<PromotionTempStockVo> promotionTempStockVoList = checkStock(stockCheckBean);
    if (promotionTempStockVoList.size() == 0) {
      return false;
    }
    for (PromotionTempStockVo promotionTempStockVo :
            promotionTempStockVoList) {
      if (promotionTempStockVo.getP_usable_sku_num() < stockCheckBean.getP_sku_num()) {
        logger.info("商品编码为" + promotionTempStockVo.getSku_code() + "的商品库存不足");
        return false;
      } else {
        PromotionTempStock promotionTempStock = new PromotionTempStock();
        //活动编码
        promotionTempStock.setP_code(stockCheckBean.getP_code());
        //sku编码
        promotionTempStock.setSku_code(stockCheckBean.getSku_code());
        promotionTempStock.setIs_deleted(1);
        promotionTempStock.setP_usable_sku_num(stockCheckBean.getResume_sku_num());
        logger.info("活动库存扣减");
        stockCheckService.updateStock(promotionTempStock);
      }
    }
    return true;
  }

  /**
   * 库存检查
   *
   * @param stockCheckBean stockCheckBean
   * @return List<PromotionTempStockVo>
   */
  public List<PromotionTempStockVo> checkStock(StockCheckBean stockCheckBean) {
    List<PromotionTempStockVo> promotionTempStockVo = null;
    if (null != stockCheckBean) {
      PromotionTempStock promotionTempStock = new PromotionTempStock();
      //活动编码
      if (StringUtils.isNoneBlank(stockCheckBean.getP_code())) {
        promotionTempStock.setP_code(stockCheckBean.getP_code());
      }
      //sku编码
      if (StringUtils.isNoneBlank(stockCheckBean.getSku_code())) {
        promotionTempStock.setSku_code(stockCheckBean.getSku_code());
      }
      promotionTempStock.setIs_deleted(1);
      logger.info("根据活动编码和sku编码查询库存");
      promotionTempStockVo = stockCheckService.findStock(promotionTempStock);
    }
    return promotionTempStockVo;
  }

}
