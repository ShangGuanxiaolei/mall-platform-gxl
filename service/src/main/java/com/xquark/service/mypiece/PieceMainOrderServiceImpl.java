package com.xquark.service.mypiece;

import com.google.common.collect.Lists;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.dal.status.MainOrderStatus;
import com.xquark.dal.status.OrderHeaderStatusMapping;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.*;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.PromotionPgMemberInfoVO;
import com.xquark.dal.vo.UserCouponVo;
import com.xquark.event.MainOrderActionEvent;
import com.xquark.service.address.AddressService;
import com.xquark.service.bill.impl.bw.FieldConstants;
import com.xquark.service.cantuan.JoinStatusService;
import com.xquark.service.cantuan.PieceGroupPayCallBackService;
import com.xquark.service.cart.CartService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.domain.DomainService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.msgcore.MessageReciver;
import com.xquark.service.msgcore.ReciverMessage;
import com.xquark.service.order.OrderAddressService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.OrderSplitterService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.pricing.impl.pricing.DiscountPricingResult;
import com.xquark.service.pricing.impl.pricing.PricingMode;
import com.xquark.service.pricing.impl.pricing.SkuDiscountDetail;
import com.xquark.service.pricing.vo.ConfirmOrderPromotionVO;
import com.xquark.service.pricing.vo.PricingResultVO;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductSkuVO;
import com.xquark.service.promotion.OrderItemPromotionService;
import com.xquark.utils.DateUtils;
import com.xquark.utils.UniqueNoUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import static com.xquark.service.pricing.impl.pricing.DiscountPricingResult.DEVIDED_CUTDOWN_DETAIL_KEY;
import static com.xquark.service.pricing.impl.pricing.DiscountPricingResult.USING_PROMOTION_KEY;

/**
 * @author gx
 */
@Service("pieceMainOrderService")
public class PieceMainOrderServiceImpl extends BaseServiceImpl implements PieceMainOrderService {

  @Autowired
  private MainOrderMapper mainOrderMapper;

  @Autowired
  private OrderMapper orderMapper;

  @Autowired
  private OrderService orderService;

  @Autowired
  private CartService cartService;

  @Autowired
  private ProductService productService;

  @Autowired
  private PromotionService promotionService;

  @Autowired
  private DomainService domainService;

  @Autowired
  private OrderAddressService orderAddressService;

  @Autowired
  private AddressService addressService;

  @Autowired
  private OrderItemMapper orderItemMapper;

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private OrderSplitterService orderSplitterService;

  @Autowired
  private OrderItemPromotionService orderItemPromotionService;

  @Autowired
  private CustomerProfileService customerProfileService;

  @Autowired
  private OrderDetailMapper orderDetailMapper;

  @Autowired
  private OrderHeaderMapper orderHeaderMapper;


  @Autowired
  private SkuMapper skuMapper;

  @Autowired
  private RecordPromotionPgTranInfoMapper recordPromotionPgTranInfoMapper;

  @Autowired
  private JoinStatusService joinStatusService;

  @Autowired
  private JugleIsGroupMasterService jugleIsGroupMasterService;

  @Autowired
  private PieceGroupPayCallBackService pieceGroupPayCallBackService;

  @Autowired
  private PromotionPgMemberInfoMapper promotionPgMemberInfoMapper;

  @Autowired
  private PromotionRewardsService promotionRewardsService;

  @Value("${sf.api.token.profile}")
  private String profile;

  @Value("${sf.api.host.url}")
  private String host;

  @Autowired
  private MessageReciver messageReciver;

  @Override
  public int insert(MainOrder mainOrder) {
    mainOrder.setOrderNo(UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.ES));
    mainOrder.setStatus(MainOrderStatus.SUBMITTED);
    log.error(mainOrder.getOrderNo() + " ****ES****");
    return mainOrderMapper.insert(mainOrder);
  }

  @Override
  public int insertOrder(MainOrder mainOrder) {
   return insert(mainOrder);
  }

  @Override
  public MainOrder load(String id) {
    return mainOrderMapper.selectByPrimaryKey(id);
  }

  /**
   * @param  shop wechat shop only
   * @param orderType 区分进货类型的订单
   * @param userSelectedPromotion 活动商品对象 可为null
   */
  @Override
  @Transactional
  public MainOrder submitBySkuId2(String skuId, OrderAddress oa, String remark,
      UserSelectedProVO userSelectedProVO, Address address, Boolean danbao,
      int qty, Shop shop, String orderType, boolean useDeduction,
      PromotionType userSelectedPromotion, String isPrivilege, boolean needInvoice,
      BigDecimal selectPoint, BigDecimal selectCommission,Long cpId,String pTranCode,Integer pgDiscount,Integer isGroupHead) {

    User buyer = (User) getCurrentUser();
    Long fromCpId = null;
    if(cpId != null){
      fromCpId = cpId;
    }else {
      fromCpId = buyer.getUpline();
    }
    Long upline = null;
    if (isGroupHead != null && isGroupHead == 1) {
      upline = customerProfileService.getUplineX(buyer.getCpId(), fromCpId);
    } else {
      upline = customerProfileService.getUpline(buyer.getCpId(), fromCpId);
    }

    //FIXME refactor dongsongjie
    ProductSkuVO productSkuVO = productService.load(productService.loadSku(skuId).getProductId(), skuId);
    Sku sku = productSkuVO.getSku();

    CartItemVO directBuyCartItem = new CartItemVO();
    directBuyCartItem.setProduct(productService.load(sku.getProductId()));
    directBuyCartItem.setShop(shop);
    directBuyCartItem.setSku(productSkuVO.getSku());
    directBuyCartItem.setUser(buyer);
    directBuyCartItem.setSkuId(skuId);
    directBuyCartItem.setProductId(sku.getProductId());
    directBuyCartItem.setAmount(qty);
    // 创建主订单对象
    MainOrder ret = createMainOrder(remark, buyer);
    //直接下单
    ret.setOrderSingleType(OrderSingleType.SUBMIT);

    Order order = new Order();
    order.setShopId(shop.getId());
    order.setSellerId(shop.getOwnerId());
    order.setBuyerId(buyer.getId());
    // 在子订单中记录主订单ID
    order.setMainOrderId(ret.getId());
    order.setRemark(remark);
    // 从当前商品中读取root shop id
    order.setRootShopId(productSkuVO.getShopId());
    order.setNeedInvoice(needInvoice);
    order.setType(OrderType.DANBAO);
    Domain domain = loadDomain(buyer);
    order.setPartner(domain.getCode());
    order.setPartnerType(UserPartnerType.KKKD);
    order.setOrderType(OrderSortType.PIECE);
    // 是否自提
    if (address == null) {
      order.setIsPickup(1);
    } else {
      order.setIsPickup(0);
    }

    // 订单计价
    Map<Shop, List<CartItemVO>> cartItemMap = new HashMap<Shop, List<CartItemVO>>();
    cartItemMap.put(shop, Lists.newArrayList(directBuyCartItem));
    ConfirmOrderPromotionVO confirmOrderPromotionVO = promotionService
        .calculate4ConfirmOrder(cartItemMap, buyer, userSelectedProVO, address,
            useDeduction,
            selectPoint, selectCommission, null, PricingMode.SUBMIT);
    PricingResultVO orderPrice = confirmOrderPromotionVO.getShopPricingResult().get(shop);

    Map<Shop, UserCouponVo> shopCoupon = confirmOrderPromotionVO.getShopCoupon();

    DiscountPricingResult discountPricingResult = orderPrice.getDiscountPricingResult();

    BigDecimal cutDownDiscount = discountPricingResult.getDiscount(PromotionType.CUT_DOWN);
    if (upline != null) {
      order.setFromCpId(upline);
    }
    order.setReduction(cutDownDiscount == null ? BigDecimal.ZERO : cutDownDiscount);

    order.setGoodsFee(orderPrice.getGoodsFee());
    order.setLogisticsFee(orderPrice.getLogisticsFee());
    order.setLogisticDiscount(orderPrice.getLogisticsDiscount());
    order.setLogisticDiscountType(orderPrice.getLogisticDiscountType());
    order.setDiscountFee(orderPrice.getDiscountFee());

    this.setDisCountFee(order,buyer, pTranCode);
    order.setTotalFee(orderPrice.getTotalFee());
    // 是否为付尾款订单 在计算预购价格时放入
    order.setIsPayRest(orderPrice.getIsPayRest());

    //记录优惠信息
    if (MapUtils.isNotEmpty(shopCoupon) && shopCoupon.get(shop) != null) {
      order.setCouponId(shopCoupon.get(shop).getId());
    }

    Map<String, SkuDiscountDetail> couponDetail = null;
    SkuDiscountDetail discountDetail = null;
    if (discountPricingResult.isPromotionInUse(userSelectedPromotion)) {
      // 如果用户选中的优惠满足了优惠条件则为订单打上该标识
      OrderSortType orderPromotionType = OrderSortType.valueOf(userSelectedPromotion.name());
      order.setOrderType(orderPromotionType);
      couponDetail = discountPricingResult
              .getSpecificDiscountDetails(userSelectedPromotion);

      Promotion usingPromotion = discountPricingResult.getExtraParam(USING_PROMOTION_KEY,
              Promotion.class);
      if (usingPromotion != null) {
        order.setPromotionId(usingPromotion.getId());
      }
      discountDetail = couponDetail.get(sku.getSkuId());
    }

    ret.setTotalFee(confirmOrderPromotionVO.getTotalPricingResult().getTotalFee());



    //生成订单行
    List<OrderItem> orderItems = createOrderItem(productSkuVO,
                                                   isPrivilege,
                                                   directBuyCartItem,
                                                   sku,
                                                   discountPricingResult,
                                                   qty,
                                                   userSelectedPromotion,
                                                   order,
                                                   discountDetail);
    // 保存订单到数据库
    Map<Order, List<OrderItem>> orders = new HashMap<>(1);
    orders.put(order, orderItems);

    // 订单对象创建完成，优惠计算完毕，开始订单拆分
    Map<Order, List<OrderItem>> splitedOrders = orderSplitterService.split(orders);

    save(ret, splitedOrders, oa, couponDetail);
    saveToOrder(buyer.getCpId(), splitedOrders);
    return ret;
  }

  /**
   * 创建订单行（orderItem）
   * @param productSkuVO
   * @param isPrivilege
   * @param directBuyCartItem
   * @param sku
   * @param discountPricingResult
   * @param qty
   * @param userSelectedPromotion
   * @param order
   * @return
   */
  private List<OrderItem> createOrderItem(
          ProductSkuVO productSkuVO,
          String isPrivilege,
          CartItemVO directBuyCartItem,
          Sku sku,
          DiscountPricingResult discountPricingResult,
          int qty,
          PromotionType userSelectedPromotion,
          Order order,
          SkuDiscountDetail discountDetail) {
    OrderItem orderItem = new OrderItem();
    orderItem.setProductId(productSkuVO.getId());
    orderItem.setProductName(productSkuVO.getName());
    orderItem.setProductImg(productSkuVO.getImg());

    // 修改价格为计算后的价格
    // 特权商品购买不需要花钱
    if ("1".equals(isPrivilege)) {
      orderItem.setPrice(BigDecimal.ZERO);
      orderItem.setMarketPrice(BigDecimal.ZERO);
    } else {
      orderItem.setPrice(directBuyCartItem.getSku().getPrice());
      orderItem.setMarketPrice(directBuyCartItem.getSku().getMarketPrice());
    }

    orderItem.setSkuId(sku.getId());
    orderItem.setSkuStr(sku.getSpec());
    orderItem.setAmount(qty);
    BigDecimal bAmount = BigDecimal.valueOf(qty);
    orderItem.setPromoAmt(sku.getPromoAmt().multiply(bAmount));
    orderItem.setServerAmt(sku.getServerAmt().multiply(bAmount));

    setOrderItemDetailDiscount(orderItem, discountDetail);

    @SuppressWarnings("unchecked")
    Map<String, SkuDiscountDetail> cutDownDetail = (Map<String, SkuDiscountDetail>) discountPricingResult
            .getExtraParam(DEVIDED_CUTDOWN_DETAIL_KEY);

    if (MapUtils.isNotEmpty(cutDownDetail)) {
      SkuDiscountDetail cutDownDetailSku = cutDownDetail.get(sku.getSkuId());
      if (cutDownDetailSku != null) {
        orderItem.setReduction(cutDownDetailSku.getDiscount());
      }
    }

    List<OrderItem> orderItems = new ArrayList<>(1);
    orderItems.add(orderItem);
    return orderItems;
  }

  /**
   * 设置订单项的优惠详情
   *
   * @param orderItem 订单项
   * @param detailDiscount 优惠详情
   */
  void setOrderItemDetailDiscount(OrderItem orderItem,
      SkuDiscountDetail detailDiscount) {
    BigDecimal price = orderItem.getPrice();
    if (detailDiscount != null) {
      // 数据库中记录单个的优惠值
      BigDecimal discount = detailDiscount.getDiscount().divide(BigDecimal
          .valueOf(orderItem.getAmount()), 2, BigDecimal.ROUND_HALF_EVEN);
      orderItem.setPromotionType(detailDiscount.getPromotionType());
      // 优惠大于原价
      if (discount.compareTo(price) > 0) {
        orderItem.setDiscount(price);
        orderItem.setDiscountPrice(BigDecimal.ZERO);
      } else {
        BigDecimal discountPrice = price.subtract(discount);
        orderItem.setDiscount(discount);
        orderItem.setDiscountPrice(discountPrice);
      }
      return;
    }
    orderItem.setDiscountPrice(price);
    orderItem.setDiscount(BigDecimal.ZERO);
  }

  /**
   * * @param orderType 区分进货类型的订单
   */
  @Override
  @Transactional
  public MainOrder submitBySkuIds2(
      List<String> skuIds,
      OrderAddress oa,
      Map<String, String> shopRemarks,
      UserSelectedProVO userSelectedProVO,
      PromotionType userSelectedPromotion,
      Address address,
      Boolean danbao,
      Shop shop,
      String orderType,
      Boolean useDeduction,
      Boolean needInvoice,
      BigDecimal selectPoint,
      BigDecimal selectCommission,
      Long cpId,
      String pCode) {

    User buyer = (User) getCurrentUser();
    //根据购买者身份计算fromCpId
    Long fromCpId = this.getFromCpId(buyer, pCode);

    // 创建主订单对象
    MainOrder ret = createMainOrder(null, buyer);
    ret.setOrderSingleType(OrderSingleType.CART);

    List<CartItemVO> cartItems = cartService.checkout(new HashSet<>(skuIds), buyer, shop.getId(), null);

    //按店铺初始化购物车集合, 检查商品是否下架
    Map<Shop, List<CartItemVO>> cartItemsMap = new HashMap<>(4);
    for (CartItemVO cartItem : cartItems) {
      List<CartItemVO> list = cartItemsMap.get(cartItem.getShop());
      if (list == null) {
        list = new ArrayList<CartItemVO>(8);
        cartItemsMap.put(cartItem.getShop(), list);
      }
      list.add(cartItem);
    }

    ConfirmOrderPromotionVO confirmOrderPromotionVO = promotionService
        .calculate4ConfirmOrder(cartItemsMap, buyer, userSelectedProVO, address,
            useDeduction, selectPoint, selectCommission, null, PricingMode.SUBMIT);
    Map<Shop, UserCouponVo> shopCoupon = confirmOrderPromotionVO.getShopCoupon();
    Map<Shop, PricingResultVO> shopPrices = confirmOrderPromotionVO.getShopPricingResult();
    PricingResultVO totalPrice = confirmOrderPromotionVO.getTotalPricingResult();

    DiscountPricingResult discountPricingResult = totalPrice.getDiscountPricingResult();
    Map<String, SkuDiscountDetail> discountDetails = null;
    boolean isPromotionInUse = discountPricingResult.isPromotionInUse(userSelectedPromotion);

    BigDecimal cutDownDiscount = discountPricingResult.getDiscount(PromotionType.CUT_DOWN);

    //创建子订单
    Map<Order, List<OrderItem>> orders = new HashMap<>();
    for (Entry<Shop, List<CartItemVO>> entry : cartItemsMap.entrySet()) {
     // Shop shop = entry.getKey();
      Order order = new Order();
      if (fromCpId != null) {
        order.setFromCpId(fromCpId);
      }
      order.setShopId(shop.getId());
      order.setSellerId(shop.getOwnerId());
      order.setBuyerId(buyer.getId());
      order.setType(OrderType.DANBAO);
      Domain domain = loadDomain(buyer);
      order.setPartner(domain.getCode());
      order.setReduction(cutDownDiscount == null ? BigDecimal.ZERO : cutDownDiscount);
      order.setPartnerType(UserPartnerType.KKKD);
      order.setOrderType(OrderSortType.PIECE);
      order.setRemark(shopRemarks.get(shop.getId()));
      order.setPaidYundou(0L);
      order.setFullYundou(false);
      order.setNeedInvoice(needInvoice);
      if (isPromotionInUse) {
        OrderSortType orderPromotionType = OrderSortType
            .valueOf(userSelectedPromotion.name());
        order.setOrderType(orderPromotionType);
        discountDetails = discountPricingResult
            .getSpecificDiscountDetails(userSelectedPromotion);
      }
      // 是否自提
      if (address == null) {
        order.setIsPickup(1);
      } else {
        order.setIsPickup(0);
      }

      List<OrderItem> orderItems = new ArrayList<>();
      Map<String, Integer> map = new HashMap<>();

      // 立减分摊
      @SuppressWarnings("unchecked")
      Map<String, SkuDiscountDetail> cutDownDetail = (Map<String, SkuDiscountDetail>) discountPricingResult
          .getExtraParam(DEVIDED_CUTDOWN_DETAIL_KEY);

      for (CartItemVO cartItem : entry.getValue()) {
        String skuId = cartItem.getSkuId();
        ProductSkuVO productSkuVO = productService
            .load(cartItem.getProductId(), cartItem.getSkuId());
        if (StringUtils.isEmpty(order.getRootShopId())) {
          order.setRootShopId(productSkuVO.getShopId()); // 从当前商品中读取root shop id
        }

        Sku sku = productSkuVO.getSku();
        OrderItem orderItem = new OrderItem();
        orderItem.setProductId(productSkuVO.getId());
        orderItem.setProductName(productSkuVO.getName());
        orderItem.setProductImg(productSkuVO.getImg());

        //价格暂时读取原商品价格
        orderItem.setPrice(productSkuVO.getSkuPrice());
        orderItem.setMarketPrice(productSkuVO.getSkuMarketPrice());

        orderItem.setSkuId(sku.getId());
        orderItem.setSkuStr(sku.getSpec());
        orderItem.setAmount(cartItem.getAmount());

        orderItem.setPromoAmt(sku.getPromoAmt().multiply(cartItem.getBAmount()));
        orderItem.setServerAmt(sku.getServerAmt().multiply(cartItem.getBAmount()));
        orderItems.add(orderItem);

        if (cutDownDetail != null) {
          SkuDiscountDetail cutDownDetailSku = cutDownDetail.get(skuId);
          if (cutDownDetail != null) {
            orderItem.setReduction(cutDownDetailSku.getDiscount());
          }
        }
        SkuDiscountDetail couponDetail =
            discountDetails == null ? null : discountDetails.get(skuId);
        setOrderItemDetailDiscount(orderItem, couponDetail);
        map.put(orderItem.getSkuId(), orderItem.getAmount());
      }

      PricingResultVO prices = shopPrices.get(shop);
      order.setGoodsFee(prices.getGoodsFee());
      order.setLogisticsFee(prices.getLogisticsFee());
      order.setLogisticDiscount(prices.getLogisticsDiscount());
      order.setLogisticDiscountType(prices.getLogisticDiscountType());
      order.setDiscountFee(prices.getDiscountFee());
      order.setTotalFee(prices.getTotalFee());

      //记录优惠信息
      if (MapUtils.isNotEmpty(shopCoupon) && shopCoupon.get(shop) != null) {
        order.setCouponId(shopCoupon.get(shop).getId());
      }
      orders.put(order, orderItems);
    }
    ret.setTotalFee(totalPrice.getTotalFee());
    ret.setDiscountFee(totalPrice.getDiscountFee());

    // 订单对象创建完成，优惠计算完毕，开始订单拆分
    Map<Order, List<OrderItem>> splitedOrders = orderSplitterService.split(orders);

    // 保存订单
    save(ret, splitedOrders, oa, discountDetails);

    //保存到第三方库
    saveToOrder(buyer.getCpId(), splitedOrders);

    return ret;
  }

  @Override
  public MainOrderVO loadVO(String orderId) {
    MainOrder mainOrder = mainOrderMapper.selectByPrimaryKey(orderId);
    List<Order> orders = orderMapper.selectByMainOrderId(orderId);
    List<OrderVO> orderVOs = new ArrayList<OrderVO>();
    for (Order order : orders) {
      orderVOs.add(orderService.loadVO(order.getId()));
    }

    if (mainOrder != null && !CollectionUtils.isEmpty(orders)) {
      return new MainOrderVO(mainOrder, orderVOs);
    }
    return null;
  }

  @Transactional
  public void addOrder(List<OrderDetail> orderDetails, List<OrderHeader> orderHeaders) {
    orderDetailMapper.batchInsert(orderDetails);
    orderHeaderMapper.batchInsert(orderHeaders);
  }

  /**
   * 开团支付成功后状态处理
   */
  @Override
  public void updatePiecePayStatus(MainOrderVO order,PaymentMode paymentMode,String bizNo){
//    piecePay(order.getOrderNo(), paymentMode, bizNo);
  }

  //2018-9-23 3:25無調用
  @Override
  public void pay(String orderNo, PaymentMode mode, String payNo) {
    pieceGroupPayCallBackService.updateOrderStatus(orderNo, payNo, MainOrderStatus.PAID);
  }

  /**
   * 拼团订单一起生效
   * @param pCode
   * @param pTranCode
   */
  @Override
  public void effectOrders (String pCode, String pTranCode) {
    log.info("成团成功，拼团编码pTranCode=" + pTranCode);
    //5.订单生效：扣减活动库存、更新订单状态、更新成团状态
    pieceGroupPayCallBackService.effectOrder(pCode, pTranCode);
    //更新订单状态 =OrderStatus.PAID

    //组团成功发送成功消息
    List<PromotionPgMemberInfoVO> memberList = promotionPgMemberInfoMapper.selectMemberInfo(pTranCode,
        null, null);
    for (PromotionPgMemberInfoVO member : memberList) {
      //团长p
      ReciverMessage msg = new ReciverMessage();
      msg.setType(FieldConstants.PUSH_CHANNEL);
      CollagePushMessage collagePushMessage = new CollagePushMessage();
      collagePushMessage.setBelognTo(member.getMemberId());
      collagePushMessage.setTypeMsg("SUCESS");
      collagePushMessage.setStatus(0);
      String content = "您参加的拼团活动已组团成功，我们会尽快安排发货。";
      collagePushMessage.setContent(content);
      msg.setCollagePushMessage(collagePushMessage);
      messageReciver.reciverMsg(msg);
    }
  }

//  @Override
//  public MainOrder submitBySkuIds1(List<String> skuIds, OrderAddress oa, Map<String, String> shopRemarks, UserSelectedProVO userSelectedProVO, PromotionType userSelectedPromotion, Address address, Boolean danbao, Boolean useDeduction, String pCode) {
//    return null;
//  }

  Domain loadDomain(User user) {
    Domain domain = null;
    if (StringUtils.isNotBlank(user.getPartner())) {
      domain = domainService.loadByCode(user.getPartner());
    } else {
      domain = domainService.loadRootDomain();
    }

    if (domain == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "用户错误 partner=[" + user.getPartner() + "]");
    }
    return domain;
  }

  MainOrder createMainOrder(String remark, User user) {
    MainOrder ret = new MainOrder();
    ret.setTotalFee(new BigDecimal(0));
    ret.setDiscountFee(new BigDecimal(0));
    ret.setPaidFee(new BigDecimal(0));
    ret.setBuyerId(user.getId());
    //默认为担保交易类型
    ret.setType(OrderType.DANBAO);
    ret.setRemark(remark);
    return ret;
  }

  /**
   * 设置减免金额
   * @param order
   * @param user
   * @param pTranCode
   */
  void setDisCountFee(Order order, User user, String pTranCode) {

//    if (hadDoubleDiscount(user, pTranCode)) {
//      BigDecimal pointToCash = order.getPaidPoint().divide(new BigDecimal(100));
//      order.setDiscountFee(order.getPaidFee().add(pointToCash).add(order.getPaidCommission()));
//    }
  }

  /**
   * 折上折判断处理
   *
   */
  @Override
  public boolean hadDoubleDiscount (User user, String pTranCode) {
    if (null == pTranCode) {
      return false;
    }
    //0. 自己是不是纯白人
    if (!customerProfileService.isNew(user.getCpId().toString())) {
      return false;
    }
    //1. 查询活动有无折上折
    Boolean hasZSZ = promotionRewardsService.queryPromotionRewardsZSZInfoByPTranCode(pTranCode);
    if (null == hasZSZ) {
      return false;
    }
    if(!hasZSZ){
      log.info("判断没有折上折,pTranCode:"+pTranCode);
      return false;

    }
    //2. 查询参团是否已有纯白人
    int newCount = promotionPgMemberInfoMapper.countNewPerson(pTranCode);
    if (0 == newCount) {
      return false;
    }
    return true;
  }
//
//  /**
//   * 获取订单的下单方式
//   */
//  private void setOrderTypeInfo(Boolean danbao, Order order) {
//    OrderType orderType = OrderType.DIRECT;
//
//    if (Boolean.TRUE.equals(danbao)) {
//      orderType = OrderType.DANBAO;
//    } else {
//      String shopId = order.getShopId();
//      if (StringUtils.isBlank(shopId)) {
//        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
//                "设置订单的交易方式的时候，订单所属的店铺不存在");
//      }
//
//      Shop shop = shopService.load(shopId);
//      if (shop == null) {
//        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
//                "设置订单的交易方式的时候，店铺" + shopId + "不存在");
//      }
//
//      if (shop.getDanbao() != null && shop.getDanbao()) {
//        orderType = OrderType.DANBAO;
//      }
//    }
//    order.setType(orderType);
//  }

  void save(MainOrder mainOrder, Map<Order, List<OrderItem>> orders,
                    OrderAddress orderAddress,
                    Map<String, SkuDiscountDetail> skuDiscountDetails) {
    //保存主订单
    insert(mainOrder);
    Iterator<Entry<Order, List<OrderItem>>> iterator = orders.entrySet().iterator();
    while (iterator.hasNext()) {
      Entry<Order, List<OrderItem>> entry = iterator.next();
      Order order = entry.getKey();
      List<OrderItem> orderItems = entry.getValue();
      // 设置主订单ID到子订单
      order.setMainOrderId(mainOrder.getId());
      orderService.insert(order);
      if (orderAddress != null) {
        orderAddress.setOrderId(order.getId());
        Address address = new Address();
        BeanUtils.copyProperties(orderAddress, address);
        address.setCommon(false);
        address.setUserId(getCurrentUser().getId());
        if (address.getWeixinId() == null) {
          address.setWeixinId("");
        }
        orderAddressService.insert(orderAddress);
        addressService.saveUserAddress(address, true);
      }
      // 保存订单项
      for (OrderItem orderItem : orderItems) {
        orderItem.setOrderId(order.getId());
        orderItemMapper.insert(orderItem);
      }
      // 根据订单类型保存订单活动或积分的明细
      saveOrderDetail(order, orderItems, skuDiscountDetails);

    }
    //保存--拼团--订单详情
    String main_order_id = mainOrder.getId();
    MainOrder pieceMainOrder = mainOrderMapper.selectByPrimaryKey(main_order_id);
    List<Order> pieceOrder = orderMapper.selectByMainOrderId(main_order_id);
    PromotionOrderDetail promotionOrderDetail = new PromotionOrderDetail();
    if(null != pieceMainOrder){
      promotionOrderDetail.setOrder_head_code(pieceMainOrder.getOrderNo());
    }
    if(pieceOrder.size()>0){
      promotionOrderDetail.setOrder_detail_code(pieceOrder.get(0).getOrderNo());
    }
    //活动编码
    /*promotionOrderDetail.setP_code();
    //活动详情编码
    promotionOrderDetail.setP_detail_code();
    //活动类型
    promotionOrderDetail.setP_type();*/
    //暂时不明白,此方法作用
    applicationContext.publishEvent(new MainOrderActionEvent(
            MainOrderActionType.SUBMIT, mainOrder));

  }

  /**
   * 保存每个订单项的优惠明细
   *
   * @param order 订单对象
   * @param orderItems 订单对应的订单项
   * @param skuDiscountDetailMap sku对应的优惠明细
   */
  void saveOrderDetail(Order order, List<OrderItem> orderItems,
                               Map<String, SkuDiscountDetail> skuDiscountDetailMap) {
    if (MapUtils.isNotEmpty(skuDiscountDetailMap)) {
      for (OrderItem orderItem : orderItems) {
        String skuId = orderItem.getSkuId();
        SkuDiscountDetail detail = skuDiscountDetailMap.get(skuId);
        if (detail != null) {
          String promotionId = detail.getPromotionId();
          PromotionType type = detail.getPromotionType();
          OrderItemPromotion orderItemPromotion = new OrderItemPromotion();
          orderItemPromotion.setArchive(false);
          orderItemPromotion.setPromotionId(promotionId);
          orderItemPromotion.setType(type);
          orderItemPromotion.setOrderItemId(orderItem.getId());
          orderItemPromotionService.insert(orderItemPromotion);
        }
      }
    }
  }

  private List<OrderItem> buildGiftOrderItems(Set<Product> giftProducts) {
    List<OrderItem> result = new ArrayList<OrderItem>();
    for (Product product : giftProducts) {
      List<Sku> skus = productService.listSkus(product);
      if (CollectionUtils.isNotEmpty(skus)) {
        Sku sku = skus.get(0);
        OrderItem giftItem = new OrderItem();
        giftItem.setProductId(product.getId());
        giftItem.setProductName(product.getName());
        giftItem.setProductImg(product.getImg());

        //价格暂时读取原商品价格
        giftItem.setPrice(BigDecimal.ZERO);
        giftItem.setMarketPrice(BigDecimal.ZERO);

        giftItem.setSkuId(sku.getId());
        giftItem.setSkuStr(sku.getSpec());
        giftItem.setAmount(1);
        result.add(giftItem);
      }
    }
    return result;
  }

  void saveToOrder(Long cpId, Map<Order, List<OrderItem>> order) {
    // 部分属性需要根据身份变更
    boolean hasIdentity = customerProfileService.hasIdentity(cpId);
    List<OrderDetail> orderDetails = new ArrayList<>();
    List<OrderHeader> orderHeaders = new ArrayList<>();
    Set<Entry<Order, List<OrderItem>>> entrySet = order.entrySet();
    for (Entry<Order, List<OrderItem>> e : entrySet) {
      List<OrderItem> orderItems = e.getValue();
      OrderHeader orderHeader = new OrderHeader();
      Order key = e.getKey();
      orderHeader.setIscalculated(0);
      orderHeader.setIsdeleted(0);
      orderHeader.setFromCpId(key.getFromCpId());
      //created_at创建时间
      if (key.getCreatedAt() == null) {
        key.setCreatedAt(new Date());
      }
      orderHeader.setIsActive(0);
      //cpid资格证号
      orderHeader.setCpid(cpId);
      //orderid订单号，种子表生成
      orderHeader.setOrderid(key.getOrderNo());
      //ordermonth订单月
      orderHeader.setOrdermonth(getMonth(key.getCreatedAt()));
      //freightamt运费
      orderHeader.setFreightamt(key.getLogisticsFee());
      //totaldue应付
      orderHeader.setTotaldue(key.getTotalFee());
      //totalpaid实付
      orderHeader.setTotalpaid(key.getPaidFee());
      //status状态
      orderHeader.setStatus(getStatus(OrderStatus.SUBMITTED));
      //commissionProductamt计酬金额
      if (key.getPaidPoint() == null) {
        key.setPaidPoint(new BigDecimal(0));
      }
      orderHeader.setCommissionProductamt(null);
      //createtime订单创建时间
      orderHeader.setCreatetime(key.getCreatedAt());
      //deadline订单过期时间
      // 订单过期时间, 半个小时
      // FIXME 处理硬编码
      Date deadLineTime = DateUtils.addSeconds(new Date(), 30 * 60);
      orderHeader.setDeadline(deadLineTime);
      //paytime申请支付时间
      orderHeader.setPaytime(null);
      //paytimeFinish支付完成时间
      orderHeader.setPaytimeFinish(null);
      //sendtimeFinish发货完成时间
      orderHeader.setSendtimeFinish(null);
      //finishtime完成时间
      orderHeader.setFinishtime(null);
      //storeId外键ID，店铺表
      orderHeader.setStoreId(key.getShopId());
      //orderinguserId外键ID，用户表
      orderHeader.setOrderinguserId(key.getBuyerId());
      //totalFreight配送费,保险费 + 运费
      orderHeader.setTotalFreight(key.getLogisticsFee());
      //remark备注
      orderHeader.setRemark(key.getRemark());
      //source来源平台 1 hds, 2 vivilife, 3 ecommerce
      orderHeader.setSource((byte) PlatformType.E.getCode());
      //createdAt创建时间
      orderHeader.setCreatedAt(new Date());
      //updatedAt更新时间
      orderHeader.setUpdatedAt(key.getUpdatedAt());
      //productamt产品金额
      orderHeader.setProductamt(key.getGoodsFee());
      //discountamt折扣金额
      orderHeader.setDiscountamt(key.getDiscountFee());
      //detailCount订单明细记录数，用于核对header和detail数据
      orderHeader.setDetailCount(orderItems.size());
      orderHeader.setUsePoints(key.getPaidCommission());
      orderHeader.setUseDePoints(key.getPaidPoint());
      BigDecimal vpTotal = BigDecimal.ZERO;
      BigDecimal reductionTotal = BigDecimal.ZERO;
      //vp总VP值
      for (OrderItem o : orderItems) {
        OrderDetail orderDetail = new OrderDetail();
        Sku sku = skuMapper.selectByPrimaryKey(o.getSkuId());
        if (sku == null) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "sku不存在");
        }
        Integer amount = o.getAmount();
        BigDecimal bAmount = BigDecimal.valueOf(amount);
        orderDetail.setIsdeleted(0);
        //orderid订单号，种子表生成
        orderDetail.setOrderid(key.getOrderNo());
        orderDetail.setCreatedAt(key.getCreatedAt());
        //updated_at更新时间
        orderDetail.setUpdatedAt(key.getUpdatedAt());
        //product_source产品来源
        orderDetail.setProductSource(3L);
        //points购买时积分
//        orderDetail.setPoints(key.getPaidCommission());
        //quantity数量
        orderDetail.setQuantity(amount);
        //unitprice购买时单价
        orderDetail.setUnitprice(o.getPrice());
        //detailid订单明细主键
        orderDetail.setDetailid(o.getId());
        //product_sku产品SKU编号
        orderDetail.setProductSku(sku.getSkuCode());
        orderDetail.setTaxrate(BigDecimal.valueOf(0.17));
        BigDecimal vp = sku.getNetWorth().multiply(bAmount);
        BigDecimal reduction = sku.getPoint().multiply(bAmount);
        // 有身份的人才累加, 立减
        if (hasIdentity) {
          reductionTotal = reductionTotal.add(reduction);
        }
        vpTotal = vpTotal.add(vp);
        // 更新后立减只在order_header表
        orderDetail.setPromoAmt(o.getPromoAmt());
        orderDetail.setServerAmt(o.getServerAmt());
        orderDetail.setSavingAmt(reduction);
        orderDetail.setVp(vp);
        orderDetails.add(orderDetail);
      }
      // vp值
      orderHeader.setVp(vpTotal);
      // 返利
      orderHeader.setPromoAmt(reductionTotal);
      orderHeader.setUsePoints(key.getPaidCommission());
      orderHeaders.add(orderHeader);
    }
    addOrder(orderDetails, orderHeaders);
  }

  Integer getStatus(OrderStatus orderStatus) {
    return OrderHeaderStatusMapping.mapping(orderStatus);
  }

  public Integer getMonth(Date date) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMM");
    Integer month = Integer.parseInt(simpleDateFormat.format(date));
    return month;
  }


  /**
   * 计算fromCpId
   * @param buyer
   * @param pCode
   * @return
   */
  Long getFromCpId(User buyer, String pCode) {
    Long cpId = buyer.getCpId();
    Long fromCpId = null;
    //根据pCode查询团长的cpId
    String groupMasterId = recordPromotionPgTranInfoMapper.selectMaster(pCode);
    //如果是纯白人，则为团长
    if (customerProfileService.isNew(cpId.toString())) {
      fromCpId = new Long(groupMasterId);
      return fromCpId;
    }

    Long upline = customerProfileService.getUpline(buyer.getCpId(), fromCpId);
    fromCpId = upline;
    return fromCpId;
  }
}
