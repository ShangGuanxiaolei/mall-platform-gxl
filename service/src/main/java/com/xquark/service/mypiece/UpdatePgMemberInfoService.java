package com.xquark.service.mypiece;

import com.xquark.dal.mapper.UpdatePgMemberInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 支付成功回调 update status`
 * 0：参团未完成支付 1：参团成功
 * @author qiuchuyi
 * @date 2018/9/12 14:54
 */
@Service
public class UpdatePgMemberInfoService {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private UpdatePgMemberInfoMapper updatePgMemberInfoMapper;

    @Transactional(rollbackFor = Exception.class)
    public void UpdatePgMemberInfoService(String pieceGroupDetailCode, Integer status){

            Integer result = updatePgMemberInfoMapper.updatePgMemberInfopieceGroupDetailCode(pieceGroupDetailCode,status);
            if(result != 1){
                log.error("支付成功后回调修改拼团成员支付状态异常, pieceGroupDetailCode=[" + pieceGroupDetailCode
                        + "] ");
            }
    }
}
