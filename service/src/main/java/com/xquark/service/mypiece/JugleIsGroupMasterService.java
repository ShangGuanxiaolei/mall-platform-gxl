package com.xquark.service.mypiece;

import com.xquark.dal.mapper.JugleIsGroupMasterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author qiuchuyi
 * @date 2018/9/13 19:16
 */
@Service
public class JugleIsGroupMasterService {
    private Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private JugleIsGroupMasterMapper jugleIsGroupMasterMapper;

    public Boolean jugleIsGroupMasterByOrderHeadCode(String OrderHeadCode){
        try{
            int ret= jugleIsGroupMasterMapper.jugleIsGroupMasterByOrderHeadCode(OrderHeadCode);
            return ret>=1;
        }catch (Exception e){
            log.error("根据OrderHeadCode查询是否团长异常, OrderHeadCode=[" + OrderHeadCode
                    + "] ",e);
            throw new RuntimeException();
        }
    }
}
