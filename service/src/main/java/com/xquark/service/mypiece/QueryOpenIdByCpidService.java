package com.xquark.service.mypiece;

import com.xquark.dal.mapper.QueryOpenIdByCpidMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author qiuchuyi
 * @date 2018/9/15 14:55
 */
@Service
public class QueryOpenIdByCpidService {
    private Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private QueryOpenIdByCpidMapper queryOpenIdByCpidMapper;

    public String queryOpenIdByCpid(String cpid){

        try{
            return queryOpenIdByCpidMapper.queryOpenIdByCpid(cpid);
        }catch (Exception e){
            log.error("根据cpid查询openId异常, cpid=[" + cpid
                    + "] ",e);
            throw new RuntimeException(e);
        }
    }
}
