package com.xquark.service.mypiece;


import com.xquark.dal.model.mypiece.PromotionTempStock;
import com.xquark.dal.model.mypiece.PromotionTempStockVo;

import java.util.List;

/**
 * @author gx
 */
public interface PieceStockCheckService {

    List<PromotionTempStockVo> findStock(PromotionTempStock promotionTempStock);

    boolean updateStock(PromotionTempStock promotionTempStock);
}
