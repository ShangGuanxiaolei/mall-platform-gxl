package com.xquark.service.mypiece;


import com.xquark.dal.mapper.StockCheckMapper;
import com.xquark.dal.model.mypiece.PromotionTempStock;
import com.xquark.dal.model.mypiece.PromotionTempStockVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author gx
 */
@Service
public class PieceStockCheckServiceImpl implements PieceStockCheckService {

    @Autowired
    private StockCheckMapper stockCheckMapper;

    @Override
    public List<PromotionTempStockVo> findStock(PromotionTempStock promotionTempStock) {
        return stockCheckMapper.findStock(promotionTempStock);
    }

    @Override
    public boolean updateStock(PromotionTempStock promotionTempStock) {
        return stockCheckMapper.updateStock(promotionTempStock);
    }
}
