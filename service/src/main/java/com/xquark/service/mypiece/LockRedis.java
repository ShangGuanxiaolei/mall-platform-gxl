package com.xquark.service.mypiece;

import com.xquark.config.CachingConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisException;

import java.util.UUID;

/**
 * redis分布式锁
 *
 * @author tanggb
 * Create in 13:31 2018/9/11
 */
@Service
public class LockRedis{

    @Autowired
    private CachingConfig cachingConfig;
    /**
     * 分布式锁超时时间
     */
    private static final Long TIMEOUT = 60 * 1000L;
    /**
     * 分布式锁获取锁超时时间
     */
    private static final Long ACQUIRETIMEOUT = 5 * 1000L;
    /**
     * 获取分布式锁
     *
     * @param OrderId  redis setNx()的key
     * @return
     */
    public String getRedisLock(String OrderId) {
        Jedis jedis = null;
        String keyIdentifier = null;
        try {
            //获取redis连接
            jedis = cachingConfig.getJedisPool().getResource();
            //生成唯一的value
            String identifier = Thread.currentThread().getName() + ":" + UUID.randomUUID().toString();
            //通过订单号生成一个唯一的key
            String lockKey = "LOCK:" + OrderId;
            //设置锁超时时间防止死锁
            int lockExpire = (int) (TIMEOUT / 1000);
            //获取分布式锁最后时间 acquire_timeout：获取锁超时时间
            long end = System.currentTimeMillis() + ACQUIRETIMEOUT;
            while (System.currentTimeMillis() < end) {
                if (jedis == null) {
                    Thread.sleep(100);
                    continue;
                }

                if (jedis.setnx(lockKey, identifier) == 1) {
                    jedis.expire(lockKey, lockExpire);
                    keyIdentifier = identifier;
                    System.out.println(Thread.currentThread().getName() + "  获取锁:"+keyIdentifier);
                    return keyIdentifier;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return keyIdentifier;
    }

    /**
     * 释放redis锁
     *
     * @param OrderId 通过订单号来作为redis setNx()的key
     * @param identifier 通过唯一value来比对进行锁的释放，防止删除别人的key
     * @return
     */
    public boolean unRedisLock(String OrderId, String identifier) {
        Jedis redis = null;
        String lockKey = "LOCK:" + OrderId;
        boolean retFlag = false;
        //通过key查询redis的value
        String newIdentifier = "";
        try {
            redis = cachingConfig.getJedisPool().getResource();
            while (true) {
                if (redis == null) {
                    try {
                        Thread.sleep(10);
                        redis = cachingConfig.getJedisPool().getResource();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
                }
                //监听key
                redis.watch(lockKey);
                newIdentifier = redis.get(lockKey);
                if (newIdentifier == null || "".equals(newIdentifier)) {
                    return retFlag;
                } else if (identifier.equals(newIdentifier)) {
                    //删除key结果
                    long delResult = redis.del(lockKey);
                    if (delResult == 1) {
                        retFlag = true;
                    } else {
                        System.out.println(Thread.currentThread().getName() + "  释放锁失败，锁已提前释放");
                        //continue;
                    }
                } else {
                    System.out.println(Thread.currentThread().getName() + "  锁已过期失效，被污染");
                }
                //放弃监听
                redis.unwatch();
                break;
            }
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            if (redis != null) {
                redis.close();
            }
        }
        return retFlag;
    }


//    public void ee(){
//        ne
//    }
}
