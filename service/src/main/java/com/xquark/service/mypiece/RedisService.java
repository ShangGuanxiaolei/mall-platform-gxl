package com.xquark.service.mypiece;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Service;

/**
 * redis接口类
 * 
 * @类名: IRedisService .
 * @描述: TODO .
 * @程序猿: gx .
 * @日期: 2017年3月6日 下午12:58:18 .
 * @版本号: V1.0 .
 */

@Service
public class RedisService {

	@Autowired
	private RedisTemplate<String, ?> redisTemplate;

	public String get(final String key) {
		String result = redisTemplate.execute(new RedisCallback<String>() {
			public String doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = redisTemplate.getStringSerializer();
				byte[] value = connection.get(serializer.serialize(key));
				connection.close();
				connection.closePipeline();
				return serializer.deserialize(value);
			}
		});
		return result;
	}

	public String getSet(final String key, final String value) {
		String result = redisTemplate.execute(new RedisCallback<String>() {
			public String doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = redisTemplate.getStringSerializer();
				byte[] values = connection.getSet(serializer.serialize(key), serializer.serialize(value));
				connection.close();
				connection.closePipeline();
				return serializer.deserialize(values);
			}
		});
		return result;
	}
	public boolean setNX(final String key, final String value) {
		boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = redisTemplate.getStringSerializer();
				Boolean result = connection.setNX(serializer.serialize(key), serializer.serialize(value));
				connection.close();
				connection.closePipeline();
				return result;
			}
		});
		return result;
	}
}
