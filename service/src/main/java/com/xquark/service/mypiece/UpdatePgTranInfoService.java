package com.xquark.service.mypiece;

import com.xquark.dal.mapper.UpdatePgTranInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 支付成功回调 update piece_status
 * 0:开团未支付 1:已开团 2:已成团 3:库存不足取消 4:失效取消',
 * @author qiuchuyi
 * @date 2018/9/12 14:54
 */
@Service
public class UpdatePgTranInfoService {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private UpdatePgTranInfoMapper updatePgTranInfoMapper;

    @Transactional
    public void UpdatePgTranInfoService(String pieceGroupTranCode, String piece_status){
            Integer result = updatePgTranInfoMapper.UpdatePgTranInfoPieceGroupTranCode(pieceGroupTranCode, piece_status);
            if(result!=1){
                log.error("支付成功后回调修改拼团交易状态异常, pieceGroupTranCode=[" + pieceGroupTranCode
                        + "] ");
            }
    }
}
