package com.xquark.service.mypiece;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author tanggb
 * Create in 14:48 2018/9/11
 */

@Deprecated
public class RedisLockTest {

    @Autowired
    private LockRedis lockRedis;

    public void test(){
        System.out.println(Thread.currentThread().getName() + " 启动运行");
        String indentifier = lockRedis.getRedisLock("resource");
        if (indentifier == null) {
            System.out.println(Thread.currentThread().getName() + " 获取锁失败，取消任务");
        }else{
            System.out.println(Thread.currentThread().getName() + "  执行任务");
            lockRedis.unRedisLock("resource", indentifier);
        }
        System.out.println(Thread.currentThread().getName() + " 完成推出");
    }



}

