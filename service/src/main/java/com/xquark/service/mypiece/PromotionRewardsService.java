package com.xquark.service.mypiece;

import com.xquark.dal.mapper.PromotionRewardsMapper;
import com.xquark.dal.model.mypiece.PromotionRewardsVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author qiuchuyi
 * @date 2018/9/21 23:47
 */
@Service
public class PromotionRewardsService {
    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private PromotionRewardsMapper promotionRewardsMapper;

    @Transactional(rollbackFor = Exception.class)
    public Boolean queryPromotionRewardsZSZInfoByPTranCode(String pTranCode){
        try{
            Integer result = promotionRewardsMapper.queryPromotionRewardsZSZInfoByPTranCode(pTranCode);
            if(result == null){
                throw new RuntimeException();
            }
            return result==1;
        }catch (RuntimeException e){
            log.error("根据piece_group_tran_code查询相应拼团活动奖励异常, pTranCode=[" + pTranCode
                    + "] ",e);
            return null;
        }
    }
}
