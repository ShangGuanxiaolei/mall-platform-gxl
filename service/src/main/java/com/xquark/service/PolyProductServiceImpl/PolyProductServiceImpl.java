package com.xquark.service.PolyProductServiceImpl;

import com.xquark.dal.mapper.PloyProductMapper;
import com.xquark.dal.model.GoodInfo;
import com.xquark.dal.model.Skus;
import com.xquark.service.PolyProductService.PolyProductService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @auther liuwei
 * @date 2018/6/14 10:20
 */
@Service
public class PolyProductServiceImpl implements PolyProductService {

  @Autowired
  private PloyProductMapper ployProductMapper;

  @Override
  public Integer getPolyCountTotal() {
    return ployProductMapper.countTotalPolyProduct();
  }

  @Override
  public List<GoodInfo> getPloyProductList(Integer PageIndex, Integer PageSize) {

    if(PageIndex >= 1 && PageSize > 0){
      int offset = (PageIndex-1)*PageSize;
      List<GoodInfo> goodlists = ployProductMapper.limitSelect(offset,PageSize);

      for (GoodInfo goods : goodlists) {
        goods.setSkus(getPloySkusList(goods.getPlatProductID()));
      }
      return goodlists;
    }else {
      throw new RuntimeException("参数不符合要求");
    }
  }

  @Override
  public void savePloyProductQuantity(Integer quantity, Integer id) {
    ployProductMapper.updateNewQuantity(quantity, id);
  }

  @Override
  public void savePloyProductSkuQuantity(Integer quantity,String skuId) {
    ployProductMapper.updateNewSkuQuantity(quantity,skuId);
  }

  @Override
  public boolean checkProductId(Integer id) {
    Integer b = ployProductMapper.selectIdByProductId(id);
    if(b>0){
      return true;
    }else {
      return false;
    }
  }


  public List<Skus> getPloySkusList(String id){
    return ployProductMapper.selectSku(id);
  }

  @Override
  public Integer getPloyProductQuantity(Integer id) {
    int quantity = ployProductMapper.selectQuantityNewById(id);
    if(quantity > 0){
      return quantity;
    }else {
      throw new RuntimeException("商品不存在或没有库存");
    }
  }
}
