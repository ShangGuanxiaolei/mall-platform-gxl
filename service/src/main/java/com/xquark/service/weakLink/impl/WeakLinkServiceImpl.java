package com.xquark.service.weakLink.impl;


import com.xquark.dal.mapper.CustomerProfileMapper;
import com.xquark.dal.mapper.WeakLinkMapper;
import com.xquark.dal.model.CustomerProfile;
import com.xquark.dal.model.WeakLink;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.weakLink.WeakLinkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author hs
 */
@Service
public class WeakLinkServiceImpl implements WeakLinkService {

    private WeakLinkMapper weakLinkMapper;
    private Logger log = LoggerFactory.getLogger(WeakLinkServiceImpl.class);

    @Autowired
    private CustomerProfileMapper customerProfileMapper;

    @Autowired
    private CustomerProfileService customerProfileService;

    @Autowired
    public void setWeakLinkMapper(WeakLinkMapper weakLinkMapper) {
        this.weakLinkMapper = weakLinkMapper;
    }

    /**
     * 1、有身份的人/已经绑定过的人 不用绑定
     * 2、纯白人或者白人没有上级的情况需要绑定
     * @param weakLink
     * @return int
     */
    @Override
    public int bound(WeakLink weakLink) {
        //判断是否绑定过弱关系
        Long lCount = weakLinkMapper.selectCount(weakLink.getCpid());
        if (lCount != 0){
            log.info("{}已经绑定过弱关系",weakLink.getCpid());
            return 0;
        }
        //判断是否互相绑定过
        WeakLink weakLinkOne = weakLinkMapper.selectWeakLinkBound(weakLink);
        if (weakLinkOne != null){
            log.info("{}->{}已有绑定关系,不能互绑",weakLink.getCpid(),weakLink.getShareCpid());
            return 0;
        }
        //判断是否有绑定过次强关系,消除弱关系表垃圾数据
        boolean subStrong = customerProfileService.isSubStrong(weakLink.getCpid());
        if(subStrong){
            log.info("{}->{}已经绑定过次强关系", weakLink.getCpid(), weakLink.getShareCpid());
            return 0;
        }
        //判断是否绑定过强关系
        boolean strong = customerProfileService.isStrong(weakLink.getCpid());
        if(strong){
            log.info("{}->{}已经绑定过强关系", weakLink.getCpid(), weakLink.getShareCpid());
            return 0;
        }
        //判断不能自己绑自己,场景：自己抢自己的红包
        if (weakLink.getCpid().equals(weakLink.getShareCpid())) {
            //查询客户信息，并判断是否有上级，没有则绑定公司，有则绑定上级
            CustomerProfile profile = customerProfileMapper.selectByPrimaryKey(weakLink.getCpid());
            if (Objects.isNull(profile) && Objects.isNull(profile.getUplineCpId())) {
                weakLink.setShareCpid(2000002L);
            }
            log.info("{}->{}不能自己互绑，改为绑公司", weakLink.getCpid(), weakLink.getShareCpid());
        }
        return weakLinkMapper.insert(weakLink);
    }
}
