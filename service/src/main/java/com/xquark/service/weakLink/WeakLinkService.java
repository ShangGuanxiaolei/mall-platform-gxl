package com.xquark.service.weakLink;

import com.xquark.dal.model.WeakLink;

/**
 * @author hs
 */
public interface WeakLinkService {
    int bound(WeakLink weakLink);
}
