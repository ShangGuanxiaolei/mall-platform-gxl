package com.xquark.service.productPackage.impl;

import com.google.common.base.Preconditions;
import com.xquark.dal.mapper.PackageMapper;
import com.xquark.dal.model.Package;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.productPackage.PackageService;
import com.xquark.utils.ExcelUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: kong Date: 18-6-13. Time: 下午6:06 产品包装箱
 */
@Service
@Transactional
public class PackageServiceImpl extends BaseServiceImpl implements PackageService {

  @Autowired
  private PackageMapper baseMapper;

  /**
   * 根据id查询
   */
  @Override
  public Package getByPrimaryKey(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.getByPrimaryKey(id);
  }

  /**
   * 插入数据
   */
  @Override
  public boolean insert(Package obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.insert(obj) > 0;
  }

  /**
   * 修改数据
   */
  @Override
  public boolean update(Package obj) {
    Preconditions.checkNotNull(obj);
    return baseMapper.update(obj) > 0;
  }

  @Override
  public boolean delete(String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return baseMapper.delete(id) > 0;
  }

  /**
   * 批量删除
   *
   * @param ids id集合
   */
  @Override
  public boolean batchDelete(@NotEmpty List<String> ids) {
    Preconditions.checkNotNull(ids, "param cant be null");
    final boolean ret = baseMapper.batchDelete(ids) > 0;
    return ret;
  }


  /**
   * 分页查询
   */
  @Override
  public List<Package> list(Pageable page, Map<String, Object> params) {
    return baseMapper.list(page, params);
  }

    @Override
    public Boolean AddByExcel(File file) {
        InputStream inputStream = null;
        String[][] line = null;
        try {
            inputStream = new FileInputStream(file);
            line = ExcelUtils.excelImport(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (line == null || line.length == 0) {
            throw new RuntimeException("excelData is null");
        }
        List<Package> packages = new ArrayList<>();
        for (int j = 3; j < line.length; j++) {
            Package packageExcel = new Package();
            for (int i = 0; i < line[j].length; i++) {
                if (line[j][i] == null || line[j][i].equals("")) {
                    continue;
                }
                switch (i) {
                    case 0:
                        //名称
                        packageExcel.setName(line[j][i]);
                        break;
                    case 1:
                        //重量　
                        packageExcel.setWeight(Integer.parseInt(line[j][i]));
                        break;
                    case 2:
                        //长度
                        packageExcel.setLength(Integer.parseInt(line[j][i]));
                        break;
                    case 3:
                        //宽度
                        packageExcel.setWidth(Integer.parseInt(line[j][i]));
                        break;
                    case 4:
                        //高度
                        packageExcel.setHeight(Integer.parseInt(line[j][i]));
                        break;
                    case 5:
                        //价格
                        packageExcel.setPrice(new BigDecimal(line[j][i]));
                        break;
                }
            }
            if (packageExcel.getName() == null || packageExcel.getName().equals("")) {
                break;
            }
            System.out.println("测试：" + packageExcel);
            packages.add(packageExcel);
        }
        List<String> codes = baseMapper.getName();
        for (Package p : packages) {
            if (codes.contains(p.getName())) {
                baseMapper.updateByName(p);
            } else {
                baseMapper.insert(p);
            }
        }
        return null;
    }


  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long count(Map<String, Object> params) {
    return baseMapper.count(params);
  }


}
