package com.xquark.service.productPackage;

import com.xquark.dal.model.Package;

import java.io.File;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

/**
 * User: kong Date: 18-6-13. Time: 下午6:04 产品包装箱
 */
public interface PackageService {

  /**
   * 添加
   */
  boolean insert(Package obj);

  /**
   * 通过id查找
   */
  Package getByPrimaryKey(String id);

  /**
   * 更新
   */
  boolean update(Package obj);


  /**
   * 通过id删除
   */
  boolean delete(String id);

  /**
   * 批量删除
   */
  boolean batchDelete(List<String> ids);

  /**
   * 计算数据的数量
   */
  Long count(Map<String, Object> params);


  /**
   * 分页查询
   */
  List<Package> list(Pageable page, Map<String, Object> params);

    /**
     * 根据Excel插入信息
     */
    Boolean AddByExcel(File file);
}