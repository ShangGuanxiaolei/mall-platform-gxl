package com.xquark.service.productSales.vo;

/**
 * @类名: CheckSaleCustomer
 * @描述: 折扣促销活动 受众类型校验
 * @程序猿: LuXiaoLing
 * @日期: 2019/4/2 11:33
 * @版本号: V1.0 .
 */
public class CheckSaleCustomer {
    //当前用户是否可以购买折扣促销商品
    public Boolean  canBuyCustomer;
    //提示语
    public String toast;

    public Boolean getCanBuyCustomer() {
        return canBuyCustomer;
    }

    public void setCanBuyCustomer(Boolean canBuyCustomer) {
        this.canBuyCustomer = canBuyCustomer;
    }

    public String getToast() {
        return toast;
    }

    public void setToast(String toast) {
        this.toast = toast;
    }
}
