package com.xquark.service.productSales;

import com.xquark.dal.model.Promotion4ProductSalesVO;
import com.xquark.dal.model.User;
import com.xquark.dal.page.PageHelper;
import com.xquark.service.productSales.vo.CheckSaleCustomer;

/**
 * @类名: ProductSalesService
 * @描述: TODO .
 * @程序猿: LuXiaoLing
 * @日期: 2019/3/27 15:01
 * @版本号: V1.0 .
 */
public interface ProductSalesService {

    PageHelper<Promotion4ProductSalesVO> selectBeginningSalesProducts(Integer page, Integer pageSize, User user, String pCode);

    CheckSaleCustomer checkCustomerType(String customerType, User user);


    PageHelper<Promotion4ProductSalesVO> selectGrandSalesProducts(Integer page, Integer pageSize, User user, String grandSaleId);

}
