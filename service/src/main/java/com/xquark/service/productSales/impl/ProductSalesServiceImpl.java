package com.xquark.service.productSales.impl;

import com.alibaba.fastjson.JSONObject;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.mapper.FootprintMapper;
import com.xquark.dal.mapper.PromotionGoodsMapper;
import com.xquark.dal.model.Promotion4ProductSalesVO;
import com.xquark.dal.model.User;
import com.xquark.dal.page.PageHelper;
import com.xquark.dal.vo.SaleProduactVO;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.productSales.ProductSalesService;
import com.xquark.service.productSales.vo.CheckSaleCustomer;
import com.xquark.service.promotion.PromotionBaseInfoService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @类名: ProductSalesServiceImpl
 * @描述: TODO .
 * @程序猿: LuXiaoLing
 * @日期: 2019/3/27 15:03
 * @版本号: V1.0 .
 */


@Service
public class ProductSalesServiceImpl implements ProductSalesService {
    @Autowired
    private PromotionBaseInfoService promotionBaseInfoService;
    @Autowired
    private PromotionGoodsMapper promotionGoodsMapper;
    //判断当前用户有没有下过单 及 是否新人
    @Autowired
    private FootprintMapper footprintMapper;
    //白名单校验
    @Autowired
    private PromotionService promotionService;

    private static final Long MAGIC_NUMBER = 9999999L;


    @Override
    public PageHelper<Promotion4ProductSalesVO> selectBeginningSalesProducts(Integer page, Integer pageSize, User user, String pCode) {
        PageHelper<Promotion4ProductSalesVO> helper = new PageHelper<>();
        helper.setPage(page);
        helper.setPageSize(pageSize);
        if (StringUtils.isNotBlank(pCode)) {
            //白名单控制,数据库存储的数据逻辑是反的，没办法只能按错的来，坑爹
            if(this.promotionService.checkIsWhite(MAGIC_NUMBER,"PRODUCT_SALES")){
                if(!this.promotionService.checkIsWhite(user.getCpId(),"PRODUCT_SALES")){
                    return helper;
                }
            }
            //如果pCode不为空 为二级页面的查询某个指定活动
            List<Promotion4ProductSalesVO> salesPromotions = promotionBaseInfoService.selectSalesPromotion(pCode);
            List<Promotion4ProductSalesVO> showsalesPromotions = new ArrayList<>();

            if (CollectionUtils.isNotEmpty(salesPromotions)) {
                for (Promotion4ProductSalesVO salesPromotion : salesPromotions) {

                    String customerType = salesPromotion.getCustomerType();
                    //校验受众类型
                    CheckSaleCustomer checkCustomerType = this.checkCustomerType(customerType, user);
                    if (checkCustomerType.getCanBuyCustomer()) {
                        //根据活动编码查询活动商品信息
                        List<SaleProduactVO> saleProduactVOS = promotionGoodsMapper.selectSalesProductsByPcode(salesPromotion.getpCode(), null);
                        this.convertPrice(saleProduactVOS, user.getCpId());
                        salesPromotion.setSaleProduacts(saleProduactVOS);
                        showsalesPromotions.add(salesPromotion);

                    }
                }
            } else {
                return helper;
            }
            helper.setResult(showsalesPromotions);
            return helper;

        } else {
            //白名单控制,数据库存储的数据逻辑是反的，没办法只能按错的来，坑爹
            if(this.promotionService.checkIsWhite(MAGIC_NUMBER,"PRODUCT_SALES")){
                if(!this.promotionService.checkIsWhite(user.getCpId(),"PRODUCT_SALES")){
                    return helper;
                }
            }
            //查首頁活動信息
            List<Promotion4ProductSalesVO> salesPromotions = promotionBaseInfoService.selectSalesPromotion(pCode);
            List<Promotion4ProductSalesVO> showsalesPromotions = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(salesPromotions)) {
                for (Promotion4ProductSalesVO salesPromotion : salesPromotions) {
                    String customerType = salesPromotion.getCustomerType();
                    //校验受众类型
                    CheckSaleCustomer checkCustomerType = this.checkCustomerType(customerType, user);
                    if (checkCustomerType.getCanBuyCustomer()) {
                        //根据活动编码查询活动商品信息
                        List<SaleProduactVO> saleProduactVOS = promotionGoodsMapper.selectSalesProductsByPcode(salesPromotion.getpCode(), 1);
                        this.convertPrice(saleProduactVOS, user.getCpId());
                        salesPromotion.setSaleProduacts(saleProduactVOS);
                        showsalesPromotions.add(salesPromotion);
                    }
                }
            } else {
                return helper;
            }
            helper.setResult(showsalesPromotions);
            return helper;
        }

    }

    //受众类型检查
    @Override
    public CheckSaleCustomer checkCustomerType(String customerType, User user) {
        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        String json = redisUtils.get("login_return_" + user.getCpId());
        Boolean isProxy= false;
        Boolean isMember= false;
        if(json != null) {
            //redis中的身份
            JSONObject jsonObject = JSONObject.parseObject(json);
            isProxy = jsonObject.getBoolean("isProxy");
            isMember = jsonObject.getBoolean("isMember");
        }
        String currentType = "";
        CheckSaleCustomer checkCustomerType = new CheckSaleCustomer();
        String transCustomerType = customerType.replace("2", "12");
        Boolean canBuyCustomer= false;
        String checkToast="";
        //不限购
        if (customerType.contains("0")) {
            canBuyCustomer = true;
            checkCustomerType.setCanBuyCustomer(canBuyCustomer);
            return checkCustomerType;
        }
        Long orderCount = footprintMapper.alreadyBuyOrderCount(String.valueOf(user.getCpId()));
        if (isProxy||isMember||user.getHasIdentity()||user.getIsMember()||user.getIsProxy()||user.getIsShopOwner()) {
            //有身份
            currentType = "3";
        } else {
            //白人
            currentType = "2";
        }
        //未下过单 为新人
        if (!(isProxy||isMember||user.getHasIdentity()||user.getIsMember()||user.getIsProxy()||user.getIsShopOwner()) && orderCount.intValue() == 0) {
            currentType = "1";
        }
        if (transCustomerType.contains(currentType)) {
            canBuyCustomer= true;
        } else {
            canBuyCustomer= false;
        }
        if(!canBuyCustomer && (currentType.equals("1")||currentType.equals("2"))&&customerType.equals("3")){
            checkToast="温馨提示，该商品只限会员购买";
        }
        if(!canBuyCustomer && (currentType.equals("3"))
                &&(customerType.equals("1")
                ||customerType.equals("2")
                ||customerType.equals("1,2")
                ||customerType.equals("2,1"))){
            checkToast="抱歉，该商品只限非会员购买";
        }
        if(!canBuyCustomer && (currentType.equals("2"))
                &&(customerType.equals("1,3")
                ||customerType.equals("3,1")
               )){
            checkToast="抱歉，该商品您无法购买";
        }

        if(!canBuyCustomer && (currentType.equals("2")
                &&customerType.equals("1")) ){
            checkToast="抱歉，该商品只限新人购买";
        }
        if(!canBuyCustomer && (currentType.equals("3")
                &&customerType.equals("1")) ){
            checkToast="抱歉，该商品只限新人购买";
        }
        checkCustomerType.setCanBuyCustomer(canBuyCustomer);
        checkCustomerType.setToast(checkToast);
        return checkCustomerType;
    }

    @Override
    public PageHelper<Promotion4ProductSalesVO> selectGrandSalesProducts(Integer page, Integer pageSize, User user, String grandSaleId) {
        PageHelper<Promotion4ProductSalesVO> helper = new PageHelper<>();
        helper.setPage(page);
        helper.setPageSize(pageSize);
        //查首頁活動信息
        List<Promotion4ProductSalesVO> salesPromotions = promotionBaseInfoService.selectGrandSalesPromotion(grandSaleId);
        List<Promotion4ProductSalesVO> showsalesPromotions = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(salesPromotions)) {
            for (Promotion4ProductSalesVO salesPromotion : salesPromotions) {
                String customerType = salesPromotion.getCustomerType();
                //校验受众类型在520促销活动
                CheckSaleCustomer checkCustomerType = this.checkCustomerType(customerType, user);
                if (checkCustomerType.getCanBuyCustomer()) {

                    List<SaleProduactVO> saleProduactVOS = promotionGoodsMapper.selectAllProductForGrandSale(salesPromotion.getpCode());
                    this.convertPrice(saleProduactVOS, user.getCpId());

                    salesPromotion.setSaleProduacts(saleProduactVOS);
                    showsalesPromotions.add(salesPromotion);
                }
            }
        } else {
            return helper;
        }
        helper.setResult(showsalesPromotions);
        return helper;
    }

    private void convertPrice(List<SaleProduactVO> saleProduactVOS, Long cpId) {
        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        String json = redisUtils.get("login_return_" + cpId);
        if (json != null) {
            JSONObject jsonObject = JSONObject.parseObject(json);
            Boolean isProxy = jsonObject.getBoolean("isProxy");
            Boolean isMember = jsonObject.getBoolean("isMember");
            //根据身份转换价格
            if (isProxy) {
                for (SaleProduactVO saleProduactVO : saleProduactVOS) {
                    saleProduactVO.setMemberPrice(saleProduactVO.getPrice().subtract(saleProduactVO.getPoint()));
                    saleProduactVO.setProxyPrice(saleProduactVO.getPrice().subtract(saleProduactVO.getPoint()).subtract(saleProduactVO.getServerAmt()));
                    saleProduactVO.setCashExchange(saleProduactVO.getCashExchange().subtract(saleProduactVO.getPoint()).subtract(saleProduactVO.getServerAmt()));
                }
            } else if (isMember) {
                for (SaleProduactVO saleProduactVO : saleProduactVOS) {
                    saleProduactVO.setMemberPrice(saleProduactVO.getPrice().subtract(saleProduactVO.getPoint()));
                    saleProduactVO.setCashExchange(saleProduactVO.getCashExchange().subtract(saleProduactVO.getPoint()));
                }
            }

        }
    }

}
