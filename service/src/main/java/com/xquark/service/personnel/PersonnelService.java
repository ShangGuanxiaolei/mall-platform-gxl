package com.xquark.service.personnel;

import com.xquark.dal.model.Personnel;
import com.xquark.dal.type.UserPartnerType;

public interface PersonnelService {

  Personnel loadByExtUserIdAndPartner(String userId, UserPartnerType partner);

  Personnel loadByInnerIdAndPartner(String innerId, UserPartnerType partner);
}
