package com.xquark.service.personnel.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.PersonnelMapper;
import com.xquark.dal.model.Personnel;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.service.personnel.PersonnelService;

@Service("personnelService")
public class PersonnelServiceImpl implements PersonnelService {

  @Autowired
  private PersonnelMapper personnelMapper;

  @Override
  public Personnel loadByExtUserIdAndPartner(String userId, UserPartnerType partner) {
    return personnelMapper.loadByExtUserIdAndPartner(userId, partner);
  }

  @Override
  public Personnel loadByInnerIdAndPartner(String innerId, UserPartnerType partner) {
    return personnelMapper.loadByInnerIdAndPartner(innerId, partner);
  }
}
