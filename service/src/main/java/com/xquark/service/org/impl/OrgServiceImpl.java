package com.xquark.service.org.impl;

import com.xquark.dal.mapper.MerchantMapper;
import com.xquark.dal.mapper.OrgMapper;
import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.Org;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.vo.MerchantOrgVO;
import com.xquark.dal.vo.OrgTreeVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.org.OrgService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * 作者: wangxh 创建日期: 17-3-16 简介:
 */
@Service
public class OrgServiceImpl implements OrgService {

  @Autowired
  private OrgMapper orgMapper;

  @Autowired
  private MerchantMapper merchantMapper;

  @Override
  public List<Map<String, Object>> listTree() {
    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
    List<Org> allList = orgMapper.listAll();
    Map<String, Object> rootMap = new HashMap<String, Object>();
    rootMap.put("id", "0");
    rootMap.put("text", "部门分类");
    List<OrgTreeVO> cList = this.treeMenuList(allList, null);
    rootMap.put("children", cList);
    list.add(rootMap);
    return list;
  }

  /**
   * 根据sortno分页查询信息
   */
  @Override
  public List<Org> listBySortNo(String parentId, Pageable pageable, Sort.Direction direction) {
    return orgMapper.listBySortNo(parentId, pageable, direction);
  }

  /**
   * 根据name分页查询信息
   */
  @Override
  public List<Org> listByName(String parentId, Pageable pageable, Sort.Direction direction) {
    return orgMapper.listByName(parentId, pageable, direction);
  }

  /**
   * 根据创建时间查询分页
   */
  @Override
  public List<Org> listByTime(String parentId, Pageable pageable, Sort.Direction direction) {
    return orgMapper.listByTime(parentId, pageable, direction);
  }

  @Override
  public List<MerchantOrgVO> listMerchant(String orgId, Pageable pageable, String order,
      String keyword, Direction direction, boolean ifPage) {
    return merchantMapper.listByOrgId(orgId, pageable, order, keyword, direction, ifPage);
  }

  @Override
  public Integer updateMerchantOrg(String id, String orgId) {
    Merchant merchant = new Merchant();
    merchant.setId(id);
    if (StringUtils.isEmpty(orgId) || "0".equals(orgId)) {
      merchant.setOrgId("74"); //74编码后为0, 清除部门关系
    } else {
      Org org = orgMapper.select(orgId);
      if (org == null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "没有部门为" + orgId + " 的信息");
      }
      merchant.setOrgId(orgId);
    }
    return merchantMapper.updateByPrimaryKey(merchant);
  }

  @Override
  public Integer countByOrgId(String orgId) {
    return merchantMapper.countByOrgId(orgId);
  }

  @Override
  public Org load(String id) {
    return orgMapper.select(id);
  }

  @Override
  public Org save(String parentId, String name, String remark) {
    checkName(name);
    // 检查同一父节点下是否已存在
    Org org = orgMapper.selectChild(parentId, name);
    if (org == null) {
      org = new Org();
      org.setName(name);
      // 暂时都设置为true
      org.setIs_auto_expand(true);
      org.setRemark(remark);
      org.setArchive(false);
      if (parentId != null) {
        Org parentOrg = orgMapper.select(parentId);
        if (parentOrg == null) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "部门的上级不存在");
        }
        org.setParent_id(parentId);
        org.setIs_leaf(true);
      } else {
        org.setIs_leaf(false);
      }
      int subCounts = orgMapper.loadSubCounts(parentId);
      org.setSort_no(++subCounts);
      orgMapper.insert(org);
    } else {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "部门[" + name + "]已经存在");
    }
    String encodedId = String.valueOf(IdTypeHandler.encode(Long.parseLong(org.getId())));
    org.setId(encodedId);
    return org;
  }

  @Override
  public void remove(String id) {
    Org org = orgMapper.select(id);
    if (org != null) {
      remove(org);
    } else {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "要删除的部门不存在");
    }
  }

  /**
   * 递归删除节点
   */
  private void remove(Org org) {
    orgMapper.deleteByPrimaryKey(org.getId());
    List<Org> cList = orgMapper.loadSubs(org.getId());
    if (cList != null) {
      for (Org child : cList) {
        remove(child);
      }
    }
  }

  @Override
  public Org update(String id, String parentId, String name, String remark) {
    checkName(name);
    Org org = orgMapper.select(id);
    if (org == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "更新的部门不存在");
    }
    if (StringUtils.isEmpty(parentId)) {
      parentId = null;
    }
    Org orgExists = orgMapper.selectChild(parentId, name);
    if (orgExists != null && !orgExists.getId().equals(id)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "重名名失败，已经有重名的部门");
    }
    orgMapper.update(id, name, remark);
    return orgMapper.select(id);
  }

  @Override
  public Integer loadSubCounts(String parentId) {
    return orgMapper.loadSubCounts(parentId);
  }

  @Override
  public Integer loadCounts() {
    return orgMapper.loadCounts();
  }

  /**
   * 递归得到部门的上下级关系
   *
   * @param allList 全部列表
   * @param parentId 父节点id
   * @return 整理好关系的列表
   */
  private List<OrgTreeVO> treeMenuList(List<Org> allList, String parentId) {
    List<OrgTreeVO> result = new ArrayList<OrgTreeVO>();
    for (Org org : allList) {
      String menuId = org.getId();
      String pid = org.getParent_id();
      OrgTreeVO treeVO = new OrgTreeVO();
      treeVO.setId(menuId);
      treeVO.setText(org.getName());
      String img = org.getIcon_name();
      if (StringUtils.isNotEmpty(img)) {
        treeVO.setIcon(img);
      }
      if (pid == parentId || (parentId != null && parentId.equals(pid))) {
        List<OrgTreeVO> cList = treeMenuList(allList, menuId);
        treeVO.setChildren(cList);
        result.add(treeVO);
      }
    }
    return result;
  }

  private void checkName(String name) {
    if (StringUtils.isEmpty(name)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "部门的名称不能为空");
    }
    // 控制分类的名称的长度，全中文8位，中文+英文/英文 长度为16
    int nameLength = 16;
    if (!Pattern.matches("/^[\u4E00-\u9FA5]+$/", name)) {
      // 不全是中文
    } else {
      //全是中文
      nameLength = 8;
    }

    if (name.length() > nameLength) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "部门的名称不能大于 " + nameLength + " 位");
    }
  }

}
