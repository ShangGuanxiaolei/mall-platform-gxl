package com.xquark.service.org;

import com.xquark.dal.model.Org;
import com.xquark.dal.vo.MerchantOrgVO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Sort.Direction;

/**
 * 作者: wangxh 创建日期: 17-3-16 简介:
 */
public interface OrgService {

  List<Map<String, Object>> listTree();

  List<Org> listBySortNo(String parentId, Pageable pageable, Sort.Direction direction);

  List<Org> listByName(String parentId, Pageable pageable, Sort.Direction direction);

  List<Org> listByTime(String parentId, Pageable pageable, Sort.Direction direction);

  Integer countByOrgId(String orgId);

  Org load(String id);

  Org save(String parentId, String name, String remark);

  void remove(String id);

  Org update(String id, String name, String parentId, String remark);

  Integer loadSubCounts(String parentId);

  Integer loadCounts();

  List<MerchantOrgVO> listMerchant(String orgId, Pageable pageable, String order, String keyword,
      Direction direction, boolean ifPage);

  Integer updateMerchantOrg(String id, String orgId);
}
