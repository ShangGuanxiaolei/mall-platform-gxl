package com.xquark.service.activity.impl;

import com.xquark.dal.mapper.ActivityPushMapper;
import com.xquark.dal.model.ActivityPush;
import com.xquark.service.activity.ActivityPushService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.push.type.CastType;
import com.xquark.service.push.type.Platform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.xquark.utils.MessageCastUtils.castCpIds;
import static com.xquark.utils.MessageCastUtils.castParam;

/**
 * @author luqig
 * @since 2019-04-29
 */
@Service("activityPushService")
public class ActivityPushServiceImpl extends BaseServiceImpl implements ActivityPushService {
  private ActivityPushMapper activityPushMapper;

  private final Lock lock = new ReentrantLock();

  @Autowired
  public ActivityPushServiceImpl(ActivityPushMapper activityPushMapper) {
    this.activityPushMapper = activityPushMapper;
  }

  @Override
  public int delete(String id) {
    return 0;
  }

  @Override
  public int undelete(String id) {
    return 0;
  }

  @Override
  public int insert(ActivityPush activityPush) {
    return activityPushMapper.insert(activityPush);
  }

  @Override
  public int insertOrder(ActivityPush activityPush) {
    return 0;
  }

  @Override
  public ActivityPush load(String id) {
    return activityPushMapper.loadById(id);
  }

  @Override
  public void pushMessage() {
    if (this.lock.tryLock()) {
      try {
        List<ActivityPush> needPush = activityPushMapper.findNotTimeOut(5);

        for (ActivityPush activityPush : needPush) {
          Map<String, String> map = castParam(activityPush.getNeedParam());
          String text = activityPush.getContent();
          // 替换消息参数
          for (Map.Entry<String, String> entry : map.entrySet()) {
            text = text.replace(entry.getKey(), entry.getValue());
          }

          Boolean pushState = false;

          // 推用户列表
          if (Objects.equals(activityPush.getPushUserType(), 2)) {
            pushState =
                CastType.CUSTOMIZEDCAST.pushMassage(
                    Platform.ALL,
                    castCpIds(activityPush.getPushUserCpIds()),
                    activityPush.getPriority(),
                    activityPush.getCustomUrl(),
                    activityPush.getAbbreviation(),
                    text);
          } else if (Objects.equals(activityPush.getPushUserType(), 1)) {
            // 推送所有用户 cpIds 为null
            pushState =
                CastType.BROADCAST.pushMassage(
                    Platform.ALL,
                    null,
                    activityPush.getPriority(),
                    activityPush.getCustomUrl(),
                    activityPush.getAbbreviation(),
                    text);
          }

          if (pushState) {
            activityPush.setPushTime(new Date());
            activityPush.setPushState(1);
          }

          activityPushMapper.updateById(activityPush);
        }
      } finally {
        lock.unlock();
      }
    } else {
      log.warn("有任务推送中，稍后再试");
    }
  }
}
