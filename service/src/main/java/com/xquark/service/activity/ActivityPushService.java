package com.xquark.service.activity;

import com.xquark.dal.model.ActivityPush;
import com.xquark.service.ArchivableEntityService;

/**
 * @author luqig
 * @since 2019-04-29
 */
public interface ActivityPushService extends ArchivableEntityService<ActivityPush> {
  void pushMessage();
}
