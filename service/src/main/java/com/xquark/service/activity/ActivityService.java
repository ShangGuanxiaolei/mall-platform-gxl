package com.xquark.service.activity;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.Activity;
import com.xquark.dal.model.ActivityTicket;
import com.xquark.dal.model.CampaignProduct;
import com.xquark.dal.model.User;
import com.xquark.dal.type.ActivityType;
import com.xquark.dal.vo.ActivityEX;
import com.xquark.dal.vo.CampaignProductEX;
import com.xquark.dal.vo.XQHomeActProductVO;
import com.xquark.service.ArchivableEntityService;
import com.xquark.service.product.ProductSP;

/**
 * 销售活动服务接口
 *
 * @author tonghu
 */
public interface ActivityService extends ArchivableEntityService<Activity> {

  // 删除 活动
  int deleteActivity(String activityId, String userId);

  //获取活动及优惠信息
  ActivityEX loadActivityEx(String activityId, String shopId, ActivityType activityType);

  Activity selectByPrimaryKeyOwner(String activityId);

  // 保存卖家创建的活动
  Activity saveActivity(Activity activity);

  ActivityTicket saveActivityTicket(ActivityTicket ticket);

  void updateDiscountByTicket(String ticketId, Float discount);

  // 列出当前已参加活动的商品
  List<CampaignProduct> listCampaignProducts(String activityId);

  // 列出平台活动
  List<ActivityVO> listPublic();

  // 列出卖家私有活动
  List<ActivityVO> listByUser(String userId);

  // 列出平台和私有活动
  List<ActivityVO> listPublicAndPrivate(String userId, String shopId);

  // 列出所有的活动(分销活动，老活动)
  List<ActivityEX> listAll();

  // 保存卖家创建的活动
  ActivityVO loadVO(String activityId);

  /**
   * 当期用户更新活动基本信息
   */
  Activity update(Activity activity);

  /**
   * 传参用户更新活动
   */
  Activity update(Activity activity, String userId, String shopId);

  // 关闭活动基本信息
  void close(String activityId);

  // 获取活动的默认ticket
  ActivityTicket getDefaultJoinActivityTicket(String shopId, String activityId, ActivityType type);

  /**
   * 特价商品是否是特价商品   seker 2015-01-17
   */
  ProductSP checkProductSP(String product_id);


  /**
   * 检查是否是特价商品及用户购买量   seker 2015-01-17
   */
  ProductSP checkProductSP(String product_id, User user);

  /**
   * 特价商品是否可以购买，只从特价商品维度进行检查， 不检查库存    seker 2015-01-06
   *
   * @param amount 新购买的数量
   * @return 0 不是特价商品可以购买  1 特价商品 允许购买   2还没开放购买  3超过购买上限  4请先登录  (0 不是特价商品   >0 特价商品)
   */
  int checkBuyProductSP(User user, String product_id, Integer amount);

  // 商品添加到活动中
  void addProducts(List<CampaignProduct> products);

  // 商品从活动中删除
  void removeProducts(String id, List<String> productIds);

  // 请求参加活动
//    ActivityTicket requestTicket(String activityId);

  // 更新活动及ticket状态到in progress
  void batchUpdateStatusToInProgressPrivate();

  // 更新活动及ticket状态到in progress
  void batchUpdateStatusToInProgressPublic();

  // 更新活动及ticke从状态not_started到closed
  void batchUpdateStatusNotStartedToClosed();

  // 更新活动及ticket从状态in_progress到closed
  void batchUpdateStatusInProgressToClosed();

  void insertTicket(ActivityTicket ticket);

  void updateTicket(ActivityTicket ticket);

  void unjoin(String id);

  // 获取商品进行中的活动
  Activity obtainProductCurrentActivities(String productId);

  // 全店铺活动加入活动商品
  void addProductByShopDiscount(Activity activity, Float discount);

  //活动删除后处理活动商品
  void optActivityProducts(String activityId);

  /**
   * 某个用户的活动在时间上是否有重叠
   */
  boolean existInRange(String userId, Date from, Date to, String excludeId);

  void updateStatusToInProgress();

  void updateStatusToClosed();

  List<CampaignProductEX> loadCampaignProductEX(String activityId, String shopId);

  List<ActivityEX> selectPubAndPrivate(String userId, String shopId);

  Long countActivitysByQuery(Map<String, Object> paramsMap);

  Long countCampaignProduct4Home(Map<String, Object> paramsMap);

  List<XQHomeActProductVO> listCampaignProduct4Home(Map<String, Object> paramsMap, Pageable page);

  List<Activity> listActivitysByQuery(Map<String, Object> paramsMap, Pageable pager);

  Long countCampaignProductByQuery(Map<String, Object> paramsMap);

  List<CampaignProductEX> listCampaignProductByQuery(Map<String, Object> paramsMap, Pageable pager);

  ActivityTicket loadSubmittedTicket4Audit(String activityId, String productId);

  void auditTicketProduct(String newTicketId, String activityId, String oldTicketId,
      String productId,
      String prouctBrand, String shortName, Integer sort, String imagePc, String imageApp);

  Activity selectByPrimaryKey(String activityId);

  ActivityTicket getDefaultJoinActivityTicket(String actId);

  void close(String activityId, String userId);

  void optActivityProducts(String activityId, String userId);

  /**
   * 添加商品到私有活动中
   */
  void addProductToPrivateAct(ActivityTicket ticket, String shopId,
      List<CampaignProduct> products);

  boolean existProductInRange(Date start, Date end, String activityId,
      String productId);

  ActivityTicket loadTicket4Audit(String activityId, String productId);

  /**
   * 通过商品的id获取商品参加活动的信息
   */
  List<CampaignProduct> getCampaignByProdcut(String productId);

  /**
   * 通过商品的id删除商品的活动报名信息
   */
  void removeCampaignAndTicketByProductId(String productId);

}
