package com.xquark.service.activity.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xquark.dal.mapper.ActivityMapper;
import com.xquark.dal.mapper.ActivitySPMapper;
import com.xquark.dal.mapper.ActivityTicketMapper;
import com.xquark.dal.mapper.CampaignProductMapper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.mapper.ShopMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.Activity;
import com.xquark.dal.model.ActivitySP;
import com.xquark.dal.model.ActivityTicket;
import com.xquark.dal.model.CampaignProduct;
import com.xquark.dal.model.PreferentialType;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.ActivityTicketAuditStatus;
import com.xquark.dal.type.ActivityChannel;
import com.xquark.dal.type.ActivityStatus;
import com.xquark.dal.type.ActivityType;
import com.xquark.dal.type.SyncEvType;
import com.xquark.dal.vo.ActivityEX;
import com.xquark.dal.vo.CampaignProductEX;
import com.xquark.dal.vo.SyncMqEvent;
import com.xquark.dal.vo.XQHomeActProductVO;
import com.xquark.service.activity.ActivityService;
import com.xquark.service.activity.ActivityVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.product.ProductSP;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.impl.ProductSPImpl;
import com.xquark.service.rocketmq.SyncRocketMq;
import com.xquark.service.sync.SyncEvent;
import com.xquark.service.syncevent.ActivityItem;
import com.xquark.service.syncevent.KDSyncData;
import com.xquark.service.syncevent.SyncEventService;
import com.xquark.service.syncevent.SyncEventType;
import com.xquark.service.user.UserService;
import com.xquark.utils.PartnerConstants;

/**
 * 营销活动的服务
 *
 * @author tonghu
 */
@Service("activityService")
public class ActivityServiceImpl extends BaseServiceImpl implements ActivityService {

  @Autowired
  private ShopMapper shopMapper;
  @Autowired
  private ActivityMapper activityMapper;
  @Autowired
  private ActivityTicketMapper activityTicketMapper;
  @Autowired
  private ActivitySPMapper activitySPMapper;
  @Autowired
  private CampaignProductMapper campaignProductMapper;
  @Autowired
  private ProductMapper productMapper;
  @Autowired
  private SkuMapper skuMapper;
  @Autowired
  private UserService userService;
  @Autowired
  private ProductService productService;
  @Autowired
  private SyncEventService syncEventService;

  @Autowired
  private SyncRocketMq syncRocketMq;

  @Value("${site.web.host.name}")
  private String domainName;

  @Value("${sync.scheme.switch}")
  private Integer syncSwitch;

  @Value("${site.web.host.name}")
  String siteHost;

  @Override
  public Activity selectByPrimaryKeyOwner(String activityId) {
    Activity activity = activityMapper.loadById(activityId);
    return activityMapper.selectByPrimaryKey(activityId, getCurrentUser().getShopId(),
        activity.getType() == ActivityType.PUBLIC_FOREVER);
  }

  @Override
  public Activity selectByPrimaryKey(String activityId) {
    return activityMapper.loadById(activityId);
  }

  @Transactional
  @Override
  public Activity saveActivity(Activity activity) {
    activityMapper.insert(activity);
    return activity;
  }

  @Transactional
  @Override
  public ActivityTicket saveActivityTicket(ActivityTicket ticket) {
    if (ticket.getId() == null) {
      activityTicketMapper.insert(ticket);
    } else {
      activityTicketMapper.update(ticket);
    }

    return activityTicketMapper.selectOne(ticket.getActivityId(), ticket.getShopId());
  }

  @Transactional
  @Override
  public void addProductToPrivateAct(ActivityTicket ticket, String shopId,
      List<CampaignProduct> products) {

    for (CampaignProduct prod : products) {
      //查询该商品是否已在同一时点的其他活动中有出现过
      if (existProductInRange(ticket.getStartTime(), ticket.getEndTime(), prod.getActivityId(),
          prod.getProductId())) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "多个活动的有效时间不能有重叠");
      }
    }

    addProducts(products);
  }

  @Transactional
  @Override
  public void addProductByShopDiscount(Activity activity, Float discount) {
    Shop shop = shopMapper.selectByUserId(getCurrentUser().getId());
    List<CampaignProduct> listCampaignProducts = new ArrayList<CampaignProduct>();
    ActivityTicket ticket = null;
    List<Product> products = productMapper.listProductsByOnsaleAt(shop.getId(), "", "", null, null);
    ticket = getDefaultJoinActivityTicket(null, activity.getId(), null);
    if (products.size() > 0) {
      for (Product product : products) {
        CampaignProduct campaignProduct = new CampaignProduct();
        campaignProduct.setTicketId(ticket.getId());
        campaignProduct.setActivityId(activity.getId());
        campaignProduct.setProductId(product.getId());
        campaignProduct.setDiscount(discount);

        //全店折扣，不记录reduction
//        		campaignProduct.setReduction(activity.getReduction());
        campaignProduct.setActivityAmount(product.getAmount());
        listCampaignProducts.add(campaignProduct);
      }
      //TODO 似乎缺少清空的逻辑
      addProducts(listCampaignProducts);
    }
  }

  @Override
  public List<CampaignProduct> listCampaignProducts(String activityId) {
    ActivityTicket ticket = getDefaultJoinActivityTicket(null, activityId, null);
    // TODO

    if (ticket != null) {
      return campaignProductMapper.selectByTicket(ticket.getId());
    } else {
      return new ArrayList<CampaignProduct>();
    }
  }

  /**
   * 删除活动
   */
  @Override
  public int deleteActivity(String activityId, String userId) {
    if (StringUtils.isBlank(activityId) || StringUtils.isBlank(userId)) {
      return 0;
    }

    Activity activity = checkAct(activityId, userId);

    // 活动商品解锁
    List<Product> products = productMapper.listProductByActivityId(activityId);
    List<String> productIds = new ArrayList<String>();
    for (Product product : products) {
      productIds.add(product.getId());
    }
    if (productIds.size() > 0) {
      productService.unlockProductByIds(productIds);
    }

    // 没有关闭的活动，关闭活动
    if (!ActivityStatus.CLOSED.equals(activity.getStatus())) {
      close(activityId, userId);
    }

    // 删除报名活动
    activityTicketMapper.deleteByActivityId(activityId);

    if (!activity.getType().equals(ActivityType.PRIVATE)) {
      return activityMapper.delete(activityId, null);
    } else {
      return activityMapper.delete(activityId, userId);
    }
  }


  @Override
  public int delete(String activityId) {
    return deleteActivity(activityId, getCurrentUser().getId());
  }

  @Override
  public int insert(Activity activity) {
    return activityMapper.insert(activity);
  }

  @Override
  public int insertOrder(Activity activity) {
    return activityMapper.insert(activity);
  }

  @Override
  public ActivityEX load(String id) {
    Activity activity = activityMapper.loadById(id);
    ActivityEX ex = activityMapper.selectByPrimaryKey(id, getCurrentUser().getShopId(),
        activity.getType() == ActivityType.PUBLIC_FOREVER);
    if (ex == null) {
      ex = new ActivityEX();
      BeanUtils.copyProperties(activity, ex);
    }
    return ex;
  }

  @Override
  public List<ActivityEX> listAll() {
    return activityMapper.selectAll();
  }

  @Override
  public List<ActivityEX> selectPubAndPrivate(String userId, String shopId) {
    return activityMapper.selectPubAndPrivate(userId, shopId);
  }

  @Override
  public List<ActivityVO> listPublicAndPrivate(String userId, String shopId) {
    List<ActivityVO> result = new ArrayList<ActivityVO>();
    List<ActivityEX> list = new ArrayList<ActivityEX>();
    List<ActivityEX> publicForeverList = activityMapper.selectPublicForeverList();
    for (int i = 0; i < publicForeverList.size(); i++) {
      ActivityEX bean = publicForeverList.get(i);
      ActivityEX temp = activityMapper.selectPublicForeverSelf(bean.getId(), shopId);
      if (temp == null) {
        list.add(bean);
      } else {
        list.add(temp);
      }
    }
    list.addAll(activityMapper.selectPubAndPrivate(userId, shopId));
    for (ActivityEX activity : list) {
      result.add(trans2VO(activity));
    }
    return result;
  }

  @Override
  public List<ActivityVO> listPublic() {
    List<ActivityEX> list = activityMapper.selectPub();
    List<ActivityVO> result = new ArrayList<ActivityVO>();
    for (ActivityEX activity : list) {
      result.add(trans2VO(activity));
    }
    return result;
  }

  @Override
  public List<ActivityVO> listByUser(String userId) {
    List<ActivityEX> list = activityMapper.selectByUser(userId);
    List<ActivityVO> result = new ArrayList<ActivityVO>();
    for (ActivityEX activity : list) {
      result.add(trans2VO(activity));
    }
    return result;
  }

  @Override
  public void addProducts(List<CampaignProduct> products) {
    for (CampaignProduct prod : products) {
      campaignProductMapper.deleteActivityProducts(prod.getActivityId(), prod.getProductId());
      campaignProductMapper.insert(prod);
    }
  }

  @Override
  public void removeProducts(String activityId, List<String> productIds) {
    campaignProductMapper
        .deleteActivityProducts(activityId, productIds.toArray(new String[productIds.size()]));
  }

  @Override
  public ActivityVO loadVO(String activityId) {
    ActivityEX activity = load(activityId);
    if (activity == null) {
      return null;
    }
    return trans2VO(activity);
  }

  @Override
  public void insertTicket(ActivityTicket ticket) {
    activityTicketMapper.insert(ticket);
  }

  @Override
  public void updateTicket(ActivityTicket ticket) {
    activityTicketMapper.update(ticket);
  }

  @Override
  public void unjoin(String activityId) {

    Activity activity = activityMapper.loadById(activityId);
    Activity activityEX = activityMapper
        .selectByPrimaryKey(activityId, getCurrentUser().getShopId(),
            activity.getType() == ActivityType.PUBLIC_FOREVER);
    ActivityTicket ticket = null;
    if (activityEX.getType().equals(ActivityType.PUBLIC)) {
      ticket = activityTicketMapper.selectOne(activityEX.getId(), getCurrentUser().getShopId());
    } else {
      // 暂时未支持参加私有的销售活动
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "暂时未支持参加私有的销售活动");
    }

    if (ticket != null) {
      if (ticket.getStatus().equals(ActivityStatus.CLOSED)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动已经结束");
      } else {
        activityTicketMapper.updateAuditStatus(ticket.getId(), ActivityTicketAuditStatus.CANCELLED);
      }
    } else {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "您还未设置参加活动的商品");
    }
  }

  /**
   * 获取活动默认的参与记录
   */
  @Override
  public ActivityTicket getDefaultJoinActivityTicket(String shopId, String activityId,
      ActivityType type) {
    // 获取必要参数，调用该方法的应该传参，这里以后不用再次获取，待改进todo
    if (StringUtils.isBlank(shopId)) {
      shopId = getCurrentUser().getShopId();
    }

    if (type == null) {
      type = activityMapper.loadById(activityId).getType();
    }

    // 验证下这样的活动存不存在
    boolean isForverAct = type == ActivityType.PUBLIC_FOREVER;
    ActivityEX activityEX = activityMapper.selectByPrimaryKey(activityId, shopId, isForverAct);
    if (activityEX == null) {
      return null;
    }

    // 无限期活动，第三方的活动获取已报名的xinx
    if (isForverAct) {
      // 只取报名记录，为了兼容多次报名和多次审核
      return activityTicketMapper.loadJoinTicketByActivityAndShop(activityId, shopId);
    }

    // 个人活动，无需审核
    return activityTicketMapper.selectOne(activityEX.getId(), shopId);
  }


  /**
   * 特价商品是否是特价商品   seker 2015-01-17
   */
  public ProductSP checkProductSP(String productId) {
    ProductSP prod = new ProductSPImpl();
    prod.setFirstActivity(activitySPMapper.getFirstActivitySP(productId));
    //初始化已购数量
    prod.setBoughtAmount(0);

    return prod;
  }


  /**
   * 检查是否是特价商品及用户购买量   seker 2015-01-17
   */
  public ProductSP checkProductSP(String productId, User user) {
    ProductSP prod = checkProductSP(productId);
    //用户未登录 或以匿名用户登录, 不再取已购买量
    if (user == null || user.getLoginname().startsWith("CID")) {
      return prod;
    }
    if (prod.getFirstActivity() == null) {
      return prod;
    }

    int amount = activitySPMapper.getBuyQty(user.getId(), prod.getFirstActivity());
    prod.setBoughtAmount(amount);

    return prod;
  }

  /**
   * 特价商品是否可以购买，只从特价商品维度进行检查， 不检查库存    seker 2015-01-06
   *
   * @param amount 新购买的数量
   * @return 0 不是特价商品可以购买  1 特价商品 允许购买   2还没开放购买  3超过购买上限  4请先登录
   */
  public int checkBuyProductSP(User user, String productId, Integer amount) {
    ProductSP productSP = checkProductSP(productId, user);
    ActivitySP activity = productSP.getFirstActivity();
    if (activity == null) {
      return 0;
    }

    //用户未登录 或以匿名用户登录
    if (user == null || user.getLoginname().startsWith("CID")) {
      return 4;
    }

    if (!productSP.onSale()) {
      return 2;
    }
    if (productSP.overLimit(amount)) {
      return 3;
    }

    return 1;
  }

  @Override
  public int undelete(String id) {
    return 0;
  }


  private void setCheckFlagByAuditStatus(Activity activity, ActivityVO item) {
    // 设置checkflag
    ActivityTicket ticket = getDefaultJoinActivityTicket(null, activity.getId(), null);
    if (activity.getType() == ActivityType.PRIVATE) {
      item.setCheckFlag(ActivityVO.CHECK_FLAG_APPROVED);
    } else if (activity.getType() == ActivityType.PUBLIC) {
      //报名活动的状态
      if (ticket == null || ticket.getAuditStatus().equals(ActivityTicketAuditStatus.CANCELLED)) {
        item.setCheckFlag(ActivityVO.CHECK_FLAG_UNJOINED);
      } else {
        item.setCheckFlag(ActivityVO.CHECK_FLAG_AUDITING);
      }
    } else if (activity.getType() == ActivityType.PUBLIC_FOREVER) {
      if (ticket == null || ticket.getAuditStatus().equals(ActivityTicketAuditStatus.CANCELLED)) {
        item.setCheckFlag(ActivityVO.CHECK_FLAG_UNJOINED);
      } else {
        item.setCheckFlag(ActivityVO.CHECK_FLAG_AUDITING);
      }
    } else {
      throw new BizException(GlobalErrorCode.UNKNOWN, "未知类型的活动");
    }
  }

  @Override
  public void batchUpdateStatusToInProgressPrivate() {
    List<ActivityEX> activities = activityMapper.selectNotStartedToBeInProgress();
    log.info("总共[" + activities.size() + "]件活动准备开始...");
    for (ActivityEX act : activities) {
      try {
        updateStatusToInProgress(act);
        log.info("活动[id=" + act.getId() + ", name=" + act.getName() + "]已经开始，商品价格已经更新为活动价");
      } catch (BizException e) {
        log.error("活动[id=" + act.getId() + ", name=" + act.getName() + "]", e);
      }
    }
  }

  @Override
  public void batchUpdateStatusToInProgressPublic() {
    List<ActivityEX> activities = activityMapper.selectNotStartedToBeInProgressPublic();
    log.info("总共[" + activities.size() + "]件活动准备开始....");
    for (ActivityEX act : activities) {
      try {
        updateStatusToInProgress(act);
        log.info("活动[id=" + act.getId() + ", name=" + act.getName() + "]已经开始，商品价格已经更新为活动价");
      } catch (BizException e) {
        e.printStackTrace();
      }
    }
  }

  private void pushSyncQueue(Long dstFlag, SyncEvent evType, ActivityItem ai) {
    if (ai == null) {
      return;
    }
    KDSyncData ev = new KDSyncData();
    ev.setSiteHost(siteHost);
    ev.setPartnerFlag(dstFlag);
    ev.setEvType(evType);
    List<ActivityItem> aiList = new ArrayList<ActivityItem>();
    aiList.add(ai);
    ev.setActList(aiList);
    syncEventService.publishEvent(ev);
  }

  private List<String> productsToList(List<Product> list) {
    List<String> ret = new ArrayList<String>();
    if (list == null) {
      return ret;
    }

    for (Product p : list) {
      ret.add(p.getId());
    }
    return ret;
  }

  @Transactional
  private void updateProductPrice(String actId, int type, Object obj, ActivityStatus as) {
    Activity act = activityMapper.loadById(actId);

    if (act == null || obj == null) {
      return;
    }
    log.info("开始更新活动[" + act.getId() + "] 价格, 活动类型:" + as.toString());
    List<Product> list = new ArrayList<Product>();
    int cnt = 0;
    if (PreferentialType.SHOP_DISCOUNT == type) {
      ActivityTicket ticket = (ActivityTicket) obj;
      // 全店折扣
      if (as.equals(ActivityStatus.IN_PROGRESS)) {
        cnt = skuMapper.discountByShop(ticket.getShopId(), ticket.getDiscount());
        log.info("店铺sku[shopId=" + ticket.getShopId() + "]价格已经更新，折扣：" + ticket.getDiscount()
            + ",更新sku cnt=" + cnt);
        if (cnt > 0) {
          cnt = productMapper.discountByShop(ticket.getShopId(), ticket.getDiscount());
          log.info("店铺商品[shopId=" + ticket.getShopId() + "]价格已更新，折扣：" + ticket.getDiscount()
              + ", 更新商品cnt=" + cnt);
          list = productService.selectActivityBegin(ticket.getShopId());  // 该店铺下待更新商品

          syncActToThirdpartner(act, as, list);
        }
      } else if (as.equals(ActivityStatus.CLOSED)) {
        cnt = skuMapper.closeDiscountByShop(ticket.getShopId());
        log.info("活动ticket[id=" + ticket.getId() + ", shopid=" + ticket.getShopId() + "]的商品sku[cnt="
            + cnt + "]价格已经恢复");

        if (cnt > 0) {
          cnt = productMapper.closeDiscountByShop(ticket.getShopId());
          log.info(
              "活动ticket[id=" + ticket.getId() + ", shopid=" + ticket.getShopId() + "]的商品[cnt=" + cnt
                  + "]价格已经恢复");
          list = productService.selectActivityEnd(ticket.getShopId());  // 该店铺下待更新商品

          syncActToThirdpartner(act, as, list);
        }
      } else {
        log.error("unknow activity type");
      }
      // 商品折扣
    } else if (PreferentialType.ACTIVITY_PRODUCT_DISCOUNT == type) {
      CampaignProduct cp = (CampaignProduct) obj;
      if (as.equals(ActivityStatus.IN_PROGRESS)) {
        // 先关闭这商品原有正在进行中的活动优惠
        closeProuductDiscount(cp);
        cnt = skuMapper.discountByProduct(cp.getProductId(), cp.getDiscount());
        log.info(
            "sku[productId=" + cp.getProductId() + "]价格已更新，折扣：" + cp.getDiscount() + ",更新sku cnt="
                + cnt);

        if (cnt > 0) {
          cnt = productMapper.discountByProduct(cp.getProductId(), cp.getDiscount());
          log.info(
              "商品[productId=" + cp.getProductId() + "]价格已更新，折扣：" + cp.getDiscount() + ", 更新商品cnt="
                  + cnt);
          list.add(productService.load(cp.getProductId()));

          syncActToThirdpartner(act, as, list);
        }
      } else if (as.equals(ActivityStatus.CLOSED)) {
        cnt = skuMapper.closeDiscountByProduct(cp.getProductId());
        log.info(
            "活动[id=" + act.getId() + "]的商品sku[id=" + cp.getProductId() + "]价格已经恢复" + ",更新sku cnt="
                + cnt);

        if (cnt > 0) {
          cnt = productMapper.closeDiscountByProduct(cp.getProductId());
          log.info(
              "活动[id=" + act.getId() + "]的商品[id=" + cp.getProductId() + "]价格已经恢复" + ", 更新商品cnt="
                  + cnt);
          list.add(productService.load(cp.getProductId()));
          syncActToThirdpartner(act, as, list);

          beginToShopDiscount(cp);
        }
      } else {
        log.error("unknow activity type");
      }
      // 商品减价
    } else if (PreferentialType.PRODUCT_REDUCTION_PRICE == type) {
      CampaignProduct cp = (CampaignProduct) obj;
      // 先关闭这商品原有正在进行中的活动优惠
      if (as.equals(ActivityStatus.IN_PROGRESS)) {
        closeProuductDiscount(cp);
        cnt = skuMapper.reductionByProduct(cp.getProductId(), cp.getReduction());
        log.info(
            "sku[productId=" + cp.getProductId() + "]价格已更新，减价：" + cp.getReduction() + ",更新sku cnt="
                + cnt);

        if (cnt > 0) {
          cnt = productMapper.reductionByProduct(cp.getProductId(), cp.getReduction());
          log.info(
              "商品[productId=" + cp.getProductId() + "]价格已更新，减价：" + cp.getReduction() + ", 更新商品cnt="
                  + cnt);
          list.add(productService.load(cp.getProductId()));
          syncActToThirdpartner(act, as, list);

        }
      } else if (as.equals(ActivityStatus.CLOSED)) {
        cnt = skuMapper.closeDiscountByProduct(cp.getProductId());
        log.info(
            "活动[id=" + act.getId() + "]的商品sku[id=" + cp.getProductId() + "]价格已经恢复" + ",更新sku cnt="
                + cnt);

        if (cnt > 0) {
          cnt = productMapper.closeDiscountByProduct(cp.getProductId());
          log.info(
              "活动[id=" + act.getId() + "]的商品[id=" + cp.getProductId() + "]价格已经恢复" + ", 更新商品cnt="
                  + cnt);
          list.add(productService.load(cp.getProductId()));
          syncActToThirdpartner(act, as, list);

          beginToShopDiscount(cp);
        }
      } else {
        log.error("unknow activity type");
      }
    } else {
    }

  }

  private void syncActToThirdpartner(Activity act, ActivityStatus as, List<Product> list) {
    if (list != null && list.size() != 0) {
      if (syncSwitch == 0) {
        for (Product p : list) {
          //				if(p.getSynchronousFlag()==null){
          //					log.warn("同步标志未设置:" + p.getId());
          //					continue;
          //				}
          //				if ( p.getSynchronousFlag().substring(9,10).isEmpty() || !p.getSynchronousFlag().substring(9,10).equals("1")) { // TODO: 待改进, 没同步到想去不能参与活动
          //					log.warn("没有同步到想去的标识:" + p.getId());
          //					continue;
          //				}
          ActivityItem ai = new ActivityItem();
          ai.setActId(act.getId());
          ai.setActName(act.getName());
          ai.setShopId(p.getShopId());
          ai.setActPrice(p.getPrice());
          ai.setMarkPrice(p.getMarketPrice());
          ai.setProductId(p.getId());

          if (as.equals(ActivityStatus.IN_PROGRESS)) {
            pushSyncQueue(KDSyncData.XIANGQU_DST, SyncEventType.ACTIVITY_START, ai);
          } else if (as.equals(ActivityStatus.CLOSED)) {
            pushSyncQueue(KDSyncData.XIANGQU_DST, SyncEventType.ACTIVITY_CLOSE, ai);
          }
          log.info("活动商品开始同步:" + p.getId());
        } // end for
      } else {
        /**SyncMqEvent event = new SyncMqEvent();
         event.setIds(productsToList(list));
         event.setType(SyncEvType.EV_ACTIVITY.ordinal());
         event.setTimestamp(new Date().getTime());

         if (as.equals(ActivityStatus.IN_PROGRESS)) {
         event.setEvent(1);
         } else if (as.equals(ActivityStatus.CLOSED)) {
         event.setEvent(2);
         }
         syncRocketMq.sendToMQ(event);**/
      }
    } else {
      log.warn("活动同步商品列表出错为空");
    }

  }

  private boolean checkUpdateStatusToInProgress(ActivityEX act) {
    if (act.getPreferentialType().equals(PreferentialType.SHOP_DISCOUNT)) {
      // 全店折扣
      if (act.getDiscount() == null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
            "店铺折扣活动[" + act.getId() + "]检查，折扣数据不能为空");
      }
      if (act.getDiscount() < 0 || act.getDiscount() > 1) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
            "店铺折扣活动[" + act.getId() + "]检查，折扣[" + act.getDiscount() + "]应该是一个大于0小于1的浮点数");
      }
    }
    return true;
  }

  private ActivityTicket shopInActProgressByProdId(String productId, int type) {
    if (productId == null) {
      return null;
    }
    Product p = productService.load(productId);
    if (p == null || p.getShopId() == null) {
      return null;
    }

    List<ActivityTicket> list = activityTicketMapper.selectInProgressByShopId(p.getShopId(), type);
    if (list != null && list.size() != 0) {
      return list.get(0);
    }
    return null;
  }

  private void closeProuductDiscount(CampaignProduct cp) {

    ActivityTicket ticket = shopInActProgressByProdId(cp.getProductId(),
        PreferentialType.SHOP_DISCOUNT);
    if (ticket == null) {
      return;
    }

    log.info("begin closeProuductDiscount...");
    // update price and sync to thired partner
    int cnt = skuMapper.closeDiscountByProduct(cp.getProductId());
    log.info("close sku[productId=" + cp.getProductId() + "]价格已恢复，折扣： " + ticket.getDiscount()
        + ",更新sku cnt=" + cnt);

    if (cnt > 0) {
      cnt = productMapper.closeDiscountByProduct(cp.getProductId());
      log.info("close 商品[productId=" + cp.getProductId() + "]价格已恢复，折扣：" + ticket.getDiscount()
          + ", 更新商品cnt=" + cnt);
    }

    Activity act = activityMapper.loadById(ticket.getActivityId());
    List<Product> list = new ArrayList<Product>();
    list.add(productService.load(cp.getProductId()));
    syncActToThirdpartner(act, ActivityStatus.CLOSED, list);
    log.info("end closeProuductDiscount...");
  }

  /**
   * seker 2015-05-07 当商品的优惠结束时  如果对应商店已经在活动中 启用活动优惠
   */
  private void beginToShopDiscount(CampaignProduct cp) {
    //判断对应店铺是否有全店折扣已经在in_progress
    ActivityTicket ticket = shopInActProgressByProdId(cp.getProductId(),
        PreferentialType.SHOP_DISCOUNT);
    if (ticket != null) {
      // update price and sync to thired partner
      log.info("beginToShopDiscount...");
      int cnt = skuMapper.discountByProduct(cp.getProductId(), ticket.getDiscount());
      log.info("begin sku[productId=" + cp.getProductId() + "]价格已更新，折扣：" + ticket.getDiscount()
          + ",更新sku cnt=" + cnt);

      if (cnt > 0) {
        cnt = productMapper.discountByProduct(cp.getProductId(), ticket.getDiscount());
        log.info("begin 商品[productId=" + cp.getProductId() + "]价格已更新，折扣：" + ticket.getDiscount()
            + ", 更新商品cnt=" + cnt);
      }
      Activity act = activityMapper.loadById(ticket.getActivityId());
      List<Product> list = new ArrayList<Product>();
      list.add(productService.load(cp.getProductId()));
      syncActToThirdpartner(act, ActivityStatus.IN_PROGRESS, list);
      log.info("end beginToShopDiscount...");
    }
  }

  @Transactional
  private void updateStatusToInProgress(ActivityEX act) {
    // 准备开始的活动的数据完整性校验，数据不正确，退出
    checkUpdateStatusToInProgress(act);

    activityMapper.updateStatusFromStatus(act.getId(), ActivityStatus.NOT_STARTED,
        ActivityStatus.IN_PROGRESS);
    log.info("活动[id=" + act.getId() + ", preferentialType=" + act.getPreferentialType() + "] 已经开始");

    List<ActivityTicket> tickets = activityTicketMapper.selectNotStartedToBeInProgress(act.getId());
    if (tickets == null || tickets.size() == 0) {
      log.warn("活动已开始,但没找到tickets");
      return;
    }
    for (ActivityTicket ticket : tickets) {
      activityTicketMapper.updateStatusFromStatus(ticket.getId(), ActivityStatus.NOT_STARTED,
          ActivityStatus.IN_PROGRESS);
      log.info("活动ticket[id=" + ticket.getId() + "] 已经开始");
      if (ticket.getPreferentialType().equals(PreferentialType.SHOP_DISCOUNT)) {
        // 全店折扣
        updateProductPrice(act.getId(), PreferentialType.SHOP_DISCOUNT, ticket,
            ActivityStatus.IN_PROGRESS);
      } else {
        List<CampaignProduct> cps = campaignProductMapper.selectByTicket(ticket.getId());
        if (ticket.getPreferentialType().equals(PreferentialType.ACTIVITY_PRODUCT_DISCOUNT)) {
          // 活动商品折扣
          for (CampaignProduct cp : cps) {
            updateProductPrice(act.getId(), PreferentialType.ACTIVITY_PRODUCT_DISCOUNT, cp,
                ActivityStatus.IN_PROGRESS);
          }
        } else if (ticket.getPreferentialType().equals(PreferentialType.PRODUCT_REDUCTION_PRICE)) {
          // 活动商品减价
          for (CampaignProduct cp : cps) {
            updateProductPrice(act.getId(), PreferentialType.PRODUCT_REDUCTION_PRICE, cp,
                ActivityStatus.IN_PROGRESS);
          }
        } else {
          log.error("活动[id=" + act.getId() + "]getPreferentialType为空，数据有问题！");
        }
      }
    }
  }

  @Transactional
  @Override
  public void batchUpdateStatusNotStartedToClosed() {
//        List<ActivityEX> activities = activityMapper.selectNotStartedToBeClosed();
//        log.info("总共[" + activities.size() + "]件活动准备从未开始到关闭状态");
//        for (ActivityEX act : activities) {
    updateStatusFromNotStartedToClosed();
//            log.info("活动[id=" + act.getId() + ", name=" + act.getName() + "]已经关闭");
//        }
  }

  private void updateStatusFromNotStartedToClosed() {
    //TODO 拆分成活动单独更新状态
//        activityMapper.updateStatusFromStatus(act.getId(), ActivityStatus.NOT_STARTED, ActivityStatus.CLOSED);
    List<ActivityTicket> tickets = activityTicketMapper.selectNotStartedToBeClosed();
    for (ActivityTicket ticket : tickets) {
      activityTicketMapper.updateStatusFromStatus(ticket.getId(), ActivityStatus.NOT_STARTED,
          ActivityStatus.CLOSED);
      log.info("活动ticket[id=" + ticket.getId() + "]状态已更新为：" + ActivityStatus.CLOSED);
    }
  }

  public void batchUpdateStatusInProgressToClosed() {
//        List<ActivityEX> activities = activityMapper.selectInProgressToBeClosed();
//        log.info("总共[" + activities.size() + "]件活动准备从开始到关闭状态");
//        for (ActivityEX act : activities) {
    updateStatusFromInProgresssToClosed();
//            log.info("活动[id=" + act.getId() + ", name=" + act.getName() + "]已经关闭， 商品价格恢复");
//        }
  }

  private void updateStatusFromInProgresssToClosed() {
    //TODO 拆分成活动单独更新状态
//        activityMapper.updateStatusFromStatus(act.getId(), ActivityStatus.IN_PROGRESS, ActivityStatus.CLOSED);
    List<ActivityTicket> tickets = activityTicketMapper.selectInProgressToBeClosed();
    for (ActivityTicket ticket : tickets) {
      int count = activityTicketMapper
          .updateStatusFromStatus(ticket.getId(), ActivityStatus.IN_PROGRESS,
              ActivityStatus.CLOSED);
      if (count > 0) {
        log.info("活动ticket[id=" + ticket.getId() + "]关闭");
      } else {
        log.warn("活动ticket[id=" + ticket.getId() + "]没有更新");
      }

      if (ticket.getPreferentialType().equals(PreferentialType.SHOP_DISCOUNT)) {
        // 恢复全店折扣
        updateProductPrice(ticket.getActivityId(), PreferentialType.SHOP_DISCOUNT, ticket,
            ActivityStatus.CLOSED);
      } else {
        List<CampaignProduct> cps = campaignProductMapper.selectByTicket(ticket.getId());
        for (CampaignProduct cp : cps) {
          if (ticket.getPreferentialType().equals(PreferentialType.ACTIVITY_PRODUCT_DISCOUNT)) {
            updateProductPrice(ticket.getActivityId(), PreferentialType.ACTIVITY_PRODUCT_DISCOUNT,
                cp, ActivityStatus.CLOSED);
          } else if (ticket.getPreferentialType()
              .equals(PreferentialType.PRODUCT_REDUCTION_PRICE)) {
            updateProductPrice(ticket.getActivityId(), PreferentialType.PRODUCT_REDUCTION_PRICE, cp,
                ActivityStatus.CLOSED);
          } else {
          }
        }
      }
      Activity act = activityMapper.loadById(ticket.getActivityId());
      // 永久报名活动结束后, 删除报名信息以便后面报名还能重新设置折扣信息
      if (act != null && act.getType().equals(ActivityType.PUBLIC_FOREVER)) {
        List<ActivityTicket> list = activityTicketMapper
            .loadAllTicketByActId(ticket.getActivityId());
        String submitId = "";
        int cnt = 0;
        for (ActivityTicket at : list) {
          if (ActivityTicketAuditStatus.SUBMITTED.equals(at.getAuditStatus())) {
            submitId = at.getId();
            continue;
          }
          if (!ActivityStatus.CLOSED.equals(at.getStatus()) && ActivityTicketAuditStatus.APPROVED
              .equals(at.getAuditStatus())) {
            cnt++;
          }
        } // end for
        if (cnt == 0) { // 本条报名记录已经处于关闭状态
          // TODO:
          log.debug("删除报名记录以便再次参加永久活动时可以重新设置折扣值");

          List<String> ids = new ArrayList<String>();
          ids.add(submitId);
          ids.add(ticket.getId());
          activityTicketMapper.deleteByIds(ids.toArray(new String[ids.size()]));
        }
      } // end if public_forever
    } // end for in_progress to closed
  }

  @Override
  public void close(String activityId) {
    checkAct(activityId, getCurrentUser().getId());
    close(activityId, getCurrentUser().getId());
  }

  @Override
  public void close(String activityId, String userId) {
    //关闭指定活动
    activityMapper.updateStatusToClosedById(activityId);

    //根据ticket恢复价格
    List<ActivityTicket> ticketList = activityTicketMapper.loadAllTicketByActId(activityId);
    if (ticketList == null) {
      return;
    }

    for (int i = 0; i < ticketList.size(); i++) {
      ActivityTicket ticket = ticketList.get(i);

      // 优惠券在服务期内
      if (ticket.getStatus() == ActivityStatus.IN_PROGRESS) {
        // 优惠券的折扣类型
        if (ticket.getPreferentialType().equals(PreferentialType.SHOP_DISCOUNT)) {
          // 恢复全店折扣
          updateProductPrice(ticket.getActivityId(), PreferentialType.SHOP_DISCOUNT, ticket,
              ActivityStatus.CLOSED);
        } else {
          List<CampaignProduct> cps = campaignProductMapper.selectByTicket(ticket.getId());
          for (CampaignProduct cp : cps) {
            if (ticket.getPreferentialType().equals(PreferentialType.ACTIVITY_PRODUCT_DISCOUNT)) {
              updateProductPrice(ticket.getActivityId(), PreferentialType.ACTIVITY_PRODUCT_DISCOUNT,
                  cp, ActivityStatus.CLOSED);
            } else if (ticket.getPreferentialType()
                .equals(PreferentialType.PRODUCT_REDUCTION_PRICE)) {
              updateProductPrice(ticket.getActivityId(), PreferentialType.PRODUCT_REDUCTION_PRICE,
                  cp, ActivityStatus.CLOSED);
            }
          }
        }
      }
      activityTicketMapper.updateStatusToClosedById(ticket.getId());
    }

  }

  private Activity checkAct(String activityId, String userId) {
    Activity activity = activityMapper.loadById(activityId);
    if (activity == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动不存在");
    }

    if (activity.getType().equals(ActivityType.PRIVATE) && !activity.getCreatorId()
        .equals(userId)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "你没有权限操作该活动");
    }

    return activity;

  }

  @Override
  public Activity obtainProductCurrentActivities(String productId) {
    Product product = productMapper.selectByPrimaryKey(productId);
    if (product == null) {
      return null;
    } else {
      return activityMapper
          .obtainProductCurrentActivities(Long.toString(IdTypeHandler.decode(productId)));
    }
  }

  @Override
  public void optActivityProducts(String activityId, String userId) {
    String shopId = "";
    if (!StringUtils.isBlank(userId)) {
      User user = userService.load(userId);
      shopId = user.getShopId();
    } else {
      shopId = getCurrentUser().getShopId();
    }

    Activity activity = activityMapper.loadById(activityId);
    //todo
    Activity activityEX = activityMapper
        .selectByPrimaryKey(activityId, shopId, activity.getType() == ActivityType.PUBLIC_FOREVER);

    if (activityEX == null || activityEX.getStatus() == null) {
      return;
    }

    List<Product> products = productMapper.listProductByActivityId(activityId);
    List<String> productIds = new ArrayList<String>();
    for (Product product : products) {
      productIds.add(product.getId());
    }

    campaignProductMapper
        .deleteActivityProducts(activityId, productIds.toArray(new String[productIds.size()]));
  }


  @Override
  public void optActivityProducts(String activityId) {
    log.debug("in optActivityProducts... actId:" + activityId);
    Activity activity = activityMapper.loadById(activityId);
    Activity activityEX = activityMapper
        .selectByPrimaryKey(activityId, getCurrentUser().getShopId(),
            activity.getType() == ActivityType.PUBLIC_FOREVER);
    List<Product> products = new ArrayList<Product>();
    List<String> productIds = new ArrayList<String>();
    if (activityEX == null || activityEX.getStatus() == null) {
      return;
    }

    products = productMapper.listProductByActivityId(activityId);
    if (activityEX.getStatus().equals(ActivityStatus.NOT_STARTED)) {
      for (Product product : products) {
        productIds.add(product.getId());
      }
      campaignProductMapper
          .deleteActivityProducts(activityId, productIds.toArray(new String[productIds.size()]));
      productService.unlockProductByIds(productIds);
    } else {
      removeProducts(activityId, productIds);
    }
  }

  private ActivityVO trans2VO(ActivityEX activity) {
    ActivityVO vo = new ActivityVO();
    if (activity == null) {
      return vo;
    }
    BeanUtils.copyProperties(activity, vo);
    if (StringUtils.isNotBlank(activity.getImg())) {
      vo.setImgUrl(activity.getImg());
    }
    if (StringUtils.isNotBlank(activity.getBanner())) {
      vo.setBannerUrl(activity.getBanner());
    }
    if (activity.getChannel() == ActivityChannel.PRIVATE) {
      vo.setOwnerName(userService.load(activity.getCreatorId()).getName());
      vo.setUrl(domainName + "/activity/" + activity.getId());
    } else {
      vo.setOwnerName(activity.getChannel() != null ? activity.getChannel().toString()
          : PartnerConstants.XIANGQU);
    }
    setCheckFlagByAuditStatus(activity, vo);

    return vo;
  }

  @Override
  public ActivityEX loadActivityEx(String activityId, String shopId, ActivityType activityType) {
    return activityMapper.loadActivityEx(activityId, shopId, activityType);
  }

  @Override
  public void updateStatusToInProgress() {
    activityMapper.updateStatusToInProgress();
  }

  @Override
  public void updateStatusToClosed() {
    activityMapper.updateStatusToClosed();
  }

  @Override
  public List<CampaignProductEX> loadCampaignProductEX(String activityId, String shopId) {
    return campaignProductMapper.loadCampaignProductEX(activityId, shopId);
  }

  @Override
  public Long countActivitysByQuery(Map<String, Object> paramsMap) {
    return activityMapper.countActivitysByQuery(paramsMap);
  }

  @Override
  public List<Activity> listActivitysByQuery(Map<String, Object> paramsMap, Pageable pager) {
    return activityMapper.listActivitysByQuery(paramsMap, pager);
  }

  @Override
  public Long countCampaignProductByQuery(Map<String, Object> paramsMap) {
    return campaignProductMapper.countCampaignProductByQuery(paramsMap);
  }

  @Override
  public List<CampaignProductEX> listCampaignProductByQuery(Map<String, Object> paramsMap,
      Pageable pager) {
    List<CampaignProductEX> result = campaignProductMapper
        .listCampaignProductByQuery(paramsMap, pager);
    if (result == null) {
      return Collections.emptyList();
    }

    return result;
  }

  @Override
  public Long countCampaignProduct4Home(Map<String, Object> paramsMap) {
    return campaignProductMapper.countCampaignProduct4Home(paramsMap);
  }

  @Override
  public List<XQHomeActProductVO> listCampaignProduct4Home(Map<String, Object> paramsMap,
      Pageable page) {
    List<XQHomeActProductVO> result = campaignProductMapper
        .listCampaignProduct4Home(paramsMap, page);
    if (result == null) {
      return Collections.emptyList();
    }

    return result;
  }

  @Override
  public void auditTicketProduct(String newTicketId, String activityId, String oldTicketId,
      String productId,
      String brand, String shortName, Integer sort, String imagePc, String imageApp) {

    campaignProductMapper
        .auditTicketProduct(newTicketId, activityId, oldTicketId, productId, brand, shortName, sort,
            imagePc, imageApp);
  }

  /**
   * 某个商品的活动在时间上是否有重叠
   */
  @Override
  public boolean existProductInRange(Date start, Date end, String activityId, String productId) {
    return campaignProductMapper.selectCountByRange(start, end, activityId, productId) > 0;
  }

  /**
   * 获取没审核的
   */
  @Override
  public ActivityTicket loadSubmittedTicket4Audit(String activityId, String productId) {
    return activityTicketMapper.loadSubmittedTicket4Audit(activityId, productId);
  }

  /**
   * 获取审核过的商品
   */
  @Override
  public ActivityTicket loadTicket4Audit(String activityId, String productId) {
    return activityTicketMapper.loadTicket4Audit(activityId, productId);
  }

  @Override
  public void updateDiscountByTicket(String ticketId, Float discount) {
    campaignProductMapper.updateDisCountByTicket(ticketId, discount);
  }

  @Override
  public Activity update(Activity activity, String userId, String shopId) {
    // 获取数据库中活动
    Activity dbAct = activityMapper.loadById(activity.getId());
    if (dbAct == null) {
      log.error("您要修改的活动[" + activity.getId() + "]不存在");
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "您要修改的活动不存在");
    }

    // 获取活动的报名信息   todo需要获取 店铺id避免后面根据当前用户来获取
    ActivityTicket ticket = getDefaultJoinActivityTicket(shopId, activity.getId(), dbAct.getType());
    if (ticket != null && (ticket.getStatus().equals(ActivityStatus.IN_PROGRESS) || ticket
        .getStatus().equals(ActivityStatus.CLOSED))) {
      log.error("您要修改的活动[" + activity.getId() + "]状态不可编辑");
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "已经开始/结束的活动不能编辑");
    }

    // 店铺自己的活动
    if (dbAct.getType() == ActivityType.PRIVATE) {
      if (!dbAct.getCreatorId().equals(userId)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "用户没有修改活动的权限");
      }

      if (existInRange(userId, activity.getStartTime(), activity.getEndTime(), activity.getId())) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "多个活动的有效时间不能有重叠");
      }
    }

    activityMapper.update(activity);
    return activity;
  }

  @Override
  public Activity update(Activity activity) {
    return update(activity, getCurrentUser().getId(), getCurrentUser().getShopId());
  }

  @Override
  public boolean existInRange(String userId, Date from, Date to, String excludeId) {
    return activityMapper.selectCountByRange(userId, from, to, excludeId) > 0;
  }

  @Override
  public ActivityTicket getDefaultJoinActivityTicket(String actId) {
    return getDefaultJoinActivityTicket(null, actId, null);
  }

  @Override
  public List<CampaignProduct> getCampaignByProdcut(String productId) {
    List<CampaignProduct> cps = campaignProductMapper.selectByProdcut(productId);
    if (cps == null) {
      return Collections.emptyList();
    }

    return cps;
  }

  @Override
  public void removeCampaignAndTicketByProductId(String productId) {
    List<CampaignProduct> cps = getCampaignByProdcut(productId);

    // 不存在活动报名信息
    if (cps.isEmpty()) {
      return;
    }

    // 获取ticket
    List<String> ticketIds = new ArrayList<String>();
    for (CampaignProduct p : cps) {
      ticketIds.add(p.getTicketId());
    }

    // 删除ticket
    activityTicketMapper.deleteByIds(ticketIds.toArray(new String[ticketIds.size()]));

    // 删除CampaignProduct
    List<String> org = new ArrayList<String>();
    org.add(productId);
    removeProducts(null, org);
  }

}
