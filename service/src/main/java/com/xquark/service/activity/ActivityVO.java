package com.xquark.service.activity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import com.xquark.dal.vo.ActivityEX;

public class ActivityVO extends ActivityEX {

  private static final long serialVersionUID = -1281549060486585345L;

  private String ownerName;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String imgUrl = "";

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String bannerUrl = "";

  private Integer checkFlag = 0;

  public static final int CHECK_FLAG_REJECTED = -1;
  public static final int CHECK_FLAG_UNJOINED = 0;
  public static final int CHECK_FLAG_AUDITING = 1;
  public static final int CHECK_FLAG_APPROVED = 2;

  private Date serverTime = new Date();

  public String getOwnerName() {
    return ownerName;
  }

  public void setOwnerName(String ownerName) {
    this.ownerName = ownerName;
  }

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }

  public String getBannerUrl() {
    return bannerUrl;
  }

  public void setBannerUrl(String bannerUrl) {
    this.bannerUrl = bannerUrl;
  }

  public Date getServerTime() {
    return serverTime;
  }

  public void setServerTime(Date serverTime) {
    this.serverTime = serverTime;
  }

  public Integer getCheckFlag() {
    return checkFlag;
  }

  public void setCheckFlag(Integer checkFlag) {
    this.checkFlag = checkFlag;
  }

}
