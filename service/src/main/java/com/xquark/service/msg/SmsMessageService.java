package com.xquark.service.msg;

import com.xquark.dal.model.SmsMessage;

public interface SmsMessageService {

  int insert(SmsMessage message);

  int reSend();
}
