package com.xquark.service.msg.impl;

import com.xquark.dal.mapper.HelperMessageMapper;
import com.xquark.dal.model.HelperMessage;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.msg.HelperMessageService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

//@Service("helperService")
public class HelperMessageServiceImpl extends BaseServiceImpl implements HelperMessageService {

  @Autowired
  private HelperMessageMapper helperMessageMapper;


  @Override
  public List<HelperMessage> loadHelperList(Pageable page) {
    return helperMessageMapper.selectAll(page);
  }

  @Override
  public int insert(HelperMessage record, String icon) {
    return helperMessageMapper.insert(record, icon);
  }

  @Override
  public int update(HelperMessage record, String id, String icon) {
    return helperMessageMapper.update(record, id, icon);
  }

  @Override
  public Boolean delete(String id) {
    return helperMessageMapper.delete(id);
  }

  @Override
  public List<HelperMessage> listMessageByAdmin(Map<String, Object> params, Pageable page) {
    return helperMessageMapper.listMessageByAdmin(params, page);
  }

  @Override
  public Long countMessageByAdmin(Map<String, Object> params) {
    return helperMessageMapper.countMessageByAdmin(params);
  }

}
