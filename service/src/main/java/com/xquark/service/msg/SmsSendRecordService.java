package com.xquark.service.msg;

import com.xquark.dal.model.SmsSendRecord;

public interface SmsSendRecordService {

  int insert(SmsSendRecord record);

  int updateSendResult(String mobile, String thirdBatchId, String status, String thirdResult);
}
