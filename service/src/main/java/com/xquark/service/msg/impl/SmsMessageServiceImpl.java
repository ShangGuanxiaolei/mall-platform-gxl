package com.xquark.service.msg.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.SmsMessageMapper;
import com.xquark.dal.model.SmsMessage;
import com.xquark.dal.status.SmsMessageStatus;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.msg.SmsMessageService;
import com.xquark.service.push.PushService;

@Service("smsMessageService")
public class SmsMessageServiceImpl extends BaseServiceImpl implements SmsMessageService {

  @Autowired
  SmsMessageMapper smsMessageMapper;

  @Autowired
  private PushService pushService;

  @Override
  public int insert(SmsMessage message) {
    return smsMessageMapper.insert(message);
  }

  @Override
  public int reSend() {
    int retVal = 0;
    boolean ret = false;
    List<SmsMessage> messages = smsMessageMapper.selectFail();
    for (SmsMessage message : messages) {
      if (message.getPhone() == null) {
        ret = true;
      } else {
        ret = pushService.sendSmsEngine(message.getPhone(), message.getContent());
        try {
          Thread.sleep(500);
        } catch (Exception e) {
          log.error("短信重发失败", e);
        }
      }
      if (message.getCount() >= 5) {
        ret = true;
      }
      if (ret) {
        SmsMessage newMessage = new SmsMessage();
        newMessage.setId(message.getId());
        newMessage.setStatus(SmsMessageStatus.SUCCESS);
        smsMessageMapper.update(newMessage);
        retVal++;
      } else {
        SmsMessage newMessage = new SmsMessage();
        newMessage.setId(message.getId());
        newMessage.setStatus(SmsMessageStatus.FAIL);
        smsMessageMapper.update(newMessage);
      }
    }
    return retVal;
  }
}
