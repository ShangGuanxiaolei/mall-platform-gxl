package com.xquark.service.msg;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.HelperMessage;

public interface HelperMessageService {

  /**
   * 客户端获取小助手信息
   */
  List<HelperMessage> loadHelperList(Pageable page);

  /**
   * 新增小助手内容
   */
  int insert(HelperMessage record, String icon);

  /**
   * 编辑小助手内容
   */
  int update(HelperMessage record, String id, String icon);

  /**
   * 删除小助手内容
   */
  Boolean delete(String id);

  /**
   * 信息列表，用于后台管理
   */
  List<HelperMessage> listMessageByAdmin(Map<String, Object> params, Pageable page);

  Long countMessageByAdmin(Map<String, Object> params);

}
