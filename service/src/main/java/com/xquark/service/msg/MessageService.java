package com.xquark.service.msg;

import com.xquark.aop.anno.NotNull;
import com.xquark.dal.model.Message;
import com.xquark.service.msg.vo.UserMessageVO;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

public interface MessageService {

  /**
   * 群发系统消息，不指定具体的接收人
   */
  Message sendSystemMessage(String title, String content);

  boolean saveNotification(@NotNull Message message);

  boolean updateNotification(@NotNull Message message);

  UserMessageVO sendNewNotification(String toUserId, String title, String content);

  UserMessageVO sendNotificationWithId(String toUserId, String msgId);

  void sendNotification(String msgId);

  void sendNotification(@NotNull List<String> toUserIds, String msgId);

  void sendNotification(@NotNull List<String> toUserIds, String title, String content);

  /**
   * 发送系统消息
   */
  UserMessageVO sendSystemMessage(String toUserId, String title, String content);

  /**
   * 发送消息
   */
  UserMessageVO sendUserMessage(String from, String to, String title, String content,
      String replyTo);

  /**
   * 获取信息
   */
  UserMessageVO loadUserMessage(String id);

  /**
   * 获取信息
   */
  Message loadMessage(String id);

  Boolean delete(String id);

  /**
   * 删除信息
   */
  void removeUserMessage(String id);

  /**
   * 获取收件箱信息
   */
  List<UserMessageVO> loadInboxMessages();

  /**
   * 获取发件箱信息
   */
  List<UserMessageVO> loadOutboxMessages();

  /**
   * 对话的方式展示信息
   */
  List<UserMessageVO> loadAllMessages();

  /**
   * 信息列表，用于后台管理
   */
  List<Message> listMessageByAdmin(Map<String, ?> params, Pageable page);

  Long countMessageByAdmin(Map<String, ?> params);

  /**
   * 发送系统消息
   */
  void sendMessage(String toUserId, String msgId);

  /**
   * 获取用户所有未读消息
   */
  long countUnreadByUser(String userId);

  /**
   * 更新用户消息为已读
   */
  void updateUserAllRead(String userId);

  /**
   * 发送帮砍价系统消息
   */
  void sendBargainMessage(String fromUserId, String toUserId, String msgId, String content);

}
