package com.xquark.service.msg.vo;

import com.xquark.dal.model.Message;
import com.xquark.dal.model.UserMessage;
import org.apache.commons.lang3.time.DateFormatUtils;

public class UserMessageVO extends UserMessage {

  private static final long serialVersionUID = -1925981235494231898L;

  private Message message;

  public Message getMessage() {
    return message;
  }

  public void setMessage(Message message) {
    this.message = message;
  }

  private String createdAtStr;

  public String getCreatedAtStr() {
    if (this.getCreatedAt() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getCreatedAt(), "yyyy-MM-dd HH:mm:ss");
  }

}
