package com.xquark.service.msg.impl;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.xquark.aop.anno.NotNull;
import com.xquark.dal.mapper.MessageMapper;
import com.xquark.dal.mapper.UserMessageMapper;
import com.xquark.dal.mapper.UserNotificationMapper;
import com.xquark.dal.model.Message;
import com.xquark.dal.model.UserMessage;
import com.xquark.dal.status.MessageStatus;
import com.xquark.dal.status.UserMessageStatus;
import com.xquark.dal.type.MessageType;
import com.xquark.helper.Transformer;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.msg.MessageService;
import com.xquark.service.msg.vo.UserMessageVO;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service("messageService")
public class MessageServiceImpl extends BaseServiceImpl implements MessageService {

  @Autowired
  private MessageMapper messageMapper;

  @Autowired
  private UserMessageMapper userMessageMapper;

  @Autowired
  private UserNotificationMapper userNotificationMapper;

  @Override
  public Message sendSystemMessage(String title, String content) {
    Message msg = new Message();
    msg.setTitle(title);
    msg.setContent(content);
    msg.setType(MessageType.SYSTEM);
    msg.setStatus(MessageStatus.VALID);
    msg.setSessId(UUID.randomUUID().toString());
    messageMapper.insert(msg);
    return msg;
  }

  @Override
  public boolean saveNotification(Message message) {
    Preconditions.checkNotNull(message);
    message.setType(MessageType.NOTIFICATION);
    message.setStatus(MessageStatus.VALID);
    message.setSessId("");
    return messageMapper.insert(message) > 0;
  }

  @Override
  public boolean updateNotification(Message message) {
    return messageMapper.updateByPrimaryKeySelective(message) > 0;
  }

  @Override
  public UserMessageVO sendNewNotification(String toUserId, String title, String content) {
    final Message msg = new Message();
    msg.setTitle(title);
    msg.setContent(content);
    msg.setType(MessageType.NOTIFICATION);
    msg.setStatus(MessageStatus.VALID);
    msg.setSessId(UUID.randomUUID().toString());
    messageMapper.insert(msg);
    return sendMessageToUSer(toUserId, msg);
  }

  @Override
  public UserMessageVO sendNotificationWithId(String toUserId, String msgId) {
    final Message message = messageMapper.selectByPrimaryKey(msgId);
    if (message == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "消息不存在");
    }
    return sendMessageToUSer(toUserId, message);
  }


  @Override
  public void sendNotification(String msgId) {
    List<String> toUserIds = userNotificationMapper.listUSerIdByMsgId(msgId);
    if (CollectionUtils.isEmpty(toUserIds)) {

    }
  }

  @Override
  public void sendNotification(@NotNull List<String> toUserIds, String msgId) {
    Message message = messageMapper.selectByPrimaryKey(msgId);
    if (message == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "消息不存在");
    }
    sendNotifications(toUserIds, message);
  }

  @Override
  public void sendNotification(@NotNull List<String> toUserIds, String title, String content) {
    final Message msg = new Message();
    msg.setTitle(title);
    msg.setContent(content);
    msg.setType(MessageType.NOTIFICATION);
    msg.setStatus(MessageStatus.VALID);
    msg.setSessId(UUID.randomUUID().toString());
    messageMapper.insert(msg);

    sendNotifications(toUserIds, msg);
  }

  private void sendNotifications(final List<String> toUserIds, final Message message) {
    assert toUserIds != null;
    assert message != null;
    final List<UserMessage> ums = Transformer.fromIterable(toUserIds,
        new Function<String, UserMessage>() {
          @Override
          public UserMessage apply(String toUserId) {
            UserMessage um = new UserMessage();
            um.setToUserId(toUserId);
            um.setMsgId(message.getId());
            um.setStatus(UserMessageStatus.UNREAD);
            return um;
          }
        });
    // 开一个线程后台保存消息不阻塞当前线程返回
    ExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    try {
      executorService.execute(new Runnable() {
        @Override
        public void run() {
          userMessageMapper.insertList(ums);
        }
      });
    } finally {
      executorService.shutdown();
    }
  }

  @Override
  public UserMessageVO sendSystemMessage(String toUserId, String title, String content) {
    final Message msg = new Message();
    msg.setTitle(title);
    msg.setContent(content);
    msg.setType(MessageType.SYSTEM);
    msg.setStatus(MessageStatus.VALID);
    msg.setSessId(UUID.randomUUID().toString());
    messageMapper.insert(msg);
    return sendMessageToUSer(toUserId, msg);
  }

  private UserMessageVO sendMessageToUSer(String toUserId, Message msg) {
    UserMessage um = new UserMessage();
    um.setToUserId(toUserId);
    um.setMsgId(msg.getId());
    um.setStatus(UserMessageStatus.UNREAD);
    userMessageMapper.insert(um);

    UserMessageVO result = new UserMessageVO();
    BeanUtils.copyProperties(um, result);
    result.setMessage(msg);
    return result;
  }

  @Override
  public UserMessageVO sendUserMessage(String from, String to, String title, String content,
      String replyTo) {
    Message msg = new Message();
    msg.setTitle(title);
    msg.setContent(content);
    msg.setType(MessageType.BUSSINESS);
    msg.setStatus(MessageStatus.VALID);
    if (!StringUtils.isEmpty(replyTo)) {
      Message parentMsg = messageMapper.selectByPrimaryKey(replyTo);
      msg.setReplyTo(replyTo);
      msg.setSessId(parentMsg.getId());
    } else {
      msg.setSessId(UUID.randomUUID().toString());
    }
    msg.setReplyTo(replyTo);
    messageMapper.insert(msg);

    UserMessage um = new UserMessage();
    um.setFromUserId(from);
    um.setToUserId(to);
    um.setMsgId(msg.getId());
    um.setStatus(UserMessageStatus.UNREAD);
    userMessageMapper.insert(um);

    UserMessageVO result = new UserMessageVO();
    BeanUtils.copyProperties(um, result);
    result.setMessage(msg);
    return result;
  }

  /**
   * 发送系统消息
   */
  @Override
  public void sendMessage(String toUserId, String msgId) {
    UserMessage um = new UserMessage();
    um.setToUserId(toUserId);
    um.setMsgId(msgId);
    um.setStatus(UserMessageStatus.UNREAD);
    userMessageMapper.insert(um);
  }

  @Override
  public UserMessageVO loadUserMessage(String id) {
    UserMessage um = userMessageMapper.selectByPrimaryKey(id);
    Message msg = messageMapper.selectByPrimaryKey(um.getMsgId());

    UserMessageVO result = new UserMessageVO();
    BeanUtils.copyProperties(um, result);
    result.setMessage(msg);
    return result;
  }

  @Override
  public Message loadMessage(String id) {
    return messageMapper.selectByPrimaryKey(id);
  }

  @Override
  public Boolean delete(String id) {
    return messageMapper.delete(id) > 0;
  }

  @Override
  public void removeUserMessage(String id) {
    userMessageMapper.updateUserMessageStatus(id, UserMessageStatus.DELETED);
  }

  @Override
  public List<UserMessageVO> loadInboxMessages() {
    List<UserMessage> list = userMessageMapper.selectByReceiver(getCurrentUser().getId());
    List<UserMessageVO> result = new ArrayList<UserMessageVO>();
    for (UserMessage um : list) {
      UserMessageVO r = new UserMessageVO();
      BeanUtils.copyProperties(um, r);
      Message msg = messageMapper.selectByPrimaryKey(um.getMsgId());
      r.setMessage(msg);
      result.add(r);
    }
    return result;
  }

  @Override
  public List<UserMessageVO> loadOutboxMessages() {
    List<UserMessage> list = userMessageMapper.selectBySender(getCurrentUser().getId());
    List<UserMessageVO> result = new ArrayList<UserMessageVO>();
    for (UserMessage um : list) {
      UserMessageVO r = new UserMessageVO();
      BeanUtils.copyProperties(um, r);
      Message msg = messageMapper.selectByPrimaryKey(um.getMsgId());
      r.setMessage(msg);
      result.add(r);
    }
    return result;
  }

  @Override
  public List<UserMessageVO> loadAllMessages() {
    List<UserMessage> list = userMessageMapper.selectByUser(getCurrentUser().getId());
    List<UserMessageVO> result = new ArrayList<>();
    for (UserMessage um : list) {
      UserMessageVO r = new UserMessageVO();
      BeanUtils.copyProperties(um, r);
      Message msg = messageMapper.selectByPrimaryKey(um.getMsgId());
      // 有的消息需要Formatter下附属文本，比如 您有一笔拼团活动%s
      String content = um.getContent();
      if (StringUtils.isNotEmpty(content)) {
        Formatter fmt = new Formatter();
        try {
          fmt.format(msg.getContent(), content);
          msg.setContent(fmt.toString());
        } finally {
          fmt.close();
        }
      }
      r.setMessage(msg);
      result.add(r);
    }
    return result;
  }

  @Override
  public List<Message> listMessageByAdmin(Map<String, ?> params, Pageable page) {
    return messageMapper.listMessageByAdmin(params, page);
  }

  @Override
  public Long countMessageByAdmin(Map<String, ?> params) {
    return messageMapper.countMessageByAdmin(params);
  }

  /**
   * 获取用户所有未读消息
   */
  @Override
  public long countUnreadByUser(String userId) {
    return userMessageMapper.countUnreadByUser(userId);
  }

  /**
   * 更新用户消息为已读
   */
  @Override
  public void updateUserAllRead(String userId) {
    userMessageMapper.updateUserAllRead(userId);
  }

  /**
   * 发送帮砍价系统消息
   */
  @Override
  public void sendBargainMessage(String fromUserId, String toUserId, String msgId,
      String content) {
    // 先删除之前的帮砍信息
    //userMessageMapper.deleteBargainMessage(fromUserId, toUserId, msgId);
    // 插入新的帮砍信息
    UserMessage um = new UserMessage();
    um.setFromUserId(fromUserId);
    um.setToUserId(toUserId);
    um.setMsgId(msgId);
    um.setStatus(UserMessageStatus.UNREAD);
    um.setContent(content);
    userMessageMapper.insert(um);
  }

}
