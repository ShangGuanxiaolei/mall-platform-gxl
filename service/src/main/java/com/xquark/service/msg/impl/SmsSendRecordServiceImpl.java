package com.xquark.service.msg.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.SmsSendRecordMapper;
import com.xquark.dal.model.SmsSendRecord;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.msg.SmsSendRecordService;

@Service("smsSendRecordService")
public class SmsSendRecordServiceImpl extends BaseServiceImpl implements SmsSendRecordService {

  @Autowired
  private SmsSendRecordMapper smsSendRecordMapper;

  @Override
  public int insert(SmsSendRecord record) {
    return smsSendRecordMapper.insert(record);
  }

  @Override
  public int updateSendResult(String mobile, String thirdBatchId, String status,
      String thirdResult) {
    return smsSendRecordMapper.updateSendResult(mobile, thirdBatchId, status, thirdResult);
  }
}
