package com.xquark.service.tovip;


import com.xquark.dal.model.PromotionToVip;
import org.springframework.data.domain.Pageable;
import java.util.List;

public interface PromotionToVipService {
    List<PromotionToVip> selectList(Pageable page);

    Boolean updateRule(PromotionToVip promotionToVip);

    Boolean updateAndUse(PromotionToVip promotionToVip);

    Boolean changeState(PromotionToVip promotionToVip);

    Boolean addRule(PromotionToVip promotionToVip);
}
