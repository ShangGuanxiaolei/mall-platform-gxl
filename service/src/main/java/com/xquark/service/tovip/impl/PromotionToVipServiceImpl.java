package com.xquark.service.tovip.impl;

import com.xquark.dal.mapper.PromotionToVipMapper;
import com.xquark.dal.model.PromotionToVip;
import com.xquark.service.tovip.PromotionToVipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.data.domain.Pageable;
import java.util.ArrayList;
import java.util.List;
@Service
public class PromotionToVipServiceImpl implements PromotionToVipService {
    @Autowired
    private PromotionToVipMapper promotionToVipMapper;
    @Override
    public List<PromotionToVip> selectList(Pageable page) {
        List<PromotionToVip> promotionList = promotionToVipMapper.selectList(page);
        if (null != promotionList && !promotionList.isEmpty()) {
            return promotionList;
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public Boolean updateRule(PromotionToVip promotionToVip) {
        return promotionToVipMapper.updateByPrimaryKey(promotionToVip)>0;
    }

    @Override
    public Boolean updateAndUse(PromotionToVip promotionToVip) {
        return promotionToVipMapper.updateAndUse(promotionToVip)>0;
    }

    @Override
    public Boolean changeState(PromotionToVip promotionToVip) {
        return promotionToVipMapper.changeState(promotionToVip)>0;
    }

    @Override
    public Boolean addRule(PromotionToVip promotionToVip) {
        return promotionToVipMapper.insert(promotionToVip)>0;
    }
}
