package com.xquark.service.reserve;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionPgPrice;
import com.xquark.dal.model.Sku;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.vo.ReserveVO;

/**
 * 预售
 *
 * @author: yyc
 * @date: 19-4-17 下午2:02
 */
public interface PromotionReserveService {

  /**
   * 查询预售优惠
   *
   * @param promotionId String
   * @param skuId String
   * @return Promotion
   */
  Promotion loadPromotion(String promotionId, String skuId);

  /**
   * 保存预付款订单
   *
   * @param promotionId String
   * @param productId String
   * @param skuId String
   * @param qty Integer
   * @return boolean
   */
  boolean saveReserveOrder(String promotionId, String productId, String skuId, Integer qty);

  /**
   * 统一预约单的数量
   *
   * @param orderStatus OrderStatus
   * @return int
   */
  int countOrderStatus(OrderStatus orderStatus);

  /**
   * 校验活动库存和相关信息
   *  @param preOrderNo String
   *
   */
  void checkPromotion(String preOrderNo);

  /**
   * 查询预售专区的活动列表单条
   *
   * @param promotionId String
   * @return ReserveVO
   */
  ReserveVO loadActivityById(String promotionId);

	/**
	 * 结束预售活动
	 */
	boolean endReserveStatus();

    boolean updateReserveOrderStatus(String preOrderNo, OrderStatus status);

    boolean updateReserveOrderStatusToPaid(String preOrderNo);

    boolean modifyStock(String promotionId, String skuId, int amt);

    OrderStatus selectOrderStatus(String preOrderNo);

    String loadPromotionIdByPreOrderNo(String preOrderNo);

    PromotionPgPrice buildPromotionPgPrice(String promotionId, Sku sku);
}
