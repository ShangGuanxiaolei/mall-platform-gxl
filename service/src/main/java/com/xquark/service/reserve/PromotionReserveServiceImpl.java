package com.xquark.service.reserve;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.xquark.dal.mapper.PromotionReserveMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.CareerLevelType;
import com.xquark.dal.type.OrderSortType;
import com.xquark.dal.vo.PromotionReserveOrderVO;
import com.xquark.dal.vo.PromotionReserveProductVO;
import com.xquark.dal.vo.ReserveProductVO;
import com.xquark.dal.vo.ReserveVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.promotion.PromotionProductLoader;
import com.xquark.utils.DynamicPricingUtil;
import com.xquark.utils.UniqueNoUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: yyc
 * @date: 19-4-17 下午1:59
 */
@Service
public class PromotionReserveServiceImpl extends BaseServiceImpl
    implements PromotionReserveService, PromotionProductLoader<PromotionReserveProductVO> {

  private final PromotionReserveMapper promotionReserveMapper;
  private final CustomerProfileService customerProfileService;
	private final SkuMapper skuMapper;

	@Autowired
  public PromotionReserveServiceImpl(
      PromotionReserveMapper promotionReserveMapper,
      CustomerProfileService customerProfileService,
			SkuMapper skuMapper) {
    this.promotionReserveMapper = promotionReserveMapper;
    this.customerProfileService = customerProfileService;
		this.skuMapper = skuMapper;
  }

  @Override
  public Optional<PromotionReserveProductVO> load(String promotionId, String productId) {
    PromotionReserveProductVO promotionReserveProductVO =
        promotionReserveMapper.selectVOByPidAndProductId(promotionId, productId, null);
    if (promotionReserveProductVO == null) {
      return Optional.empty();
    }

    PromotionPgPrice pricing = skuMapper.getAllSkuByproductId(productId)
            .stream()
            .map(s -> buildPromotionPgPrice(promotionId, s))
            .filter(Objects::nonNull)
            .min(Comparator.comparing(PromotionPgPrice::getPrice))
            .orElse(null);
    promotionReserveProductVO.setPricing(pricing);

    if (pricing != null) {
      // 根据身份动态计算兑换价, 覆盖掉原来的兑换价
      promotionReserveProductVO.setPromotionPrice(pricing.getPrice());
      promotionReserveProductVO.setPromotionPoint(pricing.getPoint());
      BigDecimal changePrice = DynamicPricingUtil
              .genChangePrice(promotionReserveProductVO, getCurrentUser());
      promotionReserveProductVO.setPromotionConversionPrice(changePrice);
    }


    Promotion promotion = promotionReserveProductVO.getPromotion();
    if (promotion == null || promotion.getValidFrom() == null || promotion.getPayTo() == null) {
      return Optional.empty();
    }
    // 每个人对应该商品的预约数量
    Integer buyCount =
        promotionReserveMapper.selectUserBuyCount(getCurrentUser().getId(), promotionId, productId);
    promotion.setBuyCount(buyCount);
    return Optional.of(promotionReserveProductVO);
  }

  @Override
  public Promotion loadPromotion(String promotionId, String skuId) {
    PromotionReserveProductVO promotionReserveProductVO =
        promotionReserveMapper.selectVOByPidAndProductId(promotionId, null, skuId);
    Promotion promotion = promotionReserveProductVO.getPromotion();
    if (promotion == null || promotion.getPayFrom() == null || promotion.getPayTo() == null) {
      throw new BizException(GlobalErrorCode.RESERVE_PAY_TIME_ERROR);
    }
    return promotion;
  }

  @Transactional(rollbackFor = Exception.class)
  @Override
  public boolean saveReserveOrder(String promotionId, String productId, String skuId, Integer qty) {
    String userId = getCurrentUser().getId();
    ReserveProductVO vo = checkReserveProduct(promotionId, productId, skuId, qty, userId);
    // 保存订单
    PromotionReserveOrder order = new PromotionReserveOrder();
    order.setUserId(userId);
    order.setPromotionId(promotionId);
    order.setOrderNo(UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.PRO));
    order.setOrderStatus(OrderStatus.SUBMITTED);
    order.setOrderType(OrderSortType.RESERVE);
    order.setTotalFee(vo.getPromotionPrice().multiply(BigDecimal.valueOf(qty)));
    order.setGoodsFee(vo.getPrice().multiply(BigDecimal.valueOf(qty)));
    promotionReserveMapper.insertReserveOrder(order);
    // 保存子订单
    PromotionReserveOrderItem orderItem = new PromotionReserveOrderItem();
    orderItem.setUserId(userId);
    orderItem.setAmount(qty);
    orderItem.setOrderId(order.getId());
    orderItem.setOrderStatus(OrderStatus.SUBMITTED);
    orderItem.setPrice(vo.getPrice());
    orderItem.setPromotionPrice(vo.getPromotionPrice());
    orderItem.setSkuId(skuId);
    orderItem.setProductId(productId);
    orderItem.setPromotionId(promotionId);
    promotionReserveMapper.insertReserveOrderItem(orderItem);
    return true;
  }

  @Override
  public int countOrderStatus(OrderStatus orderStatus) {
    Integer reserveCount =
        promotionReserveMapper.selectReserveOrderCount(getCurrentUser().getId(), orderStatus);
    return reserveCount == null ? 0 : reserveCount;
  }

  @Override
  public void checkPromotion(String preOrderNo) {
    String userId = getCurrentUser().getId();
    List<PromotionReserveOrderVO> reserveOrderVOS =
        promotionReserveMapper.selectReserveOrders(
            ImmutableMap.of("userId", userId, "preOrderNo", preOrderNo));
    if (CollectionUtils.isEmpty(reserveOrderVOS)) {
      throw new BizException(GlobalErrorCode.RESERVE_PRE_ORDER_NO_NOT_EXISTS);
    }
    //noinspection ResultOfMethodCallIgnored
    reserveOrderVOS.stream()
        .peek(
            orderVO -> {
              // 检查活动状态和时间
              if (!orderVO.getCanPay()) {
                throw new BizException(GlobalErrorCode.RESERVE_PAY_TIME_ERROR);
              }
              if (orderVO.getOrderStatus() == OrderStatus.CANCELLED) {
                throw new BizException(GlobalErrorCode.RESERVE_PRE_ORDER_NO_CANCELLED);
              }
              // 检查活动库存
              checkPayAmount(
                      orderVO.getPromotionId(),
                  orderVO.getSkuId(),
                  orderVO.getAmount()
              );
            })
        .collect(Collectors.toList());
  }

  @Override
  public ReserveVO loadActivityById(String promotionId) {
    List<ReserveVO> reserveVOS = promotionReserveMapper.pageActivities(null, promotionId);
    if (CollectionUtils.isEmpty(reserveVOS)) {
      return null;
    }
    ReserveVO reserveVO = reserveVOS.get(0);
    CareerLevelType currLevelType = customerProfileService.getCurrLevelType();
    if (!checkCurrLevelType(currLevelType, reserveVO.getTargetPerson())) {
      return null;
    }
    List<ReserveProductVO> reserveProductVOS =
        promotionReserveMapper.selectProductVOs(
            ImmutableMap.of("promotionId", reserveVO.getPromotionId()));
    // 分类目
    reserveVO.setReserveCategoryVOS(obtainActivityProduct(reserveProductVOS));
    return reserveVO;
  }

	/**
	 * 预约结束状态　归还库存
	 */
	@Override
	@Transactional
	public boolean endReserveStatus() {
      List<String> idList = promotionReserveMapper.selectExpiredPromotionId()
              .stream().map(IdTypeHandler::encode).collect(Collectors.toList());
      if (CollectionUtils.isNotEmpty(idList)) {
        for (String promotionId : idList) {
          // 更新订单状态到取消
          promotionReserveMapper.updateStatusByPromotionId(promotionId, OrderStatus.CANCELLED.name(), OrderStatus.SUBMITTED.name());
        }
      }

      // 更新活动状态为已结束
      if (promotionReserveMapper.updateReserveEndStatus() <= 0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "更新活动状态失败ｉ");
      }
      return true;
	}

	/**
   * 根据条件查询活动商品
   *
   * @param params ImmutableMap
   * @return List
   */
  private List<ReserveProductVO> loadProductVOs(ImmutableMap<String, Object> params) {
    return promotionReserveMapper.selectProductVOs(params);
  }

  /**
   * 提交订单时检查活动库存
   * @param promotionId String
   * @param skuId String
   * @param amount Integer
   */
  private void checkPayAmount(
          String promotionId, String skuId, Integer amount) {
    PromotionReserve promotionReserve = obtainActivity(promotionId);
    // 如果活动不存在或者支付时间结束、状态终止
    if (promotionReserve == null || !promotionReserve.canPay()) {
      throw new BizException(GlobalErrorCode.RESERVE_PAY_TIME_ERROR);
    }
    List<ReserveProductVO> reserveProductVOS =
        loadProductVOs(ImmutableMap.of("promotionId", promotionId, "skuId", skuId));
    if (CollectionUtils.isNotEmpty(reserveProductVOS)) {
      ReserveProductVO reserveProductVO = reserveProductVOS.get(0);
      int currAmt = reserveProductVO.getPayAmount();
      if (amount > currAmt) {
        throw new BizException(GlobalErrorCode.RESERVE_PROMOTION_AMOUNT_LIMIT);
      }
    }
  }

  public void checkPreOrder(String preOrderNo) {
    final OrderStatus orderStatus = promotionReserveMapper.selectOrderStatus(preOrderNo);
    if (orderStatus == null) {
      log.error("预购订单号不存在");
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "内部错误，请稍后再试");
    }
  }

  @Override
  public boolean updateReserveOrderStatus(String preOrderNo, OrderStatus status) {
    return promotionReserveMapper.updateReserveOrderStatus(preOrderNo, status) > 0;
  }

  @Override
  public boolean updateReserveOrderStatusToPaid(String preOrderNo) {
      return promotionReserveMapper.updateReserveOrderStatusToPaid(preOrderNo) > 0;
  }

  @Override
  public boolean modifyStock(String promotionId, String skuId, int amt) {
    return promotionReserveMapper.updateAmtByPromotionAndSkuId(promotionId, skuId, amt) > 0;
  }

  @Override
  public OrderStatus selectOrderStatus(String preOrderNo) {
      return promotionReserveMapper.selectOrderStatus(preOrderNo);
  }

  @Override
  public String loadPromotionIdByPreOrderNo(String preOrderNo) {
    return Optional.ofNullable(promotionReserveMapper.selectPromotionIdByPreOrderNo(preOrderNo))
            .map(IdTypeHandler::encode)
            .orElse(null);
  }

  /**
   * 通过预售商品加原商品数据构建价格体系对象
   */
  @Override
  public PromotionPgPrice buildPromotionPgPrice(String promotionId, Sku sku) {
    final String skuId = Objects.requireNonNull(sku).getId();
    final PromotionReserveProductVO reserveSku =
            promotionReserveMapper.selectVOByPidAndProductId(promotionId, null, skuId);
    if (reserveSku == null) {
      return null;
    }
    final PromotionPgPrice pgPrice = new PromotionPgPrice();
    BeanUtils.copyProperties(sku, pgPrice);
    pgPrice.setPromotionPrice(reserveSku.getPromotionPrice());
    pgPrice.setPoint(reserveSku.getPromotionPoint());
    return pgPrice;
  }

  /**
   * 判断预约单是否超过设置的最大限制
   *  @param promotionId String
   * @param productId String
   * @param skuId skuId
   * @param amount Integer
   */
  private ReserveProductVO checkReserveProduct(
          String promotionId, String productId, String skuId, Integer amount, String userId) {
    PromotionReserve promotionReserve = obtainActivity(promotionId);
    if (promotionReserve == null || !promotionReserve.canOrder()) {
      throw new BizException(GlobalErrorCode.RESERVE_ORDER_TIME_ERROR);
    }
    if (!promotionReserve.isValid()) {
      throw new BizException(GlobalErrorCode.RESERVE_PAY_TIME_ERROR);
    }
    List<ReserveProductVO> reserveProductVOS =
        loadProductVOs(ImmutableMap.of("promotionId", promotionId, "skuId", skuId));
    if (CollectionUtils.isEmpty(reserveProductVOS)) {
      throw new BizException(GlobalErrorCode.RESERVE_PRODUCT_NOT_EXISTS);
    }
    ReserveProductVO vo = reserveProductVOS.get(0);
    // 判断预约总数限制
    int reserveCount = amount + vo.getPreReserveCount();
    if (reserveCount > vo.getReserveLimit()) {
      throw new BizException(GlobalErrorCode.RESERVE_LIMIT);
    } else {
      // 更新总预约数量
      int i = promotionReserveMapper.updateReserveAmt(vo.getId(), amount);
      if (i <= 0) {
        throw new BizException(GlobalErrorCode.RESERVE_LIMIT);
      }
    }

    // 判断用户预约限制
    Integer hasBuyCount = promotionReserveMapper.selectUserBuyCount(userId, promotionId, productId);
    hasBuyCount = hasBuyCount == null ? 0 : hasBuyCount;
    int buyCount = hasBuyCount + amount;
    if (buyCount > vo.getBuyLimit()) {
      throw new BizException(GlobalErrorCode.RESERVE_USER_LIMIT);
    }
    return vo;
  }

  /**
   * 检查定向人群
   *
   * @param careerLevelType CareerLevelType
   * @param targetPerson Integer
   * @return boolean
   */
  private boolean checkCurrLevelType(CareerLevelType careerLevelType, Integer targetPerson) {
    if (targetPerson == 1) {
      return true;
    } else if (targetPerson == 2) {
      return careerLevelType == CareerLevelType.PW || careerLevelType == CareerLevelType.RC;
    } else if (targetPerson == 3) {
      return careerLevelType == CareerLevelType.DS || careerLevelType == CareerLevelType.SP;
    }
    return false;
  }

  /**
   * 活动商品去重并挑选最小活动价、兑换价规格
   *
   * @param list List
   */
  private Map<String, List<ReserveProductVO>> obtainActivityProduct(List<ReserveProductVO> list) {
    List<ReserveProductVO> reserveProductVOS = Lists.newArrayList();
    // 根据productId分组
    Map<String, List<ReserveProductVO>> map =
        list.stream().collect(Collectors.groupingBy(ReserveProductVO::getProductId));
    // 排序
    map.forEach(
        (k, v) -> {
          v.sort(
              Comparator.comparing(ReserveProductVO::getPromotionPrice)
                  .thenComparing(ReserveProductVO::getConversionPrice));
          // 挑选最优的活动价和兑换价
          reserveProductVOS.add(v.get(0));
        });
    return reserveProductVOS.stream()
        .collect(Collectors.groupingBy(ReserveProductVO::getCategoryName));
  }

  /**
   * 根据活动id查询活动
   *
   * @param promotionId String
   * @return PromotionReserve
   */
  private PromotionReserve obtainActivity(String promotionId) {
    return promotionReserveMapper.selectReserveById(promotionId);
  }
}
