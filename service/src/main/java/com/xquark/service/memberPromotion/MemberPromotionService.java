package com.xquark.service.memberPromotion;

import com.google.common.collect.ImmutableMap;
import com.xquark.dal.mapper.MemberPromotionMapper;
import com.xquark.dal.model.MemberPromotion;
import com.xquark.dal.type.MemberPromotionStatus;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by wangxinhua on 18-3-14. DESC:
 */
@Service
public class MemberPromotionService {

  private final MemberPromotionMapper memberPromotionMapper;

  @Autowired
  public MemberPromotionService(MemberPromotionMapper memberPromotionMapper) {
    this.memberPromotionMapper = memberPromotionMapper;
  }

  /**
   * 查询
   */
  public MemberPromotion load(String id) {
    return memberPromotionMapper.selectByPrimaryKey(id);
  }

  /**
   * 保存会员活动
   */
  public boolean save(MemberPromotion mp) {
    return memberPromotionMapper.insert(mp) > 0;
  }

  /**
   * 更新会员活动
   */
  public boolean update(MemberPromotion mp) {
    return memberPromotionMapper.updateByPrimaryKeySelective(mp) > 0;
  }

  /**
   * 删除会员活动
   */
  public boolean delete(String id) {
    return memberPromotionMapper.deleteByPrimaryKey(id) > 0;
  }

  /**
   * 分页查询会员活动
   */
  public Map<String, ?> list(Pageable pageable) {
    List<MemberPromotion> list = memberPromotionMapper.list(pageable, null);
    Long total = memberPromotionMapper.count(null);
    return ImmutableMap.of("list", list, "total", total);
  }

  /**
   * 不分页查询会员活动
   */
  public List<MemberPromotion> list(MemberPromotionStatus status) {
    return memberPromotionMapper.list(null, status);
  }

}
