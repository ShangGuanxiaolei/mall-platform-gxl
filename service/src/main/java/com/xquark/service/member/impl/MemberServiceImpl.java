package com.xquark.service.member.impl;

import com.xquark.dal.mapper.MemberMapper;
import com.xquark.dal.model.User;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.UserStatus;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.UserMemberVO;
import com.xquark.service.member.MemberService;
import com.xquark.service.member.vo.OrderStatusCountVO;
import com.xquark.service.order.OrderService;
import com.xquark.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by quguangming on 16/3/3.
 */
@Service("memberService")
public class MemberServiceImpl implements MemberService {

  @Autowired
  private OrderService orderService;

  @Autowired
  private UserService userService;


  @Autowired
  private MemberMapper memberMapper;

  @Override
  public OrderStatusCountVO getCountByOrderStatus(String buyerId, String shopId) {

    OrderStatusCountVO oscVo = new OrderStatusCountVO();

    long submitted = orderService
        .getCountByStatus4Buyer(OrderStatus.SUBMITTED, buyerId, shopId); // 已提交 ，未付款
    long paid = orderService
        .getCountByStatus4Buyer(OrderStatus.PAID, buyerId, shopId);           // 已付款  未发货
    long shipped = orderService
        .getCountByStatus4Buyer(OrderStatus.SHIPPED, buyerId, shopId);     // 已发货
    long refunding = orderService
        .getCountByStatus4Buyer(OrderStatus.REFUNDING, buyerId, shopId); // 退款申请中
    long finished = orderService
        .getCountByMoreStatus4Buyer(new String[]{OrderStatus.SUCCESS.toString(),
            OrderStatus.CLOSED.toString(),
            OrderStatus.CANCELLED.toString()
        }, buyerId, shopId);  // 已完成

    oscVo.setSubmittedCount(submitted);
    oscVo.setFinishedCount(finished);
    oscVo.setPaidCount(paid);
    oscVo.setRefundingCount(refunding);
    oscVo.setShippedCount(shipped);

    return oscVo;
  }

  @Override
  public List<OrderVO> getOrderListByOrderStatus(String[] orderStatusArr, String userId,
      String shopId, Pageable pageable, Map<String, Object> params) {
    return orderService.listByStatusAndMember(orderStatusArr, userId, shopId, pageable, params);
  }

  @Override
  public long countOrderListByOrderStatus(String[] orderStatusArr, String userId, String shopId,
      Map<String, Object> params) {
    return orderService.countByStatusAndMember(orderStatusArr, userId, shopId, params);
  }

  @Override
  public List<OrderVO> getSellerOrderListByOrderStatus(String[] orderStatusArr, String sellerShopId,
      String shopId, Pageable pageable, Map<String, Object> params) {
    return orderService
        .listSellerByStatusAndMember(orderStatusArr, sellerShopId, shopId, pageable, params);
  }

  @Override
  public boolean checkRegister(String phone) {
    return userService.isRegistered(phone);
  }

  /**
   * 检测用户是否已登录
   *
   * @return 登录状态
   */
  @Override
  public boolean checkSignature() {

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    User user = null;
    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof User) {
        user = (User) principal;
      }
    }
    if (user != null && UserStatus.LOGINED.equals(user.getUserStatus())) {
      return true;
    }
    return false;
  }

  @Override
  public List<UserMemberVO> getMemberList(Map<String, Object> params, Pageable pageable) {
    return memberMapper.getMemberList(params, pageable);
  }

  /**
   * 获取微信会员列表数据
   *
   * @param params 含查询条件
   */
  @Override
  public List<User> getWechatMemberList(Map<String, Object> params, Pageable pageable) {

    return memberMapper.getWechatMemberList(params, pageable);
  }

  @Override
  public Long countMember(Map<String, Object> params) {
    return memberMapper.countMember(params);
  }

  @Override
  public Long countMembers(Integer day) {
    return memberMapper.countMembersAll(day);
  }

  @Override
  public Long countWechatMember(Map<String, Object> params) {
    return memberMapper.countWechatMember(params);
  }

  /**
   * 设置会员等级
   */
  @Override
  public int setWechatMemberLevel(Map<String, Object> params) {

    return memberMapper.setMemberLevel(params);
  }

  /**
   * 设置会员消费积分
   */
  @Override
  public int setWechatMemberConsumptionPoints(Map<String, Object> params) {
    return memberMapper.setMemberPoints(params);
  }

  /**
   * 获取会员图表数据
   */
  @Override
  public Map<String, Object> getWechantMemberChart(Map<String, Object> params) {
    return null;
  }

  @Override
  public Long countMembersYesterday() {
    return memberMapper.countDealMembers(1);
  }

  @Override
  public Long countDealMembers(Integer pastDay) {
    return memberMapper.countDealMembers(pastDay);
  }

  @Override
  public Map<String, Object> lastWeekMemberChart() {
    int day = 7;
    List<Map<Date, Long>> membersMap = memberMapper.addUpMembers(day);
    List<Map<Date, Long>> dealMemberMap = memberMapper.addUpDealMembers(day);
    Map<String, Object> resultMap = new HashMap<String, Object>();
    resultMap.put("member", membersMap);
    resultMap.put("dealMember", dealMemberMap);
    return resultMap;
  }
}
