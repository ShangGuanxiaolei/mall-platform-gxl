package com.xquark.service.member;


import com.xquark.dal.model.User;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.UserMemberVO;
import com.xquark.service.member.vo.OrderStatusCountVO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * Created by quguangming on 16/3/3.
 */
public interface MemberService {

  /**
   * 通过买家账号获取产生不同类型订单的数量
   *
   * @param buyerId 买家ID
   * @return 不同类型订单的数量
   */
  OrderStatusCountVO getCountByOrderStatus(String buyerId, String shopId);

  /**
   * 获取订单列表
   *
   * @param orderStatusArr 订单状态
   * @param userId 用户Id
   * @param shopId 店铺Id
   * @return 订单列表
   */
  List<OrderVO> getOrderListByOrderStatus(String[] orderStatusArr, String userId, String shopId,
      Pageable pageable, Map<String, Object> params);

  /**
   * 获取卖出的订单列表
   *
   * @param orderStatusArr 订单状态
   * @param sellerShopId 店铺Id
   * @param shopId 总店id
   * @return 订单列表
   */
  List<OrderVO> getSellerOrderListByOrderStatus(String[] orderStatusArr, String sellerShopId,
      String shopId,
      Pageable pageable, Map<String, Object> params);

  /**
   * 确定用户是否已注册
   *
   * @param phone 手机号(用户名)
   */
  boolean checkRegister(String phone);

  /**
   * 确定买家是否是已登录状态
   *
   * @return 登录状态  true 已登录
   */
  boolean checkSignature();

  /**
   * 获取所有会员信息
   *
   * @param params 含查询条件(可选) phone - 手机号条件, name - 名称条件, cardId - 对应的会员卡id
   * @param pageable 分页信息
   * @return 会员列表信息 或 空列表
   */
  List<UserMemberVO> getMemberList(Map<String, Object> params, Pageable pageable);

  /**
   * 获取相应条件i下的会员数量
   *
   * @param params 查询条件
   * @return 会员数量
   */
  Long countMember(Map<String, Object> params);

  /**
   * 获取店铺微信会员列表信息
   *
   * @param params 含查询条件
   * @return 会员列表信息
   */
  List<User> getWechatMemberList(Map<String, Object> params, Pageable pageable);


  Long countMembers(Integer day);

  /**
   * 获取店铺微信会员列表信息
   *
   * @param params 含查询条件
   * @return 会员列表信息
   */
  Long countWechatMember(Map<String, Object> params);


  /**
   * 设置微信会员等级
   */
  int setWechatMemberLevel(Map<String, Object> params);

  /**
   * 设置会员积分
   */
  int setWechatMemberConsumptionPoints(Map<String, Object> params);

  /**
   * 获取微信图表数据
   */
  Map<String, Object> getWechantMemberChart(Map<String, Object> params);

  /**
   * 查询昨天新增的用户数量
   *
   * @return 用户数量
   */
  Long countMembersYesterday();

  /**
   * 查询成交的用户
   *
   * @param pastDay 过去几天内
   * @return 用户数量
   */
  Long countDealMembers(Integer pastDay);

  /**
   * 过去一周新增会员数与新增成交会员图标数据
   *
   * @return Map 日期与数量键值对
   */
  Map<String, Object> lastWeekMemberChart();

  long countOrderListByOrderStatus(String[] orderStatusArr, String userId, String shopId,
      Map<String, Object> params);
}
