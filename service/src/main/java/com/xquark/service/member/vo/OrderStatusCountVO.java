package com.xquark.service.member.vo;

/**
 * Created by quguangming on 16/3/7.
 */
public class OrderStatusCountVO {

  private long submittedCount;
  private long paidCount;
  private long shippedCount;
  private long refundingCount;
  private long finishedCount;


  public long getSubmittedCount() {
    return submittedCount;
  }

  public void setSubmittedCount(long submittedCount) {
    this.submittedCount = submittedCount;
  }

  public long getPaidCount() {
    return paidCount;
  }

  public void setPaidCount(long paidCount) {
    this.paidCount = paidCount;
  }

  public long getShippedCount() {
    return shippedCount;
  }

  public void setShippedCount(long shippedCount) {
    this.shippedCount = shippedCount;
  }

  public long getRefundingCount() {
    return refundingCount;
  }

  public void setRefundingCount(long refundingCount) {
    this.refundingCount = refundingCount;
  }

  public long getFinishedCount() {
    return finishedCount;
  }

  public void setFinishedCount(long finishedCount) {
    this.finishedCount = finishedCount;
  }
}
