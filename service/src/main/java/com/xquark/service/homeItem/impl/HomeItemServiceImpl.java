package com.xquark.service.homeItem.impl;

import com.xquark.dal.mapper.ActivityGrouponDetailMapper;
import com.xquark.dal.mapper.ActivityGrouponMapper;
import com.xquark.dal.mapper.HomeItemMapper;
import com.xquark.dal.model.ActivityGroupon;
import com.xquark.dal.model.ActivityGrouponDetail;
import com.xquark.dal.model.HomeItem;
import com.xquark.dal.status.ActivityGrouponStatus;
import com.xquark.dal.status.HomeItemBelong;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.vo.HomeItemVO;
import com.xquark.dal.vo.PromotionGrouponVO;
import com.xquark.service.groupon.ActivityGrouponService;
import com.xquark.service.homeItem.HomeItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by chh on 16-12-6.
 */
@Service("homeItemServiceImpl")
public class HomeItemServiceImpl implements HomeItemService {

  @Autowired
  private HomeItemMapper homeItemMapper;

  @Override
  public HomeItemVO selectByPrimaryKey(String id) {
    return homeItemMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(HomeItem homeItem) {
    return homeItemMapper.insert(homeItem);
  }

  @Override
  public int modify(HomeItem homeItem) {
    return homeItemMapper.modify(homeItem);
  }


  /**
   * @param id
   * @return
   */
  @Override
  public int delete(String id) {
    return homeItemMapper.delete(id);
  }


  /**
   * @return
   */
  @Override
  public List<HomeItemVO> listByApp(HomeItemBelong belong) {
    return homeItemMapper.listByApp(belong);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<HomeItemVO> list(Pageable pager, String belong) {
    return homeItemMapper.list(pager, belong);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(String belong) {
    return homeItemMapper.selectCnt(belong);
  }


}
