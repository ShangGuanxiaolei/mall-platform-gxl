package com.xquark.service.homeItem;

import com.xquark.dal.model.ActivityGroupon;
import com.xquark.dal.model.ActivityGrouponDetail;
import com.xquark.dal.model.HomeItem;
import com.xquark.dal.status.ActivityGrouponStatus;
import com.xquark.dal.status.HomeItemBelong;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.vo.HomeItemVO;
import com.xquark.dal.vo.PromotionGrouponVO;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by chh on 16-12-6. 首页模块配置Service
 */
public interface HomeItemService {

  HomeItemVO selectByPrimaryKey(String id);

  int insert(HomeItem homeItem);

  int modify(HomeItem homeItem);


  /**
   * @param id
   * @return
   */
  int delete(String id);


  /**
   * @return
   */
  List<HomeItemVO> listByApp(HomeItemBelong belong);

  /**
   * 服务端分页查询数据
   */
  List<HomeItemVO> list(Pageable pager, String belong);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(String belong);


}
