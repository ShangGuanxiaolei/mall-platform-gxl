package com.xquark.service.scrm;

import com.xquark.dal.model.SCrmHttpLog;

/**
 * Created by wangxinhua on 18-3-1. DESC:
 */
public interface SCrmLogService {

  /**
   * 记录SCRM访问请求日志
   */
  boolean log(SCrmHttpLog log);

}
