package com.xquark.service.scrm;

import static org.parboiled.common.Preconditions.checkNotNull;

import com.xquark.dal.mapper.SCrmHttpLogMapper;
import com.xquark.dal.model.SCrmHttpLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wangxinhua on 18-3-1. DESC:
 */
@Service
public class SCrmLogServiceImpl implements SCrmLogService {

  private final SCrmHttpLogMapper logMapper;

  @Autowired
  public SCrmLogServiceImpl(SCrmHttpLogMapper logMapper) {
    this.logMapper = logMapper;
  }

  @Override
  public boolean log(SCrmHttpLog log) {
    checkNotNull(log);
    return logMapper.insert(log) > 0;
  }

}
