package com.xquark.service.cashier;

import com.xquark.dal.model.CashierItem;
import com.xquark.dal.model.MainOrder;
import com.xquark.dal.model.PaidAble;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.PaymentChannel;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.vo.CouponInfoVO;
import com.xquark.service.outpay.impl.ThirdPaymentQueryRes;
import com.xquark.service.pay.model.PaymentResponseVO;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface CashierService {

  /**
   * 保存
   */
  void save(List<CashierItem> item, String bizNo, PaymentChannel paymentChannel);

  List<CashierItem> listByBizNo(String bizNo);

  /**
   * 下一个需要支付的item
   */
  CashierItem nextCashierItem(String bizNo);

  /**
   * 某一个已支付完成
   */
  CashierItem paid(String bizNo, String bizType, PaymentMode paymentMode, String itemId);

  void handlePending(MainOrder o);

  void handleThirdRes(PaidAble paidAble, ThirdPaymentQueryRes res);

  @Transactional(rollbackFor = Exception.class)
  void handlePaymentResponse(PaidAble paidAble, PaymentResponseVO paymentResponse);

  void handlePaymentResponse(String payNo, PaymentMode paymentMode, PaymentResponseVO res);

  /**
   * 某一个已支付完成
   */
  CashierItem paid(String bizNo, String bizType, PaymentMode paymentMode);

  /**
   * 关闭支付，回滚
   */
  Boolean closeTrade(String bizNo, String userId, String batchBizNos);

  BigDecimal loadPaidFee(String bizNo);

  List<Map<String, Object>> loadHongBaoFee(String bizNo);
  
  CashierItem loadByPayTypeAndOrderNo( PaymentChannel paymentChannel,String payType,String orderNo);

  CashierItem load(String id);

  int update(CashierItem item);

  List<CouponInfoVO> loadCouponInfoByOrderNo(String bizNo);

  String findDefaultBizId(String bizNo);

  List<CashierItem> listByBatchBizNo(String batchBizNo, String userId, PaymentStatus status);
}
