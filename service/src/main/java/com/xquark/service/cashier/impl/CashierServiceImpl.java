package com.xquark.service.cashier.impl;

import com.alibaba.fastjson.JSON;
import com.xquark.dal.mapper.CashierItemMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.CouponStatus;
import com.xquark.dal.status.MainOrderStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.PayStatus;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.*;
import com.xquark.dal.vo.CouponInfoVO;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.helper.Transformer;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.error.PayNotifyException;
import com.xquark.service.error.ThirdRefundData;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderService;
import com.xquark.service.outpay.OutPayLogs;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.outpay.ThirdPartyPayment;
import com.xquark.service.outpay.ThirdPaymentQueryService;
import com.xquark.service.outpay.impl.TenPaymentImpl;
import com.xquark.service.outpay.impl.ThirdPaymentQueryRes;
import com.xquark.service.pay.OutPayService;
import com.xquark.service.pay.PayRequestApiService;
import com.xquark.service.pay.model.PaymentResponseVO;
import com.xquark.service.pricing.CouponService;
import com.xquark.service.user.UserService;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static com.xquark.dal.status.PaymentStatus.CLOSED;

@Service("cashierService")
public class CashierServiceImpl implements CashierService {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private OrderService orderService;

  @Autowired
  private MainOrderService mainOrderService;

  @Autowired
  private UserService userService;

  @Autowired
  private CouponService couponService;

  @Autowired
  private CashierItemMapper cashierItemMapper;

  @Autowired
  private PayRequestApiService payRequestApiService;

  @Autowired
  private OutPayService outPayService;

  @Autowired
  private ThirdPartyPayment xqPayment;

  @Autowired
  private ThirdPartyPayment aliPayment;

  @Autowired
  private AccountApiService accountApiService;


  @Autowired
  private TenPaymentImpl tenPayment;

  @Autowired
  private ThirdPaymentQueryService thirdPaymentQueryService;

  @Override
  @Transactional
  public void save(List<CashierItem> items, String bizNo, PaymentChannel paymentChannel) {

    //去掉之前的红包锁定状态
    unLock(bizNo, items.get(0).getUserId(), items.get(0).getBatchBizNos(), paymentChannel);

    for (CashierItem item : items) {
      if (item.getAmount().compareTo(BigDecimal.ZERO) < 1) {
        continue;
      }

      cashierItemMapper.insert(item);
      lock(item);
    }
  }

  @Override
  public Boolean closeTrade(String bizNo, String userId, String batchBizNos) {

    if (StringUtils.isNotBlank(bizNo)) {
      List<CashierItem> items = listByBizNo(bizNo);
      for (CashierItem item : items) {
        close(item, null);
      }
    } else {
      unLock(bizNo, userId, batchBizNos, null);
    }

    return true;
  }

  /**
   * 根据收银台item,关闭支付（包括优惠券和其他第三方支付渠道） closeTrade为根据bizNo关闭
   *
   * @param notIncludeBizNo 不关闭本订单现次支付的订单
   */
  private void close(CashierItem item, String notIncludeBizNo) {
    if (item.getStatus() != PaymentStatus.PENDING) {
      return;
    }

    if (StringUtils.isNoneBlank(item.getCouponId())) {
      Coupon coupon = couponService.load(item.getCouponId());
      if (coupon == null) {
        log.error("取消订单时，使用的红包不存在");
        return;
      }

      if (coupon.getStatus() == CouponStatus.LOCKED) {
        couponService.unLock(coupon.getId(), item.getBizNo());
        if (StringUtils.isNotBlank(coupon.getExtCouponId())) {
          xqPayment.payUnLock(coupon.getExtCouponId());
        }
      }
    } else if (item.getPaymentMode() == PaymentMode.ALIPAY
        || item.getPaymentMode() == PaymentMode.ALIPAY_PC) {
      if (!item.getBizNo().equals(notIncludeBizNo)) {
        aliPayment.closeTrade(item.getBizNo());
      }
    }

    //cashierItemMapper.close(item);
  }


  private void unLock(String bizNo, String userId, String batchBizNos,
      PaymentChannel paymentChannel) {
//		List<CashierItem> items = cashierItemMapper.listByBizNo(bizNo);
//		if(items.size() == 0){
//			return;
//		}
//
    //unlock该笔订单之前为组合支付，则需要去除之前的组合支付的记录
//		CashierItem item = items.get(0);
    for (String batchBizNo : batchBizNos.split(",")) {
      List<CashierItem> colseItmes = listByBatchBizNo(batchBizNo, userId,
          PaymentStatus.PENDING);
      for (CashierItem closeItem : colseItmes) {
        if (paymentChannel != null && closeItem.getPaymentChannel() != paymentChannel) {
          continue;
        }
        close(closeItem, bizNo);
      }
    }
  }

  /**
   * 在使用优惠券时需要锁定 ，防止其他的订单选择到该笔优惠券
   */
  private void lock(CashierItem item) {
    //只有优惠券有锁定接口，其他第三方支付渠道无需锁定，
    //但unlock时需要关闭
    if (StringUtils.isBlank(item.getCouponId())) {
      return;
    }

    couponService.lock(item.getCouponId(), item.getBizNo());
    Coupon coupon = couponService.load(item.getCouponId());
    if (StringUtils.isNotBlank(coupon.getExtCouponId())) {
      xqPayment.payLock(coupon.getExtCouponId());
    }
  }

  @Override
  public int update(CashierItem item) {
    return cashierItemMapper.update(item);
  }

  /**
   * 检查outpay表里有没相应的成功记录
   * 检查outpay是否已经成功
   */
  private boolean checkOutPay(String bizNo, PaymentMode paymentMode) {
    boolean rc = false;
    String payType = paymentMode.toString();
    if (payType.equals("UNION")) {
      payType = "UMPAY";
    }
    List<OutPay> lsOutPay = outPayService.listByBillNo(bizNo);
    for (OutPay outPay : lsOutPay) {
      if ((outPay.getOutId().equals(payType) || outPay.getOutId().equals("EPS")) && outPay.getStatus() == PaymentStatus.SUCCESS
          && outPay.getOutStatus().equals("SUCCESS")) {
        rc = true;
        break;
      }
    }

    return rc;
  }

  @Override
  public List<CashierItem> listByBizNo(String bizNo) {
    return cashierItemMapper.listByBizNo(bizNo);
  }

  @Override
  public List<CashierItem> listByBatchBizNo(String batchBizNo, String userId,
      PaymentStatus status) {
    batchBizNo = "%" + batchBizNo + "%";
    return cashierItemMapper.listByBatchBizNo(batchBizNo, userId, status);
  }
  
  @Override
  public CashierItem loadByPayTypeAndOrderNo( PaymentChannel paymentChannel,String payType,String orderNo) {
     List<CashierItem> items = cashierItemMapper.listByPayTypeAndOrderNo(paymentChannel,payType,orderNo);
     if(items != null && items.size() > 0)
    	 return items.get(0);
     return null;
  }

  @Override
  public CashierItem load(String id) {
    return cashierItemMapper.selectByPrimaryKey(id);
  }

  @Override
  public CashierItem nextCashierItem(String bizNo) {
    List<CashierItem> items = listByBizNo(bizNo);

    for (CashierItem item : items) {
      if (item.getStatus() == PaymentStatus.PENDING) {
        return item;
      }
    }
    return null; //所有支付渠道已完成
  }

  /**
   * 处理PENDING状态的订单
   *
   * @param o 主订单
   */
  @Transactional
  @Override
  public void handlePending(MainOrder o) {
    if (o.getStatus() != MainOrderStatus.PENDING || o.getStatus() != MainOrderStatus.SUBMITTED) {
      log.error("orderNo {} 不在PENDING/SUBMITTED状态", o.getOrderNo());
      return;
    }
    Optional<ThirdPaymentQueryRes> query = thirdPaymentQueryService.query(o);
    query.ifPresent(
        q -> {
          if (!q.isPaid() || Objects.equals(q.getBillStatus(), CLOSED)) {
            log.warn("订单 {} 第三方返回状态为 {} 未支付或已关闭， 跳过PENDING检查", o.getOrderNo(), q.getStatus());
            throw new BizException(
                GlobalErrorCode.INVALID_ARGUMENT,
                String.format(
                    "订单 %s 第三方返回状态为 %s 未支付或已关闭， 跳过PENDING检查", o.getOrderNo(), q.getStatus()));
          }
          // 插入outPay并支付
          handleThirdRes(o, q);
        });
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public void handleThirdRes(PaidAble paidAble, ThirdPaymentQueryRes res) {
    final PaymentResponseVO response = Transformer.fromBean(res, PaymentResponseVO.class);
    this.handlePaymentResponse(paidAble, response);
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public void handlePaymentResponse(PaidAble paidAble, PaymentResponseVO paymentResponse) {
    this.handlePaymentResponse(paidAble.getPayNo(), paidAble.getPayType(), paymentResponse);
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public void handlePaymentResponse(String payNo, PaymentMode paymentMode, PaymentResponseVO res) {
    Long mainOrderId = mainOrderService.loadUnPaidOrder(payNo);
    boolean isToPay = false;
    if (mainOrderId == null) {
    	//未查询到该支付单号的未支付主单,
    	//1. 该支付单号对应已支付的主单,重复回调; 2. 订单已取消 退款；3. 主单已用其他支付单号支付成功，退款；4. 该支付单号对应的其他订单支付中，走正常支付流程
    	List<CashierItem> items = this.listByBizNo(payNo);
    	if( items == null || items.size() <= 0 || items.get(0) == null){
    		thirdRefundLogger.info("notify cannot find cashierItem,payNo:{}",payNo);
    		return;
    	}
    	CashierItem item =  items.get(0);
    	MainOrderVO mainOrder = mainOrderService.loadByOrderNo( item.getBatchBizNos() );
    	if(mainOrder == null ) {
    		thirdRefundLogger.info("notify cannot find mainOrder,payNo:{}",payNo);	
    		return;
    	}
		if(MainOrderStatus.SUCCESS == mainOrder.getStatus() && mainOrder.getPayNo().equalsIgnoreCase(payNo)) {
			//支付成功订单号一致,重复回调，不做处理
			thirdRefundLogger.info("renotify success...do nothing:{}",JSON.toJSONString(res));
			return;
		}else if( MainOrderStatus.CANCELLED == mainOrder.getStatus()  //订单已取消，做退款
				    || ( MainOrderStatus.SUBMITTED != mainOrder.getStatus()  // 订单成功但是支付单号不一致，做退款
					 		&& MainOrderStatus.PENDING != mainOrder.getStatus()
					 		&& !mainOrder.getPayNo().equalsIgnoreCase(payNo) ) ) {
			this.doThirdRefund(item);//需要退款
		}else if(MainOrderStatus.PENDING == mainOrder.getStatus()
				|| MainOrderStatus.SUBMITTED == mainOrder.getStatus() ){
			//支付中,更新支付单号,更新支付状态
			isToPay = true;
			thirdRefundLogger.info("order:{} will change payno: {} -> {}",mainOrder.getOrderNo(),mainOrder.getPayNo(),payNo);
		}
    	
      // 对主订单未支付状态做forUpdate查询，防止重复支付
      //log.info("订单id: {}, payNo: {} 已支付，重复请求PAID", mainOrderId, payNo);
      //return;
    }else
    	isToPay = true;//查询到未支付的主单
    
    if(isToPay) {
    	outPayService.savePayResult(res);
		this.paid(payNo, "order", paymentMode);
    }
    
  }

  @Override
  @Transactional
  public CashierItem paid(String bizNo, String bizType, PaymentMode paymentMode) {
    return paid(bizNo, bizType, paymentMode, null);
  }

  @Override
  @Transactional
  public CashierItem paid(String bizNo, String bizType, PaymentMode paymentMode, String itemId) {
    log.error("支付回调开始， bizNo=[" + bizNo + "] paymentMode=[" + paymentMode + "]" + "itemId="+itemId);
    if (!"order".equals(bizType) || StringUtils.isBlank(bizNo)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "暂不支持该类型 type=" + bizType);
    }
    List<CashierItem> items = listByBizNo(bizNo);
    boolean b = false;
    for (CashierItem item : items) {
      if (item.getPaymentMode() != paymentMode) {
        continue;
      }

      if (itemId != null && !itemId.equals(item.getId())) {
        continue;
      }
      if (item.getStatus() == PaymentStatus.PENDING) {
        if (cashierItemMapper.paid(item.getId()) != 1) {

          CashierItem updateItem = cashierItemMapper.selectByPrimaryKey(item.getId());
          if (updateItem.getStatus() == PaymentStatus.SUCCESS) {
            b = true;
            continue;
          } else {
            log.error("收银台更新状态失败 orderNo=[" + bizNo + "] paymentMode=[" + paymentMode
                + "]");
            throw new BizException(GlobalErrorCode.UNKNOWN, "状态更新失败");
          }
        }

        //去outpay表里查找是否有相应的记录
        if (!checkOutPay(bizNo, paymentMode)) {
          log.error("收银台更新状态失败 bizNo=[" + bizNo + "] paymentMode=[" + paymentMode + "]");
          throw new BizException(GlobalErrorCode.UNKNOWN, "状态更新失败");
        }
        SubAccount fromAccount = null, toAccount = null;
        if (StringUtils.isNotBlank(item.getCouponId())) {
          //红包充值
          fromAccount = accountApiService
              .findSubAccountByPayMode(item.getPaymentMode(), AccountType.AVAILABLE);
          toAccount = accountApiService
              .findSubAccountByUserId(item.getUserId(), AccountType.HONGBAO);
          PayRequest request = new PayRequest(item.getBizNo(), item.getBizNo(),
              PayRequestBizType.ORDER, PayRequestPayType.RECHARGEHONGBAO,
              item.getAmount(), fromAccount.getId(), toAccount.getId(), null);
          payRequestApiService.payRequest(request);

          fromAccount = accountApiService
              .findSubAccountByUserId(item.getUserId(), AccountType.HONGBAO);
          toAccount = accountApiService
              .findSubAccountByUserId(item.getUserId(), AccountType.CONSUME);
          request = new PayRequest(item.getBizNo(), item.getBizNo(),
              PayRequestBizType.ORDER, PayRequestPayType.HONGBAO2CONSUME,
              item.getAmount(), fromAccount.getId(), toAccount.getId(),
              request.getId());
          payRequestApiService.payRequest(request);

        } else {
          fromAccount = accountApiService
              .findSubAccountByPayMode(item.getPaymentMode(), AccountType.AVAILABLE);
          toAccount = accountApiService
              .findSubAccountByUserId(item.getUserId(), AccountType.AVAILABLE);
          PayRequest request = new PayRequest(item.getBizNo(), item.getBizNo(),
              PayRequestBizType.ORDER, PayRequestPayType.RECHARGE, item.getAmount(),
              fromAccount.getId(), toAccount.getId(), null);
          payRequestApiService.payRequest(request);

          fromAccount = accountApiService
              .findSubAccountByUserId(item.getUserId(), AccountType.AVAILABLE);
          toAccount = accountApiService
              .findSubAccountByUserId(item.getUserId(), AccountType.CONSUME);

          request = new PayRequest(item.getBizNo(), item.getBizNo(),
              PayRequestBizType.ORDER, PayRequestPayType.AVAILABLE2CONSUME,
              item.getAmount(), fromAccount.getId(), toAccount.getId(),
              request.getId());
          payRequestApiService.payRequest(request);
        }

        b = true;
      } else if(item.getStatus() == PaymentStatus.SUCCESS){
        b = true; //已经调用过了
      }
      break;
    }
    if (!b) {
      log.error("收银台更新状态失败 未找到 paymentMode=[" + paymentMode + "] orderNo=[" + bizNo + "]");
      throw new BizException(GlobalErrorCode.UNKNOWN, "状态更新失败");
    }
    items = listByBizNo(bizNo);
    CashierItem nextCashierItem = autoPaid(nextCashierItem(bizNo));
    if (nextCashierItem == null) {
      String[] batchBizNos = items.get(0).getBatchBizNos().split(",");
      MainOrderVO order;
      for (String batchBizNo : batchBizNos) {
        order = mainOrderService.loadByOrderNo(batchBizNo);
        if (null == order || CollectionUtils.isEmpty(order.getOrders())) {
          continue;
        }
        try {
          mainOrderService.pay(order.getOrderNo(), paymentMode, bizNo);
          //订单行支付后更新
          List<OrderVO> orderVOs = order.getOrders();
          for (OrderVO ordervo : orderVOs) {
            if (ordervo.getStatus() == OrderStatus.PAID) {
              continue;
            }
            String orderNo = ordervo.getOrderNo();
            Order updateOrder = new Order();
            BeanUtils.copyProperties(ordervo, updateOrder);
            updateOrder.setPayType(order.getPayType());
            orderService.update(updateOrder);
            orderService.pay(orderNo, paymentMode, bizNo);
          }
        } catch (BizException e) {
          log.error("cashier callback maiOrderService returns false, bizNo=[" + bizNo
              + "] ", e);
          // 中断事务，保证订单状态一致
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "支付回调失败", e);
        }
      }
    }
    return nextCashierItem;
  }

  private void updateOrderStatus(String bizNo){
    OutPay outPay = outPayService.queryOutPayBybillNo(bizNo);
    if(outPay != null && outPay.getStatus() == CLOSED){
      List<Order> orderList = orderService.loadByPayNo(bizNo);
      if(orderList != null && !orderList.isEmpty()) {
        for(Order order : orderList) {
          order.setStatus(OrderStatus.CANCELLED);
          orderService.update(order);
        }

        MainOrder mainOrder = mainOrderService.load(orderList.get(0).getMainOrderId());
        mainOrder.setStatus(MainOrderStatus.CANCELLED);
        mainOrderService.update(mainOrder);
      }
    }

    MainOrder mainOrder = mainOrderService.queryMainOrderByPayNo(bizNo);
    if (mainOrder != null) {
      List<Order> orderList = orderService.queryOrderByMainOrderId(mainOrder.getId());
      for (Order order : orderList) {
        if (order.getStatus() == OrderStatus.CANCELLED && order.getPaidFee() == null) {
          PaymentMode mode = mainOrder.getPayType();
          order.setPayType(mainOrder.getPayType());
          order.setPayNo(mainOrder.getPayNo());
          order.setRefundFee(mainOrder.getTotalFee());

          if (mode == PaymentMode.ALIPAY) {
            PaymentRequestVO paymentRequestVO = new PaymentRequestVO();
            paymentRequestVO.setOutTradeNo(mainOrder.getPayNo());
            paymentRequestVO
                .setTotalFee(mainOrder.getTotalFee().setScale(2, BigDecimal.ROUND_HALF_UP));
            paymentRequestVO.setOutRequestNo(mainOrder.getOrderNo());

            try {
              aliPayment.payRefund(null, null, paymentRequestVO);
            } catch (Exception e) {
              log.error("alipay refund error:", e);
            }
          } else if (mode == PaymentMode.WEIXIN_APP) {
            tenPayment.refund(order);
          } else if (mode == PaymentMode.WEIXIN_MINIPROGRAM) {
            tenPayment.refund(order);
          }

          break;
        }
      }
    }
  }

  private CashierItem autoPaid(CashierItem item) {
    CashierItem nextCashierItem = null;
    if (item == null) {
      return null;
    }
    if (StringUtils.isNotBlank(item.getCouponId())
        && item.getPaymentMode() == PaymentMode.XIANGQU) {
      User user = userService.load(item.getUserId());
      PaymentRequestVO request = new PaymentRequestVO();
      request.setCouponId(item.getCouponId());
      request.setTradeNo(item.getBizNo());
      request.setTotalFee(item.getAmount());
      request.setOutUserId(user.getExtUserId());
      request.setProductId(item.getProductId());
      request.setSubject(item.getBizNo());

      try {
        xqPayment.payRequest(null, null, request);
      } catch (Exception e) {
        e.printStackTrace();
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "想去红包使用失败");
      }

      //自动充值支付
      nextCashierItem = paid(item.getBizNo(), "order", item.getPaymentMode(), item.getId());
    } else {
      nextCashierItem = item;
    }
    return nextCashierItem;
  }

  @Override
  public BigDecimal loadPaidFee(String bizNo) {
    BigDecimal paidFee = cashierItemMapper.loadPaidFee(bizNo);
    return ObjectUtils.defaultIfNull(paidFee, BigDecimal.ZERO);
  }

  @Override
  public List<Map<String, Object>> loadHongBaoFee(String bizNo) {
    return cashierItemMapper.loadHongBaoFee(bizNo);
  }

  @Override
  public List<CouponInfoVO> loadCouponInfoByOrderNo(String bizNo) {
    return cashierItemMapper.loadCouponInfoByOrderNo(bizNo);
  }

  @Override
  public String findDefaultBizId(String bizNo) {
    List<CashierItem> items = listByBizNo(bizNo);
    String[] batchBizNos = items.get(0).getBatchBizNos().split(",");
    MainOrderVO mainOrderVO = mainOrderService.loadByOrderNo(batchBizNos[0]);
    return mainOrderVO.getId();
  }
  
  @Value("${pay.callback.third_refund_url}")
  private String thirdRefundCallbackUrl;
  private Logger thirdRefundLogger = OutPayLogs.thirdRefundLogger;
  
  /**
   * 支付回调失败，抛出异常，需要进行退款
   * @param cashierItem
   */
  private void doThirdRefund(CashierItem cashierItem) {
	  thirdRefundLogger.info("notify error entry...cashierItem:{}",JSON.toJSONString(cashierItem));
	  if(cashierItem == null )
		 return;
	  //初始化请求数据
	  ThirdRefundData data = new ThirdRefundData();
	  //系统来源  hanwin
	  data.setFromSystem(PayChannelConst.DEFAULT_FROM_SYSTEM);
	  //支付方式
	  if (cashierItem.getPaymentMode() == PaymentMode.ALIPAY) {
		  data.setPayType(PayTypeEnum.ALIPAY_APP.getPayType());
		  data.setPayChannel(PayChannelConst.ALIPAY);
	  }else if(cashierItem.getPaymentMode() == PaymentMode.WEIXIN_APP){
		  data.setPayType(PayTypeEnum.WECHAT_APP.getPayType());
		  data.setPayChannel(PayChannelConst.WECHAT);
	  }else if(cashierItem.getPaymentMode() == PaymentMode.WEIXIN_MINIPROGRAM){
		  data.setPayType(PayTypeEnum.WECHAT_MINIAPP.getPayType());
		  data.setPayChannel(PayChannelConst.WECHAT);
	  }
	  //支付渠道
	  if(cashierItem.getPaymentChannel() == PaymentChannel.EBS)
		  data.setPayChannel(PayChannelConst.EBANKPAY);
	  
	  data.setAmount(cashierItem.getAmount().multiply(new BigDecimal(100)).intValue());
	  data.setTotalAmount(cashierItem.getAmount().multiply(new BigDecimal(100)).intValue());
	  data.setOno(cashierItem.getBatchBizNos());
  	  data.setTradeNo(cashierItem.getBizNo());
  	  //data.setRefundNo(UniqueNoUtils.next2(UniqueNoType.R, orderService.nextval2()));
  	  data.setCpid(IdTypeHandler.decode(cashierItem.getUserId())+"");//已加密，如需解密需要IdTypeHandler.decode
  	  data.setCallbackUrl(thirdRefundCallbackUrl);
  	  thirdRefundLogger.info("throw payNotify exception for refund...thirdrefund:{}",JSON.toJSONString(data));
	  throw new PayNotifyException(200,PayNotifyException.SUCCESSMSG, data);
  }
}
