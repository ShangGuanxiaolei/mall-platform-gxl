package com.xquark.service.address.impl;

import com.xquark.dal.mapper.CustomerAddressMapper;
import com.xquark.dal.model.CustomerAddress;
import com.xquark.dal.model.SystemRegion;
import com.xquark.dal.model.User;
import com.xquark.dal.type.IdSequenceType;
import com.xquark.service.sequence.IdSequenceService;
import com.xquark.service.address.CustomerAddressService;
import com.xquark.service.address.CustomerAddressVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.http.SCrmHttpUtils;
import com.xquark.service.systemRegion.SystemRegionService;
import com.xquark.service.user.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.parboiled.common.Preconditions.checkNotNull;

/**
 * User: a9175
 * Date: 2018/6/21.
 * Time: 14:11
 */
@Service("customerAddressService")
public class CustomerAddressServiceImpl extends BaseServiceImpl implements CustomerAddressService {

    @Autowired
    private CustomerAddressMapper customerAddressMapper;

    @Autowired
    private SystemRegionService systemRegionService;

    @Autowired
    private UserService userService;
    @Autowired
    private IdSequenceService idSequenceService;

    @Autowired
    private SCrmHttpUtils sCrmHttpUtils;


    @Override
    public CustomerAddressVO saveUserAddress(CustomerAddress customerAddress, boolean notifySCrm) {
        if (customerAddress.getIsdefault() == null) {
            customerAddress.setIsdefault(false);
        }
        String userId = "";
        if (StringUtils.isEmpty(customerAddress.getOrderinguserId().toString())) {
            userId = getCurrentUser().getId();
        } else {
            userId = customerAddress.getOrderinguserId().toString(); //用户订单
        }
        if (customerAddress.getIsdefault() != null && customerAddress.getIsdefault()) {
            customerAddressMapper.emptyDefault(userId);
        }
        return saveUserAddress(customerAddress, userId, notifySCrm);
    }

    @Override   //保存客户订单
    public CustomerAddressVO preSaveUserAddress(String zoneId, String userId) {
        User user = userService.load(userId);
        checkNotNull(user, "用户不存在");
        CustomerAddress customerAddress = new CustomerAddress();
        Long cpid = idSequenceService.generateIdByType(IdSequenceType.MEMBER);
        customerAddress.setCpid(cpid);
        customerAddress.setType(2);
        customerAddress.setDistrictId(Long.parseLong(zoneId)); //三级区
        SystemRegion city = systemRegionService.findParent(zoneId);//二级市
        customerAddress.setCityId(city.getId());//
        SystemRegion parent = systemRegionService.findParent(city.getId().toString());
        customerAddress.setProvinceId(parent.getId());//一级省
        customerAddress.setReceivername(user.getName());
        customerAddress.setAddress("");
        customerAddress.setPhoneno(user.getPhone());
        customerAddress.setIsdefault(false);
        customerAddress.setIsdeleted(true);
        customerAddress.setOrderinguserId(Long.parseLong(user.getId()));
        if (customerAddress.getIsdefault() != null && customerAddress.getIsdefault()) {
            customerAddressMapper.emptyDefault(userId);
        }
        return this.saveUserAddress(customerAddress, userId, true);
    }

    @Override
    public CustomerAddressVO saveUserAddress(CustomerAddress customerAddress, String userId, boolean notifySCrm) {
        if (!StringUtils.isBlank(userId)) {
            customerAddress.setOrderinguserId(Long.parseLong(userId));
        }
        //暂时不用comomon
        customerAddress.setIsdefault(false);
        if (customerAddress.getIsdefault()) {
            // 重置改用户的其他地址为非默认
            // TODO trupser 中的默认地址更新写法，可以用一个sql更新做到，稍后 refine
            List<CustomerAddress> list = customerAddressMapper.selectUserAddresses(customerAddress.getOrderinguserId().toString());
            if (list != null && !list.isEmpty()) {
                for (CustomerAddress a : list) {
                    if (a.getIsdefault()) {
                        customerAddressMapper.updateForUnCommon(a.getOrderinguserId().toString());
                    }
                }
            }
        }
        //设置新添加的地址为默认地址
//		address.setIsDefault(true);
//		addressMapper.emptyDefault(userId);

        List<SystemRegion> systemRegionList = systemRegionService.listParents(customerAddress.getDistrictId().toString());
        if (StringUtils.isEmpty(customerAddress.getAddressid().toString())) {
            customerAddressMapper.insert(customerAddress);
        } else {
            String aId = customerAddress.getAddressid().toString();
            checkAddressLegal(aId); //检查地址是否存在
            customerAddressMapper.updateByPrimaryKeySelective(customerAddress);
            Long cpid = idSequenceService.generateIdByType(IdSequenceType.MEMBER);
            //Integer extId = customerAddressMapper.selectExtIdByPrimaryKey(aId);
            //address.setExtId(extId);
            customerAddress.setCpid(cpid);
        }
        if (notifySCrm) {
            try {
                // sCrmHttpUtils.postSyncAddressAsync(customerAddress.getAddressid(), customerAddress, systemRegionList);
                //未进行判断
            } catch (Exception e) {
                log.error("同步地址信息失败: " + e);
            }
        }

        StringBuilder details = new StringBuilder();
        for (SystemRegion systemRegion : systemRegionList) {
            details.append(systemRegion.getName());
        }
        details.append(customerAddress.getAddress());
        CustomerAddressVO vo = new CustomerAddressVO(customerAddress, details.toString());
        vo.setSystemRegion(systemRegionList);
        return vo;
    }

//	@Override
//	public Address updateUserAddress(Address address) {
//		User user = this.getCurrentUser();
//		if (null != user){
//			address.setUserId(user.getId());
//		}
//		addressMapper.updateByPrimaryKeySelective(address);
//		return address;
//	}


    @Override
    public CustomerAddressVO loadUserAddress(String addressId) {
        CustomerAddress customerAddress = customerAddressMapper.selectUserAddress(getCurrentUser().getId(), addressId);

        List<SystemRegion> systemRegionList = systemRegionService.listParents(customerAddress.getDistrictId().toString());
        String details = "";
        for (SystemRegion systemRegion : systemRegionList) {
            details += systemRegion.getName();
        }
        details += customerAddress.getAddress();
        CustomerAddressVO vo = new CustomerAddressVO(customerAddress, details);
        vo.setSystemRegion(systemRegionList);

        return vo;
    }

    @Override
    public List<CustomerAddressVO> listUserAddressesVo() {
        return listUserAddressesVo2(getCurrentUser().getId());
    }

    @Override
    public List<CustomerAddressVO> listUserAddressesVo2(String userId) {
        List<CustomerAddress> customerAddresses = customerAddressMapper.selectUserAddresses(userId);
        List<CustomerAddressVO> result = new ArrayList<CustomerAddressVO>();
        for (CustomerAddress address : customerAddresses) {
            CustomerAddressVO vo = changeAddressToVo(address);
            result.add(vo);
        }
        return result;
    }

    @Override
    public CustomerAddressVO changeAddressToVo(CustomerAddress address) {
        if (address == null) {
            return null;
        }
        List<SystemRegion> systemRegionlist = systemRegionService.listParents(address.getDistrictId().toString());
        String details = "";
        for (SystemRegion systemRegion : systemRegionlist) {
            details += systemRegion.getName();
        }
        details += address.getAddress();
        CustomerAddressVO vo = new CustomerAddressVO(address, details);

        vo.setSystemRegion(systemRegionlist);
        return vo;
    }

    @Override
    public List<CustomerAddress> listUserAddresses(String userId) {
        return customerAddressMapper.selectUserAddresses(userId);
    }

    @Override
    public int archiveAddress(String addressId, boolean notifySCrm) {
        if (notifySCrm) {
            // 同步删除SCRM系统地址表
            Integer extId = this.selectExtIdByPrimaryKey(addressId);
            if (extId != null) {
                sCrmHttpUtils.postDeleteAddressAsync(extId);
            }
        }
        return customerAddressMapper.updateForArchive(addressId);
    }
    @Override
    public int insert(CustomerAddress e) {
        return customerAddressMapper.insert(e);
    }

    @Override
    public CustomerAddress load(String id) {
        return customerAddressMapper.selectByPrimaryKey(id);
    }

    @Override
    public int delete(String id) {
        return customerAddressMapper.updateForArchive(id);
    }

    @Override
    public int undelete(String id) {
        return customerAddressMapper.updateForUnArchive(id);
    }


    @Override
    public CustomerAddressVO getLatestUserAddresses(String userId) {
        CustomerAddress CustomerAddress = customerAddressMapper.selectLatestUserAddresses(userId);
        return changeAddressToVo(CustomerAddress);
    }

    /**
     * 设置默认地址zzd
     */
    @Override
    public boolean asDefault(String addressId, String userId) {
        try {
            customerAddressMapper.emptyDefault(userId);
            int cnt = customerAddressMapper.setDefault(userId, addressId);

            Integer extId = customerAddressMapper.selectExtIdByPrimaryKey(addressId);
            if (extId != null) {
                sCrmHttpUtils.postSetDefaultAddressAsync(extId);
            }
            return (cnt == 1);
        } catch (Exception e) {
            e.printStackTrace();
            throw new UnsupportedOperationException();
        }

    }

    @Override
    public boolean asDefault(String addressId) {
        return this.asDefault(addressId, getCurrentUser().getId());
    }

    @Override
    public boolean asDefaultByAddressId(String addressId) {
        CustomerAddress customerAddressExists = this.load(addressId);
        if (customerAddressExists == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "地址不存在, 请先添加地址");
        }
        String userId = customerAddressExists.getOrderinguserId().toString();
        return this.asDefault(addressId, userId);
    }
    /**
     * 默认没有则选择最新修改的
     */
    @Override
    public CustomerAddressVO getDefault(String userId) {
        CustomerAddress customerAddress = null;
        List<CustomerAddress> customerAddresses = customerAddressMapper.getDefault(userId);
        if (CollectionUtils.isEmpty(customerAddresses)) {
            List<CustomerAddressVO> vos = listUserAddressesVo2(userId);
            customerAddress = vos.size() == 0 ? null : vos.get(0);
        } else {
            customerAddress = customerAddresses.get(0);
        }
        return changeAddressToVo(customerAddress);
    }

    @Override
    public CustomerAddressVO getDefault() {
        return getDefault(getCurrentUser().getId());
    }


    @Override
    public int updateByPrimaryKeySelective(CustomerAddress record) {
        return customerAddressMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public CustomerAddress selectById(String id) {
        return customerAddressMapper.selectById(id);
    }

    /**
     * consignee，phone,zoneId,street信息这些信息是否有存在的address
     */
    @Override
    public CustomerAddress selectByInfo(String userId, String consignee, String phone, String zoneId, String street) {
        return customerAddressMapper.selectByInfo(userId, consignee, phone, zoneId, street);
    }

    @Override
    public Integer selectExtIdByPrimaryKey(String id) {
        return customerAddressMapper.selectExtIdByPrimaryKey(id);
    }

    private void checkAddressLegal(String id) {
        boolean isLegal = customerAddressMapper.isExists(id);
        if (!isLegal) {
            throw new BizException(GlobalErrorCode.SCRM_ADDRESS_ILLEGAL, "地址id不存在或已删除");
        }
    }


}
