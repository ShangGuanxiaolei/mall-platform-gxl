package com.xquark.service.address;

import java.util.List;

import com.xquark.dal.model.Zone;

public class AddressVOEX {

  private List<AddressVO> addresses;

  private List<Zone> provinceList;

  private List<Zone> cityList;

  private List<Zone> districtList;

  public List<AddressVO> getAddresses() {
    return addresses;
  }

  public void setAddresses(List<AddressVO> addresses) {
    this.addresses = addresses;
  }

  public List<Zone> getProvinceList() {
    return provinceList;
  }

  public void setProvinceList(List<Zone> provinceList) {
    this.provinceList = provinceList;
  }

  public List<Zone> getCityList() {
    return cityList;
  }

  public void setCityList(List<Zone> cityList) {
    this.cityList = cityList;
  }

  public List<Zone> getDistrictList() {
    return districtList;
  }

  public void setDistrictList(List<Zone> districtList) {
    this.districtList = districtList;
  }

}
