package com.xquark.service.address.impl;

import com.xquark.dal.mapper.AddressMapper;
import com.xquark.dal.model.Address;
import com.xquark.dal.model.SystemRegion;
import com.xquark.dal.model.User;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.IdSequenceType;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.sequence.IdSequenceService;
import com.xquark.service.systemRegion.SystemRegionService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.parboiled.common.Preconditions.checkNotNull;

@Service("addressService")
public class AddressServiceImpl extends BaseServiceImpl implements AddressService {

  @Autowired
  private AddressMapper addressMapper;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private UserService userService;

  @Autowired
  private IdSequenceService sequenceService;

  @Autowired
  private SystemRegionService systemRegionService;

  @Override
  public AddressVO saveUserAddress(Address address, boolean notifySCrm) {
    if (address.getIsDefault() == null) {
      address.setIsDefault(false);
    }
    String userId = "";
    if (StringUtils.isEmpty(address.getUserId())) {
      userId = getCurrentUser().getId();
    } else {
      userId = address.getUserId();
    }
    if (address.getIsDefault() != null && address.getIsDefault()) {
      User user = userService.load(userId);
      if (user.getCpId() != null) {
        addressMapper.emptyDefault(String.valueOf(user.getCpId()));
      }
    }
    return saveUserAddress(address, userId, notifySCrm);
  }

  @Override
  public AddressVO preSaveUserAddress(String zoneId, String userId) {
    User user = userService.load(userId);
    checkNotNull(user, "用户不存在");
    Address address = new Address();
    address.setConsignee(user.getName());
    address.setZoneId(zoneId);
    address.setStreet("");
    address.setPhone(user.getPhone());
    address.setCommon(false);
    address.setIsDefault(true);
    address.setUserId(user.getId());
    if (address.getIsDefault() != null && address.getIsDefault()) {
      addressMapper.emptyDefault(userId);
    }
    return this.saveUserAddress(address, userId, true);
  }

  @Override
  public AddressVO saveUserAddress(Address address, String userId, boolean notifySCrm) {
    if (!StringUtils.isBlank(userId)) {
      address.setUserId(userId);
    }
    //暂时不用comomon
    address.setCommon(false);
    if (address.getCommon()) {
      // 重置改用户的其他地址为非默认
      // TODO trupser 中的默认地址更新写法，可以用一个sql更新做到，稍后 refine
      User user = userService.load(userId);
      List<Address> list = addressMapper.selectUserAddresses(String.valueOf(user.getCpId()));
      if (list != null && !list.isEmpty()) {
        for (Address a : list) {
          if (a.getCommon()) {
//            addressMapper.updateForUnCommon(a.getId());
          }
        }
      }
    }
    //设置新添加的地址为默认地址
//    address.setIsDefault(true);
//    addressMapper.emptyDefault(userId);

    Long cpId = userService.selectCpIdByUserId(userId);
    address.setCpId(cpId);
    if (StringUtils.isEmpty(address.getId())) {
      saveWithUniqueId(address);
    } else {
//      String aId = address.getId();
//      checkAddressLegal(aId);
      addressMapper.updateByPrimaryKeySelective(address);
//      Integer extId = addressMapper.selectExtIdByPrimaryKey(aId);
//      address.setExtId(extId);

    }

    AddressVO vo = fillRegion(address);

    return vo;
  }

//	@Override
//	public Address updateUserAddress(Address address) {
//		User user = this.getCurrentUser();
//		if (null != user){
//			address.setUserId(user.getId());
//		}
//		addressMapper.updateByPrimaryKeySelective(address);
//		return address;
//	}

  @Override
  public AddressVO loadUserAddress(String addressId) {
    User user = getCurrentUser();
    Address address = addressMapper.selectUserAddress(String.valueOf(user.getCpId()), addressId);

    return fillRegion(address);
  }

  @Override
  public AddressVO loadUserDefault(Long cpId) {
    Address defaultAdd = addressMapper.selectUserDefault(cpId);
    if (defaultAdd == null) {
      defaultAdd = addressMapper.selectUserFirst(cpId);
    }
    if (defaultAdd == null) {
      return null;
    }
    return fillRegion(defaultAdd);
  }

  private AddressVO fillRegion(Address defaultAdd) {
    if (defaultAdd == null) {
      return null;
    }
    List<SystemRegion> systemRegionList = systemRegionService.listParents(defaultAdd.getZoneId());
    StringBuilder details = new StringBuilder();
    for (SystemRegion systemRegion : systemRegionList) {
      details.append(systemRegion.getName());
    }
    details.append(defaultAdd.getStreet());
    AddressVO vo = new AddressVO(defaultAdd, details.toString());
    vo.setSystemRegions(systemRegionList);
    return vo;
  }

  @Override
  public AddressVO loadCurrDefault() {
    User curr = getCurrentUser();
    return loadUserDefault(curr.getCpId());
  }

  @Override
  public List<AddressVO> listUserAddressesVo() {
    User user = getCurrentUser();
    return listUserAddressesVo2(String.valueOf(user.getCpId()));
  }

  @Override
  public List<AddressVO> listUserAddressesVo2(String cpId) {

    List<Address> addresses = addressMapper.selectUserAddresses(cpId);
    List<AddressVO> result = new ArrayList<AddressVO>();
    for (Address address : addresses) {
      AddressVO vo = changeAddressToVo(address);
      result.add(vo);
    }
    return result;
  }

  @Override
  public AddressVO changeAddressToVo(Address address) {
    if (address == null) {
      return null;
    }
    return fillRegion(address);
  }

  @Override
  public List<Address> listUserAddresses(String userId) {
    User user = userService.load(userId);
    log.info("current userInfo = {}", user.toString());
    return addressMapper.selectUserAddresses(String.valueOf(user.getCpId()));
  }

  @Override
  public int archiveAddress(String addressId, boolean notifySCrm) {
    if (notifySCrm) {
      // 同步删除SCRM系统地址表
//      Integer extId = this.selectExtIdByPrimaryKey(addressId);
//      if (extId != null) {
//        sCrmHttpUtils.postDeleteAddressAsync(extId);
//      }
    }
    return addressMapper.updateForArchive(addressId);
  }

  public int insert(Address e) {
    return addressMapper.insert(e);
  }

  @Override
  public int insertOrder(Address address) {
    return addressMapper.insert(address);
  }

  @Override
  public Address load(String id) {
    return addressMapper.selectByPrimaryKey(id);
  }

  @Override
  public int delete(String id) {
    return addressMapper.updateForArchive(id);
  }

  @Override
  public int undelete(String id) {
    return addressMapper.updateForUnArchive(id);
  }


  @Override
  public AddressVO getLatestUserAddresses(String userId) {
    User user = userService.load(userId);
    Address address = addressMapper.selectLatestUserAddresses(String.valueOf(user.getCpId()));
    return changeAddressToVo(address);
  }


  /**
   * 设置默认地址zzd
   */

  @Override
  @Transactional
  public boolean asDefault(String addressId, String userId) {
    checkAddressLegal(addressId);
    User user = userService.load(userId);
    addressMapper.emptyDefault(String.valueOf(user.getCpId()));
    int cnt = addressMapper.setDefault(String.valueOf(user.getCpId()), addressId);

//    Integer extId = addressMapper.selectExtIdByPrimaryKey(addressId);
//    if (extId != null) {
//      sCrmHttpUtils.postSetDefaultAddressAsync(extId);
//    }

    return (cnt == 1);
  }

  @Override
  public boolean asDefault(String addressId) {
    return this.asDefault(addressId, getCurrentUser().getId());
  }

  @Override
  public boolean asDefaultByAddressId(String addressId) {
    Address addressExists = this.load(addressId);
    if (addressExists == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "地址不存在, 请先添加地址");
    }
    String userId = addressExists.getUserId();
    return this.asDefault(addressId, userId);
  }

  /**
   * 默认没有则选择最新修改的
   */
  @Override
  public AddressVO getDefault(String userId) {
    Address address = null;
    User user = userService.load(userId);
    List<Address> addresses = addressMapper.getDefault(String.valueOf(user.getCpId()));
    if (CollectionUtils.isEmpty(addresses)) {
      List<AddressVO> vos = listUserAddressesVo2(userId);
      address = vos.size() == 0 ? null : vos.get(0);
    } else {
      address = addresses.get(0);
    }
    return changeAddressToVo(address);
  }

  @Override
  public AddressVO getDefault() {
    User user = (User) getCurrentUser();
    return getDefault(String.valueOf(user.getCpId()));
  }


  @Override
  public int updateByPrimaryKeySelective(Address record) {
    return addressMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public Address selectById(String id) {
    return addressMapper.selectById(id);
  }

  /**
   * consignee，phone,zoneId,street信息这些信息是否有存在的address
   */
  @Override
  public Address selectByInfo(String userId, String consignee, String phone, String zoneId,
                              String street) {
    User user = userService.load(userId);
    return addressMapper
            .selectByInfo(String.valueOf(user.getCpId()), consignee, phone, zoneId, street);
  }

  @Override
  public Integer selectExtIdByPrimaryKey(String id) {
    return null;
//    return addressMapper.selectExtIdByPrimaryKey(id);
  }

  /**
   * 通过取号器获取唯一id并保存到中台地址表
   */
  public boolean saveWithUniqueId(Address address) {
    Long aId = sequenceService
            .generateIdByType(IdSequenceType.ADDRESS);
    address.setId(String.valueOf(aId));
    boolean ret = addressMapper.insert(address) > 0;
    address.setId(IdTypeHandler.encode(aId));
    return ret;
  }

  private void checkAddressLegal(String id) {
    boolean isLegal = addressMapper.isExists(id);
    if (!isLegal) {
      throw new BizException(GlobalErrorCode.SCRM_ADDRESS_ILLEGAL, "地址id不存在或已删除");
    }
  }

}
