package com.xquark.service.address;

import com.xquark.dal.model.CustomerAddress;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: a9175
 * Date: 2018/6/21.
 * Time: 10:51
 * ${DESCRIPTION}
 */
public interface CustomerAddressService {

    int delete(String id);

    int undelete(String id);

    /**
     * 检查默认的not null，和default value等约束要求，
     */
    int insert(CustomerAddress e);

    CustomerAddress load(String id);

    /**
     * 保存用户地址
     */
    CustomerAddressVO saveUserAddress(CustomerAddress customerAddress, boolean notifySCrm);

    /**
     * 更新用户地址
     * @param address
     * @return
     */
//	Address updateUserAddress(Address address);

    /**
     * 用户获取某一地址
     */
    CustomerAddressVO loadUserAddress(String addressId);

    /**
     * 获取用户的所有地址
     */
    List<CustomerAddressVO> listUserAddressesVo();

    CustomerAddressVO getLatestUserAddresses(String userId);

    /**
     * 获取会话用户的所有地址
     */
    List<CustomerAddress> listUserAddresses(String userId);

    /**
     * 用户删除地址
     */
    int archiveAddress(String addressId, boolean notifySCrm);


    List<CustomerAddressVO> listUserAddressesVo2(String userId);

    CustomerAddressVO preSaveUserAddress(String zoneId, String userId);

    CustomerAddressVO saveUserAddress(CustomerAddress customerAddress, String userId, boolean notifySCrm);


    /**
     * 用户设置默认地址
     */
    boolean asDefault(String addressId, String userId);

    boolean asDefault(String addressId);

    boolean asDefaultByAddressId(String addressId);

    CustomerAddressVO getDefault(String userId);

    CustomerAddressVO getDefault();

    CustomerAddressVO changeAddressToVo(CustomerAddress address);

    int updateByPrimaryKeySelective(CustomerAddress record);

    CustomerAddress selectById(String id);

    /**
     * consignee，phone,zoneId,street信息这些信息是否有存在的address
     */
    CustomerAddress selectByInfo(String userId, String consignee, String phone, String zoneId, String street);

    Integer selectExtIdByPrimaryKey(String id);


}
