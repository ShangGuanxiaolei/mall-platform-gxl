package com.xquark.service.address;

import java.util.List;

import com.xquark.dal.model.SystemRegion;
import org.springframework.beans.BeanUtils;

import com.xquark.dal.model.Address;
import com.xquark.dal.model.Zone;

public class AddressVO extends Address {

  private static final long serialVersionUID = -7472612896539013215L;

  private String details;

  private List<Zone> zones;

  private List<SystemRegion> systemRegions;

  public AddressVO() {
  }

  public AddressVO(Address address, String details) {
    BeanUtils.copyProperties(address, this);
    this.details = details;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }

  public List<Zone> getZones() {
    return zones;
  }

  public void setZones(List<Zone> zones) {
    this.zones = zones;
  }

  public List<SystemRegion> getSystemRegions() {
    return systemRegions;
  }

  public void setSystemRegions(List<SystemRegion> systemRegions) {
    this.systemRegions = systemRegions;
  }
}
