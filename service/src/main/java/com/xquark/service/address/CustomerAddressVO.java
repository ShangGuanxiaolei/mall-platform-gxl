package com.xquark.service.address;

import com.xquark.dal.model.Address;
import com.xquark.dal.model.CustomerAddress;
import com.xquark.dal.model.SystemRegion;
import com.xquark.dal.model.Zone;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.List;

/**
 * User: a9175
 * Date: 2018/6/21.
 * Time: 13:50
 */
public class CustomerAddressVO extends CustomerAddress implements Serializable {

    private static final long serialVersionUID = -4095332278579602798L;

    private String details; //说明

    private List<SystemRegion> systemRegion;

    public CustomerAddressVO() {
    }

    public CustomerAddressVO(CustomerAddress customerAddress, String details) {
        BeanUtils.copyProperties(customerAddress, this);
        this.details = details;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public List<SystemRegion> getSystemRegion() {
        return systemRegion;
    }

    public void setSystemRegion(List<SystemRegion> systemRegion) {
        this.systemRegion = systemRegion;
    }

    @Override
    public String toString() {
        return "CustomerAddressVO{" +
                "details='" + details + '\'' +
                ", systemRegion=" + systemRegion +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CustomerAddressVO that = (CustomerAddressVO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(details, that.details)
                .append(systemRegion, that.systemRegion)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(details)
                .append(systemRegion)
                .toHashCode();
    }
}
