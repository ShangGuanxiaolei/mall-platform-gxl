package com.xquark.service.address;

import com.xquark.dal.model.Address;
import com.xquark.service.ArchivableEntityService;
import java.util.List;

public interface AddressService extends ArchivableEntityService<Address> {

  /**
   * 保存用户地址
   */
  AddressVO saveUserAddress(Address address, boolean notifySCrm);

  /**
   * 更新用户地址
   * @param address
   * @return
   */
//	Address updateUserAddress(Address address);

  /**
   * 用户获取某一地址
   */
  AddressVO loadUserAddress(String addressId);

  /**
   * 查询用户默认地址
   */
  AddressVO loadUserDefault(Long cpId);

  AddressVO loadCurrDefault();

  /**
   * 获取用户的所有地址
   */
  List<AddressVO> listUserAddressesVo();

  AddressVO getLatestUserAddresses(String userId);

  /**
   * 获取会话用户的所有地址
   */
  List<Address> listUserAddresses(String userId);

  /**
   * 用户删除地址
   */
  int archiveAddress(String addressId, boolean notifySCrm);


  List<AddressVO> listUserAddressesVo2(String userId);

  AddressVO preSaveUserAddress(String zoneId, String userId);

  Address saveUserAddress(Address form, String userId, boolean notifySCrm);


  /**
   * 用户设置默认地址
   */
  boolean asDefault(String addressId, String userId);

  boolean asDefault(String addressId);

  boolean asDefaultByAddressId(String addressId);

  AddressVO getDefault(String userId);

  AddressVO getDefault();

  AddressVO changeAddressToVo(Address address);

  int updateByPrimaryKeySelective(Address record);

  Address selectById(String id);

  /**
   * consignee，phone,zoneId,street信息这些信息是否有存在的address
   */
  Address selectByInfo(String userId, String consignee, String phone, String zoneId, String street);

  Integer selectExtIdByPrimaryKey(String id);
}
