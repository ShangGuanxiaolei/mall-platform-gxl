package com.xquark.service.apiVisitorLog.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.ApiVisitorLogMapper;
import com.xquark.dal.model.ApiVisitorLog;
import com.xquark.service.apiVisitorLog.ApiVisitorLogService;

@Service("apiVisitorLogService")
public class ApiVisitorLogServiceImpl implements ApiVisitorLogService {

  private Logger log = LoggerFactory.getLogger(ApiVisitorLogServiceImpl.class);

  @Autowired
  private ApiVisitorLogMapper apiVisitorLogMapper;

  @Override
  public void visit(String userId, String url) {
    ApiVisitorLog bean = apiVisitorLogMapper.findByUserAndUrl(userId, url);
    if (bean == null) {
      try {
        apiVisitorLogMapper.insert(userId, url);
      } catch (DuplicateKeyException e) {
        log.info("插入数据异常，userId=" + userId + ", url=" + url);
      }
    } else {
      apiVisitorLogMapper.visit(bean.getId());
    }
  }

  @Override
  public ApiVisitorLog findByUserAndUrl(String userId, String url) {
    return apiVisitorLogMapper.findByUserAndUrl(userId, url);
  }
}
