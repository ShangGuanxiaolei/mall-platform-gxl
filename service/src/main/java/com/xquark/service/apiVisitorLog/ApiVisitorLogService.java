package com.xquark.service.apiVisitorLog;

import com.xquark.dal.model.ApiVisitorLog;

public interface ApiVisitorLogService {

  void visit(String userId, String url);

  ApiVisitorLog findByUserAndUrl(String userId, String url);
}
