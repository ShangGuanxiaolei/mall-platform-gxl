package com.xquark.service.module;

import com.xquark.dal.model.MerchantRoleModule;
import com.xquark.dal.model.Module;
import com.xquark.dal.vo.ModuleVO;
import com.xquark.dal.vo.UrlRoleVO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者: wangxh 创建日期: 17-3-27 简介:
 */
public interface ModuleService {

  Integer save(Module module);

  Module load(String id);

  Module loadChild(String parent, String name);

  List<Module> loadSubs(String parentId);

  /**
   * 返回JSTree需要的数据格式
   */
  List<Map<String, Object>> listTreeMenu();

  List<ModuleVO> treeMenuListAll();

  /**
   * 整理出各个角色菜单取并集后的菜单列表
   */
  List<ModuleVO> listTree(List<String> roles);

//    List<ModuleVO> listTree(String role);

  Integer loadSubCounts(String parentId);

  Integer loadCounts();

  Integer delete(String id);

  Integer deleteModules(String... ids);

  Integer update(Module module);

  List<Module> listAll();

  List<Module> listByRole(String roleId);

  List<Module> listByOrder(String parentId, String order, Pageable pageable,
      Sort.Direction direction);

  /**
   * 绑定角色跟菜单关系
   */
  Integer bindRoleModule(List<MerchantRoleModule> roleModuleList);

  /**
   * 解除绑定
   */
  Integer unBindRoleModule(String[] moduleIdList, String roleId);

  List<Module> listByRoleName(String roleName);

  /**
   * 根据url获取该url的访问权限
   */
  List<UrlRoleVO> loadUrlRoles(String url);

  HashMap<String, List<String>> loadUrlRoleMap();
}
