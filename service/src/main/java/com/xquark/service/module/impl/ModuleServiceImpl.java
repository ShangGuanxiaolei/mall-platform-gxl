package com.xquark.service.module.impl;

import com.xquark.dal.mapper.ModuleMapper;
import com.xquark.dal.model.MerchantRoleModule;
import com.xquark.dal.model.Module;
import com.xquark.dal.vo.ModuleTreeVo;
import com.xquark.dal.vo.ModuleVO;
import com.xquark.dal.vo.UrlRoleVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.module.ModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 作者: wangxh 创建日期: 17-3-27 简介:
 */
@Service
public class ModuleServiceImpl implements ModuleService {

  @Autowired
  private ModuleMapper moduleMapper;

  @Override
  public Integer save(Module module) {
    if (module == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "菜单不能为空");
    }
    Module moduleExists = moduleMapper.selectChild(module.getParentId(), module.getName());
    if (moduleExists != null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "级菜单已存在");
    }
    if (module.getParentId() != null) {
      Module parent = moduleMapper.select(module.getParentId());
      if (parent == null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "上级菜单不存在");
      }
      // 手动设置排序号
      module.setIs_Leaf(true);
    } else {
      module.setIs_Leaf(false);
    }
    int subCounts = moduleMapper.loadSubCounts(module.getParentId());
    module.setIs_AutoExpand(false);
    module.setSortNo(++subCounts);
    module.setArchive(false);
    return moduleMapper.insert(module);
  }

  @Override
  public Module load(String id) {
    return moduleMapper.select(id);
  }

  @Override
  public Module loadChild(String parent, String name) {
    return moduleMapper.selectChild(parent, name);
  }

  @Override
  public List<Module> loadSubs(String parentId) {
    return moduleMapper.loadSubs(parentId);
  }

  @Override
  public Integer loadCounts() {
    return moduleMapper.loadCounts();
  }

  @Override
  public List<Map<String, Object>> listTreeMenu() {
    List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
    Map<String, Object> rootMap = new HashMap<String, Object>();
    rootMap.put("id", "0");
    rootMap.put("text", "所有菜单");
    List<ModuleTreeVo> moduleList = treeMenuList(moduleMapper.listAll(), null);
    rootMap.put("children", moduleList);
    result.add(rootMap);
    return result;
  }

  @Override
  public List<ModuleVO> listTree(List<String> roles) {
    Set<Module> moduleSet = new HashSet<Module>();
    for (String role : roles) {
      moduleSet.addAll(moduleMapper.listByRoleName(role));
    }
    // 在所有菜单中递归整理出moduleSet中有权限访问的菜单以及它的父菜单
    return treeMenuList(moduleMapper.listAll(), null, moduleSet);
  }

//    @Override
//    public List<ModuleVO> listTree(String role) {
//        List<Module> permittedModules = moduleMapper.listByRoleName(role);
//        return treeMenuSet(permittedModules, null);
//    }

  @Override
  public Integer loadSubCounts(String parentId) {
    return moduleMapper.loadSubCounts(parentId);
  }

  @Override
  public Integer delete(String id) {
    List<String> idList = collectIds(id);
    return deleteModules(idList.toArray(new String[idList.size()]));
  }

  /**
   * 递归查找所有子id
   */
  private List<String> collectIds(String id) {
    List<String> result = new ArrayList<String>();
    result.add(id);
    List<Module> subList = moduleMapper.loadSubs(id);
    for (Module module : subList) {
      result.addAll(collectIds(module.getId()));
    }
    return result;
  }

  @Override
  public Integer deleteModules(String... ids) {
    return moduleMapper.deleteModules(ids);
  }

  @Override
  public Integer update(Module module) {
    if (module == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "菜单不能为空");
    }
    Module moduleExists = moduleMapper.select(module.getId());
    if (moduleExists == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "要更新的菜单不存在");
    }
    return moduleMapper.update(module);
  }

  @Override
  public List<Module> listAll() {
    return moduleMapper.listAll();
  }

  @Override
  public List<Module> listByOrder(String parentId, String order, Pageable pageable,
      Sort.Direction direction) {
    return moduleMapper.listByOrder(parentId, order, pageable, direction);
  }

  @Override
  public Integer bindRoleModule(List<MerchantRoleModule> roleModuleList) {
    return moduleMapper.insertRoleModule(roleModuleList);
  }

  @Override
  public Integer unBindRoleModule(String[] moduleIdList, String roleId) {
    return moduleMapper.deleteRoleModule(moduleIdList, roleId);
  }

  @Override
  public List<Module> listByRole(String roleId) {
    return moduleMapper.listByRole(roleId);
  }

  @Override
  public List<Module> listByRoleName(String roleName) {
    return moduleMapper.listByRoleName(roleName);
  }

  /**
   * 递归整理所有菜单
   */
  private List<ModuleTreeVo> treeMenuList(List<Module> list, String parentId) {
    List<ModuleTreeVo> result = new ArrayList<ModuleTreeVo>();
    for (Module module : list) {
      String menuId = module.getId();
      String pid = module.getParentId();
      ModuleTreeVo moduleTreeVo = ModuleTreeVo.buildTreeVo(module);
      if (pid == parentId || (pid != null && pid.equals(parentId))) {
        moduleTreeVo.setChildren(treeMenuList(list, menuId));
        result.add(moduleTreeVo);
      }
    }
    return result;
  }

  private List<ModuleVO> treeMenuListAll(List<Module> list, String parentId) {
    List<ModuleVO> result = new ArrayList<ModuleVO>();
    for (Module module : list) {
      String menuId = module.getId();
      String pid = module.getParentId();
      ModuleVO moduleVO = new ModuleVO(module);
      if (pid == parentId || pid != null && pid.equals(parentId)) {
        moduleVO.setChildren(treeMenuListAll(list, menuId));
        result.add(moduleVO);
      }
    }
    return result;
  }

  public List<ModuleVO> treeMenuListAll() {
    return treeMenuListAll(moduleMapper.listAll(), null);
  }

  /**
   * 递归整理在roleModules中的菜单，并添加它的父菜单
   *
   * @param list 所有菜单
   * @param roleModules 可见菜单Set
   */
  private List<ModuleVO> treeMenuList(List<Module> list, String parentId, Set<Module> roleModules) {
    List<ModuleVO> result = new ArrayList<ModuleVO>();
    for (Module module : list) {
      String menuId = module.getId();
      String pid = module.getParentId();
      ModuleVO moduleVO = new ModuleVO(module);
      // 根据equals判断
      boolean contains = roleModules.contains(module);
      if (pid == parentId || (pid != null && pid.equals(parentId))) {
        List<ModuleVO> cList = treeMenuList(list, menuId, roleModules);
        moduleVO.setChildren(cList);
        // 1. 符合条件的子节点
        // 2. 有子节点的父节点
        if (contains || (pid == null && cList.size() > 0)) {
          result.add(moduleVO);
        }
      }
    }
    return result;
  }


  @Override
  public List<UrlRoleVO> loadUrlRoles(String url) {
    return moduleMapper.selectUrlRoles(url);
  }

  @Override
  public HashMap<String, List<String>> loadUrlRoleMap() {
    List<UrlRoleVO> urlRoles = moduleMapper.selectAllUrlRoles();
    if (urlRoles == null) {
      return null;
    }

    HashMap<String, List<String>> urlRoleMap = new HashMap<String, List<String>>();
    String url;
    String prefix = "ROLE_";
    for (UrlRoleVO urlRoleVO : urlRoles) {
      url = urlRoleVO.getUrl();
      if (urlRoleMap.containsKey(url)) {
        List<String> roleList = urlRoleMap.get(url);
        roleList.add(prefix + urlRoleVO.getRole());
      } else {
        List<String> roleList = new ArrayList<String>();
        roleList.add(prefix + urlRoleVO.getRole());
        urlRoleMap.put(url, roleList);
      }
    }
    return urlRoleMap;
  }
}
