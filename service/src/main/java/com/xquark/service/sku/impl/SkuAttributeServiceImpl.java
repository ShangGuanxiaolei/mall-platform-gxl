package com.xquark.service.sku.impl;

import com.xquark.dal.mapper.SkuAttributeItemMapper;
import com.xquark.dal.mapper.SkuAttributeMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.SkuAttribute;
import com.xquark.dal.model.SkuAttributeItem;
import com.xquark.dal.vo.SkuAttributeVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.sku.SkuAttributeService;
import com.xquark.utils.ExcelUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

/**
 * sku规格属性Service
 *
 * @author chh 2017-11-22
 */
@Service("skuAttributeService")
public class SkuAttributeServiceImpl extends BaseServiceImpl implements SkuAttributeService {

  /***
   * 空sku，用于占位保证商品详情返回的skulist是填满的
   */
  private final static Sku EMPTY_SKU = new Sku();

  static {
    EMPTY_SKU.setAmount(-1001);
    EMPTY_SKU.setId("mhaa");
    EMPTY_SKU.setPrice(BigDecimal.ZERO);
    EMPTY_SKU.setNetWorth(BigDecimal.ZERO);
    EMPTY_SKU.setPoint(BigDecimal.ZERO);
    EMPTY_SKU.setServerAmt(BigDecimal.ZERO);
    EMPTY_SKU.setDeductionDPoint(BigDecimal.ZERO);
  }

  @Autowired
  private SkuAttributeMapper skuAttributeMapper;

  @Autowired
  private ProductService productService;

  @Autowired
  private SkuAttributeItemMapper skuAttributeItemMapper;

  @Autowired
  private SkuMapper skuMapper;





	@Override
  public SkuAttribute selectByPrimaryKey(String id) {
    return skuAttributeMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(SkuAttribute role) {
    return skuAttributeMapper.insert(role);
  }

  @Override
  public int modify(SkuAttribute role) {
    return skuAttributeMapper.modify(role);
  }

  @Override
  public int delete(String id) {
    return skuAttributeMapper.delete(id);
  }





  /**
   * 列表查询页面
   */
  @Override
  public List<SkuAttribute> list(Pageable page, Map<String, Object> params) {
    return skuAttributeMapper.list(page, params);
  }

  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return skuAttributeMapper.selectCnt(params);
  }

  /**
   * 返回系统设置的所有属性规格
   */
  @Override
  public List<SkuAttribute> findAll() {
    return skuAttributeMapper.findAll();
  }

  /**
   * 返回某个商品所有选中的规格属性和对应属性值
   */
  @Override
  public List<SkuAttributeVO> findAttributesByProductId(String productId) {
    List<SkuAttributeVO> result = new ArrayList<SkuAttributeVO>();
    Product product = productService.load(productId);
    String attributes = product.getAttributes();
    if (StringUtils.isNotEmpty(attributes)) {
      // 商品所有选中的规格属性
      List<String> productAttributes = Arrays.asList(attributes.split("-"));

      // 商品所有选中的规格属性值
      Set<String> skuAttributes = new HashSet<String>();
      List<String> skuAttributes_ = productService.findAttributeByProductId(productId);
      for (String skuAttribute : skuAttributes_) {
        if (StringUtils.isNotEmpty(skuAttribute)) {
          String[] skus = skuAttribute.split("-");
          for (String sku : skus) {
            skuAttributes.add(sku);
          }
        }
      }

      for (String productAttribute : productAttributes) {
        SkuAttribute skuAttribute = selectByPrimaryKey(productAttribute);
        SkuAttributeVO vo = new SkuAttributeVO();
        BeanUtils.copyProperties(skuAttribute, vo);
        // 查出该规格属性下所有的属性值
        List<SkuAttributeItem> skuItems = new ArrayList<SkuAttributeItem>();
        List<SkuAttributeItem> items = skuAttributeItemMapper
            .findAllByAttributeId(productAttribute);
        // 判断如果商品所有选中的规格属性值在此属性中，则加入到SkuAttributeVO中
        for (SkuAttributeItem item : items) {
          if (skuAttributes.contains(item.getId())) {
            skuItems.add(item);
          }
        }
        vo.setItems(skuItems);
        result.add(vo);
      }
    }
    return result;
  }

  /***
   * sku数组排序
   * @param product
   * @return skuList
   */
  @Override
  public List<Sku> sortSkuList(ProductVO product) {
    Map<String, Integer> attributeItemIndexMap = new HashMap<>(); //存储属性值在当前规格下面的索引
    Map<Integer, Integer> attributeLayerMap = new HashMap<>();//存储每一个规格层次的属性值的总数
    int totalSkuCount = 1;

    List<SkuAttributeVO> skuAttributes = product.getSkuAttributes();
    int itemIndex = 0;
    int layer = 0;
    for (SkuAttributeVO skuAttribute : skuAttributes) {
      List<SkuAttributeItem> items = skuAttribute.getItems();
      if (CollectionUtils.isNotEmpty(items)) {
        ++layer;
        attributeLayerMap.put(layer - 1, items.size());
        for (SkuAttributeItem item : items) {
          attributeItemIndexMap.put(item.getName(), itemIndex);
          ++itemIndex;
        }
        itemIndex = 0;
      }
    }
    //计算总sku的数量
    Collection<Integer> itemCount = attributeLayerMap.values();
    for (Integer count : itemCount) {
      totalSkuCount *= count;
    }
    //构建一个对应数量的sku的数组
    List<Sku> skuList = new ArrayList<>(totalSkuCount);
    for (int i = 0; i < totalSkuCount; i++) {
      skuList.add(i, EMPTY_SKU);
    }

    //将所有的sku按顺序存储到skuList中
    List<Sku> rawSkuList = product.getSkus();
    for (Sku sku : rawSkuList) {
      String specs = sku.getSpec();
      if (StringUtils.isNotBlank(specs) && specs.split(",").length == layer) {
        //计算每个sku在返回数组中的index
        int index = 0;
        String[] items = specs.split(",");
        for (int i = 0; i < layer; i++) {
          String item = items[i];
          int itemOffset = attributeItemIndexMap.get(item);
          int temp = 1;
          for (int j = i + 1; j < layer; j++) {
            Integer count = attributeLayerMap.get(j);
            temp *= count;
          }
          index += itemOffset * temp;
        }
        skuList.set(index, sku);
      }
    }
    return skuList;
  }

  public Boolean importExcel(File file) {
    Boolean buttum = true;
    InputStream inputStream;
    String[][] line;
    try {
      inputStream = new FileInputStream(file);
      line = ExcelUtils.excelImport(inputStream);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    }
    if (line == null || line.length == 0) {
      throw new RuntimeException("excelData is null");
    }
    List<Sku> skus = new ArrayList<>();
    String errorMessage = "";
    try {
      for (int j = 1; j < line.length; j++) {
        Sku sku = new Sku();
        for (int i = 0; i < line[j].length; i++) {
          if (line[j][i] == null || line[j][i].equals("")) {
            continue;
          }
          switch (i) {
            case 0:
              //产品编码
              errorMessage = "第" + (j + 1) + "行第" + (i + 1) + "列的产品编码出现错误";
              sku.setSkuCode(line[j][i]);
              break;
            case 1:
              //售价
              errorMessage = "第" + (j + 1) + "行第" + (i + 1) + "列的售价出现错误";
              sku.setPrice(new BigDecimal(line[j][i]));
              break;
            case 2:
              //兑换价
              errorMessage = "第" + (j + 1) + "行第" + (i + 1) + "列的兑换价出现错误";
              BigDecimal bigDecimal = new BigDecimal(line[j][i]);
              bigDecimal = sku.getPrice().subtract(bigDecimal);
              bigDecimal = bigDecimal.multiply(new BigDecimal(10));
              bigDecimal = bigDecimal.setScale(0, BigDecimal.ROUND_HALF_UP);
              sku.setDeductionDPoint(bigDecimal);
              break;
            case 3:
              //积分
              errorMessage = "第" + (j + 1) + "行第" + (i + 1) + "列的积分出现错误";
              sku.setPoint(new BigDecimal(line[j][i]));
              break;
            case 4:
              //净值
              errorMessage = "第" + (j + 1) + "行第" + (i + 1) + "列的净值出现错误";
              sku.setNetWorth(new BigDecimal(line[j][i]));
              break;
          }
        }
        if (sku.getSkuCode() == null || sku.getSkuCode().equals("")) {
          break;
        }
        skus.add(sku);
      }
      errorMessage = "success";
      importExcel(skus);
    } catch (Exception e) {
      if (!errorMessage.equals("success")) {
        throw new RuntimeException(errorMessage);
      } else {
        e.printStackTrace();
      }
    }
    return buttum;
  }

	/*@Override
	public List<Sku> requestSkuImgs(String skuId, String productId) {
		return skuMapper.getSkuImg(skuId,productId);
	}*/

	@Transactional
  public void importExcel(List<Sku> skus) {
    for (Sku s : skus) {
      skuMapper.updateSkuByCode(s);
    }
  }
}
