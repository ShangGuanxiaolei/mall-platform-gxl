package com.xquark.service.sku.impl;

import com.xquark.dal.mapper.SkuAttributeItemMapper;
import com.xquark.dal.model.SkuAttributeItem;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.sku.SkuAttributeItemService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * sku规格属性值Service
 *
 * @author chh 2017-11-22
 */
@Service("skuAttributeItemService")
public class SkuAttributeItemServiceImpl extends BaseServiceImpl implements
    SkuAttributeItemService {

  @Autowired
  private SkuAttributeItemMapper skuAttributeItemMapper;

  @Override
  public SkuAttributeItem selectByPrimaryKey(String id) {
    return skuAttributeItemMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(SkuAttributeItem role) {
    return skuAttributeItemMapper.insert(role);
  }

  @Override
  public int modify(SkuAttributeItem role) {
    return skuAttributeItemMapper.modify(role);
  }

  @Override
  public int delete(String id) {
    return skuAttributeItemMapper.delete(id);
  }

  /**
   * 列表查询页面
   */
  @Override
  public List<SkuAttributeItem> list(Pageable page, Map<String, Object> params) {
    return skuAttributeItemMapper.list(page, params);
  }

  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return skuAttributeItemMapper.selectCnt(params);
  }

  /**
   * 查找某个属性规格的所有属性值
   */
  @Override
  public List<SkuAttributeItem> findAllByAttributeId(String attributeId) {
    return skuAttributeItemMapper.findAllByAttributeId(attributeId);
  }

  /**
   * 查找所有属性值
   */
  @Override
  public List<SkuAttributeItem> findAll() {
    return skuAttributeItemMapper.findAll();
  }


}
