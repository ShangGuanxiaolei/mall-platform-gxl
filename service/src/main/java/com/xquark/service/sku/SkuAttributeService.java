package com.xquark.service.sku;

import com.xquark.dal.model.Sku;
import com.xquark.dal.model.SkuAttribute;
import com.xquark.dal.vo.SkuAttributeVO;
import com.xquark.service.product.vo.ProductVO;
import org.springframework.data.domain.Pageable;

import java.io.File;
import java.util.List;
import java.util.Map;


/**
 * sku规格属性Service
 *
 * @author chh 2017-11-22
 */
public interface SkuAttributeService {





  SkuAttribute selectByPrimaryKey(String id);

  int insert(SkuAttribute role);

  int modify(SkuAttribute role);

  int delete(String id);

  /**
   * 列表查询页面
   */
  List<SkuAttribute> list(Pageable page, Map<String, Object> params);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 返回系统设置的所有属性规格
   */
  List<SkuAttribute> findAll();

  /**
   * 返回某个商品所有选中的规格属性和对应属性值
   */
  List<SkuAttributeVO> findAttributesByProductId(String productId);

  List<Sku> sortSkuList(ProductVO product);

  Boolean importExcel(File file);

	//List<Sku> requestSkuImgs(String skuId, String productId);
}
