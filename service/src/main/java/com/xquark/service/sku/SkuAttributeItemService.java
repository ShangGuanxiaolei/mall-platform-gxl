package com.xquark.service.sku;

import com.xquark.dal.model.SkuAttributeItem;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;


/**
 * sku规格属性值Service
 *
 * @author chh 2017-11-22
 */
public interface SkuAttributeItemService {


  SkuAttributeItem selectByPrimaryKey(String id);

  int insert(SkuAttributeItem role);

  int modify(SkuAttributeItem role);

  int delete(String id);

  /**
   * 列表查询页面
   */
  List<SkuAttributeItem> list(Pageable page, Map<String, Object> params);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 查找某个属性规格的所有属性值
   */
  List<SkuAttributeItem> findAllByAttributeId(String attributeId);

  /**
   * 查找所有属性值
   */
  List<SkuAttributeItem> findAll();

}
