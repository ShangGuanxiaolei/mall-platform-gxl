package com.xquark.service.activityPtOutTime.impl;

import com.xquark.dal.mapper.ActivityPtOutTImeMapper;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.mapper.ServMessageMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.ServMessageStatus;
import com.xquark.service.activityCancelPt.CancelPtService;
import com.xquark.service.activityPtOutTime.PtlSelectOutTimeService;
import com.xquark.service.order.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author liuyandong
 * @date 2018/9/10 9:15
 */
@Service
public class PtSelectOutTimeServiceImp implements PtlSelectOutTimeService {
  @Autowired private ActivityPtOutTImeMapper activityPtOutTImeMapper;
  @Autowired private CancelPtService cancelPtService;
  @Autowired private ServMessageMapper servMessageMapper;
  @Autowired private OrderMapper orderMapper;
  @Autowired private OrderService orderService;

  private static final Logger LOGGER = LoggerFactory.getLogger(PtSelectOutTimeServiceImp.class);

  @Override
  public void PtOutTimeService() {
    // 查询所有的有效活动
    List<String> codes = activityPtOutTImeMapper.selectActivityCode();
    for (String pCode : codes) {
      try {
        // 查询活动的成团有效时间
        int effectTime = activityPtOutTImeMapper.selectEffectTime(pCode);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp time =
            Timestamp.valueOf(
                df.format(new Date(System.currentTimeMillis() - effectTime * 1000 * 3600)));
        // 获得失效的拼团编码//TODO 将当前时间-成团有效时间，判断大于开团时间，即为过期
        List<String> list = activityPtOutTImeMapper.selectOutTimePt(time, pCode);
        List<String> list1 = activityPtOutTImeMapper.selectOutStock(pCode);
        List<String> list2 = activityPtOutTImeMapper.selectTimeOver(pCode);

        // 根据拼团memberInfo查询, 部分退款
        List<String> list3 = activityPtOutTImeMapper.selectOutStockDetail(pCode);
        // 时间过期的退款
        tryCancel(pCode, list);
        // 库存不足的退款
        tryCancel(pCode, list1);
        // 提前结束的活动退款
        tryCancel(pCode, list2);

        tryCancelByDetail(pCode, list3);
      } catch (Exception e) {
        LOGGER.error("pCode: {} 退款失败", pCode);
      }
    }
  }

    @Override
    public void clearUnrefundPieceOrders() {
        List<String> orderNos = activityPtOutTImeMapper.findUnrefundPieceOrders();
        for (String orderNo : orderNos) {
            Order order = orderMapper.selectByOrderNo(orderNo);
            // 不要影响其他退款订单
            refundSystem(order);
        }
    }

    private void refundSystem(Order order) {
      try {
          if (null != order && order.getPaidFee().compareTo(BigDecimal.ZERO) > 0
                  && OrderStatus.PAIDNOSTOCK.equals(order.getStatus())) {
              orderService.refundSystem(order.getId(), order.getPaidFee());
          }
      } catch (Exception e) {
          LOGGER.error("拼团订单【{}】补偿退款失败", order.getOrderNo(), e);
      }
    }

    private void tryCancelByDetail(String pCode, List<String> detailCodeList) {
     for (String detailCode : detailCodeList) {
      try{
        cancelPtService.cancelPtDetail(pCode, detailCode);
      } catch (Exception e) {
        LOGGER.error("pCode: {}, detailCode: {}, 退款失败", pCode, detailCode);
      }
    }
    }

  private void tryCancel(String pCode, List<String> tranCodeList) {
    for (String tranCode : tranCodeList) {
      try{
        cancelPtService.cancelPt(pCode, tranCode);
      } catch (Exception e) {
        LOGGER.error("pCode: {}, tranCode: {}, 退款失败", pCode, tranCode);
      }
    }
  }
}
