package com.xquark.service.activityPtOutTime;

/**
 * @author liuyandong
 * @date 2018/9/9 23:46
 */
public interface PtOrderCancelService {
    void orderCancel(String orderId);
}
