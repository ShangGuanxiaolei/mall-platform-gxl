package com.xquark.service.activityPtOutTime.impl;

import com.xquark.dal.mapper.ActivityPtOrderStatusMapper;
import com.xquark.dal.mapper.OrderItemMapper;
import com.xquark.dal.mapper.ServMessageMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.ServMessageStatus;
import com.xquark.service.activityCancelPt.impl.CancelPtServiceImp;
import com.xquark.service.activityPtOutTime.PtOrderCancelService;
import com.xquark.service.order.OrderService;
import com.xquark.service.product.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author liuyandong
 * @date 2018/9/9 23:50
 */
@Service
public class PtOrderCancelServiceImp implements PtOrderCancelService {

    private final static Logger LOGGER = LoggerFactory.getLogger(CancelPtServiceImp.class);
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderItemMapper orderItemMapper;
    @Autowired
    private ProductService productService;
    @Autowired
    private ActivityPtOrderStatusMapper orderStatusMapper;
    @Autowired private ServMessageMapper servMessageMapper;
    @Override
    public void orderCancel(String mainOrderNo) {
        //ordermainid为订单主编号,a为主订单id
        int a=orderStatusMapper.selectMainOrderId(mainOrderNo);
        //order为子订单实体类
        Order order = orderStatusMapper.selectOrderId(a);
        //需要改成如果非 已支付无库存 状态 订单无法取消
        if(order!=null) {
           //拼團轉臺
            if (OrderStatus.PAIDNOSTOCK.equals(order.getStatus())) {
                BigDecimal bGoodsFee = order.getPaidFee();//实际付款费用
                // 调用退款
                orderService.refundSystem(order.getId(), bGoodsFee);
                ServMessage(order.getBuyerId(),order.getOrderNo());
            }
        }
    }


    /**
     * 发送消息
     */
    private void ServMessage(String buyerId,String orderNo){
        try {
                String cpid=servMessageMapper.selectMemberId(buyerId);
                Integer push=servMessageMapper.message(ServMessageStatus.FAILPIECECONTENT.getWord(orderNo),ServMessageStatus.TYPEMSG.getWord(),ServMessageStatus.STATUS.getWord(),cpid,ServMessageStatus.TYPESYSTEM.getWord());
                if(push<1){
                    LOGGER.error("成团消息推送错误，用户id为{}",cpid);
                }

        }catch (Exception e){
            LOGGER.error("拼团成员buyerId{}消息推送错误！",buyerId,e);
        }
    }

}
