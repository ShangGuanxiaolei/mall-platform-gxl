package com.xquark.service.enableCode;

import com.xquark.dal.model.Empower;

public interface EnableCodeService {


    Empower memberInfo(Long cpId,long shopId,String shopName);

    boolean updateNickName(String nickName,Long cpId);

}
