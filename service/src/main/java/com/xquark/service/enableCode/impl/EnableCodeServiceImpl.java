package com.xquark.service.enableCode.impl;

import com.xquark.dal.mapper.MemberInfoMapper;
import com.xquark.dal.mapper.UserMapper;
import com.xquark.dal.model.Empower;
import com.xquark.dal.model.MemberTypeAndNickName;
import com.xquark.dal.model.User;
import com.xquark.service.config.SystemConfigService;
import com.xquark.service.enableCode.EnableCodeService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service("enableCodeService")
public class EnableCodeServiceImpl implements EnableCodeService {

    @Autowired
    private MemberInfoMapper memberInfoMapper;

    @Autowired
    private SystemConfigService systemConfigService;

    @Autowired
    private UserMapper userMapper;

    /**
     * 根据身份查询赋能卡数据
     * @param cpId
     * @return
     */
    @Override
    public Empower memberInfo(Long cpId,long shopId,String shopName) {
        Empower empower=new Empower();
        empower.setCpid(cpId);
        empower.setShopId(shopId);

        MemberTypeAndNickName memberNickAndType=memberInfoMapper.selectMemberInfo(cpId);
        // 微信分享标题
        empower.setWechatShareTitle(getWechatShareTitle(memberNickAndType, cpId));

        //商家
        if(-1!=shopId){

            empower.setType(1);
            empower.setShopName(shopName);
            IdentityNameByType(empower,cpId);
            //TODO setQrCode
            return empower;
        }

        //非商家
        if(-1==shopId){
            String nickNameIsNull=memberInfoMapper.selectUserNickNameIsNull(cpId);
            String nickName=memberInfoMapper.selectNickName(cpId);
            empower.setType(0);
            IdentityNameByType(empower,cpId);

            if(!("").equals(nickNameIsNull) && null!=nickNameIsNull) {
                empower.setMemberName(memberNickAndType.getNickName());
            }else{
                empower.setMemberName(nickName);
            }
        }
        return empower;
    }

    private String getWechatShareTitle(MemberTypeAndNickName m, Long cpId){
        String nickName = "";
        if(Objects.isNull(m) || StringUtils.isBlank(m.getNickName())){
            User user = userMapper.selectByCpId(cpId);
            if(user != null){
                nickName = user.getName();
            }
        } else {
            nickName = m.getNickName();
        }
        // 获取配置
        String title = systemConfigService.getValue("hw_invite_code_wechat_share_title");

        if(StringUtils.isBlank(title))
            title = "送您15元新手红包，邀请您一起选购汉薇精品！";

        StringBuilder sb = new StringBuilder();
        title = sb.append(nickName).append(title).toString();
        return title;
    }
    /**
     * 根据cpid修改昵称
     * @param nickName
     * @param cpId
     * @return
     */
    @Override
    public boolean updateNickName(String nickName, Long cpId) {
        boolean a=memberInfoMapper.updateNickName(nickName, cpId);
        return a;
    }



    public void IdentityNameByType(Empower empower,Long cpId){

        MemberTypeAndNickName memberNickAndType = memberInfoMapper.selectMemberInfo(cpId);

        empower.setIdentityType("RC");
        empower.setIdentityName("顾客");

        if(("DS").equals(memberNickAndType.getIdentityType()) || ("DS").equals(memberNickAndType.getViviType())){
            empower.setIdentityType("DS");
            empower.setIdentityName("会员");
        }
        if(("SP").equals(memberNickAndType.getIdentityType()) || ("SP").equals(memberNickAndType.getViviType())){
            empower.setIdentityType("SP");
            empower.setIdentityName("代理");
        }

    }


}
