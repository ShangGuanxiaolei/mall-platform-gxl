package com.xquark.service.fragment.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xquark.dal.mapper.FragmentImageMapper;
import com.xquark.dal.model.FragmentImage;
import com.xquark.dal.vo.FragmentImageVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.fragment.FragmentImageService;

@Service("fragmentImageService")
public class FragmentImageServiceImpl extends BaseServiceImpl implements FragmentImageService {

  @Autowired
  private FragmentImageMapper fragmentImageMapper;

  @Override
  public int insert(FragmentImage fragmentImage) {
    Integer maxIdx = fragmentImageMapper.selectMaxByFragmentId(fragmentImage.getFragmentId());
    fragmentImage.setIdx(maxIdx == null ? 0 : maxIdx + 1);
    return fragmentImageMapper.insert(fragmentImage);
  }

  public int deleteById(String id) {
    return fragmentImageMapper.deleteById(id);
  }

  @Override
  public int deleteByFragmentId(String id) {
    return fragmentImageMapper.deleteByFragmentId(id);
  }

  @Override
  public List<FragmentImageVO> selectByFragmentId(String fragmentId) {
    return fragmentImageMapper.selectByFragmentId(fragmentId);
  }

  @Override
  @Transactional
  public void moveBefore(String srcId, String destId) {
    FragmentImageVO dest = fragmentImageMapper.selectById(destId);
    fragmentImageMapper.incAllBeforeDest(dest.getFragmentId(), dest.getIdx(), 1);
    fragmentImageMapper.updateSrcIdx(srcId, dest.getIdx());
  }

  @Override
  @Transactional
  public void moveAfter(String srcId, String destId) {
    FragmentImageVO dest = fragmentImageMapper.selectById(destId);
    fragmentImageMapper.decAllAfterDest(dest.getFragmentId(), dest.getIdx(), -1);
    fragmentImageMapper.updateSrcIdx(srcId, dest.getIdx());
  }

  @Override
  @Transactional
  public boolean batchDeleteByFragmentIds(List<String> fragmentIds) {
    if (fragmentIds.isEmpty()) {
      return false;
    }
    return fragmentImageMapper.batchDeleteByFragmentIds(fragmentIds);

  }
}
