package com.xquark.service.fragment;

import java.util.List;

import com.xquark.dal.model.ProductFragment;
import com.xquark.service.BaseService;

public interface ProductFragmentService extends BaseService {

  int insert(ProductFragment productFragment);

  int deleteById(String id);

  int deleteByProductId(String productId);

  List<ProductFragment> selectByProductId(String productId);

  void moveBefore(String srcId, String desId);

  void moveAfter(String srcId, String desId);
}
