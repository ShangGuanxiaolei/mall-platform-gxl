package com.xquark.service.fragment.impl;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xquark.dal.mapper.FragmentMapper;
import com.xquark.dal.model.Fragment;
import com.xquark.dal.vo.FragmentVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.fragment.FragmentService;

@Service("fragmentService")
public class FragmentServiceImpl extends BaseServiceImpl implements FragmentService {

  @Autowired
  private FragmentMapper fragmentMapper;

  @Override
  public int insert(Fragment fragment) {
    Integer maxIdx = fragmentMapper.selectMaxByShopId(fragment.getShopId());
    fragment.setIdx(maxIdx == null ? 0 : maxIdx + 1);
    return fragmentMapper.insert(fragment);
  }

  @Override
  public int update(Fragment fragment) {
    return fragmentMapper.update(fragment);
  }

  @Override
  public void update(Fragment... fragments) {
    if (fragments == null || ArrayUtils.isEmpty(fragments)) {
      return;
    }

    for (Fragment f : fragments) {
      update(f);
    }
  }

  @Override
  public int delete(String id) {
    return fragmentMapper.deleteById(id);
  }

  @Override
  public FragmentVO selectById(String id) {
    return fragmentMapper.selectById(id);
  }

  @Override
  public List<FragmentVO> selectByProductId(String productId) {
    List<FragmentVO> list = fragmentMapper.selectByProductId(productId);
    if (list == null) {
      return Collections.emptyList();
    }

    // 过滤数据为null的设置为空
    for (Fragment vo : list) {
      if (vo.getDescription() == null) {
        vo.setDescription(StringUtils.EMPTY);
      }
    }

    return list;
  }

  @Override
  public List<FragmentVO> selectByShopId(String shopId) {
    return fragmentMapper.selectByShopId(shopId);
  }

  @Override
  @Transactional
  public void moveBefore(String srcId, String destId) {
    FragmentVO dest = fragmentMapper.selectById(destId);
    fragmentMapper.incAllBeforeDest(dest.getShopId(), dest.getIdx(), 1);
    fragmentMapper.updateSrcIdx(srcId, dest.getIdx());
  }

  @Override
  @Transactional
  public void moveAfter(String srcId, String destId) {
    FragmentVO dest = fragmentMapper.selectById(destId);
    fragmentMapper.decAllAfterDest(dest.getShopId(), dest.getIdx(), -1);
    fragmentMapper.updateSrcIdx(srcId, dest.getIdx());
  }

  @Override
  public boolean batchDeleteByFragmentIds(List<String> fragmentIds) {
    if (fragmentIds.isEmpty()) {
      return false;
    }
    return fragmentMapper.batchDeleteByFragmentIds(fragmentIds);
  }


}
