package com.xquark.service.fragment;

import java.util.List;

import com.xquark.dal.model.FragmentImage;
import com.xquark.dal.vo.FragmentImageVO;
import com.xquark.service.BaseService;

public interface FragmentImageService extends BaseService {

  int insert(FragmentImage fragmentImage);

  int deleteById(String id);

  int deleteByFragmentId(String fragmentId);

  List<FragmentImageVO> selectByFragmentId(String fragmentId);

  void moveBefore(String srcId, String desId);

  void moveAfter(String srcId, String desId);

  boolean batchDeleteByFragmentIds(List<String> fragmentIds);
}
