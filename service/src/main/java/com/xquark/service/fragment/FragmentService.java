package com.xquark.service.fragment;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.Fragment;
import com.xquark.dal.vo.FragmentVO;
import com.xquark.service.BaseService;

public interface FragmentService extends BaseService {

  int insert(Fragment fragment);

  int update(Fragment fragment);

  void update(Fragment... fragments);

  int delete(String id);

  FragmentVO selectById(String id);

  List<FragmentVO> selectByProductId(String productId);

  List<FragmentVO> selectByShopId(@Param(value = "shopId") String shopId);

  void moveBefore(String srcId, String desId);

  void moveAfter(String srcId, String desId);

  boolean batchDeleteByFragmentIds(List<String> fragmentIds);
}
