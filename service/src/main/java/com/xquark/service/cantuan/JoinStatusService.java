package com.xquark.service.cantuan;

import com.xquark.dal.mapper.PromotionOrderDetailMapper;
import com.xquark.dal.mapper.RecordPromotionPgMemberInfoMapper;
import com.xquark.dal.model.PromotionPgMemberInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 修改参团状态
 * @Author: tanggb
 * @Time: Create in 20:55 2018/9/12
 * @Modified by:
 */
@Service
public class JoinStatusService {

    @Autowired
    private RecordPromotionPgMemberInfoMapper recordPromotionPgMemberInfoMapper;

    @Autowired
    private PromotionOrderDetailMapper promotionOrderDetailMapper;


    /**
     * 通过订单号更新参团状态
     * @param orderId
     */
    public void updateStatus(String orderId){
        //1.根据订单号查寻出行号
        String pTranDetailCode = promotionOrderDetailMapper.findPTranDetailCode(orderId);
        //2.根据行号更新参团状态
        PromotionPgMemberInfo promotionPgMemberInfo = new PromotionPgMemberInfo();
        promotionPgMemberInfo.setPieceGroupDetailCode(pTranDetailCode);
//        promotionPgMemberInfo.setIsGroupHead(0);
//        int isGroupHead = promotionPgMemberInfo.getIsGroupHead();
//        if(!(isGroupHead==1)){
//            promotionPgMemberInfo.setIsGroupHead(0);
//        }
        promotionPgMemberInfo.setStatus(1);
        recordPromotionPgMemberInfoMapper.updateStatus(promotionPgMemberInfo);
    }

}
