package com.xquark.service.cantuan;

import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.PromotionConstants;
import com.xquark.dal.mapper.PromotionPgDetailMapper;
import com.xquark.dal.model.PromotionPgDetail;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pricing.base.PieceStateException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author wangxinhua.
 * @date 2018/11/8 跟缓存相关的拼团service
 */
@Service
public class PieceCacheService {

  private final static Logger LOGGER = LoggerFactory.getLogger(PieceCacheService.class);

  private final PromotionPgDetailMapper promotionPgDetailMapper;

  @Autowired
  public PieceCacheService(PromotionPgDetailMapper promotionPgDetailMapper) {
    this.promotionPgDetailMapper = promotionPgDetailMapper;
  }

  /**
   * 开团后在redis中初始化总可参团人数
   */
  public void initTranMember(String tranCode, String detailCode, Date expireAt) {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    PromotionPgDetail detail = promotionPgDetailMapper.selectByDetailCode(detailCode);
    if (detail == null) {
      LOGGER.error("tranCode: {}, detailCode: {}, detail不存在", tranCode, detailCode);
      return;
    }
    String restMemberKey = PromotionConstants.getRestTranMemberKey(tranCode);
    String totalMemberKey = PromotionConstants.getTotalTranMemberKey(tranCode);
    // 除去发起人
    int amount = detail.getPieceGroupNum() - 1;

    //TODO 拼团三期加入插队拼团，此处需要修改
    // 支付回调可能会导致多次调用, 只在不存在时初始化
    // 剩余可参团人数
    redisUtils.setIfAbsent(restMemberKey, amount);
    // 总可参团人数
    redisUtils.setIfAbsent(totalMemberKey, amount);

    if (expireAt != null) {
      Date now = new Date();
      // 拼团时间到期后一天把key移除
      long expireMillis = expireAt.getTime() - now.getTime() + TimeUnit.DAYS.toMillis(1);
      redisUtils.expire(totalMemberKey, expireMillis, TimeUnit.MILLISECONDS);
      redisUtils.expire(restMemberKey, expireMillis, TimeUnit.MILLISECONDS);
    }

    LOGGER.info("团 {} 初始化完毕 总可参团人数: {}, 剩余可参团人数: {}", tranCode, amount, amount);
  }


  /**
   * 参团后把redis中的参团人数减一
   */
  public void decrementTranRestMember(String tranCode) {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    String key = PromotionConstants.getRestTranMemberKey(tranCode);
    if (redisUtils.hasKey(key)) {
      LOGGER.info("tranCode: {} 参团人数减1", tranCode);
      redisUtils.increment(key, -1);
    }
  }

  /**
   * 参团失败后把redis中的参团人数加一
   */
  public void incrementTranRestMember(String tranCode) {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    String key = PromotionConstants.getRestTranMemberKey(tranCode);
    if (redisUtils.hasKey(key)) {
      LOGGER.info("tranCode: {} 参团人数加1", tranCode);
      redisUtils.incrementOne(key);
    }
  }


  /**
   * 获取参团中是否锁定, 且锁定人不是自己
   */
  public boolean isPayFreeUesd(String tranCode, Long cpId) {
    RedisUtils<Long> redisUtils = RedisUtilFactory.getInstance(Long.class);
    String key = PromotionConstants.getFreePayMemberKey(tranCode);
    final Long lockCpId = redisUtils.get(key);
    LOGGER.info("isLock = " + lockCpId);
    if (lockCpId == null) {
      // 没有被占用
      return false;
    }
    // 被别人占用了才返回true
    return !Objects.equals(cpId, lockCpId);
  }
  /**
   * 减少剩余免单人数
   */
  public boolean lockPayFreeMember(String tranCode, Long cpId) {
    RedisUtils<Long> redisUtils = RedisUtilFactory.getInstance(Long.class);
    String key = PromotionConstants.getFreePayMemberKey(tranCode);
    return redisUtils.setIfAbsent(key, cpId);
  }

  /**
   * 增加剩余免单人数
   */
  public void unLockFreePayMember(String tranCode, Long cpId) {
    RedisUtils<Long> redisUtils = RedisUtilFactory.getInstance(Long.class);
    String key = PromotionConstants.getFreePayMemberKey(tranCode);
    Long lockCpId = redisUtils.get(key);
    if (Objects.equals(lockCpId, cpId)) {
      LOGGER.info("解锁新人免单名额 {} :: {}", tranCode, cpId);
      redisUtils.del(key);
    }
  }

  /**
   * 获取拼团当前参团人数
   */
  public Integer getCurrTranMember(String tranCode) {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    String key = PromotionConstants.getRestTranMemberKey(tranCode);
    return redisUtils.get(key);
  }

  /**
   * 获取拼团当前总可参团人数
   */
  public Integer getTotalTranMemebr(String tranCode) {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    String key = PromotionConstants.getTotalTranMemberKey(tranCode);
    return redisUtils.get(key);
  }

  /**
   * 校验当前已参团人数是否已满
   */
  public boolean isOverMember(String tranCode) throws PieceStateException {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    String restMemberKey = PromotionConstants.getRestTranMemberKey(tranCode);
    Integer restMember = redisUtils.get(restMemberKey);
    if (restMember == null) {
      // 开团信息没有被初始化到redis, 异常情况, 不能参团
      LOGGER.error("tranCode: {} 开团信息未初始化到redis", tranCode);
      throw new PieceStateException("当前开团信息不正确");
    }
    return restMember <= 0;
  }

  /**
   * 检查当前团的免单人数是否已经达到上限
   */
  public boolean isOverFreePayMember(String tranCode) throws PieceStateException {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    String restFreePayMemberKey = PromotionConstants.getFreePayMemberKey(tranCode);
    Integer restFreePayMember = redisUtils.get(restFreePayMemberKey);
    if (restFreePayMember == null) {
      // 开团信息没有被初始化到redis, 异常情况, 不能参团
      LOGGER.error("tranCode: {} 开团信息未初始化到redis", tranCode);
      throw new PieceStateException("当前开团信息不正确");
    }
    return restFreePayMember <= 0;
  }

  /**
   * @deprecated 副作用，调用方自己处理
   * 校验参团人数, 并同时减少当前参团人数 // TODO 原子操作
   */
  @Deprecated
  public void checkAndDecrement(String tranCode) {
    // 参团限购, 参团人数检验
    if (StringUtils.isNotBlank(tranCode)) {
      if (check(tranCode)) {
        decrementTranRestMember(tranCode);
        return;
      }
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "参团人数已满");
    }
  }

  /**
   * 使用享受拼主价的次数
   * @param cpid
   */
  public void incrementPromotionPriceNum(String cpId,String detailCode) {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    final String key = PromotionConstants.getPromotionPriceNum(cpId, detailCode);
    if (redisUtils.hasKey(key)) {
      redisUtils.increment(key, 1);
    }
  }

  /**
   * 返回享受拼主价的次数
   * @param cpId cpId
   * @param detailCode detailCode
   */
  public void decrementPromotionPriceNum(String cpId,String detailCode) {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    final String key = PromotionConstants.getPromotionPriceNum(cpId, detailCode);
    if (redisUtils.hasKey(key)) {
      redisUtils.increment(key, -1);
    }
  }


    /**
     * 是否超过享受拼主最大限制
     * @param cpId cpId
     * @param detailCode detailCode
     * @param limit limit
     * @return true or false
     */
    public boolean isCanObtainOpenMemberPrice(String cpId, String detailCode,Integer limit) {
        if (limit == null) {
            return true;
        }
        RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
        final String key = PromotionConstants.getPromotionPriceNum(cpId, detailCode);
        if (redisUtils.hasKey(key)) {
          //存在key
          return redisUtils.get(key) < limit;
        } else {
          //不存在key，设置拼主价次数
          redisUtils.set(key, 0);
        }
        return true;
    }
  /**
   * 校验参团人数
   * @param tranCode 团code
   * @return true 检验成功 | false 校验失败 (团满了)
   */
  public boolean check(String tranCode) {
    if (StringUtils.isNotBlank(tranCode)) {
      // 参团
      try {
        return !this.isOverMember(tranCode);
      } catch (PieceStateException e) {
        throw new BizException(GlobalErrorCode.PIECE_ERROR, e.getMessage());
      }
    }
    return false;
  }
}
