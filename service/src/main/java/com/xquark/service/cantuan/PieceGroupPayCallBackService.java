package com.xquark.service.cantuan;

import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.MainOrder;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.PromotionPgTranInfo;
import com.xquark.dal.model.mypiece.StockCheckBean;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.MainOrderStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.PayStatus;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.PromotionOrderDetailVo;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.mypiece.StockCheckService;
import com.xquark.service.order.OrderService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 *
 * @Author: tanggb
 * @Time: Create in 14:13 2018/9/12
 * @Modified by:
 */
@Service
public class PieceGroupPayCallBackService {

    private Log logger = LogFactory.getLog(PieceGroupPayCallBackService.class);

    @Autowired
    private MainOrderMapper mainOrderMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderService orderService;

    @Autowired
    PromotionOrderDetailMapper promotionOrderDetailMapper;

    @Autowired
    PromotionPgDetailMapper promotionPgDetailMapper;

    @Autowired
    PromotionPgMemberInfoMapper promotionPgMemberInfoMapper;

    @Autowired
    RecordPromotionPgTranInfoMapper recordPromotionPgTranInfoMapper;

    @Autowired
    private StockCheckService stockCheckService;

    @Autowired
    private OrderItemMapper orderItemMapper;

//    private static final String SUCCESS_GROUP = "2";

    /**
     * 通过订单号查询出拼团编号
     * @param orderId
     * @return
     */
    public String getPTranCode (String orderId) {
        //1.通过订单号查询出拼团编号
        String pTranCode = promotionOrderDetailMapper.findPTranCode(orderId);

        return pTranCode;
    }

    /**
     * 通过订单号查询出拼团订单信息
     * @param orderId
     * @return
     */
    public Map<String, Object> findPTranDetailCode (String orderId, String subOrderId) {
        //1.通过订单号查询出拼团编号
        Map<String, Object> pTranCode = promotionOrderDetailMapper.findPgOrderInfo(orderId, subOrderId);

        return pTranCode;
    }
    /**
     * 通过子订单ESO123456...00012查主订单
     */
    public String findEsByEso(String Eso){
        return mainOrderMapper.findEsByEso(Eso);
    }

    /**
     * 更新订单状态信息
     * @param orderNo
     * @param payNo
     */
//    @Transactional
    public void updateOrderStatus (String orderNo, String payNo, MainOrderStatus status) {
        MainOrderVO order = loadByOrderNo(orderNo);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("payNo", payNo);
        // 更新支付单号
        MainOrder updateOrder = new MainOrder();
        updateOrder.setId(order.getId());
        updateOrder.setPayNo(payNo);
        updateOrder.setStatus(status);
        updateOrder.setPaidStatus(PayStatus.SUCCESS);
        updateOrder.setPaidAt(new Date());
        update(updateOrder);
    }

    /**
     * 检查用户是否参加过拼团活动
     * @param pTranCode pTranCode
     * @param memberId memberId
     * @return boolean
     */
    public boolean hadJoined (String pTranCode, String memberId) {
        int joinTimes = promotionPgMemberInfoMapper.checkJoinTimes(pTranCode, memberId);
        return joinTimes > 0;
    }

    /**
     * 活动订单生效：扣减库存、订单状态修改
     * @param pCode 活动编码
     * @param pTranCode 拼团编码
     */
    @Transactional
    public void effectOrder (String pCode, String pTranCode) {
        //查询（拼团）活动的商品
//        List<PromotionSkuVo> skus = promotionOrderDetailMapper.findSkus(pCode);
        List<Map> skus2 = promotionOrderDetailMapper.findSkus2(pCode);
//        if (CollectionUtils.isEmpty(skus)) {
//            return;
//        }
        if (CollectionUtils.isEmpty(skus2)) {
            return;
        }
        String skuCode = (String) skus2.get(0).get("sku_code");
//        String skuCode = skus.get(0).getSkuCode();
        String lockKey = pCode + skuCode;
        //4.2 订单生效
//        List<String> orders = promotionOrderDetailMapper.findOrders(pTranCode);
        List<Map> orders = promotionOrderDetailMapper.findOrderMapList(pTranCode);
        if (CollectionUtils.isEmpty(orders)) {
            return;
        }

        //拿RedisUtil实例
        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class,1000, 2000);
        try {
            //拿锁
            boolean isOk = redisUtils.lock(lockKey);
            int times =3;
            //库存锁定失败则重试三次
            if (!isOk) {
                while (times > 0) {
                    isOk = redisUtils.lock(lockKey);
                    if (isOk) {
                        break;
                    } else {
                        times--;
                    }
                }
            }
            //库存锁定失败则退出
            if (!isOk) {
                return;
            }
            //拿锁成功，则库存扣减
            //计算扣减库存数量
            StockCheckBean stockCheckBean = new StockCheckBean();
            int total = 0;
            for (int i=0; i < orders.size(); i++) {
                Map<String, Object> orderMap = orders.get(i);
                if (null == orderMap.get("amount")) {
                    continue;
                }
                int amount = (Integer)orderMap.get("amount");
                total = total + amount;
            }
            stockCheckBean.setResume_sku_num(total);
            stockCheckBean.setP_code(pCode);
            stockCheckBean.setSku_code(skuCode);
            logger.info("成团成功，拼团pTranCode=" + pTranCode + ";扣减skuCode="+skuCode+"库存total=" + total);
            //库存扣减，库存不足则退出
            boolean isSuccess = stockCheckService.deduceStock(stockCheckBean);
            if (!isSuccess) {
                return;
            }

            //4.2.1 循环修改订单状态
            for (int i=0; i < orders.size(); i++) {
                Map<String, Object> orderMap = orders.get(i);
                if (null == orderMap.get("order_detail_code")) {
                    continue;
                }
                String orderItemNo = (String)orderMap.get("order_detail_code");
                String order_head_code = (String) orderMap.get("order_head_code");
                logger.info("成团成功，拼团pTranCode=" + pTranCode + ";更新orderNo="+orderItemNo+"订单状态");
//                updateOrderStatus(order_head_code);
                updateOrderStatus2(order_head_code, orderItemNo);

            }
            //4.3 修改成团状态
            PromotionPgTranInfo promotionPgTranInfo = new PromotionPgTranInfo();
            promotionPgTranInfo.setPieceGroupTranCode(pTranCode);
            promotionPgTranInfo.setPieceStatus("2");
            logger.info("成团成功，拼团pTranCode=" + pTranCode + ";更新成团状态");
            recordPromotionPgTranInfoMapper.updatePieceStatus(promotionPgTranInfo);
        } catch (InterruptedException e) {
            logger.warn("get stock lock failed,", e);
            return;
        } finally {
            redisUtils.unlock(lockKey);
        }
    }

    /**
     * 活动订单生效：扣减库存、订单状态修改
     * @param pTranCode
     *
     *
     */
    public void effectOrder (String pTranCode) {
        //查询活动编码
        List<PromotionOrderDetailVo> voList = promotionOrderDetailMapper.findOrderInfos(pTranCode);
        if (CollectionUtils.isEmpty(voList)) {
            return;
        }
        String pCode = voList.get(0).getpCode();

        this.effectOrder(pCode, pTranCode);
    }

    /**
     * 修改拼团支付状态
     * @param orderId
     */
    private void updateOrderStatus(String orderId) {
        // 更新支付单号
        MainOrder updateOrder = new MainOrder();
        updateOrder.setOrderNo(orderId);
        //已支付
        updateOrder.setStatus(MainOrderStatus.PAID);
        updateOrder.setPaidStatus(PayStatus.SUCCESS);
        updateOrder.setPaidAt(new Date());
        update(updateOrder);
    }

    /**
     * 修改拼团支付未支付状态
     * @param orderNo
     */
    public void updateOrderStatusNoStock(String orderNo, String orderItemId) {
        // 更新支付单号
        MainOrder updateOrder = new MainOrder();
        updateOrder.setOrderNo(orderNo);
        //已支付
        updateOrder.setStatus(MainOrderStatus.PAID_NO_STOCK);
        updateOrder.setPaidStatus(PayStatus.SUCCESS);
        updateOrder.setPaidAt(new Date());
        update2(updateOrder);

        OrderItem orderItem =orderItemMapper.selectByPrimaryKey(IdTypeHandler.encode(new Long(orderItemId)));
        Order order = new Order();
        order.setId(orderItem.getOrderId());
        order.setStatus(OrderStatus.PAIDNOSTOCK);
        orderMapper.updateByPrimaryKeySelective(order);
    }

    /**
     * 修改拼团支付状态
     * @param orderNo
     */
    public void updateOrderStatus2(String orderNo, String orderItemId) {
        // 更新支付单号
        MainOrder updateOrder = new MainOrder();
        updateOrder.setOrderNo(orderNo);
        //已支付
        updateOrder.setStatus(MainOrderStatus.PAID);
        updateOrder.setPaidStatus(PayStatus.SUCCESS);
        updateOrder.setPaidAt(new Date());
        update2(updateOrder);


        OrderItem orderItem =orderItemMapper.selectByPrimaryKey2(IdTypeHandler.encode(new Long(orderItemId)));
        if (null != orderItem) {
            Order order = new Order();
            order.setId(orderItem.getOrderId());
            order.setStatus(OrderStatus.PAID);
            orderMapper.updateByPrimaryKeySelective(order);
        }
    }


//    @Override

    /**
     * 根据订单号查询订单信息
     * @param batchBizNo
     * @return
     */
    public MainOrderVO loadByOrderNo(String batchBizNo) {
        MainOrder mainOrder = mainOrderMapper.selectByOrderNo(batchBizNo);
        List<Order> orders = orderMapper.selectByMainOrderId(mainOrder.getId());
        List<OrderVO> orderVOs = new ArrayList<OrderVO>();
        for (Order order : orders) {
            orderVOs.add(orderService.loadVO(order.getId()));
        }

        if (mainOrder != null && !org.apache.commons.collections4.CollectionUtils.isEmpty(orders)) {
            return new MainOrderVO(mainOrder, orderVOs);
        }
        return null;
    }

    /**
     * 更新开团信息表
     * @param promotionPgTranInfo
     */
    public void updatePgTanInfo (PromotionPgTranInfo promotionPgTranInfo) {
        recordPromotionPgTranInfoMapper.updatePieceStatus(promotionPgTranInfo);
    }

    /**
     * 更新订单
     * @param mainOrder
     * @return
     */
//    @Override
    public int update(MainOrder mainOrder) {
        checkMoney(mainOrder.getTotalFee());
        return mainOrderMapper.updateByPrimaryKeySelective(mainOrder);
    }

    /**
     * 更新订单
     * @param mainOrder
     * @return
     */
//    @Override
    public int update2(MainOrder mainOrder) {
        checkMoney(mainOrder.getTotalFee());
            return mainOrderMapper.updateByPrimaryKeySelectiveByNo(mainOrder);
    }

    /**
     * 合法性校验
     * @param price
     */
    private void checkMoney(BigDecimal price) {
        if (price == null) {
            return;
        }

        if (String.valueOf(price).length() > 9) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成订单的价格"
                    + price + "不合理，超出数据库");
        }
    }
}

