package com.xquark.service.cantuan;

import com.xquark.dal.model.User;
import com.xquark.dal.model.mypiece.ConfirmAndCheckVo;
import com.xquark.utils.base.BaseApiService;

/**
 * 参团接口
 *
 * @Author: tanggb
 * @Time: Create in 14:37 2018/9/7
 * @Modified by:
 */
public interface PromotionPaGroupService {

    /**
     * 判断是否活动过期,拼团时间是否过期
     *
     * @param confirmAndCheckVo
     * @param user
     * @return
     */
    BaseApiService isActivity(ConfirmAndCheckVo confirmAndCheckVo, User user);

    /**
     * 判断库存是否充足
     *
     * @param confirmAndCheckVo
     * @param user
     * @return
     */
    BaseApiService isStock(ConfirmAndCheckVo confirmAndCheckVo,User user);



}
