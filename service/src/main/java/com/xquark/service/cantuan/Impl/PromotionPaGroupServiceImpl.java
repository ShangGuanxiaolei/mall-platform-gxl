package com.xquark.service.cantuan.Impl;


import com.xquark.dal.mapper.RecordPromotionPgTranInfoMapper;
import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.StatusAndTimesVO;
import com.xquark.dal.model.User;
import com.xquark.dal.model.mypiece.ConfirmAndCheckVo;
import com.xquark.dal.model.mypiece.PromotionTempStockVo;
import com.xquark.dal.model.mypiece.StockCheckBean;
import com.xquark.service.cantuan.PromotionPaGroupService;
import com.xquark.service.mypiece.StockCheckService;
import com.xquark.service.order.OrderService;
import com.xquark.service.pintuan.CheckStatusAndTimesService;
import com.xquark.service.pintuan.CommitOrderService;
import com.xquark.utils.base.BaseApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @Author: tanggb
 * @Time: Create in 17:36 2018/9/7
 * @Description: 参团接口实现类
 * @Modified by:
 */
@Service
public class PromotionPaGroupServiceImpl extends BaseApiService
        implements PromotionPaGroupService {

  @Autowired
  OrderService orderService;
  @Autowired
  private StockCheckService stockCheckService;
  @Autowired
  private CheckStatusAndTimesService checkStatusAndTimesService;
  @Autowired
  private RecordPromotionPgTranInfoMapper recordPromotionPgTranInfoMapper;
  @Autowired
  private CommitOrderService commitOrderService;

  @Deprecated
  @Override
  public BaseApiService isActivity(ConfirmAndCheckVo confirmAndCheckVo, User user) {
    //1.查询活动是否过期
    StockCheckBean stockCheckBean = confirmAndCheckVo.getStockCheckBean();
    Timestamp timestamp = new Timestamp((new Date()).getTime());
    selectActivityInvalid(stockCheckBean, timestamp);
    //2.查询拼团是否过期
    final String pCode = stockCheckBean.getP_code();
    Timestamp starttime = recordPromotionPgTranInfoMapper.selectTime(pCode);
    //拼团结束时间，在原有基础上加一天
    Timestamp endTime = timestampAddDate(starttime);
    if (!timestamp.before(starttime) && timestamp.after(endTime)) {
      return setResultError("拼团已无效");
    }

    return setResultSuccess();
  }

  @Deprecated
  @Override
  public BaseApiService isStock(ConfirmAndCheckVo confirmAndCheckVo, User user) {
    StockCheckBean stockCheckBean = confirmAndCheckVo.getStockCheckBean();
    List<PromotionTempStockVo> promotionTempStockVoList = stockCheckService.checkStock(stockCheckBean);
    for (PromotionTempStockVo promotionTempStockVo :
            promotionTempStockVoList) {
      if (promotionTempStockVo == null) {
        return setResultError("未检查到该商品");
      }

      if (promotionTempStockVo.getP_usable_sku_num() < stockCheckBean.getP_sku_num()) {
        return setResultError("库存不足");
      }
    }

    Map<String, Object> stringObjectMap = commitOrderService.confirmPiece(confirmAndCheckVo.getPieceConfirmOrder(), user);

    return setResultSuccess(stringObjectMap);
  }


  /**
   * 查询活动是否过期
   *
   * @param stockCheckBean
   * @return
   */
  public BaseApiService selectActivityInvalid(StockCheckBean stockCheckBean, Timestamp timestamp) {
    PromotionBaseInfo promotionBaseInfo = new PromotionBaseInfo();
    promotionBaseInfo.setpCode(stockCheckBean.getP_code());
    promotionBaseInfo.setIsDeleted(1);
    StatusAndTimesVO info = checkStatusAndTimesService.check(promotionBaseInfo);
    if (info == null) {
      return setResultError("活动不存在");
    }
    String pStatus = "5";
    if (!pStatus.equals(info.getpStatus())) {
      return setResultError("活动未开始或已过期");
    }

    if (!timestamp.before(info.getEffectTo()) && timestamp.after(info.getEffectFrom())) {
      return setResultError("活动已失效");
    }
    return setResultSuccess();
  }


  private Timestamp timestampAddDate(Timestamp timestamp) {

    Date date = new Date(timestamp.getTime() + 86400000);
    Timestamp times = new Timestamp(date.getTime());

    return times;
  }

}
