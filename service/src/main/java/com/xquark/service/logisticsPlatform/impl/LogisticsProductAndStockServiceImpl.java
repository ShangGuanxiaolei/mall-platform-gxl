package com.xquark.service.logisticsPlatform.impl;

import com.xquark.dal.mapper.LogisticsProductAndStockMapper;
import com.xquark.dal.model.GoodInfo;
import com.xquark.dal.model.Skus;
import com.xquark.service.logisticsPlatform.LogisticsProductAndStockService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @auther liuwei
 * @date 2018/9/19 10:20
 */
@Service
public class LogisticsProductAndStockServiceImpl implements LogisticsProductAndStockService {

  @Autowired
  private LogisticsProductAndStockMapper productAndStockMapper;

  @Override
  public Integer getCountTotal(String supplierId) {
    return productAndStockMapper.countTotalPolyProduct(supplierId);
  }

  @Override
  public List<GoodInfo> getProductList(Integer PageIndex, Integer PageSize,String supplierId) {

    if(PageIndex >= 1 && PageSize > 0){
      int offset = (PageIndex-1)*PageSize;
      List<GoodInfo> goodlists = productAndStockMapper.limitSelect(supplierId,offset,PageSize);

      for (GoodInfo goods : goodlists) {
        goods.setSkus(getPloySkusList(goods.getPlatProductID()));
      }
      return goodlists;
    }else {
      throw new RuntimeException("参数不符合要求");
    }
  }

  @Override
  public void saveProductQuantity(Integer quantity, Long id) {
    productAndStockMapper.updateNewQuantity(quantity, id);
  }

  @Override
  public Integer saveProductSkuQuantity(Integer quantity,String skuId,String code) {
    return productAndStockMapper.updateNewSkuQuantity(quantity,skuId,code);
  }

  @Override
  public boolean checkProductId(Long id) {
    Integer b = productAndStockMapper.selectIdByProductId(id);
    if(b>0){
      return true;
    }else {
      return false;
    }
  }


  public List<Skus> getPloySkusList(String id){
    return productAndStockMapper.selectSku(id);
  }

  @Override
  public Integer getProductQuantity(String barCode,String supplierCode) {
    int quantity = productAndStockMapper.selectQuantityNewByBarCode(barCode,supplierCode);
    if(quantity > 0){
      return quantity;
    }else {
      throw new RuntimeException("商品不存在或没有库存");
    }
  }
}
