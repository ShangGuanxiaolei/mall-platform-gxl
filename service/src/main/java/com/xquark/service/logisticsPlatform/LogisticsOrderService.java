package com.xquark.service.logisticsPlatform;

import com.xquark.dal.vo.PolyapiGoodInfoVO;
import com.xquark.dal.vo.PolyapiOrderItemVO;

import java.util.Date;
import java.util.List;

/**
 * author: LiuWei Date: 18-9-19. Time: 上午11:22
 */
public interface LogisticsOrderService {
  /**
   * 查询所有订单数量
   */
  String getNumTotalAllOrder(String code);

  /**
   * 根据状态查询订单数量
   */
  String getNumTotalOrder(String status,String code,Date startTime,Date endTime);

  Long countOrderItem(Date StartTime,Date EndTime,String OrderStatus,String code);

  /**
   * 查询订单
   */
  List<PolyapiOrderItemVO> getOrderItem(Integer pageIndex, Integer pageSize,
      Date StartTime,Date EndTime,String OrderStatus,String code);

  /**
   * 查询所有订单
   */
  List<PolyapiOrderItemVO> getAllOrderItem(Integer pageIndex, Integer pageSize,
      Date StartTime,Date EndTime,String code);

  /**
   * 根据订单号查找商品
   */
  List<PolyapiGoodInfoVO> getGoodInfoByOrderNo(String orderNo,String code);

  /**
   * 根据订单状态查找订单
   */
  List<PolyapiOrderItemVO> getOrder(Integer pageIndex, Integer pageSize,Date StartTime,
      Date EndTime,String OrderStatus,String code);

  /**
   * 查找所有订单
   */
  List<PolyapiOrderItemVO> getAllOrder(Integer pageIndex, Integer pageSize,Date StartTime,
      Date EndTime,String code);

}
