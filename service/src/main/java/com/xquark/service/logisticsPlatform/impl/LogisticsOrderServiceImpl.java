package com.xquark.service.logisticsPlatform.impl;

import com.xquark.dal.mapper.LogisticsOrderMapper;
import com.xquark.dal.vo.PolyBusinessOrderStatus;
import com.xquark.dal.vo.PolyapiGoodInfoVO;
import com.xquark.dal.vo.PolyapiOrderItemVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.logisticsPlatform.LogisticsOrderService;
import com.xquark.service.systemRegion.SystemRegionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.xquark.utils.DateUtils.addSeconds;

/** author: liuwei Date: 18-9-19. Time: 上午11:26 */
@Service
@Transactional
public class LogisticsOrderServiceImpl implements LogisticsOrderService {

  private static final Logger LOGGER = LoggerFactory.getLogger(LogisticsOrderServiceImpl.class);

  @Autowired private LogisticsOrderMapper orderMapper;

  @Autowired private SystemRegionService systemRegionService; // 新的地区

  public String getNumTotalOrder(String status, String code, Date startTime, Date endTime) {
    return orderMapper.getNumTotalOrder(status, code, startTime, endTime);
  }

  @Override
  public Long countOrderItem(Date StartTime, Date EndTime, String OrderStatus, String code) {
      return orderMapper.countOrderItem(StartTime, EndTime, OrderStatus, code);
  }

  public String getNumTotalAllOrder(String code) {
    return orderMapper.getNumTotalAllOrder(code);
  }

  public List<PolyapiOrderItemVO> getOrderItem(
      Integer pageIndex,
      Integer pageSize,
      Date StartTime,
      Date EndTime,
      String OrderStatus,
      String code) {
    return orderMapper.getOrderItem(pageIndex, pageSize, StartTime, EndTime, OrderStatus, code);
  }

  public List<PolyapiOrderItemVO> getAllOrderItem(
      Integer pageIndex, Integer pageSize, Date StartTime, Date EndTime, String code) {
    return orderMapper.getAllOrderItem(pageIndex, pageSize, StartTime, EndTime, code);
  }

  public List<PolyapiGoodInfoVO> getGoodInfoByOrderNo(String orderNo, String code) {
    return orderMapper.getGoodInfoByOrderNo(orderNo, code);
  }

  public List<PolyapiOrderItemVO> getOrder(
      Integer pageIndex,
      Integer pageSize,
      Date StartTime,
      Date EndTime,
      String OrderStatus,
      String code) {
    int delayTime = 3600;
    // 拉取支付时间在前一个小时的订单
    Date paidStart = Optional.ofNullable(StartTime).map(addSeconds(-delayTime)).orElse(null);
    Date paidEnd = Optional.ofNullable(EndTime).map(addSeconds(-delayTime)).orElse(null);

    List<PolyapiOrderItemVO> list = orderMapper.getOrderItem(
            (pageIndex - 1) * pageSize, pageSize, paidStart, paidEnd, OrderStatus, code);
    for (PolyapiOrderItemVO p : list) {
      p.setGoodInfos(orderMapper.getGoodInfoByOrderNo(p.getOrderId(), code));
      p.setTradeStatus(
          PolyBusinessOrderStatus.getPolyStatusByOrderStatus(
                  com.xquark.dal.status.OrderStatus.valueOf(p.getTradeStatus()))
              .name());
      try {
        p.setArea(systemRegionService.load(p.getZip()).getName());
        p.setCity(systemRegionService.findParent(p.getZip()).getName());
        p.setProvince(
            systemRegionService
                .findParent(Long.toString(systemRegionService.findParent(p.getZip()).getId()))
                .getName());
      } catch (Exception e) {
        p.setArea("");
        p.setCity("");
        p.setProvince("");
        e.printStackTrace();
      }
    }
    return list;
  }

  public List<PolyapiOrderItemVO> getAllOrder(
      Integer pageIndex, Integer pageSize, Date StartTime, Date EndTime, String code) {
    pageIndex = Optional.ofNullable(pageIndex).map(p -> (p - 1) * p).orElse(null);
    List<PolyapiOrderItemVO> list;
    try {
      list = orderMapper.getAllOrderItem(pageIndex, pageSize, StartTime, EndTime, code);
      for (PolyapiOrderItemVO p : list) {
        p.setGoodInfos(orderMapper.getGoodInfoByOrderNo(p.getOrderId(), code));
        p.setTradeStatus(
            PolyBusinessOrderStatus.getPolyStatusByOrderStatus(
                    com.xquark.dal.status.OrderStatus.valueOf(p.getTradeStatus()))
                .name());
        try {
          p.setArea(systemRegionService.load(p.getZip()).getName());
          p.setCity(systemRegionService.findParent(p.getZip()).getName());
          p.setProvince(
              systemRegionService
                  .findParent(Long.toString(systemRegionService.findParent(p.getZip()).getId()))
                  .getName());
        } catch (Exception e) {
          p.setArea("");
          p.setCity("");
          p.setProvince("");
          e.printStackTrace();
        }
      }
    } catch (Exception e) {
      LOGGER.error("获取订单异常", e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "获取订单错误");
    }
    return list;
  }
}
