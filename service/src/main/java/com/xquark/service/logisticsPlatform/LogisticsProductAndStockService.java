package com.xquark.service.logisticsPlatform;

import com.xquark.dal.model.GoodInfo;
import java.util.List;

/**
 * @auther liuwei
 * @date 2018/9/19 22:01
 */
public interface LogisticsProductAndStockService {

  Integer getCountTotal(String supplierId);

  List<GoodInfo> getProductList(Integer PageIndex, Integer PageSize,String supplierId);

  void saveProductQuantity(Integer quantity, Long id);

  Integer saveProductSkuQuantity(Integer quantity, String skuId,String code);

  boolean checkProductId(Long id);

  Integer getProductQuantity(String barCode,String supplierCode);

}
