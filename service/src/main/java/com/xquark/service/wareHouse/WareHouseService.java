package com.xquark.service.wareHouse;

import com.xquark.dal.model.WareHouse;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA. User: shihao Date: 18-6-14. Time: 下午12:16 ${DESCRIPTION}
 */
public interface WareHouseService {

  /**
   * 通过id删除
   */
  Boolean delete(String id);


  /**
   * 添加
   */
  Boolean insert(WareHouse record);


  /**
   * 更新
   */
  Boolean update(WareHouse record);

  /**
   * 计算数据的数量
   */
  Long count(Map<String, Object> params);

  /**
   * 通过id查找
   */
  WareHouse getByPrimaryKey(String id);

  /**
   * 分页查询
   */
  List<WareHouse> list(Map<String, Object> params);

  /**
   * 批量删除
   */
  Boolean batchDelete(List<String> ids);

  WareHouse loadByName(String name);


}
