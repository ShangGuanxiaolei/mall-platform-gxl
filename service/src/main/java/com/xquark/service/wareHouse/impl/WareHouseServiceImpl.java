package com.xquark.service.wareHouse.impl;

import com.google.common.base.Preconditions;
import com.xquark.aop.anno.NotNull;
import com.xquark.dal.mapper.WareHouseMapper;
import com.xquark.dal.model.WareHouse;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.wareHouse.WareHouseService;
import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import org.springframework.validation.annotation.Validated;


/**
 * User: shihao Date: 18-6-14. Time: 下午12:19
 */
@Service("wareHouseServiceImpl")
@Validated
public class WareHouseServiceImpl extends BaseServiceImpl implements WareHouseService {

  @Autowired
  private WareHouseMapper wareHouseMapper;

  @Override  //删除仓库
  public Boolean delete(@NotBlank String id) {
    Preconditions.checkState(StringUtils.isNotBlank(id));
    return wareHouseMapper.delete(id) > 0;
  }

  @Override // 插入 仓库数据
  public Boolean insert(@NotNull WareHouse record) {
    Preconditions.checkNotNull(record);
    return wareHouseMapper.insert(record) > 0;
  }

  @Override
  public Boolean update(@NotNull WareHouse record) {
    return wareHouseMapper.updateByPrimaryKeySelective(record) > 0;
  }

  @Override  // 根据id  查询仓库
  public WareHouse getByPrimaryKey(@NotBlank String id) {
    WareHouse wareHouse = wareHouseMapper.getByPrimaryKey(id);
    return wareHouse;
  }

  @Override  //分页查询
  public List<WareHouse> list(Map<String, Object> params) {
    return wareHouseMapper.list(params);
  }

  @Override
  public Boolean batchDelete(@NotEmpty List<String> ids) {
    for (String id : ids) {
      wareHouseMapper.delete(id);
    }
    return true;
  }

  @Override
  public WareHouse loadByName(String name) {
    return wareHouseMapper.getByName(name);
  }

  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long count(Map<String, Object> params) {
    return wareHouseMapper.count(params);
  }





}
