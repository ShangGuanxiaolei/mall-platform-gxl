package com.xquark.service.diamond;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionDiamond;
import com.xquark.dal.vo.PromotionDiamondProductVO;
import com.xquark.dal.vo.PromotionDiamondVO;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * Created by chh on 16-10-26. 钻石专享设置表Service
 */
public interface PromotionDiamondService {

  /**
   * 根据id查询
   */
  PromotionDiamondVO selectByPrimaryKey(String id);

  /**
   * 新增
   */
  int insert(PromotionDiamond promotionDiamond);

  /**
   * 修改
   */
  int modifyPromotionDiamond(PromotionDiamond promotionDiamond);

  /**
   * 手动结束钻石专享活动
   */
  int close(String id);

  /**
   * 列表查询钻石专享活动
   */
  List<Promotion> listPromotion(Pageable pageable, String keyword);

  /**
   * 对分页钻石专享活动列表接口取总数
   */
  Long selectPromotionCnt(String keyword);

  /**
   * 列表查询钻石专享活动关联商品
   */
  List<PromotionDiamondProductVO> listPromotionProduct(Pageable page, String id);

  /**
   * 对分页钻石专享活动关联商品列表接口取总数
   */
  Long selectPromotionProductCnt(String id);

  /**
   * 将id对应的xquark_promotion_Diamond记录archive设置为true
   */
  int delete(@Param("id") String id);

  /**
   * 查询某个商品在钻石专享活动中的个数
   */
  Long selectPromotionByProductId(String promotionId, String id, String productId);

  /**
   * 找到某个商品对应的限时抢购信息
   */
  PromotionDiamondVO selectByProductId(String productId, Date date);


  /**
   * 查找某个商品在其他有效钻石专享活动中的信息，用于判断这个商品有没有存在同一时段中不同活动中
   */
  List<PromotionDiamondVO> listExistPromotionProduct(String promotionId, String productId);

  /**
   * app端列表查询钻石专享分类数据
   */
  List<PromotionDiamondProductVO> listDiamondApp(String shopId, String categoryId, int limit);


}
