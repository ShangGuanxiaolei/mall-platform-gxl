package com.xquark.service.diamond.impl;

import com.xquark.dal.mapper.PromotionDiamondMapper;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionDiamond;
import com.xquark.dal.vo.PromotionDiamondProductVO;
import com.xquark.dal.vo.PromotionDiamondVO;
import com.xquark.service.diamond.PromotionDiamondService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by chh on 16-10-12.
 */
@Service("promotionDiamondServiceImpl")
public class PromotionDiamondServiceImpl implements PromotionDiamondService {

  @Autowired
  private PromotionDiamondMapper promotionDiamondMapper;

  /**
   * 根据id查询
   */
  @Override
  public PromotionDiamondVO selectByPrimaryKey(String id) {
    return promotionDiamondMapper.selectByPrimaryKey(id);
  }

  /**
   * 新增
   */
  @Override
  public int insert(PromotionDiamond promotionDiamond) {
    return promotionDiamondMapper.insert(promotionDiamond);
  }

  /**
   * 修改
   */
  @Override
  public int modifyPromotionDiamond(PromotionDiamond promotionDiamond) {
    return promotionDiamondMapper.modifyPromotionDiamond(promotionDiamond);
  }

  /**
   * 手动结束钻石专享活动
   */
  @Override
  public int close(String id) {
    return promotionDiamondMapper.close(id);
  }

  /**
   * 列表查询钻石专享活动
   */
  @Override
  public List<Promotion> listPromotion(Pageable pageable, String keyword) {
    return promotionDiamondMapper.listPromotion(pageable, keyword);
  }

  /**
   * 对分页钻石专享活动列表接口取总数
   */
  @Override
  public Long selectPromotionCnt(String keyword) {
    return promotionDiamondMapper.selectPromotionCnt(keyword);
  }

  /**
   * 列表查询钻石专享活动关联商品
   */
  @Override
  public List<PromotionDiamondProductVO> listPromotionProduct(Pageable page, String id) {
    return promotionDiamondMapper.listPromotionProduct(page, id);
  }

  /**
   * 对分页钻石专享活动关联商品列表接口取总数
   */
  @Override
  public Long selectPromotionProductCnt(String id) {
    return promotionDiamondMapper.selectPromotionProductCnt(id);
  }

  /**
   * 将id对应的xquark_promotion_Diamond记录archive设置为true
   */
  @Override
  public int delete(String id) {
    return promotionDiamondMapper.delete(id);
  }

  /**
   * 查询某个商品在钻石专享活动中的个数
   */
  @Override
  public Long selectPromotionByProductId(String promotionId, String id, String productId) {
    return promotionDiamondMapper.selectPromotionByProductId(promotionId, id, productId);
  }

  /**
   * 找到某个商品对应的钻石专享信息
   */
  @Override
  public PromotionDiamondVO selectByProductId(String productId, Date date) {
    return promotionDiamondMapper.selectByProductId(productId, date);
  }

  /**
   * 查找某个商品在其他有效钻石专享活动中的信息，用于判断这个商品有没有存在同一时段中不同活动中
   */
  @Override
  public List<PromotionDiamondVO> listExistPromotionProduct(String promotionId, String productId) {
    return promotionDiamondMapper.listExistPromotionProduct(promotionId, productId);
  }

  /**
   * app端列表查询钻石专享分类数据
   */
  @Override
  public List<PromotionDiamondProductVO> listDiamondApp(String shopId, String categoryId,
      int limit) {
    return promotionDiamondMapper.listDiamondApp(shopId, categoryId, limit);
  }

}
