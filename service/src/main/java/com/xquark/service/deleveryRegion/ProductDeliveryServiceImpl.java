package com.xquark.service.deleveryRegion;

import com.xquark.dal.mapper.XquarkProductMapper;
import com.xquark.dal.vo.XquarkProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2018/12/24
 * Time: 13:55
 * Description: 后台更新区域service
 */
@Service
public class ProductDeliveryServiceImpl {

    @Autowired
    private XquarkProductMapper xquarkProductMapper;

    public Integer  updateProductDeliveryByid(Integer id , String provinceid){
        Integer integer = xquarkProductMapper.updateDeliveryRegionByid(id, provinceid);
        return integer;
    };

    public XquarkProduct  queryDeliveryByid(String code){
        XquarkProduct xquarkProduct = xquarkProductMapper.queryDeliveryByid(code);
        return xquarkProduct;
    }
    public List<Map<String,String>> queryRegionById(){
        List<Map<String, String>> maps =xquarkProductMapper.queryRegionById();
        return maps;
    }
    public Integer queryParentId(Integer regionid){
        Integer integer = xquarkProductMapper.queryParentId(regionid);
        return integer;
    }
    public List<String> querySfByCoid(String code){
        List<String> source = xquarkProductMapper.querySfByCoid(code);
        return source;
    }

    public String getParent (String provincename){
        String parent = xquarkProductMapper.getParent(provincename);
        return parent;
    }
}