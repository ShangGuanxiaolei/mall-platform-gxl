package com.xquark.service.product.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.SFApiConstants;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.ProductReviewStatus;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.*;
import com.xquark.helper.Transformer;
import com.xquark.service.brand.BrandService;
import com.xquark.service.category.CategoryService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.http.pojo.sf.base.SFTokenParam;
import com.xquark.service.http.pojo.sf.base.SFTokenResponse;
import com.xquark.service.http.pojo.sf.product.*;
import com.xquark.service.product.*;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.UserService;
import com.xquark.service.wareHouse.WareHouseService;
import com.xquark.utils.DateUtils;
import com.xquark.utils.Email.HvSendEmailUtil;
import com.xquark.utils.Email.vo.HvEmail;
import com.xquark.utils.HttpClientUtil;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.parboiled.common.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;

import static com.xquark.service.product.SFTaskGlobalContext.*;

/**
 * created by
 *
 * @author wangxinhua at 18-6-1 下午2:29 顺丰商品同步service
 */
@Service
public class SFProductServiceImpl implements SFProductService, InitializingBean {

  private final static Logger LOGGER = LoggerFactory.getLogger(SFProductServiceImpl.class);

  @Value("${sf.api.host.url}")
  private String host;

  private ProductService productService;

  private UserService userService;

  private ShopService shopService;

  private CategoryService categoryService;

  private BrandService brandService;

  private SfProductLibraryMapper sfProductLibraryMapper;

  private ProductMapper productMapper;

  private SkuMapper skuMapper;

  private SupplierMapper supplierMapper;

  private SfAppConfigMapper sfConfigMapper;

  private WareHouseService wareHouseService;

  @Autowired
  private SFProductSign sfProductSign;

  @Autowired
  private ShippingMapper shippingMapper;
  @Autowired
  private PromotionConfigMapper promotionConfigMapper;

  Map<String,String> sfCategoryMap;

  @Autowired
  private OrderMapper orderMapper;

  @Value("${sf.api.token.profile}")
  private String profile;
  @Value("${profiles.active}")
  private String profilesActiveValue;

  private final static TypeReference<SFTokenResponse> TOKEN_RESPONSE_TYPE_REFERENCE
      = new TypeReference<SFTokenResponse>() {
  };

  private final static Function<SFProductDetail, ThirdDetail> DETAIL_TRANSFORM_FUNC =
      new Function<SFProductDetail, ThirdDetail>() {
        @Override
        public ThirdDetail apply(SFProductDetail sfProductDetail) {
          return new ThirdDetail(String.valueOf(sfProductDetail.getProductId()),
              ProductSource.SF, sfProductDetail.getContent());
        }
      };

  private final static String SF_USER_NAME = "顺丰优选";

  private final static String SF_PICTURE_HOST = "http://p02.sfimg.cn";

  private final static String RD_PRODUCTS_KEY = "SF_PRODUCTS";

  /**
   * 提前60分钟刷新
   */
  private final static int RENEW_INTERVAL_MINUTES = 60;

  /**
   * 顺丰接口分页开始数据
   */
  private final static int PAGE_START = 1;

  /**
   * 顺丰接口每页数量
   */
  private final static int PAGE_SIZE = 120;

  /**
   * 拉取商品线程数
   */
  private final static int PULL_WORKERS = 5;

  private final static Comparator<SFPicture> PICTURE_COMPARATOR = new Comparator<SFPicture>() {
    @Override
    public int compare(SFPicture o1, SFPicture o2) {
      Boolean v1 = o1.getIsDefault();
      Boolean v2 = o2.getIsDefault();
      if (v1) {
        return 1;
      }
      if (v2) {
        return -1;
      }
      return 0;
    }
  };

  /**
   * 顺丰拉取商品需要更新的字段
   */
  private final static List<String> UPDATE_PRODUCT_FIELDS = ImmutableList.of(
      "name", "description",
      "encode", "sourceProductId", "sfairline", "sfshipping", "merchantNumber"
  );

  /**
   * sku更新字段
   */
  private final static List<String> UPDATE_SKU_FIELDS = ImmutableList.of(
      "amount",
      "secureAmount",
      "partnerProductPrice"
  );

  @Autowired
  public void setProductService(ProductService productService) {
    this.productService = productService;
  }

  @Autowired
  public void setShopService(ShopService shopService) {
    this.shopService = shopService;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setCategoryService(CategoryService categoryService) {
    this.categoryService = categoryService;
  }

  @Autowired
  public void setSfConfigMapper(SfAppConfigMapper sfConfigMapper) {
    this.sfConfigMapper = sfConfigMapper;
  }

  @Autowired
  public void setBrandService(BrandService brandService) {
    this.brandService = brandService;
  }

  @Autowired
  public void setProductMapper(ProductMapper productMapper) {
    this.productMapper = productMapper;
  }

  @Autowired
  public void setSfProductLibraryMapper(
      SfProductLibraryMapper sfProductLibraryMapper) {
    this.sfProductLibraryMapper = sfProductLibraryMapper;
  }


  @Autowired
  public void setSkuMapper(SkuMapper skuMapper) {
    this.skuMapper = skuMapper;
  }

  @Autowired
  public void setSupplierMapper(SupplierMapper supplierMapper) {
    this.supplierMapper = supplierMapper;
  }

  @Autowired
  public void setWareHouseService(WareHouseService wareHouseService) {
    this.wareHouseService = wareHouseService;
  }

  /**
   * 同步顺丰商品列表
   *
   * @param sfProducts 顺丰拉取的商品列表
   * @return 同步是否成功
   */
  @Override
  public boolean syncPdList(List<SFProduct> sfProducts, Set<String> baseEncodes,
      User user, Shop shop) {

    RedisUtils<Integer> contains = RedisUtilFactory.getInstance(Integer.class);
    contains.set("contains", 0);
    for (SFProduct sfProduct : sfProducts) {
      String encode = sfProduct.getBaseInfo().getNumber();
      // 只有商品在优选品中才同步
      if (baseEncodes.contains(encode) || baseEncodes.contains("11111")) {
        contains.incrementOne("contains");
        LOGGER.info("=== 同步顺丰商品 {} ===", encode);
        syncPd(sfProduct, user, shop);
        LOGGER.info("=== 顺丰商品 {} 同步完成 ===");
      }
    }
    return true;
  }

  /**
   * 同步单个商品
   *
   * @param sfProduct 顺丰单商品
   * @param user 用户信息
   * @param shop 店铺信息
   * @return 同步是否成功
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public boolean syncPd(SFProduct sfProduct, User user, Shop shop) {
    SFBaseInfo baseInfo = sfProduct.getBaseInfo();
    String encode = baseInfo.getNumber();
    //数据库商品信息
    Product product = productService.selectThirdProductByEncode(encode, ProductSource.SF);
    //顺丰查询出来的商品
    Product syncProduct = collectPd(sfProduct, user, shop);
    if (product == null) {
      product = syncProduct;
    } else {
      // 更新商品属性
      copyProperties2(syncProduct, product, UPDATE_PRODUCT_FIELDS);
    }

    List<ProductImage> images = collectImages(sfProduct);

    if (CollectionUtils.isNotEmpty(images) && images.size() > 0) {
      product.setImg(images.get(0).getImg());
    }

    int ret;

    String productId = product.getId();
    List<Sku> singleSku = Collections.singletonList(collectSku(sfProduct));
    if (StringUtils.isBlank(productId)) {
      // transactional
      ret = productService.create(product, singleSku,
          null, null, images, null);
      productId = product.getId();
    } else {
      List<Sku> dbSingleSku = skuMapper.selectByProductId(productId);
      // 顺丰拉取的商品应该只有唯一sku
      if (CollectionUtils.isEmpty(dbSingleSku) || dbSingleSku.size() != 1) {
        throw new BizException(GlobalErrorCode.UNKNOWN, "顺丰商品sku错误");
      }
      // 更新sku属性
      copyProperties2(singleSku.get(0), dbSingleSku.get(0), UPDATE_SKU_FIELDS);
      ret = productService.update(product, dbSingleSku, null, images, null);
    }

//    SFShipping sfShipping = sfProduct.getSfshipping();
//    Shipping shipping = new Shipping(sfShipping.getId(), sfShipping.getProductId(),
//        sfShipping.getSfshipping(), sfShipping.getSfairline());
//    //更新运输条件
//    boolean isExist = sfShippingService.selectShippingByProductId(sfShipping.getProductId());
//    if (!isExist) {
//      //插入运输条件
//      sfShippingService.addSFShipping(shipping);
//
//    }

    // 保存商品分类
    saveCategories(productId, sfProduct, user, shop);

    // 保存品牌信息
    saveBrand(productId, sfProduct);
    return ret > 0;
  }

  @Override
  public Set<String> loadTargetProductEncodes() {
    RedisUtils<Set> redisUtils = RedisUtilFactory.getInstance(Set.class);
    // load from cache
    @SuppressWarnings("unchecked")
    Set<String> encodes = redisUtils.get(RD_PRODUCTS_KEY);
    if (CollectionUtils.isEmpty(encodes)) {
      // load from db
      List<String> dbEncodes = sfProductLibraryMapper.loadAllEncodes();
      if (CollectionUtils.isEmpty(dbEncodes)) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "顺丰优选商品未初始化");
      }
      encodes = new HashSet<>(dbEncodes);
      redisUtils.set(RD_PRODUCTS_KEY, encodes);
    }
    return encodes;
  }

  /**
   * 抽取商品
   */
  private Product collectPd(SFProduct sfProduct, User user, Shop shop) {
    Product product = new Product();
    SFBaseInfo baseInfo = sfProduct.getBaseInfo();
    //升级2.0,原价被供货价替换
    BigDecimal costPrice = BigDecimal.valueOf(baseInfo.getSupplyPrice())
            .divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_EVEN);
    BigDecimal price = BigDecimal.valueOf(baseInfo.getPrice())
        .divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_EVEN);

    product.setUserId(user.getId());
    product.setShopId(shop.getId());
    product.setSource(ProductSource.SF);
    product.setSourceProductId(String.valueOf(baseInfo.getProductId()));
    product.setName(baseInfo.getName());
    product.setEncode(baseInfo.getNumber());
    product.setU8Encode(baseInfo.getBarCode());
    product.setStatus(ProductStatus.DRAFT);
    product.setType(ProductType.NORMAL);
    product.setModel("");
    // 初始化库存为0, 等刷新库存
    product.setAmount(100000);
    product.setOriginalPrice(costPrice);
    product.setMarketPrice(price);
    product.setSales(0);
    product.setRecommend(false);
    product.setDescription(baseInfo.getSpecification());
    product.setMinYundouPrice(BigDecimal.ONE);
    product.setOneyuanPurchase(false);
    product.setLogisticsType(LogisticsType.TEMPLATE);
    product.setAmount(Integer.MAX_VALUE);
    product.setSfairline(Integer.parseInt(sfProduct.getSfshipping().getSfairline()));
    product.setSfshipping(Integer.parseInt(sfProduct.getSfshipping().getSfshipping()));
    product.setMerchantNumber(baseInfo.getMerchantNumber() + "");
    // FIXME hard code
    product.setTemplateValue(IdTypeHandler.encode(4500));

    WareHouse wareHouse = wareHouseService.loadByName("顺丰仓");
    if (wareHouse != null) {
      product.setWareHouseId(wareHouse.getId());
    }
    product.setDetailH5("");

    product.setWidth(Optional.fromNullable(baseInfo.getWeight()).or(1D));
    product.setHeight(Optional.fromNullable(baseInfo.getHeight()).or(1D));
    product.setLength(Optional.fromNullable(baseInfo.getLength()).or(1D));
    product.setWeight(Optional.fromNullable(baseInfo.getWeight()).or(1D).intValue());

    Supplier supplier = supplierMapper.selectByCode("SF");
    product.setSupplierId(supplier == null ? null : supplier.getId());

    product.setReviewStatus(ProductReviewStatus.WAIT_CHECK);
    product.setKind("");
    product.setPoint(new BigDecimal(0));
    product.setNumInPackage(1);
    product.setDeductionDPoint(BigDecimal.ZERO);

    product.setSupportRefund(false);
    return product;
  }

  /**
   * 保存顺丰商品sku信息
   * @param sfProduct
   * @return
   */
  private Sku collectSku(SFProduct sfProduct) {
    Sku sku = new Sku();
    SFBaseInfo baseInfo = sfProduct.getBaseInfo();
    BigDecimal price = BigDecimal.valueOf(baseInfo.getPrice())
        .divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_EVEN);
    BigDecimal supplyPrice = BigDecimal.valueOf(baseInfo.getSupplyPrice())
            .divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_EVEN);
    sku.setMarketPrice(supplyPrice);
    sku.setOriginalPrice(supplyPrice);
    sku.setPrice(price);
    sku.setAmount(100000);
    sku.setSpec("无");
    sku.setSpec1("无");
    sku.setSecureAmount(1);
    sku.setWeight(Optional.fromNullable(baseInfo.getWeight()).or(1D).intValue());
    sku.setSkuCodeResources(SkuCodeResourcesType.getSkuCodeResourceBySupplierCode("SF").orElseThrow(RuntimeException::new).getName());
    sku.setDeductionDPoint(BigDecimal.ZERO);
    sku.setBarCode(baseInfo.getBarCode());
    sku.setSkuCode(baseInfo.getNumber());
    sku.setPartnerProductPrice(supplyPrice);
    return sku;
  }

  private List<ProductImage> collectImages(SFProduct sfProduct) {
    List<SFPicture> pictures = sfProduct.getPicture();
    if (CollectionUtils.isEmpty(pictures)) {
      return Collections.emptyList();
    }
    // 把主图放到第一张
    // FIXME 拉取顺丰产品图片顺序不对，不使用主图排序规则
//    Collections.sort(pictures, PICTURE_COMPARATOR);
    SFBaseInfo baseInfo = sfProduct.getBaseInfo();
    String number = baseInfo.getNumber();
    List<ProductImage> images = new ArrayList<>();
    int idx = 0;
    for (SFPicture picture : pictures) {
      String filePath = getPicturePath(SF_PICTURE_HOST, picture.getYear(), number,
          picture.getFilename());
      ProductImage pImg = new ProductImage();
      pImg.setImgOrder(idx++);
      pImg.setImg(filePath);
      //快店发布、编辑商品，图片默认为组图
      pImg.setType(1);
      images.add(pImg);
    }
    return images;
  }

  private String getPicturePath(String host, Integer year, String number, String file) {
    return String.format("%s/%d/%s/middle_%s", host, year, number, file);
  }

  private User initSFUser() {
    User baseUser = new User();
    baseUser.setPassword("");
    baseUser.setName(SF_USER_NAME);
    baseUser.setLoginname(SF_USER_NAME);
    baseUser.setAvatar("");
    return baseUser;
  }

  /**
   * 获取存储的顺丰用户数据 如果需要将顺丰的店铺区分则调用此方法
   *
   * @return user - {@link com.xquark.dal.model.User} shop - {@link com.xquark.dal.model.Shop}
   */
  @Deprecated
  private Map<String, Object> loadUserInfoForSF() {
    Map<String, Object> ret = new HashMap<>();
    User sfUser = userService.loadByLoginname(SF_USER_NAME);
    if (sfUser == null) {
      sfUser = initSFUser();
      userService.insert(sfUser);
    }
    Shop shop = userService.createShopIfNotExists(sfUser);
    ret.put("user", sfUser);
    ret.put("shop", shop);
    return ret;
  }

  /**
   * 通过指定属性拷贝对象
   *
   * @param source 源对象
   * @param target 目标对象
   * @param updateFields 指定需要更新的属性值
   */
  private void copyProperties2(Object source, Object target,
      Iterable<String> updateFields) {
    BeanWrapper srcWrap = PropertyAccessorFactory.forBeanPropertyAccess(source);
    BeanWrapper trgWrap = PropertyAccessorFactory.forBeanPropertyAccess(target);
    for (String prop : updateFields) {
      trgWrap.setPropertyValue(prop, srcWrap.getPropertyValue(prop));
    }
  }

  /**
   * 保存商品分类
   */
  @Transactional
  void saveCategories(String productId, SFProduct sfProduct, User user, Shop shop) {
    SFCategory categoryOne = sfProduct.getCategoryOne();
    SFCategory categoryTwo = sfProduct.getCategoryTwo();
    SFCategory categoryThree = sfProduct.getCategoryThree();

    if (categoryOne == null || categoryTwo == null
        || categoryThree == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "顺丰商品分类不存在");
    }

    Long code = categoryOne.getCategoryId();
    String innerName = getHDSCategoryName(String.valueOf(code));
    Category child = categoryService.loadByName(innerName);
    if(child != null){
      // 关联商品分类关系
      categoryService.addProductCategory(productId, child.getId());
    }
  }

  public String getHDSCategoryName(String name){
    return sfCategoryMap.get(name);
  }

  @Override
  public void pullProducts(Integer limit) {
    ThreadPoolExecutor ec =
        new ThreadPoolExecutor(PULL_WORKERS, PULL_WORKERS,
            1, TimeUnit.SECONDS,
            new LinkedBlockingDeque<Runnable>(128));
    ec.setRejectedExecutionHandler(new CallerRunsPolicy());

    SfAppConfig appConfig = loadConfig();
    Set<String> baseEncodes = this.loadTargetProductEncodes();

    LOGGER.info("======= 开始同步顺丰商品 ========");
    long begin = System.currentTimeMillis();
    try {
      syncProductWithPage(ec, appConfig, PAGE_START, PAGE_SIZE, baseEncodes, limit);
    } finally {
      if (!ec.isShutdown()) {
        ec.shutdown();
      }
      try {
        ec.awaitTermination(10, TimeUnit.MINUTES);
        long after = System.currentTimeMillis();
        RedisUtils<Integer> contains = RedisUtilFactory.getInstance(Integer.class);
        LOGGER.info("======= 顺丰商品同步完成, 耗时: {} ms, 更新 {} 条 =======",
            after - begin, contains.get("contains"));
      } catch (InterruptedException e) {
        LOGGER.error("======= 商品同步超时, 已结束 =======", e);
      }
    }
    // 同步商品详情
    pullProductDetails(null);
  }

  @Override
  public void pullProductByEncode(String encode) {
    SfAppConfig appConfig = this.loadConfig();
    String json = "{\"page\":1,\"pageSize\":1,\"number\":"+ encode +"}";
    //请求顺丰的url
    String pullProductUrl = sfProductSign.basePullSFProductUrl(appConfig,json);

    Map<String, Object> userInfoMap = loadUserInfoForAdmin();
    User user = (User) userInfoMap.get("user");
    Shop shop = (Shop) userInfoMap.get("shop");

    SFProductWrapper productWrapper = requestProducts1(pullProductUrl,json,encode);
    List<SFProduct> products = productWrapper.getProductList();
    LOGGER.info("");
    if (products.size() > 0) {
      boolean b = this.syncPdList(products, ImmutableSet.of("11111"), user, shop);
    }
  }

  @Override
  public void pullProductDetails(String encode) {
    SfAppConfig appConfig = this.loadConfig();
    String pullDetailUrl = String.format("%s?app_key=%s&access_token=%s&timestamp=%s&format=json&version=1.0",
        host + SFApiConstants.PULL_PD_DETAILS, appConfig.getClientId(),
        appConfig.getAccessToken(),
        String.valueOf(System.currentTimeMillis()));

    List<String> existsEncodes;
    if (StringUtils.isNotBlank(encode)) {
      existsEncodes = ImmutableList.of(encode);
    } else {
      existsEncodes = productMapper.listSFProductEncode();
    }
    SFProductDetailWrapper detailWrapper = SFTaskHelper
        .requestPdDetail(pullDetailUrl, existsEncodes);
    List<SFProductDetail> details = detailWrapper.getProductInfoDetails();
    if (CollectionUtils.isNotEmpty(details)) {
      // 更新本地商品详情
      LOGGER.info("======= 开始更新商品详情, 共 {} 条 =======", details.size());
      List<ThirdDetail> detailToUpdate = Transformer.fromIterable(details, DETAIL_TRANSFORM_FUNC);
      CollectionUtils.filter(detailToUpdate, new Predicate<ThirdDetail>() {
        @Override
        public boolean evaluate(ThirdDetail object) {
          return StringUtils.isNotBlank(object.getDetailH5());
        }
      });
      // 批量更新
      for (ThirdDetail detail : detailToUpdate) {
        productMapper.updateThirdDetail(detail);
      }
      LOGGER.info("======= 商品详情更新结束, 成功更新 {} 条 =======");
    }
  }

  @Transactional(rollbackFor = Exception.class)
  void saveBrand(String productId, SFProduct sfProduct) {
    Brand sfBrand = brandService.loadSFBrand();
    if (sfBrand == null) {
      LOGGER.warn("顺丰分类不存在");
      return;
    }
    // 统一使用顺丰品牌
    brandService.addProductBrand(productId, sfBrand.getId());
//    SFBrand sfBrand = sfProduct.getBrand();
//    if (sfBrand == null) {
//      return;
//    }
//    String sourceId = String.valueOf(sfBrand.getBrandId());
//
//    Brand brand = brandService.loadByThird(sourceId, ProductSource.SF);
//    if (brand == null) {
//      brand = new Brand();
//    }
//    brand.setName(sfBrand.getBrandName());
//    brand.setDescription(sfBrand.getBrandDesc());
//    brand.setLogo(sfBrand.getBrandLogo());
//    brand.setSource(ProductSource.SF);
//    brand.setSourceId(sourceId);
//    boolean isShow = sfBrand.getIsShow() == null || sfBrand.getIsShow() > 0;
//    brand.setIsShow(isShow);
//    brand.setSortOrder(0);
//    brand.setSiteHost(StringUtils.defaultString(sfBrand.getSiteUrl()));
//
//    if (StringUtils.isBlank(brand.getId())) {
//      brandService.save(brand);
//    } else {
//      brandService.update(brand);
//    }
//
//    brandService.addProductBrand(productId, brand.getId());
  }

  /**
   * 获取顺丰配置, 该方法保证获取到的token为最新未过期的值
   *
   * @return 顺丰配置
   */
  @Override
  public SfAppConfig loadConfig() {
    SfAppConfig appConfig = sfConfigMapper.selectByProfile(profile);
    if (appConfig == null || needRenewToken(new Date(), appConfig.getAccessTokenExpiredAt())) {
      // 手动更新token
      renewToken();
      appConfig = sfConfigMapper.selectByProfile(profile);
    }
    return appConfig;
  }

  public static void main(String[] args) throws ParseException {
    final SFProductServiceImpl sfProductService = new SFProductServiceImpl();
    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    final Date parse = simpleDateFormat.parse("2019-01-03 19:03:27");
    final boolean b = sfProductService.needRenewToken(new Date(), parse);
    System.out.println(b);
  }
  @Override
  public void renewToken() {
    LOGGER.info("========== 同步顺丰token ===========");
    SfAppConfig config = sfConfigMapper.selectByProfile(profile);
    if (config == null) {
      LOGGER.error("顺丰token信息未初始化");
      return;
    }
    Date expireAt = config.getAccessTokenExpiredAt();
    Date now = new Date();
    if (expireAt == null || needRenewToken(now, expireAt)) {
      SFTokenParam tokenParam = new SFTokenParam(config.getGrantType(),
          config.getClientId(), config.getClientSecret());

      HttpInvokeResult res = PoolingHttpClients.post(host + SFApiConstants.RENEW_TOKEN,
          tokenParam);
      LOGGER.info("重新获取顺丰token,返回结果: {}",res);
      if(res.getStatusCode() != 200 ){
        return;
      }
      SFTokenResponse resObj;
      try {
        resObj = JSON.parseObject(res.getContent(), TOKEN_RESPONSE_TYPE_REFERENCE);
      } catch (Exception e) {
        LOGGER.error("顺丰token接口解析失败");
        return;
      }
      config.setAccessToken(resObj.getAccess_token());
      config.setAccessTokenExpiredAt(DateUtils.addSeconds(now, resObj.getExpires_in()));
      sfConfigMapper.updateByPrimaryKeySelective(config);
    }
  }

  /**
   * 分页拉取商品
   *
   * @param ec executor service
   * @param appConfig 顺丰token配置
   * @param page 当前第几页
   * @param pageSize 每页拉取数量
   * @param baseEncodes 顺丰优选需要的商品列表
   */
  @SuppressWarnings("SameParameterValue")
  private void syncProductWithPage(ExecutorService ec, SfAppConfig appConfig,
      int page, int pageSize,
      Set<String> baseEncodes, Integer limit) {

    Map<String, Object> userInfoMap = loadUserInfoForAdmin();
    User user = (User) userInfoMap.get("user");
    Shop shop = (Shop) userInfoMap.get("shop");

    APP_CONFIG = appConfig;
    SIZE = pageSize;
    SFTaskGlobalContext.SF_PRODUCT_SERVICE = this;
    HOST = host;
    SFTaskGlobalContext.USER = user;
    SFTaskGlobalContext.SHOP = shop;
    SFTaskGlobalContext.BASE_ENCODES = baseEncodes;

    Integer total = limit;
    if (total == null) {
      String json = "{\"page\":1,\"pageSize\":1}";
      String pullProductUrl = sfProductSign.basePullSFProductUrl(APP_CONFIG,json);

      SFProductWrapper productWrapper = SFTaskHelper.requestProducts1(pullProductUrl, json);
      total = productWrapper.getTotalCount();
    }
    // 先请求总数

    LOGGER.info("======= 顺丰商品待同步共 {} 条 =======", total);

    while ((page - 1) * SIZE <= total && !ec.isShutdown()) {
      ec.execute(new PullProductTask(page));
      page += 1;
    }
  }

  /**
   * 直接获取后台用户的用户和店铺
   *
   * @return user - {@link com.xquark.dal.model.User} shop - {@link com.xquark.dal.model.Shop}
   */
  // FIXME 不该暴露该方法
  @Override
  public Map<String, Object> loadUserInfoForAdmin() {
    Map<String, Object> ret = new HashMap<String, Object>();
    Shop shop = shopService.loadRootShop();
    String userId = shop.getOwnerId();
    User sfUser = userService.load(userId);
    ret.put("user", sfUser);
    ret.put("shop", shop);
    return ret;
  }

  /**
   * 是否需要更新token
   */
  @Override
  public boolean needRenewToken(Date now, Date expiredAt) {
    if (expiredAt == null) {
      return true;
    }
    return expiredAt.getTime() <= now.getTime() ;
  }

    /**
     * 单个校验顺丰商品是否下架
     * @param encode
     * @return 返回true下架
     */
    @Override
    public boolean checkProductIdShowStatus(String encode) {
        if(StringUtils.isBlank(encode)){
            return false;
        }
        SFProductWrapper productWrapper = this.getSFProductWrapperBySfUrl(encode);
        LOGGER.info("自动下架查询顺丰商品接口encode："+encode+"对应返回的数据ProductList{}"+productWrapper.getProductList());

        //解析数据
        if(productWrapper == null){
            return false;
        }

        List<SFProduct> productList = productWrapper.getProductList();
        if(productList == null || productList.isEmpty()){
            return false;
        }

        SFProduct sfProduct = productList.get(0);
        if(sfProduct == null){
            return false;
        }

        SFBaseInfo baseInfo = sfProduct.getBaseInfo();
        if (baseInfo == null){
            return false;
        }

//        if("1400004481".equals(encode)){
//          LOGGER.info("测试"+baseInfo.getIsShow());
//          baseInfo.setIsShow(0);
//        }
        //是否显示（上下渠道）0.下渠道 1.上渠道 2.商家下渠道 3.预上渠道(只针对特食商品) 为0时商家不允许上渠道
        int isShow = baseInfo.getIsShow();
        if(0 == isShow){
            //下架
            return true;
        }
        return false;
    }

    /**
     * 顺丰商品自动下架
     */
    @Override
    public boolean autoInstockSF() {
      List<String> list = new ArrayList<>();
      List<Product> productList = this.productService.selectSfProduct();
      if(productList == null && productList.isEmpty()){
        return false;
      }

      for(Product p : productList){
        if(p != null){
          boolean b = this.checkProductIdShowStatus(p.getEncode());
          if(b){
            long decode = IdTypeHandler.decode(p.getId());
            list.add(decode + "");
          }
        }
      }
      //更新商品状态
      Integer total = 0;
      if(list != null && !list.isEmpty()){
        total = productMapper.productInstock(list);
        LOGGER.info("顺丰商品自动下架数据库更新："+total+"条");
        List<Product> pList = new ArrayList<>();
        for (String s : list){
          Product p = new Product();
          p.setId(IdTypeHandler.encode(Long.parseLong(s)));
          pList.add(p);
        }
        this.sfAutoInstockSendMail(pList);
      }
      LOGGER.info("顺丰商品自动下架共："+total+"条");
      return true;
    }

  /**
   * 保存顺丰拆单的物流号
   * @param logisticsNo
   * @return
   */
  @Override
  public int saveSFlogistics(String logisticsNo,String orderNo) {
    return orderMapper.saveSFlogistics(logisticsNo,orderNo);
  }

  /**
   * 保存顺丰拆单的物流号
   * @param partnerOrderNo
   * @return
   */
  @Override
  public int saveSFsplitOrder(String partnerOrderNo, String orderNo) {
    return orderMapper.saveSFsplitOrder(partnerOrderNo,orderNo);
  }

  /**
   * 更新顺丰订单状态
   * @param partnerOrderNo
   * @param status
   * @return
   */
  @Override
  public int updateSFOrderStatus(String partnerOrderNo, String status) {
    return orderMapper.updateSFOrderStatus(partnerOrderNo,status);
  }

  /**
   * 验证开发者账号
   * @param clientId
   * @return
   */
  @Override
  public int checkDevAccount(String clientId) {
    return sfConfigMapper.checkDevAccount(clientId);
  }

  /**
   * 修改顺丰商品状态
   * @param list
   * @return
   */
  @Override
  public int updateSFProductStatus(List<String> list,String status) {
      int sfProductStatus = productMapper.updateSFProductStatus(list, status);
      return sfProductStatus;
  }

  /**
   * 顺丰商品自动下架发送邮件
   * @param productList
   */
  private boolean sfAutoInstockSendMail(List<Product> productList) {
    //是否开启发送邮件
    boolean flag = sfAutoInstockSendMailSwitch();
    if(flag){
      return false;
    }
    HvEmail hv = new HvEmail("","","");

    List<String> list = new ArrayList<>(); //收件人列表
    Map map = new HashMap();
    map.put("configName","SF_PRODUCT_AUTO_INSTOCK_SEND_EMAIL_USER");
    //查询数据库配置邮件列表
    PromotionConfig c = promotionConfigMapper.selectByConfig(map);
    if(c != null){
      String configValue = c.getConfigValue();
      String[] arr = configValue.split(",");
      for (String s : arr){
        if(StringUtils.isNotBlank(s)){
          list.add(s);
        }
      }
    }
    if(list == null || list.isEmpty()){
      list.add("yarm.yang@handeson.com");
    }
    hv.setToList(list);
    hv.setSubject("顺丰商品自动下架邮件提示");//标题
    hv.setContent(this.getSfAutoInstockSendMailContent(productList));//内容
    try {
      boolean b = HvSendEmailUtil.sendMsg(hv);
      return b;
    } catch (MessagingException e) {
      LOGGER.error("顺丰商品自动下架发送邮件失败！错误信息MessagingException：" + e.getMessage());
    }
    return false;
  }

  /**
   * 是否发送邮件的开关
   * @return
   */
  private boolean sfAutoInstockSendMailSwitch() {
    Map map = new HashMap();
    map.put("configName","PRODUCT_SOLD_OUT_SEND_EMAIL_SWITCH");
    //是否开启发送邮件的开关,false不执行发送邮件代码
    PromotionConfig config = promotionConfigMapper.selectByConfig(map);
    if(config != null){
      if(StringUtils.isNotBlank(config.getConfigValue())
              && "false".equals(config.getConfigValue())){
        return true;
      }
    }
    return false;
  }

  /**
   * 顺丰商品自动下架邮件内容
   * @param productList
   * @return
   */
  private String getSfAutoInstockSendMailContent(List<Product> productList) {
    StringBuffer sb = new StringBuffer();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    sb.append("以下顺丰商品于");
    sb.append(sdf.format(new Date()));
    sb.append("自动下架：<br>");

    for (Product p : productList){
      if(p != null){
        Product product = this.productMapper.selectByPrimaryKey(p.getId());
        if(product != null){
          sb.append(product.getName());
          sb.append("（对应商品id：");
          sb.append(IdTypeHandler.decode(product.getId()));
          sb.append(",对应encode：");
          sb.append(product.getEncode());
          sb.append("）");
          sb.append("<br>");
        }
      }
    }
    sb.append("发送邮件的系统环境：");
    sb.append(profilesActiveValue);

    return sb.toString();
  }

  private SFProductWrapper getSFProductWrapperBySfUrl(String encode) {
    LOGGER.info("请求顺丰商品接口request数据encode；" + encode);
    SfAppConfig appConfig = this.loadConfig();
    String json = "{\"page\":1,\"pageSize\":1,\"number\":"+ encode +"}";
    String url = sfProductSign.basePullSFProductUrl(appConfig,json);
    SFProductWrapper productWrapper = null;
    try {
      productWrapper = this.requestProducts1(url,json,encode);
    }catch (Exception e){
      LOGGER.error("请求顺丰商品接口异常，请求encode是"+encode+"{}异常信息是："+e.getMessage());
    }

    LOGGER.info("请求顺丰商品接口response数据；" + productWrapper+"{}对应encode:" + encode);
    return productWrapper;
  }

    @Override
  public void afterPropertiesSet() {
    List<SFCategoryMapping> mapping = shippingMapper.selectSFCategoryMap();
    sfCategoryMap = new HashMap<>();
    for (SFCategoryMapping categoryMapping : mapping) {
      sfCategoryMap.put(categoryMapping.getCode(), categoryMapping.getName());
    }
  }

    /**
     * 获取顺丰商品2.0版
     * @param url
     * @return
     */
    private SFProductWrapper requestProducts1(String url, String json,String encode) {
        LOGGER.info("请求地址 {}", url);
        LOGGER.info("请求参数 {}", json);
        String result = HttpClientUtil.doPost(url, json, "utf-8");
        JSONObject jsonObject = JSONObject.parseObject(result);
        LOGGER.info("请求顺丰接口，获取顺丰商品信息，返回结果： {}", jsonObject);
        int code = jsonObject.getIntValue("code");
        String data = jsonObject.getString("data");
        String msg = jsonObject.getString("msg");

        //token失效，重新请求
        if (code == 120022 || code == 110006) {
            renewToken();
            this.pullProductByEncode(encode);
        }

        if (code != 0) {
            // TODO 请求异常时重新刷新token或进行其他异常处理

            LOGGER.error("请求地址：" + url);
            throw new RuntimeException("顺丰接口返回：" + msg);
        }

        return JSONObject.parseObject(data, SFProductWrapper.class);
    }
}
