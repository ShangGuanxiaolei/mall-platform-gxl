package com.xquark.service.product.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xquark.dal.mapper.ProductDescMapper;
import com.xquark.dal.model.ProductDesc;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.product.ProductDescService;

@Service("productDescService")
public class ProductDescServiceImpl extends BaseServiceImpl implements
    ProductDescService {

  @Autowired
  private ProductDescMapper productDescMapper;

  @Override
  public ProductDesc load(String productId) {
    return productDescMapper.selectByPrimaryKey(productId);
  }

  @Override
  @Transactional
  public int insert(ProductDesc productDesc) {
    if (productDesc == null) {
      return 0;
    }

    if (StringUtils.isBlank(productDesc.getProductId())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "添加商品描述时，商品的id不能为空");
    }

    if (StringUtils.isBlank(productDesc.getDescription())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "添加商品描述时，商品的 description 不能为空");
    }

    int rc = productDescMapper.insert(productDesc);
    return rc;
  }

  @Override
  public int insertOrder(ProductDesc productDesc) {
    if (productDesc == null) {
      return 0;
    }

    if (StringUtils.isBlank(productDesc.getProductId())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "添加商品描述时，商品的id不能为空");
    }

    if (StringUtils.isBlank(productDesc.getDescription())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "添加商品描述时，商品的 description 不能为空");
    }

    int rc = productDescMapper.insert(productDesc);
    return rc;
  }

  public int update(ProductDesc productDesc) {
    if (StringUtils.isBlank(productDesc.getProductId())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "修改商品描述时，商品的id不能为空");
    }

    return productDescMapper.update(productDesc);
  }


  @Override
  public int delete(String id) {
    ProductDesc p = new ProductDesc();
    p.setArchive(true);
    return productDescMapper.update(p);
  }


  @Override
  public int undelete(String id) {
    ProductDesc p = new ProductDesc();
    p.setArchive(false);
    return productDescMapper.update(p);
  }


}
