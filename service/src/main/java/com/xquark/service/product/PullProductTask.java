package com.xquark.service.product;

import static com.xquark.service.product.SFTaskGlobalContext.APP_CONFIG;
import static com.xquark.service.product.SFTaskGlobalContext.BASE_ENCODES;
import static com.xquark.service.product.SFTaskGlobalContext.SF_PRODUCT_SERVICE;
import static com.xquark.service.product.SFTaskGlobalContext.SHOP;
import static com.xquark.service.product.SFTaskGlobalContext.SIZE;
import static com.xquark.service.product.SFTaskGlobalContext.USER;

import com.xquark.service.http.pojo.sf.product.SFProduct;
import com.xquark.service.http.pojo.sf.product.SFProductWrapper;
import java.util.List;

import com.xquark.service.product.impl.SFProductSign;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * created by
 *
 * @author wangxinhua at 18-6-3 下午9:17 简单处理分页商品拉取, 后续有必要的话将请求跟数据库写入作为生产者消费者处理
 */
public class PullProductTask implements Runnable {

  @Autowired
  private SFProductSign sfProductSign;

  private final int page;

  private final static Logger LOGGER = LoggerFactory.getLogger(PullProductTask.class);

  public PullProductTask(int page) {
    this.page = page;
  }

  @Override
  public void run() {
    LOGGER.info("============ 同步第 {} 页的 {} 条数据 =============", page,
        SIZE);
    String json = "{\"page\":"+page+",\"pageSize\":"+SIZE+"}";
    String pullProductUrl = sfProductSign.basePullSFProductUrl(APP_CONFIG,json);

    SFProductWrapper data;
    try {
      data = SFTaskHelper.requestProducts1(pullProductUrl, json);
      // 如果请求异常统一走异常处理
      assert data != null;

      List<SFProduct> products = data.getProductList();
      SF_PRODUCT_SERVICE.syncPdList(products, BASE_ENCODES, USER, SHOP);
    } catch (Exception e) {
      LOGGER.error("========== 分页 {} 到 {} 商品同步失败  ============", page, SIZE, e);
      // TODO 将同步失败的商品分页放入失败队列后期重试
    }
  }

}
