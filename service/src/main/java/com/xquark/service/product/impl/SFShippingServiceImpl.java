package com.xquark.service.product.impl;

import com.xquark.dal.mapper.ShippingMapper;
import com.xquark.dal.model.Shipping;
import com.xquark.service.http.pojo.sf.product.SFShipping;
import com.xquark.service.product.SFShippingService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @auther liuwei
 * @date 2018/6/8 15:12
 */
@Service
public class SFShippingServiceImpl implements SFShippingService {

  @Autowired
  private ShippingMapper shippingMapper;

  @Override
  public boolean selectShippingByProductId(int productId) {
    boolean isExist = shippingMapper.selectShippingByProductId(productId);
    if (isExist) {
      return true;
    }
    return false;
  }

  @Override
  public int addSFShipping(Shipping shipping) {
    int ret = shippingMapper.insertSFShipping(shipping);
    return ret;
  }
}
