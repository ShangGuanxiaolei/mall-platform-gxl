package com.xquark.service.product.impl;

import com.alibaba.fastjson.JSONObject;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.mapper.ProductCollectionMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.vo.ProductCollectionVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.freshman.FreshManProductService;
import com.xquark.service.product.ProductCollectionService;
import com.xquark.service.product.ProductService;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.user.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Service("productCollectionService")
public class ProductCollectionServiceImpl extends BaseServiceImpl implements
    ProductCollectionService {

  private SkuMapper skuMapper;
  @Autowired
  public void setSkuMapper(SkuMapper skuMapper) {
    this.skuMapper = skuMapper;
  }

  @Autowired
  ProductCollectionMapper productCollectionMapper;

  @Autowired
  private UserService userService;
  @Autowired
  private ProductService productService;
  @Autowired
  private FreshManProductService freshManProductService;

  @Autowired
  private PromotionBaseInfoService promotionBaseInfoService;

  @Override
  public int insert(ProductCollection team) {
    return productCollectionMapper.insert(team);
  }

  @Override
  public int insertOrder(ProductCollection productCollection) {
    return productCollectionMapper.insert(productCollection);
  }

  @Override
  public ProductCollection load(String id) {
    return productCollectionMapper.selectByPrimaryKey(id);
  }



  @Override
  public int deleteForArchive(String id) {
    return productCollectionMapper.updateForArchive(id);
  }

  @Override
  public int update(ProductCollection record) {
    return productCollectionMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<ProductCollectionVO> list(Pageable pager, Map<String, Object> params) {
    return productCollectionMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return productCollectionMapper.selectCnt(params);
  }

  /**
   * 用户取消某个商品收藏
   */
  @Override
  public int deleteByProductId(String userId, String productId) {
    return productCollectionMapper.deleteByProductId(userId, productId);
  }

  /**
   * 查询用户是否收藏某个商品
   */
  @Override
  public ProductCollection selectByProductId(String userId, String productId) {
    return productCollectionMapper.selectByProductId(userId, productId);
  }

  /**
   * 用户我的收藏商品列表
   */
  @Override
  public List<ProductCollectionVO> listByApp(Pageable pager, String userId) {
    List<ProductCollectionVO> result = productCollectionMapper
        .listByApp(pager, userId);

    RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
    User user = (User) getCurrentUser();
    Boolean isProxy = user.getIsProxy();
    Boolean isMember = user.getIsMember();

    for (ProductCollectionVO collection : result) {
      String pId;
      if(StringUtils.isNotBlank(pId=collection.getProductId())){
        //判断是否是新人专区商品
        List<FreshManProductVo> freshManProductVos = freshManProductService.selectByProductId(pId);
        if(CollectionUtils.isNotEmpty(freshManProductVos)){
          //是否是新人专区商品
          collection.setFreshmanProduct(true);
        }
        List<Sku> skus = skuMapper.selectByProductId(pId);
        if(CollectionUtils.isNotEmpty(skus)){
          Sku sku = skus.get(0);
          collection.setDeductionDPoint(sku.getDeductionDPoint());
          collection.setPoint(sku.getPoint());
          collection.setNetWorth(sku.getNetWorth());
          collection.setProductPrice(sku.getPrice());
          collection.setPromoAmt(sku.getPromoAmt());
          collection.setServerAmt(sku.getServerAmt());

          BigDecimal conversionPrice = sku.getPrice().subtract(sku.getDeductionDPoint().divide(new BigDecimal(10))).setScale(2,1);
          BigDecimal subtractValue = sku.getPoint().add(sku.getServerAmt());
          BigDecimal proxyPrice = sku.getPrice().subtract(subtractValue).setScale(2,1);
          collection.setProxyPrice(proxyPrice);

          collection.setMemberPrice(sku.getPrice().subtract(sku.getPoint()).setScale(2,1));

          if(isProxy) {
            collection.setChangePrice(conversionPrice.subtract(subtractValue).setScale(2,1));
          }else if(isMember){
            collection.setChangePrice(conversionPrice.subtract(sku.getPoint()).setScale(2,1));
          }else{
            collection.setChangePrice(conversionPrice);
          }
        }
      }
      boolean isSoldOut = this.isSoldOut(collection.getProductId());
      collection.setIsSoldOut(isSoldOut);
    }
    return result;
  }

  //是否售罄逻辑
  private boolean isSoldOut(String productId) {
    boolean isSoldOut = false;
    boolean commonProduct = this.productService.isCommonProduct(productId);
    if(commonProduct){//普通商品
      isSoldOut = this.productService.checkCommonProductSoldOut(productId);
    }else {
      List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService.selectByProductId(productId);
      isSoldOut = this.productService.checkGroupSoldOut(productId,promotionBaseInfos.get(0).getpCode());
    }
    return isSoldOut;
  }

}
