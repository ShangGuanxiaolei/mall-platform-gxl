package com.xquark.service.product.vo;

import java.util.List;

import org.springframework.beans.BeanUtils;

import com.xquark.dal.model.SkuMapping;

public class SkuMappingVO extends SkuMapping {

  private static final long serialVersionUID = 1L;
  List<String> mappingValues;

  public SkuMappingVO(SkuMapping skuMapping) {
    BeanUtils.copyProperties(skuMapping, this);
  }

  public List<String> getMappingValues() {
    return mappingValues;
  }

  public void setMappingValues(List<String> mappingValues) {
    this.mappingValues = mappingValues;
  }


}
