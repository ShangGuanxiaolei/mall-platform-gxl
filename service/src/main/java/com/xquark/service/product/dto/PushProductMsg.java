package com.xquark.service.product.dto;

import java.util.List;

/**
 * 更新商品短信息
 */
public class PushProductMsg {
    private String devAccount;
    private List<Integer> productIds;
    private List<String> productSns;
    private String type;

    public String getDevAccount() {
        return devAccount;
    }

    public void setDevAccount(String devAccount) {
        this.devAccount = devAccount;
    }

    public List<Integer> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }

    public List<String> getProductSns() {
        return productSns;
    }

    public void setProductSns(List<String> productSns) {
        this.productSns = productSns;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
