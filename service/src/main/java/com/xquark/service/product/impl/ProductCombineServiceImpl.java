package com.xquark.service.product.impl;

import com.xquark.dal.mapper.SkuCombineExtraMapper;
import com.xquark.dal.mapper.SkuCombineMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.SkuCombine;
import com.xquark.dal.model.SkuCombineExtra;
import com.xquark.dal.vo.SkuBasicVO;
import com.xquark.dal.vo.SlaveInfoVO;
import com.xquark.helper.Functions;
import com.xquark.helper.Transformer;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.product.ProductCombineService;
import com.xquark.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * 组合套餐默认实现
 *
 * @author wangxinhua.
 * @date 2018/9/28
 */
@Service
public class ProductCombineServiceImpl implements ProductCombineService {

  private final SkuCombineMapper skuCombineMapper;

  private final SkuCombineExtraMapper skuCombineExtraMapper;

  private final ProductService productService;

  private final SkuMapper skuMapper;

  @Autowired
  public ProductCombineServiceImpl(SkuCombineMapper skuCombineMapper,
      SkuCombineExtraMapper skuCombineExtraMapper,
      ProductService productService,
      SkuMapper skuMapper) {
    this.skuCombineMapper = skuCombineMapper;
    this.skuCombineExtraMapper = skuCombineExtraMapper;
    this.productService = productService;
    this.skuMapper = skuMapper;
  }

  /**
   * 保存或更新组合
   */
  @Transactional(rollbackFor = Exception.class)
  @Override
  public boolean saveOrUpdateCombine(String masterSkuId, String masterProductId,
      List<SlaveInfoVO> slaves, Date validFrom,
      Date validTo) {
    SkuCombine skuCombine = skuCombineMapper.selectBySkuId(masterSkuId);
    if (skuCombine != null) {
      skuCombine.setValidTo(validTo);
      skuCombine.setValidFrom(validFrom);
      skuCombine.setSkuId(masterSkuId);
      skuCombine.setProductId(masterProductId);
      skuCombineMapper.updateByPrimaryKeySelective(skuCombine);
    } else {
      skuCombine = new SkuCombine();
      skuCombine.setValidFrom(validFrom);
      skuCombine.setValidTo(validTo);
      skuCombine.setSkuId(masterSkuId);
      skuCombine.setProductId(masterProductId);
      skuCombineMapper.insert(skuCombine);
    }
    return saveOrUpdateExtra(skuCombine.getId(), slaves);
  }

  /**
   * 保存商品组合信息, 以master为准, 如果关系已存在, 则先删除关联后再新建
   *
   * @param masterId 套餐商品id
   * @param slaves 套餐包含的商品id
   * @return 保存结果
   */
  @Override
  public boolean saveOrUpdateExtra(String masterId, List<SlaveInfoVO> slaves) {
    boolean isMasterExist = skuCombineExtraMapper.selectIsMasterExist(masterId);
    if (isMasterExist) {
      // 清空关联关系
      skuCombineExtraMapper.deleteByMasterId(masterId);
    }
    List<SkuCombineExtra> skuCombineExtras = buildCombine(masterId, slaves);
    return skuCombineExtraMapper.batchInsert(skuCombineExtras) > 0;
  }

  @Override
  public boolean isCombineProduct(String productId) {
    return skuCombineMapper.isCombineProduct(productId);
  }

  @Override
  public boolean isCombineSku(String skuId) {
    return skuCombineMapper.isCombineSku(skuId);
  }

  @Override
  public boolean unCombine(String masterSkuId, String slaveSkuId) {
    return skuCombineExtraMapper.deleteCombine(masterSkuId, slaveSkuId) > 0;
  }

  @Override
  public List<String> listSlaveIdBySkuId(String skuId) {
    List<SkuCombineExtra> extras = skuCombineExtraMapper.listSlaveBySkuId(skuId);
    return Transformer.fromIterable(extras, Functions.<SkuCombineExtra>newIdPropertyFunction());
  }

  @Override
  public List<SkuCombineExtra> listSlaveBySkuId(String skuId) {
    return skuCombineExtraMapper.listSlaveBySkuId(skuId);
  }

  @Override
  public void checkAmount(Sku sku) {
    checkNotNull(sku, "sku不能为空");
    String skuId = sku.getId();
    // TODO 校验套装商品生效时间
    if (!isCombineSku(skuId)) {
      return;
    }
    if (!isCombineProduct(sku.getProductId())) {
      return;
    }
    List<SkuCombineExtra> extras = skuCombineExtraMapper.listSlaveBySkuId(skuId);
    for (SkuCombineExtra extra : extras) {
      SkuBasicVO slaveSku = skuMapper.selectBasicVOByPrimaryKey(extra.getSlaveId());
      if (slaveSku == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "套装中子商品不存在");
      }
      Integer needAmount = extra.getAmount();
      Integer actureAmount = slaveSku.getAmount();
      if (needAmount > actureAmount) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "套装中子商品 " + slaveSku.getFullName() + "库存不足");
      }
    }
  }

  @Override
  public SkuCombineExtra loadExtraBySkuIdAndSlaveId(String skuId, String slaveId) {
    return skuCombineExtraMapper.loadExtraBySkuIdAndSlaveId(skuId, slaveId);
  }

  @Override
  public Integer sumBindedSlaveAmount(String slaveId) {
    return skuCombineExtraMapper.sumBindedSlaveAmount(slaveId);
  }

  @Override
  public Boolean selectIsCombinedSalve(String skuId) {
    return skuCombineExtraMapper.selectIsBindedSlave(skuId);
  }

  /**
   * 构建组合对象
   */
  private List<SkuCombineExtra> buildCombine(String masterId, List<SlaveInfoVO> slaves) {
    List<SkuCombineExtra> ret = new ArrayList<>();
    for (SlaveInfoVO slave : slaves) {
      SkuCombineExtra combine = new SkuCombineExtra();
      combine.setMasterId(masterId);
      combine.setSlaveId(slave.getId());
      // TODO 前台配置页需要传递数量, 商品id暂时不需要
      combine.setAmount(slave.getNumber());
      combine.setSlaveProductId(null);
      ret.add(combine);
    }
    return ret;
  }
}
