package com.xquark.service.product;

import com.xquark.dal.model.Shipping;
import com.xquark.service.http.pojo.sf.product.SFShipping;

/**
 * @auther liuwei
 * @date 2018/6/8 14:29
 */
public interface SFShippingService {

  /**
   * 查询id是否存在
   *
   * @return 返回true存在，false不存在
   */
  boolean selectShippingByProductId(int productId);

  /**
   * 插入顺丰运输信息
   */
  int addSFShipping(Shipping shipping);
}
