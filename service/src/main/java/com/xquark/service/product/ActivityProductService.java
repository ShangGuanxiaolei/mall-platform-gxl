package com.xquark.service.product;

import com.xquark.dal.model.ActivityExclusive;

/**
 * 活动商品服务
 */
public interface ActivityProductService {

    void insertActivityProduct(ActivityExclusive activityExclusive);

    void updateActivityProduct(Integer productId,Integer status);
}
