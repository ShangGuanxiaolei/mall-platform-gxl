package com.xquark.service.product.vo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/2/13
 * Time:9:59
 * Des:顺丰库存
 */
public class SfStockVO {
    public SfStockVO(){}
    private boolean sfProduct = true;
    //private boolean isSoldOut;
    //private String soldOutName;
    private int stockStatus; //库存状态
    private String stockStatusName; //库存状态名称，0=缺货（已售罄），1=无法送达，2=现货，3=预订，4=商品错误

    public boolean getSfProduct() {
        return sfProduct;
    }

    public void setSfProduct(boolean sfProduct) {
        this.sfProduct = sfProduct;
    }

    public int getStockStatus() {
        return stockStatus;
    }

    public void setStockStatus(int stockStatus) {
        this.stockStatus = stockStatus;
    }

    public String getStockStatusName() {
        return stockStatusName;
    }

    public void setStockStatusName(String stockStatusName) {
        this.stockStatusName = stockStatusName;
    }
}