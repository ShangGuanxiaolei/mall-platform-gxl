package com.xquark.service.product.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Objects;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/28
 * Time:15:45
 * Des:商品搜索过滤关键字
 */
public class ProductSearchVO {

    public ProductSearchVO(){};

    private Integer selfOperatedKey;//汉薇自营,0：非自营，1：自营
    private Integer stockKey; //仅看有货,1：是，0：否

    public Integer getSelfOperatedKey() {
        return selfOperatedKey;
    }

    public void setSelfOperatedKey(Integer selfOperatedKey) {
        this.selfOperatedKey = selfOperatedKey;
    }

    public Integer getStockKey() {
        return stockKey;
    }

    public void setStockKey(Integer stockKey) {
        this.stockKey = stockKey;
    }
}