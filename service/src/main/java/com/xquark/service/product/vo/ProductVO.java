package com.xquark.service.product.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.Shareable;
import com.xquark.dal.model.*;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import com.xquark.dal.vo.BasePromotionCouponVO;
import com.xquark.dal.vo.CategoryVO;
import com.xquark.dal.vo.FilterBasicVO;
import com.xquark.dal.vo.FlashSalePromotionProductVO;
import com.xquark.dal.vo.FmtQiNiuImgVO;
import com.xquark.dal.vo.FragmentVO;
import com.xquark.dal.vo.PromotionBargainProductVO;
import com.xquark.dal.vo.PromotionDiamondVO;
import com.xquark.dal.vo.PromotionGrouponVO;
import com.xquark.dal.vo.SkuAttributeVO;
import com.xquark.dal.vo.YundouProductVO;
import com.xquark.service.yundou.YundouSettingService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.List;
import java.util.function.Consumer;



/**
 * 用于展现单个商品详情
 *
 * @author jamesp
 */
public class ProductVO extends Product implements Shareable, FmtQiNiuImgVO, DynamicPricing {

  private static final long serialVersionUID = 1L;

  private static String HTML_HEADER_FILE="/html/header.html";

  private static String HTML_ENCODING="UTF-8";

  private static String HTML_HEADER="";

  private String detailH5WithHeader="";

  private BigDecimal conversionPrice;

  private BigDecimal reducedPrice;

  public String getDetailH5WithHeader() {
      if(StringUtils.isBlank(HTML_HEADER)){
        try {
          HTML_HEADER=IOUtils.resourceToString(HTML_HEADER_FILE,Charset.forName(HTML_ENCODING));
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    return HTML_HEADER+super.getDetailH5();
  }

  @ApiModelProperty(value = "商品所有sku信息")
  private List<Sku> skus;
  private List<String> mainImgs;

  private List<SkuMappingVO> skuMappings;

  private List<BasePromotionCouponVO> couponList;

  private List<Tag> tags;

  // 滤芯
  private List<? extends FilterBasicVO> filters;

  // 随机取的一个商品的分类
  @JsonIgnore
  private CategoryVO category;

  // 一个商品会对应多个分类
  private List<CategoryVO> categorys;

  private ProductDesc productDesc; // 富文本信息

  @ApiModelProperty(value = "商品所有图片（即商品轮播图）")
  private List<ProductImageVO> imgs;

  private List<Product> relateds = null;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String imgUrl;

  private String url;

  private BigDecimal commission;

  private String directCartUrl;

  @ApiModelProperty(value = "商品片段信息，如果这里有值，则商详使用这里的数据，如果没有值，则使用imgs中的所有图片当作商详信息")
  private List<FragmentVO> fragments;

  private long countSeller;

  private Boolean onSaleForSeller;

  private String productType;

  private boolean isFlashsale; // 是否正参与限时特购活动

  private boolean isDiamond; // 是否正在参与品牌专题活动

  private boolean isBargain; // 是否正在参与砍价活动

  private boolean isGroupon; // 是否正在参与团购活动

  private boolean isYundou; // 是否正在参与积分活动

  private boolean isSelect;

  private BigDecimal discountPrice; // 折扣价

  private BigDecimal fullCutPrice;

  private BigDecimal insideBuyPrice;

  private PromotionDiamondVO diamondVO;

  private FlashSalePromotionProductVO flashsaleVO;

  private PromotionGrouponVO grouponVO;

  private YundouProductVO yundouVO;

  private boolean isSoldOut;

  private int selfOperated; //是否自营:0非自营，1自营

  @Override
  public int getSelfOperated() {
    return selfOperated;
  }

  @Override
  public void setSelfOperated(int selfOperated) {
    this.selfOperated = selfOperated;
  }

  public boolean isIsSoldOut() {
    return isSoldOut;
  }

  public void setIsSoldOut(boolean soldOut) {
    isSoldOut = soldOut;
  }

  //是否新人专区商品
  private boolean freshmanProduct;

  public boolean isFreshmanProduct() {
    return freshmanProduct;
  }

  public void setFreshmanProduct(boolean freshmanProduct) {
    this.freshmanProduct = freshmanProduct;
  }

  @ApiModelProperty(value =
      "该商品对应的所有规格属性和规格属性值，用于web端展示多sku，该vo中每个规格的Item的id用'-'相加即对应sku属性的attribute字段,如商品有颜色，大小两种规格，且颜色有白色（item id为1）,红色(item id为2)；大小有S码（item id为a），有L码（item id为b）\n"
          +
          "则用户选择白色+S码，代表选中sku中attribute属性为\"1-a\"的记录，库存和价格变换取该sku记录中的amount和price")
  private List<SkuAttributeVO> skuAttributes;

  public List<SkuAttributeVO> getSkuAttributes() {
    return skuAttributes;
  }

  public void setSkuAttributes(List<SkuAttributeVO> skuAttributes) {
    this.skuAttributes = skuAttributes;
  }

  public PromotionDiamondVO getDiamondVO() {
    return diamondVO;
  }

  public void setDiamondVO(PromotionDiamondVO diamondVO) {
    this.diamondVO = diamondVO;
  }

  public boolean getIsFlashsale() {
    return isFlashsale;
  }

  public void setIsFlashsale(boolean isFlashsale) {
    this.isFlashsale = isFlashsale;
  }

  public boolean getIsDiamond() {
    return isDiamond;
  }

  public void setIsDiamond(boolean diamond) {
    isDiamond = diamond;
  }

  public BigDecimal getDiscountPrice() {
    return discountPrice;
  }

  public void setDiscountPrice(BigDecimal discountPrice) {
    this.discountPrice = discountPrice;
  }

  public String getProductType() {
    return productType;
  }

  public void setProductType(String productType) {
    this.productType = productType;
  }

  public ProductVO(Product product) {
    BeanUtils.copyProperties(product, this);
    this.setCollected(product.getIsCollected());
    this.setDNAProduct(product.getIsDNAProduct());
  }

  public long getCountSeller() {
    return countSeller;
  }

  public void setCountSeller(long countSeller) {
    this.countSeller = countSeller;
  }

  public Boolean getOnSaleForSeller() {
    return onSaleForSeller;
  }

  public void setOnSaleForSeller(Boolean onSaleForSeller) {
    this.onSaleForSeller = onSaleForSeller;
  }

  public List<Tag> getTags() {
    return tags;
  }

  public void setTags(List<Tag> tags) {
    this.tags = tags;
  }

  public List<SkuMappingVO> getSkuMappings() {
    return skuMappings;
  }

  public void setSkuMappings(List<SkuMappingVO> skuMappings) {
    this.skuMappings = skuMappings;
  }

  public List<Sku> getSkus() {
    return skus;
  }

  public void setSkus(List<Sku> skus) {
    this.skus = skus;
  }

  public List<ProductImageVO> getImgs() {
    return imgs;
  }

  public void setImgs(List<ProductImageVO> imgs) {
    this.imgs = imgs;
  }

  public List<Product> getRelateds() {
    return relateds;
  }

  public void setRelateds(List<Product> relateds) {
    this.relateds = relateds;
  }

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public BigDecimal getCommission() {
    return commission;
  }

  public void setCommission(BigDecimal commission) {
    this.commission = commission;
  }

  public CategoryVO getCategory() {
    return category;
  }

  public void setCategory(CategoryVO category) {
    this.category = category;
  }

  public String getDirectCartUrl() {
    return directCartUrl;
  }

  public void setDirectCartUrl(String directCartUrl) {
    this.directCartUrl = directCartUrl;
  }

  public List<FragmentVO> getFragments() {
    return fragments;
  }

  public void setFragments(List<FragmentVO> fragments) {
    this.fragments = fragments;
  }

  public List<String> getMainImgs() {
    return mainImgs;
  }

  public void setMainImgs(List<String> mainImgs) {
    this.mainImgs = mainImgs;
  }

  public ProductDesc getProductDesc() {
    return productDesc;
  }

  public void setProductDesc(ProductDesc productDesc) {
    this.productDesc = productDesc;
  }

  public List<CategoryVO> getCategorys() {
    return categorys;
  }

  public void setCategorys(List<CategoryVO> categorys) {
    this.categorys = categorys;
  }

  public boolean isFlashsale() {
    return isFlashsale;
  }

  public void setFlashsale(boolean flashsale) {
    isFlashsale = flashsale;
  }

  public boolean getIsBargain() {
    return isBargain;
  }

  public void setIsBargain(boolean bargain) {
    isBargain = bargain;
  }

  public boolean getIsGroupon() {
    return isGroupon;
  }

  public void setIsGroupon(boolean groupOn) {
    isGroupon = groupOn;
  }

  public boolean getIsYundou() {
    return isYundou;
  }

  public void setIsYundou(boolean yundou) {
    isYundou = yundou;
  }

  public YundouProductVO getYundouVO() {
    return yundouVO;
  }

  public void setYundouVO(YundouProductVO yundouVO) {
    this.yundouVO = yundouVO;
  }

  public PromotionGrouponVO getGrouponVO() {
    return grouponVO;
  }

  public void setGrouponVO(PromotionGrouponVO grouponVO) {
    this.grouponVO = grouponVO;
  }

  public List<? extends FilterBasicVO> getFilters() {
    return filters;
  }

  public void setFilters(List<? extends FilterBasicVO> filters) {
    this.filters = filters;
  }



  /**
   * 获取商品的积分兑换比例或全局兑换比例
   *
   * @return 该商品的兑换比例
   */
  public Integer getScaleOrUseGlobal() {
    if (getYundouScale() != null && getYundouScale() != 0) {
      return getYundouScale();
    }
    return YundouSettingService.GLOBAL_SETTING.getAmount();
  }

  @Override
  public String getShareTitle() {
    return this.getName();
  }

  @Override
  public String getShareImg() {
    return this.getImgUrl();
  }

  public boolean isSelect() {
    return isSelect;
  }

  public void setSelect(boolean select) {
    isSelect = select;
  }

  @Override
  public String getTinyImgUrl() {
    return imgUrl;
  }

  @Override
  public String getMiddleImgUrl() {
    return imgUrl;
  }

  public List<BasePromotionCouponVO> getCouponList() {
    return couponList;
  }

  public void setCouponList(List<BasePromotionCouponVO> couponList) {
    this.couponList = couponList;
  }

  public BigDecimal getFullCutPrice() {
    return fullCutPrice;
  }

  public void setFullCutPrice(BigDecimal fullCutPrice) {
    this.fullCutPrice = fullCutPrice;
  }

  public BigDecimal getInsideBuyPrice() {
    return insideBuyPrice;
  }

  public void setInsideBuyPrice(BigDecimal insideBuyPrice) {
    this.insideBuyPrice = insideBuyPrice;
  }

  public FlashSalePromotionProductVO getFlashsaleVO() {
    return flashsaleVO;
  }

  public void setFlashsaleVO(FlashSalePromotionProductVO flashsaleVO) {
    this.flashsaleVO = flashsaleVO;
  }

  @Override
  public BigDecimal getReduction() {
      return getPoint();
  }

  @Override
  public void setReduction(BigDecimal point) {
    this.setPoint(point);
  }

  public BigDecimal getConversionPrice() {
    return this.getPrice().subtract(this.getDeductionDPoint().divide(new BigDecimal(10))).setScale(2,1);
  }

  @Override
  public void setReducedPrice(BigDecimal reducedPrice) {
    this.reducedPrice = reducedPrice;
  }

  public void setConversionPrice(BigDecimal conversionPrice) {
    this.conversionPrice = conversionPrice;
  }

  /**
   * 谨慎调用，有副作用
   * @param cons 消费函数
   */
  public void modifySkus(Consumer<Sku> cons) {
    if (cons != null && CollectionUtils.isNotEmpty(this.skus)) {
      for (Sku sku : skus) {
        cons.accept(sku);
      }
    }
  }
}
