package com.xquark.service.product.impl;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.xquark.aop.anno.NotNull;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.PromotionConstants;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.ProductReviewStatus;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.ProductListSortType;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.type.ProductType;
import com.xquark.dal.type.SkuCodeResourcesType;
import com.xquark.dal.vo.*;
import com.xquark.helper.Transformer;
import com.xquark.service.activity.ActivityService;
import com.xquark.service.category.CategoryService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.file.ImageService;
import com.xquark.service.fragment.FragmentImageService;
import com.xquark.service.fragment.FragmentService;
import com.xquark.service.fragment.ProductFragmentService;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.logistics.LogisticsGoodsService;
import com.xquark.service.mail.JavaMailWithAttachment;
import com.xquark.service.order.SfRegionService;
import com.xquark.service.product.ProductCollectionService;
import com.xquark.service.product.ProductCombineService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.*;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.promotion.PromotionSkusService;
import com.xquark.service.rocketmq.SyncRocketMq;
import com.xquark.service.shop.ShopPostAgeService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.sync.SyncEvent;
import com.xquark.service.syncevent.KDSyncData;
import com.xquark.service.syncevent.SyncEventService;
import com.xquark.service.syncevent.SyncEventType;
import com.xquark.service.tags.impl.ProductTagsService;
import com.xquark.service.user.UserService;
import com.xquark.service.vo.LogisticsStock;
import com.xquark.service.vo.LogisticsStockParam;
import com.xquark.service.yundou.YundouSettingService;
import com.xquark.utils.Email.HvSendEmailUtil;
import com.xquark.utils.Email.vo.HvEmail;
import com.xquark.utils.ExcelUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.list.SetUniqueList;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.util.FieldUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkArgument;
import static com.xquark.helper.Transformer.idHandlerConvertFunction;

@Service("productService")
public class ProductServiceImpl extends BaseServiceImpl implements
    ProductService {

  @Autowired
  private ShopMapper shopMapper;
  @Autowired
  private CategoryActivityMapper categoryActivityMapper;
  @Autowired
  private ActivityMapper activityMapper;
  @Autowired
  private ProductMapper productMapper;
  @Autowired
  private ActivityService activityService;
  @Autowired
  private SkuMapper skuMapper;
  @Autowired
  private ShopService shopService;
  @Autowired
  private FragmentImageService fragmentImageService;
  @Autowired
  private FragmentService fragmentService;
  @Autowired
  private SkuMappingMapper skuMappingMapper;
  @Autowired
  private ProductTagMapper productTagMapper;
  @Autowired
  private TagMapper tagMapper;
  @Autowired
  private ProductImageMapper productImageMapper;
  @Autowired
  private ProdSyncMapper prodSyncMapper;

  @Autowired
  private ShopTreeMapper shopTreeMapper;

  @Autowired
  private ImageService imageService;
  @Autowired
  private SyncEventService syncEventService;

  @Autowired
  private UserService userService;

  @Autowired
  @Qualifier("productCollectionService")
  private ProductCollectionService collectionService;

  @Autowired
  private ProductTagsService tagsService;

  @Autowired
  private YundouSettingService yundouSettingService;

  @Autowired
  private ProductUserMapper productUserMapper;

  @Autowired
  private ProductFilterMapper productFilterMapper;

  @Autowired
  private ProductCombineService productCombineService;

  @Value("${site.web.host.name}")
  String siteHost;

  @Value("${spider.update.switch}")
  private int updateSwitch;

  @Value("${sync.scheme.switch}")
  private Integer syncSwitch;
  @Value("${profiles.active}")
  private String profilesActiveValue;
  @Autowired
  private ProductFragmentService productFragmentService;

  Integer skuMappingMaxLen = 5;
  @Autowired
  private CategoryService categoryService;
  @Autowired
  private ShopPostAgeService shopPostAgeService;
  @Autowired
  private SyncRocketMq syncRocketMq;

  @Autowired
  private BrandMapper brandMapper;

  @Autowired
  private WareHouseMapper wareHouseMapper;

  @Autowired
  private TermRelationshipMapper termRelationshipMapper;

  @Autowired
  private BrandProductMapper brandProductMapper;

  @Autowired
  private PromotionSkusService promotionSkusService;
  @Autowired
  private PromotionBaseInfoService promotionBaseInfoService;

  @Autowired
  private PromotionConfigMapper promotionConfigMapper;

  @Autowired
  private SfRegionService sfRegionService;
  @Autowired
  private LogisticsGoodsService logisticsGoodsService;
  @Autowired
  private SystemRegionMapper systemRegionMapper;

  @Autowired
  private FreshManService freshManService;

  // 商品相关的查询
  @Override
  public Product findProductById(String id) {
    Product p = productMapper.selectByPrimaryKey(id);
    if (p == null) {
      log.warn("您要查看的商品" + id + "不存在");
    }

    return p;
  }

  @Override
  public List<Product> findProductsBySourceProductID(String id) {
    return productMapper.listProductsBySourceId(id);
  }

  @Override
  public List<Product> findProductsBySkuCode(String shopId, String skuCode) {
    return productMapper.findProductsBySkuCode(shopId, skuCode);
  }

  @Override
  public ProductVO load(String productId) {
    return productToVo(findProductById(productId));
  }

  public ProductVO load(String productId, Boolean syncSource) {
    ProductVO prodVO = productToVo(findProductById(productId));

    // TODO:2015-11-09@liubei how to handle multiple level product distribution?
    // need better control for distribution product info sync on writting but not reading
    // check source product
    if (syncSource.equals(Boolean.TRUE) && !prodVO.getSourceProductId().isEmpty()) {
      ProductVO srcProdVO = productToVo(findProductById(prodVO.getSourceProductId()));
      if (srcProdVO != null) {
        prodVO.setAmount(srcProdVO.getAmount());
        prodVO.setStatus(srcProdVO.getStatus());
        log.info("load source product info to distribution productVO, productId:" + productId
            + " srcStatus:" + srcProdVO.getStatus() + " and srcAmount:" + srcProdVO.getAmount());
      }
    }

    return prodVO;
  }

  @Override
  public boolean exists(String productId) {
    return productMapper.exists(productId);
  }

  @Override
  public ProductSkuVO load(String sellerId, String productId, String skuId) {
    final Product product = findProductById(productId);

    // 验证用户传参的用户id 是否是商品的创建人
    if (!StringUtils.isBlank(sellerId)
        && !product.getUserId().equals(sellerId)) {
      log.warn("您要查看的商品的创建人" + product.getUserId() + "和传参" + sellerId
          + "不一致，没有权限查看");
      return null;
    }

    final Sku sku = loadSku(productId, skuId);
    final ProductSkuVO productSkuVO = new ProductSkuVO(product);
    productSkuVO.setSku(sku);

    // 如果是代销商品则加载原商品信息
    String sourceProductId = product.getSourceProductId();
    if (StringUtils.isNotEmpty(sourceProductId)) {
      Product sourceProduct = findProductById(sourceProductId);
      Sku sourceSku = loadSku(sourceProductId, sku.getSourceSkuId());
      productSkuVO.setSourceProduct(sourceProduct);
      productSkuVO.setSourceSku(sourceSku);
    }

    return productSkuVO;
  }

  @Override
  public ProductSkuVO load(String productId, String skuId) {
    return load(null, productId, skuId);
  }

  // sku相关的查询

  /**
   * 根据sku和mapping，获得spec描述值
   *
   * @param br 是否分行显示 加/n
   */
  @Override
  public String findSkuSpec(Sku sku, boolean br) {
    final List<SkuMapping> skuMappings = skuMappingMapper
        .selectByProductId(sku.getProductId());
    return findSkuSpec(sku, skuMappings, br);
  }

  private String findSkuSpec(Sku sku, List<SkuMapping> skuMappings, boolean br) {
    if (sku == null) {
      return StringUtils.EMPTY;
    }

    StringBuffer spec = new StringBuffer();
    for (final SkuMapping skuMapping : skuMappings) {
      if (spec.length() > 0) {
        spec.append(br ? " \n" : ";");
      }
      spec.append(skuMapping.getSpecName())
          .append(":")
          .append((String) FieldUtils.getProtectedFieldValue(
              skuMapping.getSpecKey(), sku));
    }

    if (StringUtils.isBlank(spec)) {
      return sku.getSpec();
    }

    return spec.toString();
  }

  /**
   * 写sku的spec值
   */
  private void initSkuSpec(List<Sku> skus, List<SkuMapping> skuMappings) {
    if (skuMappings == null || skuMappings.size() == 0) {
      return;
    }

    StringBuffer spec;
    for (final Sku sku : skus) {
      spec = null;
      for (final SkuMapping skuMapping : skuMappings) {
        if (spec == null) {
          spec = new StringBuffer("");
        } else {
          spec.append(";");
        }
        spec.append(skuMapping.getSpecName()
            + ":"
            + (String) FieldUtils.getProtectedFieldValue(
            skuMapping.getSpecKey(), sku));
      }
      sku.setSpec(spec.toString());
    }
  }

  /**
   * 检查sku和skuMapping是否符合
   */
  private boolean checkSkuAndSkuMapping(List<Sku> skus,
      List<SkuMapping> skuMappings) {
    if (skus == null || skus.size() == 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "sku不能为空");
    }

    for (final Sku sku : skus) {
      if (StringUtils.isBlank(sku.getSpec())
          && StringUtils.isBlank(sku.getSpec1())) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            "sku spec1不能为空");
      }
    }

    if (skuMappings == null || skuMappings.size() == 0) {
      return true;
    }

    if (skuMappings.size() > skuMappingMaxLen) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "skuMapping的数量目前最大支持" + skuMappingMaxLen + "维,而传入了"
              + skuMappings.size());
    }

    // sku和skuMapping是否按顺序设置
    final String specPrefix = "spec";
    for (int i = skuMappingMaxLen; i > 0; i--) {
      for (final SkuMapping skuMapping : skuMappings) {
//				if (skuMapping.getSpecKey().equals(specPrefix + "i")
//						&& i > skuMappings.size()) {
//					throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
//							"请依次设置sku mapping的spec值");
//				}

        if (StringUtils.isBlank(skuMapping.getSpecName())
            || StringUtils.isBlank(skuMapping.getSpecKey())) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "skuMapping specName 和specKey 不能为空");
        }
      }
      for (final Sku sku : skus) {
//				if (StringUtils.isNoneBlank(sku.getSpec())) {
//					throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
//							"sku的spec不能有值");
//				}

        if (StringUtils.isNotBlank((String) FieldUtils
            .getProtectedFieldValue("spec" + i, sku))
            && i > skuMappings.size()) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "请依次设置sku的spec值或者spec的数量大于mapping的数量");
        }
      }
    }

    // 检查skuMapping有无对应的sku的spec1/2/3/4/5
    for (final Sku sku : skus) {
      for (final SkuMapping skuMapping : skuMappings) {
        if (StringUtils.isBlank((String) FieldUtils
            .getProtectedFieldValue(skuMapping.getSpecKey(), sku))) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "sku的" + skuMapping.getSpecName() + "的"
                  + skuMapping.getSpecKey() + "不能为空");
        }
      }

    }
    return true;
  }

  /**
   * 增加 mappingValues,如颜色mapping，里有黄色、绿色，红色，方便客户端显示
   */
  private List<SkuMappingVO> skuMappingToVO(String productId, List<Sku> skus) {
    final List<SkuMappingVO> skuMappingVos = new ArrayList<SkuMappingVO>();

    // 无效sku==一个商品只有一个sku
    if (skus.size() == 1 && "无".equals(skus.get(0).getSpec())) {
      return skuMappingVos;
    }

    final List<SkuMapping> skuMappings = skuMappingMapper
        .selectByProductId(productId);
    for (final SkuMapping skuMapping : skuMappings) {
      final SkuMappingVO vo = new SkuMappingVO(skuMapping);
      final List<String> values = SetUniqueList
          .setUniqueList(new ArrayList<String>());
      for (final Sku sku : skus) {
        final String spec = (String) FieldUtils.getProtectedFieldValue(
            skuMapping.getSpecKey(), sku);
        values.add(spec);
      }
      vo.setMappingValues(values);
      skuMappingVos.add(vo);
    }

    return skuMappingVos;
  }

  @Override
  public Sku loadSku(String skuId) {
    Sku sku = skuMapper.selectByPrimaryKey(skuId);
    if (sku == null) {
      log.warn("您要查询的sku" + skuId + "不存在");
    }
    return sku;
  }

  @Override
  public Sku loadSku(String productId, String skuId) {
    final Sku sku = skuMapper.select(productId, skuId);
    if (sku == null) {
      log.warn("您要查询的商品" + productId + "sku" + skuId + "不存在");
      return null;
    }
    return sku;
  }

  private void pushSyncQueue(Long dstFlag, SyncEvent evType,
      List<Product> list) {
    final List<ProductVO> prodList = new ArrayList<ProductVO>();
    if (list == null || list.size() == 0) {
      return;
    }
    for (final Product product : list) {
      prodList.add(productToVo(product));
    }
    final KDSyncData ev = new KDSyncData();
    ev.setSiteHost(siteHost);
    ev.setPartnerFlag(dstFlag);
    ev.setEvType(evType);
    ev.setProdList(prodList);
    syncEventService.publishEvent(ev);
  }

  private List<SkuMapping> adaptaOldClient(List<Sku> skus,
      List<SkuMapping> mappings) {
    if (mappings != null && mappings.size() > 0) {
      return mappings;
    }

    final List<SkuMapping> skuMappings = new ArrayList<SkuMapping>();

    if (skus.size() == 1 && "无".equals(skus.get(0).getSpec())
        || "无".equals(skus.get(0).getSpec1())) {
      skus.get(0).setSpec("无");
      skus.get(0).setSpec1("无");
      return mappings;
    }

    for (final Sku sku : skus) {
      sku.setSpec1(sku.getSpec());
      sku.setSpec2(null);
    }

    final SkuMapping skuMapping = new SkuMapping();
    skuMapping.setSpecKey("spec1");
    skuMapping.setSpecName("型号");
    skuMappings.add(skuMapping);

    return skuMappings;
  }

  @Override
  public int create(Product product, List<Sku> skus, List<Tag> tags, List<ProductImage> imgs,
      List<SkuMapping> skuMapping) {
    return this.create(product, skus, tags, null, imgs, skuMapping);
  }

  /**
   * 创建产品，并自动汇总Sku中的库存总额和最低的价格，到产品主表上
   */
  @Override
  @Transactional
  public int create(Product product, List<Sku> skus, List<Tag> tags, List<Tags> tagsToSave,
      List<ProductImage> imgs, List<SkuMapping> skuMapping) {
    if (product.getStatus() == ProductStatus.FORSALE
        && product.getForsaleAt() == null) {
      // 发布计划没有发布时间
      return 0;
    }

    if (product.getStatus() == ProductStatus.ONSALE) {
      product.setOnsaleAt(Calendar.getInstance().getTime());
    }
    // 设置延迟发布默认值
    if (product.getDelayed() == null) {
      product.setDelayed(0);
    }
    // 设置是否支持退款
    if (product.getSupportRefund() == null) {
      product.setSupportRefund(false);
    }
    // 设置推荐的默认值
    if (product.getRecommend() == null) {
      product.setRecommend(Boolean.FALSE);
    }
    calProduct(product, skus);

    // 设置首张图片尺寸
    initImgSize(product);

    final int rc = insert(product);
    // 添加code
    productMapper.addCode(product.getId());

    //checkSkuAndSkuMapping(skus, skuMapping);
    //initSkuSpec(skus, skuMapping);
    //skuMapping = adaptaOldClient(skus, skuMapping); // 适配老的客户端

    //创建代销商品时同时复制skuid到代销sku
    if (StringUtils.isNotEmpty(product.getSourceProductId())) {
      for (Sku sku : skus) {
        sku.setSourceSkuId(sku.getId());
      }
    }

    saveSku(null, product, skus); // 保存sku
    //saveSkuMapping(null, product, skuMapping); // 保存skuMapping
//		saveTags(null, product, tags); // 保存tags信息
    saveTagsWithProduct(product, tagsToSave);
    saveImgs(null, product, imgs); // 保存商品图片

    if (syncSwitch == 0) {
      final List<Product> aList = new ArrayList<Product>();
      aList.add(product);
      pushSyncQueue(KDSyncData.XIANGQU_DST, SyncEventType.STATUS_ONSALE,
          aList);
    } else {
      /**final SyncMqEvent event = new SyncMqEvent();
       final List<String> ids = new ArrayList<String>();
       ids.add(product.getId());
       event.setIds(ids);
       event.setEvent(1);
       event.setType(SyncEvType.EV_PRODUCT.ordinal());
       event.setTimestamp(new Date().getTime());
       syncRocketMq.sendToMQ(event);**/
    }

    return rc;
  }

  /*
   * @Override public void updatePrice(Product product, List<Sku> skus,
   * BigDecimal newPrice, UpdatePriceType type) { }
   */

  @Override
  public void lockProduct(String productId, Boolean ret) {
    final Product p = new Product();
    p.setId(productId);
    p.setUpdateLock(ret);
    productMapper.updateByPrimaryKeySelective(p);
  }

  /**
   * 更新产品 操作流程： 1.检查被修改的产品和Sku是否属于操作者本身 2.并自动汇总Sku中的库存总额和最低的价格，到产品主表上
   * 3.如果产品库存为0，则自动下架，并记录下架时间，如果从0改为>0的值，则自动上架，并记录上架时间 4.删除图片 5.保存更新sku 6.删除被标志为删除的sku
   */
  @Override
  @Transactional
  public int update(Product product, List<Sku> skus, List<Tag> tags,
      List<ProductImage> imgs, List<SkuMapping> skuMapping) {
    // 检查product和sku是否属于自己
    final ProductVO dbProduct = load(product.getId());
    if (dbProduct == null) {
      return 0;
    }

    if (!validOwner(dbProduct, product, skus, product.getUserId())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "你没有权限修改该商品");
    }

    if (Boolean.TRUE.equals(dbProduct.getUpdateLock())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "商品正在参与促销活动，不能编辑");
    }

    return update(dbProduct, product, skus, tags, imgs, skuMapping, true);
  }

  /**
   * 更新分销产品 操作流程： 1.检查被修改的产品和Sku是否属于操作者本身 2.并自动汇总Sku中的库存总额和最低的价格，到产品主表上
   * 3.如果产品库存为0，则自动下架，并记录下架时间，如果从0改为>0的值，则自动上架，并记录上架时间 4.删除图片 5.保存更新sku,不同于update方法的行为是确保修改分销商品所属的Sku&SkuMapping,避免load方法返回ProductVO对象时包含的是源商品Sku&SkuMapping引起的副作用
   * 6.删除被标志为删除的sku
   *
   * @param skuMapping @return
   */
  @Override
  public int updateDistributionProduct(Product product, List<Sku> skus, List<Tag> tags,
      List<ProductImage> imgs, List<SkuMapping> skuMapping) {
    // 检查product和sku是否属于自己
    final ProductVO dbProduct = load(product.getId());
    if (dbProduct == null) {
      return 0;
    }

    // 读取真实Sku
    final List<Sku> rawSkus = findByProductId(product.getId());
    dbProduct.setSkus(rawSkus);
    // 读取真实SkuMapping
    dbProduct.setSkuMappings(skuMappingToVO(product.getId(), rawSkus));

//		if (!validOwner(dbProduct, product, skus, product.getUserId())) {
//			throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "你没有权限修改该商品");
//		}

    if (Boolean.TRUE.equals(dbProduct.getUpdateLock())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "商品正在参与促销活动，不能编辑");
    }

    return update(dbProduct, product, skus, tags, imgs, skuMapping, true);
  }

  private int update(ProductVO modProduct, Product product, List<Sku> skus,
      List<Tag> tags, List<ProductImage> imgs,
      List<SkuMapping> skuMapping, boolean isChangePrice) {
    if (product.getStatus() == ProductStatus.ONSALE
        && modProduct.getStatus() != ProductStatus.ONSALE) {
      product.setOnsaleAt(Calendar.getInstance().getTime());
    }

    log.debug("product update id=[" + modProduct.getId() + "]");

    // 设置首张图片尺寸
    initImgSize(product);

    // 计算产品的库存和最低价格
    if (skus != null) {
      calProduct(product, skus);
    }

    if (ProductSource.NEWSPIDER.name().equals(product.getSource())) { // 不更新商品和sku价格
      product.setPrice(null);
      product.setMarketPrice(null);
      product.setOriginalPrice(null);
      product.setAmount(null);
    }

    // 验证商品的价格是否合理
    checkMoney(product.getPrice());
    checkMoney(product.getMarketPrice());
    checkMoney(product.getOriginalPrice());

    final int rc = productMapper.updateByPrimaryKeySelective(product);
    List<Product> products = productMapper.selectCopyProduct(product.getId());
    if(products.size()!=0){
      productMapper.updateCopyProductByPrimaryKey(product);
    }


    // spider ignore sku changeds for now
    if (skus != null
        && !StringUtils.equals(ProductSource.NEWSPIDER.name(), product.getSource())) {
      checkSkuAndSkuMapping(skus, skuMapping);
      initSkuSpec(skus, skuMapping);
      skuMapping = adaptaOldClient(skus, skuMapping); // 适配老的客户端
      log.debug("start save sku...");
      saveSku(modProduct, product, skus); // 保存sku
      log.debug("start save skumapping...");
      saveSkuMapping(modProduct, product, skuMapping); // 保存skuMapping
    } else {
      log.info("do not processing the sku. productId:{}", product.getId());
    }

    if (tags != null) {
      log.debug("start save tags...");
      saveTags(modProduct, product, tags); // 保存tags信息
    }
    if (imgs != null) {
      log.debug("start save imgs...");
      if (ProductSource.NEWSPIDER.name().equals(product.getSource())) {
        saveImgsForSpider(product, imgs, 1); // 保存商品图片
      } else {
        saveImgs(modProduct, product, imgs); // 保存商品图片
      }
    }

    if (syncSwitch == 0) {
      final List<Product> aList = new ArrayList<Product>();
      aList.add(product);
      if (product.getStatus() == ProductStatus.ONSALE
          && modProduct.getStatus() != ProductStatus.ONSALE) {
        product.setOnsaleAt(Calendar.getInstance().getTime());
        pushSyncQueue(KDSyncData.XIANGQU_DST,
            SyncEventType.STATUS_ONSALE, aList);
      } else {
        pushSyncQueue(KDSyncData.XIANGQU_DST,
            SyncEventType.ATTR_UPDATE, aList);
      }
    } else {
      /**final SyncMqEvent event = new SyncMqEvent();
       final List<String> ids = new ArrayList<String>();
       ids.add(product.getId());
       event.setIds(ids);
       event.setEvent(1);
       event.setType(SyncEvType.EV_PRODUCT.ordinal());
       event.setTimestamp(new Date().getTime());
       syncRocketMq.sendToMQ(event);**/
    }
    return rc;
  }

  private void initImgSize(Product product) {
    log.info("init img size");
    if (product.getImg() != null) {
      final Image image = imageService.loadByImgKey(product.getImg());
      if (image != null) {
        product.setImgHeight(image.getHeight());
        product.setImgWidth(image.getWidth());
      }
    }
  }

  @Transactional
  void saveSkuMapping(ProductVO modProduct, Product product,
      List<SkuMapping> skuMappings) {
    // 保存sku
    int order = 1;
    if (modProduct != null) {
      skuMappingMapper.deleteByProductId(modProduct.getId());
    }

    if (skuMappings != null) {
      for (final SkuMapping skuMapping : skuMappings) {
        skuMapping.setOrder(order++);
        skuMapping.setProductId(product.getId());
        skuMappingMapper.insert(skuMapping);
      }
    }
  }

  @Transactional
  void saveSku(ProductVO modProduct, Product product, List<Sku> skus) {
    // 保存sku
    int order = 1;
    int cnt = 0;
    List<Long> ids = skuMapper.selectCopyProductId(product.getId());

    for (final Sku sku : skus) {
      checkMoney(sku.getPrice());
      sku.setOriginalPrice(sku.getPrice());
      sku.setOrder(order++);

      if (StringUtils.isBlank(sku.getId()) || modProduct == null) {
        sku.setProductId(product.getId());
        sku.setMarketPrice(sku.getPrice());
        cnt = skuMapper.insert(sku);
        log.info("insert sku cnt:[{}], sku:[{}]", cnt,
            JSON.toJSONString(sku));
        skuMapper.addCode(sku.getId());
      } else {
        if (ProductSource.NEWSPIDER.name().equals(product.getSource())) { // 不更新商品和sku价格
          sku.setPrice(null);
        }
        sku.setMarketPrice(sku.getPrice());
        sku.setOriginalPrice(sku.getPrice());
        int i = skuMapper.updateByPrimaryKeySelective(sku);
        if (i == 0) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "sku [" + sku.getSpec() + "]更新失败");
        }
        for (Long id : ids) {
          if(ids.size()!=0){
            sku.setMarketPrice(sku.getPrice());
            sku.setOriginalPrice(sku.getPrice());
            sku.setProductId(id.toString());
            sku.setSourceSkuCode(sku.getSkuCode());
            if(i!=0){
              Integer integer = skuMapper.updateCopyProductSku(sku);
              if(integer==0){
                log.info("该规格复制不存在:"+sku.getSkuCode());
              }
            }
          }
        }
      }
    }

    // // 比较该商品之前的sku与现在传入的sku，有哪些不在这次传入的sku中，说明是被删掉的sku
    if (modProduct != null) {
      final List<Sku> skusTmp = modProduct.getSkus();
      for (final Sku skuTmp : skusTmp) {
        boolean bInclude = false;
        for (final Sku sku : skus) {
          if (sku.getId().equals(skuTmp.getId())) {
            bInclude = true;
            break;
          }
        }
        if (!bInclude) {
          skuMapper.updateForArchive(skuTmp.getId());
        }
      }
    }
  }

  /**
   * 保存tags信息 在删除操作时，不直接删除，使用job任务定时清理
   */
  @Transactional
  protected void saveTags(Product modProduct, Product product, List<Tag> tags) {
    if (tags == null) {
      tags = new ArrayList<Tag>();
    }
    // 去重
    final Set<Tag> h = new LinkedHashSet<Tag>(tags);
    tags.clear();
    tags.addAll(h);
    //
    final List<ProductTag> pTags = new ArrayList<ProductTag>();
    Tag tagTmp = null;
    ProductTag pTag = null;

    final Shop shop = shopService.load(product.getShopId());
    // 先保存基础表中的tag
    for (final Tag tag : tags) {
      if (StringUtils.isBlank(tag.getId())) {
        tagTmp = tagMapper.selectByOwnerTagName(tag.getTag(),
            getCurrentUser().getId());
        if (tagTmp != null && StringUtils.isNotBlank(tagTmp.getId())) {
          tag.setId(tagTmp.getId());
        } else {
          if (shop != null && shop.getOwnerId() != null) {
            tag.setCreatorId(shop.getOwnerId());
          }
          tagMapper.insert(tag);
        }
      }
      pTag = new ProductTag();
      pTag.setProductId(product.getId());
      pTag.setShopId(product.getShopId());
      pTag.setTagId(tag.getId());
      pTags.add(pTag);
    }

    // 再保存关联表
    // 取出原标签
    List<ProductTag> modPTags = null;
    if (modProduct != null) {
      modPTags = productTagMapper.selectByProductId(product.getId());
    } else {
      modPTags = new ArrayList<ProductTag>();
    }
    for (final ProductTag pTag1 : pTags) {
      boolean bInclude = false;
      for (final ProductTag modPTag : modPTags) {
        if (modPTag.getTagId().equals(pTag1.getTagId())) {
          bInclude = true;
          break;
        }
      }
      if (!bInclude) {
        productTagMapper.insert(pTag1);
      }
    }

    // 删除已删除的原关联表
    for (final ProductTag pTagTmp : modPTags) {
      boolean bInclude = false;
      for (final ProductTag pTag1 : pTags) {
        if (pTagTmp.getTagId().equals(pTag1.getTagId())) {
          bInclude = true;
          break;
        }
      }
      if (!bInclude) {
        productTagMapper.updateForArchive(pTagTmp.getId());
      }
    }
  }

  /**
   * 保存商品图片信息 在删除操作时，不直接删除，使用job任务定时清理
   */
  /*
   * @Transactional // private void saveImgs(Product modProduct, Product
   * product, // List<ProductImage> imgs) { // if (imgs == null) { // imgs =
   * new ArrayList<ProductImage>(); // } // // 保存关联表 // List<ProductImage>
   * modImgs = null; // 取出原标签 // if (modProduct != null) { // modImgs =
   * productImageMapper.selectByProductId(product.getId()); // } else { //
   * modImgs = new ArrayList<ProductImage>(); // } // for (ProductImage img :
   * imgs) { // boolean bInclude = false; // for (ProductImage modImg :
   * modImgs) { // if (img.getImg().equals(modImg.getImg())) { // bInclude =
   * true; // break; // } // } // img.setProductId(product.getId()); // if
   * (!bInclude) { // productImageMapper.insert(img); // } else { //
   * productImageMapper.updateImgOrder(img); // } // } // // // 删除已删除的原关联表 //
   * for (ProductImage modImg : modImgs) { // boolean bInclude = false; // for
   * (ProductImage img : imgs) { // if (img.getImg().equals(modImg.getImg()))
   * { // bInclude = true; // break; // } // } // if (!bInclude) { //
   * productImageMapper.updateForArchive(modImg.getId()); // } // } // }
   *
   * private void saveImgs(Product modProduct, Product product,
   * List<ProductImage> imgs) { if (imgs == null) { imgs = new
   * ArrayList<ProductImage>(); } // 保存关联表 List<ProductImage> modImgs = null;
   * // 取出原标签 if (modProduct != null) { modImgs =
   * productImageMapper.selectByProductId(product.getId()); } else { modImgs =
   * new ArrayList<ProductImage>(); }
   *
   *
   * for (ProductImage img : imgs) { img.setProductId(product.getId());
   * boolean hit = false; int i = 0; for (ProductImage modImg : modImgs) { if
   * (img.getImg().equals(modImg.getImg())) { hit = true; if(img.getImgOrder()
   * != modImg.getImgOrder()) { // img mathc, order does not math // udpate
   * productImageMapper.updateImgOrder(img); } else { // img match, order
   * match // do nothing } // remove key modImgs.remove(i); break; } i++; } if
   * (!hit) { // img does not match // insert productImageMapper.insert(img);
   * } } if(modImgs.size() > 0) { for(ProductImage modImg : modImgs) {
   * productImageMapper.updateForArchive(modImg.getId()); } } }
   */
  private void saveImgsForSpider(Product product, List<ProductImage> imgs,
      Integer type) {
    if (imgs == null || product == null || type == null) {
      return;
    }
    if (type == 1 || type == 2) {
      productImageMapper.updateForArchiveByParam(product.getId(), 1);
      productImageMapper.updateForArchiveByParam(product.getId(), 2);
    } else if (type == 3) {
      productImageMapper.updateForArchiveByParam(product.getId(), 3);
    }
    for (final ProductImage img : imgs) {
      img.setProductId(product.getId());
      productImageMapper.insert(img);
    }
  }

  // update product img, do it like newspider, archive all and then insert all
  // FIXME 更新商品图片时会创建多张图片
  private void saveImgs(Product modProduct, Product product,
      List<ProductImage> imgs) {
    if (imgs == null || product == null) {
      return;
    }
    productImageMapper.updateForArchiveByParam(product.getId(), null);

    int idx = 0;
    for (final ProductImage img : imgs) {
      img.setProductId(product.getId());
      img.setImgOrder(idx++);
      productImageMapper.insert(img);
    }
  }

  private void saveTagsWithProduct(Product product, List<Tags> tagsToSave) {
    if (product == null || tagsToSave == null) {
      return;
    }
    for (final Tags tag : tagsToSave) {
      tagsService.saveWithRelation(product.getId(), tag);
    }
  }

  /**
   * 检查product和sku是否属于自己
   *
   * @param unionUserId 需要校验当前用户,则传入null
   */
  private boolean validOwner(ProductVO modProduct, Product product,
      List<Sku> skus, String unionUserId) {
    //FIXME 在接口封装,这是临时方案 dongsongjie
//		if (modProduct == null) {
//			log.warn("modProduct is null");
//			return false;
//		}
//
//		// 没有传递用户 ID 获取当前用户信息
//		if (StringUtils.isBlank(unionUserId)) {
//			//FIXME 在接口封装,这是临时方案 dongsongjie
// 			IUser iuser = getCurrentUser();
//			if (iuser instanceof User) {
//				unionUserId = getCurrentUser().getId();
//			} else {
//				Shop shop = shopService.load(iuser.getShopId());
//				unionUserId  = userService.load(shop.getOwnerId()).getId();
//			}
//		}
//
//		if (!modProduct.getUserId().equals(unionUserId)
//				|| !modProduct.getShopId().equals(product.getShopId())) {
//			log.warn(
//					"product user is not valid [unionUserId]={}, modeUser={}]",
//					unionUserId, modProduct.getUserId());
//			log.warn(
//					"product user is not valid [modProduct.shopId]={}, product.shopId={}]",
//					modProduct.getShopId(), product.getShopId());
//			return false;
//		}
//
//		String skuIds = "";
//		for (final Sku sku : modProduct.getSkus()) {
//			skuIds = skuIds + "," + sku.getId();
//		}
//
//		// 能否修改
//		for (final Sku sku : skus) {
//			if (StringUtils.isBlank(sku.getId())) {
//				continue;
//			}
//
//			if (!skuIds.contains(sku.getId())) {
//				log.warn("skuIds[{}] does not contains sku.id[{}]", skuIds,
//						sku.getId());
//				return false;
//			}
//		}

    return true;
  }

  /**
   * 计算产品的库存和最低价格
   */
  private void calProduct(Product product, List<Sku> skus) {
    log.info("Calc product min price and stock");
    product.setAmount(0);
    product.setPrice(BigDecimal.ZERO);
    int totalAmount = 0;
    for (final Sku sku : skus) {
      sku.setAmount(ObjectUtils.defaultIfNull(sku.getAmount(), 0));
      sku.setPrice(ObjectUtils.defaultIfNull(sku.getPrice(),
          BigDecimal.ZERO));
      if (product.getPrice() == BigDecimal.ZERO) {
        product.setPrice(sku.getPrice());
        product.setMarketPrice(product.getPrice());
      } else {
        product.setPrice(product.getPrice().min(sku.getPrice()));
        product.setMarketPrice(product.getPrice());
      }
      if (product.getOriginalPrice() == null) {
        product.setOriginalPrice(product.getPrice());
      }
      totalAmount += sku.getAmount();
    }
    product.setAmount(totalAmount);

    if (product.getRecommend() != null && product.getRecommend()) {
      product.setRecommendAt(Calendar.getInstance().getTime());
    }
  }

  /**
   * 计划发布
   */
  @Override
  public int forsale(String id, Date forsaleAt) {
    final Product product = load(id);
    if (!product.getUserId().equals(getCurrentUser().getId())
        || forsaleAt.after(new Date())) {
      return 0;
    }
    return productMapper.updateForForsale(id, forsaleAt);
  }

  /**
   * 上架
   */
  @Override
  public int onsale(String id) {
    final Product product = load(id);
    if (!product.getUserId().equals(getCurrentUser().getId())) {
      return 0;
    }

    if (syncSwitch == 0) {
      final List<Product> aList = new ArrayList<Product>();
      aList.add(product);
      pushSyncQueue(KDSyncData.XIANGQU_DST, SyncEventType.STATUS_ONSALE,
          aList);
    } else {
      /**final SyncMqEvent event = new SyncMqEvent();
       final List<String> ids = new ArrayList<String>();
       ids.add(product.getId());
       event.setIds(ids);
       event.setEvent(1);
       event.setType(SyncEvType.EV_PRODUCT.ordinal());
       event.setTimestamp(new Date().getTime());
       syncRocketMq.sendToMQ(event);**/
    }

    return productMapper.updateForOnsale(id);
  }

  /**
   * 下架
   */
  @Override
  public int instock(String id) throws Exception {
    final Product product = load(id);

    if (Boolean.TRUE.equals(product.getUpdateLock())) {
      throw new Exception("商品参与的促销活动活动正在进行中，商品不能编辑");
    }

//		if (Boolean.TRUE.equals(product.getIsDistribution()) && product.getSourceProductId()!= "") {
//			throw new Exception("分销商品不能编辑");
//		}

    if (!product.getUserId().equals(getCurrentUser().getId())) {
      throw new Exception("当前用户与卖家不匹配");
    }

    if (syncSwitch == 0) {
      final List<Product> aList = new ArrayList<Product>();
      aList.add(product);
      pushSyncQueue(KDSyncData.XIANGQU_DST, SyncEventType.STATUS_INSTOCK,
          aList);
    } else {
      /**final SyncMqEvent event = new SyncMqEvent();
       final List<String> ids = new ArrayList<String>();
       ids.add(product.getId());
       event.setIds(ids);
       event.setEvent(2);
       event.setType(SyncEvType.EV_PRODUCT.ordinal());
       event.setTimestamp(new Date().getTime());
       syncRocketMq.sendToMQ(event);**/
    }

    return productMapper.updateForInstock(id);
  }

  /**
   * spider调用下架 下架目前只判断快店商品是否与第三方有做id关联
   */
  @Override
  public int instockEX(String shopId, String thirdItemId) throws Exception {
    int ret = 0;
    final Long productId = selectProductIdByThirdItemId(shopId,
        BigInteger.valueOf(Long.parseLong(thirdItemId)));
    if (productId != null && productId > 0) {
      final Product product = load(IdTypeHandler.encode(productId));
      if (product == null) {
        log.info("not find product by productId[" + productId
            + "], maybe code is not exist.");
      }
      if (Boolean.TRUE.equals(product.getUpdateLock())) {
        throw new Exception(
            "activity is going on, can not edit product");
      }
      log.info("find product by shopId[" + shopId + "] and thirdItemId["
          + thirdItemId + "], start to instock product["
          + product.getId() + "]");
      ret = productMapper.updateForInstock(IdTypeHandler
          .encode(productId));
      if (ret > 0) { // 下架成功才同步到第三方
        if (syncSwitch == 0) {
          final List<Product> aList = new ArrayList<Product>();
          aList.add(product);
          pushSyncQueue(KDSyncData.XIANGQU_DST,
              SyncEventType.STATUS_INSTOCK, aList);
        } else {
          /**final SyncMqEvent event = new SyncMqEvent();
           final List<String> ids = new ArrayList<String>();
           ids.add(product.getId());
           event.setIds(ids);
           event.setEvent(2);
           event.setType(SyncEvType.EV_PRODUCT.ordinal());
           event.setTimestamp(new Date().getTime());
           syncRocketMq.sendToMQ(event);**/
        }
        log.info("success to instock product. id[" + productId + "]");
      } else {
        log.info("error to instock product. id[" + productId + "]");
      }
    } else {
      log.info("not find product by thirdItemId[" + shopId
          + "], thirdItemId[" + thirdItemId
          + "], can not instock product");
    }
    return ret;
  }

  @Override
  public int insert(Product e) {
    if (e == null) {
      return 0;
    }

    // 添加商品之前，查看商品的价格，是否合理
    checkMoney(e.getPrice());
    checkMoney(e.getOriginalPrice());
    checkMoney(e.getMarketPrice());
    if (e.getIsDistribution() != null && e.getIsDistribution().equals(true)) {
      return productMapper.insertDistribution(e);
    } else {
      return productMapper.insert(e);
    }

  }

  @Override
  public int insertOrder(Product e) {
    if (e == null) {
      return 0;
    }

    // 添加商品之前，查看商品的价格，是否合理
    checkMoney(e.getPrice());
    checkMoney(e.getOriginalPrice());
    checkMoney(e.getMarketPrice());
    if (e.getIsDistribution() != null && e.getIsDistribution().equals(true)) {
      return productMapper.insertDistribution(e);
    } else {
      return productMapper.insert(e);
    }
  }

  private void checkMoney(BigDecimal price) {
    if (price == null) {
      return;
    }

    if (String.valueOf(price).length() > 9) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品的价格"
          + price + "太长了，数据库存不下了");
    }

  }

  /**
   * 按上架时间
   */
  @Override
  public List<Product> listProductsByOnsaleAt(String shopId, String category, String isGroupon,
      Pageable page) {
    return listProductsByOnsaleAt(shopId, category, isGroupon, page,
        new HashMap<String, Object>());
  }

  @Override
  public List<Product> listAllProductsByOnsaleAt(String shopId,
      Pageable pageable) {
    return productMapper.listAllProductsByOnsaleAt(shopId, pageable);
  }

  @Override
  public List<Product> listProductsAvailableByChannel(String activityId,
      String shopId, Pageable page, String channel, String key) {
    if (StringUtils.isNotBlank(key)) {
      key = "%" + key + "%";
    } else {
      key = null;
    }
    return productMapper.listProductsAvailableByChannel(activityId, shopId,
        page, channel, key);
  }

  @Override
  public List<Product> listNoCodeProducts() {
    return productMapper.listNoCodeProducts();
  }

  @Override
  public Long countProductsAvailableByChannel(String activityId,
      String shopId, String channel, String key) {
    if (StringUtils.isNotBlank(key)) {
      key = "%" + key + "%";
    } else {
      key = null;
    }
    return productMapper.countProductsAvailableByChannel(activityId,
        shopId, channel, key);
  }

  @Override
  public List<Product> listProductsByActId(String actId, Pageable pageable) {
    final List<String> sortList = new ArrayList<String>();
    String sortStr = null;
    if (pageable.getSort() != null) {
      final Iterator<org.springframework.data.domain.Sort.Order> iter = pageable
          .getSort().iterator();
      while (iter.hasNext()) {
        final org.springframework.data.domain.Sort.Order o = iter
            .next();
        if (o.getProperty().equalsIgnoreCase(
            ProductListSortType.FORWARD.name())) {
          sortList.add("id desc");
        } else {
          sortList.add("price * commission_rate desc");
        }
      }
      if (sortList.size() > 0) {
        sortStr = StringUtils.join(sortList.toArray(), ",");
      }
    }

    return productMapper.listProductsByActId(actId, sortStr);
  }

  @Override
  public List<Product> listActivityProducts(Pageable pageable, int cateid, String userid) {
    String activity_id = IdTypeHandler.encode(0);
    if (userid != null) {
      List<Activity> aclist = activityMapper.listUserByQuey(userid);
      for (Activity activity : aclist) {
        activity_id = activity.getId();
      }
    }
    long acid = IdTypeHandler.decode(activity_id);
    final List<String> sortList = new ArrayList<String>();
    String sortStr = null;
    if (pageable.getSort() != null) {
      final Iterator<org.springframework.data.domain.Sort.Order> iter = pageable
          .getSort().iterator();
      while (iter.hasNext()) {
        final org.springframework.data.domain.Sort.Order o = iter
            .next();
        if (o.getProperty().equalsIgnoreCase(
            ProductListSortType.FORWARD.name())) {
          if (acid == 0) {
            sortList.add("id desc");
          } else {
            sortList.add("tb.id desc");
          }
        } else {
          if (acid == 0) {
            sortList.add("price * commission_rate desc");
          } else {
            sortList.add("tb.price * tb.commission_rate desc");
          }
        }
      }
      if (sortList.size() > 0) {
        sortStr = StringUtils.join(sortList.toArray(), ",");
      }
    }
    List<Product> plist;
    if (acid == 0) {
      if (cateid == 0) {
        plist = productMapper.listActivityProducts(pageable, sortStr);
      } else {
        plist = productMapper.listActivityByProductsCateid(pageable, sortStr, cateid);
      }
    } else {
      if (cateid == 0) {
        plist = productMapper.listCategoryActivityProducts(pageable, sortStr, activity_id);
      } else {
        plist = productMapper
            .listCategoryActivityByProductsCateid(pageable, sortStr, cateid, activity_id);
      }
    }
    for (Product pt : plist) {
      pt.setNewactivityId(acid);
    }
    return plist;
  }

  @Override
  public List<Product> listActivityProducts(Pageable pageable, int cateid) {
    final List<String> sortList = new ArrayList<String>();
    String sortStr = null;
    if (pageable.getSort() != null) {
      final Iterator<org.springframework.data.domain.Sort.Order> iter = pageable
          .getSort().iterator();
      while (iter.hasNext()) {
        final org.springframework.data.domain.Sort.Order o = iter
            .next();
        if (o.getProperty().equalsIgnoreCase(
            ProductListSortType.FORWARD.name())) {
          sortList.add("id desc");
        } else {
          sortList.add("price * commission_rate desc");
        }
      }
      if (sortList.size() > 0) {
        sortStr = StringUtils.join(sortList.toArray(), ",");
      }
    }
    if (cateid == 0) {
      return productMapper.listActivityProducts(pageable, sortStr);
    } else {
      return productMapper.listActivityByProductsCateid(pageable, sortStr, cateid);
    }
  }


  @Override
  public List<Product> listProductsByOnsaleAt(String shopId, String category, String isGroupon,
      Pageable page,
      Map<String, Object> params) {

    return productMapper.listProductsByOnsaleAt(shopId, category, isGroupon, page, params);


  }

  /**
   * 取最近days有发布的商品日期和每天发布商品中的数量， 以后此部分可以进客户缓存管理
   *
   * @param date 从哪一天开始
   * @param days 取几天
   */
  @Override
  public List<Map<String, Object>> listProductByRecently(String shopId,
      Date date, int days) throws ParseException {
    final List<Map<String, Object>> vosMap = productMapper
        .listProductByRecently(shopId, date, days);
    Map<String, Object> params = null;
    for (final Map<String, Object> voMap : vosMap) {
      final Date d = DateUtils.parseDate((String) voMap.get("date"),
          "yyyy-MM-dd");
      voMap.put("date", d);
      params = new HashMap<String, Object>();
      params.put("onsaleAt1", d);
      params.put("onsaleAt2", DateUtils.addDays(d, 1));
      voMap.put(
          "products",
          listProductsByOnsaleAt(shopId, "", "", new PageRequest(0, 12),
              params));
    }
    return vosMap;
  }

  /**
   * 按销量
   */
  @Override
  public List<Product> listProductsBySales(String shopId, String category, String isGroupon,
      Pageable page,
      Direction direction, String sellerShopId, List<String> subCategoryIds,
      String priceStart, String priceEnd, ProductStatus status, ProductReviewStatus reviewStatus,
      ProductSearchVO productSearchVO,ProductType... type) {
    if (Objects.isNull(productSearchVO))
      productSearchVO = new ProductSearchVO();
    return productMapper
        .listProductsBySales(shopId, category, isGroupon, page, direction, sellerShopId,
            subCategoryIds, priceStart, priceEnd, status, reviewStatus,
                productSearchVO.getSelfOperatedKey(),
                type);
  }

    /**
     * 按销量
     */
    @Override
    public List<Product> listProductsBySales(String shopId, String category, String isGroupon,
                                             Pageable page,
                                             Direction direction, String sellerShopId, List<String> subCategoryIds,
                                             String priceStart, String priceEnd, ProductStatus status, ProductReviewStatus reviewStatus,
                                             ProductType... type) {
        return productMapper
                .listProductsBySales(shopId, category, isGroupon, page, direction, sellerShopId,
                        subCategoryIds, priceStart, priceEnd, status, reviewStatus,
                        0,
                        type);
    }

  @Override
  public List<Product> listFilterBySpec(String order,
      Direction direction, Pageable pageable, Map<String, ?> params) {
    return productMapper.listFilterBySpec(order, direction, pageable, params);
  }

  @Override
  public Long countFilterBySpec(Map<String, ?> params) {
    return productMapper.countFilterBySpec(params);
  }

  /**
   * 按上架时间
   */
  @Override
  public List<Product> listProductsBySaleAt(String shopId, String category, String isGroupon,
      Pageable page,
      Direction direction, String sellerShopId, List<String> subCategoryIds,
      String priceStart, String priceEnd, ProductSearchVO productSearchVO, ProductType... type) {
    if (Objects.isNull(productSearchVO))
      productSearchVO = new ProductSearchVO();
    return productMapper
        .listProductsBySaleAt(shopId, category, isGroupon, page, direction, sellerShopId,
            subCategoryIds, priceStart, priceEnd,
                productSearchVO.getSelfOperatedKey(),type);

  }

  @Override
  public List<Product> listProductsByCm(String shopId, String category, String isGroupon,
      Pageable page, Direction direction, String sellerShopId) {
    return productMapper
        .listProductsByCm(shopId, category, isGroupon, page, direction, sellerShopId);
  }

  /**
   * 按发布时间
   */
  @Override
  public List<Product> listProductsByOnsaleDate(String shopId, Pageable page,
      Direction direction) {
    return productMapper.listProductsByOnsaleDate(shopId, page, direction);

  }

  @Override
  public Long countProductsBySales(String shopId) {
    return productMapper.countProductsBySales(shopId);
  }

  /**
   * 按库存
   */
  @Override
  public List<Product> listProductsByAmount(String shopId, String category, String isGroupon,
      Pageable page,
      Direction direction, ProductType type, ProductStatus status,
      ProductReviewStatus reviewStatus) {

    return productMapper
        .listProductsByAmount(shopId, category, isGroupon, page, direction, type, status,
            reviewStatus);

  }

  /**
   * 下架商品
   */
  @Override
  public List<Product> listProductsBySoldout(String shopId, String category, String isGroupon,
      Pageable page,
      Direction direction, ProductType type, ProductStatus status,
      ProductReviewStatus reviewStatus) {

    return productMapper
        .listProductsBySoldout(shopId, category, isGroupon, page, direction, type, status,
            reviewStatus);


  }

  /**
   * 相关商品
   */
  @Override
  public List<Product> listProductsByRelated(String shopId, String id,
      Pageable page) {
    return productMapper.listProductsByRelated(shopId, id);
  }

  @Override
  public List<Product> listProductsByRecommend(String shopId, Pageable page) {
    log.debug("123");
    return productMapper.listProductsByRecommend(shopId, page);
  }

  @Override
  public Long countByRecommend(String shopId) {
    return productMapper.countByRecommend(shopId);
  }

  @Override
  public Long countProductsByStatus(String shopId, ProductStatus status) {
    return productMapper.countProductsByStatus(shopId, status);
  }

  /**
   * modify by zzd 新增分页功能 商品查找
   */
  @Override
  public List<Product> search(@Deprecated String shopId, String key, Pageable page,
      ProductType type) {
    if (StringUtils.isNotEmpty(key)) {
      key = "%" + key + "%";
    }
    return productMapper
        .listProductsBySearch(shopId, key, page, type);
  }

  @Override
  public List<Product> selfSupport(@Deprecated String shopId, Pageable page, ProductType type) {
    return productMapper.listProductsBySelfSupport(shopId, page, type);
  }

  @Override
  public List<Product> searchWithNoStatus(String shopId, String key, Pageable page,
      ProductType type, String productId, String category) {
    if (StringUtils.isNotEmpty(key) && key.equals("null")) {
      key = null;
    }
    if (StringUtils.isNotEmpty(key)) {
      key = "%" + key + "%";
    }
    return productMapper
        .listProductsBySearchWithNoStatus(shopId, key, page, type, productId, category);
  }

  /**
   * modify by zzd 新增分页功能 商品查找
   */
  @Override
  public List<Product> searchAll(String shopId, String key, Pageable page) {
    if (StringUtils.isNotEmpty(key)) {
      key = "%" + key + "%";
    }
    return productMapper
        .listProductsBySearchAll(shopId, key, page);
  }

  /**
   * modify by zzd 新增分页功能 商品查找
   */
  @Override
  public List<Product> searchByPrice(@Deprecated String shopId, String key, Pageable page,
      String direction,
      ProductType type) {
    if (StringUtils.isNotEmpty(key)) {
      key = "%" + key + "%";
    }
    return productMapper
        .listProductsBySearchAndPrice(shopId, key, page, direction, type);
  }

  @Override
  public List<Product> selfSupportByPrice(@Deprecated String shopId, Pageable page, String direction, ProductType type) {
    return productMapper.listProductsBySelfSupportAndPrice(shopId, page, direction, type);
  }

  @Override
  public List<Product> search(String shopId, String key, ProductType type) {
    // TODO pager
    return productMapper
        .listProductsBySearch(shopId, "%" + key + "%", null, type);
  }

  private List<String> productsToList(List<Product> list) {
    final List<String> ret = new ArrayList<String>();
    if (list == null) {
      return ret;
    }

    for (final Product p : list) {
      ret.add(p.getId());
    }
    return ret;
  }

  /**
   * 定时任务
   */
  @Override
  public int autoOnSaleByTask() {

    final List<Product> aList = productMapper.getForsaleNow();

//    if (syncSwitch == 0) {
    // if (false) {
//      for (final Product p : aList) { // 暂时不支持列表
//        final List<Product> list = new ArrayList<Product>();
//        list.add(p);
//        pushSyncQueue(KDSyncData.XIANGQU_DST,
//            SyncEventType.STATUS_ONSALE, list);
//      }
    // pushSyncQueue(KDSyncData.XIANGQU_DST,
    // SyncEventType.STATUS_ONSALE, aList);
//    } else if (aList.size() != 0) {
    /**final SyncMqEvent event = new SyncMqEvent();
     final List<String> ids = new ArrayList<String>();
     ids.addAll(productsToList(aList));
     event.setIds(ids);
     event.setEvent(1);
     event.setType(SyncEvType.EV_PRODUCT.ordinal());
     event.setTimestamp(new Date().getTime());
     syncRocketMq.sendToMQ(event);**/
//    }
    return productMapper.autoOnSaleByTask();
  }

  /**
   * 商品删除 逻辑删除
   */
  @Override
  public int delete(String id) {
    final Product product = load(id);

    if (product == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "商品不存在");
    }
    if (!product.getUserId().equals(getCurrentUser().getId())) {
      log.error("您要删除的商品所属人" + product.getUserId() + "不是当前用户"
          + getCurrentUser().getId() + "所有");
      return 0;
    }
    if (product.getUpdateLock() != null && product.getUpdateLock()) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "活动中锁定的商品不能删除");
    }

    final int affectNum = productMapper.updateForArchive(id);
    if (affectNum > 0) {
      // 商品删除归为商品下架事件
      if (syncSwitch == 0) {
        // if (false) { // 线上开启不常用接口测试mq同步功能
        final List<Product> aList = new ArrayList<Product>();
        aList.add(product);
        pushSyncQueue(KDSyncData.XIANGQU_DST,
            SyncEventType.STATUS_INSTOCK, aList);
      } else {
        /**final SyncMqEvent event = new SyncMqEvent();
         final List<String> ids = new ArrayList<String>();
         ids.add(product.getId());
         event.setIds(ids);
         event.setEvent(3); // 商品删除事件
         event.setType(SyncEvType.EV_PRODUCT.ordinal());
         event.setTimestamp(new Date().getTime());
         syncRocketMq.sendToMQ(event);**/
      }
    }

    // 删除商品相关的活动申请
    // activityService.removeCampaignAndTicketByProductId(id);
    // 删除商品相关的分类
    categoryService.removeProductCategory(id, null);
    return affectNum;
  }

  /**
   * 恢复商品
   */
  @Override
  public int undelete(String id) {
    final Product product = load(id);
    if (!product.getUserId().equals(getCurrentUser().getId())) {
      return 0;
    }
    final int affectNum = productMapper.updateForUnArchive(id);

    if (affectNum > 0 && ProductStatus.ONSALE.equals(product.getStatus())) {
      // 商品从删除状态恢复，并且商品状态为ONSALE的归为商品上架事件
      if (syncSwitch == 0) {
        final List<Product> aList = new ArrayList<Product>();
        aList.add(product);
        pushSyncQueue(KDSyncData.XIANGQU_DST,
            SyncEventType.STATUS_ONSALE, aList);
      } else {
        /**final SyncMqEvent event = new SyncMqEvent();
         final List<String> ids = new ArrayList<String>();
         ids.add(product.getId());
         event.setIds(ids);
         event.setEvent(1);
         event.setType(SyncEvType.EV_PRODUCT.ordinal());
         event.setTimestamp(new Date().getTime());
         syncRocketMq.sendToMQ(event);**/
      }
    }
    return affectNum;
  }

  @Override
  public List<Product> listProductsByStatusDraft(String shopId, String category, String isGroupon,
      Pageable page, ProductType type, ProductStatus status,
      ProductReviewStatus reviewStatus) {

    return productMapper
        .listProductsByStatusDraft(shopId, category, isGroupon, page, type, status, reviewStatus);

  }

  @Override
  public List<Product> listProductsByForSale(String shopId, Pageable page) {
    return productMapper.listProductsByForSale(shopId, page);
  }

  @Override
  public List<Product> listProductsByOutOfStock(String shopId, String category, String isGroupon,
      Pageable page, ProductType type, ProductStatus status,
      ProductReviewStatus reviewStatus) {

    return productMapper
        .listProductsByOutOfStock(shopId, category, isGroupon, page, type, status, reviewStatus);


  }

  @Override
  public List<Product> listProductsByDelay(String shopId, String category, String isGroupon,
      Pageable page, ProductType type, ProductStatus status,
      ProductReviewStatus reviewStatus) {

    return productMapper
        .listProductsByDelay(shopId, category, isGroupon, page, type, status, reviewStatus);


  }

  @Override
  public Long countProductsByOutofStock(String shopId) {
    return productMapper.countProductsByOutofStock(shopId);
  }

  /**
   * 后台删除 逻辑删除
   */
  @Override
  public int deleteByAdmin(String[] ids) {
    return productMapper.deleteByAdmin(ids);
  }

  private ProductVO productToVo(Product product) {
    if (product == null) {
      return null;
    }

    final ProductVO vo = new ProductVO(product);

    final List<Sku> newSkus = new ArrayList<Sku>();

    FreshManProductVo freshManProductVo = freshManService.selectFreshmanProductById(product.getId());
    if(Objects.isNull(freshManProductVo)){
      // 不追溯源商品
      final List<Sku> skus = skuMapper.selectByProductId(product.getId());
      for (final Sku sku : skus) {
        // if (sku.getAmount() > 0) {
        newSkus.add(sku);
        // }
      }
    }else {
      //新人专区
      List<Sku> skuList = this.freshManService.selectFreshManSku(product.getId());
      for(final Sku sku : skuList){
        newSkus.add(sku);
      }
    }

    vo.setSkus(newSkus);

    // 获取skuMap
    vo.setSkuMappings(skuMappingToVO(product.getId(), newSkus));

    // 标签
    final List<Tag> tags = tagMapper.selectByProductId(product.getId());
    vo.setTags(tags);

    //商品收藏
    String userId = null;
    try {
      userId = getCurrentUser().getId();
    } catch (Exception e) {
      log.warn("商品service获取不到当前用户");
    }
    ProductCollection collection = collectionService
        .selectByProductId(userId, product.getId());
    vo.setCollected(null != collection);

    // 图片
    final List<ProductImageVO> imgsVO = new ArrayList<ProductImageVO>();
    final List<ProductImage> imgs ;
    if(null!= product.getSourceId()){
      String productCode = IdTypeHandler.encode(product.getSourceId());
      imgs = productImageMapper.selectByProductId(productCode);

    }else {
      imgs = productImageMapper.selectByProductId(product.getId());
    }
    for (final ProductImage i : imgs) {
      final ProductImageVO iVO = new ProductImageVO();
      iVO.setImg(i.getImg());
      imgsVO.add(iVO);
    }
    vo.setImgs(imgsVO);

    final CategoryVO categoryVO = categoryService.loadCategoryByProductId(product.getId());
    vo.setCategory(categoryVO);
    final List<CategoryVO> categorysVO = categoryService.loadCategorysByProductId(product.getId());
    vo.setCategorys(categorysVO);
    vo.setImgUrl(vo.getImg());

    return vo;
  }

  @Override
  public ProductVO loadByAdmin(String id) {
    final Product product = productMapper.selectByAdmin(id);
    return productToVo(product);
  }

  @Override
  public int undeleteByAdmin(String[] ids) {
    return productMapper.undeleteByAdmin(ids);
  }

  @Override
  public List<ProductAdmin> listProductsByAdmin(Map<String, Object> params,
      Pageable page) {
    return productMapper.listProductsByAdmin(params, page);
  }

  @Override
  public Long countProductsByAdmin(Map<String, Object> params) {
    return productMapper.countProductsByAdmin(params);
  }

  @Override
  public void updateSkuStock(String skuId, Integer amount) {
    log.info("udpate sku[" + skuId + "] stock delta[" + amount + "]");
    if (skuMapper.updateAmount(skuId, amount) <= 0) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "商品规格[" + skuId
          + "]库存不足");
    }
    final Sku sku = skuMapper.selectByPrimaryKey(skuId);
    updateProductStatusOnSkuStockChange(sku.getProductId());
  }

  @Override
  public void updateAmountForRefund(String productId, Integer amount) {
    log.info("udpate product[" + productId + "] stock delta[" + amount
            + "]");
    if (productMapper.updateAmountForRefund(productId, amount) <= 0) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "商品[" + productId
              + "]库存不足");
    }
  }

  /**
   * 检查商品的sku的库存是不是都是到0，如果是
   */
  private void updateProductStatusOnSkuStockChange(String productId) {
    final Product product = productMapper.selectByAdmin(productId);
    final List<Sku> skus = listSkus(product);
    int total = 0;
    if (skus != null) {
      for (final Sku sku : skus) {
        total += sku.getAmount();
      }
    }
    final List<Product> aList = new ArrayList<Product>();
    aList.add(product);

    // 应该下架，商品状态为在售
    if (total <= 0 && ProductStatus.ONSALE.equals(product.getStatus())) {
      // 商品库存为0
      // updateStock(productId, 0);
      // 商品下架
      productMapper.updateForInstock(productId);

      if (syncSwitch == 0) {
        pushSyncQueue(KDSyncData.XIANGQU_DST,
            SyncEventType.STATUS_INSTOCK, aList);
      } else {
        /**final SyncMqEvent event = new SyncMqEvent();
         final List<String> ids = new ArrayList<String>();
         ids.add(product.getId());
         event.setIds(ids);
         event.setEvent(2);
         event.setType(SyncEvType.EV_PRODUCT.ordinal());
         event.setTimestamp(new Date().getTime());
         syncRocketMq.sendToMQ(event);**/
      }
    }

    // 应该上架，商品状态为下架
/*    if (total > 0 && ProductStatus.INSTOCK.equals(product.getStatus())) {
      // 商品库存为0
      // updateStock(productId, 0);
      // 商品下架
      productMapper.updateForOnsale(productId);

      if (syncSwitch == 0) {
        pushSyncQueue(KDSyncData.XIANGQU_DST,
            SyncEventType.STATUS_ONSALE, aList);
      } else {
        *//**final SyncMqEvent event = new SyncMqEvent();
     final List<String> ids = new ArrayList<String>();
     ids.add(product.getId());
     event.setIds(ids);
     event.setEvent(1);
     event.setType(SyncEvType.EV_PRODUCT.ordinal());
     event.setTimestamp(new Date().getTime());
     syncRocketMq.sendToMQ(event);**//*
      }
    }*/
  }

  @Override
  public void updateStock(String productId, Integer amount) {
    log.info("udpate product[" + productId + "] stock delta[" + amount
        + "]");
    if (productMapper.updateAmountAndSales(productId, amount) <= 0) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "商品[" + productId
          + "]库存不足");
    }

    final Product product = productMapper.selectByPrimaryKey(productId);
    if (product != null) {

      if (syncSwitch == 0) {
        final List<Product> aList = new ArrayList<Product>();
        aList.add(product);
        pushSyncQueue(KDSyncData.XIANGQU_DST,
            SyncEventType.ATTR_UPDATE, aList);
      } else {
        /**final SyncMqEvent event = new SyncMqEvent();
         final List<String> ids = new ArrayList<String>();
         ids.add(product.getId());
         event.setIds(ids);
         event.setEvent(1);
         event.setType(SyncEvType.EV_PRODUCT.ordinal());
         event.setTimestamp(new Date().getTime());
         syncRocketMq.sendToMQ(event);**/
      }
    }
  }

  @Override
  public BigDecimal loadCommissionRate(String productId, String unionId) {
    BigDecimal rate = BigDecimal.ZERO;
    final Product product = productMapper.selectByAdmin(productId);
    if (StringUtils.isNotBlank(unionId)) {
      final Map<String, Object> params = new HashMap<String, Object>();
      params.put("unionId", unionId);
      params.put("auditSts", "PASS");
      params.put("shopId", product.getShopId());
      final List<ProdSync> commissionRates = prodSyncMapper.findByParmas(
          params, null);
      if (commissionRates != null && commissionRates.size() == 1) {
        rate = commissionRates.get(0).getCommissionRate();
      }
    }
    if (rate.compareTo(BigDecimal.ZERO) == 1) {
      return rate;
    } else {
      if (product.getCommissionRate() != null
          && product.getCommissionRate().compareTo(BigDecimal.ZERO) == 1) {
        return product.getCommissionRate();
      } else {

        final Shop shop = shopService.load(product.getShopId());
        if (shop.getCommisionRate() != null
            && shop.getCommisionRate().compareTo(BigDecimal.ZERO) == 1) {
          return shop.getCommisionRate();
        } else {
          return BigDecimal.ZERO;
        }
      }
    }
  }

  @Override
  public List<Product> listProducts(String shopId) {
    return productMapper.selectAll(shopId);
  }

  @Override
  public List<Product> listRootProducts(String shopId) {
    ShopTree ShopTree = shopTreeMapper.selectRootShopByShopId(shopId);
    String rootShopId = ShopTree.getRootShopId();
    return productMapper.selectAll(rootShopId);
  }

  @Override
  public Boolean instockByAdmin(String[] ids) {
    return productMapper.instockByAdmin(ids);
  }

  @Override
  public Long getLastTotalCnt(String shopId, String category, String isGroupon, String catType,
      Map<String, Object> params) {
    return productMapper.selectLastCnt(shopId, category, isGroupon, catType, params);
  }


  @Override
  public void setFakeSales(String id, int count) {
    productMapper.updateFakeSales(id, count);
  }

  @Override
  public List<Sku> listSkus(String productId) {
    final Product product = productMapper.selectByPrimaryKey(productId);
    if (product == null
        || !ProductStatus.ONSALE.equals(product.getStatus())) {
      return new ArrayList<Sku>();
    }
    return skuMapper.selectByProductId(productId);
  }

  @Override
  public List<Sku> listSkusOrig(String productId) {
    final Product product = productMapper.selectByPrimaryKey(productId);
    if (product == null) {
      return Collections.emptyList();
    }
    return skuMapper.selectByProductId(productId);
  }

  @Override
  public List<Sku> listSkus(Product product) {
    return skuMapper.selectByProductId(product.getId());
  }

  @Override
  public Long countDelayProduct(String shopId) {
    return productMapper.countDelayProduct(shopId);
  }

  @Override
  public List<Product> listDelayProduct(String shopId, Pageable pageable) {
    return productMapper.listDelayProduct(shopId, pageable);
  }

  /**
   * 创建产品，并自动汇总Sku中的库存总额和最低的价格，到产品主表上
   *
   * @param product setUserId(unionUserId)
   */
  @Override
  @Transactional
  public int createByUnion(Product product, List<Sku> skus, List<Tag> tags,
      List<ProductImage> imgs) {
    if (product.getStatus() == ProductStatus.FORSALE
        && product.getForsaleAt() == null) {
      // 发布计划没有发布时间
      return 0;
    }
    if (product.getStatus() == ProductStatus.ONSALE) {
      product.setOnsaleAt(Calendar.getInstance().getTime());
    }
    // 设置延迟发布默认值
    if (product.getDelayed() == null) {
      product.setDelayed(new Integer(0));
    }
    // 设置推荐的默认值
    if (product.getRecommend() == null) {
      product.setRecommend(Boolean.FALSE);
    }
    calProduct(product, skus);

    // 设置首张图片尺寸
    initImgSize(product);

    final int rc = insert(product);
    log.debug("start save sku...");
    saveSku(null, product, skus); // 保存sku
    log.debug("start save tags...");
    saveTags(null, product, tags); // 保存tags信息
    log.debug("start save imgs...");
    saveImgs(null, product, imgs); // 保存商品图片

    if (syncSwitch == 0) {
      final List<Product> aList = new ArrayList<Product>();
      aList.add(product);
      pushSyncQueue(KDSyncData.XIANGQU_DST, SyncEventType.STATUS_ONSALE,
          aList);
    } else {
      /**final SyncMqEvent event = new SyncMqEvent();
       final List<String> ids = new ArrayList<String>();
       ids.add(product.getId());
       event.setIds(ids);
       event.setEvent(1);
       event.setType(SyncEvType.EV_PRODUCT.ordinal());
       event.setTimestamp(new Date().getTime());
       syncRocketMq.sendToMQ(event);**/
    }
    return rc;
  }

  /**
   * 更新产品 操作流程： 1.检查被修改的产品和Sku是否属于操作者本身 2.并自动汇总Sku中的库存总额和最低的价格，到产品主表上
   * 3.如果产品库存为0，则自动下架，并记录下架时间，如果从0改为>0的值，则自动上架，并记录上架时间 4.删除图片 5.保存更新sku 6.删除被标志为删除的sku
   *
   * @param product setUserId(unionUserId)
   */
  @Override
  @Transactional
  public int updateByUnion(Product product, List<Sku> skus, List<Tag> tags,
      List<ProductImage> imgs) {
    // 检查product和sku是否属于自己
    final ProductVO modProduct = load(product.getId());
    // if (!validOwner(modProduct, product, skus, product.getUserId())) {
    // return 0;
    // }

    if (product.getStatus() == ProductStatus.ONSALE
        && modProduct.getStatus() != ProductStatus.ONSALE) {
      product.setOnsaleAt(Calendar.getInstance().getTime());
    }

    // 计算产品的库存和最低价格
    calProduct(product, skus);
    // 设置首张图片尺寸
    initImgSize(product);

    final int rc = productMapper.updateByPrimaryKeySelective(product);

    saveSku(modProduct, product, skus); // 保存sku

    saveTags(modProduct, product, tags); // 保存tags信息
    saveImgs(modProduct, product, imgs); // 保存商品图片

    final List<Product> aList = new ArrayList<Product>();
    aList.add(product);
    return rc;
  }

  @Override
  public int synchronous(String ids, String sourceCodes) {
    return productMapper.synchronousSource(ids, sourceCodes);
  }

  @Override
  public List<String> obtainDbSynchronous(List<String> ids) {
    return productMapper.obtainDbSynchronous(ids);
  }

  @Override
  public List<Product> listProductsByPostAge(String shopId, String category, String isGroupon,
      Pageable page,
      Direction direction, ProductType type, ProductStatus status,
      ProductReviewStatus reviewStatus) {
    if (shopId == null) {
      return null;
    }

    int from = 0;
    int to = 20;
    Map<String, Object> params = new HashMap();
    params.put("type", type);
    params.put("status", status);
    params.put("reviewStatus", reviewStatus);
    final List<Product> list = listProductsByOnsaleAt(shopId, category, isGroupon, null,
        params); // 这里不能分页, 对结果手动分页
    final ShopPostAge sp = shopPostAgeService.getPostAgeByShop(shopId);
    final List<Product> frees = new ArrayList<Product>();
    final List<Product> others = new ArrayList<Product>();

    // 手动处理分页... sucks TODO:
    if (page != null) {
      from = page.getOffset();
      to = page.getOffset() + page.getPageSize();
      if (from >= list.size()) {
        return new ArrayList<Product>();
      }
      if (list.size() - to < page.getPageSize()) {
        to = list.size();
      }
    }

    if (sp != null && sp.getFreeShippingGoods() != null) {
      for (final Product p : list) {
        if (sp.getFreeShippingGoods().contains(p.getId())) {
          p.setFreePostage(true);
          frees.add(p);
        } else {
          p.setFreePostage(false);
          others.add(p);
        }
      }
    } else if (sp.getFreeShippingGoods() == null) {
      return list.subList(from, to);
    }

    if (!Direction.DESC.equals(direction)) {
      others.addAll(frees);
      return others.subList(from, to);
    } else {
      frees.addAll(others);
      return frees.subList(from, to);
    }
  }

  // 获取活动下商品列表 (xquark_campaign_product)
  @Override
  public List<Product> listProductByActivityId(String activityId) {
    return productMapper.listProductByActivityId(activityId);
  }

  @Override
  public List<Product> selectActivityBegin(String shopId) {
    return productMapper.selectAll(shopId);
  }

  @Override
  public List<Product> selectActivityEnd(String shopId) {
    return productMapper.selectAll(shopId);
  }

  @Override
  public int updateEx(Product product) {
    try {
      final int rc = 0;
      // 判断是否有thirdItemId关联
      final Long productId = selectProductIdByThirdItemId(
          product.getShopId(), product.getThirdItemId());
      if (productId != null && productId > 0) {
        log.info("find product by shopId[" + product.getShopId()
            + "] and thirdItemId[" + product.getThirdItemId()
            + "], ignore update product[" + productId + "]"
            + " updateSwitch:" + updateSwitch);
        product.setId(IdTypeHandler.encode(productId));
        return 0;
      }
      return rc;
    } catch (final Exception e) {
      e.printStackTrace();
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "Error to sync KKKD from spider");
    }
  }

  /**
   * spider的调用逻辑调用更新 判断product是否已存在，存在则更新，不存在则重新创建 一重判断itemid，二重判断名称是否一致 2015-03-16
   */
  @Override
  public int updateEx(Product product, List<Sku> skus, List<Tag> tags,
      List<ProductImage> imgs, List<SkuMapping> skuMapping, Integer option) {
    try {
      int rc = 0;
      // 判断是否有thirdItemId关联
      final Long productId = selectProductIdByThirdItemId(
          product.getShopId(), product.getThirdItemId());
      // new spider api may remove ouer_user_id parameter
      product.setUserId(shopService.load(product.getShopId())
          .getOwnerId());
      // option:0 只新建商品, option:1 新建并更新商品
      if (option == null || option == 1) {
        if (productId != null) {
          log.info("start to create product third_item_id{}",
              product.getThirdItemId());
          rc = create(product, skus, tags, null, imgs, skuMapping);
        } else {
          if (updateSwitch == 1) {
            rc = updateEx(productId, product, skus, tags, imgs,
                skuMapping);
          } else {
            product.setId(IdTypeHandler.encode(productId));
            log.info(
                "sync ignore shop:[{}], product:[{}], third_item_id:[{}], sync_option:[{}], updateSwitch:[{}]",
                product.getShopId(), productId,
                product.getThirdItemId(), option, updateSwitch);
          }
        }
      } else {
        if (productId == null) {
          log.info("start to create product...{}",
              product.getThirdItemId());
          rc = create(product, skus, tags, null, imgs, skuMapping);
        } else {
          product.setId(IdTypeHandler.encode(productId));
          log.info(
              "sync ignore shop:[{}], product:[{}], third_item_id:[{}], sync_option:[{}], updateSwitch:[{}]",
              product.getShopId(), productId,
              product.getThirdItemId(), option, updateSwitch);
        }
      }
      return rc;
    } catch (final Exception e) {
      e.printStackTrace();
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "Error to sync KKKD from spider");
    }
  }

  private int updateEx(Long productId, Product product, List<Sku> skus,
      List<Tag> tags, List<ProductImage> imgs, List<SkuMapping> skuMapping) {
    int rc = 0;

    final String sProductId = IdTypeHandler.encode(productId);
    // 设置productID
    product.setId(sProductId);
    if (skus != null) {
      for (final Sku sku : skus) {
        sku.setProductId(sProductId);
      }
    }
    if (skuMapping != null) {
      for (final SkuMapping skuMap : skuMapping) {
        skuMap.setProductId(sProductId);
      }
    }
    if (imgs != null) {
      for (final ProductImage productImage : imgs) {
        productImage.setProductId(sProductId);
      }
    }

    // 检查product和sku是否属于自己
    final ProductVO modProduct = load(sProductId);
    if (modProduct == null) {
      log.warn("modproduct not found id:{}, enId:{}", productId,
          sProductId);
      return 0;
    } else {
      rc = update(modProduct, product, skus, tags, imgs, skuMapping,
          false);
    }

    return rc;
  }

  // 根据thirdItemId返回商品id
  private Long selectProductIdByThirdItemId(String shopId,
      BigInteger thirdItemId) {
    final Long productId = productMapper.selectProductIdByThirdItemId(
        shopId, thirdItemId);

    return productId;
  }

  @Override
  public List<ProductImage> requestImgs(String productId, String type) {
    return productImageMapper.getProductImgs(productId, type);
  }

  @Override
  public List<Product> selectByIds(String[] ids) {
    return productMapper.selectByIds(ids);
  }

  @Override
  public List<Sku> findSkusByIds(String... ids) {
    if (ArrayUtils.isEmpty(ids)) {
      return Collections.emptyList();
    }

    return skuMapper.selectByIds(ids);
  }

  @Override
  public List<Sku> findByProductId(String productId) {
    if (StringUtils.isBlank(productId)) {
      return Collections.emptyList();
    }

    return skuMapper.selectByProductId(productId);
  }

  @Override
  public void lockProductByShopId(String shopId) {
    if (shopId == null) {
      return;
    }
    productMapper.lockProductByShopId(shopId);
  }

  @Override
  public void unlockProductByShopId(String shopId) {
    productMapper.unlockProductByShopId(shopId);
  }

  private String[] list2arrayByProduct(List<Product> list) {
    if (list == null || list.size() == 0) {
      return new String[0];
    }
    final String[] array = new String[list.size()];
    for (int i = 0; i < list.size(); i++) {
      array[i] = list.get(i).getId();
    }
    return array;
  }

  @Override
  public void unlockProductByShopIdEx(String shopId) {
    // get this shop all activity products
    final List<Product> listIds = productMapper
        .listAllActivityProdsByShop(shopId);
    if (listIds == null || listIds.size() == 0) {
      return;
    }
    productMapper.unlockProductByIdsRevs(list2arrayByProduct(listIds),
        shopId);
  }

  @Override
  public void unlockProductByIds(List<String> list) {
    if (list == null || list.size() == 0) {
      return;
    }
    productMapper.unlockProductByIds(list.toArray(new String[list.size()]));
  }

  @Override
  public List<Product> listProductsByPrice(String shopId, String category, String isGroupon,
      Pageable page,
      Direction direction, String sellerShopId, List<String> subCategoryIds,
      String priceStart, String priceEnd, ProductSearchVO productSearchVO, ProductType... type) {
    if (Objects.isNull(productSearchVO))
      productSearchVO = new ProductSearchVO();
    return productMapper
        .listProductsByPrice(shopId, category, isGroupon, page, direction, sellerShopId,
            subCategoryIds, priceStart, priceEnd, productSearchVO.getSelfOperatedKey(),type);

  }

  @Transactional
  @Override
  public boolean updateReviewStatus(@NotBlank String id, @NotNull ProductReviewStatus status) {
    return productMapper.updateReviewStatus(id, status) > 0;
  }

  @Override
  public Long CountTotalByName(@Deprecated String shopId, String key) {
    return productMapper.CountTotalByName(shopId, "%" + key + "%");
  }

  @Override
  public Long CountTotalBySelfSupport(@Deprecated String shopId) {
    return productMapper.CountTotalBySelfSupport(shopId);
  }

  @Override
  public Long CountTotalByNameWithNoStatus(@Deprecated String shopId, String key, String productId, String category) {
    if (StringUtils.isNotEmpty(key) && key.equals("null")) {
      key = null;
    }
    if (StringUtils.isNotEmpty(key)) {
      key = "%" + key + "%";
    }
    return productMapper.CountTotalByNameWithNoStatus(shopId, productId, key, category);
  }

  @Override
  public void addCode(String... ids) {
    if (ids == null || ArrayUtils.isEmpty(ids)) {
      return;
    }

    for (final String p : ids) {
      productMapper.addCode(p);
    }
  }

  // 层级太多
  @Override
  public void clear4noneProductByShopId(String shopId) {
    try {
      int ret = 0;
      // not in cart_item
      final List<Product> products = productMapper
          .selectUnCartProdByShop(shopId);
      for (final Product product : products) {
        // exist in order_item
        if (productMapper.countOrderProdById(product.getId()) > 0 ? false
            : true) {
          // exist in xiangqu.comment
          if (productMapper.countCommentProdById(product.getId()) > 0 ? false
              : true) {
            // exist in xiangqu.faver
            if (productMapper.countFaverProdById(product.getId()) > 0 ? false
                : true) {
              // delete
              ret = productMapper.clear4noneProduct(product
                  .getId());
              if (ret > 0) {
                log.info("success to clear 4none products. shopId:"
                    + shopId);
              }
            } else {
              continue;
            }
          } else {
            continue;
          }
        } else {
          continue;
        }
      }
    } catch (final Exception e) {
      log.error(e.getMessage());
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "error to clear 4none products.");
    }
  }

  @Override
  public int updateEnableDesc(Product p) {
    if (p == null) {
      return 0;
    }
    return productMapper.updateByPrimaryKeySelective(p);
  }

  /**
   * for spider
   */
  @Override
  public void updateSpecImg(Product product, List<ProductImage> imgs) {
    // 判断是否有thirdItemId关联
    final Long productId = selectProductIdByThirdItemId(
        product.getShopId(), product.getThirdItemId());
    if (productId != null && productId > 0) { // update desc img
      if (updateSwitch == 1) {
        product.setId(IdTypeHandler.encode(productId));
        saveImgsForSpider(product, imgs, 3); // 保存商品图片
      }
    } else { // insert desc img
      saveImgs(null, product, imgs); // 保存商品图片
    }
  }

  @Override
  public List<Product> listProducts4Sync(String shopId, Pageable page) {
    // TODO Auto-generated method stub
    return productMapper.listProducts4Sync(shopId, page);
  }

  @Override
  public long countAllShopProduct(String shopId) {

    return productMapper.countAllShopProduct(shopId);
  }

  @Override
  public List<ProductAdmin> listProds2ActivityByAdmin(
      Map<String, Object> params, Pageable page) {
    return productMapper.listProds2ActivityByAdmin(params, page);
  }

  @Override
  public Long countProds2ActivityByAdmin(Map<String, Object> params) {
    return productMapper.countProds2ActivityByAdmin(params);
  }

  @Override
  public List<ProductVO> listCrossProductsBySales(String shopId,
      Pageable page, Direction desc) {
    List<ProductVO> prodList = new ArrayList<ProductVO>();
    List<Product> proLi = productMapper.listCrossProductsBySales(shopId, page, desc);
    if (null != proLi && proLi.size() > 0) {
      for (Product product : proLi) {
        ProductVO pvo = productToVo(product);
        if (null != pvo.getSkus() && pvo.getSkus().size() > 0) {
          for (Sku sku : pvo.getSkus()) {
            if (SkuCodeResourcesType.getSkuCodeResourceBySupplierCode("SENMIAO").orElseThrow(RuntimeException::new).getName().equals(sku.getSkuCodeResources())) {
              prodList.add(pvo);
              break;
            }
          }
        }
      }
    }
    return prodList;
  }

  @Override
  public List<Activity> listActivity(Map<String, Object> params, Pageable page) {
    return activityMapper.newlistActivitysByQuery(params, page);
  }

  @Override
  public long countAllactivity(Map<String, Object> params) {
    return activityMapper.newcountActivitysByQuery(params);
  }


  @Override
  public int createActivity(Map<String, String> params) {
    int count = activityMapper.existsName(params.get("name")).intValue();
    if (count == 0) {
      return activityMapper.createActivity(params);
    } else {
      return -1;
    }
  }

  @Override
  public int deleteActivity(String activityid) {
    int ret = 0;
    ret = activityMapper.delete(activityid, null);
    if (ret > 0) {
      List<Shop> shoplist = shopMapper.listShopIdsByActivity(activityid);
      for (Shop shop : shoplist) {
        ret += shopMapper.activityUpdated(shop.getId(), IdTypeHandler.encode(0));
      }
      return ret;
    }
    return 0;
  }

  @Override
  public List<CategoryActivity> listCategorybyActivity(Map<String, Object> params, Pageable page,
      String activityId) {
    return categoryActivityMapper.listCategorybyActivity(params, page, activityId);
  }

  @Override
  public int createCategoryActivity(Map<String, String> params) {
    return categoryActivityMapper.createCategory(params);
  }

  @Override
  public int deleteCategoryActivity(String id) {
    return categoryActivityMapper.deleteCategory(id);
  }

  @Override
  public int deleteCategoryActivityRelation(String id) {
    return categoryActivityMapper.deleteCategoryRelation(id);
  }

  @Override
  public Long countCategorybyActivity(String activityId) {
    return categoryActivityMapper.countCategorybyActivity(activityId);
  }

  @Override
  public List<ProductAdmin> listProductsByCategory(Map<String, Object> params, Pageable page,
      String activityId, String categoryId) {
    return productMapper.listProductsByCategory(params, page, activityId, categoryId);
  }

  @Override
  public Long countProductsByCategory(Map<String, Object> params, String activityId,
      String categoryId) {
    return productMapper.countProductsByCategory(params, activityId, categoryId);
  }

  @Override
  public Long existsNameforCategoryActivity(Map<String, String> params) {
    return categoryActivityMapper.existsName(params);
  }

  @Override
  public Product addDistributionProduct(Product productSrc, String insertOrUpdate) {
    Product product = new Product();
    String insert = StringUtils.defaultIfBlank(insertOrUpdate, "insert").toLowerCase();
    String productId = productSrc.getId();
    BeanUtils.copyProperties(productSrc, product);
    product.setId("");

    product.setSourceProductId(productSrc.getId());
    product.setSourceShopId(productSrc.getShopId());
    product.setIsDistribution(true);
    product.setUserId(userService.getCurrentUser().getId());
    Shop shop = shopService.mine();
    product.setShopId(shop.getId());
    List<Sku> skus = skuMapper.selectByProductId(productId);
    List<SkuMapping> skuMapping = skuMappingMapper.selectByProductId(productId);
    List<ProductImage> imgs = productImageMapper.selectByProductId(productId);
    List<Tag> tags = tagMapper.selectByProductId(productId);
    product.setImg(null != imgs && imgs.size() > 0 ? imgs.get(0).getImg() : null);
    if (productSrc.getEnableDesc() == null) {
      product.setEnableDesc(false);
    }
    product.setSource(ProductSource.CLIENT);
    if ("insert".equals(insert)) {
      create(product, skus, tags, null, imgs, skuMapping);
    } else {
      update(product, skus, tags, imgs, skuMapping);
    }
    List<ProductFragment> listProductFragment = productFragmentService.selectByProductId(productId);
    if (listProductFragment != null) {
      for (int i = 0; i < listProductFragment.size(); i++) {
        ProductFragment productFragmentCopy = new ProductFragment();
        ProductFragment productFragment = listProductFragment.get(i);
        BeanUtils.copyProperties(productFragment, productFragmentCopy);
        productFragmentCopy.setId("");
        productFragmentCopy.setProductId(product.getId());
        productFragmentService.insert(productFragmentCopy);
        Fragment fragmentCopy = new Fragment();
        FragmentVO fragmentVO = fragmentService.selectById(productFragment.getFragmentId());
        BeanUtils.copyProperties(fragmentVO, fragmentCopy);
        fragmentCopy.setId("");
        fragmentCopy.setShopId(shop.getId());
        fragmentService.insert(fragmentCopy);
        List<FragmentImageVO> listFragmentImageVO = fragmentVO.getImgs();
        if (listFragmentImageVO != null) {
          for (int j = 0; j < listFragmentImageVO.size(); j++) {
            FragmentImageVO fragmentImageVO = listFragmentImageVO.get(i);
            FragmentImage fragmentImageCopy = new FragmentImage();
            BeanUtils.copyProperties(fragmentImageVO, fragmentImageCopy);
            fragmentImageCopy.setId("");
            fragmentImageService.insert(fragmentImageCopy);
          }
        }

      }
    }
    return product;
  }

  @Override
  public List<Product> listDistributeProductsBySales(String rootShopId, String categoryId,
      String shopId, Pageable pageable) {
    return productMapper.listDistributeProductsBySales(rootShopId, categoryId, shopId, pageable);
  }

  @Override
  public List<ProductCardsVO> listProductWithCards(String order, Direction direction,
      Pageable pageable) {
    return productMapper.listProductWithCards(order, direction, pageable);
  }

  @Override
  public boolean deleteDiscountProduct(String productId) {
    return productMapper.deleteDiscountProduct(productId) > 0;
  }

  @Override
  public Long countProductWithCards() {
    return productMapper.countProductWithCards();
  }

  @Override
  public void batchDelete(String[] ids) {
    for (String id : ids) {
      if (StringUtils.isNotEmpty(id) && !"undefined".equals(id)) {
        delete(id);
      }
    }
  }

  /**
   * 自动下架
   */
  @Override
  public int autoInstockByTask() {
    //注释自动下架 by yarm
    //return productMapper.autoInstockByTask();
    return 0;
  }

  @Override
  public List<Product> listGiftProduct(String category, String order, Direction direction,
      Pageable pageable) {
    return productMapper.listGiftProduct(category, order, direction, pageable);
  }

  @Override
  public Long countGiftProduct(String category) {
    return productMapper.countGiftProduct(category);
  }

  @Override
  public String selectConcatName(String[] ids, Boolean giftOnly) {
    return productMapper.selectConcatNameByIds(ids, giftOnly);
  }

  /**
   * 获取某个类别下商品总数
   */
  @Override
  public Long countCategoryProducts(String shopId, String category) {
    return productMapper.countCategoryProducts(shopId, category);
  }

  /**
   * 找到某个商品对应的sku的所有属性值
   */
  @Override
  public List<String> findAttributeByProductId(String productId) {
    return skuMapper.findAttributeByProductId(productId);
  }

  @Override
  public Integer getScaleOrUseGlobal(String productId) {
    if (!exists(productId)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品不存在");
    }
    Integer scale = this.loadYundouScaleById(productId);
    if (scale == null || scale == 0) {
      scale = yundouSettingService.getGlobalSetting().getAmount();
    }
    return scale;
  }

  @Override
  public Integer getScaleOrUseGlobal(@NotNull Product product) {
    Integer scale = product.getYundouScale();
    if (scale == null || scale == 0) {
      scale = yundouSettingService.getGlobalSetting().getAmount();
    }
    return scale;
  }

  @Override
  public Integer loadYundouScaleById(String id) {
    return productMapper.selectYundouScaleById(id);
  }


  /**
   * 根据分类过滤条件，统计该过滤条件下总商品数
   */
  @Override
  public Long countProductsByCategoryAndPrice(String shopId, String category,
      List<String> subCategoryIds, String priceStart, String priceEnd,
      ProductType... type) {
    return productMapper
        .countProductsByCategoryAndPrice(shopId, category, subCategoryIds, priceStart, priceEnd,
            type);
  }

  @Override
  public boolean bindUserIfNotBounded(@NotNull ProductUser productUser) {
    String productId = productUser.getProductId();
    String userId = productUser.getUserId();
    if (productUserMapper.selectIsBounded(productId, userId)) {
      return true;
    }
    return productUserMapper.insert(productUser) > 0;
  }

  @Override
  public boolean unBindUserIfBounded(@NotNull ProductUser productUser) {
    String productId = productUser.getProductId();
    String userId = productUser.getUserId();
    if (!productUserMapper.selectIsBounded(productId, userId)) {
      return true;
    }
    return productUserMapper.deleteBounded(productId, userId) > 0;
  }

  @Override
  public List<ProductBasicVO> listBoundedProductsWithFilters(String userId, Pageable pageable) {
    List<ProductBasicVO> results = this.listBoundedProducts(userId, pageable);
    for (ProductBasicVO basicVO : results) {
      String id = basicVO.getId();
      List<? extends FilterBasicVO> filters = listFilterSkuByProductId(id);
      basicVO.setFilters(filters);
    }
    return results;
  }

  @Override
  public List<ProductBasicVO> listBoundedProducts(String userId, Pageable pageable) {
    return productUserMapper.listBoundedProducts(userId, pageable);
  }

  @Override
  public Long countBoundedProducts(String userId) {
    return productUserMapper.countBoundedProducts(userId);
  }

  @Override
  public boolean bindFilterIfNotBounded(@NotNull ProductFilter productFilter) {
    String productId = productFilter.getProductId();
    String filterId = productFilter.getFilterId();
    checkArgument(productMapper.selectIsProduct(productId), "商品已删除或类型不正确");
    checkArgument(!productMapper.selectIsProduct(filterId), "滤芯已删除或类型不正确");
    if (productFilterMapper.selectIsBounded(productId, filterId)) {
      return Boolean.TRUE;
    }
    return productFilterMapper.insert(productFilter) > 0;
  }

  @Override
  public boolean isFilterBounded(String productId, String filterId) {
    return productFilterMapper.selectIsBounded(productId, filterId);
  }

  @Override
  @Transactional
  public boolean bindFilterIfNotBounded(final String productId,
      @NotNull List<String> filterIds) {
    this.unBindFilterAll(productId);
    if (CollectionUtils.isEmpty(filterIds)) {
      return true;
    }
    for (String filterId : filterIds) {
      ProductFilter pf = new ProductFilter(productId, filterId);
      if (!this.bindFilterIfNotBounded(pf)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "绑定滤芯失败");
      }
    }
    return true;
  }

  @Override
  public boolean unBindFilterIfBounded(@NotNull ProductFilter productFilter) {
    String productId = productFilter.getProductId();
    String filterId = productFilter.getFilterId();
    if (!productFilterMapper.selectIsBounded(productId, filterId)) {
      return true;
    }
    return productFilterMapper.deleteBounded(productId, filterId) > 0;
  }

  @Override
  @Transactional
  public boolean unBindFilterIfBounded(String productId, List<String> filterIds) {
    if (CollectionUtils.isEmpty(filterIds)) {
      return true;
    }
    for (String filterId : filterIds) {
      ProductFilter pf = new ProductFilter(productId, filterId);
      if (!this.unBindFilterIfBounded(pf)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "解除绑定滤芯失败");
      }
    }
    return true;
  }

  @Override
  public boolean unBindFilterAll(String productId) {
    return productFilterMapper.deleteByProductId(productId) > 0;
  }

  @Override
  public List<String> boundedFilterIds(String productId) {
    List<Long> selectedFilterIds = productFilterMapper.selectBoundedFilterIds(productId);
    return Transformer.fromIterable(selectedFilterIds, idHandlerConvertFunction());
  }

  @Override
  public List<FilterBasicVO> listFilterByProductId(String productId, Map<String, ?> params,
      Pageable pageable) {
    return productMapper.listFilterByProductId(productId, pageable, params);
  }

  @Override
  public List<FilterBasicVO> listFilterByProductId(String productId) {
    return this.listFilterByProductId(productId, null, null);
  }

  @Override
  public List<? extends FilterBasicVO> listFilterSkuByProductId(String productId,
      Map<String, ?> params,
      Pageable pageable) {
    return Transformer.fromIterable(listFilterByProductId(productId, params, pageable),
        new Function<FilterBasicVO, FilterBasicSkuVO>() {
          @Override
          public FilterBasicSkuVO apply(FilterBasicVO filterBasicVO) {
            FilterBasicSkuVO skuVO = new FilterBasicSkuVO(filterBasicVO);
            final List<Sku> skus = skuMapper.selectByProductId(skuVO.getId());
            skuVO.setSkuList(skus);
            return skuVO;
          }
        });
  }

  @Override
  public List<? extends FilterBasicVO> listFilterSkuByProductId(String productId) {
    return listFilterSkuByProductId(productId, null, null);
  }

  @Override
  public List<Product> listFilterByProductModel(String model, Pageable pageable) {
    return productMapper.listFilterByProductModel(model, pageable);
  }

  @Override
  public Long countFilterByProductId(String productId, Map<String, ?> params) {
    return productMapper.countFilterByProductId(productId, params);
  }

  @Override
  public Future<Object> getCRMProductInfo(String code) {
    return null;
  }

  /**
   * 根据商品id查找商品编码 可能出现无对应编码的情况
   */
  @Override
  public Optional<String> selectEncodeById(String id) {
    return Optional.fromNullable(productMapper
        .selectEncodeById(id));
  }

  @Override
  public String selectIdByEncode(String encode) {
    Long id = productMapper.selectIdByEncode(encode);
    if (id == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "编码对应的商品不存在");
    }
    return IdTypeHandler.encode(id);
  }

  @Override
  public Boolean selectIsModelExists(String model) {
    return productMapper.selectExistsByModel(model);
  }

  @Override
  public boolean updatePriority(String id, Integer priority) {
    return productMapper.updatePriority(id, priority) > 0;
  }

  @Override
  public List<ProductBasicVO> listProductWithParams(Map<String, Object> params, Pageable pageable) {
    String keyword = (String) params.get("keyword");
    if (StringUtils.isNoneBlank(keyword) && !keyword.startsWith("%")) {
      params.put("keyword", "%" + keyword + "%");
    }
    return productMapper.listProductWithParams(pageable, params);
  }

  /**
   * 获取商品以及该商品的选择信息
   *
   * @param params 搜索参数
   * @param selectProvider 获取某个商品是否被选中
   * @param pageable 分页参数
   * @return 结果
   */
  @Override
  public List<ProductSelectVO> listProductWithSelectInfo(final String excludePromotionId,
      Map<String, Object> params,
      final Function<PdPromotionInner, Boolean> selectProvider,
      Pageable pageable) {
    List<ProductBasicVO> basicResults = listProductWithParams(params, pageable);
    if (CollectionUtils.isEmpty(basicResults)) {
      return Collections.emptyList();
    }
    return Transformer.fromIterable(basicResults,
        new Function<ProductBasicVO, ProductSelectVO>() {
          @Override
          public ProductSelectVO apply(ProductBasicVO productBasicVO) {
            String pId = productBasicVO.getId();
            return new ProductSelectVO(productBasicVO,
                selectProvider.apply(new PdPromotionInner(pId, excludePromotionId)));
          }
        });
  }

  @Override
  public Long countProductWithParams(Map<String, Object> params) {
    String keyword = (String) params.get("keyword");
    if (StringUtils.isNoneBlank(keyword) && !keyword.startsWith("%")) {
      params.put("keyword", "%" + keyword + "%");
    }
    return productMapper.countWithParams(params);
  }

  @Override
  public boolean selectThirdProductExists(Long id, ProductSource source) {
    return productMapper.selectThirdExists(id, source);
  }

  @Override
  public Product selectThirdProduct(Long thirdId, ProductSource source) {
    return productMapper.selectThirdProduct(thirdId, source);
  }

  @Override
  public Product selectThirdProductByEncode(String encode, ProductSource source) {
    return productMapper.selectThirdProductByEncode(encode, source);
  }

  @Override
  public List<String> listSFProductEncode() {
    return productMapper.listSFProductEncode();
  }

  @Override
  public List<WmsProduct> getWmsProduct() {
    List<WmsProduct> list = productMapper.getWmsProduct();
    for (WmsProduct w : list) {
      w.setUpdatedBy("汉薇商城");
      w.setEdiStatus(0);
      if (w.getWareHouseId() == null) {
        w.setWareHouseId(1);
      }
    }
    return list;
  }

  @Override
  public Boolean updateByCode(WmsInventory wmsInventory) {
    return productMapper.updateByCode(wmsInventory) > 0;
  }

  /**
   * 推送导入产品分支
   * @param inputStream
   * @return
   */
  @Override
  @Transactional
  public Integer importExcelToInsert(InputStream inputStream) {
    String[][] line = null;
    try {
      line = ExcelUtils.excelImport(inputStream);
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (line == null || line.length == 0) {
      throw new RuntimeException("excelData is null");
    }
    List<Product> products=new ArrayList<>();
    List<Category> categories=categoryService.getNameAndId();
    List<Brand> brands=brandMapper.getNameAndId();
    List<WareHouse> wareHouses=wareHouseMapper.getNameAndId();
    List<BrandProduct> brandProducts=new ArrayList<>();
    List<TermRelationship> termRelationships=new ArrayList<>();
    String errorMessage="";
    try {
      for (int j=1;j<line.length;j++) {
        Product product=new Product();
        Product originalProduct = new Product();
        BrandProduct brandProduct=new BrandProduct();
//        TermRelationship termRelationship=new TermRelationship();
        for (int i=0;i<line[j].length;i++) {
          if(line[j][i]==null||line[j][i].equals("")){
            continue;
          }
          switch (i){
            case 0:
              //产品名称
              errorMessage="第"+(j+1)+"行第"+(i+1)+"列的产品名称出现错误";
              product.setName(line[j][i]);
              break;
            case 1:
              //产品型号
              errorMessage="第"+(j+1)+"行第"+(i+1)+"列的产品型号出现错误";
              product.setModel(line[j][i]);
              break;
            case 2:
              //SKU编码
              product.setSkuCode(line[j][i]);
              if (null != product.getSkuCode()) {
                  Long productId = productMapper.selectProductIdBySkuCode(product.getSkuCode());
                  if (null != productId) {
                      originalProduct = productMapper.selectProductByProductId(productId);
                  } else {
                      new Product();
                  }
              } else {
                  errorMessage="第"+(j+1)+"行第"+(i+1)+"列的SKU编码出现错误";
              }
              break;
            case 3:
              //产品条形码
              errorMessage="第"+(j+1)+"行第"+(i+1)+"列的产品条形码出现错误";
              product.setBarCode(line[j][i]);
              break;
            case 4:
              //商品类型
              errorMessage="第"+(j+1)+"行第"+(i+1)+"列的商品类型出现错误";
              if(line[j][i]==null){
                line[j][i]="";
              }
              product.setKind(line[j][i]);
              break;
            case 5:
              //商品长度
              errorMessage="第"+(j+1)+"行第"+(i+1)+"列的商品长度出现错误";
              product.setLength(Double.parseDouble(line[j][i]));
              if (product.getLength().intValue() < 0) {
                  log.error("导出商品的excel内容出错," + errorMessage);
                  throw new RuntimeException(errorMessage);
              }
              break;
            case 6:
              //商品宽度
              errorMessage="第"+(j+1)+"行第"+(i+1)+"列的商品宽度出现错误";
              product.setWidth(Double.parseDouble(line[j][i]));
              if (product.getWidth().intValue() < 0) {
                  log.error("导出商品的excel内容出错," + errorMessage);
                  throw new RuntimeException(errorMessage);
              }
              break;
            case 7:
              //商品高度
              errorMessage="第"+(j+1)+"行第"+(i+1)+"列的商品高度出现错误";
              product.setHeight(Double.parseDouble(line[j][i]));
              if (product.getHeight().intValue() < 0) {
                  log.error("导出商品的excel内容出错," + errorMessage);
                  throw new RuntimeException(errorMessage);
              }
              break;
            case 8:
              //商品重量
              errorMessage="第"+(j+1)+"行第"+(i+1)+"列的商品重量出现错误";
              product.setWeight(Integer.parseInt(line[j][i]));
              if (product.getWeight() < 0) {
                  log.error("导出商品的excel内容出错," + errorMessage);
                  throw new RuntimeException(errorMessage);
              }
              break;
            case 9:
              //商品装箱数
              errorMessage="第"+(j+1)+"行第"+(i+1)+"列的商品装箱数出现错误";
              product.setNumInPackage(Integer.parseInt(line[j][i]));
              if (product.getNumInPackage() < 0) {
                  log.error("导出商品的excel内容出错," + errorMessage);
                  throw new RuntimeException(errorMessage);
              }
              break;
            case 10:
              //是否支持退货（是/否）
              errorMessage="第"+(j+1)+"行第"+(i+1)+"列的是否支持退货出现错误";
              if(line[j][i].equals("是")){
                product.setSupportRefund(true);
              }else{
                product.setSupportRefund(false);
              }
              break;
            case 11:
              //库存
              errorMessage="第"+(j+1)+"行第"+(i+1)+"列的库存出现错误";
              product.setAmount(Integer.parseInt(line[j][i]));
              if (product.getAmount() < 0) {
                  log.error("导出商品的excel内容出错," + errorMessage);
                  throw new RuntimeException(errorMessage);
              }
              break;
            case 12:
              //安全库存
              errorMessage="第"+(j+1)+"行第"+(i+1)+"列的安全库存出现错误";
              product.setSecureAmount(Integer.parseInt(line[j][i]));
              if (product.getSecureAmount() < 0) {
                  log.error("导出商品的excel内容出错," + errorMessage);
                  throw new RuntimeException(errorMessage);
              }
              break;
            case 13:
              //售价
              errorMessage = "第" + (j + 1) + "行第" + (i + 1) + "列的售价出现错误";
              product.setPrice(new BigDecimal(line[j][i]));
              if (product.getPrice().intValue() < 0) {
                  log.error("导出商品的excel内容出错," + errorMessage);
                  throw new RuntimeException(errorMessage);
              }
              break;
            case 14:
              //兑换价
              errorMessage = "第" + (j + 1) + "行第" + (i + 1) + "列的兑换价出现错误";
              BigDecimal bigDecimal = new BigDecimal(line[j][i]);
              if (bigDecimal.intValue() < 0) {
                  log.error("导出商品的excel内容出错," + errorMessage);
                  throw new RuntimeException(errorMessage);
              } else {
                  if (null == product.getPrice()) {
                      bigDecimal = originalProduct.getPrice().subtract(bigDecimal);
                  } else {
                      bigDecimal = product.getPrice().subtract(bigDecimal);
                  }
              }
              bigDecimal = bigDecimal.multiply(new BigDecimal(10));
              bigDecimal = bigDecimal.setScale(0, BigDecimal.ROUND_HALF_UP);
              product.setDeductionDPoint(bigDecimal);
              break;
            case 15:
              //积分
              errorMessage = "第" + (j + 1) + "行第" + (i + 1) + "列的积分出现错误";
              product.setPoint(new BigDecimal(line[j][i]));
              if (product.getPoint().intValue() < 0) {
                  log.error("导出商品的excel内容出错," + errorMessage);
                  throw new RuntimeException(errorMessage);
              }
              break;
            case 16:
              //净值
              errorMessage = "第" + (j + 1) + "行第" + (i + 1) + "列的净值出现错误";
              product.setNetWorth(new BigDecimal(line[j][i]));
              if (product.getNetWorth().intValue() < 0) {
                  log.error("导出商品的excel内容出错," + errorMessage);
                  throw new RuntimeException(errorMessage);
              }
              break;
            case 17:
              //推广费
              errorMessage = "第" + (j + 1) + "行第" + (i + 1) + "列的推广费出现错误";
              product.setPromoAmt(new BigDecimal(line[j][i]));
              if (product.getPromoAmt().intValue() < 0) {
                  log.error("导出商品的excel内容出错," + errorMessage);
                  throw new RuntimeException(errorMessage);
              }
              break;
            case 18:
              //服务费
              errorMessage = "第" + (j + 1) + "行第" + (i + 1) + "列的服务费出现错误";
              product.setServerAmt(new BigDecimal(line[j][i]));
              if (product.getServerAmt().intValue() < 0) {
                  log.error("导出商品的excel内容出错," + errorMessage);
                  throw new RuntimeException(errorMessage);
              }
              break;
          }
        }
        if(product.getSkuCode()==null||product.getSkuCode().equals("")){
          break;
        }
        System.out.println(product);
        Shop shop = shopService.loadRootShop();
        String userId = shop.getOwnerId();
        product.setUserId(userId);
        product.setShopId(shop.getId());
        product.setStatus(ProductStatus.DRAFT);
        product.setDetailH5("");
        product.setSales(0);
        product.setSource(ProductSource.CLIENT);
        //2019-02-21:新增导入产品时添加默认的运费模版
        product.setTemplateValue(String.valueOf(4500));
//        brandProduct.setProductId(productMapper.getIdByCode(product.getCode()));
//        brandProduct.setProductId(product.getCode());
//        termRelationship.setObjId(productMapper.getIdByCode(product.getCode()));
//        termRelationship.setObjId(product.getCode());
//        if(product.getKind()==null){
//          product.setKind("");
//        }
        products.add(product);
        if(products.size() > 1000){
          errorMessage = "导入商品数量不能超过1000行";
          throw new RuntimeException(errorMessage);
        }
        brandProducts.add(brandProduct);
//        termRelationships.add(termRelationship);
      }
      errorMessage="success";
      try {
        importExcel(products,null,brandProducts);
      }catch (RuntimeException e){
        log.error("导出商品的excel内容出错",e);
        errorMessage = e.getMessage();
        throw new RuntimeException(errorMessage);
      }
    }catch (Exception e){
      if(!errorMessage.equals("success")){
        log.error("导出商品的excel内容出错",e);
        throw new RuntimeException(errorMessage);
      }else{
        log.error("导出商品的excel内容出错",e);
        e.printStackTrace();
      }

    }
    return products.size();
  }

  @Override
  public List<Product> listProductsByDiscount(String rootShopId, String categoryId, String groupon, Pageable pageable, Direction direction, String sellerShopId, List<String> subCategoryIds, String priceStart, String priceEnd, ProductType type) {
    return productMapper
            .listProductsByDiscount(rootShopId, categoryId, groupon, pageable, direction, sellerShopId,
                    subCategoryIds, priceStart, priceEnd, type);
  }

  @Override
  public List<Product> listProductsByPoint(String rootShopId, String categoryId, String groupon,
      Pageable pageable, Direction direction, String sellerShopId, List<String> subCategoryIds,
      String priceStart, String priceEnd, ProductType... type) {
        return productMapper
            .listProductsByPoint(rootShopId, categoryId, groupon, pageable, direction, sellerShopId,
                    subCategoryIds, priceStart, priceEnd, type);
  }

  @Override
  public Integer productForSale(String productId, Date time) {
    return productMapper.productForSale(productId, time);
  }

  @Override
  public Integer productInstock() {
    List<Map<String, Object>> productList = productMapper.findInstock();
    List<String> idList = new ArrayList<>();
    StringBuffer buffer = new StringBuffer("商品 ");
    StringBuffer topic = new StringBuffer("商品下架通知(");

    int total = productList.size();
    int len = 0;
    for(Map<String,Object> map : productList){
      idList.add(map.get("id").toString());

      topic.append(map.get("skuCode").toString());
      buffer.append(map.get("name").toString());

      if(++len < total){
        buffer.append("、");
        topic.append("，");
      }
    }

    topic.append(")");

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    buffer.append(" 已于" + sdf.format(new Date()) + "下架");

    int count = 0;
    if (CollectionUtils.isNotEmpty(idList)) {
      count = productMapper.productInstock(idList);

      JavaMailWithAttachment se = new JavaMailWithAttachment(true);
      se.doSendHtmlEmail(topic.toString(), buffer.toString(), se.getSender_username(), null);
    }
    return count;
  }

  /**
   * 检查库存小于安全库存的sku, 并下架
   */
  @Override
  public Integer checkStockAndInStock() {
    List<InStockProdVO> inStockProds = productMapper.findInStockProd();
    StringBuilder buffer = new StringBuilder("商品 ");
    StringBuilder topic = new StringBuilder("商品下架通知(");

    List<String> prodIds = new ArrayList<>();
    int total = inStockProds.size();
    int len = 0;
    for(InStockProdVO prod : inStockProds){
      prodIds.add(prod.getId());

      topic.append(prod.getProdCode());
      buffer.append(prod.getName());
      if(++len < total){
        buffer.append("、");
        topic.append("，");
      }
    }

    topic.append(")");

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    buffer.append(" 已于").append(sdf.format(new Date())).append("下架");

    int count = 0;
    if (CollectionUtils.isNotEmpty(prodIds)) {
      count = productMapper.productInstock(prodIds);

      JavaMailWithAttachment se = new JavaMailWithAttachment(true);
      se.doSendHtmlEmail(topic.toString(), buffer.toString(), se.getSender_username(), null);
    }
    return count;
  }

  @Override
  public Integer updateProductAmountByMaxSkuAmount(@NotBlank  String productId) {
    return productMapper.updateProductAmountByMaxSkuAmount(productId);
  }

  private void saveBrand(BrandProduct brandProduct,List<Brand> brands,String brand,int j,int i,String errorMessage){
    i++;
    j++;
    for (Brand b:brands) {
      if(b.getName().equals(brand)){
        brandProduct.setBrandId(b.getId());
        return;
      }
    }
    errorMessage="第"+j+"行第"+i+"列的商品品牌不存在";
//    throw new RuntimeException();
  }

  private void saveTerm(TermRelationship termRelationship,List<Category> categories,String term,int j,int i,String errorMessage){
    j++;
    i++;
    for (Category c:categories) {
      if(c.getName().equals(term)){
        termRelationship.setCategoryId(c.getId());
        return;
      }
    }
    errorMessage="第"+j+"行第"+i+"列的商品分类不存在";
    throw new RuntimeException();
  }

  private void saveWare(Product product,List<WareHouse> wareHouses,String name,int j,int i,String errorMessage){
    j++;
    i++;
    for (WareHouse w:wareHouses) {
      if(w.getName().equals(name)){
        product.setWareHouseId(w.getId());
        return;
      }
    }
    errorMessage="第"+j+"行第"+i+"列的仓库不存在";
//    throw new RuntimeException();
  }

  public void importExcel(List<Product> products,List<TermRelationship> termRelationships,List<BrandProduct> brandProducts){
    List<String> skuCodeList = new ArrayList<>();
    for (Product p:products) {
      skuCodeList.add(p.getSkuCode());
      if(p.getName() != null || p.getSkuCode() != null){
        Long productId = productMapper.selectProductIdBySkuCode(p.getSkuCode());
        if(productId != null){
          p.setIdRaw(productId);
          productMapper.updateByExcel(p);
          productMapper.updateSkuByExcel(p.getAmount(),p.getSecureAmount(),p.getSkuCode(),p.getBarCode(),p.getPrice(),
              p.getDeductionDPoint(),p.getPoint(),p.getNetWorth(),p.getPromoAmt(),p.getServerAmt(), p.getLength(),
              p.getHeight(), p.getWidth(), p.getWeight(), p.getNumInPackage());
          //productMapper.updateCategoryRelationship(productId.toString());
        }else {
          if (p.getSkuCode().toUpperCase().startsWith("TZ"))
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                    "skuCode= "+ p.getSkuCode() + " 是套装商品,不能导入创建哦,请至套装页面添加...");
          if(p.getKind()==null){
            p.setKind("");
          }
          if(p.getAmount()==null){
            p.setAmount(0);
          }
          productMapper.insertByExcel(p);
          productMapper.updateProductCodeById(p.getId(),p.getIdRaw());
          productMapper.insertSkuByExcel(p.getSkuCode(),p.getBarCode(),p.getIdRaw(),p.getAmount(),
              p.getSecureAmount(),p.getPrice(),p.getDeductionDPoint(),p.getPoint(),
              p.getNetWorth(),p.getPromoAmt(),p.getServerAmt(), p.getLength(),
              p.getHeight(), p.getWidth(), p.getWeight(), p.getNumInPackage());
//          productMapper.insertCategoryRelationship(p.getId());
        }
      }else {
        throw new RuntimeException("商品名称和SKU编码不能为空");
      }
    }
    if(hasSame(skuCodeList) == false){
      throw new RuntimeException("SKU编码不能重复");
    }
//    List<String> tcodes=termRelationshipMapper.getProductCode();
//    for (TermRelationship t:termRelationships) {
//      t.setObjId(productMapper.getIdByCode(t.getObjId()));
//      if(tcodes.contains(t.getObjId())){
//        termRelationshipMapper.updateByCode(t);
//      }else {
//        termRelationshipMapper.insertExcel(t);
//      }
//    }
//    List<String> bcodes=brandProductMapper.getProductCode();
//    for (BrandProduct b:brandProducts) {
//      b.setProductId(productMapper.getIdByCode(b.getProductId()));
//      if(bcodes.contains(b.getProductId())){
//        brandProductMapper.updateByCode(b);
//      }else {
//        if(b.getBrandId()==null){
//          continue;
//        }
//        brandProductMapper.insertExcel(b);
//      }
//    }
  }

  private static boolean hasSame(List<? extends Object> list) {
    if(null == list)
      return false;
    return list.size() == new HashSet<Object>(list).size();
  }

  @Override
  public List<String> listHotSearchedKeys(int displayNum) {
    List<String> keys = productMapper.listHotSearchedKeys(displayNum);
    if (null == keys || keys.size() < 1) {
      keys = new ArrayList<>();
    }
    return keys;
  }

  @Override
  public Boolean checkHotsearchKeyExists(String key) {
    Integer count = productMapper.checkHotsearchKeyExists(key);
    return null != count && count > 0;
  }

  @Override
  public Long countHotSearchedProducts(String key) {
    String pids = productMapper.selectProductIdByKey(key);
    String[] pidArray = StringUtils.defaultIfEmpty(pids, "").split(",");
    return Long.valueOf(productMapper.countHotSearchedProducts(Arrays.asList(pidArray)));
  }

  @Override
  public List<Product> filterHotSearchedProducts(String key, String order, String direction, Pageable page, ProductSearchVO productSearchVO) {
    String pids = productMapper.selectProductIdByKey(key);
    String[] pidArray = StringUtils.defaultIfEmpty(pids, "").split(",");
    List<String> list = Arrays.asList(pidArray);
    //对pid再次过滤
    if(productSearchVO != null && list != null){
      //1.汉薇自营
      if(!Objects.isNull(productSearchVO.getSelfOperatedKey()) &&
              1 == productSearchVO.getSelfOperatedKey()){
        list = this.filterSelfOperated(list);
      }

      //2.是否有库存（现货）
      if(!Objects.isNull(productSearchVO.getStockKey()) &&
              1 == productSearchVO.getStockKey()){
        list = this.filterIsStock(list);
      }
    }
    if(list == null || list.isEmpty())
      return null;
    return productMapper.selectHotSearchedProducts(list, order, direction, page);
  }

  private List<String> filterIsStock(List<String> list) {
    if(list == null || list.isEmpty()){
      return list;
    }
    List<String> result = new ArrayList<>();
    Iterator<String> it = list.iterator();
    while (it.hasNext()){
      String productId = it.next();
      List<Sku> skuList = this.skuMapper.selectByProductId(productId);
      if(skuList == null || skuList.isEmpty()){
        continue;
      }
      //若是顺丰商品不用自己的库存校验
      boolean isSFProduct = this.checkIsSFProduct(IdTypeHandler.decode(productId));
      if(isSFProduct){
        continue;
      }

      boolean b = checkStockByProductIdAndSkuCode(productId,skuList);
      //无库存
      if(b)
        result.add(productId);
    }
    return result;
  }

  private List<String> filterSelfOperated(List<String> list) {
    if(list == null || list.isEmpty()){
      return list;
    }
    List<String> result = new ArrayList<>();
    Iterator<String> it = list.iterator();
    while(it.hasNext()){
      String productId = it.next();
      if(StringUtils.isBlank(productId))
        continue;
      //确认是否是汉薇自营
      boolean b = productMapper.checkIsSelfOperated(productId,"1");
      if(b){
        result.add(productId);
      }
    }
    return result;
  }

  private boolean checkStockByProductIdAndSkuCode(String productId, List<Sku> skuList) {
     Set<Boolean> bList = new HashSet<>();
    for(Sku sku : skuList){
      if(sku != null){
        String skuCode = sku.getSkuCode();
        boolean b = this.skuMapper.checkCommonProductStock(productId,skuCode);
        bList.add(b);
      }
    }
    if(bList == null || bList.isEmpty()){
      return false;
    }
    if(bList.size() == 1 &&  bList.contains(true)){
      return false;
    }
    //有库存
    return true;
  }

  @Override
  public Boolean setHotSearchKeyDisplayState(String id, Integer display) {
    return productMapper.updateDisplayState(id, display) > 0;
  }

  @Override
  public Boolean updateHotKeyState(String id, Integer state) {
    return productMapper.updateHotKeyState(id, state) > 0;
  }

  @Override
  public Boolean newHotSearchKey(String key, String pids) {
    Integer exits = productMapper.checkKeyExists(key, null);
    if (null == exits || exits < 1) {
      // 不存在则新建
      return productMapper.newHotSearchKey(key, pids) > 0;
    }
    //存在则返回
    return false;
  }

  @Override
  public Boolean removeHotSearchKeyByIds(List<String> ids) {
    if (CollectionUtils.isEmpty(ids)) {
      return true;
    }
    return productMapper.updateHotKeyArchive(ids) > 0;
  }

  @Override
  public HotSearchKey getHotSearchKeyById(String id) {
    HotSearchKey hsk = productMapper.getHotSearchKeyById(id);
    if (null == hsk) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "找不到指定Id的关键词");
    }

    if (null != hsk.getProductId() && hsk.getProductId().length() > 0) {
      String[] ids = hsk.getProductId().split(",");
      List<Product> pobjs = new ArrayList<>();
      for (String idstr : ids) {
        Product p = productMapper.selectByPrimaryKey(idstr);
        if (null != p) {
          pobjs.add(p);
        }
      }
      hsk.setProducts(pobjs);
    }
    return hsk;
  }

  @Override
  public Boolean updateHotSearchKey(String id, String key, String pids) {
    Integer exits = productMapper.checkKeyExists(key, id);
    if (null == exits || exits < 1) {
      // 不存在则新建
      return productMapper.updateHotSearchKey(id, key, pids) > 0;
    }
    return false;
  }

  @Override
  public Integer countTotalByKey(String key) {
    return productMapper.countTotalByKey("%"+key+"%");
  }

  @Override
  public List<HotSearchKey> listValidHotSearchKeys(String key, Pageable page) {
    String searchKey = null == key ? null : "%" + key + "%";
    List<HotSearchKey> keys = productMapper.findValidHotSearchedKeys(searchKey, page);
    for (HotSearchKey hsk : keys) {
      String[] pids = hsk.getProductId().split(",");
      List<Product> pdcts = new ArrayList<>();
      for (String pid : pids) {
        Product p = productMapper.selectByPrimaryKey(pid);
        if (null != p) {
          pdcts.add(p);
        }
      }
      hsk.setProducts(pdcts);
    }
    return keys;
  }

  @Override
  public Boolean updateHotSearchKeyDisplayLimit(String value) {
    return productMapper.updateHotSearchKeyDisplayLimit(value) > 0;
  }

  @Override
  public Integer findHotSearchKeyDisplayLimit() {
    String limit = productMapper.findHotSearchKeyDisplayLimit();
    if (null != limit && limit.length() > 0) {
      return Integer.parseInt(limit);
    }
    return 10;
  }

  @Override
  public List<Product> listProductsBySearchOrdered(@Deprecated String shopId, String key, Pageable page,
                                                   String direction,ProductType type, String order) {
    if (StringUtils.isNotEmpty(key)) {
      key = "%" + key + "%";
    }
    return productMapper.listProductsBySearchOrdered(shopId, key, page, direction, type, order);
  }

  @Override
  public List<Product> selfSupportByOrdered(@Deprecated String shopId, Pageable page, String direction,ProductType type, String order) {
    return productMapper.listProductsBySelfSupportAndOrdered(shopId, page, direction, type, order);
  }

  @Override
  public List<ProductBasicVO> listCombine(String productId, Pageable pageable) {
    return productMapper.listCombineProduct(productId, pageable);
  }

  @Override
  public Long countCombine(String productId) {
    return productMapper.countCombineProduct(productId);
  }

  @Override
  public List<ProductBasicVO> listUnCombine(String productId, Pageable pageable) {
    return productMapper.listUnCombineProduct(productId, pageable);
  }

  @Override
  public Long countUnCombine(String productId) {
    return productMapper.countUnCombineProduct(productId);
  }

  @Override
  public List<SkuCombineVO> listCombineSku(String skuId,
      Long categoryId, String supplierId, List<String> selectedIds,
      String keyword, Pageable pageable) {
    List<SkuCombineVO> ret = skuMapper
        .listCombine(skuId, categoryId, supplierId, selectedIds, keyword, pageable);
    if (StringUtils.isNotBlank(skuId)) {
      for (SkuCombineVO combine : ret) {
        // 如果用户绑定了数量则加载数量
        SkuCombineExtra extra = productCombineService
            .loadExtraBySkuIdAndSlaveId(skuId, combine.getId());
        if (extra != null) {
          combine.setNumber(extra.getAmount());
        }
      }
    }
    return ret;
  }


  @Override
  public Long countCombineSku(String skuId, Long categoryId, String supplierId,
      List<String> selectedIds, String keyword) {
    return skuMapper.countCombine(skuId, categoryId, supplierId, selectedIds, keyword);
  }

  @Override
  public List<SkuCombineVO> listUnCombineSku(String skuId, String supplierId,
      Long categoryId, List<String> selectedIds, String keyword,
      Pageable pageable) {
    return skuMapper.listUnCombine(skuId, supplierId, categoryId, selectedIds, keyword, pageable);
  }

  @Override
  public Long countUnCombineSku(String skuId, String supplierId, Long categoryId,
      List<String> selectedIds, String keyword) {
    return skuMapper.countUnCombine(skuId, supplierId, categoryId, selectedIds, keyword);
  }

  @Override
  public Product productsBySale(String category, String sellerShopId) {
    return productMapper.productsBySale(category, sellerShopId);
  }

  //拼团已售罄
  @Override
  public boolean checkGroupSoldOut(String productId, String pCode) {

    if (StringUtils.isEmpty(productId)) {
      return true;
    }
    //是否是ONSALE状态
    boolean isOnSale = this.isOnSaleByProductId(productId);
    if(!isOnSale){
      return false;
    }
    //若是顺丰商品不用自己的库存校验
    boolean isSFProduct = this.checkIsSFProduct(IdTypeHandler.decode(productId));
    if(isSFProduct){
      return false;
    }

    String skuCode = promotionSkusService.querySkuCodeByProductid(productId, pCode);
    PromotionBaseInfo baseInfo = promotionBaseInfoService.selectByPCode(pCode);
    List<String> skuids = new ArrayList<>();
    if (StringUtils.isNotEmpty(skuCode)) {
      Sku sku = skuMapper.selectBySkuCode(skuCode);
      if (sku != null) {
        skuids.add(sku.getId());
      }
    }
    List<String> promotionSkuIds = null;
    if (CollectionUtils.isNotEmpty(skuids)) {
      promotionSkuIds = getPromotionSkuIds(skuids);
    }
    if (CollectionUtils.isNotEmpty(promotionSkuIds)) {
      boolean hasEnoughStock = hasEnoughStock(promotionSkuIds);
      if (!hasEnoughStock) {
        //已售罄发送邮件
        try {
          this.sendEmailWhenSoldOut(productId);
        }catch (Exception e){
          log.error("拼团或活动商品已售罄发送提示邮件出现异常，错误信息是：" + e.getMessage());
        }
        return true;
      }
    }
    if (baseInfo.getEffectTo().before(new Date())) {
      return true;
    }
    //库存已经充足，将redis发邮件标记清空
    //this.removeSoldOutEmailContent(Long.toString(IdTypeHandler.decode(productId)));
    return false;
  }

  private boolean checkIsSFProduct(long productId) {
    Product product = this.productMapper.selectProductByProductId(productId);
    if(product != null){
      if(ProductSource.SF.name().equals(product.getSource())){
        return true;
      }
    }
    return false;
  }

  //普通商品已售罄
  @Override
  public boolean checkCommonProductSoldOut(String productId) {

    //是否是ONSALE状态
    boolean isOnSale = this.isOnSaleByProductId(productId);
    if(!isOnSale){
      //目前还没有处理非ONSALE状态，认为未售罄
      return false;
    }

    //顺丰商品校验售罄
    List<Sku> skuList = this.skuMapper.selectByProductId(productId);
    if(skuList == null || skuList.isEmpty()){
      return true;
    }

    //若是顺丰商品不用自己的库存校验
    boolean isSFProduct = this.checkIsSFProduct(IdTypeHandler.decode(productId));
    if(isSFProduct){
      return false;
    }

    Set<Boolean> bList = new HashSet<>();
    for(Sku s : skuList){
      if(s != null){
        String skuCode = s.getSkuCode();
        //通过商品id和skuCode校验是否有库存，有返回true
        boolean b = this.skuMapper.checkCommonProductStock(productId,skuCode);
        bList.add(b);
      }
    }

    if(bList == null || bList.isEmpty()){
      //已售罄发送邮件
      try {
        this.sendEmailWhenSoldOut(productId);
      }catch (Exception e){
        log.error("普通商品已售罄发送提示邮件出现异常，错误信息是：" + e.getMessage());
      }
      return true;
    }
    if(bList.size()==1 && bList.contains(true)){
      //已售罄发送邮件
      try {
        this.sendEmailWhenSoldOut(productId);
      }catch (Exception e){
        log.error("普通商品已售罄发送提示邮件出现异常，错误信息是：" + e.getMessage());
      }
      return true;
    }

    //库存已经充足，将redis发邮件标记清空
    //this.removeSoldOutEmailContent(Long.toString(IdTypeHandler.decode(productId)));
    return false;
  }

  /**
   * 清空商品已售罄的redis标记
   * @param productId
   */
  public void removeSoldOutEmailContent(String productId) {
    String key = PromotionConstants.getProductSoldOutSendMailKey(productId);
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    redisUtils.del(key);
  }

  @Override
  public boolean isCommonProduct(String productId) {
    boolean isCommonProduct = true; //标记是否是普通商品
    List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService.selectByProductId(productId);
    if (CollectionUtils.isNotEmpty(promotionBaseInfos) && promotionBaseInfos.get(0).getpType()
            .contains("meeting")) {
      isCommonProduct = false;
    }

    //拼团增加已售罄功能没有pCode by Yarm
    if (CollectionUtils.isNotEmpty(promotionBaseInfos) && promotionBaseInfos.get(0).getpType()
            .contains("piece")) {
      isCommonProduct = false;
    }

    return isCommonProduct;
  }

  /**
   * 商品已经售罄发送邮件
   * @param productId
   * @return
   */
  @Override
  public boolean sendEmailWhenSoldOut(String productId) throws Exception{
    Map map = new HashMap();
    map.put("configName","PRODUCT_SOLD_OUT_SEND_EMAIL_SWITCH");
    //是否开启发送邮件的开关,false不执行发送邮件代码
    PromotionConfig config = promotionConfigMapper.selectByConfig(map);
    if(config != null){
      if(StringUtils.isNotBlank(config.getConfigValue())
      && "false".equals(config.getConfigValue())){
        return false;
      }
    }
    if(StringUtils.isBlank(productId)){
      return false;
    }
    //校验redis是否已经发送过邮件
    Integer value = this.getSoldOutRedisFlag(Long.toString(IdTypeHandler.decode(productId)));
    if( value != null){
      return true;
    }
    //发送邮件
    this.sendEmail(productId);
    return false;
  }

  @Override
  public List<SfStockVO> checkSfStock(String id, String province, String city , String region) {
    log.info("顺丰商品校验已售罄获取到的商品id是："+IdTypeHandler.decode(id)+"{}省是：" + province+"{}市："+city +"{}区县："+ region);
    //通过名字获取省的id
    List<SfStockVO> list = new ArrayList<>();//返回数据
    Long regionId = null;//省id
    if(StringUtils.isNotBlank(province)){//去掉市
        province = province.replace("市", "");
    }
    if(StringUtils.isNotBlank(city)){
      city = city.replace("市","");
    }
    //获取区县的地址
    regionId = this.getRegionId(province, city, region);
    //若取不到地址，不做校验
    if(regionId == null){
      return this.convertToNotSf(list);
    }

    List<LogisticsStockParam> paramList = new ArrayList<>();
    Product product = this.productMapper.selectProductByProductId(IdTypeHandler.decode(id));
    if(product == null){
      return this.convertToNotSf(list);
    }
    if (ProductSource.SF.name().equals(product.getSource())) {
      LogisticsStockParam p = new LogisticsStockParam();
      p.setProductid(IdTypeHandler.decode(product.getSourceProductId()));
      Long areaId = sfRegionService.getsfid(regionId);
      if(areaId != null){
        p.setRegionId(Integer.parseInt(String.valueOf(areaId)));
      }
      p.setSfairline(product.getSfairline());
      p.setSfshipping(product.getSfshipping());
      paramList.add(p);
    }else {
      //非顺丰商品
      return this.convertToNotSf(list);
    }

    //查询顺丰库存
    List<LogisticsStock> logisticsStockList = logisticsGoodsService.getStock(paramList);
    //转化数据
    if(logisticsStockList == null || logisticsStockList.isEmpty()){
      SfStockVO vo = new SfStockVO();
      vo.setStockStatus(0);
      vo.setStockStatusName("缺货");
      list.add(vo);
      return list;
    }
    for (LogisticsStock logisticsStock : logisticsStockList){
      if(logisticsStock != null){
        SfStockVO vo = this.convertToSfStockVO(logisticsStock);
        if(vo != null){
          list.add(vo);
        }
      }
    }

    return list;
  }

    /**
     * 查询顺丰商品
     * @return
     */
    @Override
    public List<Product> selectSfProduct() {
        return this.productMapper.selectSfProduct();
    }

    @Override
    public List<Product> listProductsBySoldOutNotSf(String shopId, String category, String isGroupon,
                                                    Pageable page,
                                                    Direction direction, ProductType type, ProductStatus status,
                                                    ProductReviewStatus reviewStatus) {
        //库存校验是否已经下架
        List<Product> productList = this.productMapper.listAllProductsByOnsaleAt(null, null);
        if(productList == null || productList.isEmpty()){
            return null;
        }
        List<String> list = new ArrayList<>();
        for (Product p : productList){
            if(p == null){
                continue;
            }
            if(ProductSource.SF.name().equals(p.getSource())){
                continue;//过滤顺丰商品
            }
            boolean b = this.checkIsSoldOutByProductIdAndSkuCode(p.getId());
            if(b){
                list.add(p.getId());
            }
        }
        if(list == null || list.isEmpty()){
            return null;
        }
        //分页查询
        List<Product> resultList = this.productMapper.listProductsBySoldOutNotSf(shopId, category, isGroupon, page, direction, type, status,
              reviewStatus, list);
        for(Product p : resultList){
          if(p != null){
            p.setStatus(ProductStatus.SOLDOUT);
          }
        }
      return resultList;
    }

    /**
     *  确认订单状态
     * @param productId
     * @param status
     * @return
     */
    @Override
    public boolean checkProductStatus(String productId, String status) {
        return this.productMapper.checkProductStatus(productId, status);
    }

    @Override
    public List<Product> filterSelfOperatedByPVo(List<Product> list) {
        if(list == null || list.isEmpty()){
            return list;
        }
        List<Product> result = new ArrayList<>();
        Iterator<Product> it = list.iterator();
        while(it.hasNext()){
            Product p = it.next();
            if(StringUtils.isBlank(p.getId()))
                continue;
            //确认是否是汉薇自营
            boolean b = productMapper.checkIsSelfOperated(p.getId(),"1");
            if(b){
                result.add(p);
            }
        }
        return result;
    }

    @Override
    public List<Product> filterIsStockByPVo(List<Product> list) {
        if(list == null || list.isEmpty()){
            return list;
        }
        List<Product> result = new ArrayList<>();
        Iterator<Product> it = list.iterator();
        while (it.hasNext()){
            Product p = it.next();
            if(p == null){
              continue;
            }
            List<Sku> skuList = this.skuMapper.selectByProductId(p.getId());
            if(skuList == null || skuList.isEmpty()){
                continue;
            }
            //若是顺丰商品不用自己的库存校验
            boolean isSFProduct = this.checkIsSFProduct(IdTypeHandler.decode(p.getId()));
            if(isSFProduct){
                continue;
            }

            boolean b = checkStockByProductIdAndSkuCode(p.getId(),skuList);
            //有库存
            if(b)
                result.add(p);
        }
        return result;
    }

    /**
   * 校验状态是否是ONSALE
   * @param productId
   * @return
   */
    private boolean isOnSaleByProductId(String productId){
      String name = ProductStatus.ONSALE.name();
      boolean b = this.checkProductStatus(productId, name);
      return b;
    }

    /**
     * 包含多规格已售罄
     * @param productId
     * @return
     */
    private boolean checkIsSoldOutByProductIdAndSkuCode(String productId) {

        List<Sku> skuList = this.skuMapper.selectByProductId(productId);
        Set<Boolean> bList = new HashSet<>();
        for(Sku s : skuList){
            if(s != null){
                String skuCode = s.getSkuCode();
                //通过商品id和skuCode校验是否有库存，有返回true
                boolean b = this.skuMapper.checkCommonProductStock(s.getProductId(),skuCode);
                bList.add(b);
            }
        }
        if(bList == null || bList.isEmpty()){
            return true;
        }
        if(bList.size()==1 && bList.contains(true)){
            return true;
        }
        return false;
    }


    private Long getRegionId(String province, String city, String region) {
    if(StringUtils.isBlank(province) || StringUtils.isBlank(city) || StringUtils.isBlank(region)){
      return  null;
    }
    //查询省
    SystemRegion provinceInfo = this.systemRegionMapper.selectByNameAndParentId(province, "1");
    if(province == null){
      return null;
    }
    SystemRegion cityInfo = this.systemRegionMapper.selectByNameAndParentId(city, provinceInfo.getId().toString());

    if(cityInfo == null){
      return null;
    }
    SystemRegion regionInfo = this.systemRegionMapper.selectByNameAndParentId(region, cityInfo.getId().toString());
    if(regionInfo == null){
      return null;
    }

    return regionInfo.getId();
  }

  /**
   * 非顺丰商品
   * @param list
   * @return
   */
  private List<SfStockVO> convertToNotSf(List<SfStockVO> list) {
    SfStockVO vo = new SfStockVO();
    vo.setSfProduct(false);
    vo.setStockStatusName("现货");
    vo.setStockStatus(2);
    list.add(vo);
    return list;
  }

  private SfStockVO convertToSfStockVO(LogisticsStock logisticsStock) {
    SfStockVO vo = new SfStockVO();
    if(logisticsStock != null){
      Integer stockStatus = logisticsStock.getStockStatus();
      convertSfstockStatus(vo, stockStatus);
    }
    return vo;
  }

  /**
   * 顺丰：0=缺货，1=无法送达，2=现货，3=预订，4=商品错误
   * 汉薇：0=已售罄，1=超出配送范围，2=现货
   * @param stockStatus
   * @return
   */
  private void convertSfstockStatus(SfStockVO vo,Integer stockStatus) {
    switch (stockStatus){
      case 0:
        vo.setStockStatusName("已售罄");
        vo.setStockStatus(0);
        break;
      case 1:
        vo.setStockStatusName("超出配送范围");
        vo.setStockStatus(1);
        break;
      case 2:
        vo.setStockStatusName("现货");
        vo.setStockStatus(2);
        break;
      case 3:
        vo.setStockStatusName("预订");
        vo.setStockStatus(2);
        break;
      case 4:
        vo.setStockStatusName("已售罄");
        vo.setStockStatus(0);
        break;
      default:
        vo.setStockStatusName("现货");
        vo.setStockStatus(2);
        break;
    }
  }

  /**
   * 发送邮件
   * @param productId
   */
  private boolean sendEmail(String productId) {
    HvEmail hv = new HvEmail("","","");

    List<String> list = new ArrayList<>(); //收件人列表
    Map map = new HashMap();
    map.put("configName","PRODUCT_SOLD_OUT_SEND_EMAIL_USER");
    //查询数据库配置邮件列表
    PromotionConfig config = promotionConfigMapper.selectByConfig(map);
    if(config != null){
      String configValue = config.getConfigValue();
      String[] mailArr = configValue.split(",");//数据库中配置的邮件列表以逗号隔开，且是西文逗号
      if(mailArr != null){
        for (String str : mailArr){
          list.add(str);
        }
      }
    }
    if(list == null || list.isEmpty()){
      list.add("yarm.yang@handeson.com");
    }
    hv.setToList(list);
    hv.setSubject("商品已售罄邮件提示");//标题
    hv.setContent(this.getSoldOutEmailContent(productId));//内容
    try {
        //redis写入标记
        this.setSoldOutRedisFlag(Long.toString(IdTypeHandler.decode(productId)));
        log.info("商品id是" + productId + "的商品发送已售罄邮件，收件人是:" + list.toString());
        boolean b = HvSendEmailUtil.sendMsg(hv);
        if(b){
            log.info("商品id是" + productId + "已成功发送已售罄提示邮件");
        }
      return b;
    } catch (MessagingException e) {
      log.error("商品售罄发送邮件失败！错误信息MessagingException：" + e.getMessage());
    }
    return false;
  }

  /**
   * 获取已售罄redis标记
   * @param productId
   */
  private Integer getSoldOutRedisFlag(String productId) {
    String key = PromotionConstants.getProductSoldOutSendMailKey(productId);
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    Integer integer = redisUtils.get(key);
    return integer;
  }

  /**
   * 设置已售罄发邮件的redis标记
   * @param productId
   */
  private void setSoldOutRedisFlag(String productId) {
    String key = PromotionConstants.getProductSoldOutSendMailKey(productId);
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    Map map = new HashMap();
    Long redisTime = null;
    map.put("configName","PRODUCT_SOLD_OUT_SEND_EMAIL_REDIS_TIME");
    //查询商品已售罄发送邮件频率
    PromotionConfig config = promotionConfigMapper.selectByConfig(map);
    if(config != null){
      String configValue = config.getConfigValue();
      if(StringUtils.isNotBlank(configValue)){
        redisTime = Long.parseLong(configValue);
      }
    }
    if(redisTime == null){
      redisTime = 12*60*60*1000L;
    }
    redisUtils.set(key, 1);
    redisUtils.expire(key, redisTime, TimeUnit.MILLISECONDS);
  }

  /**
   *
   * 获取售罄邮件内容
   * @param productId
   * @return
   */
  private String getSoldOutEmailContent(String productId) {
    Product product = productMapper.selectProductSoldOutByProductIdIdHandler(productId);
    List<Sku> skus = skuMapper.selectByProductId(productId);
    if(product == null || skus == null){
      return null;
    }
    StringBuffer sb = new StringBuffer();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    sb.append("商品Id：");
    sb.append(IdTypeHandler.decode(product.getId()));
    sb.append("<br>");
    sb.append("商品名称：");
    sb.append(product.getName());
    sb.append("<br>");
    sb.append("售罄时间：");
    sb.append(sdf.format(new Date()));
    sb.append("<br>");
    sb.append("供应商：");
    sb.append(product.getSupplierId());
    sb.append("<br>");
    sb.append("商品对应的skuCode和库存如下：");
    sb.append("<br>");
    for (Sku sku : skus){
      sb.append("skuCode是");
      sb.append(sku.getSkuCode());
      sb.append("对应库存：");
      sb.append(sku.getAmount());
      sb.append("<br>");
    }
    sb.append("发送邮件的系统环境：");
    sb.append(profilesActiveValue);
    String s = sb.toString();
    return s;
  }

  /**
   * 过滤活动商品
   */
  private List<String> getPromotionSkuIds(List<String> skuIds) {
    List<String> promotionSkuIds = new ArrayList<String>();
    for (String skuId : skuIds) {
      Sku sku = skuMapper.selectByPrimaryKey(skuId);
      SkuExtend skuEx = new SkuExtend();
      BeanUtils.copyProperties(sku, skuEx);

      //检查这个商品有无活动，没有则认为是没有赠品
      List<PromotionSkuVo> promotionSkuVos = promotionSkusService
              .getPromotionSkus(skuEx.getSkuCode());
      if (org.apache.commons.collections.CollectionUtils.isEmpty(promotionSkuVos)) {
        continue;
      }
      promotionSkuIds.add(sku.getId());
    }
    return promotionSkuIds;
  }

  /**
   * 检查是否有足够的活动库存
   */
  public boolean hasEnoughStock(List<String> promotionSkuIds) {

    boolean isEnough = promotionSkusService.hasEnoughStock(promotionSkuIds);
    return isEnough;
  }
}
