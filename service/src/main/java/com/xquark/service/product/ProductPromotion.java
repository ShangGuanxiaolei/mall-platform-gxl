package com.xquark.service.product;

import com.xquark.service.cart.vo.CartPromotionInfo;

/**
 * Created by wangxinhua on 17-11-3. DESC: 检查商品是否在订单中接口
 */
public interface ProductPromotion {

    boolean inPromotion(String productId, String excludePromotionId, String shopId);

  void changeStatus(CartPromotionInfo info, String productId, String shopId);

}
