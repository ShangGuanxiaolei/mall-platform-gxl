package com.xquark.service.product.impl;

import com.xquark.dal.mapper.ProductTopMapper;
import com.xquark.dal.model.ProductTop;
import com.xquark.dal.vo.ProductTopVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.product.ProductTopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("productTopService")
public class ProductTopServiceImpl extends BaseServiceImpl implements ProductTopService {

  @Autowired
  ProductTopMapper productTopMapper;

  @Override
  public int insert(ProductTop productTop) {
    return productTopMapper.insert(productTop);
  }

  @Override
  public int insertOrder(ProductTop productTop) {
    return productTopMapper.insert(productTop);
  }

  @Override
  public ProductTopVO load(String id) {
    return productTopMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return productTopMapper.updateForArchive(id);
  }

  @Override
  public int update(ProductTop record) {
    return productTopMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<ProductTopVO> list(Pageable pager, Map<String, Object> params) {
    return productTopMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return productTopMapper.selectCnt(params);
  }

  /**
   * 查询是否已经有该商品的分佣设置
   */
  @Override
  public Long selectByProductId(String shopId, String productId, String id) {
    return productTopMapper.selectByProductId(shopId, productId, id);
  }

  /**
   * 供客户端调用，获取首页爆款推荐商品
   */
  @Override
  public List<ProductTopVO> getHomeForApp(Pageable pager, String shopId) {
    return productTopMapper.getHomeForApp(pager, shopId);
  }

}
