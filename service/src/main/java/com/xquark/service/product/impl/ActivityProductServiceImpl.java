package com.xquark.service.product.impl;

import com.xquark.dal.mapper.PromotionExclusiveMapper;
import com.xquark.dal.model.ActivityExclusive;
import com.xquark.service.product.ActivityProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("activityProductService")
public class ActivityProductServiceImpl implements ActivityProductService {

    @Autowired
    private PromotionExclusiveMapper promotionExclusiveMapper;

    @Override
    @Transactional
    public void insertActivityProduct(ActivityExclusive activityExclusive) {
        promotionExclusiveMapper.insertActivityProduct(activityExclusive);
    }

    @Override
    public void updateActivityProduct(Integer productId, Integer status) {
        try {
            if (productId != null && status != null) {
                promotionExclusiveMapper.updateActivityExclusive(productId, status);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
