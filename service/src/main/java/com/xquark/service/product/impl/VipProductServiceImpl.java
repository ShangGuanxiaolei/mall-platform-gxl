package com.xquark.service.product.impl;

import static com.xquark.service.cache.constant.CacheKeyConstant.PRODUCT_SUITS_VIP;

import com.xquark.dal.mapper.PromotionVipsuitMapper;
import com.xquark.dal.model.ActivityExclusive;
import com.xquark.dal.model.PromotionVipsuit;
import com.xquark.dal.model.VipProduct;
import com.xquark.dal.model.VipProductBlackList;
import com.xquark.service.cache.annotation.DoGuavaCache;
import com.xquark.service.cache.constant.ExpireAt;
import com.xquark.service.product.VipProductService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("vipProductService")
public class VipProductServiceImpl implements VipProductService {

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PromotionVipsuitMapper promotionVipsuitMapper;


    @Override
    @DoGuavaCache(key = PRODUCT_SUITS_VIP,expireAt = ExpireAt.FIVE)
    public List<VipProduct> listVipProducts() {
        List<VipProduct> vipProducts = promotionVipsuitMapper.selectByReleaseTime();
        if (null != vipProducts && vipProducts.size() > 0) {
            for (VipProduct vipProduct : vipProducts) {
                if (vipProduct.getPrice() != null) {
                    vipProduct.setPrice(vipProduct.getPrice().subtract(vipProduct.getDeductionDPoint().divide(new BigDecimal(10))).setScale(2, 1));
                }
            }
            return vipProducts;
        } else {
            log.info("未查询到VIP套装列表");
            return new ArrayList<>();
        }
    }


    @Override
    public List<VipProduct> listVIPRecommends(String productId) {
				return promotionVipsuitMapper.selectVIPRecommends(productId);

    }

  @Override
    public Boolean findVipProductByProductId(String productId) {
        Boolean aBoolean = promotionVipsuitMapper.findVipProductByProductId(productId);
        return aBoolean;
    }

    @Override
    public List<VipProductBlackList> listVipsuits(Pageable page) {
        List<VipProductBlackList> vipProducts = promotionVipsuitMapper.selectListUseforBackstage(page);
        if (null != vipProducts && !vipProducts.isEmpty()) {
            return vipProducts;
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    @Transactional
    public void insertPromotionVipsuit(String... productCodes) {
        List<PromotionVipsuit> promotionVipsuits = promotionVipsuitMapper.selectProductIdFromVipsuit(productCodes);
        List<String> allProductCode = new ArrayList<>(Arrays.asList(productCodes));
        List<String> existProductCode = new ArrayList<>();
        if (promotionVipsuits != null && promotionVipsuits.size() > 0) {
            for (PromotionVipsuit promotionVipsuit : promotionVipsuits) {
                existProductCode.add(promotionVipsuit.getProductCode());
            }
            allProductCode.removeAll(existProductCode);
            addPromotionVipsuit(allProductCode);
        } else {
            addPromotionVipsuit(allProductCode);
        }
    }
    private void addPromotionVipsuit(List<String> allProductCode) {
        if (allProductCode != null &&  allProductCode.size() > 0) {
            for (String notExistProductCode : allProductCode) {
                PromotionVipsuit promotionVipsuit = promotionVipsuitMapper.selectByProductId(notExistProductCode);
                promotionVipsuit.setStatus(1); // 待生效
                promotionVipsuit.setIsDeleted(1); // 是否逻辑删除：否
                promotionVipsuitMapper.insertVipProductByProductId(promotionVipsuit); // 插入商品数据到VIP套装表中

                ActivityExclusive activityExclusive = new ActivityExclusive();
                activityExclusive.setProductId(promotionVipsuit.getProductId()); // 活动商品ID
                activityExclusive.setStatus(1); // 正常(在普通商品中不展示)
                activityExclusive.setpCode("VIP"); // 活动类型：VIP商品
                promotionVipsuitMapper.insertVipProductToExclusive(activityExclusive); // 添加到活动表中
            }
        } else {
            log.info( "VIP套装商品已经存在，不可重复添加");
        }
    }

    @Override
    @Transactional
    public void updateVipStatus(Integer productid, Integer status) {
        try {
            PromotionVipsuit promotionVipsuit = promotionVipsuitMapper.selectVipSuitByid(productid);
            if (promotionVipsuit == null) {
                return;
            } else {
                // 更新VIP商品表
                promotionVipsuitMapper.updateVipStatu(productid, status);
                // 更新活动表
                if (status == 1 || status == 2) {
                    if (status == 2) {
                        status = 1; // 1：活动商品表正常
                    }
                    promotionVipsuitMapper.updateActivityExclusive(productid, status);
                }
                else if (status == 0 || status == 3) {
                    if (status == 3) {
                        status = 0; // 0：活动商品表失效
                    }
                    promotionVipsuitMapper.updateActivityExclusive(productid, status);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Long> listExclusiveActivity() {
        List<ActivityExclusive> activityExclusives = promotionVipsuitMapper.selectExclusiveByStatus();
        List ids = new ArrayList();
        if (null != activityExclusives && activityExclusives.size() > 0) {
            for (ActivityExclusive activityExclusive : activityExclusives) {
                ids.add(activityExclusive.getProductId());
            }
        }
        return ids;
    }
}