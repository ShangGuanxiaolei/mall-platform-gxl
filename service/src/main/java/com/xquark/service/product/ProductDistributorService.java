package com.xquark.service.product;

import com.xquark.dal.model.*;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.vo.ProductAdmin;
import com.xquark.service.ArchivableEntityService;
import com.xquark.service.product.vo.ProductSkuVO;
import com.xquark.service.product.vo.ProductVO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ProductDistributorService extends ArchivableEntityService<ProductDistributor> {


  ProductDistributor load(String id);

  ProductDistributor selectByProductIdAndShopId(String productId, String shopId);

  int updateForInstock(String id);

  int updateForOnsale(String id);


}
