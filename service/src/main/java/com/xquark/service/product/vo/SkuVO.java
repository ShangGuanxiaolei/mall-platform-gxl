package com.xquark.service.product.vo;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.model.Sku;
import com.xquark.dal.type.Money;
import com.xquark.service.yundou.YundouSettingService;
import com.xquark.utils.CommonUtils;
import java.math.BigDecimal;
import org.springframework.beans.BeanUtils;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Created by wangxinhua on 17-12-7. DESC:
 */
public class SkuVO extends Sku {

  public SkuVO(Sku sku, BigDecimal minYundouPrice, Integer yundouScale) {
    BeanUtils.copyProperties(sku, this);
    this.minYundouPrice = minYundouPrice;
    this.yundouScale = yundouScale;
  }

  @ApiModelProperty(value = "最少需要的RMB数")
  private BigDecimal minYundouPrice;

  @ApiModelProperty(value = "积分兑换比例")
  private Integer yundouScale;

  public BigDecimal getMinYundouPrice() {
    return minYundouPrice;
  }

  public Integer getYundouScale() {
    if (yundouScale != null && yundouScale != 0) {
      return yundouScale;
    }
    return YundouSettingService.GLOBAL_SETTING.getAmount();
  }

  /**
   * 获取当前商品的积分购显示价格
   *
   * @return {@code String} 类型的商品价格/积分值
   */
  public String getPriceStr() {
    BigDecimal price = getPrice();
    BigDecimal minYundouPrice = getMinYundouPrice();
    int yundouScale = this.getYundouScale();
    if (price == null || price.signum() == 0) {
      return "";
    }
    assert minYundouPrice != null && minYundouPrice.signum() == 0;
    long neededYundou = price.subtract(minYundouPrice)
        .multiply(BigDecimal.valueOf(yundouScale))
        .longValue();
    BigDecimal needPrice = minYundouPrice.signum() == 0 ?
        price : minYundouPrice;
    Money money = new Money(needPrice);
    return CommonUtils
        .getNumberFormatFromCache(LocaleContextHolder.getLocale(), money.getCurrency())
        .format(money.getAmount())
        .concat("元/").concat(String.valueOf(neededYundou)).concat("积分");
  }
}
