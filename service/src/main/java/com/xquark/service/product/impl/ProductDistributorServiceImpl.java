package com.xquark.service.product.impl;


import com.xquark.dal.mapper.ProductDistributorMapper;
import com.xquark.dal.model.ProductDistributor;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.product.ProductDistributorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("productDistributorService")
public class ProductDistributorServiceImpl extends BaseServiceImpl implements
    ProductDistributorService {

  @Autowired
  private ProductDistributorMapper productDistributorMapper;


  @Override
  @Transactional
  public int insert(ProductDistributor productDistributor) {
    return productDistributorMapper.insert(productDistributor);
  }

  @Override
  public int insertOrder(ProductDistributor productDistributor) {
    return productDistributorMapper.insert(productDistributor);
  }

  @Override
  public ProductDistributor load(String id) {
    return productDistributorMapper.selectByPrimaryKey(id);
  }

  @Override
  public ProductDistributor selectByProductIdAndShopId(String productId, String shopId) {
    return productDistributorMapper.selectByProductIdAndShopId(productId, shopId);
  }

  @Override
  public int updateForInstock(String id) {
    return productDistributorMapper.updateForInstock(id);
  }

  @Override
  public int updateForOnsale(String id) {
    return productDistributorMapper.updateForOnsale(id);
  }

  @Override
  public int delete(String id) {
    return productDistributorMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int undelete(String id) {
    return productDistributorMapper.undeleteByPrimaryKey(id);
  }
}
