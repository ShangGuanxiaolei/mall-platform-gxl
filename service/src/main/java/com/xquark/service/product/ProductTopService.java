package com.xquark.service.product;

import com.xquark.dal.model.ProductTop;
import com.xquark.dal.vo.ProductTopVO;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface ProductTopService extends BaseEntityService<ProductTop> {

  ProductTopVO load(String id);

  int insert(ProductTop record);

  int deleteForArchive(String id);

  int update(ProductTop record);

  /**
   * 服务端分页查询数据
   */
  List<ProductTopVO> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 查询是否已经有该商品的分佣设置
   */
  Long selectByProductId(String shopId, String productId, String id);

  /**
   * 供客户端调用，获取首页爆款推荐商品
   */
  List<ProductTopVO> getHomeForApp(Pageable pager, String shopId);

}

