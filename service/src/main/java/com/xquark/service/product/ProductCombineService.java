package com.xquark.service.product;

import com.xquark.dal.model.Sku;
import com.xquark.dal.model.SkuCombineExtra;
import com.xquark.dal.vo.SlaveInfoVO;
import java.util.Date;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 * 组合套餐接口
 *
 * @author wangxinhua.
 * @date 2018/9/28
 */
public interface ProductCombineService {

  @Transactional(rollbackFor = Exception.class)
  boolean saveOrUpdateCombine(String masterSkuId, String masterProductId,
      List<SlaveInfoVO> slaves, Date validFrom,
      Date validTo);

  boolean saveOrUpdateExtra(String masterId, List<SlaveInfoVO> slaves);

  boolean isCombineProduct(String productId);

  boolean isCombineSku(String skuId);

  boolean unCombine(String masterSkuId, String slaveSkuId);

  List<String> listSlaveIdBySkuId(String skuId);

  List<SkuCombineExtra> listSlaveBySkuId(String skuId);

  /**
   * 校验套装sku下面的子商品库存是否充足
   */
  void checkAmount(Sku sku);

  SkuCombineExtra loadExtraBySkuIdAndSlaveId(String skuId, String slaveId);

  /**
   * 查询某个sku被绑定到套装的库存数量
   */
  Integer sumBindedSlaveAmount(String skuId);

  Boolean selectIsCombinedSalve(String skuId);

}
