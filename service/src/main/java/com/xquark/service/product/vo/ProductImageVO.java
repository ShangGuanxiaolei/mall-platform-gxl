package com.xquark.service.product.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

/**
 * 商品图片展示
 *
 * @author huxaya
 */
public class ProductImageVO {

  private String img;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String imgUrl;

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }


}
