package com.xquark.service.product;

import com.xquark.dal.model.SfAppConfig;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.service.product.SFProductService;
import java.util.Set;
import java.util.concurrent.ExecutorService;

/**
 * created by
 *
 * @author wangxinhua at 18-6-5 下午1:55 存放线程常量
 */
public class SFTaskGlobalContext {

  public static ExecutorService EC;

  public static int SIZE = 40;

  public static SfAppConfig APP_CONFIG;

  public static String HOST;

  public static SFProductService SF_PRODUCT_SERVICE;

  public static Set<String> BASE_ENCODES;

  public static User USER;

  public static Shop SHOP;

}
