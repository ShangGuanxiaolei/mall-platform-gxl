package com.xquark.service.product.impl;

import com.xquark.dal.consts.SFApiConstants;
import com.xquark.dal.model.SfAppConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * 顺丰商品签名
 * @author tanggb
 * @date 2019/05/15 15:47
 */
@Service
public class SFProductSign{
    private final static Logger LOGGER = LoggerFactory.getLogger(SFProductSign.class);

    @Value("${sf.api.host.url}")
    public String host;
//    private String host="https://openapi.sfbest.com";

    /**
     * 生成SF公共参数 2.0签名
     * @param config
     * @return
     */
    public String generateSign(SfAppConfig config){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("format=json");
        stringBuilder.append("&timestamp="+System.currentTimeMillis()/1000);
        stringBuilder.append("&version=2.0");
        stringBuilder.append("&"+config.getClientId());
        stringBuilder.append("&"+config.getAccessToken());
        stringBuilder.append("&{\"page\":1,\"pageSize\":1}");
        LOGGER.info("顺丰签名URL: " + stringBuilder);
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String sign = Base64.getUrlEncoder().encodeToString(md.digest(stringBuilder.toString().getBytes(StandardCharsets.UTF_8)));
        return sign;
    }

    /**
     * 生成SF公共参数 2.0签名,使用商品编码查询商品信息
     * @param config
     * @return
     */
    public String generateSign(SfAppConfig config,String json){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("format=json");
        stringBuilder.append("&timestamp="+System.currentTimeMillis()/1000);
        stringBuilder.append("&version=2.0");
        stringBuilder.append("&"+config.getClientId());
        stringBuilder.append("&"+config.getAccessToken());
        stringBuilder.append("&" + json);
        LOGGER.info("顺丰签名URL: " + stringBuilder);
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] bytes = stringBuilder.toString().getBytes(StandardCharsets.UTF_8);
        byte[] digest = md.digest(bytes);
        String sign = Base64.getUrlEncoder().encodeToString(digest);
//        String sign = Base64.getUrlEncoder().encodeToString(md.digest(stringBuilder.toString().getBytes(StandardCharsets.UTF_8)));
        return sign;
    }

    /**
     * 获取顺丰商品公共参数URL
     * @param config
     * @return
     */
    public String  basePullSFProductUrl(SfAppConfig config){
//        SFConfig sfConfig = new SFConfig();
        String pullProductUrl = String.format("%s?app_key=%s&access_token=%s&timestamp=%s&sign=%s&format=json&version=2.0",
                host + SFApiConstants.PULL_PRODUCTS, config.getClientId(),
                config.getAccessToken(),
                String.valueOf(System.currentTimeMillis()/1000),
                this.generateSign(config));
        LOGGER.info("请求顺丰获取商品URL:" + pullProductUrl);
        return pullProductUrl;
    }

    /**
     * 获取顺丰商品公共参数URL，使用商品编码查询商品信息
     * @param config
     * @return
     */
    public String basePullSFProductUrl(SfAppConfig config,String json){

        String pullProductUrl = String.format("%s?app_key=%s&access_token=%s&timestamp=%s&sign=%s&format=json&version=2.0",
                host + SFApiConstants.PULL_PRODUCTS, config.getClientId(),
                config.getAccessToken(),
                String.valueOf(System.currentTimeMillis()/1000),
                this.generateSign(config,json));
        LOGGER.info("请求顺丰获取商品URL:" + pullProductUrl);
        return pullProductUrl;
    }

    public static void main(String[] args) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("format=json");
        stringBuilder.append("&timestamp="+System.currentTimeMillis()/1000);
        stringBuilder.append("&version=1.0");
        stringBuilder.append("&RSXE2QGL01");
        stringBuilder.append("&4918bffc-e291-4791-8dee-0da476530948");
//        stringBuilder.append("&{\"page\": 1,\"pageSize\": 20,\"businessModel\":\"3\",\"isShow\":0}");
        stringBuilder.append("&{\"addTime\":1559034900,\"address\":\"link您咯是利拉鲁肽来了兴民的人了哦\",\"area\":0,\"city\":383,\"consignee\":\"红川\",\"device\":28,\"district\":3230,\"isList\":0,\"mobile\":\"18639718532\",\"moneyPaid\":10000,\"orderAmount\":10000,\"orderProducts\":[{\"productNum\":1,\"productSn\":\"4300256811\",\"productType\":0,\"sellPrice\":10000}],\"orderSource\":28,\"outerId\":\"ESO190528165513003003\",\"payId\":1,\"payTime\":1559033715,\"preSale\":false,\"productAmount\":10000,\"province\":31,\"shippingFee\":0}");

        LOGGER.info("顺丰签名URL: " + stringBuilder);
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] bytes = stringBuilder.toString().getBytes(StandardCharsets.UTF_8);
        byte[] digest = md.digest(bytes);
        String sign = Base64.getUrlEncoder().encodeToString(digest);
        System.out.println("签名：" + sign);
    }
}
