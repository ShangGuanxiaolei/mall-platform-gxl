package com.xquark.service.product;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.xquark.service.http.pojo.sf.base.SFProductEntity;
import com.xquark.service.http.pojo.sf.base.SFResponse;
import com.xquark.service.http.pojo.sf.product.SFProductDetailWrapper;
import com.xquark.service.http.pojo.sf.product.SFProductWrapper;
import com.xquark.service.http.pojo.sf.product.SFRegionWrapper;
import com.xquark.service.http.pojo.sf.product.SFStockInfoWrapper;
import com.xquark.utils.HttpClientUtil;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * created by 该类用于返回响应json的code和data
 *
 * @author wangxinhua at 18-6-5 下午2:54
 */
public class SFTaskHelper {

  private final static Logger LOGGER = LoggerFactory.getLogger(SFTaskHelper.class);

  private final static TypeReference<SFResponse<SFProductDetailWrapper>> DETAIL_REFERENCE
      = new TypeReference<SFResponse<SFProductDetailWrapper>>() {
  };

  private final static TypeReference<SFResponse<SFProductWrapper>> PRODUCT_REFERENCE
      = new TypeReference<SFResponse<SFProductWrapper>>() {
  };

  private final static TypeReference<SFResponse<SFRegionWrapper>> REGION
      = new TypeReference<SFResponse<SFRegionWrapper>>() {
  };

  private final static TypeReference<SFResponse<SFStockInfoWrapper>> STOCKINFO_REFERENCE
      = new TypeReference<SFResponse<SFStockInfoWrapper>>() {
  };

  /**
   * 批量查询商品详情
   *
   * @param url 地址
   * @param encodes 商品编码
   * @return 商品详情列表
   */
  @SuppressWarnings("unchecked")
  public static SFProductDetailWrapper requestPdDetail(String url, List<String> encodes) {
    HttpInvokeResult res = PoolingHttpClients
        .postJSON(url, encodes, 5000);
    if (!res.isOK()) {
      LOGGER.error("拉取商品详情接口调用失败", res.getException());
      throw new RuntimeException("request failed", res.getException());
    }
    SFResponse<SFProductDetailWrapper> response;
    try {
      response = JSON.parseObject(res.getContent(), DETAIL_REFERENCE);
    } catch (Exception e) {
      LOGGER.error("拉取商品请求解析错误", e);
      throw new RuntimeException(e);
    }
    return response.getData();
  }

  /**
   * 拉取单个商品
   */
  public static SFProductWrapper requestProduct(String url, String encode) {
    HttpInvokeResult res = PoolingHttpClients
        .postJSON(url, new SFProductEntity(encode), 5000);
    SFResponse<SFProductWrapper> response = JSON.parseObject(res
        .getContent(), PRODUCT_REFERENCE);

    int code = response.getCode();
    if (code != 0) {
      // TODO 请求异常时重新刷新token或进行其他异常处理
      if (code == 120022) {
        // TODO token失效处理
      }
      LOGGER.error("请求地址：" + url);
      throw new RuntimeException("顺丰接口返回：" + response.getMsg());
    }

    return response.getData();
  }

  public static SFProductWrapper requestProducts(String url, int page, int size) {
    LOGGER.info("请求地址 {}", url);
    HttpInvokeResult res = PoolingHttpClients
        .postJSON(url, new SFProductEntity(page, size), 5000);
    if (!res.isOK()) {
      // 重试一次
      res = PoolingHttpClients.postJSON(url, new SFProductEntity(page, size), 5000);
    }
    if (!res.isOK()) {
      LOGGER.error("顺丰商品 {} -> {} 拉取失败", page, size, res.getException());
    }

    SFResponse<SFProductWrapper> response = JSON.parseObject(res
        .getContent(), PRODUCT_REFERENCE);

    int code = response.getCode();
    if (code != 0) {
      // TODO 请求异常时重新刷新token或进行其他异常处理
      if (code == 120022) {
        // TODO token失效处理
      }
      LOGGER.error("请求地址：" + url);
      throw new RuntimeException("顺丰接口返回：" + response.getMsg());
    }

    return response.getData();
  }

  public static SFRegionWrapper requestRegion(String url) {
    HttpInvokeResult res = PoolingHttpClients.postJSON(url);
    if (!res.isOK()) {
      LOGGER.error("获取区域接口调用失败", res.getException());
      throw new RuntimeException("request failed");
    }
    SFResponse<SFRegionWrapper> response = JSON.parseObject(res.getContent(), REGION);

    int code = response.getCode();
    if (code != 0) {
      // TODO 请求异常时重新刷新token或进行其他异常处理
      if (code == 120022) {
        // TODO token失效处理
      }
      LOGGER.error("请求地址：" + url);
      throw new RuntimeException("顺丰接口返回：" + response.getMsg());
    }

    return response.getData();
  }

  /**
   * 获取顺丰商品2.0版
   * @param url
   * @return
   */
  public static SFProductWrapper requestProducts1(String url, String json) {
    LOGGER.info("请求地址 {}", url);
    LOGGER.info("请求参数 {}", json);
    JSONObject jsonObject = doPost(url, json);
    LOGGER.info("请求顺丰返回结果： {}", jsonObject);
    int code = jsonObject.getIntValue("code");
    String data = jsonObject.getString("data");
    String msg = jsonObject.getString("msg");
    if (code != 0) {
      // TODO 请求异常时重新刷新token或进行其他异常处理
      if (code == 120022) {
        // TODO token失效处理
      }

      LOGGER.error("请求地址：" + url);
      throw new RuntimeException("顺丰接口返回：" + msg);
    }

    return JSONObject.parseObject(data, SFProductWrapper.class);
  }

  private static JSONObject doPost(String url, String json){
    String result = HttpClientUtil.doPost(url, json, "utf-8");
    JSONObject jsonObject = JSONObject.parseObject(result);
    return jsonObject;
  }

}
