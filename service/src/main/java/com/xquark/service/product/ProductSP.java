package com.xquark.service.product;

import com.xquark.dal.model.ActivitySP;

/**
 * 特价抢购商品
 *
 * @author seker
 */
public interface ProductSP {

  //已购买数量
  Integer getBoughtAmount();

  void setBoughtAmount(Integer amount);

  ActivitySP getFirstActivity();

  void setFirstActivity(ActivitySP activitySP);

  //是否已经放开购买
  boolean onSale();

  //超出购买上限
  boolean overLimit(Integer amount);
}
