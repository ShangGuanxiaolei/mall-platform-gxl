package com.xquark.service.product;

import com.xquark.dal.model.ProductBlackList;
import com.xquark.dal.vo.ProductBlackListVO;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface ProductBlackListService extends BaseEntityService<ProductBlackList> {

  ProductBlackListVO load(String id);

  int insert(ProductBlackList team);

  int deleteForArchive(String id);

  int update(ProductBlackList record);

  /**
   * 服务端分页查询数据
   */
  List<ProductBlackListVO> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 删除黑名单商品
   */
  int deleteByProductId(String userId, String productId);

}

