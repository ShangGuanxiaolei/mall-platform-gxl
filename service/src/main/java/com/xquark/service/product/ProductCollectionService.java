package com.xquark.service.product;

import com.xquark.dal.model.ProductCollection;
import com.xquark.dal.vo.ProductCollectionVO;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface ProductCollectionService extends BaseEntityService<ProductCollection> {

  ProductCollection load(String id);

  int insert(ProductCollection team);

  int deleteForArchive(String id);

  int update(ProductCollection record);

  /**
   * 服务端分页查询数据
   */
  List<ProductCollectionVO> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 用户取消某个商品收藏
   */
  int deleteByProductId(String userId, String productId);

  /**
   * 查询用户是否收藏某个商品
   */
  ProductCollection selectByProductId(String userId, String productId);

  /**
   * 用户我的收藏商品列表
   */
  List<ProductCollectionVO> listByApp(Pageable pager, String userId);

}

