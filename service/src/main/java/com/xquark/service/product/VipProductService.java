package com.xquark.service.product;

import com.xquark.dal.model.VipProduct;
import com.xquark.dal.model.VipProductBlackList;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Vip套装商品服务层接口
 */
public interface VipProductService {

    /**
     * 供前端调用的VIP套装列表接口
     * @return List<VipProduct>
     */
    List<VipProduct> listVipProducts();

    /**
     * 查询VIP推荐商品
     * @param productId
     */
    List<VipProduct> listVIPRecommends(String productId);

    /**
     * 根据商品的ID，查询该商品是否是VIP套装商品
     * @param productId
     * @return boolean
     */
    Boolean findVipProductByProductId(String productId);

    /**
     * 供后台管理系统调用的VIP套装列表接口
     * @return List<VipProduct>
     */
    List<VipProductBlackList> listVipsuits(Pageable page);

    /**
     * 根据商品ID添加VIP套装商品信息
     * @param productCodes
     * @return
     */
    void insertPromotionVipsuit(String... productCodes);

    void updateVipStatus(Integer productid,Integer status);

    /**
     * 普通商品需要过滤掉的活动商品集合
     * @return
     */
    List<Long> listExclusiveActivity();
}
