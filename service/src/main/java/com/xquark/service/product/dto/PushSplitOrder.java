package com.xquark.service.product.dto;

public class PushSplitOrder {
    private int msg_type;
    private String outer_id;
    private String parent_order_sn;

    public int getMsg_type() {
        return msg_type;
    }

    public void setMsg_type(int msg_type) {
        this.msg_type = msg_type;
    }

    public String getOuter_id() {
        return outer_id;
    }

    public void setOuter_id(String outer_id) {
        this.outer_id = outer_id;
    }

    public String getParent_order_sn() {
        return parent_order_sn;
    }

    public void setParent_order_sn(String parent_order_sn) {
        this.parent_order_sn = parent_order_sn;
    }
}
