package com.xquark.service.product;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.xquark.aop.anno.NotNull;
import com.xquark.dal.model.*;
import com.xquark.dal.status.ProductReviewStatus;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.type.ProductType;
import com.xquark.dal.vo.*;
import com.xquark.service.ArchivableEntityService;
import com.xquark.service.product.vo.ProductSearchVO;
import com.xquark.service.product.vo.ProductSkuVO;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.product.vo.SfStockVO;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

public interface ProductService extends ArchivableEntityService<Product> {

  @Override
  ProductVO load(String productId);

  ProductVO load(String productId, Boolean syncSource);

  boolean exists(String productId);

  ProductSkuVO load(String sellerId, String productId, String skuId);

  ProductSkuVO load(String productId, String skuId);

  Sku loadSku(String skuId);

  Sku loadSku(String productId, String skuId);

  List<Sku> listSkus(String productId);

  List<Sku> listSkusOrig(String productId);

  List<Sku> listSkus(Product product);

  List<Product> selectByIds(String[] ids);

  /**
   * 创建产品，并自动汇总Sku中的库存总额和最低的价格，到产品主表上
   */
  int create(Product product, List<Sku> skus, List<Tag> tags, List<Tags> tagsToSave,
      List<ProductImage> imgs, List<SkuMapping> skuMapping);

  /**
   * 不保存标签的原实现
   */
  int create(Product product, List<Sku> skus, List<Tag> tags, List<ProductImage> imgs,
      List<SkuMapping> skuMapping);

  //public void updatePrice(Product product, List<Sku> skus,  BigDecimal newPrice, UpdatePriceType type);

  public void lockProduct(String productId, Boolean ret);

  public void unlockProductByShopId(String shopId);

  public void lockProductByShopId(String shopId);

  public void unlockProductByShopIdEx(String shopId);

  public void unlockProductByIds(List<String> ids);

  /**
   * 更新产品 操作流程： 1.检查被修改的产品和Sku是否属于操作者本身 2.并自动汇总Sku中的库存总额和最低的价格，到产品主表上
   * 3.如果产品库存为0，则自动下架，并记录下架时间，如果从0改为>0的值，则自动上架，并记录上架时间 4.删除图片 5.保存更新sku 6.删除被标志为删除的sku
   */
  int update(Product product, List<Sku> skus, List<Tag> tags,
      List<ProductImage> imgs, List<SkuMapping> skuMapping);

  /**
   * 更新分销产品 操作流程： 1.检查被修改的产品和Sku是否属于操作者本身 2.并自动汇总Sku中的库存总额和最低的价格，到产品主表上
   * 3.如果产品库存为0，则自动下架，并记录下架时间，如果从0改为>0的值，则自动上架，并记录上架时间 4.删除图片 5.保存更新sku,不同于update方法的行为是确保修改分销商品所属的Sku&SkuMapping,避免load方法返回ProductVO对象时包含的是源商品Sku&SkuMapping引起的副作用
   * 6.删除被标志为删除的sku
   */
  int updateDistributionProduct(Product product, List<Sku> skus, List<Tag> tags,
      List<ProductImage> imgs, List<SkuMapping> skuMapping);

  /**
   * newSpider调用更新 2015-06-23
   */
  void updateSpecImg(Product product, List<ProductImage> imgs);

  /**
   * spider调用更新 2015-03-04
   */
  int updateEx(Product product, List<Sku> skus, List<Tag> tags,
      List<ProductImage> imgs, List<SkuMapping> skuMapping, Integer option);

  /**
   * spider调用更新 2015-06-23
   */
  int updateEx(Product product);

  /**
   * 定时上架
   */
  int forsale(String id, Date forsaleAt);

  /**
   * 上架
   */
  int onsale(String id);

  /**
   * 下架
   */
  int instock(String id) throws Exception;

  /**
   * spider调用下架
   *
   * @param thirdItemId 第三方平台itemId 2015-03-06
   */
  int instockEX(String shopId, String thirdItemId) throws Exception;


  List<Product> listProducts4Sync(String shopId, Pageable page);

  /**
   * 上架商品列表
   */
  List<Product> listProductsByOnsaleAt(String shopId, String category, String isGroupon,
      Pageable page);

  /**
   * 上架商品列表
   */
  List<Product> listProductsAvailableByChannel(String activityId, String shopId, Pageable page,
      String channel, String key);

  Long countProductsAvailableByChannel(String activityId, String shopId, String channel,
      String key);

  /**
   * 上架商品列表，并带查询参数
   */
  List<Product> listProductsByOnsaleAt(String shopId, String category, String isGroupon,
      Pageable page, Map<String, Object> params);

  /**
   * 取最近days有发布的商品日期和每天发布商品中的数量， 以后此部分可以进客户缓存管理
   *
   * @param date 从哪一天开始
   * @param days 取几天
   */
  List<Map<String, Object>> listProductByRecently(String shopId, Date date, int days)
      throws ParseException;

  /**
   * 销量列表
   */
  List<Product> listProductsBySales(String shopId, String category, String isGroupon,
      Pageable page, Direction direction, String sellerShopId, List<String> subCategoryIds,
      String priceStart, String priceEnd, ProductStatus status, ProductReviewStatus reviewStatus,
      ProductType... type);

  List<Product> listProductsBySales(String shopId, String category, String isGroupon,
                                    Pageable page, Direction direction, String sellerShopId, List<String> subCategoryIds,
                                    String priceStart, String priceEnd, ProductStatus status, ProductReviewStatus reviewStatus,
                                    ProductSearchVO productSearchVO ,ProductType... type);

  List<Product> listFilterBySpec(String order,
      Direction direction, Pageable pageable, Map<String, ?> params);

  Long countFilterBySpec(Map<String, ?> params);

  /**
   * 上架时间列表
   */
  List<Product> listProductsBySaleAt(String shopId, String category, String isGroupon,
      Pageable page, Direction direction, String sellerShopId, List<String> subCategoryIds,
      String priceStart, String priceEnd,ProductSearchVO productSearchVO, ProductType... type);

  /**
   * 利润列表
   */
  List<Product> listProductsByCm(String shopId, String category, String isGroupon, Pageable page,
      Direction direction, String sellerShopId);

  Long countProductsBySales(String shopId);

  /**
   * 库存列表
   */
  List<Product> listProductsByAmount(String shopId, String category, String isGroupon,
      Pageable page, Direction direction, ProductType type,
      ProductStatus status, ProductReviewStatus reviewStatus);

  /**
   * 根据市场价格进行排序
   */
  List<Product> listProductsByPrice(String shopId, String category, String isGroupon,
      Pageable page, Direction direction, String sellerShopId, List<String> subCategoryIds,
      String priceStart, String priceEnd, ProductSearchVO productSearchVO,ProductType... type);


  boolean updateReviewStatus(String id, ProductReviewStatus status);
  /**
   * 根据发布时间进行排序
   */
  List<Product> listProductsByOnsaleDate(String shopId, Pageable page, Direction direction);

  /**
   * 下架商品列表
   */
  List<Product> listProductsBySoldout(String shopId, String category, String isGroupon,
      Pageable page, Direction direction, ProductType type,
      ProductStatus status, ProductReviewStatus reviewStatus);

  /**
   * 草稿商品列表
   */
  List<Product> listProductsByStatusDraft(String shopId, String category, String isGroupon,
      Pageable page, ProductType type, ProductStatus status,
      ProductReviewStatus reviewStatus);

  /**
   * 计划发布商品列表
   */
  List<Product> listProductsByForSale(String shopId, Pageable page);

  /**
   * 缺货商品列表
   */
  List<Product> listProductsByOutOfStock(String shopId, String category, String isGroupon,
      Pageable page, ProductType type, ProductStatus status,
      ProductReviewStatus reviewStatus);

  /**
   * 计划发布商品列表
   */
  List<Product> listProductsByDelay(String shopId, String category, String isGroupon,
      Pageable page, ProductType type, ProductStatus status,
      ProductReviewStatus reviewStatus);


  /**
   * 包邮商品列表
   */
  List<Product> listProductsByPostAge(String shopId, String category, String isGroupon,
      Pageable page, Direction direction, ProductType type,
      ProductStatus status, ProductReviewStatus reviewStatus);

  /**
   * 相关商品
   */
  List<Product> listProductsByRelated(String shopId, String id, Pageable page);

  /**
   * 推荐商品
   */
  List<Product> listProductsByRecommend(String shopId, Pageable page);

  Long countByRecommend(String shopId);

  /**
   * 每种状态商品的数量
   */
  Long countProductsByStatus(String shopId, ProductStatus status);

  /**
   * 缺货商品数量
   */
  Long countProductsByOutofStock(String shopId);

  /**
   * 查询 modify by zzd 新增分页功能
   */
  List<Product> search(@Deprecated String shopId, String key, Pageable page,
      ProductType type);


  /**
   * 汉薇自营商品列表, 默认(按照上架时间倒序)
   * @param shopId
   * @param page
   * @param type
   * @return
   */
  List<Product> selfSupport(@Deprecated String shopId, Pageable page, ProductType type);

  /**
   * 查询且不带状态
   */
  List<Product> searchWithNoStatus(@Deprecated String shopId, String key, Pageable page,
      ProductType type, String productId, String category);

  /**
   * 查询 modify by zzd 新增分页功能
   */
  List<Product> searchAll(String shopId, String key, Pageable page);

  /**
   * 查询 modify by zzd 新增分页功能
   */
  List<Product> searchByPrice(@Deprecated String shopId, String key, Pageable page, String direction,
      ProductType type);

  /**
   * 汉薇自营商品列表, 按照价格排序
   * @param shopId
   * @param page
   * @param direction
   * @param type
   * @return
   */
  List<Product> selfSupportByPrice(@Deprecated String shopId, Pageable page, String direction, ProductType type);

  List<Product> search(String shopId, String key, ProductType type);

  /**
   * 定时任务所用，定时
   */
  int autoOnSaleByTask();

  int deleteByAdmin(String[] ids);

  ProductVO loadByAdmin(String id);

  int undeleteByAdmin(String[] ids);

  /**
   * 用于后台商品管理
   */
  List<ProductAdmin> listProductsByAdmin(Map<String, Object> params, Pageable page);

  List<ProductAdmin> listProductsByCategory(Map<String, Object> params, Pageable page,
      String activityId, String categoryId);

  Long countProductsByAdmin(Map<String, Object> params);

  List<Product> listProducts(String shopId);

  List<Product> listRootProducts(String shopId);

  Boolean instockByAdmin(String[] ids);

//	BigDecimal loadCommissionRate(String productId);

  /**
   * 对分页商品列表接口取总数
   *
   * @param shopId 店铺ID
   * @param catType 商品排序分类类型  { 'sales', 'amount', 'soldout', 'statusDraft', 'outofstock } +
   * default
   */
  Long getLastTotalCnt(String shopId, String category, String isGroupon, String catType,
      Map<String, Object> params);

  /**
   * 跟据关键字获取总记录数据
   */
  Long CountTotalByName(@Deprecated String shopId, String name);

  /**
   * 统计汉德森自营商品数量
   * @param shopId
   * @return
   */
  Long CountTotalBySelfSupport(@Deprecated String shopId);

  List<Product> listProductsByActId(String actId, Pageable pageable);

  List<Product> listActivityProducts(Pageable pageable, int cateid, String userid);

  List<Product> listActivityProducts(Pageable pageable, int cateid);

  /**
   * 商品sku更新库存
   */

  void updateSkuStock(String skuId, Integer amount);

  void updateAmountForRefund(String productId, Integer amount);

  void updateStock(String productId, Integer amount);

  void setFakeSales(String id, int i);

  Long countDelayProduct(String shopId);

  List<Product> listDelayProduct(String shopId, Pageable pageable);

  int createByUnion(Product product, List<Sku> skus, List<Tag> tags,
      List<ProductImage> imgs);

  int updateByUnion(Product product, List<Sku> skus, List<Tag> tags,
      List<ProductImage> imgs);

  BigDecimal loadCommissionRate(String productId, String unionId);

  int synchronous(String ids, String sourceCodes);

  List<String> obtainDbSynchronous(List<String> ids);

  //获取活动下商品列表 (xquark_campaign_product)
  List<Product> listProductByActivityId(String activityId);

  List<Product> selectActivityBegin(String shopId);

  List<Product> selectActivityEnd(String shopId);

  List<ProductImage> requestImgs(String productId, String type);

  String findSkuSpec(Sku sku, boolean br);

  List<Sku> findSkusByIds(String... skuId);

  Product findProductById(String id);

  List<Product> findProductsBySourceProductID(String id);

  List<Product> findProductsBySkuCode(String shopId, String skuCode);

  /**
   * 通过商品的 id 获取商品sku
   */
  List<Sku> findByProductId(String productId);

  // 获取没有code的商品信息
  List<Product> listNoCodeProducts();

  Long CountTotalByNameWithNoStatus(@Deprecated String shopId, String key, String productId, String category);

  void addCode(String... ids);

  void clear4noneProductByShopId(String shopId);

  int updateEnableDesc(Product p);

  List<Product> listAllProductsByOnsaleAt(String shopId, Pageable pageable);

  long countAllShopProduct(String shopId);

  List<ProductAdmin> listProds2ActivityByAdmin(Map<String, Object> params, Pageable page);

  Long countProds2ActivityByAdmin(Map<String, Object> params);

  List<ProductVO> listCrossProductsBySales(String shopId, Pageable page, Direction desc);

  List<Activity> listActivity(Map<String, Object> params, Pageable page);

  long countAllactivity(Map<String, Object> params);

  int createActivity(Map<String, String> params);

  int deleteActivity(String activityid);

  List<CategoryActivity> listCategorybyActivity(Map<String, Object> params, Pageable page,
      String activityId);

  int createCategoryActivity(Map<String, String> params);

  int deleteCategoryActivity(String id);

  int deleteCategoryActivityRelation(String id);

  Long countCategorybyActivity(String activityId);

  Long countProductsByCategory(Map<String, Object> params, String activityId, String categoryId);

  Long existsNameforCategoryActivity(Map<String, String> params);

  Product addDistributionProduct(Product productSrc, String insertOrUpdate);

  List<Product> listDistributeProductsBySales(String rootShopId, String categoryId, String shopId,
      Pageable pageable);

  List<ProductCardsVO> listProductWithCards(String order, Direction direction, Pageable pageable);

  boolean deleteDiscountProduct(String productId);

  Long countProductWithCards();

  void batchDelete(String[] ids);

  /**
   * 自动下架
   */
  int autoInstockByTask();

  List<Product> listGiftProduct(String category, String order, Direction direction,
      Pageable pageable);

  Long countGiftProduct(String category);

  String selectConcatName(String[] ids, Boolean giftOnly);

  /**
   * 获取某个类别下商品总数
   */
  Long countCategoryProducts(String shopId, String category);

  /**
   * 找到某个商品对应的sku的所有属性值
   */
  List<String> findAttributeByProductId(String productId);

  /**
   * 根据商品id获取该商品设置的积分比例 若没有设置则使用全局比例
   *
   * @param productId 商品id
   * @return 积分比例
   */
  Integer getScaleOrUseGlobal(String productId);

  /**
   * 根据商品id获取该商品设置的积分比例 若没有设置则使用全局比例 如果是商品VO可直接调用VO的方法
   *
   * @param product 商品对象
   * @return 积分比例
   * @see ProductVO#getScaleOrUseGlobal()
   */
  Integer getScaleOrUseGlobal(Product product);

  Integer loadYundouScaleById(String id);

  /**
   * 根据分类过滤条件，统计该过滤条件下总商品数
   */
  Long countProductsByCategoryAndPrice(String shopId, String category,
      List<String> subCategoryIds, String priceStart, String priceEnd,
      ProductType... type);

  /**
   * 用户商品绑定
   *
   * @param productUser 绑定对象
   * @return 已绑定则返回True, 未绑定则返回绑定结果
   */
  boolean bindUserIfNotBounded(@NotNull ProductUser productUser);

  /**
   * 用户商品解除绑定
   *
   * @param productUser 绑定对象
   * @return 未绑定则返回True, 已绑定则返回解绑结果
   */
  boolean unBindUserIfBounded(@NotNull ProductUser productUser);

  /**
   * 查询已绑定的商品及滤芯信息
   */
  List<ProductBasicVO> listBoundedProductsWithFilters(String userId, Pageable pageable);

  /**
   * 只查询已绑定的商品
   */
  List<ProductBasicVO> listBoundedProducts(String userId, Pageable pageable);

  /**
   * 查询已绑定商品总数
   */
  Long countBoundedProducts(String userId);

  /**
   * 商品滤芯绑定
   *
   * @param productFilter 绑定对象
   * @return 已绑定则返回True, 未绑定则返回绑定结果
   */
  boolean bindFilterIfNotBounded(@NotNull ProductFilter productFilter);

  /**
   * 查询商品滤芯是否绑定
   *
   * @param productId 商品id
   * @param filterId 滤芯id
   * @return 是否已绑定
   */
  boolean isFilterBounded(String productId, String filterId);

  /**
   * 批量绑定滤芯, 绑定前先清空记录
   *
   * @param productId 商品id
   * @param filterIds 绑定对象集合, <br>若不传也会清空记录</>
   * @return true or false
   */
  boolean bindFilterIfNotBounded(String productId,
      @NotNull List<String> filterIds);

  /**
   * 商品滤芯解除绑定
   *
   * @param productFilter 绑定对象
   * @return 未绑定则返回True, 已绑定则返回解绑结果
   */
  boolean unBindFilterIfBounded(@NotNull ProductFilter productFilter);

  /**
   * 批量解绑
   *
   * @param productId 商品id
   * @param filterIds 滤芯id集合
   * @return true or false
   */
  boolean unBindFilterIfBounded(String productId, @NotNull List<String> filterIds);

  /**
   * 解除商品所有的滤芯绑定
   *
   * @param productId 商品id
   * @return true or false
   */
  boolean unBindFilterAll(String productId);

  /**
   * 查询所有已经绑定了的滤芯id
   *
   * @param productId 商品id
   * @return 滤芯id集合
   */
  List<String> boundedFilterIds(String productId);

  /**
   * 根据商品id查询滤芯
   *
   * @param productId 商品id
   * @param params 附加参数
   * @param pageable 分页
   * @return list of filters
   */
  List<FilterBasicVO> listFilterByProductId(String productId, Map<String, ?> params,
      Pageable pageable);

  List<FilterBasicVO> listFilterByProductId(String productId);

  List<? extends FilterBasicVO> listFilterSkuByProductId(String productId, Map<String, ?> params,
      Pageable pageable);

  List<? extends FilterBasicVO> listFilterSkuByProductId(String productId);

  List<Product> listFilterByProductModel(String model, Pageable pageable);

  /**
   * 根据商品id查询对应的滤芯个数
   *
   * @param productId 商品id
   * @param params 附加参数
   * @return filter's total num
   */
  Long countFilterByProductId(String productId, Map<String, ?> params);

  Future<Object> getCRMProductInfo(String code);

  Optional<String> selectEncodeById(String id);

  String selectIdByEncode(String encode);

  Boolean selectIsModelExists(String model);

  boolean updatePriority(String id, Integer priority);

  List<ProductBasicVO> listProductWithParams(Map<String, Object> params, Pageable pageable);

	List<ProductSelectVO> listProductWithSelectInfo(String excludePromotionId,
			Map<String, Object> params,
			Function<PdPromotionInner, Boolean> isSelectProvider,
			Pageable pageable);

  Long countProductWithParams(Map<String, Object> params);

  boolean selectThirdProductExists(Long id, ProductSource source);

  Product selectThirdProduct(Long thirdId, ProductSource source);

  Product selectThirdProductByEncode(String encode, ProductSource source);

  List<String> listSFProductEncode();

  /**
   * 获取Wms产品
   */
  List<WmsProduct> getWmsProduct();

  Boolean updateByCode(WmsInventory wmsInventory);

    /**
     * 导入Excel添加商品
     */
    Integer importExcelToInsert(InputStream inputStream) throws Exception;

  /**
   * 根据折扣进行排序
   */
  List<Product> listProductsByDiscount(String rootShopId, String categoryId, String groupon, Pageable pageable, Direction direction, String sellerShopId, List<String> subCategoryIds, String priceStart, String priceEnd, ProductType type);

  List<Product> listProductsByPoint(String rootShopId, String categoryId, String groupon, Pageable pageable, Direction direction, String sellerShopId, List<String> subCategoryIds, String priceStart, String priceEnd, ProductType... type);

  /**
   * 计划发布商品
   */
  Integer productForSale(String productId, Date time);

  /**
   * @deprecated use {@link #checkStockAndInStock()} instead
   */
  @Deprecated
  Integer productInstock();

  /**
   * 库存小于安全库存的商品下架
   */
  Integer checkStockAndInStock();

  Integer updateProductAmountByMaxSkuAmount(String productId);

  @Transactional
  void importExcel(List<Product> products, List<TermRelationship> termRelationships,
      List<BrandProduct> brandProducts);


  /**
   * 与热搜词相关
   */
  List<String> listHotSearchedKeys(int displayNum);

  Long countHotSearchedProducts(String key);

  List<Product> filterHotSearchedProducts(String key, String order, String direction, Pageable page, ProductSearchVO productSearchVO);

  Boolean checkHotsearchKeyExists(String key);

  Boolean updateHotKeyState(String id, Integer state);

  Boolean setHotSearchKeyDisplayState(String id, Integer display);

  Boolean newHotSearchKey(String key, String pids);

  Boolean updateHotSearchKey(String id, String key, String pids);

  HotSearchKey getHotSearchKeyById(String id);

  Boolean removeHotSearchKeyByIds(List<String> ids);

  List<HotSearchKey> listValidHotSearchKeys(String key, Pageable page);

  Integer countTotalByKey(String key);
  /**
   * 修改与获取热词显示数量的参数
   */
  Boolean updateHotSearchKeyDisplayLimit(String value);

  Integer findHotSearchKeyDisplayLimit();

  List<Product> listProductsBySearchOrdered(@Deprecated String shopId, String key, Pageable page,
                                            String direction,ProductType type,String order);

  /**
   * 汉薇自营商品列表, 按照折扣或销量排序
   * @param shopId
   * @param page
   * @param direction
   * @param type
   * @param order
   * @return
   */
  List<Product> selfSupportByOrdered(@Deprecated String shopId, Pageable page, String direction,ProductType type,String order);

  List<ProductBasicVO> listCombine(String productId, Pageable pageable);

  Long countCombine(String productId);

  List<ProductBasicVO> listUnCombine(String productId, Pageable pageable);

  Long countUnCombine(String productId);

  List<SkuCombineVO> listCombineSku(String skuId, Long categoryId,
      String supplierId, List<String> selectedIds, String keyword,
      Pageable pageable);

  Long countCombineSku(String skuId, Long categoryId, String supplierId,
      List<String> selectedIds, String keyword);

  List<SkuCombineVO> listUnCombineSku(String skuId, String supplierId, Long categoryId,
      List<String> selectedIds, String keyword, Pageable pageable);

  Long countUnCombineSku(String skuId, String supplierId, Long categoryId,
      List<String> selectedIds, String keyword);

  Product productsBySale(String category, String sellerShopId);

  /**
   * 拼团已售罄
   * @param productId
   * @return
   */
  public boolean checkGroupSoldOut(String productId,String pCode);

  /**
   * 普通商品已售罄
   * @param productId
   * @return
   */
  public boolean checkCommonProductSoldOut(String productId);

  /**
   * 区分普通商品
   * @param productId
   * @return
   */
  public boolean isCommonProduct(String productId);

  /**
   * 商品已经售罄发送邮件
   * @param productId
   * @return
   */
  public boolean sendEmailWhenSoldOut(String productId) throws Exception;

  /**
   * 顺丰商品是否售罄
   * @param id
   * @param province 省
   * @param city 市
   * @param region 区
   * @return
   */
  List<SfStockVO> checkSfStock(String id, String province, String city , String region);

  /**
   * 查询顺丰商品
   * @return
   */
  List<Product> selectSfProduct();

  /**
   * 后台查询普通商品售罄
   * @param shopId
   * @param category
   * @param isGroupon
   * @param page
   * @param direction
   * @param type
   * @param status
   * @param reviewStatus
   * @return
   */
  List<Product> listProductsBySoldOutNotSf(String shopId, String category, String isGroupon,
                                           Pageable page,
                                           Direction direction, ProductType type, ProductStatus status,
                                           ProductReviewStatus reviewStatus);

  /**
   * 确认订单状态
   * @param productId
   * @param status
   * @return
   */
  boolean checkProductStatus(String productId, String status);

  /**
   * 自营列表
   * @param list
   * @return
   */
  public List<Product> filterSelfOperatedByPVo(List<Product> list);

  /**
   * 库存列表
   * @param list
   * @return
   */
  public List<Product> filterIsStockByPVo(List<Product> list);
}


