package com.xquark.service.product.vo;

import org.springframework.beans.BeanUtils;

import com.xquark.dal.model.Product;
import com.xquark.dal.model.Sku;

import java.math.BigDecimal;

/**
 * 用于展现商品的单个SKU信息
 *
 * @author odin
 */
public class ProductSkuVO extends Product {

  private static final long serialVersionUID = 1L;

  private Integer amount;

  private Sku sku;

  private Sku sourceSku;

  private Product sourceProduct;

  public ProductSkuVO(Product product) {
    BeanUtils.copyProperties(product, this);
  }

  public boolean isDistributed() {
    return sourceSku != null ? true : false;
  }

  public Sku getSku() {
    return sku;
  }

  public void setSku(Sku sku) {
    this.sku = sku;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }


  public Sku getSourceSku() {
    return sourceSku;
  }

  public void setSourceSku(Sku sourceSku) {
    this.sourceSku = sourceSku;
  }

  public Product getSourceProduct() {
    return sourceProduct;
  }

  public void setSourceProduct(Product sourceProduct) {
    this.sourceProduct = sourceProduct;
  }

  public BigDecimal getSkuPrice() {
    if (sourceSku != null) {
      return sourceSku.getPrice();
    } else {
      return sku.getPrice();
    }
  }

  public BigDecimal getSkuMarketPrice() {
    if (sourceSku != null) {
      return sourceSku.getMarketPrice();
    } else {
      return sku.getMarketPrice();
    }
  }
}
