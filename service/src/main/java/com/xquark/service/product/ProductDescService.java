package com.xquark.service.product;

import com.xquark.dal.model.ProductDesc;
import com.xquark.service.ArchivableEntityService;


public interface ProductDescService extends ArchivableEntityService<ProductDesc> {

  ProductDesc load(String productId);

  int update(ProductDesc productDesc);

}
