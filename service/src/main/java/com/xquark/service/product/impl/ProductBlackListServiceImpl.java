package com.xquark.service.product.impl;

import com.xquark.dal.mapper.ProductBlackListMapper;
import com.xquark.dal.model.ProductBlackList;
import com.xquark.dal.vo.ProductBlackListVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.product.ProductBlackListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("productBlackListService")
public class ProductBlackListServiceImpl extends BaseServiceImpl implements
    ProductBlackListService {

  @Autowired
  ProductBlackListMapper productBlackListMapper;

  @Override
  public int insert(ProductBlackList team) {
    return productBlackListMapper.insert(team);
  }

  @Override
  public int insertOrder(ProductBlackList productBlackList) {
    return productBlackListMapper.insert(productBlackList);
  }

  @Override
  public ProductBlackListVO load(String id) {
    return productBlackListMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return productBlackListMapper.updateForArchive(id);
  }

  @Override
  public int update(ProductBlackList record) {
    return productBlackListMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<ProductBlackListVO> list(Pageable pager, Map<String, Object> params) {
    return productBlackListMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return productBlackListMapper.selectCnt(params);
  }

  /**
   * 删除黑名单商品
   */
  @Override
  public int deleteByProductId(String userId, String productId) {
    return productBlackListMapper.deleteByProductId(userId, productId);
  }

}
