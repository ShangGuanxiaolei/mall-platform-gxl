package com.xquark.service.product;

import com.xquark.dal.model.SfAppConfig;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.service.http.pojo.sf.product.SFProduct;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * created by
 *
 * @author wangxinhua at 18-6-1 下午2:31
 */
public interface SFProductService {

  boolean syncPdList(List<SFProduct> sfProducts, Set<String> baseEncodes,
      User user, Shop shop);

  boolean syncPd(SFProduct product, User user, Shop shop);

  Set<String> loadTargetProductEncodes();

  void pullProducts(Integer limit);

  void pullProductByEncode(String encode);

  void pullProductDetails(String encode);

  SfAppConfig loadConfig();

  void renewToken();

  Map<String, Object> loadUserInfoForAdmin();

  boolean needRenewToken(Date now, Date expiredAt);

  /**
   * 校验顺丰商品是否下架
   * @param encode
   * @return
   */
  boolean checkProductIdShowStatus(String encode);

  /**
   * 顺丰商品自动下架
   * @return
   */
  boolean autoInstockSF();

  /**
   * 保存顺丰物流号
   * @param logisticsNo
   * @return
   */
  int saveSFlogistics(String logisticsNo,String orderNo);

  /**
   * 保存顺丰拆单的订单号
   * @param partnerOrderNo
   * @return
   */
  int saveSFsplitOrder(String partnerOrderNo,String orderNo);

//  /**
//   * 判断订单状态
//   * @param orderNo
//   * @param status
//   * @return
//   */
//  int checkOrderStatus(String orderNo,String status);

  /**
   * 更改顺丰订单状态
   * @param orderNo
   * @param status
   * @return
   */
  int updateSFOrderStatus(String orderNo,String status);

  /**
   * 验证开发者账号是否一致
   * @param clientId
   * @return
   */
  int checkDevAccount(String clientId);

  /**
   * 修改顺丰商品状态
   * @param list
   * @return
   */
  int updateSFProductStatus(List<String> list,String status);

}
