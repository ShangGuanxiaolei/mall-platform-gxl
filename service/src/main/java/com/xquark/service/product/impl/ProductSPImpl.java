package com.xquark.service.product.impl;

import com.xquark.dal.model.ActivitySP;
import com.xquark.service.product.ProductSP;

/**
 * 特价抢购商品
 *
 * @author seker
 */
public class ProductSPImpl implements ProductSP {

  private ActivitySP activitySP;
  private Integer boughtAmount;

  //是否已经开放购买
  public boolean onSale() {
    if (activitySP == null) {
      return true;
    }
    if (activitySP.getStartTime().getTime() > System.currentTimeMillis()) {
      return false;
    } else {
      return true;
    }
  }

  //已购买数量
  public Integer getBoughtAmount() {
    return boughtAmount;
  }

  public void setBoughtAmount(Integer amount) {
    this.boughtAmount = amount;
  }

  public ActivitySP getFirstActivity() {
    return activitySP;
  }

  public void setFirstActivity(ActivitySP activitySP) {
    this.activitySP = activitySP;
  }

  /**
   * 是否超出购买限制
   *
   * @param amount 新购买量
   */
  public boolean overLimit(Integer amount) {
    if (activitySP == null) {
      return false;
    }

    if (activitySP.getMaxQty() == 0) {
      return false;
    }
    if (boughtAmount == null) {
      boughtAmount = 0;
    }
    if (amount == null) {
      amount = 0;
    }

    if (boughtAmount + amount > activitySP.getMaxQty()) {
      return true;
    } else {
      return false;
    }
  }
}
