package com.xquark.service.brand;

import com.xquark.dal.model.Brand;
import com.xquark.dal.type.ProductSource;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * created by
 *
 * @author wangxinhua at 18-6-3 下午5:17
 */
public interface BrandService {

  boolean save(Brand brand);

  boolean deleteByPrimaryKey(String id);

  boolean batchDelete(List<String> id);

  Brand load(String id);

  Brand loadByThird(String thirdId, ProductSource source);

  // FIXME 硬编码查询顺丰品牌
  Brand loadSFBrand();

  boolean update(Brand brand);

  boolean addProductBrand(String productId, String brandId);

  List<? extends Brand> list(Map<String, Object> params);

  /**
   * 查询商品关联的品牌
   */
  List<? extends Brand> listRelatedBrand(@Param("productId") String productId);

  Long count(Map<String, Object> params);

  /**
   * 更新商品的品牌信息
   */
  boolean updateProductBrand(String productId, List<String> ids);

  // FIXME 硬编码 查询欧飞品牌的产品ID
  List<Long> loadOFBrandProduct();
}

