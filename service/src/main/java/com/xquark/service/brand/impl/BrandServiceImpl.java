package com.xquark.service.brand.impl;

import com.google.common.base.Preconditions;
import com.xquark.aop.anno.NotNull;
import com.xquark.dal.mapper.BrandMapper;
import com.xquark.dal.mapper.BrandProductMapper;
import com.xquark.dal.model.Brand;
import com.xquark.dal.model.BrandProduct;
import com.xquark.dal.type.ProductSource;
import com.xquark.service.brand.BrandService;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * created by
 *
 * @author wangxinhua at 18-6-3 下午5:18
 */
@Service
public class BrandServiceImpl implements BrandService {

  private final BrandMapper brandMapper;

  private final BrandProductMapper brandProductMapper;

  @Autowired
  public BrandServiceImpl(BrandMapper brandMapper,
      BrandProductMapper brandProductMapper) {
    this.brandMapper = brandMapper;
    this.brandProductMapper = brandProductMapper;
  }

  @Override
  public boolean save(Brand brand) {
    if (null != brand.getId()) {
      return brandMapper.insert(brand) > 0;
    } else {
      return brandMapper.insertRaw(brand) > 0;
    }
  }

  @Override
  public boolean deleteByPrimaryKey(String id) {
    Preconditions.checkNotNull(id);
    return brandMapper.deleteByPrimaryKey(id)>0;
  }

  @Override
  public boolean batchDelete(List<String> ids) {
    return  brandMapper.batchDelete(ids) > 0;
  }

  @Override
  public Brand load(String id) {
    return brandMapper.selectByPrimaryKey(id);
  }

  @Override
  public Brand loadByThird(String thirdId, ProductSource source) {
    return brandMapper.selectByThird(thirdId, source);
  }

  @Override
  public Brand loadSFBrand() {
    return brandMapper.selectSFBrand();
  }

  @Override
  public boolean update(Brand brand) {
    return brandMapper.updateByPrimaryKeySelective(brand) > 0;
  }

  @Override
  public boolean addProductBrand(String productId, String brandId) {
    boolean isAdded = brandProductMapper.selectExistsByProductIdAndBrandId(productId, brandId);
    if (!isAdded) {
      BrandProduct brandProduct = new BrandProduct(productId, brandId);
      return brandProductMapper.insert(brandProduct) > 0;
    }
    return true;
  }

  @Override
  public List<? extends Brand> list(Map<String, Object> params) {
    return brandMapper.list(params);
  }

  @Override
  public List<? extends Brand> listRelatedBrand(@NotBlank String productId) {
    return brandMapper.listRelatedBrand(productId);
  }

  @Override
  public Long count(Map<String, Object> params) {
    return brandMapper.count(params);
  }

  @Override
  @Transactional
  public boolean updateProductBrand(@NotBlank String productId,
      @NotNull @NotEmpty List<String> ids) {
    brandProductMapper.deleteRelatedBrand(productId);
    brandProductMapper.batchInsertBrandWithProduct(productId, ids);
    return true;
  }

  @Override
  public List<Long> loadOFBrandProduct() {
    return brandProductMapper.selectOFBrandProduct();
  }

}
