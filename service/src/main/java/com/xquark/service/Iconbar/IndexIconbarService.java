package com.xquark.service.Iconbar;

import com.xquark.dal.model.Iconbar;

import java.util.List;

public interface IndexIconbarService {

    List<Iconbar> getIndexIconbarList(Integer identity);
}
