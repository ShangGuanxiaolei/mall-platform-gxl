package com.xquark.service.Iconbar.impl;

import com.xquark.dal.mapper.IndexIconbarMapper;
import com.xquark.dal.model.Iconbar;
import com.xquark.service.Iconbar.IndexIconbarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 新客逻辑,首页新图标服务层
 */
@Service
public class IndexIconbarServiceImpl implements IndexIconbarService {

    @Autowired
    private IndexIconbarMapper indexIconbarMapper;

    @Override
    public List<Iconbar> getIndexIconbarList(Integer identity) {
        return indexIconbarMapper.selectIndexIconbarList(identity);
    }
}
