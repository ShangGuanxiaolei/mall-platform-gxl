package com.xquark.service.partner.impl;

import com.xquark.dal.mapper.PartnerProductCommissionDeMapper;
import com.xquark.dal.mapper.PartnerProductCommissionMapper;
import com.xquark.dal.model.PartnerProductCommission;
import com.xquark.dal.model.PartnerProductCommissionDe;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.partner.PartnerProductComDeService;
import com.xquark.service.partner.PartnerProductComService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("partnerProductCommissionDe")
public class PartnerProductComDeServiceImpl extends BaseServiceImpl implements
    PartnerProductComDeService {

  @Autowired
  PartnerProductCommissionDeMapper partnerProductCommissionDeMapper;

  @Override
  public int deleteByPrimaryKey(String id) {
    return partnerProductCommissionDeMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(PartnerProductCommissionDe record) {
    return partnerProductCommissionDeMapper.insert(record);
  }

  @Override
  public int insertSelective(PartnerProductCommissionDe record) {
    return partnerProductCommissionDeMapper.insertSelective(record);
  }

  @Override
  public PartnerProductCommissionDe selectByPrimaryKey(String id) {
    return partnerProductCommissionDeMapper.selectByPrimaryKey(id);
  }

  @Override
  public int updateByPrimaryKeySelective(PartnerProductCommissionDe record) {
    return partnerProductCommissionDeMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(PartnerProductCommissionDe record) {
    return partnerProductCommissionDeMapper.updateByPrimaryKey(record);
  }

  @Override
  public int updateForArchive(String id) {
    return partnerProductCommissionDeMapper.updateForArchive(id);
  }


}
