package com.xquark.service.partner.impl;

import com.xquark.dal.mapper.PartnerShopCommissionMapper;
import com.xquark.dal.mapper.TwitterShopCommissionMapper;
import com.xquark.dal.model.PartnerShopCommission;
import com.xquark.dal.model.TwitterShopCommission;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.partner.PartnerShopComService;
import com.xquark.service.twitter.TwitterShopComService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("partnerShopComService")
public class PartnerShopComServiceImpl extends BaseServiceImpl implements PartnerShopComService {

  @Autowired
  PartnerShopCommissionMapper partnerShopCommissionMapper;

  @Override
  public int deleteByPrimaryKey(String id) {
    return partnerShopCommissionMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(PartnerShopCommission record) {
    return partnerShopCommissionMapper.insert(record);
  }

  @Override
  public int insertSelective(PartnerShopCommission record) {
    return partnerShopCommissionMapper.insertSelective(record);
  }

  @Override
  public PartnerShopCommission selectByPrimaryKey(String id) {
    return partnerShopCommissionMapper.selectByPrimaryKey(id);
  }

  @Override
  public int updateByPrimaryKeySelective(PartnerShopCommission record) {
    return partnerShopCommissionMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(PartnerShopCommission record) {
    return partnerShopCommissionMapper.updateByPrimaryKey(record);
  }

  @Override
  public int updateForArchive(String id) {
    return partnerShopCommissionMapper.updateForArchive(id);
  }

  @Override
  public List<PartnerShopCommission> selectByShopId(String shopId) {
    return partnerShopCommissionMapper.selectByShopId(shopId);
  }


}
