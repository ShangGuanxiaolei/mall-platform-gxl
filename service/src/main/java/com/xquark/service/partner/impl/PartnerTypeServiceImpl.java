package com.xquark.service.partner.impl;

import com.xquark.dal.mapper.PartnerTypeMapper;
import com.xquark.dal.model.PartnerType;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.partner.PartnerTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("partnerTypeService")
public class PartnerTypeServiceImpl extends BaseServiceImpl implements PartnerTypeService {

  @Autowired
  PartnerTypeMapper partnerTypeMapper;

  @Override
  public int deleteByPrimaryKey(String id) {
    return partnerTypeMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(PartnerType record) {
    return partnerTypeMapper.insert(record);
  }

  @Override
  public int insertSelective(PartnerType record) {
    return partnerTypeMapper.insertSelective(record);
  }

  @Override
  public PartnerType selectByPrimaryKey(String id) {
    return partnerTypeMapper.selectByPrimaryKey(id);
  }

  @Override
  public int updateByPrimaryKeySelective(PartnerType record) {
    return partnerTypeMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(PartnerType record) {
    return partnerTypeMapper.updateByPrimaryKey(record);
  }

  @Override
  public int updateForArchive(String id) {
    return partnerTypeMapper.updateForArchive(id);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<PartnerType> list(Pageable pager, Map<String, Object> params) {
    return partnerTypeMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return partnerTypeMapper.selectCnt(params);
  }


  /**
   * 查找所有设置的合伙人类型
   */
  @Override
  public List<PartnerType> listByShopId(String shopId) {
    return partnerTypeMapper.listByShopId(shopId);
  }

}
