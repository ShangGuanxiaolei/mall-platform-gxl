package com.xquark.service.partner;

import com.xquark.dal.model.UserCard;
import com.xquark.dal.model.UserPartner;
import com.xquark.dal.status.FamilyCardStatus;
import com.xquark.dal.vo.PartnerCmVO;
import com.xquark.dal.vo.PartnerOrderCmVO;
import com.xquark.dal.vo.UserPartnerApplyVO;
import com.xquark.service.ArchivableEntityService;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface UserPartnerService extends ArchivableEntityService<UserPartner> {

  /**
   * UserPartner
   */
  int create(UserPartner userPartner);

  /**
   * UserPartner
   */
  int update(UserPartner userPartner);

  /**
   * 通过 id UserPartner
   */
  UserPartner load(String id);

  List<PartnerCmVO> selectUserPartnersCmByShopId(String shopId, Map<String, Object> params,
      Pageable pageable);

  Long countUserPartnersCmByShopId(String shopId, Map<String, Object> params);

  UserPartner selectActiveUserPartnersByUserIdAndShopId(String userId, String ownerShopId);

  /**
   * 自动将状态为申请中的合伙人状态变更为已生效
   */
  int autoAudit();

  // 获取今日数据,本周订单,推客,交易额,佣金数
  Map getSummary(String shopId);

  List<UserPartner> selectActiveUserPartnersByShopId(String shopId);

  List<PartnerOrderCmVO> listPartnerCommissionByShopId(String shopId, Map<String, Object> params,
      Pageable pageable);

  Long countPartnerCommissionByShopId(String shopId, Map<String, Object> params);

  /**
   * 合伙人申请列表
   */
  List<UserPartnerApplyVO> listPartnerApply(Map<String, Object> params, Pageable pageable);

  Long countPartnerApply(Map<String, Object> params);

  UserPartner selectUserPartnersByUserIdAndShopId(String userId, String shopId);

}
