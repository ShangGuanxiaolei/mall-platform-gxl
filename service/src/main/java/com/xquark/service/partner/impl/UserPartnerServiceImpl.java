package com.xquark.service.partner.impl;


import com.xquark.dal.mapper.UserCardMapper;
import com.xquark.dal.mapper.UserPartnerMapper;
import com.xquark.dal.model.UserCard;
import com.xquark.dal.model.UserPartner;
import com.xquark.dal.status.FamilyCardStatus;
import com.xquark.dal.vo.PartnerCmVO;
import com.xquark.dal.vo.PartnerOrderCmVO;
import com.xquark.dal.vo.UserPartnerApplyVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.partner.UserPartnerService;
import com.xquark.userFamily.UserCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;


@Service("userPartnerService")
public class UserPartnerServiceImpl extends BaseServiceImpl implements UserPartnerService {

  @Autowired
  private UserPartnerMapper userPartnerMapper;


  @Override
  public int create(UserPartner userPartner) {
    return userPartnerMapper.insert(userPartner);
  }

  @Override
  public int update(UserPartner userPartner) {
    return userPartnerMapper.updateByPrimaryKey(userPartner);
  }

  @Override
  public int insert(UserPartner userPartner) {
    return userPartnerMapper.insert(userPartner);
  }

  @Override
  public int insertOrder(UserPartner userPartner) {
    return userPartnerMapper.insert(userPartner);
  }

  @Override
  public UserPartner load(String id) {
    return userPartnerMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<PartnerCmVO> selectUserPartnersCmByShopId(String shopId, Map<String, Object> params,
      Pageable pageable) {
    return userPartnerMapper.selectUserPartnersCmByShopId(shopId, params, pageable);
  }

  @Override
  public Long countUserPartnersCmByShopId(String shopId, Map<String, Object> params) {
    return userPartnerMapper.countUserPartnersCmByShopId(shopId, params);
  }

  @Override
  public UserPartner selectActiveUserPartnersByUserIdAndShopId(String userId, String ownerShopId) {
    return userPartnerMapper.selectActiveUserPartnersByUserIdAndShopId(userId, ownerShopId);
  }

  @Override
  public int delete(String id) {
    return userPartnerMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int undelete(String id) {
    return userPartnerMapper.unDeleteByPrimaryKey(id);
  }


  @Override
  public int autoAudit() {
    return userPartnerMapper.autoAudit();
  }

  @Override
  public Map getSummary(String shopId) {
    // 缓存今天的各项值
    Map<String, Object> info = new HashMap<String, Object>();
    // 缓存一周的各项值
    Map<String, Object> nums = new HashMap<String, Object>();

    Map<String, Object> result = new HashMap<String, Object>();
    ArrayList dates = getWeekDay();

    // 订单数
    ArrayList orderlist = (ArrayList) userPartnerMapper.getOrder(shopId, dates);
    HashMap ordermap = formatList(orderlist);
    info.put("order", ordermap.get("today"));
    nums.put("order", ordermap.get("week"));

    // 合伙人数
    ArrayList partnerlist = (ArrayList) userPartnerMapper.getPartner(shopId, dates);
    HashMap partnermap = formatList(partnerlist);
    info.put("partner", partnermap.get("today"));
    nums.put("partner", partnermap.get("week"));

    // 交易额
    ArrayList amountlist = (ArrayList) userPartnerMapper.getAmount(shopId, dates);
    HashMap amountmap = formatList(amountlist);
    info.put("amount", amountmap.get("today"));
    nums.put("amount", amountmap.get("week"));

    // 佣金
    ArrayList commissionlist = (ArrayList) userPartnerMapper.getCommission(shopId, dates);
    HashMap commissionmap = formatList(commissionlist);
    info.put("commission", commissionmap.get("today"));
    nums.put("commission", commissionmap.get("week"));

    result.put("info", info);
    result.put("nums", nums);
    return result;
  }

  @Override
  public List<UserPartner> selectActiveUserPartnersByShopId(String shopId) {
    return userPartnerMapper.selectUserPartnersByShopId(shopId);
  }

  @Override
  public List<PartnerOrderCmVO> listPartnerCommissionByShopId(String shopId,
      Map<String, Object> params, Pageable pageable) {
    return userPartnerMapper.listPartnerCommissionByShopId(shopId, params, pageable);
  }

  @Override
  public Long countPartnerCommissionByShopId(String shopId, Map<String, Object> params) {
    return userPartnerMapper.countPartnerCommissionByShopId(shopId, params);
  }

  @Override
  public UserPartner selectUserPartnersByUserIdAndShopId(String userId, String shopId) {
    return userPartnerMapper.selectUserPartnersByUserIdAndShopId(userId, shopId);
  }

  /**
   * 获取一周内的所有日期
   */
  private static ArrayList getWeekDay() {
    ArrayList<String> list = new ArrayList<String>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Calendar calendar = Calendar.getInstance();
    while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
      calendar.add(Calendar.DATE, -1);
    }
    for (int i = 0; i < 7; i++) {
      list.add(sdf.format(calendar.getTime()));
      calendar.add(Calendar.DATE, 1);
    }
    return list;
  }

  /**
   * 将从数据库中取出的每天对应的各项值map数据结构做一次格式化 返回当天的值，如数据库没有则是0,同时返回周一到周日的值list(如果某天没有值，则为0)
   */
  private HashMap formatList(ArrayList<Map> list) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String today = "0";
    String temp_num = "0";
    ArrayList weekvalue = new ArrayList();
    // 今天
    String nowstr = sdf.format(new Date());
    HashMap result = new HashMap();
    // 得到一周内的每天的str字符串
    ArrayList<String> dates = getWeekDay();
    // 循环得到当天的值和周一到周天顺序每天的值
    for (String date : dates) {
      temp_num = "0";
      for (Map value : list) {
        String s_date = (String) value.get("s_date");
        String c_num = "" + value.get("c_num");
        if (nowstr.equals(s_date)) {
          today = c_num;
        }
        if (date.equals(s_date)) {
          temp_num = c_num;
          break;
        }
      }
      weekvalue.add(temp_num);
    }
    result.put("today", today);
    result.put("week", weekvalue);
    return result;
  }

  /**
   * 合伙人申请列表
   */
  @Override
  public List<UserPartnerApplyVO> listPartnerApply(Map<String, Object> params, Pageable pageable) {
    return userPartnerMapper.listPartnerApply(params, pageable);
  }

  @Override
  public Long countPartnerApply(Map<String, Object> params) {
    return userPartnerMapper.countPartnerApply(params);
  }

}
