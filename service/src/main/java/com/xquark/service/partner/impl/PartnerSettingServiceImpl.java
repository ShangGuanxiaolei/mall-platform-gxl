package com.xquark.service.partner.impl;


import com.xquark.dal.mapper.FamilyCardSettingMapper;
import com.xquark.dal.mapper.PartnerSettingMapper;
import com.xquark.dal.model.FamilyCardSetting;
import com.xquark.dal.model.PartnerSetting;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.partner.PartnerSettingService;
import com.xquark.userFamily.FamilyCardSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.commons.lang3.StringUtils;
import java.util.List;


@Service("partnerSettingService")
public class PartnerSettingServiceImpl extends BaseServiceImpl implements PartnerSettingService {

  @Autowired
  private PartnerSettingMapper partnerSettingMapper;


  @Override
  public int create(PartnerSetting partnerSetting) {
    return partnerSettingMapper.insert(partnerSetting);
  }

  @Override
  public int update(PartnerSetting familyCardSetting) {
    return partnerSettingMapper.updateByPrimaryKey(familyCardSetting);
  }

  @Override
  public int insert(PartnerSetting partnerSetting) {
    return partnerSettingMapper.insert(partnerSetting);
  }

  @Override
  public int insertOrder(PartnerSetting partnerSetting) {
    return partnerSettingMapper.insert(partnerSetting);
  }

  @Override
  public PartnerSetting load(String id) {
    return partnerSettingMapper.selectByPrimaryKey(id);
  }

  @Override
  public PartnerSetting loadByShopId(String shopId) {
    return partnerSettingMapper.selectByShopId(shopId);
  }

  @Override
  public List<PartnerSetting> loadDefaultList(String shopId) {
    return null;
  }

//	@Override
//	public FamilyCardSetting findByUser(String userId) {
//		return familyCardSettingMapper.selectByUserId(userId);
//	}
//
//	@Override
//	public FamilyCardSetting mine() {
//		return familyCardSettingMapper.selectByUserId(getCurrentUser().getId());
//	}

  @Override
  public int delete(String id) {
    return partnerSettingMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int undelete(String id) {
    return partnerSettingMapper.unDeleteByPrimaryKey(id);
  }

  @Override
  @Transactional
  public int save(PartnerSetting partnerSetting) {
    int ret = 0;

    if (StringUtils.isEmpty(partnerSetting.getId())) {
      partnerSettingMapper.insert(partnerSetting);
    } else {
      ret = partnerSettingMapper.updateByPrimaryKey(partnerSetting);
    }

    return ret;
  }
}
