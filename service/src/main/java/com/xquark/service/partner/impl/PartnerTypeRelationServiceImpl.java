package com.xquark.service.partner.impl;

import com.xquark.dal.mapper.PartnerTypeRelationMapper;
import com.xquark.dal.model.PartnerTypeRelation;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.partner.PartnerTypeRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("partnerTypeRelationService")
public class PartnerTypeRelationServiceImpl extends BaseServiceImpl implements
    PartnerTypeRelationService {

  @Autowired
  PartnerTypeRelationMapper partnerTypeRelationMapper;

  @Override
  public int deleteByPrimaryKey(String id) {
    return partnerTypeRelationMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(PartnerTypeRelation record) {
    return partnerTypeRelationMapper.insert(record);
  }

  @Override
  public int insertSelective(PartnerTypeRelation record) {
    return partnerTypeRelationMapper.insertSelective(record);
  }

  @Override
  public PartnerTypeRelation selectByPrimaryKey(String id) {
    return partnerTypeRelationMapper.selectByPrimaryKey(id);
  }

  @Override
  public int updateByPrimaryKeySelective(PartnerTypeRelation record) {
    return partnerTypeRelationMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(PartnerTypeRelation record) {
    return partnerTypeRelationMapper.updateByPrimaryKey(record);
  }

  @Override
  public int updateForArchive(String id) {
    return partnerTypeRelationMapper.updateForArchive(id);
  }

  /**
   * 获取某个合伙人的类型设置
   */
  @Override
  public List<PartnerTypeRelation> selectByUserId(String shopId, String userId) {
    return partnerTypeRelationMapper.selectByUserId(shopId, userId);
  }

  /**
   * 删除某个合伙人所有的类型设置
   */
  @Override
  public int deleteByUserId(String shopId, String userId) {
    return partnerTypeRelationMapper.deleteByUserId(shopId, userId);
  }


}
