package com.xquark.service.partner;

import com.xquark.dal.model.PartnerTypeRelation;

import java.util.List;


public interface PartnerTypeRelationService {

  int deleteByPrimaryKey(String id);

  int insert(PartnerTypeRelation record);

  int insertSelective(PartnerTypeRelation record);

  PartnerTypeRelation selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PartnerTypeRelation record);

  int updateByPrimaryKey(PartnerTypeRelation record);

  int updateForArchive(String id);

  /**
   * 获取某个合伙人的类型设置
   */
  List<PartnerTypeRelation> selectByUserId(String shopId, String userId);

  /**
   * 删除某个合伙人所有的类型设置
   */
  int deleteByUserId(String shopId, String userId);
}
