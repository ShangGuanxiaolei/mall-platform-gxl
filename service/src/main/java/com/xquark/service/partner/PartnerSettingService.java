package com.xquark.service.partner;

import com.xquark.dal.model.FamilyCardSetting;
import com.xquark.dal.model.PartnerSetting;
import com.xquark.service.ArchivableEntityService;

import java.util.List;


public interface PartnerSettingService extends ArchivableEntityService<PartnerSetting> {

  /**
   * 创建配置信息
   */
  int create(PartnerSetting partnerSetting);

  /**
   * 更新配置信息
   */
  int update(PartnerSetting partnerSetting);

  /**
   * 通过 id 查找配置信息
   */
  PartnerSetting load(String id);

  /**
   * 通过 shopId 查找配置信息
   */
  PartnerSetting loadByShopId(String shopId);

  /**
   * 通过 shopId 查找配置信息
   */
  List<PartnerSetting> loadDefaultList(String shopId);

  int save(PartnerSetting partnerSetting);

//    /**
//     * 通过 用户id 查找配置
//     * @param userId
//     * @return
//     */
//    FamilyCardSetting findByUser(String userId);

//    /**
//     * 获取当前登录用户的配置信息
//     * @return
//     */
//    FamilyCardSetting mine();
}
