package com.xquark.service.partner;

import com.xquark.dal.model.PartnerProductCommissionDe;


public interface PartnerProductComDeService {

  int deleteByPrimaryKey(String id);

  int insert(PartnerProductCommissionDe record);

  int insertSelective(PartnerProductCommissionDe record);

  PartnerProductCommissionDe selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PartnerProductCommissionDe record);

  int updateByPrimaryKey(PartnerProductCommissionDe record);

  int updateForArchive(String id);
}
