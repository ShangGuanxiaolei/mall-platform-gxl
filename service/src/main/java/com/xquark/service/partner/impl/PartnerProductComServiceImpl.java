package com.xquark.service.partner.impl;

import com.xquark.dal.mapper.PartnerProductCommissionDeMapper;
import com.xquark.dal.mapper.PartnerProductCommissionMapper;
import com.xquark.dal.mapper.PartnerShopCommissionMapper;
import com.xquark.dal.model.PartnerProductCommission;
import com.xquark.dal.model.PartnerProductCommissionDe;
import com.xquark.dal.model.PartnerShopCommission;
import com.xquark.dal.type.CommissionType;
import com.xquark.dal.vo.PartnerProductCommissionVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.partner.PartnerProductComService;
import com.xquark.service.partner.PartnerShopComService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("partnerProductCommission")
public class PartnerProductComServiceImpl extends BaseServiceImpl implements
    PartnerProductComService {

  @Autowired
  PartnerProductCommissionMapper partnerProductCommissionMapper;

  @Autowired
  PartnerProductCommissionDeMapper partnerProductCommissionDeMapper;

  @Override
  public int deleteByPrimaryKey(String id) {
    return partnerProductCommissionMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(PartnerProductCommission record) {
    return partnerProductCommissionMapper.insert(record);
  }

  @Override
  public int insertSelective(PartnerProductCommission record) {
    return partnerProductCommissionMapper.insertSelective(record);
  }

  @Override
  public PartnerProductCommissionVO selectByPrimaryKey(String id) {
    PartnerProductCommissionVO vo = partnerProductCommissionMapper.selectByPrimaryKey(id);
    List<PartnerProductCommissionDe> des = partnerProductCommissionDeMapper.getByParentId(id);
    for (PartnerProductCommissionDe de : des) {
      if (de.getType() == CommissionType.PLATFORM) {
        vo.setPlatformDe(de);
      }
      if (de.getType() == CommissionType.TEAM) {
        vo.setTeamDe(de);
      }
      if (de.getType() == CommissionType.SHAREHOLDER) {
        vo.setShareholderDe(de);
      }
    }
    return vo;
  }

  @Override
  public int updateByPrimaryKeySelective(PartnerProductCommission record) {
    return partnerProductCommissionMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(PartnerProductCommission record) {
    return partnerProductCommissionMapper.updateByPrimaryKey(record);
  }

  @Override
  public int updateForArchive(String id) {
    return partnerProductCommissionMapper.updateForArchive(id);
  }


  /**
   * 服务端分页查询数据
   */
  @Override
  public List<PartnerProductCommissionVO> list(Pageable pager, Map<String, Object> params) {
    List<PartnerProductCommissionVO> vos = partnerProductCommissionMapper.list(pager, params);
    for (PartnerProductCommissionVO vo : vos) {
      String id = vo.getId();
      List<PartnerProductCommissionDe> des = partnerProductCommissionDeMapper.getByParentId(id);
      for (PartnerProductCommissionDe de : des) {
        if (de.getType() == CommissionType.PLATFORM) {
          vo.setPlatformDe(de);
        }
        if (de.getType() == CommissionType.TEAM) {
          vo.setTeamDe(de);
        }
        if (de.getType() == CommissionType.SHAREHOLDER) {
          vo.setShareholderDe(de);
        }
      }
    }
    return vos;
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return partnerProductCommissionMapper.selectCnt(params);
  }

  /**
   * 查询是否已经有该商品的分佣设置
   */
  @Override
  public Long selectByProductId(String shopId, String productId) {
    return partnerProductCommissionMapper.selectByProductId(shopId, productId);
  }

}
