package com.xquark.service.partner;

import com.xquark.dal.model.PartnerType;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface PartnerTypeService {

  int deleteByPrimaryKey(String id);

  int insert(PartnerType record);

  int insertSelective(PartnerType record);

  PartnerType selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PartnerType record);

  int updateByPrimaryKey(PartnerType record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<PartnerType> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 查找所有设置的合伙人类型
   */
  List<PartnerType> listByShopId(String shopId);
}
