package com.xquark.service.twitter.impl;

import com.xquark.dal.mapper.TwitterLevelMapper;
import com.xquark.dal.model.TwitterLevel;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.twitter.TwitterLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("twitterLevelService")
public class TwitterLevelServiceImpl extends BaseServiceImpl implements TwitterLevelService {

  @Autowired
  TwitterLevelMapper twitterLevelMapper;

  @Override
  public int deleteByPrimaryKey(String id) {
    return twitterLevelMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(TwitterLevel record) {
    return twitterLevelMapper.insert(record);
  }

  @Override
  public TwitterLevel selectByPrimaryKey(String id) {
    return twitterLevelMapper.selectByPrimaryKey(id);
  }

  @Override
  public int updateByPrimaryKey(TwitterLevel record) {
    return twitterLevelMapper.updateByPrimaryKey(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<TwitterLevel> list(Pageable pager, Map<String, Object> params) {
    return twitterLevelMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return twitterLevelMapper.selectCnt(params);
  }

  /**
   * 查询某种类型的所有推客等级
   */
  @Override
  public List<TwitterLevel> getByType(String shopId, String type) {
    return twitterLevelMapper.getByType(shopId, type);
  }

  /**
   * 获取某个用户的推客等级
   */
  @Override
  public TwitterLevel selectByUserId(String shopId, String userId) {
    return twitterLevelMapper.selectByUserId(shopId, userId);
  }

}
