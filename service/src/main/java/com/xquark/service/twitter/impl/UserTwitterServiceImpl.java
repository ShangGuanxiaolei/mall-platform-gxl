package com.xquark.service.twitter.impl;


import com.xquark.dal.mapper.TwitterAccessLogMapper;
import com.xquark.dal.mapper.UserTwitterMapper;
import com.xquark.dal.model.TwitterAccessLog;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserTwitter;
import com.xquark.dal.vo.PartnerOrderCmVO;
import com.xquark.dal.vo.TwitterChildrenCmDisplayVO;
import com.xquark.dal.vo.UserTwitterApplyVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.twitter.UserTwitterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;


@Service("userTwitterService")
public class UserTwitterServiceImpl extends BaseServiceImpl implements UserTwitterService {

  private final static Logger LOG = LoggerFactory.getLogger(UserTwitterServiceImpl.class);
  @Autowired
  private UserTwitterMapper userTwitterMapper;

  @Autowired
  private TwitterAccessLogMapper twitterAccessLogMapper;


  @Override
  public int create(UserTwitter userTwitter) {
    return userTwitterMapper.insert(userTwitter);
  }

  @Override
  public int update(UserTwitter userTwitter) {
    return userTwitterMapper.updateByPrimaryKeySelective(userTwitter);
  }

  @Override
  public int insert(UserTwitter userTwitter) {
    return userTwitterMapper.insert(userTwitter);
  }

  @Override
  public int insertOrder(UserTwitter userTwitter) {
    return userTwitterMapper.insert(userTwitter);
  }

  @Override
  public UserTwitter load(String id) {
    return userTwitterMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<UserTwitterApplyVO> selectUserTwitterApplyingByShopId(String shopId,
      Map<String, Object> params, Pageable pageable) {
    return userTwitterMapper.selectUserTwitterApplyingByShopId(shopId, params, pageable);
  }

  @Override
  public Long countUserTwitterApplyingByShopId(String shopId, Map<String, Object> params) {
    return userTwitterMapper.countUserTwitterApplyingByShopId(shopId, params);
  }

  @Override
  public List<UserTwitter> selectByUserId(String userId) {
    return userTwitterMapper.selectByUserId(userId);
  }

  @Override
  public int delete(String id) {
    return userTwitterMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int undelete(String id) {
    return userTwitterMapper.unDeleteByPrimaryKey(id);
  }

  @Override
  public Map getSummary(String shopId) {
    // 缓存今天的各项值
    Map<String, Object> info = new HashMap<String, Object>();
    // 缓存一周的各项值
    Map<String, Object> nums = new HashMap<String, Object>();

    Map<String, Object> result = new HashMap<String, Object>();
    ArrayList dates = getWeekDay();

    // 订单数
    ArrayList orderlist = (ArrayList) userTwitterMapper.getOrder(shopId, dates);
    HashMap ordermap = formatList(orderlist);
    info.put("order", ordermap.get("today"));
    nums.put("order", ordermap.get("week"));

    // 推客数
    ArrayList twitterlist = (ArrayList) userTwitterMapper.getTwitter(shopId, dates);
    HashMap twittermap = formatList(twitterlist);
    info.put("twitter", twittermap.get("today"));
    nums.put("twitter", twittermap.get("week"));

    // 交易额
    ArrayList amountlist = (ArrayList) userTwitterMapper.getAmount(shopId, dates);
    HashMap amountmap = formatList(amountlist);
    info.put("amount", amountmap.get("today"));
    nums.put("amount", amountmap.get("week"));

    // 佣金
    ArrayList commissionlist = (ArrayList) userTwitterMapper.getCommission(shopId, dates);
    HashMap commissionmap = formatList(commissionlist);
    info.put("commission", commissionmap.get("today"));
    nums.put("commission", commissionmap.get("week"));

    result.put("info", info);
    result.put("nums", nums);
    return result;
  }

  @Override
  public UserTwitter selectByUserIdAndShopId(String userId, String shopId) {
    return userTwitterMapper.selectByUserIdAndShopId(userId, shopId);
  }

  /**
   * 获取一周内的所有日期
   */
  private static ArrayList getWeekDay() {
    ArrayList<String> list = new ArrayList<String>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Calendar calendar = Calendar.getInstance();
    while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
      calendar.add(Calendar.DATE, -1);
    }
    for (int i = 0; i < 7; i++) {
      list.add(sdf.format(calendar.getTime()));
      calendar.add(Calendar.DATE, 1);
    }
    return list;
  }

  /**
   * 将从数据库中取出的每天对应的各项值map数据结构做一次格式化 返回当天的值，如数据库没有则是0,同时返回周一到周日的值list(如果某天没有值，则为0)
   */
  private HashMap formatList(ArrayList<Map> list) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String today = "0";
    String temp_num = "0";
    ArrayList weekvalue = new ArrayList();
    // 今天
    String nowstr = sdf.format(new Date());
    HashMap result = new HashMap();
    // 得到一周内的每天的str字符串
    ArrayList<String> dates = getWeekDay();
    // 循环得到当天的值和周一到周天顺序每天的值
    for (String date : dates) {
      temp_num = "0";
      for (Map value : list) {
        String s_date = (String) value.get("s_date");
        String c_num = "" + value.get("c_num");
        if (nowstr.equals(s_date)) {
          today = c_num;
        }
        if (date.equals(s_date)) {
          temp_num = c_num;
          break;
        }
      }
      weekvalue.add(temp_num);
    }
    result.put("today", today);
    result.put("week", weekvalue);
    return result;
  }


  /**
   * 推客结算记录列表
   */
  @Override
  public List<PartnerOrderCmVO> listCommissionByShopId(String shopId, Map<String, Object> params,
      Pageable pageable) {
    return userTwitterMapper.listCommissionByShopId(shopId, params, pageable);
  }

  /**
   * 推客结算记录
   */
  @Override
  public Long countCommissionByShopId(String shopId, Map<String, Object> params) {
    return userTwitterMapper.countCommissionByShopId(shopId, params);
  }

  @Override
  public List<TwitterChildrenCmDisplayVO> selectChildrenCmByRootId(String shopId,
      Map<String, Object> params, Pageable pageable) {
    return userTwitterMapper.selectChildrenCmByRootId(shopId, params, pageable);
  }

  @Override
  public Long countChildrenCmByRootId(String shopId, Map<String, Object> params) {
    return userTwitterMapper.countChildrenCmByRootId(shopId, params);
  }

  /**
   * 我的小店会员查询
   */
  @Override
  public List<User> getMyMembers(String shopId, Pageable pageable) {
    return userTwitterMapper.getMyMembers(shopId, pageable);
  }

  /**
   * 判断用户是否第一次进入推客中心
   */
  @Override
  public boolean isFirstTwitter(String userId) {
    boolean flag = false;
    long count = twitterAccessLogMapper.countByUserId(userId);
    if (count == 0) {
      flag = true;
    }
    TwitterAccessLog twitterAccessLog = new TwitterAccessLog();
    twitterAccessLog.setArchive(false);
    twitterAccessLog.setUserId(userId);
    twitterAccessLogMapper.insert(twitterAccessLog);
    return flag;
  }

}
