package com.xquark.service.twitter.impl;

import com.xquark.dal.mapper.TwitterShopCommissionMapper;
import com.xquark.dal.model.TwitterShopCommission;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.twitter.TwitterShopComService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("twitterShopComService")
public class TwitterShopComServiceImpl extends BaseServiceImpl implements TwitterShopComService {

  @Autowired
  TwitterShopCommissionMapper twitterShopCommissionMapper;

  @Override
  public int insert(TwitterShopCommission twitterShopCommission) {
    return twitterShopCommissionMapper.insert(twitterShopCommission);
  }

  @Override
  public int insertOrder(TwitterShopCommission twitterShopCommission) {
    return twitterShopCommissionMapper.insert(twitterShopCommission);
  }

  @Override
  public TwitterShopCommission load(String id) {
    return twitterShopCommissionMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return twitterShopCommissionMapper.updateForArchive(id);
  }


  @Override
  public int createDefault(TwitterShopCommission record) {
    record.setDefaultStatus(true);
    return twitterShopCommissionMapper.insert(record);
  }

  @Override
  public int createNonDefault(TwitterShopCommission record) {
    record.setDefaultStatus(false);
    return twitterShopCommissionMapper.insert(record);
  }

  @Override
  public int updateDefaultByShopId(TwitterShopCommission record) {
    return twitterShopCommissionMapper.updateDefaultCom(record);
  }

  @Override
  public TwitterShopCommission selectDefaultByshopId(String shopId) {
    return twitterShopCommissionMapper.selectDefaultByshopId(shopId);
  }

  @Override
  public List<TwitterShopCommission> selectNonDefaultByshopId(String shopId) {
    return twitterShopCommissionMapper.selectNonDefaultByshopId(shopId);
  }

  @Override
  public TwitterShopCommission selectDefault() {
    return twitterShopCommissionMapper.selectDefault();
  }

  @Override
  public String getTwitterAlias() {
    // 推客别名显示
    TwitterShopCommission twitterShopCommission = selectDefault();
    String twitterAlias = "推客";
    if (StringUtils.isNotEmpty(twitterShopCommission.getAlias())) {
      twitterAlias = twitterShopCommission.getAlias();
    }
    return twitterAlias;
  }

}
