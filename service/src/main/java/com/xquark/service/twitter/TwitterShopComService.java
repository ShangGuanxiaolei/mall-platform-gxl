package com.xquark.service.twitter;

import com.xquark.dal.model.TwitterShopCommission;
import com.xquark.service.BaseEntityService;

import java.util.List;


public interface TwitterShopComService extends BaseEntityService<TwitterShopCommission> {

  int deleteForArchive(String id);

  int createDefault(TwitterShopCommission record);

  int createNonDefault(TwitterShopCommission record);

  int updateDefaultByShopId(TwitterShopCommission record);

  TwitterShopCommission selectDefaultByshopId(String shopId);

  List<TwitterShopCommission> selectNonDefaultByshopId(String shopId);

  TwitterShopCommission selectDefault();

  String getTwitterAlias();
}
