package com.xquark.service.twitter;

import com.xquark.dal.model.TwitterProductCommission;
import com.xquark.dal.vo.TwitterProductCommissionVO;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface TwitterProductComService extends BaseEntityService<TwitterProductCommission> {

  TwitterProductCommissionVO load(String id);

  int insert(TwitterProductCommission twitterProductCommission);

  int deleteForArchive(String id);

  int update(TwitterProductCommission record);

  TwitterProductCommission selectComByProductIdAndShopId(String productId, String shopId);

  /**
   * 服务端分页查询数据
   */
  List<TwitterProductCommission> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 查询是否已经有该商品的分佣设置
   */
  Long selectByProductId(String shopId, String productId);

}

