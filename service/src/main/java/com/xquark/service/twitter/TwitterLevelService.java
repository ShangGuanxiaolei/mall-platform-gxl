package com.xquark.service.twitter;

import com.xquark.dal.model.TwitterLevel;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface TwitterLevelService {

  int deleteByPrimaryKey(String id);

  int insert(TwitterLevel record);

  TwitterLevel selectByPrimaryKey(String id);

  int updateByPrimaryKey(TwitterLevel record);

  /**
   * 服务端分页查询数据
   */
  List<TwitterLevel> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 查询某种类型的所有推客等级
   */
  List<TwitterLevel> getByType(String shopId, String type);

  /**
   * 获取某个用户的推客等级
   */
  TwitterLevel selectByUserId(String shopId, String userId);

}
