package com.xquark.service.twitter.impl;

import com.xquark.dal.mapper.TwitterLevelRelationMapper;
import com.xquark.dal.model.TwitterLevel;
import com.xquark.dal.model.TwitterLevelRelation;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.twitter.TwitterLevelRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("twitterLevelRelationService")
public class TwitterLevelRelationServiceImpl extends BaseServiceImpl implements
    TwitterLevelRelationService {

  @Autowired
  TwitterLevelRelationMapper twitterLevelRelationMapper;

  @Override
  public TwitterLevelRelation selectByPrimaryKey(String id) {
    return twitterLevelRelationMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(TwitterLevelRelation twitterLevelRelation) {
    return twitterLevelRelationMapper.insert(twitterLevelRelation);
  }

  @Override
  public int modify(TwitterLevelRelation twitterLevelRelation) {
    return twitterLevelRelationMapper.modify(twitterLevelRelation);
  }

  /**
   * @param id
   * @return
   */
  @Override
  public int delete(String id) {
    return twitterLevelRelationMapper.delete(id);
  }

  /**
   * 删除用户之前所有的推客等级信息
   */
  @Override
  public int deleteByUserId(String shopId, String userId) {
    return twitterLevelRelationMapper.deleteByUserId(shopId, userId);
  }

  /**
   * 得到某个用户的推客等级信息
   */
  @Override
  public TwitterLevel selectByUserId(String shopId, String userId) {
    return twitterLevelRelationMapper.selectByUserId(shopId, userId);
  }

  /**
   * 自动删除到期的推客会员
   */
  @Override
  public int autoClose() {
    return twitterLevelRelationMapper.autoClose();
  }

}
