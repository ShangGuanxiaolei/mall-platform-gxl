package com.xquark.service.twitter.impl;

import com.xquark.dal.mapper.TwitterProductCommissionMapper;
import com.xquark.dal.model.TwitterProductCommission;
import com.xquark.dal.vo.TwitterProductCommissionVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.twitter.TwitterProductComService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("twitterProductComService")
public class TwitterProductComServiceImpl extends BaseServiceImpl implements
    TwitterProductComService {

  @Autowired
  TwitterProductCommissionMapper twitterProductCommissionMapper;

  @Override
  public int insert(TwitterProductCommission twitterProductCommission) {
    return twitterProductCommissionMapper.insert(twitterProductCommission);
  }

  @Override
  public int insertOrder(TwitterProductCommission twitterProductCommission) {
    return twitterProductCommissionMapper.insert(twitterProductCommission);
  }

  @Override
  public TwitterProductCommissionVO load(String id) {
    return twitterProductCommissionMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return twitterProductCommissionMapper.updateForArchive(id);
  }

  @Override
  public int update(TwitterProductCommission record) {
    return twitterProductCommissionMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public TwitterProductCommission selectComByProductIdAndShopId(String productId, String shopId) {
    return twitterProductCommissionMapper.selectComByProductIdAndShopId(productId, shopId);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<TwitterProductCommission> list(Pageable pager, Map<String, Object> params) {
    return twitterProductCommissionMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return twitterProductCommissionMapper.selectCnt(params);
  }

  /**
   * 查询是否已经有该商品的分佣设置
   */
  @Override
  public Long selectByProductId(String shopId, String productId) {
    return twitterProductCommissionMapper.selectByProductId(shopId, productId);
  }

}
