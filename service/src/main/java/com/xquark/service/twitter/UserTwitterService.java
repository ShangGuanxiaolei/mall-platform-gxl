package com.xquark.service.twitter;

import com.xquark.dal.model.User;
import com.xquark.dal.model.UserTwitter;
import com.xquark.dal.vo.PartnerOrderCmVO;
import com.xquark.dal.vo.TwitterChildrenCmDisplayVO;
import com.xquark.dal.vo.UserTwitterApplyVO;
import com.xquark.service.ArchivableEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface UserTwitterService extends ArchivableEntityService<UserTwitter> {

  /**
   * UserTwitter
   */
  int create(UserTwitter userTwitter);

  /**
   * UserTwitter
   */
  int update(UserTwitter userTwitter);

  /**
   * 通过 id UserTwitter
   */
  UserTwitter load(String id);

  List<UserTwitterApplyVO> selectUserTwitterApplyingByShopId(String shopId,
      Map<String, Object> params, Pageable pageable);

  Long countUserTwitterApplyingByShopId(String shopId, Map<String, Object> params);

  List<UserTwitter> selectByUserId(String userId);

  // 获取今日数据,本周订单,推客,交易额,佣金数
  Map getSummary(String shopId);

  UserTwitter selectByUserIdAndShopId(String userId, String shopId);

  /**
   * 推客结算记录列表
   */
  List<PartnerOrderCmVO> listCommissionByShopId(String shopId, Map<String, Object> params,
      Pageable pageable);

  /**
   * 推客结算记录
   */
  Long countCommissionByShopId(String shopId, Map<String, Object> params);

  List<TwitterChildrenCmDisplayVO> selectChildrenCmByRootId(String shopId,
      Map<String, Object> params, Pageable pageable);

  Long countChildrenCmByRootId(String shopId, Map<String, Object> params);

  /**
   * 我的小店会员查询
   */
  List<User> getMyMembers(String shopId, Pageable pageable);

  /**
   * 判断用户是否第一次进入推客中心
   */
  boolean isFirstTwitter(String userId);

}
