package com.xquark.service.twitter;

import com.xquark.dal.model.TwitterLevel;
import com.xquark.dal.model.TwitterLevelRelation;

/**
 * Created by chh on 17-06-28.
 */
public interface TwitterLevelRelationService {

  TwitterLevelRelation selectByPrimaryKey(String id);

  int insert(TwitterLevelRelation twitterLevelRelation);

  int modify(TwitterLevelRelation twitterLevelRelation);

  /**
   * @param id
   * @return
   */
  int delete(String id);

  /**
   * 删除用户之前所有的推客等级信息
   */
  int deleteByUserId(String shopId, String userId);

  /**
   * 得到某个用户的推客等级信息
   */
  TwitterLevel selectByUserId(String shopId, String userId);

  /**
   * 自动删除到期的推客会员
   */
  int autoClose();

}
