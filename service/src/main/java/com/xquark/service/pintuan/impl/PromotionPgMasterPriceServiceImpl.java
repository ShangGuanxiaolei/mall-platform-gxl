package com.xquark.service.pintuan.impl;

import com.xquark.dal.mapper.PromotionPgMasterPriceMapper;
import com.xquark.dal.model.PromotionPgMasterPrice;
import com.xquark.service.pintuan.PromotionPgMasterPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: gxl
 */
@Service
public class PromotionPgMasterPriceServiceImpl implements PromotionPgMasterPriceService {

    @Autowired
    private PromotionPgMasterPriceMapper promotionPgMasterPriceMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return promotionPgMasterPriceMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(PromotionPgMasterPrice record) {
        return promotionPgMasterPriceMapper.insert(record);
    }

    @Override
    public int insertSelective(PromotionPgMasterPrice record) {
        return promotionPgMasterPriceMapper.insertSelective(record);
    }

    @Override
    public PromotionPgMasterPrice selectByPrimaryKey(Integer id) {
        return promotionPgMasterPriceMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(PromotionPgMasterPrice record) {
        return promotionPgMasterPriceMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(PromotionPgMasterPrice record) {
        return promotionPgMasterPriceMapper.updateByPrimaryKey(record);
    }

    @Override
    public PromotionPgMasterPrice selectPromotionPgMasterPriceByDetailCode(String pDetailCode) {
        return promotionPgMasterPriceMapper.selectPromotionPgMasterPriceByDetailCode(pDetailCode);
    }
}
