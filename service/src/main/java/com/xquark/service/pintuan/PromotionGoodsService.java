package com.xquark.service.pintuan;

import com.xquark.dal.page.PageHelper;
import com.xquark.dal.vo.PromotionGoodsVO;
import com.xquark.dal.vo.PromotionPGoodsVO;

/**
 * @author LiHaoYang
 * @date 2018/9/7 0007
 */
public interface PromotionGoodsService {

  /**
   * 查询已开始的拼团商品
   */
  PageHelper<PromotionGoodsVO> selectBeginningPromotionGoods(Integer grandSaleId, String cpid, Integer page, Integer pageSize);

  /**
   * 查询已开始拼团商品(废弃)
   */
  PageHelper<PromotionPGoodsVO> selectProduct(Integer page, Integer pageSize, Long cpId);

  /**
   * 查询未开始的拼团商品
   */
  PageHelper<PromotionGoodsVO> selectNotBeginningPromotionGoods(Integer grandSaleId, String cpid, Integer page, Integer pageSize);

  /**
   * 根据pCode查询出已开始的活动商品列表
   */
  PageHelper<PromotionPGoodsVO> selectPromotionGoodsByPcode(Integer page, Integer pageSize, Long cpId, String pCode);
}
