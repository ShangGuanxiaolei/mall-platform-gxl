package com.xquark.service.pintuan.impl;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.dal.model.mypiece.PieceConfirmOrder;
import com.xquark.dal.type.PromotionType;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.cart.CartService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pintuan.CommitOrderService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.pricing.impl.pricing.PricingMode;
import com.xquark.service.pricing.vo.CartPromotionResultVO;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.shop.ShopService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @deprecated 使用统一订单提交接口
 */
@Service
@Deprecated
public class CommitOrderServiceImpl implements CommitOrderService {

    @Autowired
    private ShopService shopService;

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private AddressService addressService;

    @Autowired
    private CartService cartService;

    @Autowired
    private FlashSalePromotionProductService flashSalePromotionProductService;

    @Value("${tech.serviceFee.standard}")
    private String serviceFeethreshold;

    @Value("${order.delaysign.date}")
    private int defDelayDate;

    @Deprecated
    @Override
    public Map<String, Object> confirmPiece(PieceConfirmOrder form,User user) {
        Map<String, Object> result = new HashMap<>();
        String promotionProductId = form.getPromotionProductId();
        PromotionType promotionType = form.getPromotionFrom();
        List<CartItemVO> cartItems = new ArrayList<>();
        String realShopId = shopService.loadRootShop().getId();
        if (form.getQty() > 0) {
            // 直接下单,不走购物车流程
            Set<String> skuIds = new LinkedHashSet<>(form.getSkuId());
            cartItems = cartService.checkout(skuIds, form.getQty(), realShopId, null);
        } else if (form.getSkuId() != null && form.getSkuId().size() > 0) {
            // 以sku结算
            Set<String> skuIds = new LinkedHashSet<>(form.getSkuId());
            cartItems = cartService.checkout(skuIds, realShopId, null);
        } else if (StringUtils.isNotEmpty(realShopId)) {
            // 以店铺结算
            cartItems = cartService.checkout(realShopId, null);
        } else {
            // do nothing
        }

        if (CollectionUtils.isEmpty(cartItems)) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "请选择要结算的商品");
        }

        // 购物车中所有商品id
        List<String> productIds = new ArrayList<>();

        // 购物车商品按照店铺分组，注意shop做key，需要实现shop的equals方法
        Map<Shop, List<CartItemVO>> cartItemMap = new HashMap<>();
        for (CartItemVO item : cartItems) {
            List<CartItemVO> list = cartItemMap.get(item.getShop());
            if (list == null) {
                list = new ArrayList<>();
                cartItemMap.put(item.getShop(), list);
            }
            productIds.add(item.getProductId());
            list.add(item);
        }
        if (CollectionUtils.isNotEmpty(productIds)) {
            for (String pId : productIds) {
                // 校验抢购活动是否开始
                flashSalePromotionProductService.checkPromotionStart(pId);
            }
        }
        AddressVO address = fillAddressDefault(form.getAddressId());
        result.put("address", address);
        CartPromotionResultVO cartPromotionRet;
        UserSelectedProVO userSelectedPro = new UserSelectedProVO();
        if (form.getShopCoupons() != null) {
            Map<Shop, String> selShopCouponMap = new LinkedHashMap<>();
            Map<String, String> shopCouponParam = form.getShopCoupons();
            for (Map.Entry<String, String> entry : shopCouponParam.entrySet()) {
                String shopId = entry.getKey();
                String shopCouponId = entry.getValue();
                Shop eachShop = shopService.load(shopId);
                selShopCouponMap.put(eachShop, shopCouponId);
            }
            userSelectedPro.setSelShopCouponMap(selShopCouponMap);

            Map<Shop, String> selShopPromotionMap = new LinkedHashMap<>();
            userSelectedPro.setSelShopPromotionMap(selShopPromotionMap);

            Map<CartItemVO, String> selCartItemPromotionMap = new LinkedHashMap<>();
            userSelectedPro.setSelCartItemPromotionMap(selCartItemPromotionMap);

            Map<CartItemVO, String> selCartItemCouponMap = new LinkedHashMap<>();
            userSelectedPro.setSelCartItemCouponMap(selCartItemCouponMap);
        }
        cartPromotionRet = promotionService
                .calculate4Cart(cartItemMap, user, userSelectedPro, address,
                    false, true, null, PricingMode.CONFIRM);
        result.put("prices", cartPromotionRet.getTotalPricingResult());

        result.put("skuIds", form.getSkuId());
        if (form.getShopRemarksCache() != null) {
            result.put("shopRemarksCache", form.getShopRemarksCache());
        }
        result.put("productId", form.getProductId());
        result.put("qty", form.getQty());

        // FIXME:to delete
        result.put("skuId", form.getSkuId() != null ? form.getSkuId().get(0) : "");
        result.put("shopId", form.getShopId());
        result.put("cartItems", cartItems);
        result.put("promotionId", promotionProductId);
        return result;
    }
    private AddressVO fillAddressDefault(String addressId) {
        AddressVO address = null;
        if (StringUtils.isNotBlank(addressId)) {
            address = addressService.loadUserAddress(addressId);
        }
        if (address == null) {
            address = addressService.loadCurrDefault();
        }
        return address;
    }
}
