package com.xquark.service.pintuan;

import com.xquark.dal.vo.PromotionMemberImgVO;
import com.xquark.dal.vo.PromotionTranInfoVO;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionTranInfoService
 * @date 2018/9/13 0013
 */
public interface PromotionTranInfoService {

  /**
   * 查询当前用户为团长的拼团信息
   */
  List<? extends PromotionTranInfoVO> selectMyGroupInfo(Long cpId, Integer firstNo,
      Integer pageSize);

  /**
   * 查询当前用户不为团长的拼团信息
   */
  List<? extends PromotionTranInfoVO> selectMyNoGroupInfo(Long cpId, Integer firstNo,
      Integer pageSize);

  /**
   * 查询当前拼团用户头像
   */
  List<PromotionMemberImgVO> selectPromotioMyTranMemberImg(String tranCode);

  /**
   * 查询当前拼团traninfo信息
   */
  PromotionTranInfoVO selectPromotionPgTranCode(String tranCode);

  int selectRestStockNum(String pCode);

  /**
   * 根据skuCode活动查询库存
   * @param skuCode skuCode
   * @param pCode
   * @return int
   */
  int selectRestStockNumBySkuCode(String skuCode, String pCode);

  /**
   * 修改拼团状态
   */
  int updatePieceStatusByTranCode(String tranCode);
}
