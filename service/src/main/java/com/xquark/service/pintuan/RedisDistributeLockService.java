package com.xquark.service.pintuan;

import java.util.Collections;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

@Component
public class RedisDistributeLockService {


  private static final String LOCK_SUCCESS = "OK";
  private static final String SET_IF_NOT_EXIST = "NX";
  private static final String SET_WITH_EXPIRE_TIME = "PX";

  private static final Long RELEASE_SUCCESS = 1L;

  //
  Jedis jedis = new Jedis("127.0.0.1",6379);

  /**
   * 尝试获取分布式锁
   * 参数固定
   * @return 是否获取成功
   */
  //public boolean tryGetDistributedLock(Jedis jedis, String lockKey, String requestId, int expireTime) {
  public boolean tryGetDistributedLock() {

    String lockKey = "lockKey";
    String requestId = UUID.randomUUID().toString();
    int expireTime = 30;


    String result = jedis.set(lockKey, requestId, SET_IF_NOT_EXIST, SET_WITH_EXPIRE_TIME, expireTime);

    if (LOCK_SUCCESS.equals(result)) {
      return true;
    }
    return false;

  }



  /**
   * 释放分布式锁
   *
   * @return 是否释放成功
   */
  public boolean releaseDistributedLock() {

    String lockKey = "lockKey";
    String requestId = UUID.randomUUID().toString();

    String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
    Object result = jedis.eval(script, Collections.singletonList(lockKey), Collections.singletonList(requestId));

    if (RELEASE_SUCCESS.equals(result)) {
      return true;
    }
    return false;

  }


}
