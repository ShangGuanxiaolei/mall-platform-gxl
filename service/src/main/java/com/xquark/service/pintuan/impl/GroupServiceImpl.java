package com.xquark.service.pintuan.impl;

import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.StatusAndTimesVO;
import com.xquark.dal.model.User;
import com.xquark.dal.model.mypiece.ConfirmAndCheckVo;
import com.xquark.dal.model.mypiece.PromotionTempStockVo;
import com.xquark.dal.model.mypiece.StockCheckBean;
import com.xquark.dal.status.PieceStatus;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.mypiece.StockCheckService;
import com.xquark.service.pintuan.CheckStatusAndTimesService;
import com.xquark.service.pintuan.CommitOrderService;
import com.xquark.service.pintuan.GroupService;
import com.xquark.service.pricing.base.PieceStateException;
import com.xquark.service.pricing.impl.pricing.impl.PgPromotionServiceAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class GroupServiceImpl implements GroupService {
  private Logger logger = LoggerFactory.getLogger(GroupServiceImpl.class);
  @Autowired
  private CheckStatusAndTimesService checkStatusAndTimesService;
  @Autowired
  private StockCheckService stockCheckService;
  @Autowired
  private CommitOrderService commitOrderService;
  @Autowired
  private PgPromotionServiceAdapter pgPromotionServiceAdapter;

  @Deprecated
  @Override
  public Map<String, Object> createTeam(ConfirmAndCheckVo confirmAndCheckVo, User user) {
    StockCheckBean stockCheckBean = confirmAndCheckVo.getStockCheckBean();
    Map<String, Object> result = new HashMap<>();
    //查询活动状态和活动起止时间
    PromotionBaseInfo promotionBaseInfo = new PromotionBaseInfo();
    promotionBaseInfo.setpCode(stockCheckBean.getP_code());
    promotionBaseInfo.setIsDeleted(1);
    logger.info("检查拼团活动是否存在");
    StatusAndTimesVO info = checkStatusAndTimesService.check(promotionBaseInfo);
    if (null == info) {
      result.put("is_success", "0");
      result.put("msg", "活动不存在");
      return result;
    }
    String pStatus = "5";
    //判断活动状态是否满足条件 5代表活动已生效
    if (pStatus.equals(info.getpStatus())) {
      //判断时间是否满足条件
      Timestamp timestamp = new Timestamp((new Date()).getTime());
      if (timestamp.before(info.getEffectTo()) && timestamp.after(info.getEffectFrom())) {
        //活动商品库存检查,返回实体类,自行判断
        List<PromotionTempStockVo> promotionTempStockVoList = stockCheckService.checkStock(stockCheckBean);
        for (PromotionTempStockVo promotionTempStockVo :
                promotionTempStockVoList) {
          if (null == promotionTempStockVo) {
            result.put("is_success", "0");
            result.put("msg", "未检查出该商品");
            return result;
          }
          if (promotionTempStockVo.getP_usable_sku_num() >= stockCheckBean.getP_sku_num()) {
            result = commitOrderService.confirmPiece(confirmAndCheckVo.getPieceConfirmOrder(), user);
          } else {
            result.put("is_success", "0");
            result.put("errorMsg", "活动库存不足");
          }
        }
        return result;
      } else {
        result.put("is_success", "0");
        result.put("errorMsg", "活动已失效");
        return result;
      }
    } else {
      result.put("is_success", "0");
      result.put("errorMsg", "活动已失效");
      return result;
    }
  }

  /**
   * 提交订单前的检查
   *
   * @param stockCheckBean stockCheckBean
   * @return Map
   */
  @Override
  public Map<String, String> orderingCheck(StockCheckBean stockCheckBean) throws PieceStateException {
    String pCode = stockCheckBean.getP_code();
    String skuCode = stockCheckBean.getSku_code();
    String tranCode = stockCheckBean.getTranCode();

    PromotionBaseInfo promotionBaseInfo = new PromotionBaseInfo();
    promotionBaseInfo.setpCode(pCode);
    promotionBaseInfo.setIsDeleted(1);
    promotionBaseInfo.setTranCode(tranCode);

    logger.info("检查拼团活动是否存在开始...........................");
    Map<String, String> result = new HashMap<>();
    //根据pCode和tranCode获取当前团的成团时间及开团时间
    StatusAndTimesVO info = checkStatusAndTimesService.check(promotionBaseInfo);
    StatusAndTimesVO info1 = checkStatusAndTimesService.checkForTranInfo(promotionBaseInfo);
    if (tranCode != null) {//tranCode存在则表示参团，否则表示开团
      //判断时间是否满足条件
      Date currTime = new Date();
      if (null != info1) {
        info1.getTranEndTime().ifPresent(endTime -> {
          if (currTime.after(endTime)) {
            logger.info("活动已失效,[pCode=" + pCode + "]");
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动已失效,[pCode=" + pCode + "]");
          }
        });
      }
    } else {
      if (null == info) {
        result.put("is_success", "0");
        result.put("errorMsg", "活动不存在");
        logger.info("活动已结束,[pCode=" + pCode + "]");
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动已结束");
      }
      //判断时间是否满足条件
      Timestamp currentTime = new Timestamp((new Date()).getTime());
      //判断活动是否失效
      if (currentTime.after(info.getEffectTo())) {
        result.put("is_success", "0");
        result.put("errorMsg", "活动已失效");
        logger.info("活动已结束,[pCode=" + pCode + "]");
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动已结束");
      }
    }
    logger.info("检查拼团活动是否存在结束=========================");

    List<PromotionTempStockVo> promotionTempStockVoList = stockCheckService.checkStockForPiece(stockCheckBean);
    for (PromotionTempStockVo promotionTempStockVo :
            promotionTempStockVoList) {
      if (null == promotionTempStockVo) {
        result.put("is_success", "0");
        result.put("errorMsg", "未检查出该商品");
        logger.info("未检查出该商品,[sku_code=" + skuCode + "]");
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "未检查出该商品");
      }
      //库存不足检查
      if (promotionTempStockVo.getP_usable_sku_num() < stockCheckBean.getP_sku_num()) {
        logger.info("库存不足，执行结束拼团处理.................");
        pgPromotionServiceAdapter.batchUpdateTranAndMember(
                tranCode, PieceStatus.STOCK_CANCELED, PieceStatus.OPENED);
        logger.info("库存不足，执行结束拼团处理完成==============");
        logger.info("此规格活动库存不足,[sku_code=" + skuCode + "]");
        throw new PieceStateException("此规格活动库存不足");
      }
    }
    result.put("is_success", "1");
    return result;
  }
}
