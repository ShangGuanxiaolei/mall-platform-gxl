package com.xquark.service.pintuan;


import com.xquark.dal.model.mypiece.PGTranInfoVo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface RecordPromotionPgTranInfoService {

  boolean save(PGTranInfoVo pgTranInfoVo);
//  void save(PromotionPgTranInfo promotionPgTranInfo);

  /**
   * 根据tranCode查询组团信息
   * @param pieceGroupTranCode
   * @return
   */
  Map<String, Object> selectAllByTranCode(String pieceGroupTranCode);

  /**
   * 根据tranCode和memberId查询拼团成员信息
   * @param pieceGroupTranCode
   * @param memberId
   * @return
   */
  Map<String, Object> selectMemberInfoByTranCodeAndMemberId(String pieceGroupTranCode, String memberId);

  BigDecimal loadPgDiscount(String pCode, String detailCode);

  PGTranInfoVo loadPGTranInfo(String tranCode);

  /**
   * 更新团主的拼团状态
   */
  boolean updateStatusByTranCode(String tranCode, int status, List<Integer> originCodeList);

  List<Map> selectAllByStatusPush();

}
