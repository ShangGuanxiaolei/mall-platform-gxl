package com.xquark.service.pintuan.impl;

import com.xquark.dal.mapper.PromotionPgDetailMapper;
import com.xquark.dal.model.PromotionPgDetail;
import com.xquark.service.pintuan.RecordPromotionPgDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordPromotionPgDetailImpl implements RecordPromotionPgDetail {

    @Autowired
    private PromotionPgDetailMapper promotionPgDetailMapper;

    @Override
    public PromotionPgDetail save(PromotionPgDetail promotionPgDetail) {
        return promotionPgDetail;
    }

    /**
     * 查询拼团配置
     */
    @Override
    public PromotionPgDetail selectByCode(String pDetailCode, String pCode) {
        return promotionPgDetailMapper.seleceAllByCode(pDetailCode,pCode);
    }

    @Override
    public List<PromotionPgDetail> selectAllByPCode(String pCode) {
        return promotionPgDetailMapper.selectAllByPCode(pCode);
    }

}
