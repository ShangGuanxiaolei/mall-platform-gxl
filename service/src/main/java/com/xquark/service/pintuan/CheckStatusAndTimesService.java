package com.xquark.service.pintuan;


import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.StatusAndTimesVO;

/**
 *
 */
public interface CheckStatusAndTimesService {
  StatusAndTimesVO check(PromotionBaseInfo promotionBaseInfo);

  StatusAndTimesVO checkForTranInfo(PromotionBaseInfo promotionBaseInfo);
}
