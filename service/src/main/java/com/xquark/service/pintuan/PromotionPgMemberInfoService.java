package com.xquark.service.pintuan;

import com.xquark.dal.vo.GroupShareVO;
import com.xquark.dal.vo.PromotionOrderGoodsInfoVO;
import com.xquark.dal.vo.PromotionPgMemberInfoVO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionPgMemberInfoService
 * @date 2018/9/10 0010
 */
public interface PromotionPgMemberInfoService {

  /**
   * 查询用户拼团的商品详情
   */
  PromotionOrderGoodsInfoVO selectGoodsInfo(String bizNo);

  /**
   * 查询已拼团人数
   */
  int selectSuccessPromotionMemberCount(String bizNo);

  int selectSuccessPromotionMemberCountAndSelf(String tranCode, Long cpId);

  int selectSuccessPromotionMemberCountAndOrder(String tranCode, String orderNo);

  /**
   * 查询拼团总人数
   */
  int selectPgMemberCount(String bizNo);

  /**
   * 查询拼团成员信息
   */
  List<PromotionPgMemberInfoVO> selectMemberInfo(String tranCode);

  /**
   * 查询成团有效时间
   */
  int selectRegimentTime(String bizNo);
    /**
     * 查询当前拼团用户信息
     * @param tranCode
     * @param cpid
     * @return
     */
    List<PromotionPgMemberInfoVO> selectMemberInfoBycpId(String tranCode, Long cpid);

  /**
   * 查询开团时间
   */
  Date selectGroupOpenTime(String bizNo);

  PromotionOrderGoodsInfoVO selectGoodsInfo(String bizNo, Long cpId);

  PromotionOrderGoodsInfoVO selectGoodsInfoWithOrder(String bizNo, Long cpId, String orderNo);

  GroupShareVO loadShareVO(String tranCode);

  String loadMainOrderNoByBizNo(String bizNo);

  /**
   * 根据bizNo获取orderNo
   */
  PromotionOrderGoodsInfoVO loadByBizNo(String bizNo);

  PromotionOrderGoodsInfoVO loadByOrderNO(String orderNo);

  String selectStatus(String pieceGroupTranCode);

  List<Integer> selectMemberStatus(String pieceGroupTranCode);

}
