package com.xquark.service.pintuan.impl;

import com.xquark.dal.mapper.PromotionGoodsDetailMapper;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.User;
import com.xquark.dal.vo.PromotionGoodsDetailVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.pintuan.PromotionGoodsDetailService;
import com.xquark.utils.DynamicPricingUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionGoodsDeatilServiceImpl
 * @date 2018/9/7 0007
 */
@Service("promotionGoodsDetailService")
class PromotionGoodsDetailServiceImpl extends BaseServiceImpl implements PromotionGoodsDetailService {

  @Autowired
  private PromotionGoodsDetailMapper promotionGoodsDetailMapper;

  /**
   * 查询拼团活动的人数和对应优惠价格
   */
  @Override
  public List<PromotionGoodsDetailVO> selectPeopleAndDiscountPrice(String pCode,
      String tranCode) {
    List<PromotionGoodsDetailVO> result = promotionGoodsDetailMapper.selectPeopleAndDiscountPrice(pCode, tranCode);
    final User currUser = getCurrentUser();
    for (int i = 0; i < result.size(); i++) {
      final PromotionGoodsDetailVO detail = result.get(i);
      detail.setNum(i);
      //存入discountPrice
      detail.setDisCountPrice(result.get(i).getPromotionPrice());
      DynamicPricingUtil.rePricing(detail.getPgPrice(), currUser);
    }
    return result;
  }

  /**
   * 查询剩余时间和最高拼团价格
   */
  @Override
  public PromotionGoodsDetailVO selectPromotionTimeAndMaxDiscount(String skuId, String pCode, Long cpid) {
    PromotionGoodsDetailVO promotionGoodsDetailVO = promotionGoodsDetailMapper
        .selectPromotionTimeAndMaxDiscount(skuId, pCode);
    if (promotionGoodsDetailVO == null) {
      return null;
    }
    promotionGoodsDetailVO.setDisCountPrice(promotionGoodsDetailVO.getPromotionPrice());
    return promotionGoodsDetailVO;
  }

  @Override
  public List<Sku> selectSku(String id) {
    return promotionGoodsDetailMapper.selectSku(id);
  }

  @Override
  public Integer selectPromotionPriceLimit(String pDetailCode) {
    return promotionGoodsDetailMapper.selectPromotionPriceLimit(pDetailCode);
  }

}
