package com.xquark.service.pintuan;

import com.xquark.dal.model.User;
import com.xquark.dal.model.mypiece.PieceConfirmOrder;
import com.xquark.service.cart.CartNextForm;

import java.util.Map;

public interface CommitOrderService {
   Map<String, Object> confirmPiece(PieceConfirmOrder pieceConfirmOrder,User user);
}
