package com.xquark.service.pintuan.impl;

import com.xquark.dal.mapper.PromotionPgMapper;
import com.xquark.dal.model.PromotionPg;
import com.xquark.service.pintuan.PromotionPgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PromotionPgServiceImpl implements PromotionPgService {

    @Autowired
    private PromotionPgMapper promotionPgMapper;

    @Override
    public PromotionPg selectPromotionPgBypCode(String pCode) {
        return promotionPgMapper.selectPromotionPgBypCode(pCode);
    }

    @Override
    public String selectCanJoinGroup(String pCode){
        return promotionPgMapper.selectCanJoinGroup(pCode);
    }

    @Override
    public boolean PromotionNewPg(String pCode) {
        return promotionPgMapper.newPg(pCode) > 0;
    }

}
