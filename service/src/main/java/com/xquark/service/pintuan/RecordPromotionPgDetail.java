package com.xquark.service.pintuan;

import com.xquark.dal.model.PromotionPgDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RecordPromotionPgDetail {


    /**
     * 保存拼团活动
     */
    PromotionPgDetail save(PromotionPgDetail promotionPgDetail);

    /**
     * 查询拼团配置
     */
    PromotionPgDetail selectByCode(String pDetailCode, String pCode);

    List<PromotionPgDetail> selectAllByPCode(@Param("pCode") String pCode);

}
