package com.xquark.service.pintuan.impl;

import com.xquark.dal.mapper.PromotionGroupDetailMapper;
import com.xquark.dal.mapper.PromotionOrderDetailMapper;
import com.xquark.dal.vo.PromotionGroupDetailVO;
import com.xquark.service.pintuan.PromotionGroupDetailService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@Service("promotionGroupDetailService")
public class PromotionGroupDetailServiceImpl implements PromotionGroupDetailService {

  @Autowired
  private PromotionGroupDetailMapper promotionGroupDetailMapper;

  @Autowired
  private PromotionOrderDetailMapper promotionOrderDetailMapper;

  @Override
  public List<PromotionGroupDetailVO> selectGroupDetail(String pieceGroupTranCode) {
    return promotionGroupDetailMapper.selectGroupDetail(pieceGroupTranCode);
  }

  @Override
  public List<PromotionGroupDetailVO> selectGroupDetailByOrderNo(String orderNo) {
    String tranCode = promotionOrderDetailMapper.selectTranCodeByOrderNo(orderNo);
    if (StringUtils.isBlank(tranCode)) {
      return Collections.emptyList();
    }
    return promotionGroupDetailMapper.selectGroupDetail(tranCode);
  }

  @Override
  public Integer findPieceGroupNum(String pCode) {
    return promotionGroupDetailMapper.findPieceGroupNum(pCode);
  }

  @Override
  public Integer findPieceSkuLimit(String pCode) {
    return promotionGroupDetailMapper.findPieceSkuLimit(pCode);
  }

  @Override
  public BigDecimal showOpenMemberPrice(String pDetailCode) {
    return promotionGroupDetailMapper.selectOpenMemberPrice(pDetailCode);
  }

  @Override
  public boolean findFreshPromotionByPCode(String pDetailCode) {
    return promotionGroupDetailMapper.selectFreshPromotionByPCode(pDetailCode);
  }
}
