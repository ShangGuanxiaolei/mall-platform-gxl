package com.xquark.service.pintuan.impl;


import com.xquark.dal.mapper.CheckStatusAndTimesMapper;
import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.StatusAndTimesVO;
import com.xquark.service.pintuan.CheckStatusAndTimesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("checkStatusAndTimesService")
public class CheckStatusAndTimesServiceImpl implements CheckStatusAndTimesService {


  @Autowired
  private CheckStatusAndTimesMapper checkStatusAndTimesMapper;


  @Override
  public StatusAndTimesVO check(PromotionBaseInfo promotionBaseInfo) {
    return checkStatusAndTimesMapper.selectBypCode(promotionBaseInfo);
  }

  @Override
  public StatusAndTimesVO checkForTranInfo(PromotionBaseInfo promotionBaseInfo){
    return checkStatusAndTimesMapper.selectByTranCode(promotionBaseInfo);
  }

}
