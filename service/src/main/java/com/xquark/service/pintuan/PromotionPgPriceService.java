package com.xquark.service.pintuan;

import com.xquark.dal.model.PromotionPgPrice;
import com.xquark.dal.model.Sku;
import com.xquark.dal.type.PromotionType;

import java.util.List;

/**
 * @Author: gxl
 */
public interface PromotionPgPriceService {

    int deleteByPrimaryKey(Integer id);

    int insert(PromotionPgPrice record);

    PromotionPgPrice selectByPrimaryKey(Integer id);

    PromotionPgPrice loadByPromotionIdAndSkuId(String promotionId, Sku sku, PromotionType type);

    int updateByPrimaryKeySelective(PromotionPgPrice record);

    int updateByPrimaryKey(PromotionPgPrice record);

    PromotionPgPrice selectPromotionPgPriceByDetailCode(String pDetailCode);

    List<PromotionPgPrice> listByPromotionId(String promotionId, String productId, PromotionType type);

}
