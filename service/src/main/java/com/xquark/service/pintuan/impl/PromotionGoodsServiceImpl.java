package com.xquark.service.pintuan.impl;

import com.xquark.dal.mapper.PromotionBaseInfoMapper;
import com.xquark.dal.mapper.PromotionConfigMapper;
import com.xquark.dal.mapper.PromotionGoodsMapper;
import com.xquark.dal.mapper.PromotionWhitelistMapper;
import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.PromotionConfig;
import com.xquark.dal.page.PageHelper;
import com.xquark.dal.type.CareerLevelType;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionGoodsVO;
import com.xquark.dal.vo.PromotionPGoodsVO;
import com.xquark.service.pintuan.PromotionGoodsService;
import com.xquark.service.pintuan.PromotionPgService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pricing.PromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author LiHaoYang
 * @date 2018/9/6 0006
 */
@Service("promotionGoodsService")
public class PromotionGoodsServiceImpl implements PromotionGoodsService {
  @Autowired private PromotionGoodsMapper promotionGoodsMapper;
  @Autowired private PromotionWhitelistMapper promotionWhitelistMapper;
  @Autowired private PromotionBaseInfoMapper baseInfoMapper;
  @Autowired private PromotionConfigMapper promotionConfigMapper;
  @Autowired private CustomerProfileService customerProfileService;
  @Autowired private PromotionService promotionService;
  @Autowired private PromotionPgService promotionPgService;

  private static final Long MAGIC_NUMBER = 9999999L;

  /**
   * 分页查询已开始活动
   *
   * @param page page
   * @param pageSize pageSize
   * @return 已开始活动
   */
  @Override
  public PageHelper<PromotionGoodsVO> selectBeginningPromotionGoods(
          Integer grandSaleId, String cpId, Integer page, Integer pageSize) {
    PageHelper<PromotionGoodsVO> helper = new PageHelper<>();
    page = Optional.ofNullable(page).orElse(1);
    helper.setPage(page);
    helper.setPageSize(pageSize);

    //白名单控制，逻辑是反的?
    if(this.promotionService.checkIsWhite(MAGIC_NUMBER,"piece")){
      if(!this.promotionService.checkIsWhite(Long.parseLong(cpId),"piece")){
        return helper;
      }
    }

    PromotionGoodsVO userLevel = promotionGoodsMapper.selectUserQua(cpId);
    String hds = null;
    String vv = null;
    // 非白人
    if (userLevel != null) {
      hds = userLevel.getHdsType();
      vv = userLevel.getVivilifeType();
    }
    //根据身份查询活动数量
    int sum = promotionGoodsMapper.selectBeginningGoodsCount(grandSaleId, hds, vv, PromotionType.PIECE);
    int totalPage = sum % pageSize == 0 ? sum / pageSize : sum / pageSize + 1;
    helper.setPageCount(sum);
    helper.setTotalPage(totalPage);

    CareerLevelType careerLevelType = CareerLevelType.getFinalLevelType(hds, vv);
    boolean hasWeek = customerProfileService.getUplineWeekIgnoreIdentity(Long.valueOf(cpId)) != null;
    if (careerLevelType == CareerLevelType.RC && !hasWeek) {
      careerLevelType = CareerLevelType.PW; //纯白人身份置为PW
    }
    List<PromotionGoodsVO> result = Objects.equals(page, 1)
            ? promotionGoodsMapper.selectBeginningPromotionGoods(grandSaleId,
            careerLevelType.toString(), (page - 1) * pageSize, pageSize, PromotionType.PIECE)
            : Collections.emptyList();
    //设置前端显示的几人起拼及最低拼团价
    setAResult(result);
    for (PromotionGoodsVO p:
         result) {
      p.setIsNewPg(promotionPgService.PromotionNewPg(p.getpCode()));
    }
    helper.setResult(result);
    return helper;
  }

  /**
   * 分页查询已开始活动
   *
   * @return PromotionPGoodsVO
   */
  @Override
  public PageHelper<PromotionPGoodsVO> selectProduct(Integer page, Integer pageSize, Long cpId) {
    // 查询活动商品列表
    PageHelper<PromotionPGoodsVO> helper = new PageHelper<>();
    Map<String, String> commentMap = new HashMap<>();
    helper.setPage(page);
    helper.setPageSize(pageSize);
    // 1.检查是否存在指定魔法值：9999999，存在则不检查白名单：（true && true）
    // 2.检查是否在白名单中，不在则返回空
    if (promotionWhitelistMapper.checkIfInWhiteList(MAGIC_NUMBER) < 1) {
      if (promotionWhitelistMapper.checkIfInWhiteList(cpId) < 1) {
        return helper;
      }
    }

    List<PromotionPGoodsVO> promotionGoodsVOS = promotionGoodsMapper.selectProduct(page, pageSize);
    // 杭州大会补充，增加标题查询
    PromotionBaseInfo baseInfo = baseInfoMapper.selectByPtype("meeting");
    if (null != baseInfo) {
      // 活动首页banner标题
      commentMap.put("meetingBannerTitle", baseInfo.getpName());
      commentMap.put("meetingListTitle", baseInfo.getpName());
    }
    // 增加是否显示倒计时字段
    Map<String, String> map = new HashMap<>();
    map.put("configType", "meeting");
    map.put("configName", "SHOW_COUNT_DOWN");
    PromotionConfig config = promotionConfigMapper.selectByConfig(map);
    boolean showCountDown = false;
    if (null != config) {
      if ("1".equals(config.getConfigValue())) {
        // 1表示显示倒计时
        showCountDown = true;
      }
    }
    if (promotionGoodsVOS.size() > 0) {
      int sum = promotionGoodsVOS.size();
      int totalPage = sum % pageSize == 0 ? sum / pageSize : sum / pageSize + 1;
      helper.setPageCount(sum);
      helper.setTotalPage(totalPage);

      // 判断是否有赠品,并批量插入
      for (PromotionPGoodsVO promotionGoodsVO : promotionGoodsVOS) {
        String pCode = promotionGoodsVO.getpCode();
        String productId = String.valueOf(promotionGoodsVO.getProductId());
        Integer integer = promotionGoodsMapper.qualityGoods(pCode, productId);
        if (integer > 0) {
          promotionGoodsVO.setQualityGoods(true);
        } else {
          promotionGoodsVO.setQualityGoods(false);
        }
        // 设置是否显示倒计时
        promotionGoodsVO.setShowCountDown(showCountDown);
      }

      helper.setCommentMap(commentMap);
      helper.setResult(promotionGoodsVOS);
      return helper;

    } else {
      helper.setPageCount(0);
      helper.setTotalPage(0);

      return helper;
    }
  }

  /**
   * lxl 查询活动商品列表，包括橱窗和非橱窗
   *
   * @param page page
   * @param pageSize pageSize
   * @param cpId cpId
   * @param pCode pCode
   * @return PromotionPGoodsVO
   */
  @Override
  public PageHelper<PromotionPGoodsVO> selectPromotionGoodsByPcode(
      Integer page, Integer pageSize, Long cpId, String pCode) {
    // 查询活动商品列表
    PageHelper<PromotionPGoodsVO> helper = new PageHelper<>();
    helper.setPage(page);
    helper.setPageSize(pageSize);
    // 1.检查是否存在指定魔法值：9999999，存在则不检查白名单：（true && true）
    // 2.检查是否在白名单中，不在则返回空
    if (promotionWhitelistMapper.checkIfInWhiteList(MAGIC_NUMBER) < 1) {
      if (promotionWhitelistMapper.checkIfInWhiteList(cpId) < 1) {
        return helper;
      }
    }

    int offSet = (page - 1) * pageSize;
    List<PromotionPGoodsVO> promotionGoodsVOS =
        promotionGoodsMapper.selectProProductListByPcode(offSet, pageSize, pCode);

    int count = promotionGoodsMapper.selectProProductCount(pCode);
    PromotionBaseInfo baseInfo = baseInfoMapper.selectByPCode(pCode);

    // 增加是否显示倒计时字段
    Map<String, String> map = new HashMap<>();
    map.put("configType", baseInfo.getpType());
    map.put("configName", "SHOW_COUNT_DOWN");
    PromotionConfig config = promotionConfigMapper.selectByConfig(map);
    boolean showCountDown = false;
    if (null != config) {
      if ("1".equals(config.getConfigValue())) {
        // 1表示显示倒计时
        showCountDown = true;
      }
    }

    if (count > 0) {
      int totalPage = count % pageSize == 0 ? count / pageSize : count / pageSize + 1;
      helper.setPageCount(count);
      helper.setTotalPage(totalPage);

      if (null != promotionGoodsVOS && promotionGoodsVOS.size() > 0) {

        for (PromotionPGoodsVO promotionPGoodsVO : promotionGoodsVOS) {
          // 设置有无赠品
          String productId = String.valueOf(promotionPGoodsVO.getProductId());
          Integer integer = promotionGoodsMapper.qualityGoods(pCode, productId);
          if (integer > 0) {
            promotionPGoodsVO.setQualityGoods(true);
          } else {
            promotionPGoodsVO.setQualityGoods(false);
          }
          // 设置是否显示倒计时
          promotionPGoodsVO.setShowCountDown(showCountDown);

          if (promotionPGoodsVO.getGift() == 1) {
            promotionPGoodsVO.setGiftType("买一赠一");
          } else if (promotionPGoodsVO.getGift() == 2) {
            promotionPGoodsVO.setGiftType("买二赠一");
          } else if (promotionPGoodsVO.getGift() == 3) {
            promotionPGoodsVO.setGiftType("买三赠一");
          }else {
            promotionPGoodsVO.setGiftType("新春特惠");
          }
        }

        helper.setResult(promotionGoodsVOS);
      }
    } else {
      helper.setPageCount(0);
      helper.setTotalPage(0);
      return helper;
    }
    return helper;
  }

  /**
   * 分页查询未开始活动
   *
   * @param grandSaleId 大型促销活动id
   * @param page page
   * @param pageSize pageSize
   * @return 未开始活动
   */
  @Override
  public PageHelper<PromotionGoodsVO> selectNotBeginningPromotionGoods(
          Integer grandSaleId, String cpId, Integer page, Integer pageSize) {
    PageHelper<PromotionGoodsVO> helper = new PageHelper<>();
    page = Optional.ofNullable(page).orElse(1);
    helper.setPage(page);
    helper.setPageSize(pageSize);
    // 检查是否在白名单中，不在则返回空
    if (promotionWhitelistMapper.checkIfInWhiteList(MAGIC_NUMBER) < 1) {
      if (promotionWhitelistMapper.checkIfInWhiteList(Long.valueOf(cpId)) < 1) {
        return helper;
      }
    }

    PromotionGoodsVO userLevel = promotionGoodsMapper.selectUserQua(cpId);

    String hds = null;
    String vv = null;

    // 非白人
    if (userLevel != null) {
      hds = userLevel.getHdsType();
      vv = userLevel.getVivilifeType();
    }

    int sum = promotionGoodsMapper.selectNotBeginGoodsCount(grandSaleId, hds, vv, PromotionType.PIECE);
    int totalPage = sum % pageSize == 0 ? sum / pageSize : sum / pageSize + 1;
    helper.setPageCount(sum);
    helper.setTotalPage(totalPage);
    CareerLevelType careerLevelType = CareerLevelType.getFinalLevelType(hds, vv);
    boolean hasWeek = customerProfileService.getUplineWeekIgnoreIdentity(Long.valueOf(cpId)) != null;
    // 白人且没有弱关系, 即纯白人
    if (careerLevelType == CareerLevelType.RC && !hasWeek) {
      careerLevelType = CareerLevelType.PW; //纯白人身份置为PW
    }
    List<PromotionGoodsVO> result = Objects.equals(page, 1)
            ? promotionGoodsMapper.selectNotBeginningPromotionGoods(grandSaleId,
            careerLevelType.toString(), (page - 1) * pageSize, pageSize, PromotionType.PIECE)
            : Collections.emptyList();
    setAResult(result);
    for (PromotionGoodsVO p:
            result) {
      p.setIsNewPg(promotionPgService.PromotionNewPg(p.getpCode()));
    }
    helper.setResult(result);
    return helper;
  }

  private void setAResult(List<PromotionGoodsVO> result) {
    if (result != null) {
      for (PromotionGoodsVO aResult : result) {
        String pCode = aResult.getpCode();
        BigDecimal minPromotionPrice = promotionGoodsMapper.selectMinDiscount(pCode);
        int minPeople = promotionGoodsMapper.selectMinPeople(pCode);
        aResult.setPieceGroupNum(minPeople);
        aResult.setDisCountPrice(minPromotionPrice);
      }
    }
  }

}
