package com.xquark.service.pintuan.impl;

import com.xquark.dal.mapper.MainOrderMapper;
import com.xquark.dal.mapper.PromotionOrderDetailMapper;
import com.xquark.dal.model.User;
import com.xquark.dal.model.mypiece.PGMemberInfoVo;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.mypiece.QueryOpenIdByCpidService;
import com.xquark.service.pintuan.RecordPromotionPgMemberInfoService;
import com.xquark.service.pintuan.RecordPromotionPgTranInfoService;
import com.xquark.service.pintuan.TransPGService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.utils.UUIDGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author qiuchuyi
 * @date 2018/9/12 10:51
 */
@Service
public class TransPGServiceImpl extends BaseServiceImpl implements TransPGService {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private RecordPromotionPgMemberInfoService recordPromotionPgMemberInfoService;

  @Autowired
  private RecordPromotionPgTranInfoService recordPromotionPgTranInfoService;

  @Autowired
  private QueryOpenIdByCpidService queryOpenIdByCpidService;

  @Autowired
  private CustomerProfileService customerProfileService;

  @Autowired
  private PromotionOrderDetailMapper promotionOrderDetailMapper;

  @Autowired
  private MainOrderMapper mainOrderMapper;

  @Override
  @Transactional(rollbackFor = Exception.class)
  public void transPGService(PGMemberInfoVo pgMemberInfoVo, int isGroupMaster, PGTranInfoVo pgTranInfoVo, String orderNo, String subOrderNo, String orderItemNo, User user) {
    try {
      //如果是团长则保存开团信息
      if (1 == isGroupMaster) {
        //  得到团成员id,白人就是UUID，否则通过xquark_user查询cpid
        String pieceGroupTranCode = UUIDGenerator.getUUID();
        pgTranInfoVo.setPieceGroupTranCode(pieceGroupTranCode);
        //  开团会员Id
        pgTranInfoVo.setGroupOpenMemberId(pgTranInfoVo.getGroupOpenMemberId());
        // TODO 前端确认pDetailCode接口
        pgTranInfoVo.setpDetailCode(pgTranInfoVo.getpDetailCode());
        //拼团第一期先不做分享短链
        pgTranInfoVo.setShareLink("拼团第一期先不做分享短链");
        //交易状态
        pgTranInfoVo.setPieceStatus("0");

        recordPromotionPgTranInfoService.save(pgTranInfoVo);
      }

      //  保存开团信息
      //  是否团团长，要是白人，开团就是团长，否则团长是他上级
      if (isGroupMaster != 1) {
        pgMemberInfoVo.setIsGroupHead(0);
      } else {
        pgMemberInfoVo.setIsGroupHead(isGroupMaster);
      }
      //生成piece_group_tran_code德UUID
      pgMemberInfoVo.setPieceGroupTranCode(pgTranInfoVo.getPieceGroupTranCode());
      //  分享短链接
      pgMemberInfoVo.setShareUrl("拼团第一期先不做分享短链");
      //  微信OpenId,删除字段
//            String wxOpenId = queryOpenIdByCpidService.queryOpenIdByCpid(pgMemberInfoVo.getMemberId());
//            pgMemberInfoVo.setWxOpenId(wxOpenId);
      //删除标记位，数据库默认为1
//            pgMemberInfoVo.setIsDeleted(0);
      String pieceGroupDetailCode = UUIDGenerator.getUUID();
      pgMemberInfoVo.setPieceGroupDetailCode(pieceGroupDetailCode);
      //是否是纯新人
      if (customerProfileService.isNew(pgMemberInfoVo.getMemberId())) {
        pgMemberInfoVo.setIsNew(1);
      } else {
        pgMemberInfoVo.setIsNew(0);
      }
      recordPromotionPgMemberInfoService.save(pgMemberInfoVo);
      //


      PromotionOrderDetail promotionOrderDetail = new PromotionOrderDetail();
      promotionOrderDetail.setOrder_head_code(orderNo);
      promotionOrderDetail.setOrder_detail_code(String.valueOf(IdTypeHandler.decode(orderItemNo)));
      //活动编码
      promotionOrderDetail.setP_code(pgTranInfoVo.getpCode());
      //活动详情编码
      promotionOrderDetail.setP_detail_code(pgTranInfoVo.getpDetailCode());
      promotionOrderDetail.setPiece_group_tran_code(pgTranInfoVo.getPieceGroupTranCode());
      promotionOrderDetail.setPiece_group_detail_code(pieceGroupDetailCode);
      promotionOrderDetail.setMember_id(user.getCpId());
      promotionOrderDetail.setSub_order_no(subOrderNo);
      //活动类型
      promotionOrderDetail.setP_type("piece");
      promotionOrderDetailMapper.insert(promotionOrderDetail);
    } catch (RuntimeException e) {
      log.error("插入拼团异常, piece_group_tran_code=[" + pgTranInfoVo.getPieceGroupTranCode()
              + "] ", e);
    }
  }


  @Override
  public User getCurrentUser() throws BizException {
    return null;
  }
}
