package com.xquark.service.pintuan.vo;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionProductVO;
import com.xquark.service.product.vo.ProductVO;

import java.math.BigDecimal;
import java.util.Map;

public class PgSkuDetailVo extends ProductVO {

  private static final long serialVersionUID = 1L;

  private PromotionType canUsePromotionType;

  private PromotionProductVO canUsePromotion;

  private String productUrl;

  private String activityName;

  private String b2bProductUrl;

  private boolean inBlackList;

  private BigDecimal promotionPrice;

  private JSONObject crmProductInfo;

  private Map<String, Long> commentTotalMap;

  private boolean firstOrder;//新人浮标

  public boolean isFirstOrder() {
    return firstOrder;
  }

  public void setFirstOrder(boolean firstOrder) {
    this.firstOrder = firstOrder;
  }

  public boolean isInBlackList() {
    return inBlackList;
  }

  public void setInBlackList(boolean inBlackList) {
    this.inBlackList = inBlackList;
  }

  public String getB2bProductUrl() {
    return b2bProductUrl;
  }

  public void setB2bProductUrl(String b2bProductUrl) {
    this.b2bProductUrl = b2bProductUrl;
  }

  public PgSkuDetailVo(ProductVO product, String productUrl, String imgUrl, String activityName) {
    super(product);
//    this.setProductUrl(productUrl);
    this.setImgUrl(imgUrl);
    this.setActivityName(activityName);
  }

  public void setCanUsePromotion(PromotionProductVO canUsePromotion) {
    this.canUsePromotion = canUsePromotion;
  }

  public String getProductUrl() {
    return productUrl;
  }

  public void setProductUrl(String productUrl) {
    this.productUrl = productUrl;
  }

  public String getActivityName() {
    return activityName;
  }

  public void setActivityName(String activityName) {
    this.activityName = activityName;
  }

  public JSONObject getCrmProductInfo() {
    return crmProductInfo;
  }

  public void setCrmProductInfo(JSONObject crmProductInfo) {
    this.crmProductInfo = crmProductInfo;
  }

  public Map<String, Long> getCommentTotalMap() {
    return commentTotalMap;
  }

  public void setCommentTotalMap(Map<String, Long> commentTotalMap) {
    this.commentTotalMap = commentTotalMap;
  }

  public void setCanUsePromotionType(PromotionType canUsePromotionType) {
    this.canUsePromotionType = canUsePromotionType;
  }

  public String getPromotionTag() {
    if (canUsePromotionType == null) {
      return null;
    }
    return canUsePromotionType.getcName();
  }

  public PromotionType getCanUsePromotionType() {
    return canUsePromotionType;
  }

  public PromotionProductVO getCanUsePromotion() {
    return canUsePromotion;
  }

  public BigDecimal getPromotionPrice() {
    return promotionPrice;
  }

  public void setPromotionPrice(BigDecimal promotionPrice) {
    this.promotionPrice = promotionPrice;
  }
}
