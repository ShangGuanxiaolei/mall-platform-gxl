package com.xquark.service.pintuan.impl;

import com.xquark.dal.mapper.PromotionPgMemberInfoMapper;
import com.xquark.dal.mapper.PromotionPgPriceMapper;
import com.xquark.dal.mapper.PromotionTranInfoMapper;
import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.status.PieceStatus;
import com.xquark.dal.vo.PromotionMemberImgVO;
import com.xquark.dal.vo.PromotionPgMemberInfoVO;
import com.xquark.dal.vo.PromotionTranInfoVO;
import com.xquark.helper.Transformer;
import com.xquark.service.pintuan.PromotionPgService;
import com.xquark.service.pintuan.PromotionTranInfoService;
import com.xquark.service.pricing.impl.pricing.impl.PgPromotionServiceAdapter;
import com.xquark.service.promotion.PromotionBaseInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author LiHaoYang
 * @ClassName PromotionTranInfoServiceImpl
 * @date 2018/9/13 0013
 */
@Service("promotionTranInfoService")
public class PromotionTranInfoServiceImpl implements PromotionTranInfoService {

  @Autowired
  private PromotionTranInfoMapper promotionTranInfoMapper;

  @Autowired
  private PromotionPgMemberInfoMapper promotionPgMemberInfoMapper;

  @Autowired
  private PromotionPgPriceMapper promotionPgPriceMapper;

  @Autowired
  private PromotionPgService promotionPgService;

  @Autowired
  private PgPromotionServiceAdapter promotionServiceAdapter;

  @Autowired
  private PromotionBaseInfoService promotionBaseInfoService;

  /**
   * 查询当前用户为团长的拼团信息
   */
  @Override
  public List<? extends PromotionTranInfoVO> selectMyGroupInfo(Long cpId, Integer page,
                                                               Integer pageSize) {
    String cpid = cpId == null ? null : String.valueOf(cpId);
    int sum = promotionTranInfoMapper.selectMyTranCount(cpid);
    int totalPage = sum % pageSize == 0 ? sum
            / pageSize : sum / pageSize + 1;

    List<? extends PromotionTranInfoVO> p = promotionTranInfoMapper
            .selectMyGroupInfo(cpid, (page - 1) * pageSize, pageSize);
    for (PromotionTranInfoVO item : p) {
      this.putExtraInfo(item, page, pageSize, sum, totalPage, item.getpCode());
    }
    return p;
  }

  /**
   * 查询当前用户不为团长的拼团信息
   */
  @Override
  public List<? extends PromotionTranInfoVO> selectMyNoGroupInfo(Long cpId, Integer page,
                                                                 Integer pageSize) {
    String cpid = cpId == null ? null : String.valueOf(cpId);
    int sum = promotionTranInfoMapper.selectMyNoTranCount(cpid);
    int totalPage = sum % pageSize == 0 ? sum
            / pageSize : sum / pageSize + 1;

    List<? extends PromotionTranInfoVO> p = promotionTranInfoMapper
            .selectMyNoGroupInfo(cpid, (page - 1) * pageSize, pageSize);
    for (PromotionTranInfoVO item : p) {
      this.putExtraInfo(item, page, pageSize, sum, totalPage, item.getpCode());
    }
    return p;
  }

  /**
   * 查询当前拼团用户头像
   */
  @Override
  public List<PromotionMemberImgVO> selectPromotioMyTranMemberImg(String tranCode) {
    List<PromotionMemberImgVO> MemberImg = promotionTranInfoMapper
            .selectPromotioMyTranMemberImg(tranCode);
    //头像为空的默认值
    if (MemberImg != null && MemberImg.size() > 0) {
      for (PromotionMemberImgVO promotionMemberImgVO : MemberImg) {
        if (promotionMemberImgVO.getHeadimgurl() == null
                || promotionMemberImgVO.getHeadimgurl() == "") {
          promotionMemberImgVO
                  .setHeadimgurl("http://images.handeson.com/FsJ5nrFiLqeoEjng1-OmKtDqOslc");
        }
      }
    }
    return MemberImg;
  }

  /**
   * 查询当前拼团traninfo信息
   */
  @Override
  public PromotionTranInfoVO selectPromotionPgTranCode(String tranCode) {
    return promotionTranInfoMapper.selectPromotionPgTranCode(tranCode);
  }

  @Override
  public int selectRestStockNum(String pCode) {
    return promotionTranInfoMapper.selectRestStock(pCode);
  }

  @Override
  public int selectRestStockNumBySkuCode(String skuCode, String pCode) {
    return promotionTranInfoMapper.selectRestStockBySkuCode(skuCode, pCode);
  }

  /**
   * 修改拼团状态
   */
  @Override
  public int updatePieceStatusByTranCode(String tranCode) {
    return promotionTranInfoMapper.updatePieceStatusByTranCode(tranCode);
  }

  private void putExtraInfo(final PromotionTranInfoVO infoVO, int page, int pageSize, int sum,
                            int totalPage, String pCode) {
    infoVO.setPage(page);
    infoVO.setPageSize(pageSize);
    infoVO.setPageCount(sum);
    infoVO.setTotalPage(totalPage);
    final String tranCode = infoVO.getPieceGroupTranCode();
    //判断是否是新人团
    infoVO.setNewPg(promotionPgService.PromotionNewPg(pCode));
    //获取优惠折扣
    Double skuDiscount = (infoVO.getSkuDiscount()) / 100;
    //获取优惠价（原价×优惠折扣）
    BigDecimal discountPrice = infoVO.getMarketPrice().multiply(BigDecimal.valueOf(skuDiscount));
    //四舍五入，保留两位小数
    BigDecimal discountPriceTwo = discountPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
    //转换为double类型
    Double discountPrice2 = discountPriceTwo.doubleValue();

    BigDecimal promotionPrice = promotionPgPriceMapper.selectPromotionPrice(tranCode);
    //如果拼团价不存在，取原有的价格计算逻辑
    Double aDouble = Optional.ofNullable(promotionPrice)
            .map(BigDecimal::doubleValue)
            .orElse(discountPrice2);
    //存入discountPrice
    infoVO.setDisCountPrice(aDouble);
    long date = infoVO.getPieceEffectTime() * 1000 * (60 * 60);

    Date d = infoVO.getGroupOpenTime();
    Timestamp timestamp = new Timestamp(date + d.getTime());

    long time = timestamp.getTime();
    infoVO.setPieceEffectTime(time);

    PromotionBaseInfo baseInfo = promotionBaseInfoService.selectByPCode(pCode);

    infoVO.setIsShowInvitation(StringUtils.equals(infoVO.getPieceStatus(), String.valueOf(PieceStatus.OPENED.getCode())) ||
    StringUtils.isNotBlank(tranCode) && "5".equals(baseInfo.getpStatus()) && promotionServiceAdapter.isInCutLineTime(pCode, infoVO::getGroupFinishTime));

    List<PromotionPgMemberInfoVO> pgMemberInfo
        = promotionPgMemberInfoMapper.selectMemberInfo(tranCode, null, 10);
    // 转换图片VO
    infoVO.setMemberImgList(Transformer.fromIterable(pgMemberInfo,
        promotionPgMemberInfoVO -> {
          PromotionMemberImgVO imgVO = new PromotionMemberImgVO();
          imgVO.setHeadimgurl(promotionPgMemberInfoVO.getHeadimgurl());
          imgVO.setPiece_group_tran_code(tranCode);
          imgVO.setPiece_status(infoVO.getPieceStatus());
          return imgVO;
        }));
  }

}
