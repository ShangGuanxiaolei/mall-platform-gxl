package com.xquark.service.pintuan.impl;


import com.xquark.dal.mapper.RecordPromotionPgMemberInfoMapper;
import com.xquark.dal.model.mypiece.PGMemberInfoVo;
import com.xquark.service.pintuan.RecordPromotionPgMemberInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("recordPromotionPgMemberInfoService")
public class RecordPromotionPgMemberInfoServiceImpl implements RecordPromotionPgMemberInfoService {


  @Autowired
  private RecordPromotionPgMemberInfoMapper recordPromotionPgMemberInfoMapper;


  @Override
  public boolean save(PGMemberInfoVo pgMemberInfoVo) {
    return recordPromotionPgMemberInfoMapper.save(pgMemberInfoVo) > 0;
  }

  @Override
  public int countLegalMember(String tranCode) {
    return recordPromotionPgMemberInfoMapper.countLegalMember(tranCode);
  }

  @Override
  public PGMemberInfoVo loadByCode(String memberCode) {
    return recordPromotionPgMemberInfoMapper.selectByMemberCode(memberCode);
  }

  @Override
  public boolean updateStatusByMemberCode(String memberCode, int status) {
    return recordPromotionPgMemberInfoMapper.updateStatusByMemberCode(memberCode, status) > 0;
  }

  @Override
  public boolean updateStatusByTranCode(String tranCode, int status, List<Integer> originCodeList) {
    return recordPromotionPgMemberInfoMapper.updateStatusByTranCode(tranCode, status, originCodeList) > 0;
  }

  @Override
  public boolean updateStatusByTranCodeAndMemberId(String tranCode, int status,
                                                   List<Integer> originCodeList) {
    return recordPromotionPgMemberInfoMapper
        .updateStatusByTranCodeAndMemberId(tranCode, status, originCodeList) > 0;
  }

  @Override
  public String selectOrderNoByTranCode(String tranCode) {
    return recordPromotionPgMemberInfoMapper.selectOrderNoByTranCode(tranCode);
  }

  @Override
  public String selectGroupHeadMemberId(String tranCode) {
    return recordPromotionPgMemberInfoMapper.selectGroupHeadMemberId(tranCode);
  }
}
