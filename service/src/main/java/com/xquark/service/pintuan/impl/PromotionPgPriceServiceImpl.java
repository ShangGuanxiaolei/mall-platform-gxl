package com.xquark.service.pintuan.impl;

import com.xquark.dal.mapper.PromotionPgPriceMapper;
import com.xquark.dal.model.PromotionPgPrice;
import com.xquark.dal.model.Sku;
import com.xquark.dal.type.PromotionType;
import com.xquark.service.pintuan.PromotionPgPriceService;
import com.xquark.service.reserve.PromotionReserveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @Author: gxl
 */
@Service
public class PromotionPgPriceServiceImpl implements PromotionPgPriceService {

    @Autowired
    private PromotionPgPriceMapper promotionPgPriceMapper;

    @Autowired
    private PromotionReserveService reserveService;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return promotionPgPriceMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(PromotionPgPrice record) {
        return promotionPgPriceMapper.insert(record);
    }

    @Override
    public PromotionPgPrice selectByPrimaryKey(Integer id) {
        return promotionPgPriceMapper.selectByPrimaryKey(id);
    }

    @Override
    public PromotionPgPrice loadByPromotionIdAndSkuId(String promotionId, Sku sku, PromotionType type) {
        if (Objects.equals(type, PromotionType.RESERVE)) {
            return reserveService.buildPromotionPgPrice(promotionId, sku);
        }
        final String skuId = Objects.requireNonNull(sku).getId();
        return promotionPgPriceMapper.selectByPromotionIdAndSkuId(promotionId, skuId, type);
    }

    @Override
    public int updateByPrimaryKeySelective(PromotionPgPrice record) {
        return promotionPgPriceMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(PromotionPgPrice record) {
        return promotionPgPriceMapper.updateByPrimaryKey(record);
    }

    @Override
    public PromotionPgPrice selectPromotionPgPriceByDetailCode(String pDetailCode) {
        return promotionPgPriceMapper.selectPromotionPgPriceByDetailCode(pDetailCode);
    }

    @Override
    public List<PromotionPgPrice> listByPromotionId(String promotionId, String productId, PromotionType type) {
        return promotionPgPriceMapper.listByPromotionIdAndProductId(promotionId, productId, type);
    }
}
