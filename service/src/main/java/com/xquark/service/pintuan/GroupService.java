package com.xquark.service.pintuan;
import com.xquark.dal.model.User;
import com.xquark.dal.model.mypiece.ConfirmAndCheckVo;
import com.xquark.dal.model.mypiece.StockCheckBean;
import com.xquark.service.pricing.base.PieceStateException;

import java.util.Map;

public interface GroupService {
    /**
     * 发起拼团
     * 流程修改后的逻辑:
     * ①条件判断-->订单确认页
     * ②支付controller-->记录信息服务
     */
    Map<String,Object> createTeam(ConfirmAndCheckVo confirmAndCheckVo, User buyer);

    /**
     * 提交订单前的检查
     * @param stockCheckBean stockCheckBean
     * @return Map<String,String>
     */
    Map<String,String> orderingCheck(StockCheckBean stockCheckBean) throws PieceStateException;
}