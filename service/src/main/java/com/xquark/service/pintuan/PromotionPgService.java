package com.xquark.service.pintuan;

import com.xquark.dal.model.PromotionPg;

public interface PromotionPgService {

    /**
     * 查询成团有效时间
     */
    PromotionPg selectPromotionPgBypCode(String pCode);

    /**
     * 查询是否可以参团
     */
    String selectCanJoinGroup(String pCode);

    /**
     * 查看是否是新人团
     */
    boolean PromotionNewPg(String pCode);
}
