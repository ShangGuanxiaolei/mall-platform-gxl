package com.xquark.service.pintuan;


import com.xquark.dal.model.mypiece.PGMemberInfoVo;

import java.util.List;

public interface RecordPromotionPgMemberInfoService {

  boolean save(PGMemberInfoVo pgMemberInfoVo);

  /**
   * 查询某个团的合法已参团人数
   */
  int countLegalMember(String tranCode);

  /**
   * 获取参团人信息
   */
  PGMemberInfoVo loadByCode(String memberCode);

  /**
   * 根据成员编码更新, 更新记录应唯一
   */
  boolean updateStatusByMemberCode(String memberCode, int status);

  /**
   * 根据团编码更新, 批量更新整个团的状态
   */
  boolean updateStatusByTranCode(String tranCode, int status, List<Integer> originCodeList);

  /**
   * 更新团中某个人的状态
   */
  boolean updateStatusByTranCodeAndMemberId(String tranCode,
                                            int status, List<Integer> originCodeList);

  /**
   * 查询某个团的拼主订单编号
   */
  String selectOrderNoByTranCode(String tranCode);

  /**
   * 查询某个团的拼主cpId
   */
  String selectGroupHeadMemberId(String tranCode);
}
