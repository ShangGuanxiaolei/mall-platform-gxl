package com.xquark.service.pintuan.impl;

import com.xquark.dal.mapper.PromotionPgMemberInfoMapper;
import com.xquark.dal.mapper.PromotionPgPriceMapper;
import com.xquark.dal.model.CashierItem;
import com.xquark.dal.vo.GroupShareVO;
import com.xquark.dal.vo.PromotionOrderGoodsInfoVO;
import com.xquark.dal.vo.PromotionPgMemberInfoVO;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pintuan.PromotionPgMemberInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionPgMemberServiceImpl
 * @date 2018/9/10 0010
 */
@Service("promotionPgMemberInfoService")
public class PromotionPgMemberInfoServiceImpl implements PromotionPgMemberInfoService {

  @Autowired
  private PromotionPgMemberInfoMapper promotionPgMemberInfoMapper;
  @Autowired
  private CashierService cashierService;
  @Autowired
  private PromotionPgPriceMapper promotionPgPriceMapper;

  @Override
  public PromotionOrderGoodsInfoVO selectGoodsInfo(String bizNo) {
    return selectGoodsInfo(bizNo, null);
  }

  @Override
  public PromotionOrderGoodsInfoVO selectGoodsInfo(String bizNo, Long cpId) {
    return selectGoodsInfo(bizNo, cpId, true, null);
  }

  @Override
  public PromotionOrderGoodsInfoVO selectGoodsInfoWithOrder(String bizNo, Long cpId,
      String orderNo) {
    return selectGoodsInfo(bizNo, cpId, true, orderNo);
  }

  /**
   * 查询用户拼团的商品详情
   */
  private PromotionOrderGoodsInfoVO selectGoodsInfo(String bizNo, Long cpId,
      boolean loadMemberInfo, String orderNo) {
    PromotionOrderGoodsInfoVO promotionOrderGoodsInfoVO = promotionPgMemberInfoMapper
        .selectGoodsInfo(bizNo);
    if (promotionOrderGoodsInfoVO == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拼团商品信息不存在");
    }
    BigDecimal promotionPrice = promotionPgPriceMapper.selectPromotionPrice(bizNo);
    //存入discountPrice
    promotionOrderGoodsInfoVO.setDisCountPrice(promotionPrice.doubleValue());
    if (loadMemberInfo) {
      List<PromotionPgMemberInfoVO> pgMemberInfo = promotionPgMemberInfoMapper
          .selectMemberInfo(bizNo, orderNo, 20);
      promotionOrderGoodsInfoVO.setMemberInfo(pgMemberInfo);
    }
    return promotionOrderGoodsInfoVO;

  }

  /**
   * 把数据转换为前端需要的分享vo
   */
  @Override
  public GroupShareVO loadShareVO(String tranCode) {
    PromotionOrderGoodsInfoVO infoVO = this.selectGoodsInfo(tranCode, null, false, null);
    if (infoVO == null) {
      return null;
    }
    GroupShareVO shareVO = new GroupShareVO();
    shareVO.setPrice(BigDecimal.valueOf(infoVO.getDisCountPrice()));
    shareVO.setOriginalPrice(infoVO.getMarketPrice());
    shareVO.setProductImgURL(infoVO.getImg());
    shareVO.setProductName(infoVO.getName());
    shareVO.setTranCode(infoVO.getPieceGroupTranCode());
    int total = this.selectPgMemberCount(tranCode);
    int curr = this.selectSuccessPromotionMemberCount(tranCode);
    shareVO.setCurrMember(curr);
    shareVO.setTotalMember(total);
    return shareVO;
  }

  /**
   * 根据bizNo查询主单号
   */
  @Override
  public String loadMainOrderNoByBizNo(String bizNo) {
    return promotionPgMemberInfoMapper.selectTranCodeAndpCodeByOrderNo2(bizNo);
  }

  @Override
  public PromotionOrderGoodsInfoVO loadByBizNo(String bizNo) {
    if (bizNo.startsWith("N")) {
      String orderNo = promotionPgMemberInfoMapper.selectTranCodeAndpCodeByOrderNo2(bizNo);
      return promotionPgMemberInfoMapper
          .selectTranCodeAndpCodeByOrderNo(orderNo);
    } else {
      List<CashierItem> items = cashierService.listByBizNo(bizNo);
      String[] batchBizNos = items.get(0).getBatchBizNos().split(",");
      String orderNo = batchBizNos[0];
      return promotionPgMemberInfoMapper
          .selectTranCodeAndpCodeByOrderNo(orderNo);
    }
  }

  @Override
  public PromotionOrderGoodsInfoVO loadByOrderNO(String orderNo) {
    return promotionPgMemberInfoMapper.selectTranCodeAndpCodeByOrderNo(orderNo);
  }

  @Override
  public String selectStatus(String pieceGroupTranCode) {
    return promotionPgMemberInfoMapper.selectStatus(pieceGroupTranCode);
  }

  @Override
  public List<Integer> selectMemberStatus(String pieceGroupTranCode) {
    return promotionPgMemberInfoMapper.selectMemberStatus(pieceGroupTranCode);
  }


  /**
   * 查询已拼团人数
   */
  @Override
  public int selectSuccessPromotionMemberCount(String bizNo) {
    return promotionPgMemberInfoMapper.selectSuccessPromotionMemberCount(bizNo);
  }

  @Override
  public int selectSuccessPromotionMemberCountAndSelf(String tranCode, Long cpId) {
    return promotionPgMemberInfoMapper.selectSuccessPromotionMemberAndSelf(tranCode, cpId);
  }

  @Override
  public int selectSuccessPromotionMemberCountAndOrder(String tranCode, String orderNo) {
    return promotionPgMemberInfoMapper.selectSuccessPromotionMemberAndOrder(tranCode, orderNo);
  }

  /**
   * 查询拼团总人数
   */
  @Override
  public int selectPgMemberCount(String bizNo) {
    return promotionPgMemberInfoMapper.selectPgMemberCount(bizNo);
  }

  /**
   * 查询拼团成员信息
   */
  @Override
  public List<PromotionPgMemberInfoVO> selectMemberInfo(String tranCode) {
    List<PromotionPgMemberInfoVO> pgMemberInfo = promotionPgMemberInfoMapper
        .selectMemberInfo(tranCode, null, null);
    //判断是否为空
    if (pgMemberInfo != null) {
      for (PromotionPgMemberInfoVO aPgMemberInfo : pgMemberInfo) {
        //获取每一个用户的头像
        String img = aPgMemberInfo.getHeadimgurl();
        //判断用户是否有头像
        if (img == null || img.equals("")) {
          //如果没有，显示为默认头像
          aPgMemberInfo
                  .setHeadimgurl("http://images.handeson.com/FsJ5nrFiLqeoEjng1-OmKtDqOslc");
        }
      }
      return pgMemberInfo;
    } else {
      return null;
    }
  }

  @Override
  public List<PromotionPgMemberInfoVO> selectMemberInfoBycpId(String tranCode, Long cpId) {
    return promotionPgMemberInfoMapper.selectMemberInfoBycpId(tranCode, cpId);
  }


  /**
   * 查询成团有效时间
   */
  @Override
  public int selectRegimentTime(String bizNo) {
    return promotionPgMemberInfoMapper.selectRegimentTime(bizNo);
  }

  /**
   * 查询开团时间
   */
  @Override
  public Date selectGroupOpenTime(String bizNo) {
    return promotionPgMemberInfoMapper.selectGroupOpenTime(bizNo);
  }

}
