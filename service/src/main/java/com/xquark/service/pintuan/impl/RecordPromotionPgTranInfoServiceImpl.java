package com.xquark.service.pintuan.impl;


import com.xquark.dal.mapper.RecordPromotionPgTranInfoMapper;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.service.pintuan.RecordPromotionPgTranInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Service("recordPromotionPgTranInfoService")
public class RecordPromotionPgTranInfoServiceImpl implements RecordPromotionPgTranInfoService {

  @Autowired
  private RecordPromotionPgTranInfoMapper recordPromotionPgTranInfoMapper;

  @Override
  public boolean save(PGTranInfoVo pgTranInfoVo) {
    return recordPromotionPgTranInfoMapper.save(pgTranInfoVo) > 0;
  }

  @Override
  public Map<String, Object> selectAllByTranCode(String pieceGroupTranCode) {
    return recordPromotionPgTranInfoMapper.selectAllByTranCode(pieceGroupTranCode);
  }

  @Override
  public Map<String, Object> selectMemberInfoByTranCodeAndMemberId(String pieceGroupTranCode, String memberId) {
    return recordPromotionPgTranInfoMapper.selectMemberInfoByTranCodeAndMemberId(pieceGroupTranCode, memberId);
  }

  @Override
  public BigDecimal loadPgDiscount(String pCode, String detailCode) {
	  return recordPromotionPgTranInfoMapper.selectPgDiscount(pCode, detailCode);
  }

  /**
   * 查询拼团信息
   */
  @Override
  public PGTranInfoVo loadPGTranInfo(String tranCode) {
    return recordPromotionPgTranInfoMapper.selectTranInfoVOByCode(tranCode);
  }

  @Override
  public boolean updateStatusByTranCode(String tranCode, int status, List<Integer> originCodeList) {
    return recordPromotionPgTranInfoMapper
        .updateStatusByTranCode(tranCode, status, originCodeList) > 0;
  }

  @Override
  public List<Map> selectAllByStatusPush() {
    return recordPromotionPgTranInfoMapper.selectAllByStatusPush();
  }
}
