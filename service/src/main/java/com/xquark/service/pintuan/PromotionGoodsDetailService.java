package com.xquark.service.pintuan;

import com.xquark.dal.model.Sku;
import com.xquark.dal.vo.PromotionGoodsDetailVO;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionGoodsDetailService
 * @date 2018/9/7 0007
 */
public interface PromotionGoodsDetailService {
    /**
     * 查询当前商品的拼团人数和优惠价格
     * @param pCode
     * @param tranCode
     * @return
     */
    List<PromotionGoodsDetailVO> selectPeopleAndDiscountPrice(String pCode,
        String tranCode);

    /**
     * 查询剩余时间和最高拼团价格
     * @param code
     * @param pCode
     * @param cpid
     * @return
     */
    PromotionGoodsDetailVO selectPromotionTimeAndMaxDiscount(String code, String pCode, Long cpid);


    List<Sku> selectSku(String code);


    /**
     * 查找当前活动的人数限制
     * @param pDetailCode
     * @return
     */
    Integer selectPromotionPriceLimit(String pDetailCode);

}
