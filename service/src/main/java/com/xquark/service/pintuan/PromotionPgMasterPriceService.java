package com.xquark.service.pintuan;

import com.xquark.dal.model.PromotionPgMasterPrice;

/**
 * @Author: gxl
 */
public interface PromotionPgMasterPriceService {

    int deleteByPrimaryKey(Integer id);

    int insert(PromotionPgMasterPrice record);

    int insertSelective(PromotionPgMasterPrice record);

    PromotionPgMasterPrice selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PromotionPgMasterPrice record);

    int updateByPrimaryKey(PromotionPgMasterPrice record);

    PromotionPgMasterPrice selectPromotionPgMasterPriceByDetailCode(String pDetailCode);
}
