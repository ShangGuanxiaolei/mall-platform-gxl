package com.xquark.service.pintuan;

import com.xquark.dal.vo.PromotionGroupDetailVO;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionGroupDetailService
 * @date 2018/9/15 0015
 */
public interface PromotionGroupDetailService {
    /**
     * 我的拼团详情
     * @param pieceGroupTranCode
     * @return
     */
    List<PromotionGroupDetailVO> selectGroupDetail(String pieceGroupTranCode);

    List<PromotionGroupDetailVO> selectGroupDetailByOrderNo(String orderNo);
    /**
     * 根据pCode查询成团人数
     */
    Integer findPieceGroupNum(String pCode);

    /**
     * 查询拼团每人限购数量
     * @param pCode
     * @return
     */
    Integer findPieceSkuLimit(String pCode);

    /**
     * 查拼主价格
     * @param pCode
     * @return
     */
    BigDecimal showOpenMemberPrice(String pCode);

    /**
     * 是否享受免单
     * @param pCode
     * @return
     */
    boolean findFreshPromotionByPCode(String pCode);
}
