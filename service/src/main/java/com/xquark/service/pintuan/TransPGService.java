package com.xquark.service.pintuan;

import com.xquark.dal.model.User;
import com.xquark.dal.model.mypiece.PGMemberInfoVo;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.service.BaseService;

/**
 * @author qiuchuyi
 * @date 2018/9/12 10:46
 */
public interface TransPGService extends BaseService {
    /**
     * 交互转化记录拼团信息
     */
     void transPGService(PGMemberInfoVo pgMemberInfoVo,int isGroupMaster, PGTranInfoVo pgTranInfoVo,  String orderNo, String subOrderNo, String orderItemNo, User user);
}
