package com.xquark.service.file.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xquark.dal.mapper.ImageMapper;
import com.xquark.dal.model.Image;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.file.ImageService;

@Service("imageService")
public class ImageServiceImpl extends BaseServiceImpl implements ImageService {

  @Autowired
  private ImageMapper imageMapper;

  @Override
  @Transactional
  public int insert(Image image) {
    return imageMapper.insert(image);
  }

  @Override
  @Transactional
  public int insertOrder(Image image) {
    return imageMapper.insert(image);
  }

  @Override
  public int delete(String id) {
    return imageMapper.deleteByPrimaryKey(id);
  }

  @Override
  public Image load(String id) {
    return imageMapper.selectByPrimaryKey(id);
  }

  @Override
  public Image loadByImgKey(String imgKey) {
    return imageMapper.selectByImgKey(imgKey);
  }
}
