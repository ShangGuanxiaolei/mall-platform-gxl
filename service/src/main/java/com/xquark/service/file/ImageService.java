package com.xquark.service.file;

import com.xquark.dal.model.Image;
import com.xquark.service.BaseEntityService;

public interface ImageService extends BaseEntityService<Image> {

  /**
   * 插入图片
   *
   * @param pid 用户ID(头像),产品ID(产品图像),店铺ID(店招)
   */
  public int insert(Image image);


  /**
   * 根据文件ID得到图片
   */
  public Image load(String id);

  /**
   * 根据文件key得到图片
   */
  public Image loadByImgKey(String imgKey);


  /**
   * 删除文件
   */
  public int delete(String id);
}
