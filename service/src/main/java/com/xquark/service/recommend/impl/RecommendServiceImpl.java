package com.xquark.service.recommend.impl;

import com.xquark.dal.mapper.RecommendMapper;
import com.xquark.dal.model.RecommendProductVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.recommend.RecommendService;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecommendServiceImpl implements RecommendService {

  @Autowired private RecommendMapper recommendMapper;

  @Override
  public List<RecommendProductVO> getRecommendProductList(String recommendCode) {
    if (StringUtils.isBlank(recommendCode)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "推荐页面不能为空");
    }
    return recommendMapper.getRecommendProduct(recommendCode);
  }
}
