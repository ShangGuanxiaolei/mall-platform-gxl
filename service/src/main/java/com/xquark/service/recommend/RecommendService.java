package com.xquark.service.recommend;

import com.xquark.dal.model.RecommendProductVO;

import java.util.List;

public interface RecommendService {

	List<RecommendProductVO> getRecommendProductList(String recommendCode);
}
