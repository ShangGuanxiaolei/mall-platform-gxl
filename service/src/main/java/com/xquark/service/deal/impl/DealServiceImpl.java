package com.xquark.service.deal.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.xquark.dal.mapper.DealLogMapper;
import com.xquark.dal.mapper.DealMapper;
import com.xquark.dal.model.Account;
import com.xquark.dal.model.Deal;
import com.xquark.dal.model.DealLog;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.User;
import com.xquark.dal.model.WithdrawApply;
import com.xquark.dal.status.DealStatus;
import com.xquark.dal.type.DealType;
import com.xquark.dal.vo.DealHistoryVO;
import com.xquark.service.account.AccountService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.deal.DealService;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;

@Service("dealService")
public class DealServiceImpl extends BaseServiceImpl implements DealService {

  @Autowired
  private DealMapper dealMapper;

  @Autowired
  private DealLogMapper dealLogMapper;

  @Autowired
  private AccountService accountService;

  private static String DANBAO_ACCOUNT_ID = "b8qp"; // account id 1

  @Override
  public Deal createDepositDeal(BigDecimal fee) {
    Deal deal = new Deal();
    deal.setDealNo(UniqueNoUtils.next(UniqueNoType.TDN));
    deal.setAccountFrom(null);

    Account account = accountService.loadByUserId(getCurrentUser().getId());
    deal.setAccountTo(account.getId());

    deal.setDealType(DealType.DEPOSIT);
    deal.setFee(fee);
    deal.setStatus(DealStatus.NEW);
    dealMapper.insert(deal);
    return deal;
  }

  @Override
  public Deal createDepositDeal(Order order) {
    Deal deal = new Deal();
    deal.setDealNo(UniqueNoUtils.next(UniqueNoType.TDN));
    deal.setAccountFrom(null);

    Account account = accountService.loadByUserId(order.getBuyerId());
    deal.setAccountTo(account.getId());
    deal.setOrderId(order.getId());
    deal.setDealType(DealType.DEPOSIT);
    deal.setFee(order.getTotalFee());
    deal.setStatus(DealStatus.NEW);
    dealMapper.insert(deal);
    return deal;
  }

  @Override
  public Deal createDeal(Deal deal) {
    // 创建交易
//		Deal deal = new Deal();
//		deal.setDealNo(UniqueNoUtils.next(UniqueNoType.DO));
//		deal.setDealType(DealType.DIRECT_PAY);
//		deal.setAccountFrom(accountService.loadByUserId(order.getBuyerId()).getId());
//		deal.setAccountTo(accountService.loadByUserId(order.getSellerId()).getId());
//		deal.setAmount(order.getTotalPrice());
//		deal.setOrderId(order.getId());
//		dealService.createDeal(deal);

    dealMapper.insert(deal);
    return deal;
  }

  @Override
  public Deal loadDeal(String id) {
    return dealMapper.selectByPrimaryKey(id);
  }

  @Override
  public Deal loadDealByOrder(String orderId) {
    // TODO fix the logic later
    List<Deal> deals = dealMapper.selectByOrder(orderId);
    for (Deal deal : deals) {
      if (deal.getDealType().equals(DealType.DIRECT_PAY)) {
        return deal;
      } else if (deal.getDealType().equals(DealType.DANBAO_PAY)) {
        return deal;
      }
    }
    return null;
  }

  public Deal loadDealByOrder(String orderId, DealType type) {
    return dealMapper.selectByOrderAndType(orderId, type);
  }

  @Override
  public Deal loadDealByWithdraw(String withdrawId) {
    return dealMapper.selectByWithdraw(withdrawId);
  }

  /**
   * 担保交易支付
   */
  @Transactional
  @Override
  public boolean halfDoDanbaoDeal(String dealId) {
    Deal deal = dealMapper.selectByPrimaryKey(dealId);
    if (null == deal) {
      return false;
    }
    if (!DealType.DANBAO_PAY.equals(deal.getDealType())) {
      return false;
    }
    // 买家账户减
    boolean flag1 = accountService.decreaseBlance(deal.getAccountFrom(), deal.getFee(), dealId);
    // 担保交易账户加
    boolean flag2 = accountService.increaseBlance(DANBAO_ACCOUNT_ID, deal.getFee(), dealId);

    boolean flag3 = dealMapper.updateStatus(dealId, DealStatus.IN_PROGRESS) > 0;

    return flag1 && flag2 && flag3;
  }

  /**
   * 担保交易退款
   */
  @Transactional
  @Override
  public boolean drawbackDanbaoDeal(String dealId) {
    Deal deal = dealMapper.selectByPrimaryKey(dealId);
    if (null == deal) {
      return false;
    }
    if (deal.getDealType() != DealType.DANBAO_PAY) {
      return false;
    }
    // 担保交易账户减
    boolean flag1 = accountService.decreaseBlance(DANBAO_ACCOUNT_ID, deal.getFee(), dealId);
    // 买家交易账户加
    boolean flag2 = accountService.increaseBlance(deal.getAccountFrom(), deal.getFee(), dealId);

    boolean flag3 = dealMapper.updateStatus(dealId, DealStatus.CLOSED) > 0;

    return flag1 && flag2 && flag3;
  }

  /**
   * 担保交易成功
   */
  @Transactional
  @Override
  public boolean finishDanbaoDeal(String dealId) {
    Deal deal = dealMapper.selectByPrimaryKey(dealId);
    if (null == deal) {
      return false;
    }
    if (deal.getDealType() != DealType.DANBAO_PAY) {
      return false;
    }
    // 担保交易账户减
    boolean flag1 = accountService.decreaseBlance(DANBAO_ACCOUNT_ID, deal.getFee(), dealId);
    // 卖家交易账户加
    boolean flag2 = accountService.increaseBlance(deal.getAccountTo(), deal.getFee(), dealId);

    boolean flag3 = dealMapper.updateStatus(dealId, DealStatus.SUCCESS) > 0;

    return flag1 && flag2 && flag3;
  }

  @Transactional
  @Override
  public boolean finishDeal(String dealId) {
    Deal deal = dealMapper.selectByPrimaryKey(dealId);
    if (null == deal) {
      return false;
    }
    if (DealType.DANBAO_PAY.equals(deal.getDealType())) {
      return false;
    }
    if (DealStatus.SUCCESS.equals(deal.getStatus())) {
      return false;
    }

    boolean success = true;
    // from交易账户减
    if (deal.getAccountFrom() != null) {
      boolean flag1 = accountService.decreaseBlance(deal.getAccountFrom(), deal.getFee(), dealId);
      success = success && flag1;
    }
    // to交易账户加
    if (deal.getAccountTo() != null) {
      boolean flag2 = accountService.increaseBlance(deal.getAccountTo(), deal.getFee(), dealId);
      success = success && flag2;
    }

    boolean flag3 = dealMapper.updateStatus(dealId, DealStatus.SUCCESS) > 0;
    success = success && flag3;

    return success;
  }

  @Transactional
  @Override
  public boolean finishDealByOrderId(String orderId) {
    List<Deal> deals = dealMapper.selectByOrder(orderId);
    if (CollectionUtils.isEmpty(deals)) {
      return false;
    }
    boolean success = false;
    for (Deal deal : deals) {
      DealType type = deal.getDealType();
      if (DealType.DIRECT_PAY.equals(type)) {
        success = dealMapper.updateStatus(deal.getId(), DealStatus.SUCCESS) > 0;
      }
    }
    return success;
  }

  @Override
  public boolean closeDeal(String dealId) {
    Deal deal = dealMapper.selectByPrimaryKey(dealId);
    if (null == deal) {
      return false;
    }
    if (deal.getDealType() == DealType.DANBAO_PAY) {
      return false;
    }
    log.info("close deal[" + dealId + "]");
    return dealMapper.updateStatus(dealId, DealStatus.CLOSED) > 0;
  }

  @Override
  public List<DealLog> listDealLogsByDeal(String id) {
    return dealLogMapper.selectByDealId(id);
  }

  @Override
  public Deal createDirectPayDealByOrder(Order order) {
    Deal deal = new Deal();
    deal.setDealNo(UniqueNoUtils.next(UniqueNoType.TDN));
    deal.setDealType(DealType.DIRECT_PAY);
    deal.setAccountFrom(accountService.loadByUserId(order.getBuyerId()).getId());
    deal.setAccountTo(accountService.loadByUserId(order.getSellerId()).getId());
    deal.setFee(order.getTotalFee());
    deal.setOrderId(order.getId());
    deal.setStatus(DealStatus.NEW);
    createDeal(deal);
    log.info("create direct deal[" + deal.getId() + "]");
    return deal;
  }

  @Override
  public Deal createDanbaoPayDealByOrder(Order order) {
    Deal deal = new Deal();
    deal.setDealNo(UniqueNoUtils.next(UniqueNoType.TDN));
    deal.setDealType(DealType.DANBAO_PAY);
    deal.setAccountFrom(accountService.loadByUserId(order.getBuyerId()).getId());
    deal.setAccountTo(accountService.loadByUserId(order.getSellerId()).getId());
    deal.setFee(order.getTotalFee());
    deal.setOrderId(order.getId());
    deal.setStatus(DealStatus.NEW);
    createDeal(deal);
    log.info("create danbao deal[" + deal.getId() + "]");
    return deal;
  }

  @Override
  public Deal createWithdraw(WithdrawApply withdraw) {
    Deal deal = new Deal();
    deal.setDealNo(UniqueNoUtils.next(UniqueNoType.TDN));
    deal.setDealType(DealType.WITHDRAW);
    deal.setStatus(DealStatus.NEW);
    deal.setFee(withdraw.getApplyMoney());
    deal.setWithdrawId(withdraw.getId());
    deal.setAccountFrom(accountService.loadByUserId(withdraw.getUserId()).getId());
    createDeal(deal);
    log.info("create withdraw deal[" + deal.getId() + "]");
    return deal;
  }

  @Override
  public List<DealHistoryVO> listDealByUserId(String userId, Pageable page) {
    return dealMapper.listDealByUserId(userId, page);
  }
}