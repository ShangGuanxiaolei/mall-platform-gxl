package com.xquark.service.deal;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.Deal;
import com.xquark.dal.model.DealLog;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.WithdrawApply;
import com.xquark.dal.type.DealType;
import com.xquark.dal.vo.DealHistoryVO;
import com.xquark.service.BaseService;

public interface DealService extends BaseService {

  /**
   * 创建充值交易
   */
  Deal createDepositDeal(BigDecimal fee);

  /**
   * 创建充值交易
   */
  Deal createDepositDeal(Order order);

  /**
   * 创建直接到帐交易
   */
  Deal createDirectPayDealByOrder(Order order);

  /**
   * 创建担保交易
   */
  Deal createDanbaoPayDealByOrder(Order order);

  /**
   * 创建提现交易
   */
  Deal createWithdraw(WithdrawApply withdraw);

//	 * 创建退款交易
//	 */
//	Deal createRefundDeal(Deal deal);
//
//	/**
//	 * 创建转让交易
//	 */
//	Deal createTransferDeal(Deal deal);
//
//	/**
//	 * 创建提现交易
//	 */
//	Deal createWithdrawDeal();

  /**
   * 创建交易
   */
  Deal createDeal(Deal deal);


  Deal loadDeal(String id);

  Deal loadDealByOrder(String orderId);

  Deal loadDealByOrder(String orderId, DealType type);

  Deal loadDealByWithdraw(String withdrawId);

  /**
   * 担保交易买家付款
   */
  boolean halfDoDanbaoDeal(String dealId);

  /**
   * 担保交易退款撤回
   */
  boolean drawbackDanbaoDeal(String dealId);

  /**
   * 担保交易成功完成
   */
  boolean finishDanbaoDeal(String dealId);

  /**
   * 完成交易
   */
  boolean finishDeal(String dealId);

  boolean finishDealByOrderId(String id);

  /**
   * 关闭交易
   */
  boolean closeDeal(String dealId);


  List<DealLog> listDealLogsByDeal(String id);

  List<DealHistoryVO> listDealByUserId(String userId, Pageable page);

}
