package com.xquark.service.antifake.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.mapper.AntifakeMapper;
import com.xquark.dal.mapper.BonusMapper;
import com.xquark.dal.model.Antifake;
import com.xquark.dal.model.Bonus;
import com.xquark.dal.model.BonusDetail;
import com.xquark.dal.status.AntifakeType;
import com.xquark.dal.vo.AntifakeVO;
import com.xquark.dal.vo.BonusVO;
import com.xquark.service.antifake.AntifakeService;
import com.xquark.service.bonus.BonusService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

/**
 * Created by chh on 17-6-12.
 */
@Service("antifakeServiceImpl")
public class AntifakeServiceImpl implements AntifakeService {

  protected Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private AntifakeMapper antifakeMapper;

  /**
   * 新增分红记录
   */
  @Override
  public int insertAntifake(Antifake antifake) {
    return antifakeMapper.insertAntifake(antifake);
  }

  /**
   * 订单发货后，关联防伪码记录
   */
  @Override
  public void insertAntifakeByOrder(String userId, String orderId, int isSystem,
      List<Map> codeMap) {
    for (Map map : codeMap) {
      String code = (String) map.get("code");
      AntifakeType codeType = (AntifakeType) map.get("codeType");
      Antifake antifake = new Antifake();
      antifake.setArchive(false);
      antifake.setCode(code);
      antifake.setCodeType(codeType);
      antifake.setIsSystem(isSystem);
      antifake.setOrderId(orderId);
      antifake.setUserId(userId);
      antifakeMapper.insertAntifake(antifake);
    }
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<AntifakeVO> list(Pageable pager, Map<String, Object> params) {
    return antifakeMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return antifakeMapper.selectCnt(params);
  }


  /**
   * 调用防伪系统接口检查防伪码是否正确
   */
  @Override
  public Map checkCode(String codes) {
    Map result = new HashMap();
    ArrayList codeResult = new ArrayList();
    codes = codes.replaceAll("，", ",");
    for (String code : codes.split(",")) {
      if (StringUtils.isNotEmpty(code)) {
        codeResult.add(code);
      }
    }
    if (codeResult.size() == 0) {
      return result;
    } else {
      result = getDataFromFwei(codeResult);
      // 将防伪码返回的接口数据进行整理，将所有得到的码标和类型放到一个list中
      if ("100".equals("" + result.get("errcode")) && result.get("data") != null) {
        JSONArray dataArray = (JSONArray) result.get("data");
        List<Map> okResult = new ArrayList<Map>();
        for (int i = 0, n = dataArray.size(); i < n; i++) {
          HashMap okMap = new HashMap();
          JSONObject object = (JSONObject) dataArray.get(i);
          String code = "" + object.get("logisticsCode");
          String type = (String) object.get("relation");
          if (codeResult.indexOf(code) != -1) {
            AntifakeType codeType = null;
            if ("1".equals(type)) {
              codeType = AntifakeType.TRUNK;
            } else if ("2".equals(type)) {
              codeType = AntifakeType.BOX;
            }
            okMap.put("code", code);
            okMap.put("codeType", codeType);
            okResult.add(okMap);
          }
        }
        result.put("tdata", okResult);
      }
    }
    return result;
  }

  /**
   * 根据传入的物流码，返回关联的物流码集合，比如传入盒码，返回盒码和对应的箱码，传入箱码，返回箱码和对应的所有盒码
   */
  @Override
  public List<String> getCodes(String code) {
    List resultCodes = new ArrayList();
    ArrayList codeResult = new ArrayList();
    codeResult.add(code);
    Map result = getDataFromFwei(codeResult);
    // 将防伪码返回的接口数据进行整理，将所有得到的码标和类型放到一个list中
    if ("100".equals("" + result.get("errcode")) && result.get("data") != null && ""
        .equals((String) result.get("errmsg"))) {
      JSONArray dataArray = (JSONArray) result.get("data");
      for (int i = 0, n = dataArray.size(); i < n; i++) {
        JSONObject object = (JSONObject) dataArray.get(i);
        String logisticsCode = "" + object.get("logisticsCode");
        String relation = "" + object.get("relation");
        resultCodes.add(logisticsCode);
        // 如果查询的是盒码，需要将中箱标，大箱标都返回
        if ("2".equals(relation)) {
          String fid = "" + object.get("fid");
          String ffid = "" + object.get("ffid");
          resultCodes.add(fid);
          resultCodes.add(ffid);
        }
      }
    } else {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, (String) result.get("errmsg"));
    }
    return resultCodes;
  }


  /**
   * 调用防伪查询接口，验证物流码，箱标物流码返回盒标列表
   *
   * @param codes 需要验证的物流码
   */
  private static Map getDataFromFwei(ArrayList codes) {
    Map map = null;
    // 调用第三方接口进行防伪查询
    String url = "http://fwei.now315.com/api.php?con=Logistics&act=storage_1267";
    try {
      Map<String, Object> mapParam = new HashMap<String, Object>();
      mapParam.put("key", "0f6cbdd7b5c93211c3012c2797dad506");
      mapParam.put("dbname", "anti_1267_db");
      mapParam.put("data", codes);
      String response = getData(url, mapParam);
      //log.info("防伪查询验证物流码接口返回 " + response);
      JSONObject jsonObject = JSONObject.parseObject(response);
      map = jsonObject;
    } catch (Exception e) {
      //log.error("防伪查询验证物流码接口出错!", e);
      e.printStackTrace();
    }
    return map;
  }

  public static String getData(String url, Map<String, Object> paramMap)
      throws UnsupportedEncodingException, IOException {
    String charset = "UTF-8";
    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    HttpURLConnection connect = (HttpURLConnection) (new URL(url).openConnection());
    connect.setRequestMethod("POST");
    connect.setDoOutput(true);
    connect.setConnectTimeout(1000 * 10);
    connect.setReadTimeout(1000 * 80);
    // 设置文件字符集:
    connect.setRequestProperty("Charset", "UTF-8");
    connect.setRequestProperty("ContentType", "application/json"); //采用通用的URL百分号编码的数据MIME类型来传参和设置请求头
    connect.setDoInput(true);
    // 连接
    connect.connect();

    // 获取URLConnection对象对应的输出流
    PrintWriter out = new PrintWriter(connect.getOutputStream());

    // 设置请求属性
    String param = "";
    if (paramMap != null && paramMap.size() > 0) {
      Iterator<String> ite = paramMap.keySet().iterator();
      while (ite.hasNext()) {
        String key = ite.next();// key
        Object value = paramMap.get(key);
        String valueString = "";
        if (value instanceof List) {
          valueString = JSONObject.toJSONString((List) value);
        } else {
          valueString = (String) value;
        }
        param += key + "=" + value + "&";
      }
      param = param.substring(0, param.length() - 1);
    }

    // 发送请求参数
    out.print(param);
    // flush输出流的缓冲
    out.flush();

    // 接收数据
    int responseCode = connect.getResponseCode();
    if (responseCode == HttpURLConnection.HTTP_OK) {
      InputStream in = connect.getInputStream();
      byte[] data = new byte[1024];
      int len = 0;
      while ((len = in.read(data, 0, data.length)) != -1) {
        outStream.write(data, 0, len);
      }
      in.close();
    }
    // 关闭连接
    connect.disconnect();
    String response = outStream.toString();
    return response;
  }

  /**
   * 检查同一个物流码不能由同一个人发货多次
   */
  @Override
  public List<String> checkExist(String userId, List codes) {
    return antifakeMapper.checkExist(userId, codes);
  }

  /**
   * 测试主方法
   */
  public static void main(String[] args) {
    ArrayList list = new ArrayList();
    list.add("1001000000");
    list.add("1001000001");
    list.add("1001000001333");
    Map map = getDataFromFwei(list);
    System.out.println(JSONObject.toJSONString(map));
  }

}
