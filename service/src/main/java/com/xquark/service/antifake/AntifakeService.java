package com.xquark.service.antifake;

import com.xquark.dal.model.Antifake;
import com.xquark.dal.model.Bonus;
import com.xquark.dal.model.BonusDetail;
import com.xquark.dal.vo.AntifakeVO;
import com.xquark.dal.vo.BonusVO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * Created by chh on 17-6-12. 物流码Service
 */
public interface AntifakeService {

  /**
   * 新增物流码记录
   */
  int insertAntifake(Antifake antifake);

  /**
   * 订单发货后，关联防伪码记录
   */
  void insertAntifakeByOrder(String userId, String orderId, int isSystem, List<Map> codeMap);

  /**
   * 调用防伪系统接口检查防伪码是否正确
   */
  Map checkCode(String codes);

  /**
   * 根据传入的物流码，返回关联的物流码集合，比如传入盒码，返回盒码和对应的箱码，传入箱码，返回箱码和对应的所有盒码
   */
  List<String> getCodes(String code);

  /**
   * 服务端分页查询数据
   */
  List<AntifakeVO> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);


  /**
   * 检查同一个物流码不能由同一个人发货多次
   */
  List<String> checkExist(String userId, List codes);

}
