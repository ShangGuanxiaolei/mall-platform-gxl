package com.xquark.service.test;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.Product;
import com.xquark.dal.model.SpiderItem;

public interface TestService {

  void fixThirdNull();

  void decodeSpiderShopId();

  void duplicateProduct();

  void encodeProductId();

  void thirdnull1();

  void thirdnull2();

  void thirdnull3();

  void fixImgOrder1();

  Map<String, Object> fixPorudctBySpider(String itemId);

  Map<String, Object> reSpiderForFragment(String type);

  List<SpiderItem> listItemsByAdmin(Map<String, Object> params, Pageable page);

  Long countItemsByAdmin(Map<String, Object> params);

  Long selectShopIdByAdmin(Integer from, Integer to);

  List<Product> listProductsByAdmin(Map<String, Object> params, Pageable page);

  Long countProductsByAdmin(Map<String, Object> params);

  Integer updateThirdId(String productId, String thirdId);
}
