package com.xquark.service.test.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.mapper.ProductImageMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.mapper.TestMapper;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.ProductImage;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.SpiderItem;
import com.xquark.dal.model.SpiderItemImg;
import com.xquark.dal.model.SpiderSku;
import com.xquark.dal.model.User;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.test.TestService;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;

@Service("testService")
public class TestServiceImpl extends BaseServiceImpl implements TestService {

  @Autowired
  private TestMapper testMapper;

  @Autowired
  private ProductImageMapper productImageMapper;

  @Autowired
  private SkuMapper skuMapper;

  @Value("${spider.url}")
  private String url;

  @Override
  public void fixThirdNull() {
    // 去除third_item_id为空的product
    List<Product> products = null;
    products = testMapper.selectProductByThirdNull();
    for (Product product : products) {
      // 查找spider 对应店铺下 itemList
      List<SpiderItem> items = null;
      items = testMapper.selectSpiderItemByProductName(
          product.getShopId(), product.getName());
      if (items.size() > 1 || items.size() == 0) {
        continue;
      }
      testMapper.updateProductThird(items.get(0).getItemId(),
          product.getId());

    }
  }

  @Override
  public void decodeSpiderShopId() {
    // 解码spider店铺ID
    List<SpiderItem> items = testMapper.selectSpiderItem();
    for (SpiderItem item : items) {
      long decoShopId = IdTypeHandler.decode(item.getShopId());
      testMapper.updateDecodeShopId(item.getId(), decoShopId);
    }
  }

  @Override
  public void duplicateProduct() {
    List<Product> thirdProducts = null;
    thirdProducts = testMapper.selectProductThirdIds();
    for (Product thirdProduct : thirdProducts) {
      // 获取该third_id、shop下商品
      List<Product> products = testMapper.selectDuplicateByThird(
          thirdProduct.getShopId(), thirdProduct.getThirdItemId()
              .longValue());
      // 是否在想去
      boolean hasLeft = false;
      boolean xqExist = false;
      for (Product product : products) {
        // product
        long ret = testMapper.selectIsXQ(product.getId());
        if (ret > 1) {
          xqExist = true;
          continue;
        }
        // pool
        ret = testMapper.selectIsXQPool(product.getId());
        if (ret > 1) {
          xqExist = true;
          continue;
        }
      }
      for (Product product : products) {
        // product
        long ret = testMapper.selectIsXQ(product.getId());
        if (ret > 1) {
          hasLeft = true;
          continue;
        }
        // pool
        ret = testMapper.selectIsXQPool(product.getId());
        if (ret > 1) {
          hasLeft = true;
          continue;
        }
        if (hasLeft || xqExist) {
          testMapper.delDuplicate(product.getId());
          log.info("id:" + product.getId() + "   id1:"
              + IdTypeHandler.decode(product.getId()));
        } else {
          hasLeft = true;
        }
      }
    }
  }

  @Override
  public void encodeProductId() {
    List<Long> productIds = testMapper.selectProductForEncode();
    for (long l : productIds) {
      String encode = IdTypeHandler.encode(l);
      testMapper.encodeProductId(encode, l);
    }
  }

  @Override
  public void thirdnull1() {
    try {
      List<Product> products = null;
      products = testMapper.selectProductByThirdNull();

      BufferedWriter writer = new BufferedWriter(new FileWriter(new File(
          "C:\\Users\\Think\\Desktop\\Result.txt")));

      for (Product product : products) {
        // 快店商品图片
        List<ProductImage> productImgs = productImageMapper
            .selectByProductId(product.getId());
        // 同个店铺下标题相同的item
        List<SpiderItem> items = testMapper.selectSpiderItemByName(
            product.getShopId(), product.getName());
        for (SpiderItem item : items) {
          List<SpiderItemImg> itemImgs = testMapper
              .selectSpiderItemImgs(item.getId());
          // 对比图片
          if (compare(productImgs, itemImgs)) {
            // 设置third_id
            // testMapper.updateProductThird(item.getItemId(),
            // product.getId());
            writer.write("spiderId:" + item.getId()
                + ", productId:" + product.getId() + "\r\n");
          }
        }
      }
      writer.close();
    } catch (Exception e) {
      // TODO: handle exception
    }
  }

  @Override
  public void thirdnull2() {
    try {
      List<Product> products = null;
      products = testMapper.selectProductByThirdNull();

      BufferedWriter writer = new BufferedWriter(new FileWriter(new File(
          "C:\\Users\\Think\\Desktop\\Result.txt")));

      for (Product product : products) {
        // 快店商品图片
        List<ProductImage> productImgs = productImageMapper
            .selectByProductId(product.getId());
        // 快店商品sku
        List<Sku> productSkus = skuMapper.selectByProductId(product
            .getId());
        // 同个店铺下标题相同的item
        List<SpiderItem> items = testMapper.selectSpiderItemByName(
            product.getShopId(), product.getName());
        for (SpiderItem item : items) {
          List<SpiderItemImg> itemImgs = testMapper
              .selectSpiderItemImgs(item.getId());
          List<SpiderSku> itemSkus = testMapper
              .selectSpiderItemSkus(item.getId());
          // 对比图片、sku
          if (compare(productImgs, itemImgs)
              && compare1(productSkus, itemSkus)) {
            // 设置third_id
            writer.write("spiderId:" + item.getId()
                + ", productId:" + product.getId() + "\r\n");
            testMapper.updateProductThird(item.getItemId(),
                product.getId());
          }
        }
      }
      writer.close();
    } catch (Exception e) {
      // TODO: handle exception
    }
  }

  @Override
  public void thirdnull3() {
    List<Long> shopIds = null;
    shopIds = testMapper.selectxquarkShopIdsByThirdNull();
    for (long shopId : shopIds) {
      List<SpiderItem> items = testMapper
          .selectSpiderItemByShopId(IdTypeHandler.encode(shopId));
      if (items.size() == 0) {
        // 设置快店shop全商品来源api
        testMapper.updateSourceByShopId(shopId);
      }
    }
  }

  @Override
  public void fixImgOrder1() {
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter(new File(
          "C:\\Users\\Think\\Desktop\\Result.txt")));
      // spider下店铺
      List<String> sShopIds = testMapper.selectSpiderShop();
      if (sShopIds != null) {
        for (String sShopId : sShopIds) {
          List<SpiderItem> items = testMapper
              .selectSpiderItemByShopId(sShopId);
          if (items != null) {
            for (SpiderItem item : items) {
              // item下存在type=1的第一张img
              List<SpiderItemImg> itemImgs = testMapper.selectImgByItemId(item.getId());
              if (itemImgs == null) {
                continue;
              }
              // xquark存在该third_id的product
              List<Product> products = testMapper.selectProductByThirdId(
                  item.getShopId(), item.getItemId());
              if (products != null) {
                for (Product product : products) {
                  ProductImage productImg = testMapper.selectImgByProductId(product.getId());
                  // compare
                  if (productImg != null) {
                    if (itemImgs.size() == 0 || itemImgs.get(0).getImg() == null
                        || productImg.getImg() == null) {
                      continue;
                    }
                    // 比对第一张图片
                    if (!itemImgs.get(0).getImg().equals(productImg.getImg())) {
                      // 同步图片
                      writer.write("spiderId:" + item.getId()
                          + ", productId:" + IdTypeHandler.decode(product.getId()) + "\r\n");

                      Product modProduct = new Product();
                      modProduct.setId(product.getId());
                      Product newProduct = new Product();
                      newProduct.setId(product.getId());
                      List<ProductImage> productImgs = itemImgToProductImg(itemImgs);
                      saveImgs(modProduct, newProduct, productImgs);
                    }
                  }
                }
              }
            }
          }
        }
      }
      writer.close();
    } catch (Exception e) {
      // TODO: handle exception
      log.error("error-------------------" + e);
    }
  }

  @Override
  public Map<String, Object> fixPorudctBySpider(String itemId) {
    Map<String, Object> result = new HashMap<String, Object>();
    // 所有图片有问题的商品
    List<Product> products = testMapper.selectProductsErrorImg(itemId);
    if (products.size() > 0) {
      for (Product product : products) {
        if (product.getThirdItemId() != null) {
          // 触发item搬家
          User user = new User();
          user.setId(product.getUserId());
          user.setShopId(product.getShopId());

          result = move(user, product.getThirdItemId().toString());
          log.info(product.getId() + " moveItem_fix, code:"
              + result.get("statusCode"));
        } else {
          log.info(product.getId() + " moveItem_fix, thirdItemId is null");
        }
      }
    } else {
      // test
      User user = new User();
      user.setId("71kfsdfg");
      user.setShopId("3imxwj0a");
      result = move(user, "43355330634");
    }
    return result;
  }

  @Override
  public Map<String, Object> reSpiderForFragment(String type) {
    Map<String, Object> result = new HashMap<String, Object>();
    List<Product> products = testMapper.selectReSpiderProducts(type);
    int i = 0;
    for (Product product : products) {
      if (product.getThirdItemId() != null) {
        if (i > 1000) {
          // sleep 5min
          try {
            Thread.sleep(300000);
          } catch (Exception e) {
            log.error("thread sleep error.", e);
          }
          i = 0;
        }
        User user = new User();
        user.setId(product.getUserId());
        user.setShopId(product.getShopId());
        move(user, product.getThirdItemId().toString());
        ++i;
      }
    }

    return result;
  }

  private Map<String, Object> move(User user, String ItemId) {
    final Map<String, String> params = new HashMap<String, String>();
    params.put("ouerUserId", user.getId());
    params.put("ouerShopId", user.getShopId());
    params.put("url", "http://item.taobao.com/item.htm?id=" + ItemId);

    // 搬家
    final Map<String, Object> result = new HashMap<String, Object>();
    log.info(params.toString());
    final HttpInvokeResult rslt = PoolingHttpClients.post(url
        + "/moveItem/url/async", params);
    if (!rslt.isOK()) {
      log.error("Error to moveItem:" + rslt, rslt.getException()
          + "params:" + params.toString());
      result.put("statusCode", "501");
      return result;
    }

    final JSONObject obj = JSONObject.parseObject(rslt.getContent());
    if (obj.getIntValue("errorCode") != 200) {
      log.error("Failed to qeury movestatus:" + rslt + " params:"
          + params.toString());
      result.put("statusCode", "502");
      return result;
    }

    if (obj.getJSONObject("data").getIntValue("statusCode") != 200) {
      if (obj.getJSONObject("data").getIntValue("statusCode") == 601) {
        log.error("shop not exists:" + rslt + " params:"
            + params.toString());
        result.put("statusCode", "601");
        return result;
      } else {
        log.error("unknow error:" + rslt + " params:"
            + params.toString());
        result.put("statusCode", "602");
        return result;
      }
    }

    result.put("statusCode", "200");
    result.put("shopId", user.getShopId());

    return result;
  }

  private List<ProductImage> itemImgToProductImg(List<SpiderItemImg> itemImgs) {
    List<ProductImage> productImgs = new ArrayList<ProductImage>();
    for (SpiderItemImg itemImg : itemImgs) {
      ProductImage productImg = new ProductImage();
      productImg.setImg(itemImg.getImg());
      productImg.setType(itemImg.getType());
      productImg.setImgOrder(itemImg.getOrderNum());
      productImgs.add(productImg);
    }
    return productImgs;
  }

  private void saveImgs(Product modProduct, Product product,
      List<ProductImage> imgs) {
    if (imgs == null) {
      imgs = new ArrayList<ProductImage>();
    }
    // 保存关联表
    List<ProductImage> modImgs = null; // 取出原标签
    if (modProduct != null) {
      modImgs = productImageMapper.selectByProductId(product.getId());
    } else {
      modImgs = new ArrayList<ProductImage>();
    }
    for (ProductImage img : imgs) {
      img.setProductId(product.getId());
      boolean hit = false;
      int i = 0;
      for (ProductImage modImg : modImgs) {
        if (img.getImg().equals(modImg.getImg())) {
          hit = true;
          if (img.getImgOrder() != modImg.getImgOrder()) {
            // img mathc, order does not math
            // udpate
            productImageMapper.updateImgOrder(img);
          } else {
            // img match, order match
            // do nothing
          }
          // remove key
          modImgs.remove(i);
          break;
        }
        i++;
      }
      if (!hit) {
        // img does not match
        // insert
        productImageMapper.insert(img);
      }
    }
    if (modImgs.size() > 0) {
      for (ProductImage modImg : modImgs) {
        productImageMapper.updateForArchive(modImg.getId());
      }
    }
  }

  @Override
  public Long selectShopIdByAdmin(Integer from, Integer to) {
    long shopId = testMapper.selectShopIdByAdmin(from, to);
    // 是否spider存在
    if (testMapper.checkSpiderShop(IdTypeHandler.encode(shopId)) > 0) {
      return shopId;
    } else {
      return selectShopIdByAdmin(
          Integer.parseInt(String.valueOf(shopId)), to);
    }
  }

  @Override
  public List<SpiderItem> listItemsByAdmin(Map<String, Object> params,
      Pageable pageable) {
    return testMapper.listItemsByAdmin(params, pageable);
  }

  @Override
  public Long countItemsByAdmin(Map<String, Object> params) {
    return testMapper.countItemsByAdmin(params);
  }

  @Override
  public List<Product> listProductsByAdmin(Map<String, Object> params,
      Pageable pageable) {
    return testMapper.listProductsByAdmin(params, pageable);
  }

  @Override
  public Long countProductsByAdmin(Map<String, Object> params) {
    return testMapper.countProductsByAdmin(params);
  }

  @Override
  public Integer updateThirdId(String productId, String thirdId) {
    // 校验是否在spider存在
    if (testMapper.isSpider(thirdId) == 0) {
      return -1;
    }
    // 校验是否xquark is null
    if (testMapper.isxquark(thirdId) > 0) {
      return -2;
    }
    return testMapper.updateThirdId(productId, thirdId);
  }

  private static boolean compare(List<ProductImage> a, List<SpiderItemImg> b) {
    if (a.size() != b.size()) {
      return false;
    }
    for (int i = 0; i < a.size(); i++) {
      if (!a.get(i).getImg().equals(b.get(i).getImg())) {
        return false;
      }
    }
    return true;
  }

  private static boolean compare1(List<Sku> a, List<SpiderSku> b) {
    if (a.size() != b.size()) {
      return false;
    }
    for (int i = 0; i < a.size(); i++) {
      if (!a.get(i).getSpec().equals(b.get(i).getSpec())) {
        return false;
      }
    }
    return true;
  }
}
