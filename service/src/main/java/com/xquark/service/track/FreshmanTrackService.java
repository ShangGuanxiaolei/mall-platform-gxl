package com.xquark.service.track;

import com.xquark.dal.model.FreshmanTrack;
import com.xquark.dal.model.FreshmanTrackInfoVo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface FreshmanTrackService {

    List<FreshmanTrack> selectAllFreshmanTrack();

    FreshmanTrack selectFreshmanTrackByGrade(String grade);

    /**
     * 累计消费金额
     * @param currentCpid
     * @return
     */
    BigDecimal queryConsumedMoneyByCpid(Long currentCpid);

    FreshmanTrackInfoVo getFreshmanTrackInfo(Long currentCpid);

    /**
     * 是否有VIP订单
     * @param currentCpid
     * @return
     */
    Integer queryIsVIPOder(Long currentCpid);

    /**
     * VIP套餐生效时间
     * @param orderId
     * @return
     */
    Date queryVIPOrderPaidAtById(Integer orderId);

    /**
     * 满500s身份生效时间
     * @param currentCpid
     * @return
     */
    Date queryIdentityActiveTime(Long currentCpid);

    /**
     * 根据该用户的累计消费金额. 判断用户是否弹过窗(满200和升VIP弹窗)
     * @param currentCpid 当前用户cpid
     * @param consumedAmount 消费金额
     * @param vipOrderId vip订单ID
     * @return
     */
    Integer queryFreshmanFlag(Long currentCpid, Integer consumedAmount, Integer vipOrderId);

    Date queryFreshmanTrackStartTime();
}
