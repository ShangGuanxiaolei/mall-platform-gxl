package com.xquark.service.track.impl;

import com.xquark.dal.mapper.FreshManFlagMapper;
import com.xquark.dal.mapper.FreshmanTrackMapper;
import com.xquark.dal.mapper.UserMapper;
import com.xquark.dal.model.FreshManFlagVo;
import com.xquark.dal.model.FreshmanTrack;
import com.xquark.dal.model.FreshmanTrackInfoVo;
import com.xquark.dal.model.User;
import com.xquark.service.track.FreshmanTrackService;
import com.xquark.utils.DateUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class FreshmanTrackServiceImpl implements FreshmanTrackService {

    private static final Log log = LogFactory.getLog(FreshmanTrackServiceImpl.class);

    @Autowired
    private FreshmanTrackMapper freshmanTrackMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private FreshManFlagMapper freshManFlagMapper;

    @Override
    public List<FreshmanTrack> selectAllFreshmanTrack() {
        return freshmanTrackMapper.selectAllFreshmanTrack();
    }

    @Override
    public FreshmanTrack selectFreshmanTrackByGrade(String grade) {
        return freshmanTrackMapper.selectFreshmanTrackByGrade(grade);
    }

    @Override
    public BigDecimal queryConsumedMoneyByCpid(Long currentCpid) {
        return freshmanTrackMapper.queryConsumedMoneyByCpid(currentCpid);
    }

    @Override
    public Date queryFreshmanTrackStartTime() {
        return freshmanTrackMapper.queryFreshmanTrackStartTime();
    }

    @Override
    public FreshmanTrackInfoVo getFreshmanTrackInfo(Long currentCpid) {
        // 所有新人引导信息
        List<FreshmanTrack> freshmanTracks = freshmanTrackMapper.selectAllFreshmanTrack();
        // 新人进度条信息
        FreshmanTrackInfoVo freshmanTrackInfoVo = new FreshmanTrackInfoVo();
        //已消费金额
        BigDecimal consumedAmount = freshmanTrackMapper.queryConsumedMoneyByCpid(currentCpid);
        boolean isValidVIPOrder = false;
        Date vipOrderPaidAt = new Date();
        Integer vipOderId = freshmanTrackMapper.queryIsVIPOder(currentCpid); //是否下过VIP订单
        if (null != vipOderId && vipOderId > 0) {
            vipOrderPaidAt = freshmanTrackMapper.queryVIPOrderPaidAtById(vipOderId); //VIP套餐升级时间
            if (vipOrderPaidAt.after(new Date())) {
                isValidVIPOrder = true; //VIP订单有效
            }
        }
        String guideTextResult = null; // 进度条顶部文案
        String identityEffectTime = null; //身份生效时间,为空时不展示
        FreshManFlagVo freshManFlagVo = freshManFlagMapper.selectFlag4Toast(currentCpid);
        if (freshManFlagVo != null && freshManFlagVo.getVipType() == 3 && !isValidVIPOrder) { // 白人开团满n次升VIP
            // 恭喜你成为VIP
            guideTextResult = "恭喜您成为VIP";
            // VIP套餐身份生效时间,从支付完成页记录的时间中获取
            identityEffectTime = getIdentityActivateTime(freshManFlagVo.getEffectAt());
        } else {
            if (consumedAmount.intValue() >= 0 && consumedAmount.intValue() < 200) { // 累计消费<200
                // 再消费{0}元立得{1}德分
                for (int i = 0; i < freshmanTracks.size(); i++) {
                    if (0 == Integer.valueOf(freshmanTracks.get(i).getGrade())) {
                        String guideText = freshmanTracks.get(i).getGuideText(); // 进度条顶部文案
                        if (i + 1 <= freshmanTracks.size()) {
                            Integer isGiftPoint = freshmanTracks.get(i + 1).getIsGiftPoint();
                            if (1 == isGiftPoint) { //满足赠送德分条件
                                String point = freshmanTracks.get(i + 1).getPoint(); // 赠送德分数
                                String amount = freshmanTracks.get(i + 1).getAmount(); // 需要累计消费金额
                                String a = guideText.replace("{0}", String.valueOf(new BigDecimal(amount).subtract(consumedAmount).setScale(2, BigDecimal.ROUND_HALF_EVEN).stripTrailingZeros().toPlainString()));
                                guideTextResult = a.replace("{1}", point);
                                break;
                            }
                        }
                    }
                }
            } else if (consumedAmount.intValue() >= 200 && consumedAmount.intValue() < 500) {
                if (consumedAmount.intValue() >= 368) {
                    if (isValidVIPOrder) { //有VIP订单
                        // 恭喜你成为VIP
                        guideTextResult = "恭喜您成为VIP";
                        // VIP套餐身份生效时间,从支付完成页记录的时间中获取
                        if (freshManFlagVo != null && vipOrderPaidAt.before(freshManFlagVo.getEffectAt() == null ? DateUtils.addSeconds(new Date(), 1800) : freshManFlagVo.getEffectAt())) {
                            identityEffectTime = getIdentityActivateTime(vipOrderPaidAt);
                        } else if (freshManFlagVo != null){
                            identityEffectTime = getIdentityActivateTime(freshManFlagVo.getEffectAt());
                        }
                    } else {
                        // 再消费{0}元升级VIP
                        guideTextResult = calcConsumedTotalTitle(freshmanTracks, consumedAmount);
                    }
                } else { // 累计消费<368
                    guideTextResult = calcConsumedTotalTitle(freshmanTracks, consumedAmount);
                }
            } else { // 累计消费>=500
                if (isValidVIPOrder) {
                    // 恭喜你成为VIP
                    guideTextResult = "恭喜您成为VIP";
                    // VIP套餐身份生效时间,从支付完成页记录的时间中获取
                    if (freshManFlagVo != null && vipOrderPaidAt.before(freshManFlagVo.getEffectAt() == null ? DateUtils.addSeconds(new Date(), 1800) : freshManFlagVo.getEffectAt())) {
                        identityEffectTime = getIdentityActivateTime(vipOrderPaidAt);
                    } else if (freshManFlagVo != null){
                        identityEffectTime = getIdentityActivateTime(freshManFlagVo.getEffectAt());
                    }
                } else {
                    // 已消费满{0}元，恭喜您成为VIP
                    for (FreshmanTrack freshmanTrack : freshmanTracks) {
                        if (2 == Integer.valueOf(freshmanTrack.getGrade())) {
                            String guideText = freshmanTrack.getGuideText(); // 进度条顶部文案
                            Integer isGiftPoint = freshmanTrack.getIsGiftPoint();
                            if (0 == isGiftPoint) { //不满足赠送德分条件
                                String amount = freshmanTrack.getAmount(); // 已消费满金额
                                guideTextResult = guideText.replace("{0}", amount);
                                break;
                            }
                        }
                    }
                    // 满500身份生效时间
                    if (freshManFlagVo != null) {
                        identityEffectTime = getIdentityActivateTime(freshManFlagVo.getEffectAt());
                    }
                }
            }
        }

        Date trackActiveTime = freshmanTrackMapper.queryFreshmanTrackStartTime();
        User user = userMapper.selectByCpId(currentCpid);
        // 进度条节点信息集合
        List<Map<String, Integer>> progressor = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(freshmanTracks)) {
            for (FreshmanTrack freshmanTrack : freshmanTracks) {
                Map<String, Integer> progressorInfo = new HashMap<>();
                progressorInfo.put("amount", Integer.valueOf(freshmanTrack.getAmount()));
                progressorInfo.put("point", Integer.valueOf(freshmanTrack.getPoint()));
                if (Integer.parseInt(freshmanTrack.getGrade()) < 2) {
                    if (Integer.parseInt(freshmanTrack.getGrade()) == 0) {
                        if (user.getCreatedAt().after(trackActiveTime)) {
                            // 活动之后注册
                            progressorInfo.put("point", Integer.valueOf(freshmanTrack.getPoint()));
                        } else {
                            progressorInfo.put("point", 0);
                        }
                    }
                    progressorInfo.put("isVip", 0);
                } else {
                    progressorInfo.put("isVip", 1);
                }
                progressor.add(progressorInfo);
            }
        }

        // 底部活动生效时间
        String activityEffectTime = null;
        SimpleDateFormat sdfYMD = new SimpleDateFormat("yyyy年M月d");
        String formatDate = sdfYMD.format(trackActiveTime);
        String a = "活动时间：自{}起";
        activityEffectTime = a.replace("{}", formatDate);

        freshmanTrackInfoVo.setConsumedAmount(consumedAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN).stripTrailingZeros());
        freshmanTrackInfoVo.setTitle(guideTextResult);
        freshmanTrackInfoVo.setIdentityEffectTime(identityEffectTime);
        freshmanTrackInfoVo.setActivityEffectTime(activityEffectTime);
        freshmanTrackInfoVo.setProgressor(progressor);

        return freshmanTrackInfoVo;
    }

    @Override
    public Integer queryIsVIPOder(Long currentCpid) {
        return freshmanTrackMapper.queryIsVIPOder(currentCpid);
    }

    @Override
    public Date queryVIPOrderPaidAtById(Integer orderId) {
        return freshmanTrackMapper.queryVIPOrderPaidAtById(orderId);
    }

    @Override
    public Date queryIdentityActiveTime(Long currentCpid) {
        return freshmanTrackMapper.queryIdentityActiveTime(currentCpid);
    }

    @Override
    public Integer queryFreshmanFlag(Long currentCpid, Integer consumedAmount, Integer vipOrderId) {
        int result = consumedAmount >= 500 ? 500 : consumedAmount >= 200 ? 200 : 0;
        if (null == vipOrderId) {
            return freshmanTrackMapper.queryFreshmanFlagNotVIPOrderId(currentCpid, result);
        }
        return freshmanTrackMapper.queryFreshmanFlagHaveVIPOrderId(currentCpid);
    }

    // 再消费{0}元升级VIP
    private String calcConsumedTotalTitle(List<FreshmanTrack> freshmanTracks, BigDecimal consumedAmount) {
        String guideTextResult = null;
        for (int i = 0; i < freshmanTracks.size(); i++) {
            if (1 == Integer.valueOf(freshmanTracks.get(i).getGrade())) {
                String guideText = freshmanTracks.get(i).getGuideText(); // 进度条顶部文案
                if (i + 1 <= freshmanTracks.size()) {
                    Integer isGiftPoint = freshmanTracks.get(i + 1).getIsGiftPoint();
                    if (0 == isGiftPoint) { //不满足赠送德分条件
                        String amount = freshmanTracks.get(i + 1).getAmount(); // 需要累计消费金额
                        String againConsumedAmount = new BigDecimal(amount).subtract(consumedAmount).setScale(2, BigDecimal.ROUND_HALF_EVEN).stripTrailingZeros().toPlainString();
                        guideTextResult = guideText.replace("{0}", againConsumedAmount);
                        break;
                    }
                }
            }
        }
        return guideTextResult;
    }

    // 新人身份生效时间解析
    private String getIdentityActivateTime(Date paidAt) {
        String identityEffectTime = null;
        if (null != paidAt) {
            if (paidAt.getTime() > System.currentTimeMillis()) {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                String paidAtStr = sdf.format(paidAt);
                identityEffectTime = "身份将于" + paidAtStr + "左右生效";
            }
        }
        return identityEffectTime;
    }
}
