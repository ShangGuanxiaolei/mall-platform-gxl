package com.xquark.service.vipRight;

import com.xquark.dal.type.PromotionType;

public class VipRightParams {

    // xquark_user表中的id
    private String buyerId;
    // 订单类型
    private String orderType;
    // 活动类型
    private PromotionType type;
    private String tranCode;

    private Long cpId;

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public PromotionType getType() {
        return type;
    }

    public void setType(PromotionType type) {
        this.type = type;
    }

    public String getTranCode() {
        return tranCode;
    }

    public void setTranCode(String tranCode) {
        this.tranCode = tranCode;
    }

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }
}
