package com.xquark.service.vipRight.impl;

import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.status.OrderStatus;
import com.xquark.service.vipRight.VipRightParams;
import com.xquark.service.vipRight.VipRightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VipRightServiceImpl implements VipRightService {

    @Autowired
    private OrderMapper orderMapper;


    @Override
    public Integer checkValid(VipRightParams params) {
        Integer validation = 0;
        // 没有手机号的用户不让下单
        Integer count = orderMapper.checkPhoneNoValid(params.getCpId());
        if (count < 1) {
            validation = 3;
        }

        List<Order> orders = orderMapper.selectByTypeAndBuyerId(
                params.getBuyerId(),
                params.getOrderType()
        );
        // 如果未查到订单则该用户未购买过VIP资格商品，可以提交订单
        if (orders.size() < 1) {
            return validation;
        }
        for (Order order : orders) {
            // 已支付的不允许提交订单
            if (OrderStatus.PAID.equals(order.getStatus())||
                    OrderStatus.SUCCESS.equals(order.getStatus())||
                    OrderStatus.SHIPPED.equals(order.getStatus())||
                    OrderStatus.DELIVERY.equals(order.getStatus())) {
                validation = 1;
                break;
            }
            // 若查到订单则判断订单是否已支付，30分钟内未支付的不允许提交订单
            if (OrderStatus.SUBMITTED.equals(order.getStatus())) {
                long diff = System.currentTimeMillis() - order.getCreatedAt().getTime();
                long m30 = 30 * 60 * 1000;
                if (diff <= m30) {
                    validation = 2;
                    break;
                }
            }
        }
        return validation;
    }


//    @Override
//    public void updateOrderHeader(String payNo) {
//        List<Order> orders = orderService.loadByPayNo(payNo);
//        for (Order order : orders) {
//            if (null != order && OrderSortType.VIP_RIGHT.equals(order.getOrderType())) {
//                OrderHeader header = new OrderHeader();
//                header.setOrderid(order.getOrderNo());
//                header.setJoinType(2);
//                header.setSource((byte) PlatformType.E.getCode());
//                orderHeaderMapper.updateStatusSelective(header);
//            }
//        }
//    }

}
