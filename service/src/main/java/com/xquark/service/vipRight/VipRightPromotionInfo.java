package com.xquark.service.vipRight;

import com.xquark.dal.type.PromotionType;
import com.xquark.service.pricing.base.PromotionInfo;

/**
 *@ClassName VipRightPromotionInfo
 *@Description TODO
 *@Author lxl
 *@Date 2018/11/11 15:14
 *@Version 1.0
 **/
public class VipRightPromotionInfo extends PromotionInfo{

    public VipRightPromotionInfo() {
    }

    public VipRightPromotionInfo(String promotionId, PromotionType promotionType) {
        super(promotionId, promotionType);
    }
}
