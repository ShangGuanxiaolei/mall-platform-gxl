package com.xquark.service.footprint.impl;

import com.xquark.dal.mapper.FootprintMapper;
import com.xquark.dal.model.FootOrder;
import com.xquark.dal.model.FootOrderItem;
import com.xquark.dal.model.User;
import com.xquark.dal.model.VisitorInfo;
import com.xquark.dal.page.PageHelper;
import com.xquark.service.footprint.FootprintService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.parboiled.common.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @ClassName FootPrintServiceImpl
 * @Description TODO
 * @Author lxl
 * @Date 2018/11/14 13:30
 * @Version 1.0
 **/
@Service
public class FootprintServiceImpl implements FootprintService {

    private static final Log logger = LogFactory.getLog(FootprintServiceImpl.class);

    @Autowired
    private FootprintMapper footprintMapper;
    /**
     * 根据当前人和当前月份给存在订单的日期标记红点
     * @param date
     * @param user
     * @return
     */
    @Override
    public List<String> dateMarkedByMonth(Date date, User user) {
        //根据当前登入人查询直接下级顾客（弱关系，排除强关系存在upline的顾客）
        List<String> buyerIds = footprintMapper.selectLowerConsumerByCustomer(String.valueOf(user.getCpId()));
        //1025990
//        List<String> buyerIds = footprintMapper.selectLowerConsumerByCustomer("1025990");

        //查询这些用户在这个月存在订单的日期
        List<String> marketDates = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(buyerIds)) {
            marketDates = footprintMapper.selectOrderTimeByBuyerIds(buyerIds, date,String.valueOf(user.getCpId()));
        }
        return marketDates;
    }

    /**
     * 根据用户id和orderNo查询用户购买的订单list
     */
    @Override
    public PageHelper<FootOrder> selectOrderInfoByCpIdAndOrderNo(Long cpId,Long fromCpId, String paidAt, int page, int pageSize) {
        PageHelper<FootOrder> footOrder = new PageHelper<>();
//        int sum = footprintMapper.selectOrderNumByCpIdAndOrderNo(cpId,fromCpId, paidAt); // 顾客指定日期订单数
        int sum = footprintMapper.selectOrderNumByCpId(cpId,fromCpId); // 顾客所有订单数
        int totalPage = sum % pageSize == 0 ? sum
                / pageSize : sum / pageSize + 1;
        footOrder.setPageCount(sum);
        footOrder.setTotalPage(totalPage);
        footOrder.setPage(page);
        footOrder.setPageSize(pageSize);

//        List<FootOrder> footOrders = footprintMapper.selectOrderInfoByCpIdAndOrderNo(cpId,fromCpId,paidAt, (page - 1) * pageSize, pageSize);
        List<FootOrder> footOrders = footprintMapper.selectOrderInfoByCpId(cpId,fromCpId,(page - 1) * pageSize, pageSize);
        if (null != footOrder && !"".equals(footOrder)) {
            footOrder.setResult(footOrders);
        }
        return footOrder;
    }

    @Override
    public List<FootOrderItem> selectOrderProductInfoByCpIdAndOrderNo(Long cpId, String orderNo) {
        return footprintMapper.selectOrderProductInfoByCpIdAndOrderNo(cpId, orderNo);
    }

    @Override
    public List<FootOrderItem> selectOrderProductInfoByCpId(Long cpId) {
        return footprintMapper.selectOrderProductInfoByCpId(cpId);
    }


    @Override
    public Map<String, List<VisitorInfo>> visitorList(String cpId, String date, Boolean isToday) {
        Map<String, List<VisitorInfo>> aRetMap = new LinkedHashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date parse = sdf.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(parse);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            if (!isToday || day == 1) { // 不是当天或是每月1号，只查询指定一天的顾客数据
                date = sdf.format(calendar.getTime());
                List<String> consumers = footprintMapper.selectConsumersByDateAndCpid(cpId, date);
                if (consumers.isEmpty()) {
                    aRetMap.put(date, new ArrayList<>());
                    return aRetMap;
                }
                List<String> newPeoples = new ArrayList<>();
                //是否是新人
                for (String consumercpid : consumers) {
                    Long orderNumbers = footprintMapper.countOrderNumberByCpid(consumercpid);
                    if (orderNumbers.intValue() == 0) {
                        newPeoples.add(consumercpid);
                    }
                }
                if (newPeoples.size() == 0) {
                    aRetMap.put(date, new ArrayList<>());
                    return aRetMap;
                }
                //是否是白人
                List<String> downCpids = new ArrayList<>();
                for (String newPeople : newPeoples) {
                    String notIdentity = footprintMapper.selectCareerlevel(newPeople);
                    if (StringUtils.isNotEmpty(notIdentity)) {
                        downCpids.add(notIdentity);
                    }
                }
                if (downCpids.isEmpty()) {
                    aRetMap.put(date, new ArrayList<>());
                    return aRetMap;
                }
                List<VisitorInfo> visitorInfoList = new ArrayList<>();
                getVisitorInfoByCpid(date, downCpids, cpId, visitorInfoList);
                aRetMap.put(date, visitorInfoList);
                return aRetMap;
            } else { // 不是每月1号
                int count = 0;
                for (int i = 1; i <= day; i++) {
                    if (i == 1) {
                        calendar.add(Calendar.DAY_OF_MONTH, 0);
                    } else {
                        calendar.add(Calendar.DAY_OF_MONTH, -1);
                    }
                    date = sdf.format(calendar.getTime());
                    List<String> consumers = footprintMapper.selectConsumersByDateAndCpid(cpId, date);
                    if (!consumers.isEmpty()) {
                        List<String> newPeoples = new ArrayList<>();
                        //是否是新人
                        for (String consumercpid : consumers) {
                            Long orderNumbers = footprintMapper.countOrderNumberByCpid(consumercpid);
                            if (orderNumbers.intValue() == 0) {
                                newPeoples.add(consumercpid);
                            }
                        }
                        if (newPeoples.size() > 0) {
                            //是否是白人
                            List<String> downCpids = new ArrayList<>();
                            for (String newPeople : newPeoples) {
                                String notIdentity = footprintMapper.selectCareerlevel(newPeople);
                                if (StringUtils.isNotEmpty(notIdentity)) {
                                    downCpids.add(notIdentity);
                                }
                            }
                            if (!downCpids.isEmpty()) {
                                List<VisitorInfo> visitorInfoList = new ArrayList<>();
                                getVisitorInfoByCpid(date, downCpids, cpId, visitorInfoList);
                                count += visitorInfoList.size();
                                aRetMap.put(date, visitorInfoList);
                            } else {
                                aRetMap.put(date, new ArrayList<>());
                            }
                        } else {
                            aRetMap.put(date, new ArrayList<>());
                        }
                    } else {
                        aRetMap.put(date, new ArrayList<>());
                    }
                    if (count > 10) {
                        return aRetMap;
                    }
                }
            }
        } catch (ParseException e) {
            logger.debug("日期解析异常" + e.getMessage());
        }
        return aRetMap;
    }

    // 封装游客列表信息
    private void getVisitorInfoByCpid(String date, List<String> downCpids, String currentCpid, List<VisitorInfo> visitorInfoList) {
        for (String downCpid : downCpids) {
            String receiverName = footprintMapper.selectReceiverName(downCpid);
            VisitorInfo visitorInfo = footprintMapper.selectVisitorInfoByCpid(downCpid);
            if (null != visitorInfo) {
                Long orderNumber = footprintMapper.countOrderNumberByCpid(currentCpid);
                if (orderNumber.intValue() >= 1) {
                    visitorInfo.setPresentDPoint(true);
                } else {
                    visitorInfo.setPresentDPoint(false);
                }
                visitorInfo.setReceivername(receiverName); //获取常用收件人
                visitorInfo.setCorrelationTime(date);
                visitorInfo.setCpId(Long.valueOf(downCpid)); //当前游客的cpid,用于赠送德分
                visitorInfoList.add(visitorInfo);
            }
        }
    }

    @Override
    public List<String> visitorDateMarked(Date month, User user) {
        String currentUserCpid = user.getCpId().toString();
        //查询这些用户在这个月存在订单的日期
        List<String> consumerCpids = footprintMapper.selectAllConsumerCpid(currentUserCpid, month);
        if (consumerCpids.isEmpty()) {
            return new ArrayList<>();
        }
        List<String> newPeoples = new ArrayList<>();
        //是否是新人
        for (String consumercpid : consumerCpids) {
            Long orderNumbers = footprintMapper.countOrderNumberByCpid(consumercpid);
            if (orderNumbers.intValue() == 0) {
                newPeoples.add(consumercpid);
            }
        }
        if (newPeoples.size() == 0) {
            return new ArrayList<>();
        }
        //是否是白人
        List<String> downCpids = new ArrayList<>();
        for (String newPeople : newPeoples) {
            String notIdentity = footprintMapper.selectCareerlevel(newPeople);
            if (StringUtils.isNotEmpty(notIdentity)) {
                downCpids.add(notIdentity);
            }
        }
        if (downCpids.isEmpty()) {
            return new ArrayList<>();
        }
        return footprintMapper.selectDateByCpid(currentUserCpid, downCpids);
    }

    /**
     * 根据cpid查询顾客详情
     *
     * @param cpId
     * @return
     */
    @Override
    public VisitorInfo queryConsumerDetail(String cpId, Boolean hasServerCharge,User currentIUser) {
//        currentCpid = "1022462";
        //先查询顾客的基本信息
        VisitorInfo visitorInfo = footprintMapper.selectConsumerDetail(cpId,String.valueOf(currentIUser.getCpId()));
        //根据当前登入人的身份 去计算下级贡献的收益
        BigDecimal totalIncome = footprintMapper.selectTotalIncome(cpId, hasServerCharge,String.valueOf(currentIUser.getCpId()));
        visitorInfo.setTotalIncome(totalIncome);
        // 是否显示收益
        String notIdentity = footprintMapper.selectCareerlevel(currentIUser.getCpId().toString());
        if (StringUtils.isNotEmpty(notIdentity)) {
            visitorInfo.setShowIncome(false);
        } else {
            visitorInfo.setShowIncome(true);
        }

        return visitorInfo;
    }

    /*
     * 功能描述: 根据分享人的cpid和查询的日期，获取当前分享人的所有弱关系型顾客信息列表
     * @author Kwonghom
     * @date 2018/11/14
     * @param [cpId, date]
     * @return java.util.List<com.xquark.dal.model.VisitorInfo>
     */
    @Override
    public Map<String, List<VisitorInfo>> customerList(String cpId, String date, Boolean isToday) {
        Map<String, List<VisitorInfo>> aRetMap = new LinkedHashMap<>();
        List<VisitorInfo> visitorInfosByOneDay;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date parse = sdf.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(parse);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            if (!isToday || day == 1) { // 不是当天或是每月1号，只查询指定一天的顾客数据
                // 1025990 2018-10-02
                date = sdf.format(calendar.getTime());
                visitorInfosByOneDay = footprintMapper.selectCustomerList(Integer.parseInt(cpId), date);
                int selectOrderAllCount=footprintMapper.selectOrderAllCount(Integer.parseInt(cpId),date);
                if(visitorInfosByOneDay.size()<1 && selectOrderAllCount>0 ){
                    visitorInfosByOneDay=footprintMapper.selectOrderCountIfNUll(Integer.parseInt(cpId),date);
                }
                getCustomerInfoByCpid(cpId, date, visitorInfosByOneDay);
                aRetMap.put(date, visitorInfosByOneDay);
                return aRetMap;
            } else { // 不是每月1号
                int count = 0;
                for (int i = 1; i <= day; i++) {
                    if (i == 1) {
                        calendar.add(Calendar.DAY_OF_MONTH, 0);
                    } else {
                        calendar.add(Calendar.DAY_OF_MONTH, -1);
                    }
                    date = sdf.format(calendar.getTime());
                    visitorInfosByOneDay = footprintMapper.selectCustomerList(Integer.parseInt(cpId), date);
                    int selectOrderAllCount=footprintMapper.selectOrderAllCount(Integer.parseInt(cpId),date);
                    if(visitorInfosByOneDay.size()<1 && selectOrderAllCount>0 ){
                        visitorInfosByOneDay=footprintMapper.selectOrderCountIfNUll(Integer.parseInt(cpId),date);
                    }

                    getCustomerInfoByCpid(cpId, date, visitorInfosByOneDay);
                    count += visitorInfosByOneDay.size();
                    aRetMap.put(date, visitorInfosByOneDay);
                    if (count > 10) {
                        return aRetMap;
                    }
                }
            }
        } catch (ParseException e) {
            logger.debug("日期解析异常" + e.getMessage());
        }
        return aRetMap;
    }

    // 封装顾客列表信息
    private void getCustomerInfoByCpid(String currentCpid, String date, List<VisitorInfo> visitorInfosByOneDay) {
        for (VisitorInfo visitorInfo : visitorInfosByOneDay) {
            //判断该用户是否可以赠送德分
            Long orderNumber = footprintMapper.countOrderNumberByCpid(currentCpid);
            if (orderNumber.intValue() >= 1) {
                visitorInfo.setPresentDPoint(true);
            } else {
                visitorInfo.setPresentDPoint(false);
            }
            visitorInfo.setOrderTime(date);
        }
    }
}
