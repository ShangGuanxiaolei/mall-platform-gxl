package com.xquark.service.footprint;

import com.xquark.dal.model.FootOrder;
import com.xquark.dal.model.FootOrderItem;
import com.xquark.dal.model.User;
import com.xquark.dal.model.VisitorInfo;
import com.xquark.dal.page.PageHelper;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface FootprintService {

    // 根据日期和用户id查询顾客列表
    Map<String, List<VisitorInfo>> customerList(String cpId, String date, Boolean isToday);

    // 根据日期和用户id查询游客列表
    Map<String, List<VisitorInfo>> visitorList(String cpId, String date, Boolean isToday);

    /**
     * 根据用户id查询用户购买的订单list
     */
    PageHelper<FootOrder> selectOrderInfoByCpIdAndOrderNo(Long cpId,Long fromCpId,String paidAt,int page,int pageSize);
    /**
     * 根据用户id和orderNo查询用户购买的订单的商品信息
     */
    List<FootOrderItem> selectOrderProductInfoByCpIdAndOrderNo(Long cpId,String orderNo);

    /**
     * 顾客订单列表, 当前顾客的所有订单项List
     * @param cpId
     * @return
     */
    List<FootOrderItem> selectOrderProductInfoByCpId(Long cpId);

    List<String> dateMarkedByMonth(Date date, User currentIUser);

    List<String> visitorDateMarked(Date date, User currentIUser);

    VisitorInfo queryConsumerDetail(String cpId,Boolean hasServerCharge,User currentIUser);
}
