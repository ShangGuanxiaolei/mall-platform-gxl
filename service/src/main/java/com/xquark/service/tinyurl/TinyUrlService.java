package com.xquark.service.tinyurl;


import com.google.inject.Provider;

public interface TinyUrlService {

  String insert(String url);

  String insertUnique(String url);

  String insert(String url, Provider<String> keyStratage);

  String findUrlByKey(String key);

  String findKeyByUrl(String url);

}
