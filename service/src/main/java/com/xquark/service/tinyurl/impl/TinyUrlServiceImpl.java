package com.xquark.service.tinyurl.impl;

import com.google.inject.Provider;
import com.xquark.utils.SnowflakeIdWorker;
import java.security.MessageDigest;
import java.util.Random;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.TinyUrlMapper;
import com.xquark.dal.model.TinyUrl;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.tinyurl.TinyUrlService;

@Service("tinyUrlService")
public class TinyUrlServiceImpl extends BaseServiceImpl implements
    TinyUrlService, InitializingBean {

  @Autowired
  private TinyUrlMapper tinyurlMapper;

  @Autowired
  private SnowflakeIdWorker snowflakeIdWorker;

  private final static Provider<String> RANDOM_STRING_STRATEGY
      = new Provider<String>() {
    @Override
    public String get() {
      return getRandomString(6);
    }
  };

  private Provider<String> snowflakeStrategy;

  @Override
  public String insert(String url) {
    return insert(url, RANDOM_STRING_STRATEGY);
  }

  @Override
  public String insertUnique(String url) {
    return insert(url, snowflakeStrategy);
  }

  @Override
  public synchronized String insert(String url, Provider<String> keyStrategy) {
    String retKey = findKeyByUrl(url);
    if (retKey == null) {
      TinyUrl tinyUrl = new TinyUrl();
      tinyUrl.setMd5(getMd5(url));
      tinyUrl.setUrl(url);
      int retCount = 0;
      int count = 0;
      /**
       * 通过数据库保证唯一性，如果出错重新生成一次，最多重试三次
       */
      while (retCount < 1) {
        count++;
        tinyUrl.setKey(keyStrategy.get());
        // tinyUrl.setKey("SNSpSJ");
        try {
          retCount = tinyurlMapper.insert(tinyUrl);
        } catch (DuplicateKeyException e) {
          log.info("短链接生成串重复：" + tinyUrl.getKey());
          retCount = 0;
        }
        if (count >= 3) {
          tinyUrl.setKey(null);
          break;
        }
      }
      retKey = tinyUrl.getKey();
    }

    return retKey;
  }

  @Override
  public String findUrlByKey(String key) {
    TinyUrl tinyurl = tinyurlMapper.selectByPrimaryKey(key);
    if (null == tinyurl) {
      return null;
    }
    return tinyurl.getUrl();
  }

  @Override
  public String findKeyByUrl(String url) {
    String urlMd5 = getMd5(url);
    TinyUrl tinyurl = tinyurlMapper.selectByUrl(urlMd5);
    if (null == tinyurl) {
      return null;
    }
    return tinyurl.getKey();
  }

  /**
   * 生成随机的N位字符串
   */
  public static String getRandomString(int length) { // length表示生成字符串的长度
    String base = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    Random random = new Random();
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < length; i++) {
      int number = random.nextInt(base.length());
      sb.append(base.charAt(number));
    }
    return sb.toString();
  }

  public String getUniqueString() {
    return String.valueOf(snowflakeIdWorker.nextId());
  }

  /**
   * 生成url的md5值
   */
  public String getMd5(String url) {
    String encryptUrl = "XQuark" + url;
    String retMd5 = null;
    try {
      // 对传入网址进行 MD5加密
      MessageDigest messageDigest = MessageDigest.getInstance("MD5");
      messageDigest.update(encryptUrl.getBytes());
      byte[] bytes = messageDigest.digest();
      StringBuilder ret = new StringBuilder(bytes.length << 1);
      for (int i = 0; i < bytes.length; i++) {
        ret.append(Character.forDigit((bytes[i] >> 4) & 0xf, 16));
        ret.append(Character.forDigit(bytes[i] & 0xf, 16));
      }
      retMd5 = ret.toString();
    } catch (Exception e) {
      log.info("生成md5出错：" + url);
    }
    return retMd5;
  }

  @Override
  public void afterPropertiesSet() {
      snowflakeStrategy = new Provider<String>() {
        @Override
        public String get() {
          return String.valueOf(snowflakeIdWorker.nextId());
        }
      };
  }
}
