package com.xquark.service.distribution;

import com.xquark.dal.model.DistributionConfig;
import com.xquark.service.BaseService;

public interface DistributionConfigService extends BaseService {

  DistributionConfig selectByUser(String masterId, String promoter);


}
