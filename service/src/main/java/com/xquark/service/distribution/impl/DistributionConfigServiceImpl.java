package com.xquark.service.distribution.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.DistributionConfigMapper;
import com.xquark.dal.model.DistributionConfig;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.distribution.DistributionConfigService;

@Service("distributionConfigService")
public class DistributionConfigServiceImpl extends BaseServiceImpl implements
    DistributionConfigService {

  @Autowired
  private DistributionConfigMapper distributionConfigMapper;

  @Override
  public DistributionConfig selectByUser(String masterId, String promoter) {
    return distributionConfigMapper.selectByUser(masterId, promoter);
  }
}
