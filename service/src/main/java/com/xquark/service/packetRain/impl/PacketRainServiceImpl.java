package com.xquark.service.packetRain.impl;

import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.RainLotteryConstrants;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.vo.PromotionLotteryVO;
import com.xquark.dal.vo.PromotionPacketRainVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.grandSale.GrandSalePromotionService;
import com.xquark.service.packetRain.PacketRainService;
import com.xquark.service.promotion.PromotionWhiteService;
import com.xquark.utils.UserAgentUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 红包雨服务层实现类
 */
@Service
public class PacketRainServiceImpl implements PacketRainService {

    private static Logger logger = LoggerFactory.getLogger(PacketRainServiceImpl.class);

    /**
     * 首页banner图地址
     */
//  private static final String IMG = "http://images.handeson.com/Fs-ByfWmIlGqYS_XnfeXTTyPauwM?imageView2/2/w/640/q/100/@w/$w$@/@h/$h$@";

    /**
     * UNION_ID为null时的默认值
     */
    private static final String UNION_ID_NULL = "union_id_null";


    @Autowired
    private PromotionListMapper promotionListMapper;

    @Autowired
    private PromotionPacketRainTimeMapper promotionPacketRainTimeMapper;

    @Autowired
    private PromotionLotteryMapper promotionLotteryMapper;

    @Autowired
    private PromotionLotteryProbabilityMapper promotionLotteryProbabilityMapper;

    @Autowired
    private PromotionPacketRainMapper promotionPacketRainMapper;

    @Autowired
    private PromotionWhiteService promotionWhiteService;

    @Autowired
    private CustomerWechartUnionMapper customerWechartUnionMapper;

    @Autowired
    private GrandSalePromotionService grandSalePromotionService;

    @Override
    public PromotionLottery getCurrentPromotionLottery() {
        return promotionLotteryMapper.selectTodayLotteryActivityLimit();
    }

    @Override
    public Optional<PromotionLotteryVO> getCurrentPromotionLotteryVO() {
        return Optional.ofNullable(promotionLotteryMapper.selectTodayLotteryActivityLimitVO());
    }

    @Override
    public List<PromotionLotteryProbability> getPromotionLotteryProbability(String id) {

        return promotionLotteryProbabilityMapper.listByPromotionLotteryId(id, null);
    }

    @Override
    public boolean hasActivityRain() {
        return promotionPacketRainMapper.selectIsInPromotion(0);
    }

    @Override
    public boolean hasOnGoingActivityRain(String promotionId) {
        return promotionPacketRainMapper.selectIsInTimePromotion(0,promotionId);
    }

    @Override
    public boolean hasActivityLottery() {
        return promotionPacketRainMapper.selectIsInLottery();
    }

    @Override
    public boolean hasOnGoingActivityLottery(String promotionId) {

        return promotionPacketRainMapper.selectIsInTimeLottery(promotionId);
    }


    @Override
    public PacketRain findPacketRain(Long cpId) {
        final PromotionPacketRainVO todayPromotion = promotionPacketRainMapper.selectTodayActivityConfigLimitVO();
        if (todayPromotion == null) {
            // 活动已结束
            return PacketRain.state(2);
        }
        final String promotionId = todayPromotion.getId();

        // 当前时间段的活动
        final Optional<PromotionPacketRainTime> rainTimeOptional =
                this.loadCurrentTimePromotion(todayPromotion.getId());

        final String promotionListId = todayPromotion.getPromotionListId();
        final PromotionList promotionList = promotionListMapper.selectByPrimaryKey(promotionListId);
        if (promotionList == null) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, "活动信息不存在");
        }

        PacketRain packetRain = new PacketRain();
        BeanUtils.copyProperties(todayPromotion.getDetail(), packetRain);
        packetRain.setState(0);
        packetRain.setPromotionListId(todayPromotion.getPromotionListId());

        List<PromotionPacketRainTime> rainTimes =
                promotionPacketRainTimeMapper.listByPackRainId(promotionId);
        packetRain.setRainTimes(rainTimes);

        if (rainTimeOptional.isPresent()) {

            PromotionPacketRainTime rainTime = rainTimeOptional.get();
            packetRain.setTime(rainTime.getBetween().toHours());
            packetRain.setTriggerStartTime(promotionList.getTriggerStartTime());
            packetRain.setTriggerEndTime(promotionList.getTriggerEndTime());
            packetRain.setEffectiveTime(rainTime.getBetween().toMillis());
            packetRain.setThisEndTime(rainTime.getEndTime());

            PromotionPacketRainTime nextPromotionTime = promotionPacketRainTimeMapper
                    .selectNextAfter(promotionListId, rainTime.getEndTime());

            if (nextPromotionTime != null) {
                packetRain.setNextStartTime(nextPromotionTime.getStartTime());
            }
            //设置当前活动的状态（1 -> 在活动时间， 0 ->不在活动时间）
            packetRain.setState(1);

            //获取是否用户参与过当前时段红包雨活动
            packetRain.setJoinedPacketRain(isJoined(rainTime.getId(), cpId));

        } else {
            // 当前不在时间范围内,
            final PromotionPacketRainTime closestTimePromotion =
                    promotionPacketRainTimeMapper.selectClosestTimePromotion(promotionListId);
            if (closestTimePromotion == null) {
                // 确认已经没有活动
                packetRain.setState(2);
            } else {
                // 还有活动, 设置为不在活动时间
                packetRain.setState(0);
                packetRain.setNextStartTime(closestTimePromotion.getStartTime());
                packetRain.setTriggerStartTime(promotionList.getTriggerStartTime());
                packetRain.setTriggerEndTime(promotionList.getTriggerEndTime());
            }
        }
        return packetRain;
    }

    @Override
    public Map<String, Object> whiteCheck(Long cpId, HttpServletRequest request) {
        if (UserAgentUtils.isFromApp(request)) {
            return null;
        }
        Map<String, Object> map = new HashMap<>();
        //白名单校验
        if (!promotionWhiteService.havePacketRule(cpId)) {
            return null;
        }

        if (grandSalePromotionService.isInSaleTime(new Date())) {
            return null;
        }

        List<PromotionList> promotions = promotionListMapper.getTodayPromotion(RainLotteryConstrants.PACKET_RAIN);
        if (CollectionUtils.isNotEmpty(promotions)) {
            PromotionList list = promotions.get(0);
            map.put("imgUrl", list.getBanner());
        }
        return map;
    }

    /**
     * 将key存入redis
     *
     * @param cpId 用户身份id
     * @deprecated 由Go端统一校验
     */
    @Deprecated
    @Override
    public void savePacketRain(Long cpId) {
        RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
        PromotionPacketRain todayPromotion = promotionPacketRainMapper.selectTodayActivityConfigLimit();
        if (todayPromotion != null) {
            Optional<PromotionPacketRainTime> promotionPacketRainTime = this.loadCurrentTimePromotion(todayPromotion.getId());
            if (!promotionPacketRainTime.isPresent()) {
                throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "不在活动时间内，请耐心等待");
            }
            final PromotionPacketRainTime curr = promotionPacketRainTime.get();
            final Date endTime = curr.getEndTime();
            boolean result = isJoined(curr.getId(), cpId);
//            if (!result) {
//                // 将值放到redis中, 过期时间按毫秒计算
//                final long nowToEnd = Duration.between(new Date().toInstant(), endTime.toInstant()).toMillis();
//                savePacketRainJoin(cpId, todayPromotion.getPromotionListId(), redisUtils, nowToEnd);
//            } else {
//                throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "您已参与过当前时间段的活动");
//            }
        } else {
            logger.error("当前活动已结束");
        }
    }

    @Override
    public Optional<PromotionPacketRainTime> loadCurrentTimePromotion(String promotionId) {
        return Optional.ofNullable(promotionPacketRainTimeMapper.selectCurrentTimePromotion(promotionId));
    }

    @Override
    public Optional<PromotionPacketRainTime> loadClosestTimePromotion(String promotionId) {
        return Optional.ofNullable(promotionPacketRainTimeMapper.selectClosestTimePromotion(promotionId));
    }

    /**
     * 检查是否可以参与红包雨活动
     *
     * @param rainTimeId
     * @param cpId       用户身份id
     * @return 是否参与过活动
     */
    private boolean isJoined(String rainTimeId, Long cpId) {
        final String key = RainLotteryConstrants.USER_GENERATE_LIMIT_KEY_FUNC
                .apply(rainTimeId, cpId);
        final RedisUtils<Boolean> boolRedis = RedisUtilFactory.getInstance(Boolean.class);
        Boolean ret = boolRedis.get(key);
        return ret != null && ret;
    }

    /**
     * 保存参与过红包雨活动的key
     *
     * @param cpId            用户身份id
     * @param promotionListId 活动id
     * @param expire
     */
    private void savePacketRainJoin(Long cpId, String promotionListId, RedisUtils<Integer> redisUtils, long expire) {
        CustomerWechatUnion customerWechatUnion = customerWechartUnionMapper.selectRandomOne(cpId);

        String packetRainKey = String.join(":", promotionListId, String.valueOf(cpId),
                Optional.ofNullable(customerWechatUnion.getUnionId()).orElse(UNION_ID_NULL));

        if (!redisUtils.hasKey(packetRainKey)) {
            redisUtils.set(packetRainKey, 1);
            //设置过期时间一小时
            redisUtils.expire(packetRainKey, expire, TimeUnit.MILLISECONDS);
        } else {
            redisUtils.increment(packetRainKey, 1);
        }
    }

}
