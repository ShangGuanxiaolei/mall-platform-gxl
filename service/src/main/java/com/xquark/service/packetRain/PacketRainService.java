package com.xquark.service.packetRain;

import com.xquark.dal.model.PacketRain;
import com.xquark.dal.model.PromotionLottery;
import com.xquark.dal.model.PromotionLotteryProbability;
import com.xquark.dal.model.PromotionPacketRainTime;
import com.xquark.dal.vo.PromotionLotteryVO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 红包雨服务层接口
 */
public interface PacketRainService {

    PromotionLottery getCurrentPromotionLottery();

    Optional<PromotionLotteryVO> getCurrentPromotionLotteryVO();

    List<PromotionLotteryProbability> getPromotionLotteryProbability(String id);

    boolean hasActivityRain();

    boolean hasActivityLottery();

    boolean hasOnGoingActivityRain(String promotionId);

    boolean hasOnGoingActivityLottery(String promotionId);

    PacketRain findPacketRain(Long cpId);

    Map<String, Object> whiteCheck(Long cpId, HttpServletRequest request);

    void savePacketRain(Long cpId);

    Optional<PromotionPacketRainTime> loadCurrentTimePromotion(String promotionId);

    Optional<PromotionPacketRainTime> loadClosestTimePromotion(String promotionId);
}
