package com.xquark.service.packetRain;

import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.mapper.PromotionPacketRainMapper;
import com.xquark.dal.mapper.PromotionPacketRainTimeMapper;
import com.xquark.dal.model.PromotionPacketRain;
import com.xquark.dal.model.PromotionPacketRainTime;
import com.xquark.service.base.RainLotteryJobService;
import com.xquark.service.lottery.PromotionTime;
import com.xquark.utils.DateUtils;
import io.vavr.control.Either;
import io.vavr.control.Try;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.xquark.dal.consts.RainLotteryConstrants.*;

/**
 * @author wangxinhua
 * @date 2019-04-29
 * @since 1.0
 */
@Service
public class PacketRainJobService extends RainLotteryJobService {

    private final PromotionPacketRainMapper packetRainMapper;

    private final PromotionPacketRainTimeMapper packetRainTimeMapper;

    private final PacketRainService packetRainService;

    @Autowired
    public PacketRainJobService(PromotionPacketRainMapper packetRainMapper, PromotionPacketRainTimeMapper packetRainTimeMapper, PacketRainService packetRainService) {
        this.packetRainMapper = packetRainMapper;
        this.packetRainTimeMapper = packetRainTimeMapper;
        this.packetRainService = packetRainService;
    }

    public Either<String, Boolean> clearUserRain(Long cpId) {
        final PromotionPacketRain promotionPacketRain = packetRainMapper.selectTodayActivityConfigLimitVO();
        if (promotionPacketRain == null) {
            return Either.left("当天红包雨活动不存在");
        }
        final List<PromotionPacketRainTime> promotionPacketRainTimes =
                packetRainTimeMapper.listByPackRainId(promotionPacketRain.getId());
        if (CollectionUtils.isEmpty(promotionPacketRainTimes)) {
            return Either.left("红包雨时间");
        }

        final RedisUtils<String> redisUtil = RedisUtilFactory.getInstance(String.class);

        promotionPacketRainTimes.stream()
                .map(PromotionPacketRainTime::getId)
                .map(id -> USER_GENERATE_LIMIT_KEY_FUNC.apply(id, cpId))
                .peek(key -> log.info("删除redisKey -> {}", key))
                .forEach(redisUtil::del);

        return Either.right(Boolean.TRUE);
    }

    /**
     * 初始化红包雨redis活动
     *
     * @param force 是否强制刷新
     */
    public Either<String, Boolean> init(boolean force) {
        if (isNotActivity(PACKET_RAIN)) {
            return Either.left("不在活动时间范围内");
        }

        final PromotionPacketRain promotionPacketRain = packetRainMapper.selectTodayActivityConfigLimitVO();
        if (promotionPacketRain == null) {
            return Either.left("当天红包雨活动不存在");
        }
        final RedisUtils<List> redisList = RedisUtilFactory.getInstance(List.class);
        final RedisUtils<Integer> redisInt = RedisUtilFactory.getInstance(Integer.class);

        final List<PromotionPacketRainTime> promotionPacketRainTimes =
                packetRainTimeMapper.listByPackRainId(promotionPacketRain.getId());

        if (CollectionUtils.isEmpty(promotionPacketRainTimes)) {
            return Either.left("当天红包雨活动时间段未配置");
        }
        final List<PromotionTime> timeList = promotionPacketRainTimes.stream()
                .map(ppr -> new PromotionTime(ppr.getId(), ppr.getStartTime(),
                        ppr.getEndTime()))
                .collect(Collectors.toList());

        // FIXME 用redis原生的List数据结构
        @SuppressWarnings("unchecked") final List<LinkedHashMap> preList = redisList.get(FLEXIBLE_PACKET_TIME_RULE);
        final List<PromotionTime> existsList = Try.of(() -> preList.stream().map(o -> new PromotionTime((String) o.get("id"),
                new Date((Long) o.get("startTime")), new Date((Long) o.get("endTime"))))
                .collect(Collectors.toList()))
                .getOrElse(Collections.emptyList());

        final Date endOfToday = DateUtils.getEndOfDay(promotionPacketRain.getPromotionDate());
        final long expireAt = Duration.between(new Date().toInstant(), endOfToday.toInstant())
                .toMillis();

        //有几场活动
        final int times = promotionPacketRainTimes.size();

        final String promotionId = promotionPacketRain.getId();
        final Optional<PromotionPacketRainTime> currentPromotionPacketRainTime =
                packetRainService.loadCurrentTimePromotion(promotionId);

        // ==== 统一刷新redis ====
        // ======================

        if (!CollectionUtils.isEqualCollection(timeList, existsList)) {
            // TODO 判断集合是否相等
            redisList.set(FLEXIBLE_PACKET_TIME_RULE, timeList, expireAt, TimeUnit.MILLISECONDS);
            //活动时间
            log.info("设置活动场次-> {}", timeList);
        }

        final Integer preNormalLimit = redisInt.get(IDENTIRY_LIST_RS);
        final int normalLimit = promotionPacketRain.getNormalLimit() / times;
        if (preNormalLimit == null || preNormalLimit != normalLimit || force) {
            log.info("设置普通身份上限 -> {}", normalLimit);
            redisInt.set(IDENTIRY_LIST_RS, normalLimit, expireAt, TimeUnit.MILLISECONDS);
        }

        final Integer preVipLimit = redisInt.get(IDENTIRY_LIST_NOT_RS);
        final int vipLimit = promotionPacketRain.getVipLimit() / times;
        if (preVipLimit == null || preVipLimit != vipLimit || force) {
            log.info("设置VIP身份上限 -> {}", normalLimit);
            redisInt.set(IDENTIRY_LIST_NOT_RS, vipLimit, expireAt, TimeUnit.MILLISECONDS);
        }

        // TODO 活动刷新时间需要控制
        //不在活动时间，但是今天有活动的时候可以配置
        if ((!currentPromotionPacketRainTime.isPresent() && CollectionUtils.isNotEmpty(promotionPacketRainTimes))) {
            RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
            final Integer redisTotal = redisUtils.get(REDIS_ATOMIC_NUMBER);
            //每场的总德分
            final Integer nowTotal = promotionPacketRain.getPointTotal() / promotionPacketRainTimes.size();
            if (!Objects.equals(redisTotal, nowTotal) || force) {
                redisUtils.set(REDIS_ATOMIC_NUMBER, nowTotal);
                log.info("设置活动每场的活动总德分 -> {}", nowTotal);
            }
            // 过期时间只设置一次
            redisUtils.expire(REDIS_ATOMIC_NUMBER, expireAt, TimeUnit.MILLISECONDS);
        }

        log.info("=============今日红包雨活动配置完成==========");
        return Either.right(Boolean.TRUE);
    }

}
