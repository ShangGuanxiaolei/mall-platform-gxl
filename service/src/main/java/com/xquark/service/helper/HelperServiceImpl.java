package com.xquark.service.helper;

import com.xquark.dal.mapper.HelperMapper;
import com.xquark.dal.model.Helper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by wangxinhua on 17-12-7. DESC:
 */
@Service
public class HelperServiceImpl implements HelperService {

  private HelperMapper helperMapper;

  @Autowired
  public HelperServiceImpl(HelperMapper helperMapper) {
    this.helperMapper = helperMapper;
  }

  @Override
  public boolean save(Helper helper) {
    return helperMapper.insert(helper) > 0;
  }

  @Override
  public boolean delete(String id) {
    return helperMapper.deleteByPrimaryKey(id) > 0;
  }

  @Override
  public boolean update(Helper helper) {
    return helperMapper.updateByPrimaryKeySelective(helper) > 0;
  }

  @Override
  public Helper load(String id) {
    return helperMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<Helper> listHelper(Pageable pageable) {
    return helperMapper.list(pageable);
  }

  @Override
  public List<Helper> listHelperWithOutBLob(Pageable pageable) {
    return helperMapper.listWithOutBlob(pageable);
  }

  @Override
  public Long countHelper() {
    return helperMapper.count();
  }

}
