package com.xquark.service.helper;

import com.xquark.dal.model.Helper;
import java.util.List;
import org.springframework.data.domain.Pageable;

/**
 * Created by wangxinhua on 17-12-7. DESC: 客服数据
 */
public interface HelperService {

  /**
   * 保存客服信息
   *
   * @param helper bean
   * @return true or false
   */
  public boolean save(Helper helper);

  /**
   * 根据id删除一条jillion
   *
   * @param id helperId
   * @return true or false
   */
  public boolean delete(String id);

  /**
   * 更新客服信息
   *
   * @param helper bean
   * @return true or false
   */
  public boolean update(Helper helper);


  /**
   * 根据id查找
   *
   * @param id helperId
   * @return {@link Helper} 实例
   */
  public Helper load(String id);

  /**
   * 列表展示所有帮助问答信息
   *
   * @param pageable 分页
   * @return list of {@link Helper} or empty list
   */
  public List<Helper> listHelper(Pageable pageable);

  /**
   * 列表展示所有帮助问答信息, 不包含内容
   *
   * @param pageable 分页
   * @return list of {@link Helper} or empty list
   */
  public List<Helper> listHelperWithOutBLob(Pageable pageable);

  /**
   * 查询总条数
   *
   * @return 总条数
   */
  public Long countHelper();

}
