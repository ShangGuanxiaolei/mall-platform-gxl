package com.xquark.service.rules.impl;

import com.xquark.dal.mapper.FreshmanRuleMapper;
import com.xquark.dal.model.FreshmanRule;
import com.xquark.service.rules.FreshmanRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FreshmanRuleServiceImpl implements FreshmanRuleService {

    @Autowired
    private FreshmanRuleMapper freshmanRuleMapper;

    @Override
    public FreshmanRule getFreshmanRule(String name) {
        return freshmanRuleMapper.selectFreshmanRuleByName(name);
    }
}
