package com.xquark.service.senmiao.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.mapper.OrderItemMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.Zone;
import com.xquark.dal.type.SendErpDest;
import com.xquark.dal.type.SendErpStatus;
import com.xquark.dal.type.SkuCodeResourcesType;
import com.xquark.dal.type.SkuType;
import com.xquark.dal.vo.OrderVO;
import com.xquark.service.order.OrderService;
import com.xquark.service.senmiao.SenMiaoErpService;
import com.xquark.service.zone.ZoneService;
import com.xquark.utils.MD5Util;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("senMiaoErpService")
public class SenMiaoErpServiceImpl implements SenMiaoErpService {

  @Autowired
  private OrderService orderService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private SkuMapper skuMapper;

  @Autowired
  private OrderItemMapper orderItemMapper;

  /**
   * 旺店通接口参数-------start
   */
  @Value("${wdt.method.newOrder}")
  private String newOrder;

  @Value("${wdt.sellerId}")
  private String sellerId;

  @Value("${wdt.interfaceId}")
  private String interfaceId;

  @Value("${wdt.key}")
  private String wdtKey;

  @Value("${wdt.url}")
  private String wdtUrl;
  /**
   * 旺店通接口参数-------end
   */

  private Logger log = LoggerFactory.getLogger(getClass());

  @Override
  public boolean sendOrder(Order order) {
    if (checkOrderData(order)) {
      String orderNo = order.getOrderNo();
      OrderVO orderVO = orderService.loadByOrderNo(orderNo);
      Map<OrderItem, Sku> skus = new HashMap<OrderItem, Sku>();
      List<OrderItem> orderItems = orderItemMapper.selectByOrderId(order
          .getId());

      for (OrderItem orderItem : orderItems) {
        Sku sku = skuMapper.selectByPrimaryKey(orderItem.getSkuId());
        if (null == sku) {
          sku = new Sku();
        }
        skus.put(orderItem, sku);
      }
      Date now = new Date();
      log.info("向" + SkuCodeResourcesType.getSkuCodeResourceBySupplierCode("SENMIAO").orElseThrow(RuntimeException::new).getDes()
          + "下发业务单据，orderNO=" + orderNo + ", payNO="
          + order.getPayNo());
      try {
        JSON content = setSenmiaoData(order.getId(), setSenmiaoSkus(order.getId(), skus),
            orderVO, skus.size());
        String output = "";
        if (content != null) {
          output = new String(content.toJSONString()
              .getBytes(), "UTF-8");
        }
        log.info("请求森淼output=" + output);

        String base64 = Base64.encodeBase64String(MD5Util
            .MD5Encode((output + wdtKey), null).getBytes());
        String sign = URLEncoder.encode(base64, "UTF-8");
        senmiaoRequest(order.getId(), sign, output);
      } catch (Exception e1) {
        log.error("请求森淼系统失败，订单号：" + orderNo, e1);
        String errorMessage = "请求森淼系统失败，订单号：" + orderNo;
        orderService
            .updateOrderSendErpStatus(order.getId(), now, SendErpStatus.FAIL, SendErpDest.SENMIAO,
                errorMessage);
        return false;
      }
    }
    return true;

  }

  /**
   * 对接森淼订单，发起订单推送请求
   */
  public void senmiaoRequest(String id, String sign, String content) {
    //String result = "";
    URL url;
    Date now = new Date();
    try {
      url = new URL(wdtUrl);

      URLConnection connection = url.openConnection();
      HttpURLConnection httpconn = (HttpURLConnection) connection;
      ByteArrayOutputStream bout = new ByteArrayOutputStream();
      bout.write(content.getBytes("UTF-8"));
      byte[] b = bout.toByteArray();
      httpconn.setRequestProperty("Content-Length",
          String.valueOf(b.length));
      httpconn.setRequestProperty("Content-Type",
          "application/x-www-form-urlencoded");
      httpconn.setRequestMethod("POST");
      httpconn.setDoInput(true);
      httpconn.setDoOutput(true);
      httpconn.setUseCaches(false);

      httpconn.connect();
      OutputStream out = httpconn.getOutputStream();
      StringBuffer buff = new StringBuffer();
      buff.append("Method=").append(newOrder).append("&SellerID=").append(sellerId)
          .append("&InterfaceID=");
      buff.append(interfaceId).append("&Sign=").append(sign).append("&Content=").append(content);
      log.info("ready to push erp =" + buff.toString());
      String str = buff.toString();
      System.out.println(str);

      out.write(str.getBytes());
      out.flush();
      out.close();

      InputStreamReader isr = new InputStreamReader(
          httpconn.getInputStream());
      BufferedReader in = new BufferedReader(isr);
      String inputLine;

      while (null != (inputLine = in.readLine())) {
        try {
          JSONObject reslutJson = (JSONObject) JSON.parse(inputLine);
          if (reslutJson.get("ResultCode").equals(0)) {
            log.info("森淼订单推送返回结果---成功，ResultCode="
                + reslutJson.get("ResultCode") + "，ResultMsg="
                + reslutJson.get("ResultMsg") + "，ErpOrderCode="
                + reslutJson.get("ErpOrderCode"));
            orderService
                .updateOrderSendErpStatus(id, now, SendErpStatus.SUCCESS, SendErpDest.SENMIAO,
                    "SUCCESS");
          } else {
            log.info("森淼订单推送返回结果---失败，ResultCode="
                + reslutJson.get("ResultCode") + "，ResultMsg="
                + reslutJson.get("ResultMsg") + "，ErpOrderCode="
                + reslutJson.get("ErpOrderCode"));

            String errorMessage = "ResultCode="
                + reslutJson.get("ResultCode") + "，ResultMsg="
                + reslutJson.get("ResultMsg") + "，ErpOrderCode="
                + reslutJson.get("ErpOrderCode");
            String ResultCode = reslutJson.get("ResultCode").toString();
            String ResultMsg = reslutJson.get("ResultMsg").toString();
            if (StringUtils.isNotBlank(ResultCode) && StringUtils.isNotBlank(ResultMsg)) {
              if (ResultCode.equals("11") && ResultMsg.equals("外部单号已经存在")) {
                //刷新引起多次发送erp问题
                orderService
                    .updateOrderSendErpStatus(id, now, SendErpStatus.SUCCESS, SendErpDest.SENMIAO,
                        errorMessage);
              } else {
                orderService
                    .updateOrderSendErpStatus(id, now, SendErpStatus.FAIL, SendErpDest.SENMIAO,
                        errorMessage);
              }
            }


          }
        } catch (Exception e) {
          log.error("森淼返回结果解析失败，result：" + inputLine, e);
          orderService.updateOrderSendErpStatus(id, now, SendErpStatus.NotSend, SendErpDest.SENMIAO,
              "森淼返回结果解析失败，result：" + inputLine);
        }
      }
      in.close();
      httpconn.disconnect();
    } catch (Exception e) {
      log.error("call wdt.ERP service failed!", e);
      String errorMessage = "调用SENMIAO接口失败";
      orderService.updateOrderSendErpStatus(id, now, SendErpStatus.NotSend, SendErpDest.SENMIAO,
          errorMessage);
    }
  }

  /**
   * 设置森淼订单请求数据
   *
   * @return JSOM
   */
  private JSON setSenmiaoData(String id, JSON skuJson, OrderVO order, int itemCount) {
    List<Zone> zones = zoneService.listParents(null != order.getOrderAddress() ?
        order.getOrderAddress().getZoneId() : "");
    String warehouseNO = "";
    String shopName = "";
    Date now = new Date();
    String province = "";
    String city = "";
    String cityArea = "";
    String zipCode = "";
    String details = "";
    if (StringUtils.isNotBlank(order.getWarehouseName())) {
      warehouseNO = order.getWarehouseName();
    }
    if (StringUtils.isNotBlank(order.getShopName())) {
      shopName = order.getShopName();
    }
    // 获取地级市
    if (null != zones && zones.size() > 3) {
      province = zones.get(1).getName();
      city = zones.get(2).getName();
      cityArea = zones.get(3).getName();
      zipCode = zones.get(3).getZipCode();
    }
    // 获取详细地址
    for (Zone zone : zones) {
      details += zone.getName();
    }
    details += null != order.getOrderAddress() ? order.getOrderAddress().getStreet() : "";
    StringBuffer sb = new StringBuffer("{");
    sb.append("OutInFlag:").append(3).append(",IF_OrderCode:'").append(order.getOrderNo())
        .append("',WarehouseNO:'").append(warehouseNO).append("',GoodsTotal:")
        .append(order.getTotalFee())
        .append(",ItemCount:").append(itemCount).append(",OrderPay:").append(order.getPaidFee())
        .append(",LogisticsPay:").append(order.getLogisticsFee()).append(", ShopName:'")
        .append(shopName)
        .append("',BuyerTel:'")
        .append(null != order.getOrderAddress() ? order.getOrderAddress().getPhone() : "")
        .append("',BuyerName:'")
        .append(null != order.getOrderAddress() ? order.getOrderAddress().getConsignee() : "")
        .append("',BuyerPostCode:'").append(zipCode).append("',BuyerProvince:'").append(province)
        .append("',BuyerCity:'").append(city).append("',BuyerDistrict:'").append(cityArea)
        .append("',BuyerAdr:'").append(details)
        .append("',ItemList:").append(null != skuJson ? skuJson.toJSONString() : "");
    sb.append("}");
    try {
      return (JSON) JSON.parse(sb.toString());
    } catch (Exception e) {
      log.error("森淼请求数据转json格式失败，jsonStr：" + sb, e);
      String errorMessage = "森淼请求数据转json格式失败";
      orderService.updateOrderSendErpStatus(id, now, SendErpStatus.NotSend, SendErpDest.SENMIAO,
          errorMessage);
    }
    return null;
  }

  /**
   * 设置森淼Sku串
   */
  private JSON setSenmiaoSkus(String id, Map<OrderItem, Sku> skuMap) {
    StringBuffer sb = new StringBuffer("{Item:[");
    Date now = new Date();
    boolean flag = false;
    for (Map.Entry<OrderItem, Sku> sk : skuMap.entrySet()) {
      if (flag) {
        sb.append(",");
      }
      sb.append("{");
      sb.append("Sku_Code:'").append(sk.getValue().getSkuCode()).append("',Sku_Name:'")
          .append(sk.getKey().getProductName().trim()).append("',Sku_Price:")
          .append(sk.getKey().getMarketPrice())
          .append(",Qty:").append(sk.getKey().getAmount());
      sb.append("}");
      flag = true;
    }
    sb.append("]}");
    try {
      return (JSON) JSON.parse(sb.toString());
    } catch (Exception e) {
      log.error("sku串转json格式失败，jsonStr：" + sb, e);
      String errorMessage = "sku串转json格式失败";
      orderService.updateOrderSendErpStatus(id, now, SendErpStatus.NotSend, SendErpDest.SENMIAO,
          errorMessage);
    }
    return null;
  }

  public boolean checkOrderData(Order order) {
    boolean result = true;
    if (order != null) {
      if (StringUtils.isBlank(order.getShopName())) {
        result = false;
        log.error("erp order check:" + "order id " + order.getId() + "shopName do not exist");
      }
      if (StringUtils.isBlank(order.getWarehouseName())) {
        result = false;
        log.error("erp order check:" + "order id " + order.getId() + "warehouseName do not exist");
      }
    } else {
      log.error("erp order check:order is null");
      result = false;
    }
    return result;
  }

}

	


	


