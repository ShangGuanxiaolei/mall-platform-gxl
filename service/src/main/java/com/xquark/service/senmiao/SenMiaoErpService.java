package com.xquark.service.senmiao;

import com.xquark.dal.model.Order;

public interface SenMiaoErpService {

  public boolean checkOrderData(Order order);

  public boolean sendOrder(Order order);
}
