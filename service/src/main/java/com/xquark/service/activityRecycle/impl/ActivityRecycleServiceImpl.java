package com.xquark.service.activityRecycle.impl;

import com.xquark.dal.mapper.ActivityRecycleMapper;
import com.xquark.dal.model.pintuanTask.ProStockDetail;
import com.xquark.service.activityRecycle.ActivityRecycleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ActivityRecycleServiceImpl implements ActivityRecycleService {
    @Autowired
    private ActivityRecycleMapper activityRecycleMapper;
    @Override
    @Transactional
    public void activitySkuRecycle(String id) {//id为活动编码
        //记录库存变化实体
        //ProStockDetail detail = new ProStockDetail();
       // detail.setPcode(id);
        //查询参加活动的sku编码
        List<String> list = activityRecycleMapper.selectSkuCode(id);
        //查询每一个sku的活动剩余库；
        if(list!=null){
        for (int i = 0; i < list.size(); i++) {
            //记录库存变化实体
            ProStockDetail detail = new ProStockDetail();
            detail.setPcode(id);
            int restNum = activityRecycleMapper.selectSkuRestNum(list.get(i));
            if (restNum > 0) {
                activityRecycleMapper.updateSkuActivityStock(id,list.get(i));
                try{
                    activityRecycleMapper.updateSkuXquarkStock(restNum, list.get(i));
                }catch(Exception e) {
                    e.printStackTrace();
                }
                System.out.println("活动" + id + "释放了" + "sku编码为" + list.get(i) + "的数量" + restNum + "个");
                detail.setSkucode(list.get(i));
                detail.setNum(restNum);
                detail.setType(1);
                activityRecycleMapper.updateSkuStockDetail(detail);
            }
        }}else{
            return;
        }
        //更新数据库表中的数量
    }
}
