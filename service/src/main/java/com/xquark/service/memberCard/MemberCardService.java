package com.xquark.service.memberCard;

import com.xquark.dal.model.MemberCard;
import com.xquark.dal.model.UserMemberCard;
import com.xquark.dal.type.MemberUpgradeType;
import com.xquark.dal.vo.MemberCardUserVO;
import com.xquark.dal.vo.MemberCardVO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Created by wangxinhua on 17-6-26. DESC:
 */
public interface MemberCardService {

  /**
   * 通过id查询
   *
   * @param id 主键
   * @return 会员卡实例
   */
  MemberCard load(String id);

  /**
   * 保存会员卡信息
   *
   * @param memberCard 实体类
   * @return 是否成功
   */
  boolean save(MemberCard memberCard);

  /**
   * 更新会员卡信息
   *
   * @param memberCard 实体类
   * @return 是否成功
   */
  boolean update(MemberCard memberCard);

  /**
   * 列出所有会员卡
   *
   * @param order 排序
   * @param direction 方向
   * @param pageable 分页信息
   * @return {@link List<MemberCard>} 会员卡集合 或 空集合
   */
  List<MemberCard> list(String order, Sort.Direction direction, Pageable pageable);

  /**
   * 根据产品id选中该商品对应的会员卡
   *
   * @param productId 商品id
   * @param order 排序
   * @param direction 方向
   * @param pageable 分页信息
   * @return {@link List<MemberCardVO>} 会员卡集合 或 空集合
   */
  List<MemberCardVO> list(String productId, String order, Sort.Direction direction,
      Pageable pageable);

  List<MemberCard> listByUpgradeType(MemberUpgradeType type);

  /**
   * 查询所有会员卡以及拥有该会员卡的用户数
   *
   * @param order 排序
   * @param direction 方向
   * @param pageable 分页信息
   * @return {@link List<MemberCardVO>} 会员卡集合 或 空集合
   */
  List<MemberCardVO> listWithMemberCounts(String order, Sort.Direction direction,
      Pageable pageable);

  /**
   * 查询会员卡总数
   *
   * @return {@link Long} 总数
   */
  Long count();

  /**
   * 绑定用户和会员卡
   *
   * @param userId 用户id
   * @param cardId 会员卡id
   * @return 是否成功
   */
  boolean bindUser(String userId, String cardId);

  boolean hasBinded(String userId);

  UserMemberCard loadUserMemberCard(String userId, String cardId);

  boolean bindProduct(String productId, String[] cardIds);

  boolean unbindProduct(String productId, String cardI);

  boolean deleteProduct(String productId);

  /**
   * 根据会员卡类型查询会员卡以及符合该会员卡条件的用户集合
   *
   * @param type 升级类型
   * @return {@link List<MemberCardUserVO>} 会员卡-用户VO
   */
  List<MemberCardUserVO> listMemberCardUsersByUpgradeType(MemberUpgradeType type);

  /**
   * 查询用户购买某商品时可用的会员卡信息
   *
   * @param userId 用户id
   * @param productId 商品id
   * @return {@link MemberCard} 会员卡对象 或 null
   */
  MemberCard selectMemberCardByUserIdAndProductId(String userId, String productId);

}
