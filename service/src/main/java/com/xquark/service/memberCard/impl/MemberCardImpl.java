package com.xquark.service.memberCard.impl;

import com.xquark.dal.mapper.MemberCardMapper;
import com.xquark.dal.model.MemberCard;
import com.xquark.dal.model.UserMemberCard;
import com.xquark.dal.type.MemberUpgradeType;
import com.xquark.dal.vo.MemberCardUserVO;
import com.xquark.dal.vo.MemberCardVO;
import com.xquark.service.memberCard.MemberCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by wangxinhua on 17-6-26. DESC:
 */
@Service
public class MemberCardImpl implements MemberCardService {

  private MemberCardMapper memberCardMapper;

  @Override
  public MemberCard load(String id) {
    return memberCardMapper.selectByPrimaryKey(id);
  }

  @Override
  public boolean save(MemberCard memberCard) {
    return memberCardMapper.insert(memberCard) > 0;
  }

  @Override
  public boolean update(MemberCard memberCard) {
    return memberCardMapper.update(memberCard) > 0;
  }

  @Override
  public List<MemberCard> list(String order, Sort.Direction direction, Pageable pageable) {
    return memberCardMapper.selectList(order, direction, pageable);
  }

  @Override
  public List<MemberCardVO> list(String productId, String order, Sort.Direction direction,
      Pageable pageable) {
    return memberCardMapper.selectListWithProductId(productId, order, direction, pageable);
  }

  @Override
  public List<MemberCard> listByUpgradeType(MemberUpgradeType type) {
    return memberCardMapper.listByUpgradeType(type);
  }

  @Override
  public List<MemberCardVO> listWithMemberCounts(String order, Sort.Direction direction,
      Pageable pageable) {
    return memberCardMapper.selectListWithMemberCounts(order, direction, pageable);
  }

  @Override
  public Long count() {
    return memberCardMapper.count();
  }

  @Override
  public boolean bindUser(String userId, String cardId) {
    if (!this.hasBinded(userId)) {
      return memberCardMapper.insertUserCard(userId, cardId) > 0;
    }
    UserMemberCard userMemberCard = new UserMemberCard();
    userMemberCard.setUserId(userId);
    userMemberCard.setCardId(cardId);
    return this.updateBindInfo(userMemberCard);
  }

  @Override
  public boolean hasBinded(String userId) {
    return memberCardMapper.hasBinded(userId) > 0;
  }

  @Override
  public UserMemberCard loadUserMemberCard(String userId, String cardId) {
    return memberCardMapper.selectUserMemberCard(userId, cardId);
  }

  @Override
  public boolean bindProduct(String productId, String[] cardIds) {
    return memberCardMapper.insertProductCards(productId, cardIds) > 0;
  }

  @Override
  public boolean unbindProduct(String productId, String cardId) {
    return memberCardMapper.deleteProductCard(productId, cardId) > 0;
  }

  @Override
  public boolean deleteProduct(String productId) {
    return memberCardMapper.deleteByProductId(productId) > 0;
  }

  @Override
  public List<MemberCardUserVO> listMemberCardUsersByUpgradeType(MemberUpgradeType type) {
    return memberCardMapper.listMemberCardUsersByUpgradeType(type);
  }

  @Override
  public MemberCard selectMemberCardByUserIdAndProductId(String userId, String productId) {
    return memberCardMapper.selectMemberCardByUserIdAndProductId(userId, productId);
  }

  private boolean updateBindInfo(UserMemberCard userMemberCard) {
    return memberCardMapper.updateBindInfo(userMemberCard) > 0;
  }

  @Autowired
  public void setMemberCardMapper(MemberCardMapper memberCardMapper) {
    this.memberCardMapper = memberCardMapper;
  }

}
