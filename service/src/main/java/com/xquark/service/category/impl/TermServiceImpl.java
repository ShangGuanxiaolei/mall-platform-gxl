package com.xquark.service.category.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.TermMapper;
import com.xquark.dal.model.Term;
import com.xquark.service.category.TermService;

@Service("termService")
public class TermServiceImpl implements TermService {

  @Autowired
  private TermMapper termMapper;

  @Override
  public Term load(String id) {
    return termMapper.load(id);
  }

  @Override
  public Term loadByName(String name) {
    Term term = termMapper.loadByName(name);
    if (term == null) {
      term = new Term();
      term.setName(name);
      termMapper.insert(term);
    }
    return term;
  }

}
