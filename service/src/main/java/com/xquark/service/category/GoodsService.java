package com.xquark.service.category;

import com.xquark.dal.model.Category;
import com.xquark.dal.type.Taxonomy;
import java.util.List;

public interface GoodsService {

  List<Category> listUserRootGoods(Taxonomy taxonomy);

  Category saveUserGoods(String name);

  Category updateUserGoodsName(String id, Taxonomy taxo, String name);

  void removeUesrGoods(String id);

  long countUserGoodsProducts(String id);

}
