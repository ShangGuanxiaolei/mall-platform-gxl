package com.xquark.service.category.impl;

import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.status.CategoryStatus;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.type.Taxonomy;
import com.xquark.dal.vo.CategoryTreeVO;
import com.xquark.dal.vo.CategoryVO;
import com.xquark.service.cache.annotation.DoGuavaCache;
import com.xquark.service.cache.constant.ExpireAt;
import com.xquark.service.category.CategoryService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.xquark.service.cache.constant.CacheKeyConstant.LOAD_CATEGORY_BY_PRODUCTID;

@Service("categoryService")
public class CategoryServiceImpl extends TermTaxonomyServiceImpl implements CategoryService {

  @Autowired
  private CategoryActivityMapper categoryActivityMapper;

  @Autowired
  private CategoryMapper categoryMapper;

  @Autowired
  private TermRelationshipMapper termRelationshipMapper;

  @Autowired
  private ProductMapper productMapper;

  @Autowired
  private PosterMapper posterMapper;

  @Autowired
  private ShopMapper shopMapper;

  @Autowired
  private TermMapper termMapper;

  private Logger log = LoggerFactory.getLogger(getClass());

  @Override
  public List<Category> listUserRootGoods(Taxonomy taxonomy) {
    return categoryMapper.listRootsByUser(taxonomy, getCurrentUser().getId(), false);
  }

  @Override
  public Category saveUserGoods(String name) {
    return save(name, Taxonomy.GOODS, null, getCurrentUser().getId(), getCurrentUser().getShopId());
  }

  @Override
  public Category saveUserGoods(String name, String parentid,
      Taxonomy taxonomy) {
    return save(name, taxonomy, parentid, getCurrentUser().getId(), getCurrentUser().getShopId());
  }

  @Override
  public Category updateUserGoodsName(String id, Taxonomy taxo, String name) {
    return updateName(id, taxo, name, getCurrentUser().getId());
  }

  @Override
  public void batchUpdateUserGoodsName(List<Category> categories) {
    for (Category c : categories) {
      updateUserGoodsName(c.getId(), Taxonomy.GOODS, c.getName());
    }
  }

  @Override
  public void removeUesrGoods(String id) {
    removeUesrGoodsCategory(id);
  }

  @Override
  public void removeUesrGoodsCategory(String... ids) {
    if (ids == null || ArrayUtils.isEmpty(ids)) {
      return;
    }

    String s1 = "";
    String s2 = "";
    for (String s : ids) {
      Category c = load(s);
      if (c == null) {
        s1 = s1 + s + ",";
      } else {
        long count = countProducts(s, null);
        if (count > 0) {
          s2 = s2 + c.getName() + "(" + s + "),";
        }
      }
    }
    if (!StringUtils.isBlank(s1)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "您要操作的分类[ " + s1.substring(0, s1.length() - 1) + " ]不存在，删除失败");
    }
    if (!StringUtils.isBlank(s2)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "该类目[ " + s2.substring(0, s2.length() - 1) + " ]有所属商品，删除失败");
    }

    removeCategory(null, ids);
  }

  @Override
  public long countUserGoodsProducts(String categoryId) {
    return countProducts(categoryId, getCurrentUser().getId());
  }

  @Override
  public long countUserCategorySellers(String shopId, String categoryId) {
    //TODO
//        if(shopId==null){
//            shopId = getCurrentUser().getShopId();
//        }
//        return categoryMapper.countSellersByShopId(shopId,categoryId);
    return 0;
  }

  @Override
  public void addProductCategory(String pid, String cid) {
    String objType = "product";
    List<TermRelationship> t = termRelationshipMapper.checkProductIdExists(objType, pid);
    if (t != null && t.size() > 0) {
      log.info("termRelationshipMapper size :" + t.size());
      for (int i = 0; i < t.size(); i++) {
        log.info("termRelationshipMapper:" + t.get(i).getId());
        termRelationshipMapper.delete(t.get(i).getId());
      }
    }
    // 商品分类支持多选
    String[] cids = cid.split(",");
    for (String acid : cids) {
      if (!"".equals(acid)) {
        TermRelationship tr = new TermRelationship();
        tr.setCategoryId(acid);
        tr.setObjType(objType);
        tr.setObjId(pid);
        termRelationshipMapper.insert(tr);
      }
    }
  }

  @Override
  public void addProductsCategory(List<String> pids, String cid) {
    for (String pid : pids) {
      addProductCategory(pid, cid);
    }
  }

  @Override
  public void removeProductCategory(String pid, String cid) {
    String type = "product";
    termRelationshipMapper.deleteByCatAndObject(type, pid, cid);
  }

  @Override
  public void removeProductsCategory(List<String> pids, String cid) {
    for (String pid : pids) {
      removeProductCategory(pid, cid);
    }
  }

  @Override
  public void addPosterCategory(String posterId, String cid) {
    TermRelationship tr = new TermRelationship();
    tr.setCategoryId(cid);
    tr.setObjType("poster");
    tr.setObjId(posterId);
    termRelationshipMapper.insert(tr);
  }

  @Override
  public void removePosterCategory(String posterId, String cid) {
    String type = "poster";
    termRelationshipMapper.deleteByCatAndObject(type, posterId, cid);
  }


  @Override
  public List<Product> listProductsUnderCategory(String cid, Pageable pager) {
    List<TermRelationship> list = termRelationshipMapper
        .listUnderCategory(cid, getCurrentUser().getShopId(), "product", pager);
    List<Product> result = new ArrayList<Product>(list.size());
    for (TermRelationship tr : list) {
      Product p = productMapper.selectByPrimaryKey(tr.getObjId());
      if (p != null) {
        result.add(p);
      } else {
        log.error("product not found by id[" + tr.getObjId() + "]");
      }
    }
    return result;
  }

  @Override
  public List<Product> listProductsInCategory(String cid, Pageable pager) {
    List<TermRelationship> list = termRelationshipMapper.listInCategory(cid, "product", pager);
    List<Product> result = new ArrayList<Product>(list.size());
    for (TermRelationship tr : list) {
      Product p = productMapper.selectByPrimaryKey(tr.getObjId());
      if (p != null) {
        result.add(p);
      } else {
        // TODO
      }
    }
    return result;
  }

  @Override
  public List<Poster> listPostersInCategory(String cid, Pageable pager) {
    List<TermRelationship> list = termRelationshipMapper.listInCategory(cid, "poster", pager);
    List<Poster> result = new ArrayList<Poster>(list.size());
    for (TermRelationship tr : list) {
      Poster p = posterMapper.load(tr.getObjId());
      if (p != null) {
        result.add(p);
      } else {
        // TODO
      }
    }
    return result;
  }

  @Override
  public long countProductsUnderCategory(String cid) {
    return termRelationshipMapper.countUnderCategory(cid, "product");
  }

  @Override
  public List<Poster> listPostersUnderCategory(String cid, Pageable pager) {
    List<TermRelationship> list = termRelationshipMapper
        .listUnderCategory(cid, getCurrentUser().getShopId(), "poster", pager);
    List<Poster> result = new ArrayList<Poster>(list.size());
    for (TermRelationship tr : list) {
      Poster p = posterMapper.load(tr.getObjId());
      if (p != null) {
        result.add(p);
      } else {
        // TODO
      }
    }
    return result;
  }

  @Override
  @DoGuavaCache(key = LOAD_CATEGORY_BY_PRODUCTID,expireAt = ExpireAt.FIVE)
  public CategoryVO loadCategoryByProductId(String productId) {
    return categoryMapper.loadCategoryByProductId(productId);
  }

  @Override
  public List<CategoryVO> loadCategorysByProductId(String productId) {
    return categoryMapper.loadCategorysByProductId(productId);
  }

  @Override
  public List<Product> listUnCategoriedProducts(Pageable pageable) {
    return listUnCategoriedProducts(null, pageable);
  }

  @Override
  public List<Product> listUnCategoriedProducts(String cid, Pageable pageable) {
    return productMapper
        .listUnCategoriedProductsInShop(getCurrentUser().getShopId(), pageable, cid);
  }

  @Override
  public long countUnCategoriedProducts() {
    return productMapper.countUnCategoriedProductsInShop(getCurrentUser().getShopId());
  }

  @Override
  public void updateIdx(List<String> categoryIds) {
    for (int i = 0; i < categoryIds.size(); i++) {
      categoryMapper.updateIdx(categoryIds.get(0), i);
    }
  }

  @Override
  public void updateIdx(String cateId, Integer idx) {
    // TODO Auto-generated method stub
    categoryMapper.updateIdx(cateId, idx);
  }

  @Override
  public List<Category> listRootCategoriesByShop(String shopId,
      Taxonomy taxonomy) {
    Shop shop = shopMapper.selectByPrimaryKey(shopId);
    if (shop != null) {
      return categoryMapper.listRootsByUser(taxonomy, shop.getOwnerId(), false);
    }
    return null;
  }

  @Override
  public List<Product> listUnCategoryProducts(String cid, Pageable pageable) {
    return productMapper.listUnCategoryProductsInShop(getCurrentUser().getShopId(), pageable, cid);
  }

  @Override
  public CategoryVO selectVoById(String categoryId) {

    return categoryMapper.selectVoById(categoryId);
  }

  @Override
  public List<CategoryActivity> listCategoryActivity() {
    List<CategoryActivity> list = null;
    try {
      String userid = getCurrentUser().getId();
      list = categoryActivityMapper.listCategoryforActivity(userid);
    } catch (Exception e) {
      log.info("user does not login");
    }
    return list;
  }

  @Override
  public List listAllCategorys(String shopId, Taxonomy taxonomy) {
    ArrayList list = new ArrayList();
    List<Category> categorylist = categoryMapper.listAllCategorys(shopId, taxonomy);

    HashMap map = new HashMap();
    map.put("id", "0");
    map.put("text", "标签分类");
    ArrayList chlist = (ArrayList) treeMenuList(categorylist, null);
    if (chlist != null && chlist.size() > 0) {
      map.put("children", chlist);
    }
    list.add(map);
    return list;
  }

  /**
   * 递归得到类别的上下级关系数据
   */
  private List treeMenuList(List<Category> allList, String parentid) {
    List result = new ArrayList();
    for (Category object : allList) {
      String menuId = object.getId();
      String pid = object.getParentId();
      CategoryTreeVO treeVO = new CategoryTreeVO();
      treeVO.setId(menuId);
      treeVO.setText(object.getName());
      treeVO.setTinyImg(object.getTinyImg());
      treeVO.setFontColor(object.getFontColor());
      String img = object.getImg();
      if (StringUtils.isNotEmpty(img)) {
        treeVO.setIcon(img);
      }
      if (parentid == pid || (parentid != null && parentid.equals(pid))) {
        List c_node = treeMenuList(allList, menuId);
        treeVO.setChildren(c_node);
        result.add(treeVO);
      }
    }
    return result;
  }

  @Override
  public void updateImg(String img, String id) {
    categoryMapper.updateImg(img, id);
  }

  @Override
  public void updateTinyImg(String img, String id) {
    categoryMapper.updateTinyImg(img, id);
  }

  @Override
  public void updateFontColor(String fontColor, String id) {
    categoryMapper.updateFontColor(fontColor, id);
  }

  @Override
  public void move(String id, String parentid, String position, String sourceposition,
      String sourceparentid) {
    // 如果是同一父级节点中移动,更新其他节点的idx信息
    if ((sourceparentid + "").equals((parentid + ""))) {
      if (new Integer(sourceposition) > new Integer(position)) {
        categoryMapper.downIdx(parentid, position, sourceposition);
      } else {
        categoryMapper.upIdx(parentid, position, sourceposition);
      }
    }
    // 如果是不同父级节点中移动,更新其他节点的idx信息
    else {
      categoryMapper.jumpIdx(sourceparentid, sourceposition);
      categoryMapper.jumpupIdx(parentid, position);
    }

    // 更新移动的节点相关数据
    categoryMapper.move(id, parentid, position);
    updateChTreePath(id);
  }

  /**
   * 递归更新某节点下所有节点的treepath
   */
  private void updateChTreePath(String id) {
    Category vo = categoryMapper.load(id);
    String treePath = "";
    String parentid = vo.getParentId();
    if (parentid != null) {
      Category parentvo = categoryMapper.load(parentid);
      treePath = parentvo.getTreePath();
      treePath = treePath + parentid + ">";
    }
    categoryMapper.updateTreePath(id, treePath);
    List<Category> subList = categoryMapper.loadSubs(id);
    for (Category object : subList) {
      updateChTreePath(object.getId());
    }
  }

  @Override
  public void delete(String id) {
    removeUesrGoods(id);
    List<Category> subList = categoryMapper.loadSubs(id);
    for (Category object : subList) {
      delete(object.getId());
    }
  }

  @Override
  @Transactional
  public boolean addCategory(String id, String shopId) {
    if (shopId == null) {
      shopId = getCurrentUser().getShopId();
    }
    List<Product> categoryProducts = new ArrayList<Product>();
    Category vo = categoryMapper.loadBySourceIdAndShopId(id, shopId);
    if (vo == null) {
      vo = categoryMapper.load(id);
      //String sourceShopId = vo.getShopId();
      vo.setSourceCategoryId(vo.getId());
      vo.setShopId(shopId);
      vo.setImg(vo.getImg());
      vo.setCreatorId(getCurrentUser().getId());
      vo.setIdx(0);
      categoryMapper.insert(vo);
//            categoryProducts = productMapper.listUnCategoriedProductsInShop(sourceShopId, null, id);
//            for(Product product:categoryProducts){
//                 ProductDistributor p = new ProductDistributor();
//                 p.setProductId(product.getId());
//                 p.setShopId(shopId);
//                 p.setStatus(ProductStatus.ONSALE);
//                productDistributorService.insert(p);
//             }
    } else {
      if (vo.getStatus().equals(CategoryStatus.INSTOCK)) {
        categoryMapper.updateForOnsale(vo.getId());
//                categoryProducts = productMapper.listUnCategoriedProductsInShopInstock(vo.getShopId(), null, vo.getId());
//                for (Product product : categoryProducts) {
//                    ProductDistributor  productDistributor = productDistributorService.selectByProductIdAndShopId(product.getId(),shopId);
//                    productDistributorService.updateForInstock(productDistributor.getId());
//                }
      } else {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "此分类已上架 不允许重复上架！");
      }
    }
    return true;
  }

  @Override
  @Transactional
  public boolean updateForInstock(String id) {
    categoryMapper.updateForInstock(id);
    return true;
  }

  @Override
  public List<Category> loadCategoriesByShopId(String shopId, int level, String parentCategoryId,
      Taxonomy taxonomy) {
    List<Category> listCategory = categoryMapper.loadCategoriesByShopId(shopId, taxonomy);
    List<Category> listCategoryCopy = new ArrayList<Category>();
    Category all = new Category();
    if (level == 1) {
      all.setName("全部");
      all.setId("0");
      listCategoryCopy.add(all);
    }

    //level = 0 取所有
    if (level != 0) {
      //所有多级可用分类
      for (Category category : listCategory) {
        String treePath = category.getTreePath();
        //if (StringUtils.isNotBlank(treePath)) {
        if ((StringUtils.countMatches(treePath, ">") + 1) == level) {
          if (StringUtils.isNotBlank(parentCategoryId)) {
            if (parentCategoryId.equals(category.getParentId())) {
              listCategoryCopy.add(category);
            }
          } else {
            listCategoryCopy.add(category);
          }
        }
        //}
      }
    } else if (level == 0) {
      BeanUtils.copyProperties(listCategory, listCategoryCopy);
    }
    return listCategoryCopy;
  }

  @Override
  public List<Category> loadCategoriesByShopIdNotAll(String shopId, int level,
      String parentCategoryId) {
    List<Category> listCategory = categoryMapper.loadCategoriesByShopId(shopId, Taxonomy.GOODS);
    List<Category> listCategoryCopy = new ArrayList<Category>();
    Category all = new Category();

    //level = 0 取所有
    if (level != 0) {
      //所有多级可用分类
      for (Category category : listCategory) {
        String treePath = category.getTreePath();
        //if (StringUtils.isNotBlank(treePath)) {
        if ((StringUtils.countMatches(treePath, ">") + 1) == level) {
          if (StringUtils.isNotBlank(parentCategoryId)) {
            if (parentCategoryId.equals(category.getParentId())) {
              listCategoryCopy.add(category);
            }
          } else {
            listCategoryCopy.add(category);
          }
        }
        //}
      }
    } else if (level == 0) {
      BeanUtils.copyProperties(listCategory, listCategoryCopy);
    }
    return listCategoryCopy;
  }

  /**
   * 根据名称查找分类
   */
  @Override
  public Category loadByName(String name) {
    return categoryMapper.loadByName(name);
  }

  @Override
  public List<Category> listCategoriesOnsaleByShop(String shopId) {
    return categoryMapper.listCategoriesByShopAndStatus(shopId, CategoryStatus.ONSALE);
  }

  /**
   * 根据店铺id分页查找上架分类
   */
  @Override
  public List<Category> listCategoriesByShopAndStatusPage(Pageable pager, String shopId,
      String categoryId, CategoryStatus categoryStatus) {
    return categoryMapper
        .listCategoriesByShopAndStatusPage(pager, shopId, categoryId, categoryStatus);
  }

  @Override
  public List<Category> listByName(String shopId, String name, String parentCategoryId) {
    return categoryMapper.listByName(shopId, name, parentCategoryId);
  }

  @Override
  public int sortShopCategory(List<String> categoryIds) {

    return categoryMapper.sortShopCategorys(categoryIds);
  }

  /**
   * 总店类别更新后，同时更新店铺上架复制出来的品牌类别
   */
  @Override
  public int updateCopyCategory(String id) {
    return categoryMapper.updateCopyCategory(id);
  }

  /**
   * 总店类别删除后，同时删除店铺上架复制出来的品牌类别
   */
  @Override
  public int deleteCopyCategory(String id) {
    return categoryMapper.deleteCopyCategory(id);
  }

  /**
   * 供客户端调用，获取首页商品类别信息
   */
  @Override
  public List<Category> getHomeForApp() {
    return categoryMapper.getHomeForApp();
  }

  @Override
  public List<Category> loadSubs(String id) {
    return categoryMapper.loadSubs(id);
  }

  @Override
  public Category loadByThird(String thirdId, ProductSource source) {
    return categoryMapper.loadByThird(thirdId, source);
  }

    @Override
    public List<Category> getNameAndId() {
        return categoryMapper.getNameAndId();
    }

  @Override
  public List<Category> getRandIdList() {
    return categoryMapper.getRandIdList();
  }

  @Override
  public List<Category> getAllCategoryIdList() {
    return categoryMapper.getAllCategoryIdList();
  }

  @Override
  @Transactional
  public int deleteCategory(String type) {
    List<TermRelationship> termRelationships = getTermRelationships(type);
    Set<String> categoryIds = new HashSet<>();
    for (TermRelationship termRelationship:
        termRelationships) {
      String id = termRelationship.getId();
      if (StringUtils.isBlank(id)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "relationshipId不能为空");
      }
      String categoryId = termRelationship.getCategoryId();
      if (StringUtils.isBlank(categoryId)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "分类id为空");
      }
      categoryIds.add(categoryId);
      if (!(termRelationshipMapper.delete(id) > 0)){
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "删除relationship失败");
      }
    }
    for (String categoryId:
        categoryIds) {
      Category category = categoryMapper.load(categoryId);
      if (category == null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "查询分类失败");
      }
      String termId = category.getTermId();
      if (StringUtils.isBlank(termId)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "termId不能为空");
      }
      //删除分类
      if (!(termMapper.deleteTermById(termId) > 0)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "删除termId失败");
      }
      if (!(categoryMapper.deleteCategoryById(categoryId) > 0)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "删除分类失败");
      }
    }
    return 1;
  }

  private List<TermRelationship> getTermRelationships(String type) {
    if (org.apache.commons.lang.StringUtils.isBlank(type)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动类型不能为空");
    }
    return termRelationshipMapper.selectByObjType(type);
  }
}
