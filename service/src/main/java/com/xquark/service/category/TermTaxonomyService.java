package com.xquark.service.category;

import com.xquark.dal.model.Category;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.type.Taxonomy;
import java.util.List;

/**
 * @author ahlon
 */
public interface TermTaxonomyService {

  Category save(String term, Taxonomy taxo, String parentId, String userId, String shopId);

  Category save(String name, Taxonomy taxo, String parentId,
      String userId, String shopId, ProductSource source, String sourceId);

  Category save(Long termId, Taxonomy taxo, String parentId, String userId, String shopId);

  Category updateName(String id, Taxonomy taxo, String name, String userId);

  void remove(String id, String userId);

  void moveBefore(String srcId, String desId);

  void moveAfter(String srcId, String desId);

  Category load(String id);

  // 获取分类树的根节点
  List<Category> listRootCategories(Taxonomy taxo);

  // 获取所有节点
  List<Category> listAll(Taxonomy goods);

  // 获取分类的父节点
  Category loadParentCategory(String id);

  // 获取分类的所有祖先节点
  List<Category> listAscendantCategories(String id);

  // 获取分类的兄弟姐妹节点
  List<Category> listSiblingCategories(String id);

  // 获取分类的子节点
  List<Category> listSubCategories(String id);

  // 获取分类的所有子孙节点
  List<Category> listDescendantCategories(String id);

  void removeCategory(String userId, String... ids);

}
