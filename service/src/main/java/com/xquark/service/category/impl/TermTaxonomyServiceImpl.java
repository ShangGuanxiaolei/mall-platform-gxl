package com.xquark.service.category.impl;

import com.xquark.dal.mapper.CategoryMapper;
import com.xquark.dal.mapper.PosterMapper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.mapper.TermMapper;
import com.xquark.dal.mapper.TermRelationshipMapper;
import com.xquark.dal.model.Category;
import com.xquark.dal.model.Term;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.type.Taxonomy;
import com.xquark.service.category.TermService;
import com.xquark.service.category.TermTaxonomyService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("termTaxonomyService")
public class TermTaxonomyServiceImpl extends BaseServiceImpl implements TermTaxonomyService {

  @Autowired
  private CategoryMapper categoryMapper;

  @Autowired
  private TermMapper termMapper;

  @Autowired
  private TermService termService;

  @Autowired
  private TermRelationshipMapper termRelationshipMapper;

  @Autowired
  private ProductMapper productMapper;

  @Autowired
  private PosterMapper posterMapper;

  @Override
  public Category save(String name, Taxonomy taxo, String parentId,
      String userId, String shopId) {
    return save(name, taxo, parentId,userId, shopId, ProductSource.CLIENT, null);
  }

  @Override
  public Category save(String name, Taxonomy taxo, String parentId,
      String userId, String shopId, ProductSource source, String sourceId) {
    if (StringUtils.isEmpty(name)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "分类的名称不能为空");
    }

    // 控制分类的名称的长度，全中文8位，中文+英文/英文 长度为16
    int nameLength = 16;
    if (!Pattern.matches("/^[\u4E00-\u9FA5]+$/", name)) {
      // 不全是中文
    } else {
      //全是中文
      nameLength = 8;
    }

    if (name.length() > nameLength) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "分类的名称不能大于 " + nameLength + " 位");
    }

    if (taxo == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "分类的类别不能为空");
    }
    Term term = termMapper.loadByName(name);
    if (term == null) {
      term = new Term();
      term.setName(name);
      termMapper.insert(term);
    }

    Category cat = categoryMapper.selectByTermAndTaxonomy(term.getId(), taxo, shopId);

    if (cat == null) {
      cat = new Category();
      cat.setTermId(term.getId());
      cat.setName(term.getName());
      cat.setTaxonomy(taxo);
      cat.setCreatorId(userId);
      cat.setShopId(shopId);
      cat.setTreePath(""); // default value
      cat.setSource(source);
      cat.setSourceCategoryId(sourceId);
      if (!StringUtils.isEmpty(parentId)) {
        Category parent = load(parentId);
        if (parent == null) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "分类的父分类不存在");
        }
        cat.setParentId(parentId);
        cat.setTreePath(parent.getTreePath() + parentId + ">");
      }
      Integer maxIdx = categoryMapper.selectMaxIdx(parentId, shopId);
      cat.setIdx(maxIdx == null ? 0 : maxIdx + 1);
      categoryMapper.insert(cat);
    } else {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "类目[" + name + "]已经存在，abc与Abc是一样的哦~");
    }
    return cat;
  }

  @Override
  public Category save(Long termId, Taxonomy taxo, String parentId, String userId, String shopId) {
    if (termId == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "参数termId不能为空");
    }
    if (taxo == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "分类的类别不能为空");
    }
    Term term = termMapper.load(Long.toString(termId));
    if (term == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "termId对应的词汇找不到");
    }
    Category cat = categoryMapper.selectByTermAndTaxonomy(term.getId(), taxo, shopId);
    if (cat == null) {
      cat = new Category();
      cat.setTermId(term.getId());
      cat.setName(term.getName());
      cat.setTaxonomy(taxo);
      cat.setCreatorId(userId);
      if (!StringUtils.isEmpty(parentId)) {
        Category parent = load(parentId);
        if (parent == null) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "分类的父分类不存在");
        }
        cat.setParentId(parentId);
        cat.setTreePath(parent.getTreePath() + parentId + ">");
      }
      categoryMapper.insert(cat);
    }
    return cat;
  }

  @Override
  public Category updateName(String id, Taxonomy taxo, String name, String userId) {
    Category cat = categoryMapper.load(id);
    if (cat == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "更新的分类不存在");
    }

    Term term = termService.loadByName(name);
    cat = categoryMapper.loadByTermIdAndTaxonomy(term.getId(), taxo, userId);
    if (cat != null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "重名名失败，已经有重名的分类");
    }
    categoryMapper.updateCategoryTerm(id, term.getId(), term.getName(), userId);
    return categoryMapper.load(id);
  }

  @Override
  public void removeCategory(String userId, String... ids) {
    categoryMapper.deleteCategorys(userId, ids);
  }

  @Override
  public void remove(String id, String userId) {
    int r = categoryMapper.delete(id, userId);
    if (r == 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "要删除的类目不存在");
    }
  }

  @Override
  public Category load(String id) {
    return categoryMapper.load(id);
  }

  @Override
  public Category loadParentCategory(String id) {
    return categoryMapper.loadParent(id);
  }

  @Override
  public List<Category> listAscendantCategories(String id) {
    Category cat = categoryMapper.load(id);
    String treePath = cat.getTreePath();
    List<Category> result = new ArrayList<Category>();
    for (String str : treePath.split(">")) {
      if (StringUtils.isEmpty(str)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
            "分类的数据错误，获取id为[" + id + "]节点的祖先节点失败");
      }
      result.add(categoryMapper.load(id));
    }
    return result;
  }

  @Override
  public List<Category> listRootCategories(Taxonomy taxo) {
    return categoryMapper.selectRootNodes(taxo);
  }

  @Override
  public List<Category> listAll(Taxonomy taxo) {
    return categoryMapper.selectByTaxonomy(taxo);
  }

  @Override
  public List<Category> listSiblingCategories(String id) {
    return categoryMapper.selectSiblings(id);
  }

  @Override
  public List<Category> listSubCategories(String id) {
    return categoryMapper.loadSubs(id);
  }

  @Override
  public List<Category> listDescendantCategories(String id) {
    return categoryMapper.loadDescendants(id);
  }


  public long countProducts(String id, String userId) {
    return termRelationshipMapper.countProducts(id, userId);
  }

  @Override
  public void moveBefore(String srcId, String desId) {
    Category dest = categoryMapper.load(desId);
    categoryMapper.updateIdxNotBeforeDest(dest.getParentId(), dest.getShopId(), dest.getIdx(), 1);
    categoryMapper.updateIdx(srcId, dest.getIdx());
  }

  @Override
  public void moveAfter(String srcId, String desId) {
    Category dest = categoryMapper.load(desId);
    categoryMapper.updateIdxAfterDest(dest.getParentId(), dest.getShopId(), dest.getIdx(), 1);
    categoryMapper.updateIdx(srcId, dest.getIdx() + 1);
  }
}
