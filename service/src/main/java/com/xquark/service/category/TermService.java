package com.xquark.service.category;

import com.xquark.dal.model.Term;

public interface TermService {

  public Term load(String id);

  public Term loadByName(String name);

}
