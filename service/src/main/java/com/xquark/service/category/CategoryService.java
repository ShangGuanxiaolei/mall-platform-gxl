package com.xquark.service.category;

import com.xquark.dal.model.Category;
import com.xquark.dal.model.CategoryActivity;
import com.xquark.dal.model.Poster;
import com.xquark.dal.model.Product;
import com.xquark.dal.status.CategoryStatus;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.type.Taxonomy;
import com.xquark.dal.vo.CategoryVO;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CategoryService {

  /**
   * 获取用户所有商品分类
   */
  List<Category> listUserRootGoods(Taxonomy taxonomy);

  List<Category> listRootCategoriesByShop(String shopId, Taxonomy taxonomy);

  Category load(String id);

  /**
   * 添加分类
   */
  Category saveUserGoods(String name);

  /**
   * 添加分类
   */
  Category saveUserGoods(String name, String parentid, Taxonomy taxonomy);

  /**
   * 修改分类
   */
  Category updateUserGoodsName(String id, Taxonomy taxo, String name);

  /**
   * 删除分类（该分类下有商品，是不能删除分类的）
   */
  void removeUesrGoods(String id);

  void removeUesrGoodsCategory(String... ids);

  /**
   * 统计某个分类下的商品数量
   */
  long countUserGoodsProducts(String id);

  /**
   * 统计某个分类下卖家
   */
  long countUserCategorySellers(String shopId, String categoryId);

  void moveBefore(String srcId, String desId);

  void moveAfter(String srcId, String desId);

  /**
   * 商品添加到分类（一个商品只能属于一个分类）
   */
  void addProductCategory(String productId, String cid);

  /**
   * 批量修改商品的分类（一个商品只能有一个分类）
   */
  void addProductsCategory(List<String> productIds, String cid);

  /**
   * 商品从分类中移除
   */
  void removeProductCategory(String productId, String cid);

  /**
   * 批量把商品移除某个分类
   */
  void removeProductsCategory(List<String> asList, String cid);

  /**
   * 海报添加到分类
   */
  void addPosterCategory(String posterId, String cid);

  /**
   * 海报从分类中移除
   */
  void removePosterCategory(String posterId, String cid);

  /**
   * 列出所有店铺该分类中的商品
   */
  List<Product> listProductsInCategory(String cid, Pageable pager);

  /**
   * 列出某个店铺分类下的所有商品
   */
  List<Product> listProductsUnderCategory(String cid, Pageable pager);

  long countProductsUnderCategory(String cid);

  // 列出分类中的海报
  List<Poster> listPostersInCategory(String cid, Pageable pager);

  // 列出分类下的所有海报
  List<Poster> listPostersUnderCategory(String cid, Pageable pager);

  CategoryVO loadCategoryByProductId(String productId);

  // 取出商品下对应的所有分类
  List<CategoryVO> loadCategorysByProductId(String productId);

  /**
   * 列出店铺内没有分类的商品
   */
  List<Product> listUnCategoriedProducts(String cid, Pageable pageable);

  /**
   * 列出店铺内没有分类的商品
   */
  List<Product> listUnCategoryProducts(String cid, Pageable pageable);

  List<Product> listUnCategoriedProducts(Pageable pageable);

  List<CategoryActivity> listCategoryActivity();

  long countUnCategoriedProducts();

  void updateIdx(List<String> categoryIds);

  void updateIdx(String cateId, Integer idx);

  /**
   * 批量修改分类信息
   */
  void batchUpdateUserGoodsName(List<Category> categorys);

  CategoryVO selectVoById(String categoryId);

  /**
   * 取出所有类别数据
   */
  List listAllCategorys(String shopId, Taxonomy taxonomy);

  /**
   * 更新分类图片
   */
  void updateImg(String img, String id);

  /**
   * 更新类别小图片
   */
  void updateTinyImg(String img, String id);

  /**
   * 更新类别字体颜色
   */
  void updateFontColor(String fontColor, String id);

  /**
   * 分类节点移动
   */
  void move(String id, String parentid, String position, String sourceposition,
      String sourceparentid);

  /**
   * 删除分类，同时删除该分类所有下级分类
   */
  void delete(String id);

  /**
   * 大b分类上架到小b店铺
   */
  boolean addCategory(String id, String shopId);

  boolean updateForInstock(String id);

  List<Category> loadCategoriesByShopId(String shopId, int level, String parentCategoryId,
      Taxonomy taxonomy);

  List<Category> loadCategoriesByShopIdNotAll(String shopId, int level, String parentCategoryId);


  /**
   * 根据名称查找分类
   */
  Category loadByName(String name);

  /**
   * 根据店铺id查找上架分类
   */
  List<Category> listCategoriesOnsaleByShop(String shopId);

  /**
   * 根据店铺id分页查找上架分类
   */
  List<Category> listCategoriesByShopAndStatusPage(Pageable pager, String shopId, String categoryId,
      CategoryStatus categoryStatus);

  /**
   * 根据名称查找分类
   */
  List<Category> listByName(String shopId, String name, String parentCategoryId);

  /**
   * 我的店铺品牌排序
   */
  int sortShopCategory(List<String> categoryIds);

  /**
   * 总店类别更新后，同时更新店铺上架复制出来的品牌类别
   */
  int updateCopyCategory(String id);

  /**
   * 总店类别删除后，同时删除店铺上架复制出来的品牌类别
   */
  int deleteCopyCategory(String id);

  /**
   * 供客户端调用，获取首页商品类别信息
   */
  List<Category> getHomeForApp();

  List<Category> loadSubs(String id);

  Category loadByThird(String thirdId, ProductSource source);

    List<Category> getNameAndId();

    List<Category> getRandIdList();

    List<Category> getAllCategoryIdList();

  /**
   * 删除所有新建的分类数据
   * @param type 商品id
   * @return 删除结果
   */
  int deleteCategory(String type);
}