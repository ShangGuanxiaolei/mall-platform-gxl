package com.xquark.service.category.impl;

import com.xquark.dal.mapper.CategoryMapper;
import com.xquark.dal.model.Category;
import com.xquark.dal.type.Taxonomy;
import com.xquark.service.category.GoodsService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("goodsService")
public class GoodsServiceImpl extends CategoryServiceImpl implements GoodsService {

  @Autowired
  private CategoryMapper categoryMapper;

  @Override
  public List<Category> listUserRootGoods(Taxonomy taxonomy) {
    return categoryMapper.listRootsByUser(taxonomy, getCurrentUser().getId(), false);
  }

  @Override
  public Category saveUserGoods(String name) {
    return save(name, Taxonomy.GOODS, null, getCurrentUser().getId(), getCurrentUser().getShopId());
  }

  @Override
  public Category updateUserGoodsName(String id, Taxonomy taxo, String name) {
    return updateName(id, taxo, name, getCurrentUser().getId());
  }

  @Override
  public void removeUesrGoods(String id) {
    long count = countProducts(id, getCurrentUser().getId());
    if (count > 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "该类目下有商品，暂时不能删除");
    }
    remove(id, getCurrentUser().getId());
  }

  @Override
  public long countUserGoodsProducts(String id) {
    return countProducts(id, getCurrentUser().getId());
  }

  @Override
  public long countUserCategorySellers(String shopId, String categoryId) {
    return 0;
  }

}