package com.xquark.service.fancard.impl;

import com.xquark.dal.mapper.FanCardApplyMapper;
import com.xquark.dal.model.FanCardApply;
import com.xquark.dal.model.User;
import com.xquark.dal.type.FanCardApplyStatus;
import com.xquark.dal.type.YundouOperationType;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.fancard.FanCardService;
import com.xquark.service.user.UserService;
import com.xquark.service.yundou.Result;
import com.xquark.service.yundou.YundouAmountService;
import com.xquark.wechat.util.RandomStringGenerator;
import com.xquark.wechat.util.RandomStringGenerator.GenerateMode;
import java.util.List;
import java.util.Random;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangxinhua on 17-11-7. DESC:
 */
@Service
public class FanCardServiceImpl implements FanCardService {

  private final Logger logger = LoggerFactory.getLogger(FanCardServiceImpl.class);

  private FanCardApplyMapper applyMapper;

  private UserService userService;

  private YundouAmountService yundouAmountService;

  @Autowired
  public void setApplyMapper(FanCardApplyMapper applyMapper) {
    this.applyMapper = applyMapper;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setYundouAmountService(YundouAmountService yundouAmountService) {
    this.yundouAmountService = yundouAmountService;
  }

  @Override
  @Transactional
  public boolean apply(String userId, FanCardApply apply) {
    assert apply != null;
    User user = userService.load(userId);
    if (user == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "用户不存在");
    }
    String fanCardNo = user.getFanCardNo();
    if (StringUtils.isNotBlank(fanCardNo)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "您已申请过范卡，请勿重复提交");
    }
    // 生成6位大写字母加数字的随机注册码
    fanCardNo = createCode(10);
    if (fanCardNo == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "范卡注册码生成失败, 请稍后再试");
    }

    boolean updateUserResult = userService.updateFanCardNo(userId, fanCardNo);
    apply.setUserId(userId);
    apply.setStatus(FanCardApplyStatus.SUCCEED);

    boolean saveApplyResult = this.save(apply);

    if (!updateUserResult || !saveApplyResult) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "申请失败，请稍后再试");
    }

    return Boolean.TRUE;

  }

  @Override
  public boolean save(FanCardApply apply) {
    return applyMapper.insert(apply) > 0;
  }

  @Override
  public boolean update(FanCardApply apply) {
    return applyMapper.updateByPrimaryKeySelective(apply) > 0;
  }

  @Override
  public boolean delete(String id) {
    return applyMapper.deleteByPrimaryKey(id) > 0;
  }

  @Override
  public FanCardApply load(String id) {
    return applyMapper.selectByPrimaryKey(id);
  }

  @Override
  public FanCardApply loadByUserId(String userId) {
    return applyMapper.selectByUserId(userId);
  }

  @Override
  public List<FanCardApply> list(String order, Direction direction, Pageable page) {
    return applyMapper.list(order, direction, page);
  }

  @Override
  public boolean isCodeExist(String code) {
    return applyMapper.checkCodeExist(code);
  }

  @Override
  @Transactional
  public boolean exchange(String userId, String code) {
    User user = userService.load(userId);
    String dbCode = user.getFanCardNo();
    FanCardApply apply = applyMapper.selectByUserId(userId);
    if (apply == null || StringUtils.isBlank(dbCode)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "只有参与了双十一活动的用户可以领取哦~");
    }
    if (!StringUtils.equals(code, dbCode)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "您输入的兑换码与领取的不匹配，请重新输入");
    }
    FanCardApplyStatus status = apply.getStatus();
    if (FanCardApplyStatus.SUCCEED != status) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "您已经领取过积分，不要贪心哦");
    }
    List<Result> results = yundouAmountService
        .modifyAmount(YundouOperationType.FANCARD_EXCHANGE, userId);
    if (CollectionUtils.isEmpty(results)) {
      logger.error("用户 {} 兑换积分失败", user);
      return false;
    } else {
      logger.info("用户 {} 通过范卡code兑换了积分, 明细: {}", user, results);
    }
    apply.setStatus(FanCardApplyStatus.USED);
    return applyMapper.updateByPrimaryKeySelective(apply) > 0;
  }

  /**
   * 生成唯一兑换码
   *
   * @param tryTime 尝试次数，一般不会尝试两次
   * @return 生成的6位兑换码
   */
  private String createCode(int tryTime) {
    String fanCardNo = null;
    boolean isCodeExist = true;
    int triedTime = 0;
    while (triedTime < tryTime && isCodeExist) {
      fanCardNo = RandomStringGenerator
          .getRandomStringByLength(GenerateMode.CAPITAL_LETTERS, 6);
      triedTime += 1;
      isCodeExist = isCodeExist(fanCardNo);
    }
    return fanCardNo;
  }

  /**
   * 生成唯一的注册码
   *
   * @return 生成的注册码
   */
  private static String generateCode() {
    String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    StringBuilder salt = new StringBuilder();
    Random rnd = new Random();
    while (salt.length() < 6) { // 生成码长度.
      int index = (int) (rnd.nextFloat() * SALTCHARS.length());
      salt.append(SALTCHARS.charAt(index));
    }
    return salt.toString();
  }

  public static void main(String[] args) {
    System.out.println(FanCardServiceImpl.generateCode());
  }

}
