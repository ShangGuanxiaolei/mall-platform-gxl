package com.xquark.service.fancard;

import com.xquark.dal.model.FanCardApply;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

/**
 * Created by wangxinhua on 17-11-7. DESC:
 */
public interface FanCardService {

  /**
   * 申请注册码
   *
   * @param userId 用户id
   * @param apply 申请对象
   * @return 申请结果
   */
  boolean apply(String userId, FanCardApply apply);

  boolean save(FanCardApply apply);

  boolean update(FanCardApply apply);

  boolean delete(String id);

  FanCardApply load(String id);

  FanCardApply loadByUserId(String userId);

  List<FanCardApply> list(String order, Direction direction, Pageable page);

  boolean isCodeExist(String code);

  boolean exchange(String userId, String code);
}
