package com.xquark.service.store;

import com.xquark.dal.model.Store;
import com.xquark.dal.vo.StoreVO;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface StoreService extends BaseEntityService<Store> {

  Store load(String id);

  int insert(Store team);

  int deleteForArchive(String id);

  int update(Store record);

  /**
   * 服务端分页查询数据
   */
  List<StoreVO> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 根据code查询门店信息
   */
  Store selectByCode(String code);

}

