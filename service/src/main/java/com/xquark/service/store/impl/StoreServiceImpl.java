package com.xquark.service.store.impl;

import com.xquark.dal.mapper.StoreMapper;
import com.xquark.dal.model.Store;
import com.xquark.dal.vo.StoreVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.store.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("storeService")
public class StoreServiceImpl extends BaseServiceImpl implements StoreService {

  @Autowired
  StoreMapper storeMapper;

  @Override
  public int insert(Store team) {
    return storeMapper.insert(team);
  }

  @Override
  public int insertOrder(Store store) {
    return storeMapper.insert(store);
  }

  @Override
  public Store load(String id) {
    return storeMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return storeMapper.updateForArchive(id);
  }

  @Override
  public int update(Store record) {
    return storeMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<StoreVO> list(Pageable pager, Map<String, Object> params) {
    return storeMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return storeMapper.selectCnt(params);
  }

  /**
   * 根据code查询门店信息
   */
  @Override
  public Store selectByCode(String code) {
    return storeMapper.selectByCode(code);
  }

}
