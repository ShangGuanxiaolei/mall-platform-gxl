package com.xquark.service.store.impl;

import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.status.PartnerStatus;
import com.xquark.dal.status.ShopStatus;
import com.xquark.dal.status.StoreMemberType;
import com.xquark.dal.status.TwitterStatus;
import com.xquark.dal.type.CommissionType;
import com.xquark.dal.vo.StoreMemberVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.store.StoreMemberService;
import com.xquark.service.twitter.UserTwitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("storeMemberService")
public class StoreMemberServiceImpl extends BaseServiceImpl implements StoreMemberService {

  @Autowired
  StoreMemberMapper storeMemberMapper;

  @Autowired
  private UserTwitterMapper userTwitterMapper;

  @Autowired
  private UserMapper userMapper;

  @Autowired
  private ShopMapper shopMapper;

  @Autowired
  private ShopTreeMapper shopTreeMapper;

  @Autowired
  private UserTwitterService userTwitterService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private TeamShopCommissionMapper teamShopCommissionMapper;

  @Autowired
  private TeamMapper teamMapper;

  @Autowired
  private UserTeamMapper userTeamMapper;

  @Autowired
  private ShopService shopService;

  @Autowired
  private UserPartnerMapper userPartnerMapper;

  @Autowired
  private PartnerTypeRelationMapper partnerTypeRelationMapper;

  @Override
  public int insert(StoreMember team) {
    return storeMemberMapper.insert(team);
  }

  @Override
  public int insertOrder(StoreMember storeMember) {
    return storeMemberMapper.insert(storeMember);
  }

  @Override
  public StoreMember load(String id) {
    return storeMemberMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return storeMemberMapper.updateForArchive(id);
  }

  @Override
  public int update(StoreMember record) {
    return storeMemberMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<StoreMemberVO> list(Pageable pager, Map<String, Object> params) {
    return storeMemberMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return storeMemberMapper.selectCnt(params);
  }

  /**
   * 查询同一个门店审核通过的店长个数
   */
  @Override
  public StoreMember selectActiveLeader(String storeId) {
    return storeMemberMapper.selectActiveLeader(storeId);
  }

  /**
   * 根据门店信息生成推客数据
   */
  @Override
  public void insertTwitter(StoreMember member, StoreMember activeLeader, String rootShopId) {

    String userId = member.getUserId();
    UserTwitter twitter = userTwitterMapper.selectByUserIdAndShopId(userId, rootShopId);
    Shop rootShop = shopMapper.selectByPrimaryKey(rootShopId);
    String rootUserId = rootShop.getOwnerId();
    // 如果该用户没有推客信息，则审核门店用户时，同时生成对应推客信息
    if (twitter == null) {
      User user = userMapper.selectByPrimaryKey(userId);
      // 更新用户姓名，手机号
      User updateUser = new User();
      updateUser.setId(userId);
      updateUser.setName(member.getName());
      updateUser.setPhone(member.getPhone());
      userMapper.updateByPrimaryKeySelective(updateUser);

      // 同步更新shop姓名
      Shop existShop = shopMapper.selectByUserId(userId);
      if (existShop != null) {
        Shop shop = new Shop();
        shop.setName(member.getName());
        shop.setId(existShop.getId());
        shopMapper.updateByPrimaryKeySelective(shop);
      } else {
        Shop shop = new Shop();
        shop.setName(member.getName());
        shop.setOwnerId(userId);
        shopService.create(shop);
      }

      // 新增推客信息
      UserTwitter userTwitter = new UserTwitter();
      userTwitter.setStatus(TwitterStatus.ACTIVE);
      userTwitter.setUserId(user.getId());
      userTwitter.setOwnerShopId(rootShopId);
      String parentUserId = "";
      // 如果是店员，则上级推客默认为店长，如果是店长，上级推客默认为总店
      if (member.getType() == StoreMemberType.LEADER) {
        parentUserId = rootUserId;
      } else {
        parentUserId = activeLeader.getUserId();
      }
      userTwitter.setParentUserId(parentUserId);
      userTwitterService.create(userTwitter);

      // 然后同时变更shoptree里的上下级关系
      Shop shop = shopMapper.selectByUserId(userId);
      Shop parentShop = shopMapper.selectByUserId(parentUserId);
      shopTreeService.changeParent(rootShopId, parentShop.getId(), shop.getId());
    }

  }

  /**
   * 根据门店信息生成合伙人数据
   */
  @Override
  public void insertPartner(StoreMember member, String shopId) {
    String userId = member.getUserId();
    // 合伙人表插入数据
    UserPartner partner = new UserPartner();
    partner.setStatus(PartnerStatus.ACTIVE);
    partner.setUserId(userId);
    partner.setArchive(false);
    partner.setOwnerShopId(shopId);
    userPartnerMapper.insert(partner);

    // 默认合伙人类型为团队合伙人
    PartnerTypeRelation relation = new PartnerTypeRelation();
    relation.setUserId(userId);
    relation.setArchive(false);
    relation.setType(CommissionType.TEAM);
    relation.setShopId(shopId);
    partnerTypeRelationMapper.insert(relation);
  }

  /**
   * 根据门店信息生成战队数据
   */
  @Override
  public void insertTeam(StoreMember member, String rootShopId) {

    String userId = member.getUserId();
    Shop rootShop = shopMapper.selectByPrimaryKey(rootShopId);
    String rootUserId = rootShop.getOwnerId();
    // 如果是店长，则先生成战队数据，然后将自己插入战队队员数据
    if (member.getType() == StoreMemberType.LEADER) {
      TeamShopCommission group = teamShopCommissionMapper.selectDefaultByshopId(rootShopId);
      Team team = new Team();
      team.setShopId(rootShopId);
      team.setUserId(member.getUserId());
      team.setArchive(false);
      team.setName(member.getTeamName());
      team.setStatus(ShopStatus.ACTIVE);
      team.setGroupId(group.getId());
      team.setStoreId(member.getStoreId());
      teamMapper.insert(team);

      UserTeam userTeam = new UserTeam();
      userTeam.setTeamId(team.getId());
      userTeam.setArchive(false);
      userTeam.setStatus(ShopStatus.ACTIVE);
      userTeam.setUserId(member.getUserId());
      userTeam.setShopId(rootShopId);
      userTeamMapper.insert(userTeam);
    }
    // 如果是店员，只需要往userteam表中插入信息即可
    else {
      Team team = teamMapper.selectByStoreId(member.getStoreId());
      UserTeam userTeam = new UserTeam();
      userTeam.setTeamId(team.getId());
      userTeam.setArchive(false);
      userTeam.setStatus(ShopStatus.ACTIVE);
      userTeam.setUserId(member.getUserId());
      userTeam.setShopId(rootShopId);
      userTeamMapper.insert(userTeam);
    }

  }

  /**
   * 通过手机号查询店员信息
   */
  @Override
  public StoreMember selectByPhone(String phone) {
    return storeMemberMapper.selectByPhone(phone);
  }

  /**
   * 通过userId查询店员信息
   */
  @Override
  public StoreMember selectByUserId(String userId) {
    return storeMemberMapper.selectByUserId(userId);
  }

  /**
   * 查找某个门店下所有特约店员
   */
  @Override
  public List<StoreMember> selectSeniorByStoreId(String storeId) {
    return storeMemberMapper.selectSeniorByStoreId(storeId);
  }

  /**
   * 删除店员后，同时删除该用户信息
   */
  @Override
  public void deleteUserInfo(String userId) {
    userTeamMapper.deleteByUserId(userId);
    userPartnerMapper.deleteByUserId(userId);
    userTwitterMapper.deleteByUserId(userId);
    shopTreeMapper.deleteByUserId(userId);
    shopMapper.deleteByUserId(userId);
    userMapper.deleteByPrimaryKey(userId);
  }

  /**
   * 查找某个门店下所有店员
   */
  @Override
  public List<StoreMember> selectByStoreId(String storeId) {
    return storeMemberMapper.selectByStoreId(storeId);
  }

}
