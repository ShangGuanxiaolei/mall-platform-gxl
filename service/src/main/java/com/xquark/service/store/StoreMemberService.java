package com.xquark.service.store;

import com.xquark.dal.model.StoreMember;
import com.xquark.dal.vo.StoreMemberVO;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface StoreMemberService extends BaseEntityService<StoreMember> {

  StoreMember load(String id);

  int insert(StoreMember team);

  int deleteForArchive(String id);

  int update(StoreMember record);

  /**
   * 服务端分页查询数据
   */
  List<StoreMemberVO> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 查询同一个门店审核通过的店长个数
   */
  StoreMember selectActiveLeader(String storeId);

  /**
   * 根据门店信息生成推客数据
   */
  void insertTwitter(StoreMember member, StoreMember activeLeader, String shopId);

  /**
   * 根据门店信息生成合伙人数据
   */
  void insertPartner(StoreMember member, String shopId);

  /**
   * 根据门店信息生成战队数据
   */
  void insertTeam(StoreMember member, String shopId);

  /**
   * 通过手机号查询店员信息
   */
  StoreMember selectByPhone(String phone);

  /**
   * 通过userId查询店员信息
   */
  StoreMember selectByUserId(String userId);

  /**
   * 查找某个门店下所有特约店员
   */
  List<StoreMember> selectSeniorByStoreId(String storeId);

  /**
   * 删除店员后，同时删除该用户信息
   */
  void deleteUserInfo(String userId);

  /**
   * 查找某个门店下所有店员
   */
  List<StoreMember> selectByStoreId(String storeId);
}

