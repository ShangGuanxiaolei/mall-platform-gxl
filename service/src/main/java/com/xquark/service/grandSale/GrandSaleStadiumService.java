package com.xquark.service.grandSale;

import com.xquark.dal.model.GrandSaleStadium;
import com.xquark.dal.model.Product;
import com.xquark.dal.vo.GrandSaleStadiumVO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface GrandSaleStadiumService {
  int deleteByPrimaryKey(Integer id);

  int insert(GrandSaleStadium record);

  int insertSelective(GrandSaleStadium record);

  GrandSaleStadium selectByPrimaryKey(Integer id);

  List<GrandSaleStadiumVO> selectByGrandSaleId(Integer grandSaleId);

  List<GrandSaleStadium> selectAll();

  String selectTitleByType(String type);

  GrandSaleStadium selectOneByType(String type);

  List<Product> selectStadiumProduct(String stadiumType, String categoryId, Pageable pageable,String grandSaleId);

  Map<String, Object> selectBannerAndCategory(String stadiumType);

  String selectOneBanner(String stadiumType);

  String selectTargetUrl(String stadiumType);

  String selectShareUrl(String stadiumType);

  Map<String, Object> selectShareMessage(String stadiumType);

  int countProductsByCategoryIds(String objType, String categoryId);

  int updateByPrimaryKeySelective(GrandSaleStadium record);

  int updateByPrimaryKey(GrandSaleStadium record);

  int updateByJob();
}
