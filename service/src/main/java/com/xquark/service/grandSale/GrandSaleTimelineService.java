package com.xquark.service.grandSale;

import com.xquark.dal.model.GrandSaleTimeline;
import com.xquark.dal.vo.GrandSaleTimelineVO;

import java.util.List;

public interface GrandSaleTimelineService {
  int deleteByPrimaryKey(Integer id);

  int insert(GrandSaleTimeline record);

  int insertSelective(GrandSaleTimeline record);

  GrandSaleTimeline selectByPrimaryKey(Integer id);

  List<GrandSaleTimelineVO> selectByGrandSaleId(Integer grandSaleId);

  int updateByPrimaryKeySelective(GrandSaleTimeline record);

  int updateByPrimaryKey(GrandSaleTimeline record);
}
