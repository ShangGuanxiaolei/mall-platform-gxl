package com.xquark.service.grandSale.impl;

import com.xquark.dal.mapper.GrandSalePromotionMapper;
import com.xquark.dal.mapper.GrandSaleStadiumMapper;
import com.xquark.dal.mapper.GrandSaleStadiumTypeMapper;
import com.xquark.dal.mapper.PromotionWhitelistMapper;
import com.xquark.dal.model.GrandSalePromotion;
import com.xquark.dal.model.GrandSaleStadium;
import com.xquark.dal.model.GrandSaleStadiumType;
import com.xquark.dal.model.User;
import com.xquark.dal.vo.GrandSalePromotionVO;
import com.xquark.service.category.CategoryService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.grandSale.GrandSalePromotionService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static java.net.URLEncoder.encode;

@Service
public class GrandSalePromotionServiceImpl implements GrandSalePromotionService {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private GrandSalePromotionMapper grandSalePromotionMapper;

  @Autowired
  private GrandSaleStadiumTypeMapper grandSaleStadiumTypeMapper;

  @Autowired
  private GrandSaleStadiumMapper grandSaleStadiumMapper;

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private PromotionWhitelistMapper promotionWhitelistMapper;

  @Override
  public int deleteByPrimaryKey(Integer id) {
    return grandSalePromotionMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(GrandSalePromotion record) {
    return grandSalePromotionMapper.insert(record);
  }

  @Override
  public int insertSelective(GrandSalePromotion record) {
    return grandSalePromotionMapper.insertSelective(record);
  }

  @Override
  public GrandSalePromotion selectByPrimaryKey(Integer id) {
    return grandSalePromotionMapper.selectByPrimaryKey(id);
  }

  @Override
  public GrandSalePromotionVO selectGrandSale(User user) {
    boolean isOpen = promotionWhitelistMapper.checkIsWhiteExist("grandsale", 9999999L);
    boolean inWhiteList = promotionWhitelistMapper.checkIsWhiteExist("grandsale", user.getCpId());
    if (isOpen) {//白名单开启
      if (!inWhiteList) {//用户不在白名单内
        return null;
      }
    }

    GrandSalePromotion grandSalePromotion = grandSalePromotionMapper.selectGrandSale();
    if (grandSalePromotion == null) {
      return null;
    }
    GrandSalePromotionVO grandSalePromotionVO = new GrandSalePromotionVO();
    BeanUtils.copyProperties(grandSalePromotion, grandSalePromotionVO);
    Date beginTime = grandSalePromotion.getBeginTime();
    Date endTime = grandSalePromotion.getEndTime();
    if (null == beginTime || null == endTime) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动时间不能为空");
    }
    GrandSaleStadiumType grandSaleStadiumType = grandSaleStadiumTypeMapper.selectByType("MAIN");
    String path = encode(grandSaleStadiumType.getPath());
    //设置主会场跳转地址
    String targetUrl = "hanwei://meetingPlace?validAt=" + beginTime.getTime() +
            "&expiredAt=" +
            endTime.getTime() +
            "&url="
            + path;
    grandSalePromotionVO.setTargetUrl(targetUrl);
    return grandSalePromotionVO;
  }

  @Override
  public GrandSalePromotionVO selectGrandSaleForAdmin() {
    GrandSalePromotion grandSalePromotion = grandSalePromotionMapper.selectGrandSale();
    if (grandSalePromotion == null) {
      return null;
    }
    GrandSalePromotionVO grandSalePromotionVO = new GrandSalePromotionVO();
    BeanUtils.copyProperties(grandSalePromotion, grandSalePromotionVO);
    Date beginTime = grandSalePromotion.getBeginTime();
    Date endTime = grandSalePromotion.getEndTime();
    if (null == beginTime || null == endTime) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动时间不能为空");
    }
    GrandSaleStadiumType grandSaleStadiumType = grandSaleStadiumTypeMapper.selectByType("MAIN");
    String path = encode(grandSaleStadiumType.getPath());
    //设置主会场跳转地址
    String targetUrl = "hanwei://meetingPlace?validAt=" + beginTime.getTime() +
            "&expiredAt=" +
            endTime.getTime() +
            "&url="
            + path;
    grandSalePromotionVO.setTargetUrl(targetUrl);
    return grandSalePromotionVO;
  }

  @Override
  public int updateByPrimaryKeySelective(GrandSalePromotion record) {
    return grandSalePromotionMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(GrandSalePromotion record) {
    return grandSalePromotionMapper.updateByPrimaryKey(record);
  }

  @Override
  public int updateStatusByJob() {
    int result = 0;
    try {
      //自动结束活动
      List<GrandSalePromotion> grandSalePromotionList = grandSalePromotionMapper.selectNeedEnded();
      for (GrandSalePromotion grandSalePromotion:
          grandSalePromotionList) {
        Integer id = grandSalePromotion.getId();
        List<GrandSaleStadium> grandSaleStadiums = this.selectAllByGrandSaleId(id);
        for (GrandSaleStadium grandSaleStadium:
            grandSaleStadiums) {
          Integer stadiumId = grandSaleStadium.getId();
          if (stadiumId == null) {
            log.error("分会场id为空");
          }
          if (!(deleteStadium(stadiumId) > 0)) {
            log.error("删除分会场失败,stadiumId:{}", stadiumId);
          }
        }
        grandSalePromotionMapper.updateStatusByJob(2, id);
        result++;
      }
      //当前存在已开始活动，则直接返回
      GrandSalePromotion promotion = grandSalePromotionMapper.selectGrandSale();
      if (promotion != null) {
        return 0;
      }
      //自动开始活动
      GrandSalePromotion grandSalePromotion = grandSalePromotionMapper.selectNotStated();
      if (grandSalePromotion != null) {
        //活动已开始
        return grandSalePromotionMapper.updateStatusByJob(1, grandSalePromotion.getId());
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return result;
  }

  private int deleteStadium(Integer id) {
    //删除分会场的商品分类，商品及sku暂不删除，避免其他地方使用
    GrandSaleStadium grandSaleStadium = grandSaleStadiumMapper.selectOne(id);
    if (grandSaleStadium == null) {
      log.error("会场信息不存在");
      return 0;
    }
    String type = grandSaleStadium.getType();
    if (StringUtils.isBlank(type)) {
      log.error("会场类型为空");
      return 0;
    }
    if (!(categoryService.deleteCategory(type) > 0)) {
      log.error("删除分类失败");
      return 0;
    }
    return grandSaleStadiumMapper.deleteByPrimaryKey(id);
  }

  private List<GrandSaleStadium> selectAllByGrandSaleId(Integer grandSaleId) {
    List<GrandSaleStadium> grandSaleStadiums = grandSaleStadiumMapper.selectAllByGrandSaleId(grandSaleId);
    for (GrandSaleStadium grandSaleStadium :
        grandSaleStadiums) {
      String bannerUrl = grandSaleStadium.getBannerUrl();
      if (StringUtils.isNotBlank(bannerUrl)) {
        String[] banner = bannerUrl.split(",");
        grandSaleStadium.setBannerUrl(banner[0]);
      }
    }
    return grandSaleStadiums;
  }

  @Override
  public boolean isInSaleTime(Date date) {
      return grandSalePromotionMapper.selectExistsOpeningSale(date);
  }
}
