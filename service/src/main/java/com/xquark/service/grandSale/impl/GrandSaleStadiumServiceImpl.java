package com.xquark.service.grandSale.impl;

import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.vo.CategoryVO;
import com.xquark.dal.vo.GrandSaleStadiumVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.grandSale.GrandSaleStadiumService;
import com.xquark.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.thymeleaf.standard.expression.Each;

import java.util.*;

@Service
public class GrandSaleStadiumServiceImpl extends BaseServiceImpl implements GrandSaleStadiumService {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final static String SHARE_TYPE = "grandSale";

  private final static String STADIUM_TYPE = "MAIN";

  @Autowired
  private GrandSaleStadiumMapper grandSaleStadiumMapper;

  @Autowired
  private GrandSalePromotionMapper grandSalePromotionMapper;

  @Autowired
  private TermRelationshipMapper termRelationshipMapper;

  @Autowired
  private ProductMapper productMapper;

  @Autowired
  private CategoryMapper categoryMapper;

  @Autowired
  private GrandSaleStadiumTypeMapper grandSaleStadiumTypeMapper;

  @Autowired
  private GrandSaleKvMapper grandSaleKvMapper;

  @Autowired
  private GrandSaleShareMapper grandSaleShareMapper;

  @Override
  public int deleteByPrimaryKey(Integer id) {
    return grandSaleStadiumMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(GrandSaleStadium record) {
    return grandSaleStadiumMapper.insert(record);
  }

  @Override
  public int insertSelective(GrandSaleStadium record) {
    return grandSaleStadiumMapper.insertSelective(record);
  }

  @Override
  public GrandSaleStadium selectByPrimaryKey(Integer id) {
    return grandSaleStadiumMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<GrandSaleStadiumVO> selectByGrandSaleId(Integer grandSaleId) {
    List<GrandSaleStadium> grandSaleStadiums = grandSaleStadiumMapper.selectByGrandSaleId(grandSaleId);
    List<GrandSaleStadiumVO> grandSaleStadiumVOList = new ArrayList<>();
    Date date = new Date();
    for (GrandSaleStadium grandSaleStadium:
         grandSaleStadiums) {
      GrandSaleStadiumVO grandSaleStadiumVO = new GrandSaleStadiumVO();
      BeanUtils.copyProperties(grandSaleStadium, grandSaleStadiumVO);
      Date beginTime = grandSaleStadium.getBeginTime();
      Date endTime = grandSaleStadium.getEndTime();
      if (beginTime != null && endTime != null && DateUtils.isEffectiveDate(date, beginTime, endTime)) {
        grandSaleStadiumVO.setOpen(true);
      }
      grandSaleStadiumVOList.add(grandSaleStadiumVO);
    }
    if (grandSaleStadiumVOList.isEmpty()) {
      return null;
    }
    return grandSaleStadiumVOList;
  }

  @Override
  public List<GrandSaleStadium> selectAll() {
    List<GrandSaleStadium> grandSaleStadiums = grandSaleStadiumMapper.selectAll();
    for (GrandSaleStadium grandSaleStadium:
         grandSaleStadiums) {
      GrandSaleStadiumType grandSaleStadiumType = grandSaleStadiumTypeMapper.selectByType(grandSaleStadium.getType());
      grandSaleStadium.setPath(grandSaleStadiumType.getPath());//设置分会场path路径
    }
    GrandSalePromotion grandSalePromotion = grandSalePromotionMapper.selectGrandSale();
    GrandSaleStadium grandSaleStadium = null;
    if (grandSalePromotion != null) {
      grandSaleStadium = new GrandSaleStadium();
      BeanUtils.copyProperties(grandSalePromotion, grandSaleStadium);
      grandSaleStadium.setTitle(grandSalePromotion.getName());
      GrandSaleStadiumType grandSaleStadiumType = grandSaleStadiumTypeMapper.selectByType("MAIN");
      grandSaleStadium.setPath(grandSaleStadiumType.getPath());//设置主会场的path路径
    }
    grandSaleStadiums.add(grandSaleStadium);
    return grandSaleStadiums;
  }

  @Override
  public String selectTitleByType(String type) {
    return grandSaleStadiumMapper.selectTitleByType(type);
  }

  @Override
  public GrandSaleStadium selectOneByType(String type) {
    return grandSaleStadiumMapper.selectOneByType(type);
  }

  @Override
  public List<Product> selectStadiumProduct(String stadiumType, String categoryId, Pageable pageable,String grandSaleId) {
      List<Product> products = productMapper.listProductsByCategoryIds(stadiumType, categoryId, pageable);
      List<GrandSaleStadiumVO> grandSaleStadiumVOList = this.selectByGrandSaleId(Integer.valueOf(grandSaleId));
      GrandSaleStadiumVO grandSaleStadium  = new GrandSaleStadiumVO();
      for (GrandSaleStadiumVO grandSaleStadiumVO : grandSaleStadiumVOList) {
          if(stadiumType.equals(grandSaleStadiumVO.getType())){
              grandSaleStadium= grandSaleStadiumVO;
          }
      }
      if( null!= grandSaleStadium){
          for (Product product : products) {
              product.setStadiumOpen(grandSaleStadium.isOpen());
          }
      }
      return products;
  }

  @Override
  public Map<String, Object> selectBannerAndCategory(String stadiumType) {
    Map bannerAndGrandSaleId = grandSaleStadiumMapper.selectBannerByType(stadiumType);
    if (bannerAndGrandSaleId == null || bannerAndGrandSaleId.isEmpty()) {
      log.error("根据会场类型查询数据失败，stadiumType:{}", stadiumType);
      return null;
    }
    Integer grandSaleId = (Integer) bannerAndGrandSaleId.get("grand_sale_id");
    String banner = (String) bannerAndGrandSaleId.get("banner_url");
    if (StringUtils.isBlank(banner)) {
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "分会场类型没有配置");
    }
    String[] bannerList = banner.split(",");
    List<String> categoryIds = termRelationshipMapper.listCategoryId(stadiumType, grandSaleId);
    List<CategoryVO> categoryList = null;
    if (!categoryIds.isEmpty()) {
      categoryList = categoryMapper.selectCategoryByIds(categoryIds);
    }
    Map<String, Object> map = new HashMap<>();
    map.put("bannerList", bannerList);
    map.put("categoryList", categoryList);
    return map;
  }

  @Override
  public String selectOneBanner(String stadiumType) {
    Map bannerAndGrandSaleId = grandSaleStadiumMapper.selectBannerByType(stadiumType);
    if (bannerAndGrandSaleId == null || bannerAndGrandSaleId.isEmpty()) {
      log.error("根据会场类型查询数据失败，stadiumType:{}", stadiumType);
      return null;
    }
    String banner = (String) bannerAndGrandSaleId.get("banner_url");
    if (StringUtils.isBlank(banner)) {
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "分会场类型没有配置");
    }
    String[] bannerList = banner.split(",");
    return bannerList[0];
  }

  @Override
  public String selectTargetUrl(String stadiumType) {
    GrandSaleStadiumType grandSaleStadiumType = grandSaleStadiumTypeMapper.selectByType(stadiumType);
    if (grandSaleStadiumType == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "会场类型不正确");
    }
    return grandSaleStadiumType.getPath();
  }

  @Override
  public String selectShareUrl(String stadiumType) {
    GrandSaleStadiumType grandSaleStadiumType = grandSaleStadiumTypeMapper.selectByType(stadiumType);
    if (grandSaleStadiumType == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "会场类型不正确");
    }
    return grandSaleStadiumType.getShareUrl();
  }

  @Override
  public Map<String, Object> selectShareMessage(String stadiumType) {
    Map<String, Object> map = new HashMap<>();
    String image;
    if (STADIUM_TYPE.equals(stadiumType)) {
      image = grandSaleKvMapper.selectMainKV();
    } else {
      image = this.selectOneBanner(stadiumType);
    }
    final User user = getCurrentUser();
    String targetUrl = String.format("%s?fromCpId=%d", this.selectShareUrl(stadiumType), user.getCpId());
    GrandSaleShare grandSaleShare =
        Optional.ofNullable(grandSaleShareMapper.selectByType(stadiumType))
            .orElse(new GrandSaleShare("已经卖爆了! 上半年最热销的100件夏日护肤"));
    String content = grandSaleShare.getContent();
    map.put("sharePosterImage", image);
    map.put("meetingPlaceUrl", targetUrl);
    map.put("content", content);
    return map;
  }

  @Override
  public int countProductsByCategoryIds(String stadiumType, String categoryId) {
    return productMapper.countProductsByCategoryIds(stadiumType, categoryId);
  }

  @Override
  public int updateByPrimaryKeySelective(GrandSaleStadium record) {
    return grandSaleStadiumMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(GrandSaleStadium record) {
    return grandSaleStadiumMapper.updateByPrimaryKey(record);
  }

  @Override
  public int updateByJob() {
    int result = 0;
    int i = 0;
    try {
      i = grandSaleStadiumMapper.updateByJob(null);
      List<GrandSalePromotion> grandSalePromotions = grandSalePromotionMapper.selectNeedEnded();
      for (GrandSalePromotion grandSalePromotion:
              grandSalePromotions) {
        Integer grandSaleId = grandSalePromotion.getId();
        grandSaleStadiumMapper.updateByJob(grandSaleId);
        result++;
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return result + i;
  }
}
