package com.xquark.service.grandSale;

import com.xquark.dal.model.GrandSalePromotion;
import com.xquark.dal.model.User;
import com.xquark.dal.vo.GrandSalePromotionVO;

import java.util.Date;

public interface GrandSalePromotionService {
  int deleteByPrimaryKey(Integer id);

  int insert(GrandSalePromotion record);

  int insertSelective(GrandSalePromotion record);

  GrandSalePromotion selectByPrimaryKey(Integer id);

  GrandSalePromotionVO selectGrandSale(User user);

  GrandSalePromotionVO selectGrandSaleForAdmin();

  int updateByPrimaryKeySelective(GrandSalePromotion record);

  int updateByPrimaryKey(GrandSalePromotion record);

  int updateStatusByJob();

  boolean isInSaleTime(Date date);
}
