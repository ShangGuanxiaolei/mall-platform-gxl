package com.xquark.service.grandSale.impl;

import com.xquark.dal.mapper.GrandSaleTimelineMapper;
import com.xquark.dal.model.GrandSaleTimeline;
import com.xquark.dal.vo.GrandSaleTimelineVO;
import com.xquark.service.grandSale.GrandSaleTimelineService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class GrandSaleTimelineServiceImpl implements GrandSaleTimelineService {

  @Autowired
  private GrandSaleTimelineMapper grandSaleTimelineMapper;

  @Override
  public int deleteByPrimaryKey(Integer id) {
    return grandSaleTimelineMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(GrandSaleTimeline record) {
    return grandSaleTimelineMapper.insert(record);
  }

  @Override
  public int insertSelective(GrandSaleTimeline record) {
    return grandSaleTimelineMapper.insertSelective(record);
  }

  @Override
  public GrandSaleTimeline selectByPrimaryKey(Integer id) {
    return grandSaleTimelineMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<GrandSaleTimelineVO> selectByGrandSaleId(Integer grandSaleId) {
    List<GrandSaleTimeline> grandSaleTimeLines = grandSaleTimelineMapper.selectByGrandSaleId(grandSaleId);
    List<GrandSaleTimelineVO> grandSaleTimelineVOList = new ArrayList<>();
    for (GrandSaleTimeline grandSaleTimeline:
            grandSaleTimeLines) {
      Date timeNode = grandSaleTimeline.getTimeNode();
      GrandSaleTimelineVO grandSaleTimelineVO = new GrandSaleTimelineVO();
      BeanUtils.copyProperties(grandSaleTimeline, grandSaleTimelineVO);
      long currentTimeMillis = System.currentTimeMillis();
      if (timeNode != null && currentTimeMillis >= timeNode.getTime()) {
        grandSaleTimelineVO.setTimeNodeLight(true);//设置前端节点点亮
      }
      grandSaleTimelineVOList.add(grandSaleTimelineVO);
    }
    if (grandSaleTimelineVOList.size() == 0) {
      return null;
    }
    return grandSaleTimelineVOList;
  }

  @Override
  public int updateByPrimaryKeySelective(GrandSaleTimeline record) {
    return grandSaleTimelineMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(GrandSaleTimeline record) {
    return grandSaleTimelineMapper.updateByPrimaryKey(record);
  }
}
