package com.xquark.service.grandSale;

import com.xquark.dal.model.GrandSaleKv;
import com.xquark.dal.vo.GrandSaleKvVO;

import java.util.List;

public interface GrandSaleKvService {
  int deleteByPrimaryKey(Integer id);

  int insert(GrandSaleKv record);

  int insertSelective(GrandSaleKv record);

  GrandSaleKv selectByPrimaryKey(Integer id);

  List<GrandSaleKvVO> selectByGrandSaleId(Integer grandSaleId);

  String selectMainKV();

  int updateByPrimaryKeySelective(GrandSaleKv record);

  int updateByPrimaryKey(GrandSaleKv record);
}
