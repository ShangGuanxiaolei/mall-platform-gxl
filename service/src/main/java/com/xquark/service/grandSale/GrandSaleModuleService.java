package com.xquark.service.grandSale;

import com.xquark.dal.model.GrandSaleModule;
import com.xquark.dal.type.PromotionType;

import java.util.List;
import java.util.Map;

public interface GrandSaleModuleService {
  int deleteByPrimaryKey(Integer id);

  int insert(GrandSaleModule record);

  int insertSelective(GrandSaleModule record);

  GrandSaleModule selectByPrimaryKey(Integer id);

  Map<Object, Object> selectByGrandSaleId(Integer grandSaleId);

  List<GrandSaleModule> selectByGrandSaleIdAndPromotionType(Integer grandSaleId, PromotionType promotionType);

  int updateByPrimaryKeySelective(GrandSaleModule record);

  int updateByPrimaryKey(GrandSaleModule record);

  int updateModuleStatus();

}
