package com.xquark.service.grandSale.impl;

import com.xquark.dal.mapper.GrandSaleModuleMapper;
import com.xquark.dal.model.GrandSaleModule;
import com.xquark.dal.model.User;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.GrandSaleModuleVO;
import com.xquark.dal.vo.PromotionModuleVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.grandSale.GrandSaleModuleService;
import com.xquark.service.packetRain.PacketRainService;
import com.xquark.service.promotion.PromotionWhiteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GrandSaleModuleServiceImpl extends BaseServiceImpl implements GrandSaleModuleService {

  @Autowired
  private GrandSaleModuleMapper grandSaleModuleMapper;

  @Autowired
  private PromotionWhiteService promotionWhiteService;

  @Autowired
  private PacketRainService packetRainService;

  private final static String PIECE_URL = "/v2/promotionGoods/selectBeginningPromotionGoods2";

  private final static String FLASH_URL = "/v2/flashSale/listGroup";

  private final static String RESERVE_URL = "/v2/reserve/activity";

  private final static String PRODUCT_SALES_URL = "/v2/productSales/selectGrandSalesProducts";

  private Logger logger = LoggerFactory.getLogger(GrandSaleModuleServiceImpl.class);

  @Override
  public int deleteByPrimaryKey(Integer id) {
    return grandSaleModuleMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(GrandSaleModule record) {
    return grandSaleModuleMapper.insert(record);
  }

  @Override
  public int insertSelective(GrandSaleModule record) {
    return grandSaleModuleMapper.insertSelective(record);
  }

  @Override
  public GrandSaleModule selectByPrimaryKey(Integer id) {
    return grandSaleModuleMapper.selectByPrimaryKey(id);
  }

  @Override
  public Map<Object, Object> selectByGrandSaleId(Integer grandSaleId) {
    List<GrandSaleModule> grandSaleModules = grandSaleModuleMapper.selectByGrandSaleId(grandSaleId);
    if (null == grandSaleModules) {
      return Collections.emptyMap();
    }
    PromotionType key;
    List<GrandSaleModuleVO> listTemp;
    //使用LinkedHashMap默认就是排序完成的数据结构
    Map<PromotionType, List<GrandSaleModuleVO>> map = new LinkedHashMap<>();
    for (GrandSaleModule grandSaleModule:
            grandSaleModules) {

      if (PromotionType.PACKET_RAIN == grandSaleModule.getPromotionType()
              && !packetRainService.hasOnGoingActivityRain(grandSaleModule.getPromotionId())) {
        // 没有激活的红包雨配置则调过红包雨设置
        continue;
      }

      if (PromotionType.LOTTERY_DRAW == grandSaleModule.getPromotionType()
              && !packetRainService.hasOnGoingActivityLottery(grandSaleModule.getPromotionId())) {
        continue;
      }

      GrandSaleModuleVO grandSaleModuleVO = new GrandSaleModuleVO();
      BeanUtils.copyProperties(grandSaleModule, grandSaleModuleVO);
      //预售活动特殊处理活动id
      if (PromotionType.RESERVE.compareTo(grandSaleModuleVO.getPromotionType()) == 0) {
        String promotionId = grandSaleModuleVO.getPromotionId();
        if (isNumeric(promotionId)) {
          grandSaleModuleVO.setPromotionId(IdTypeHandler.encode(Long.valueOf(promotionId)));
        }
      }
      key = grandSaleModuleVO.getPromotionType();
      //在对象中加入请求接口地址字段
      setTargetPath(key, grandSaleModuleVO);
      listTemp = map.computeIfAbsent(key, k -> new ArrayList<>());
      listTemp.add(grandSaleModuleVO);
    }
    final User currUser = getCurrentUser();
    //键不存在，则存入null值
    // 红包雨、抽奖增加白名单校验
    final boolean havePacketRule = promotionWhiteService.havePacketRule(currUser.getCpId());
    final boolean haveLotteryRule = promotionWhiteService.haveLotteryRule(currUser.getCpId());
    if (!map.containsKey(PromotionType.PACKET_RAIN) || !havePacketRule) {
      map.put(PromotionType.PACKET_RAIN, null);
    }
    if (!map.containsKey(PromotionType.LOTTERY_DRAW) || !haveLotteryRule) {
      map.put(PromotionType.LOTTERY_DRAW, null);
    }
    if (!map.containsKey(PromotionType.PIECE)) map.put(PromotionType.PIECE, null);
    if (!map.containsKey(PromotionType.FLASHSALE)) map.put(PromotionType.FLASHSALE, null);
    if (!map.containsKey(PromotionType.PRODUCT_SALES)) map.put(PromotionType.PRODUCT_SALES, null);
    if (!map.containsKey(PromotionType.BARGAIN)) map.put(PromotionType.BARGAIN, null);
    if (!map.containsKey(PromotionType.RESERVE)) map.put(PromotionType.RESERVE, null);
    return new LinkedHashMap<>(map);
  }

  @Override
  public List<GrandSaleModule> selectByGrandSaleIdAndPromotionType(Integer grandSaleId, PromotionType promotionType) {
    return grandSaleModuleMapper.selectByGrandSaleIdAndPromotionType(grandSaleId, promotionType);
  }

  @Override
  public int updateByPrimaryKeySelective(GrandSaleModule record) {
    return grandSaleModuleMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(GrandSaleModule record) {
    return grandSaleModuleMapper.updateByPrimaryKey(record);
  }

  @Override
  public int updateModuleStatus() {
    int result = 0;
    try {
      //查询出所有状态为进行中的活动模块
      List<GrandSaleModule> grandSaleModules = grandSaleModuleMapper.selectAll();
      List<String> promotionIds = new ArrayList<>();
      Set<PromotionType> promotionTypeList = new HashSet<>();
      for (GrandSaleModule grandSaleModule :
          grandSaleModules) {
        String promotionId = grandSaleModule.getPromotionId();
        PromotionType promotionType = grandSaleModule.getPromotionType();
        promotionIds.add(promotionId);
        promotionTypeList.add(promotionType);
      }
      for (PromotionType promotionType :
          promotionTypeList) {
        //根据活动类型查询出有效的活动
        List<PromotionModuleVO> promotionModuleVOS = selectEffectivePromotion(promotionType);
        for (PromotionModuleVO promotionModuleVO :
            promotionModuleVOS) {
          String promotionId = promotionModuleVO.getPromotionId();
          boolean contains = promotionIds.contains(promotionId);
          //活动已失效,更新活动模块状态
          if (!contains) {
            if (grandSaleModuleMapper.updateStatusByPromotionId(promotionId, 2) > 0) {
              logger.info("自动失效主会场活动模块成功,{},{}", promotionId, promotionType);
              result++;
            }
          }
        }
      }
    } catch (Exception e) {
      logger.error("更新活动模块状态失败", e.getMessage(), e);
    }
    return result;
  }

  //*******私有方法专区********//
  private void setTargetPath(PromotionType key, GrandSaleModuleVO grandSaleModuleVO) {
    if (PromotionType.FLASHSALE.compareTo(key) == 0) {
      grandSaleModuleVO.setTargetUrl(FLASH_URL);
    } else if (PromotionType.isPiece(key)) {
      grandSaleModuleVO.setTargetUrl(PIECE_URL);
    } else if (PromotionType.RESERVE.compareTo(key) == 0) {
      grandSaleModuleVO.setTargetUrl(RESERVE_URL);
    } else if (PromotionType.BARGAIN.compareTo(key) == 0) {
      //FIXME 砍价活动接口路径尚未拿到
      grandSaleModuleVO.setTargetUrl("");
    } else if (PromotionType.PRODUCT_SALES.compareTo(key) == 0) {
      grandSaleModuleVO.setTargetUrl(PRODUCT_SALES_URL);
    }
  }

  /**
   * 判断字符串是否是数字
   * @param str 字符串
   * @return 判断结果
   */
  private static boolean isNumeric(String str) {
    for (int i = 0; i < str.length(); i++) {
      if (!Character.isDigit(str.charAt(i))) {
        return false;
      }
    }
    return true;
  }

  private List<PromotionModuleVO> selectEffectivePromotion(PromotionType promotionType) {
    List<PromotionModuleVO> promotionVOS;
    switch (promotionType) {
      case PIECE:
        promotionVOS = grandSaleModuleMapper.selectEffectivePiece();
        break;
      case FLASHSALE:
        promotionVOS = grandSaleModuleMapper.selectEffectiveFlash();
        break;
      case RESERVE:
        promotionVOS = grandSaleModuleMapper.selectEffectiveReserve();
        break;
      case PRODUCT_SALES:
        promotionVOS = grandSaleModuleMapper.selectEffectiveSales();
        break;
      default:
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动类型不正确");
    }
    return promotionVOS;
  }

}
