package com.xquark.service.grandSale.impl;

import com.xquark.dal.mapper.GrandSaleKvMapper;
import com.xquark.dal.model.GrandSaleKv;
import com.xquark.dal.vo.GrandSaleKvVO;
import com.xquark.service.grandSale.GrandSaleKvService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GrandSaleKvServiceImpl implements GrandSaleKvService {

  @Autowired
  private GrandSaleKvMapper grandSaleKvMapper;

  @Override
  public int deleteByPrimaryKey(Integer id) {
    return grandSaleKvMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int insert(GrandSaleKv record) {
    return grandSaleKvMapper.insert(record);
  }

  @Override
  public int insertSelective(GrandSaleKv record) {
    return grandSaleKvMapper.insertSelective(record);
  }

  @Override
  public GrandSaleKv selectByPrimaryKey(Integer id) {
    return grandSaleKvMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<GrandSaleKvVO> selectByGrandSaleId(Integer grandSaleId) {
    List<GrandSaleKv> grandSaleKvs = grandSaleKvMapper.selectByGrandSaleId(grandSaleId);
    List<GrandSaleKvVO> grandSaleKvVOList = new ArrayList<>();
    for (GrandSaleKv grandSaleKv:
         grandSaleKvs) {
      GrandSaleKvVO grandSaleKvVO = new GrandSaleKvVO();
      BeanUtils.copyProperties(grandSaleKv, grandSaleKvVO);
      grandSaleKvVOList.add(grandSaleKvVO);
    }
    if (grandSaleKvVOList.size() == 0) {
      return null;
    }
    return grandSaleKvVOList;
  }

  @Override
  public String selectMainKV() {
    return grandSaleKvMapper.selectMainKV();
  }

  @Override
  public int updateByPrimaryKeySelective(GrandSaleKv record) {
    return grandSaleKvMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(GrandSaleKv record) {
    return grandSaleKvMapper.updateByPrimaryKey(record);
  }
}
