package com.xquark.service.logistics;

import com.xquark.service.vo.LogisticsStock;
import com.xquark.service.vo.LogisticsStockParam;
import com.xquark.service.vo.SFOrderCheck;
import com.xquark.service.vo.SFOrderLog;
import com.xquark.service.vo.ShippingGoods;
import com.xquark.service.vo.ShippingGoodsAll;
import com.xquark.service.vo.ShippingGoodsAllParam;
import com.xquark.service.vo.SubOrder;
import java.util.List;
import java.util.Map;

/**
 * User: kong Date: 2018/7/7. Time: 21:55 顺丰物流查询
 */
public interface LogisticsGoodsService {

  List<LogisticsStock> getStock(List<LogisticsStockParam> params);

  ShippingGoodsAll getShippingFee(ShippingGoodsAllParam shippingGoodsAllParam);

  List<SubOrder> findSubOrder(String orderSn);

  SFOrderCheck getOrderDetail(String orderSn);

  List<SFOrderLog> getSFOrderLog(String orderSn, String billSn);
  //查询第三方物流信息
  List<SFOrderLog> getThirdLogistics(String waybillNo,String waybillType);

  Map getbillNoByOrderNo(String orderNo);
}