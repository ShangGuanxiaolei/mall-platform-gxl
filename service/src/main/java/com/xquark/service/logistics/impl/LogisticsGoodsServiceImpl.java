package com.xquark.service.logistics.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.consts.SFApiConstants;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.model.SfAppConfig;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.logistics.LogisticsGoodsService;
import com.xquark.service.product.SFProductService;
import com.xquark.service.vo.*;
import com.xquark.utils.HttpUtils;
import com.xquark.utils.StringUtil;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * User: kong Date: 2018/7/7. Time: 21:56 顺丰物流查询
 */
@Service
@Transactional
public class LogisticsGoodsServiceImpl extends BaseServiceImpl implements LogisticsGoodsService {
  @Autowired
  private OrderMapper orderMapper;

  @Autowired
  private SFProductService sfProductService;

  @Value("${sf.api.token.profile}")
  private String profile;

  @Value("${sf.api.host.url}")
  private String host;

  @Value("${aliyun.logistics.host.url}")
  private String thirdLogisticUrl;

  @Value("${aliyun.logistics.appcode}")
  private String thirdLogisticAppCode;

  private Logger log = LoggerFactory.getLogger(getClass());

  @Override
  public List<LogisticsStock> getStock(List<LogisticsStockParam> params) {
    if (CollectionUtils.isEmpty(params)) {
      return Collections.emptyList();
    }
    List<LogisticsStock> logisticsStocks = new ArrayList<LogisticsStock>();
    String err = "";
    String stockParam = "";
    String value = "";
    SfAppConfig appConfig = sfProductService.loadConfig();
    String url = String.format("%s?app_key=%s&access_token=%s&timestamp=%s",
        host + SFApiConstants.STOCK_INFO, appConfig.getClientId(),
        appConfig.getAccessToken(),
        String.valueOf(new Date().getTime()));
    try {
      JSONObject jsonObject = new JSONObject();
      JSONArray jsonArray = new JSONArray();
      jsonObject.put("invokeSource", 0);
      jsonObject.put("stockInfoRegionParamList", jsonArray);
      err = "数据封装出现错误!";
      for (int i = 0; i < params.size(); i++) {
        JSONObject jsonValue = new JSONObject();
        LogisticsStockParam logisticsStockParam = params.get(i);
        jsonValue.put("productId", logisticsStockParam.getProductid());
        jsonValue.put("productNum", logisticsStockParam.getProductNum());
        jsonValue.put("regionId", logisticsStockParam.getRegionId());
        jsonValue.put("sfairline", logisticsStockParam.getSfairline());
        jsonValue.put("sfshipping", logisticsStockParam.getSfshipping());
        jsonValue.put("bomSn", logisticsStockParam.getBomSn());
        jsonArray.add(i, jsonValue);
      }
      err = "发送请求时发生错误！";
      stockParam = jsonObject.toString();
      HttpInvokeResult res = PoolingHttpClients.postJSON(url, jsonObject);
      err = "解析数据时出现错误！";
      value = res.getContent();
      jsonObject = JSONObject.parseObject(value);
      jsonArray = jsonObject.getJSONObject("data").getJSONArray("stockInfoList");
      for (int i = 0; i < jsonArray.size(); i++) {
        LogisticsStock logisticsStock = new LogisticsStock();
        JSONObject jsonValue = jsonArray.getJSONObject(i);
        logisticsStock.setProductId(jsonValue.getLong("productId"));
        logisticsStock.setSaleNum(jsonValue.getInteger("saleNumber"));
        logisticsStock.setShipTime(jsonValue.getInteger("shipTime"));
        logisticsStock.setStockStatus(jsonValue.getInteger("stockStatus"));
        logisticsStock.setWarehouseId(jsonValue.getInteger("warehouseId"));
        logisticsStock.setMaxSaleNum(jsonValue.getInteger("maxSaleNumber"));
        logisticsStock.setRegionId(jsonValue.getInteger("regionId"));
        logisticsStocks.add(logisticsStock);
      }
    } catch (Exception e) {
      log.error(err);
    }
    log.info("顺丰库存查询：参数{}，返回{}", stockParam, value);
    return logisticsStocks;
  }

  @Override
  public ShippingGoodsAll getShippingFee(ShippingGoodsAllParam shippingGoodsAllParam) {
    SfAppConfig appConfig = sfProductService.loadConfig();
    String url = String.format("%s?app_key=%s&access_token=%s&timestamp=%s",
        host + SFApiConstants.SHIPPING_FEE, appConfig.getClientId(),
        appConfig.getAccessToken(),
        String.valueOf(new Date().getTime()));
    ShippingGoodsAll shippingGoodsAll = null;
    try {
      JSONObject jsonObject = new JSONObject();
      JSONArray jsonArray = new JSONArray();
      jsonObject.put("userrank", shippingGoodsAllParam.getUserrank());
      jsonObject.put("productlist", jsonArray);
      List<ShippingGoodsParam> shippingGoodsParams = shippingGoodsAllParam.getShippingGoodsParams();
      for (int i = 0; i < shippingGoodsParams.size(); i++) {
        JSONObject value = new JSONObject();
        jsonArray.add(i, value);
        value.put("sellPrice", shippingGoodsParams.get(i).getSellPrice());
        value.put("productSn", shippingGoodsParams.get(i).getProductSn());
        value.put("productIndex", shippingGoodsParams.get(i).getProductIndex());
        value.put("weight", shippingGoodsParams.get(i).getWeight());
        value.put("regionId", shippingGoodsParams.get(i).getRegionId());
        value.put("cityId", shippingGoodsParams.get(i).getCityId());
        value.put("productId", shippingGoodsParams.get(i).getProductId());
        value.put("sfshipping", shippingGoodsParams.get(i).getSfshipping());
        value.put("merchantNumber", shippingGoodsParams.get(i).getMerchantNumber());
      }
      HttpInvokeResult res = PoolingHttpClients.postJSON(url, jsonObject);
      String value = res.getContent();
      jsonObject = JSONObject.parseObject(value).getJSONObject("data");
      shippingGoodsAll = new ShippingGoodsAll();
      shippingGoodsAll.setTotalshippingfee(jsonObject.getInteger("totalshippingfee"));
      shippingGoodsAll.setSelfshippingfee(jsonObject.getInteger("selfshippingfee"));
      shippingGoodsAll.setBusishippingfee(jsonObject.getInteger("busishippingfee"));
      jsonArray = JSONArray.parseArray(jsonObject.getString("productlist"));

      List<ShippingGoods> shippingGoodss = new ArrayList<>();
      if(jsonArray != null) {
        for (int i = 0; i < jsonArray.size(); i++) {
          ShippingGoods shippingGoods = new ShippingGoods();
          JSONObject valueRes = jsonArray.getJSONObject(i);
          shippingGoods.setProductId(valueRes.getInteger("productId"));
          shippingGoods.setProductSn(valueRes.getString("productSn"));
          shippingGoods.setProductIndex(valueRes.getString("productIndex"));
          shippingGoods.setShippingFee(valueRes.getInteger("shippingFee"));
          shippingGoodss.add(shippingGoods);
        }
      }

      shippingGoodsAll.setProductlist(shippingGoodss);
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return shippingGoodsAll;
  }

  @Override
  public List<SubOrder> findSubOrder(String orderSn) {
    List<SubOrder> subOrders = new ArrayList<>();
    try {
      SfAppConfig appConfig = sfProductService.loadConfig();
      String url = String.format("%s?app_key=%s&access_token=%s&timestamp=%s",
          host + SFApiConstants.CHILD_ORDER, appConfig.getClientId(),
          appConfig.getAccessToken(),
          String.valueOf(new Date().getTime()));
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("parentOrderSn", orderSn);
      HttpInvokeResult res = PoolingHttpClients.postJSON(url, jsonObject);
      String value = res.getContent();
      JSONArray jsonArray = JSONObject.parseObject(value).getJSONArray("data");
      if (jsonArray == null) {
        return subOrders;
      }
      for (int i = 0; i < jsonArray.size(); i++) {
        SubOrder subOrder = new SubOrder();
        jsonObject = jsonArray.getJSONObject(i);
        subOrder.setOrderId(jsonObject.getLong("orderId"));
        subOrder.setOrderSn(jsonObject.getString("orderSn"));
        subOrder.setOrderStatus(jsonObject.getString("orderStatus"));
        subOrders.add(subOrder);
      }
    } catch (Exception e) {
      log.error("获取子订单错误", e);
    }
    return subOrders;
  }

  @Override
  public SFOrderCheck getOrderDetail(String orderSn) {
    //第一层
    SFOrderCheck OrderCheck = new SFOrderCheck();
    //第二层
    List<OrderItems> OrderItems = new ArrayList<>();
    //第三层
    List<OrderProduct> sfOrderProducts = new ArrayList<>();
    OrderCheck.setOrderItems(OrderItems);
    //只取到第一个值
//    SFOrderItems sfOrderItems1=new SFOrderItems();
//    sfOrderItems1.setSfOrderProducts(sfOrderProducts);
//    sfOrderItems.add(sfOrderItems1);
    try {
      SfAppConfig appConfig = sfProductService.loadConfig();
      String url = String.format("%s?app_key=%s&access_token=%s&timestamp=%s",
          host + SFApiConstants.SELECT_ORDER, appConfig.getClientId(),
          appConfig.getAccessToken(),
          String.valueOf(new Date().getTime()));
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("page", 1);
      jsonObject.put("pageSize", 9);
      if (orderSn != null) {
        jsonObject.put("queryParam", orderSn);
      }
      HttpInvokeResult res = PoolingHttpClients.postJSON(url, jsonObject);
      String value = res.getContent();
      jsonObject = JSONObject.parseObject(value).getJSONObject("data");
      //顺丰第一层
      OrderCheck.setOrderNum(jsonObject.getLong("orderNum"));
      JSONArray orderItems = jsonObject.getJSONArray("orderItems");
      for (int j = 0; j < orderItems.size(); j++) {
        //顺丰第二层
        OrderItems sfOrderItems1 = new OrderItems();
        JSONObject orderItemOne = orderItems.getJSONObject(j);
        JSONArray orderProducts = orderItemOne.getJSONArray("orderProduct");
        sfOrderProducts = new ArrayList<>();
        for (int i = 0; i < orderProducts.size(); i++) {
          //顺丰第三层
          OrderProduct sfOrderProduct = new OrderProduct();
          JSONObject product = orderProducts.getJSONObject(i);
          sfOrderProduct.setProductId(product.getLong("productId"));
          sfOrderProduct.setProductImg(product.getString("productImg"));
          sfOrderProducts.add(sfOrderProduct);
        }
        sfOrderItems1.setShippingSn(orderItemOne.getString("shippingSn"));
        checkOrderStatus(sfOrderItems1, orderItemOne.getInteger("orderStatus"));
        sfOrderItems1.setOrderSn(orderItemOne.getString("orderSn"));
        sfOrderItems1.setShippingTypeName(orderItemOne.getString("shippingTypeName"));
        sfOrderItems1.setSfOrderProducts(sfOrderProducts);
        OrderItems.add(sfOrderItems1);
      }
    } catch (Exception e) {
      log.error("获取订单详情错误", e);
    }
    return OrderCheck;
  }

  @Override
  public List<SFOrderLog> getSFOrderLog(String orderSn, String billSn) {
    SfAppConfig appConfig = sfProductService.loadConfig();
    String url = String.format("%s?app_key=%s&access_token=%s&timestamp=%s",
        host + SFApiConstants.PULL_LOGISTICS, appConfig.getClientId(),
        appConfig.getAccessToken(),
        String.valueOf(new Date().getTime()));
    List<SFOrderLog> sfOrderLogs = new ArrayList<>();
    try {
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("orderSn", orderSn);
      jsonObject.put("billSn", billSn);
      jsonObject.put("shippingid", 2);
      HttpInvokeResult res = PoolingHttpClients.postJSON(url, jsonObject);
      String value = res.getContent();
      JSONArray jsonArray = JSONObject.parseObject(value).getJSONArray("data");
      if (jsonArray == null || jsonArray.isEmpty()) {
        return sfOrderLogs;
      }
      for (int i = 0; i < jsonArray.size(); i++) {
        SFOrderLog sfOrderLog = new SFOrderLog();
        jsonObject = jsonArray.getJSONObject(i);
        sfOrderLog.setCommont(jsonObject.getString("commont"));
        sfOrderLog.setTime(jsonObject.getString("time"));
        sfOrderLogs.add(sfOrderLog);
      }
    } catch (Exception e) {
      log.error("获取订单路由错误", e);
    }
    return sfOrderLogs;
  }


  /**
   * 功能描述:根据运单号查询第三方物流信息
   * @author Luxiaoling
   * @date 2018/12/7 10:18
   * @param
   * @return  java.util.List<com.xquark.service.vo.SFOrderLog>
   */
  @Override
  public List<SFOrderLog> getThirdLogistics(String wayBillNo, String waybillType) {
    //根据订单号查询运单号
    List<SFOrderLog> SFOrderLogs = new ArrayList<>();
    if(StringUtil.isNotNull(wayBillNo)){
      //发送阿里云市场发送运单查询请求数
      String path = "/kdi";
      String method = "GET";
      Map<String, String> headers = new HashMap<String, String>();
      headers.put("Authorization", "APPCODE " + thirdLogisticAppCode);
      Map<String, String> querys = new HashMap<String, String>();
      querys.put("no", wayBillNo);
      querys.put("type", waybillType);

      try {
        HttpResponse response = HttpUtils.doGet(thirdLogisticUrl, path, method, headers, querys);

        String responseStr = EntityUtils.toString(response.getEntity());
        JSONObject logisticsObj = JSONObject.parseObject(responseStr);
        String status = (String)logisticsObj.get("status");
        if("0".equals(status)){
          JSONObject arrayJsonObject = logisticsObj.getJSONObject("result");
          JSONArray jsonArray = arrayJsonObject.getJSONArray("list");
          for (int i = 0; i < jsonArray.size(); i++) {
            SFOrderLog sfOrderLog = new SFOrderLog();
            logisticsObj = jsonArray.getJSONObject(i);
            sfOrderLog.setCommont(logisticsObj.getString("status"));
            //yyyy-MM-dd HH:mm:ss时间转换成以秒为单位的时间戳
            String timestr = logisticsObj.getString("time");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = formatter.parse(timestr);
            sfOrderLog.setTime(date.getTime()/1000+"");
            SFOrderLogs.add(sfOrderLog);
          }
        }

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage(),e);
      }
    }

    return SFOrderLogs;
  }

  /**
   * 功能描述: 根据订单号查询运单号
   * @author Luxiaoling
   * @date 2019/2/20 14:53
   * @param  orderNo
   * @return  java.util.Map
   */
  @Override
  public Map getbillNoByOrderNo(String orderNo) {
    return  orderMapper.selectBillNoByOrderNo(orderNo);

  }


  private void checkOrderStatus(OrderItems orderItems, Integer orderStatus) {
    String status = "";
    switch (orderStatus) {
      case 6:
        status = "待发货";
        break;
      case 7:
        status = "运输中";
        break;
      case 9:
        status = "已签收";
        break;
      default:
        status = "待确认";
        break;
    }
    orderItems.setShippingStatusStr(status);
  }
}