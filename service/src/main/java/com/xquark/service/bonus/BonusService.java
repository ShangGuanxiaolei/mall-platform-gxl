package com.xquark.service.bonus;

import com.xquark.dal.model.*;
import com.xquark.dal.vo.BonusVO;
import com.xquark.dal.vo.UserAgentCommissionVO;
import com.xquark.dal.vo.UserAgentVO;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by chh on 17-6-7. 代理用户分红Service
 */
public interface BonusService {


  /**
   * 服务端分页查询数据
   */
  List<BonusVO> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  Boolean updateStatus(String idStr, String status);

  /**
   * 根据年度，月度获取月度分红记录
   */
  Bonus getBonusMonthByPeriod(String year, String month);

  /**
   * 根据年度获取年度分红记录
   */
  Bonus getBonusYearByPeriod(String year);

  /**
   * 删除分红记录
   */
  int deleteBonus(String id);

  /**
   * 新增分红记录
   */
  int insertBonus(Bonus bonus);

  /**
   * 新增分红明细记录
   */
  int insertBonusDetail(BonusDetail bonusDetail);

  /**
   * 更新分红记录的end_time
   */
  int updateBonusEndTime(String id);

  /**
   * 获取某个代理某年月度分红各个级别数量
   */
  List<Map> getBonusMonthCount(String year, String userId);


  /**
   * 获取当前用户的分红记录
   */
  ArrayList<Map> getMyBonus(String userId);

  /**
   * 获取某年某个月份每个达标等级对应的代理总数
   */
  List<Map> getTotalBonusMonthCount(String year, String month);

  /**
   * 获取某年份每个达标等级对应的代理总数
   */
  List<Map> getTotalBonusYearCount(String year);

  /**
   * 根据每个级别达标的代理人数，平均算出每个代理能分的月度分红金额
   */
  int updateBonusMonthFee(String year, String month, String level, String count);

  /**
   * 根据每个级别达标的代理人数，平均算出每个代理能分的年度分红金额
   */
  int updateBonusYearFee(String year, String level, String count);
}
