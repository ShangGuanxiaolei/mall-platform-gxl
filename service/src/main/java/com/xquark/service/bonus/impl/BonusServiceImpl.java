package com.xquark.service.bonus.impl;

import com.alibaba.fastjson.JSON;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.AgentStatus;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.BonusType;
import com.xquark.dal.status.RoleBelong;
import com.xquark.dal.type.PushMsgId;
import com.xquark.dal.type.PushMsgType;
import com.xquark.dal.vo.BonusVO;
import com.xquark.dal.vo.UserAgentCommissionVO;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.event.MessageNotifyEvent;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.auditRule.AuditRuleService;
import com.xquark.service.bonus.BonusService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.msg.MessageService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.user.RoleService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.thirds.umpay.api.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by chh on 17-6-7.
 */
@Service("bonusServiceImpl")
public class BonusServiceImpl implements BonusService {

  protected Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private BonusMapper bonusMapper;

  @Autowired
  private OrderMapper orderMapper;

  private int year = 2017;

  private String month = "04";


  /**
   * 服务端分页查询数据
   */
  @Override
  public List<BonusVO> list(Pageable pager, Map<String, Object> params) {
    return bonusMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return bonusMapper.selectCnt(params);
  }


  @Override
  public Boolean updateStatus(String idStr, String status) {
    String[] ids = idStr.split(",");
    return bonusMapper.updateStatus(ids, status) > 0;
  }

  /**
   * 根据年度，月度获取月度分红记录
   */
  @Override
  public Bonus getBonusMonthByPeriod(String year, String month) {
    return bonusMapper.getBonusMonthByPeriod(year, month);
  }

  /**
   * 根据年度获取年度分红记录
   */
  @Override
  public Bonus getBonusYearByPeriod(String year) {
    return bonusMapper.getBonusYearByPeriod(year);
  }

  /**
   * 删除分红记录
   */
  @Override
  public int deleteBonus(String id) {
    return bonusMapper.deleteBonus(id);
  }

  /**
   * 新增分红记录
   */
  @Override
  public int insertBonus(Bonus bonus) {
    return bonusMapper.insertBonus(bonus);
  }

  /**
   * 新增分红明细记录
   */
  @Override
  public int insertBonusDetail(BonusDetail bonusDetail) {
    return bonusMapper.insertBonusDetail(bonusDetail);
  }

  /**
   * 更新分红记录的end_time
   */
  @Override
  public int updateBonusEndTime(String id) {
    return bonusMapper.updateBonusEndTime(id);
  }

  /**
   * 获取某个代理某年月度分红各个级别数量
   */
  @Override
  public List<Map> getBonusMonthCount(String year, String userId) {
    return bonusMapper.getBonusMonthCount(year, userId);
  }

  /**
   * 获取当前用户的分红记录
   */
  @Override
  public ArrayList<Map> getMyBonus(String userId) {
    ArrayList<Map> result = new ArrayList<Map>();
    List<BonusVO> vos = bonusMapper.getMyBonus(userId);
    List years = getYears();
    for (int i = 0, n = years.size(); i < n; i++) {
      Map map = new HashMap();
      map.put("year", years.get(i));
      map.put("yearVO", getYearBonus(userId, vos, "" + years.get(i)));
      List<BonusVO> months = getMonthBonus(vos, "" + years.get(i));
      map.put("month", months);
      result.add(map);
    }
    return result;
  }

  /**
   * 获取该用户某个年份下每个月份的分红
   */
  private List<BonusVO> getMonthBonus(List<BonusVO> vos, String year) {
    List<BonusVO> result = new ArrayList<BonusVO>();
    Calendar a = Calendar.getInstance();
    int curYear = a.get(Calendar.YEAR);
    int curMonth = a.get(Calendar.MONTH) + 1;
    for (int i = 12; i > 0; i--) {
      String month = "";
      if (year.equals("" + curYear) && i > curMonth) {
        continue;
      }
      if (i < 10) {
        month = "0" + i;
      } else {
        month = "" + i;
      }
      BonusVO monthVO = new BonusVO();
      monthVO.setYear(year);
      monthVO.setMonth(month);
      monthVO.setType(BonusType.MONTH);
      for (BonusVO vo : vos) {
        if (vo.getType() == BonusType.MONTH && vo.getYear().equals(year) && vo.getMonth()
            .equals(month)) {
          monthVO = vo;
          // 设置该月度分红下，各个董事姓名和订单数量信息
          List<Map> firstData = orderMapper
              .getSelfLastMonthOrder(vo.getUserId(), vo.getYear() + "-" + vo.getMonth());
          List<Map> secondData = orderMapper
              .getDirectLastMonthOrder(vo.getUserId(), vo.getYear() + "-" + vo.getMonth());
          List<Map> thirdData = orderMapper
              .getIndirectLastMonthOrder(vo.getUserId(), vo.getYear() + "-" + vo.getMonth());
          monthVO.setFirstData(firstData);
          monthVO.setSecondData(secondData);
          monthVO.setThirdData(thirdData);
        }
      }
      result.add(monthVO);
    }
    return result;
  }

  /**
   * 获取该用户当前年份的年度分红
   */
  private BonusVO getYearBonus(String userId, List<BonusVO> vos, String year) {
    BonusVO result = new BonusVO();
    result.setYear(year);
    result.setType(BonusType.YEAR);
    for (BonusVO vo : vos) {
      if (vo.getType() == BonusType.YEAR && vo.getYear().equals(year)) {
        result = vo;
      }
    }
    // 得到某个代理某年某个级别下所有分红的月份及达标数量等信息供app展示
    List<Map> yearData = new ArrayList<Map>();
    // 董事级别A达到的次数
    long aCount = 0;
    // 董事级别B达到的次数
    long bCount = 0;
    // 董事级别C达到的次数
    long cCount = 0;
    // 董事级别D达到的次数
    long dCount = 0;
    String level = "";
    // 先获取某个代理某年月度分红各个级别数量
    List<Map> months = bonusMapper.getBonusMonthCount(year, userId);
    aCount = getCount(months, "A");
    if (aCount > 0) {
      HashMap map = new HashMap();
      List<String> monthsStr = bonusMapper.getBonusMonthByType(userId, "A", year);
      map.put("level", "A");
      map.put("qty", aCount);
      map.put("months", monthsStr);
      yearData.add(map);
    }

    bCount = getCount(months, "B");
    if (bCount > 0) {
      HashMap map = new HashMap();
      List<String> monthsStr = bonusMapper.getBonusMonthByType(userId, "B", year);
      map.put("level", "B");
      map.put("qty", bCount);
      map.put("months", monthsStr);
      yearData.add(map);
    }

    cCount = getCount(months, "C");
    if (cCount > 0) {
      HashMap map = new HashMap();
      List<String> monthsStr = bonusMapper.getBonusMonthByType(userId, "C", year);
      map.put("level", "C");
      map.put("qty", cCount);
      map.put("months", monthsStr);
      yearData.add(map);
    }

    dCount = getCount(months, "D");
    if (dCount > 0) {
      HashMap map = new HashMap();
      List<String> monthsStr = bonusMapper.getBonusMonthByType(userId, "D", year);
      map.put("level", "D");
      map.put("qty", dCount);
      map.put("months", monthsStr);
      yearData.add(map);
    }
    result.setYearData(yearData);
    return result;
  }

  private int getCount(List<Map> months, String level) {
    int count = 0;
    for (Map month : months) {
      if (month.get("level") != null && month.get("level").equals(level)) {
        count = new BigDecimal("" + month.get("count")).intValue();
      }
    }
    return count;
  }


  /**
   * 获取分红涉及的所有年份
   */
  private List getYears() {
    ArrayList list = new ArrayList();
    Calendar a = Calendar.getInstance();
    int curYear = a.get(Calendar.YEAR);
    if (curYear == year) {
      list.add(curYear);
    } else {
      while (curYear >= year) {
        list.add(curYear);
        curYear--;
      }
    }

    return list;
  }

  /**
   * 获取某年某个月份每个达标等级对应的代理总数
   */
  @Override
  public List<Map> getTotalBonusMonthCount(String year, String month) {
    return bonusMapper.getTotalBonusMonthCount(year, month);
  }

  /**
   * 获取某年份每个达标等级对应的代理总数
   */
  @Override
  public List<Map> getTotalBonusYearCount(String year) {
    return bonusMapper.getTotalBonusYearCount(year);
  }

  /**
   * 根据每个级别达标的代理人数，平均算出每个代理能分的月度分红金额
   */
  @Override
  public int updateBonusMonthFee(String year, String month, String level, String count) {
    return bonusMapper.updateBonusMonthFee(year, month, level, count);
  }

  /**
   * 根据每个级别达标的代理人数，平均算出每个代理能分的年度分红金额
   */
  @Override
  public int updateBonusYearFee(String year, String level, String count) {
    return bonusMapper.updateBonusYearFee(year, level, count);
  }

  public static void main(String args[]) {
    Calendar a = Calendar.getInstance();
    int curYear = a.get(Calendar.YEAR);
    int curMonth = a.get(Calendar.MONTH) + 1;
    System.out.println(curYear);
    System.out.println(curMonth);
  }

}
