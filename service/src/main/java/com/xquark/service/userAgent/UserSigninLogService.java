package com.xquark.service.userAgent;

import com.xquark.dal.model.UserSigninLog;

import java.util.List;

public interface UserSigninLogService {

  void insert(UserSigninLog userSigninLog);

  List<Long> selectUserSignInLog(String userId);

}
