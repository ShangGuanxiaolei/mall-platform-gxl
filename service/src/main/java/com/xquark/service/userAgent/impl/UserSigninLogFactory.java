package com.xquark.service.userAgent.impl;

import javax.servlet.http.HttpServletRequest;

import com.xquark.dal.model.User;
import com.xquark.dal.model.UserSigninLog;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;

public class UserSigninLogFactory {

  public static UserSigninLog createUserSigninLog(HttpServletRequest request, User user) {
    return parse(request, user);
  }

  private static UserSigninLog parse(HttpServletRequest request, User user) {
    String ua = request.getHeader("User-Agent");
    String ip = getIpAddr(request);
    UserAgent userAgent = UserAgent.parseUserAgentString(ua);
    Browser browser = userAgent.getBrowser();
    OperatingSystem os = userAgent.getOperatingSystem();
//		DeviceType client = os.getDeviceType();
    UserSigninLog userSigninLog = new UserSigninLog();
    userSigninLog.setUserId(user.getId());
    userSigninLog.setIp(ip);
    userSigninLog.setClient(ua);
    userSigninLog.setBrowser(browser.toString());
    userSigninLog.setOs(os.toString());
    userSigninLog.setPartner(user.getPartner());
    userSigninLog.setDeviceSN(getUaAttribute("deviceSN", ua));
    return userSigninLog;
  }

  public static void main(String[] args) {
    String ua = "Mozilla/5.0 (Linux; Android 4.4.2; Che2-TL00 Build/HonorChe2-TL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36 (Hotshop; Client/Android4.4.2 V/2041|2.0.4 channel/ouertech deviceSN/ODY1NjQ3MDIwNDQ1NDc2fGYzNDhiNjI1YjQ0NWRjYzQ=";
    System.out.println(getUaAttribute("Version", ua));
    System.out.println(getUaAttribute("channel", ua));
    System.out.println(getUaAttribute("V", ua));
    System.out.println(getUaAttribute("Client", ua));
    System.out.println(getUaAttribute("Chrome", ua));
    System.out.println(getUaAttribute("deviceSN", ua));
  }

  private static String getUaAttribute(String attr, String ua) {
    if (ua != null) {
      String[] arr = ua.split(" ");
      for (String e : arr) {
        String[] a = e.split("/");
        if (a.length == 2) {
          if (a[0].equalsIgnoreCase(attr)) {
            return a[1];
          }
        } else {
          if (e.equalsIgnoreCase(attr)) {
            return e;
          }
        }
      }
    }
    return null;
  }

  private static String getIpAddr(HttpServletRequest request) {
    String ip = request.getHeader("x-forwarded-for");
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("WL-Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("HTTP_CLIENT_IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("HTTP_X_FORWARDED_FOR");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getRemoteAddr();
    }
    return ip;
  }
}
