package com.xquark.service.userAgent.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.UserSigninLogMapper;
import com.xquark.dal.model.UserSigninLog;
import com.xquark.service.userAgent.UserSigninLogService;

import java.util.List;

@Service("userSigninLogService")
public class UserSigninLogServiceImpl implements UserSigninLogService {

  @Autowired
  UserSigninLogMapper userSigninLogMapper;

  @Override
  public void insert(UserSigninLog userSigninLog) {
    userSigninLogMapper.insert(userSigninLog);
  }

  @Override
   public List<Long> selectUserSignInLog(String userId) {
      return userSigninLogMapper.selectUserSignInLog(userId);
   }
}
