package com.xquark.service.team;

import com.xquark.dal.model.TeamShopCommission;
import com.xquark.dal.vo.TeamShopCommissionVO;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface TeamShopComService extends BaseEntityService<TeamShopCommission> {

  int deleteByPrimaryKey(String id);

  int insert(TeamShopCommission record);

  int insertSelective(TeamShopCommission record);

  TeamShopCommissionVO selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(TeamShopCommission record);

  int updateByPrimaryKey(TeamShopCommission record);

  int delete(String id);

  TeamShopCommission selectDefaultByshopId(String shopId);

  List<TeamShopCommission> selectNonDefaultByshopId(String shopId);

  /**
   * 删除战队默认设置
   */
  int deleteByDefaultByshopId(String shopId);

  List<TeamShopCommission> selectAllByshopId(String shopId);

  List<TeamShopCommissionVO> listNonDefaultByshopId(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCntNonDefault(Map<String, Object> params);

  /**
   * 获取某个战队分组对应的周和月设置信息
   */
  Map getWeeksAndMonths(TeamShopCommission commission, String shopId);

}
