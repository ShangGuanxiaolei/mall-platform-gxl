package com.xquark.service.team.impl;

import com.xquark.dal.mapper.UserTeamMapper;
import com.xquark.dal.model.UserTeam;
import com.xquark.dal.vo.UserTeamVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.team.UserTeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("userTeamService")
public class UserTeamServiceImpl extends BaseServiceImpl implements UserTeamService {

  @Autowired
  UserTeamMapper userTeamMapper;

  @Override
  public int insert(UserTeam team) {
    return userTeamMapper.insert(team);
  }

  @Override
  public int insertOrder(UserTeam userTeam) {
    return userTeamMapper.insert(userTeam);
  }

  @Override
  public UserTeam load(String id) {
    return userTeamMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return userTeamMapper.updateForArchive(id);
  }

  @Override
  public int update(UserTeam record) {
    return userTeamMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<UserTeam> list(Pageable pager, Map<String, Object> params) {
    return userTeamMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return userTeamMapper.selectCnt(params);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<UserTeamVO> listByTeamId(Pageable pager, Map<String, Object> params) {
    return userTeamMapper.listByTeamId(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCntByTeamId(Map<String, Object> params) {
    return userTeamMapper.selectCntByTeamId(params);
  }


}
