package com.xquark.service.team;

import com.xquark.dal.model.TeamProductCommission;
import com.xquark.dal.vo.TeamProductCommissionVO;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface TeamProductComService extends BaseEntityService<TeamProductCommission> {

  TeamProductCommissionVO load(String id);

  int insert(TeamProductCommission teamProductCommission);

  int deleteForArchive(String id);

  int update(TeamProductCommission record);

  /**
   * 服务端分页查询数据
   */
  List<TeamProductCommissionVO> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 查询是否已经有该商品的分佣设置
   */
  Long selectByProductId(String shopId, String productId);

}

