package com.xquark.service.team;

import com.xquark.dal.model.Team;
import com.xquark.dal.vo.TeamRecordVO;
import com.xquark.dal.vo.TeamVO;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface TeamService extends BaseEntityService<Team> {

  TeamVO load(String id);

  int insert(Team team);

  int deleteForArchive(String id);

  int update(Team record);

  /**
   * 服务端分页查询数据
   */
  List<TeamVO> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 战队禁用，启用
   */
  int updateStatus(String id, String status);

  /**
   * 战队结算记录列表
   */
  List<TeamRecordVO> listCommissionByShopId(String shopId, Map<String, Object> params,
      Pageable pageable);

  /**
   * 战队结算记录
   */
  Long countCommissionByShopId(String shopId, Map<String, Object> params);

  /**
   * 战队概况数据统计
   */
  Map getSummary(String shopId);

  /**
   * 查询用户在哪个战队中
   */
  TeamVO selectByUserId(String userId);

  /**
   * 删除线下门店，同时删掉对应的战队
   */
  int deleteByStoreId(String storeId);

}

