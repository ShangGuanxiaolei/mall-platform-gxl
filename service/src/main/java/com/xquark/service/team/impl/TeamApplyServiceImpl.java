package com.xquark.service.team.impl;

import com.xquark.dal.mapper.TeamApplyMapper;
import com.xquark.dal.model.TeamApply;
import com.xquark.dal.vo.TeamApplyVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.team.TeamApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("teamApplyService")
public class TeamApplyServiceImpl extends BaseServiceImpl implements TeamApplyService {

  @Autowired
  TeamApplyMapper teamApplyMapper;

  @Override
  public int insert(TeamApply team) {
    return teamApplyMapper.insert(team);
  }

  @Override
  public int insertOrder(TeamApply teamApply) {
    return teamApplyMapper.insert(teamApply);
  }

  @Override
  public TeamApply load(String id) {
    return teamApplyMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return teamApplyMapper.updateForArchive(id);
  }

  @Override
  public int update(TeamApply record) {
    return teamApplyMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<TeamApplyVO> list(Pageable pager, Map<String, Object> params) {
    return teamApplyMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return teamApplyMapper.selectCnt(params);
  }

  /**
   * 战队禁用，启用
   */
  @Override
  public int updateStatus(String id, String status) {
    return teamApplyMapper.updateStatus(id, status);
  }

  /**
   * 根据userid查询意向用户信息
   */
  @Override
  public TeamApply selectByUserId(String userId) {
    return teamApplyMapper.selectByUserId(userId);
  }


}
