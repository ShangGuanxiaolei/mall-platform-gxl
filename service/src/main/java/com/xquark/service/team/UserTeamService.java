package com.xquark.service.team;

import com.xquark.dal.model.UserTeam;
import com.xquark.dal.vo.UserTeamVO;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface UserTeamService extends BaseEntityService<UserTeam> {

  UserTeam load(String id);

  int insert(UserTeam team);

  int deleteForArchive(String id);

  int update(UserTeam record);

  /**
   * 服务端分页查询数据
   */
  List<UserTeam> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 服务端分页查询数据
   */
  List<UserTeamVO> listByTeamId(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCntByTeamId(Map<String, Object> params);

}

