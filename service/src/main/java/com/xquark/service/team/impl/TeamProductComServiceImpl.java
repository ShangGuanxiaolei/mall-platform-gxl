package com.xquark.service.team.impl;

import com.xquark.dal.mapper.TeamProductCommissionMapper;
import com.xquark.dal.model.TeamProductCommission;
import com.xquark.dal.vo.TeamProductCommissionVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.team.TeamProductComService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("teamProductComService")
public class TeamProductComServiceImpl extends BaseServiceImpl implements TeamProductComService {

  @Autowired
  TeamProductCommissionMapper teamProductCommissionMapper;

  @Override
  public int insert(TeamProductCommission teamProductCommission) {
    return teamProductCommissionMapper.insert(teamProductCommission);
  }

  @Override
  public int insertOrder(TeamProductCommission teamProductCommission) {
    return teamProductCommissionMapper.insert(teamProductCommission);
  }

  @Override
  public TeamProductCommissionVO load(String id) {
    return teamProductCommissionMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return teamProductCommissionMapper.updateForArchive(id);
  }

  @Override
  public int update(TeamProductCommission record) {
    return teamProductCommissionMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<TeamProductCommissionVO> list(Pageable pager, Map<String, Object> params) {
    return teamProductCommissionMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return teamProductCommissionMapper.selectCnt(params);
  }

  /**
   * 查询是否已经有该商品的分佣设置
   */
  @Override
  public Long selectByProductId(String shopId, String productId) {
    return teamProductCommissionMapper.selectByProductId(shopId, productId);
  }

}
