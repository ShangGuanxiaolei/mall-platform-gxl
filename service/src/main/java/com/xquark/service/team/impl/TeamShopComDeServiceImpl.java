package com.xquark.service.team.impl;

import com.xquark.dal.mapper.TeamShopCommissionDeMapper;
import com.xquark.dal.model.TeamShopCommissionDe;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.team.TeamShopComDeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("teamShopComDeService")
public class TeamShopComDeServiceImpl extends BaseServiceImpl implements TeamShopComDeService {

  @Autowired
  TeamShopCommissionDeMapper teamShopCommissionDeMapper;

  @Override
  public int deleteByPrimaryKey(String id) {
    return teamShopCommissionDeMapper.deleteByPrimaryKey(id);
  }

  @Override
  public TeamShopCommissionDe load(String id) {
    return teamShopCommissionDeMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(TeamShopCommissionDe record) {
    return teamShopCommissionDeMapper.insert(record);
  }

  @Override
  public int insertOrder(TeamShopCommissionDe teamShopCommissionDe) {
    return teamShopCommissionDeMapper.insert(teamShopCommissionDe);
  }

  @Override
  public int insertSelective(TeamShopCommissionDe record) {
    return teamShopCommissionDeMapper.insertSelective(record);
  }

  @Override
  public TeamShopCommissionDe selectByPrimaryKey(String id) {
    return teamShopCommissionDeMapper.selectByPrimaryKey(id);
  }

  @Override
  public int updateByPrimaryKeySelective(TeamShopCommissionDe record) {
    return teamShopCommissionDeMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(TeamShopCommissionDe record) {
    return teamShopCommissionDeMapper.updateByPrimaryKey(record);
  }

  @Override
  public int delete(String id) {
    return teamShopCommissionDeMapper.updateForArchive(id);
  }


  @Override
  public List<TeamShopCommissionDe> selectByParentId(String parentId) {
    return teamShopCommissionDeMapper.selectByParentId(parentId);
  }

  /**
   * 删除战队默认设置
   */
  @Override
  public int deleteByDefaultByshopId(String shopId) {
    return teamShopCommissionDeMapper.deleteByDefaultByshopId(shopId);
  }

  /**
   * 根据parentid删除信息
   */
  @Override
  public int deleteByParentId(String parentId) {
    return teamShopCommissionDeMapper.deleteByParentId(parentId);
  }

}
