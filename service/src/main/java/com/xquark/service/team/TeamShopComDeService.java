package com.xquark.service.team;

import com.xquark.dal.model.TeamShopCommissionDe;
import com.xquark.service.BaseEntityService;

import java.util.List;


public interface TeamShopComDeService extends BaseEntityService<TeamShopCommissionDe> {

  int deleteByPrimaryKey(String id);

  int insert(TeamShopCommissionDe record);

  int insertSelective(TeamShopCommissionDe record);

  TeamShopCommissionDe selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(TeamShopCommissionDe record);

  int updateByPrimaryKey(TeamShopCommissionDe record);

  int delete(String id);


  List<TeamShopCommissionDe> selectByParentId(String parentId);

  /**
   * 删除战队默认设置
   */
  int deleteByDefaultByshopId(String shopId);

  /**
   * 根据parentid删除信息
   */
  int deleteByParentId(String parentId);

}
