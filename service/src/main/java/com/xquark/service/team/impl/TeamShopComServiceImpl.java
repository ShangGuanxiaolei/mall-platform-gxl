package com.xquark.service.team.impl;

import com.xquark.dal.mapper.TeamShopCommissionDeMapper;
import com.xquark.dal.mapper.TeamShopCommissionMapper;
import com.xquark.dal.model.TeamShopCommission;
import com.xquark.dal.model.TeamShopCommissionDe;
import com.xquark.dal.status.TeamType;
import com.xquark.dal.vo.TeamShopCommissionVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.team.TeamShopComService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("teamShopComService")
public class TeamShopComServiceImpl extends BaseServiceImpl implements TeamShopComService {

  @Autowired
  TeamShopCommissionMapper teamShopCommissionMapper;

  @Autowired
  TeamShopCommissionDeMapper teamShopCommissionDeMapper;

  @Override
  public int deleteByPrimaryKey(String id) {
    return teamShopCommissionMapper.deleteByPrimaryKey(id);
  }

  @Override
  public TeamShopCommission load(String id) {
    return teamShopCommissionMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(TeamShopCommission record) {
    return teamShopCommissionMapper.insert(record);
  }

  @Override
  public int insertOrder(TeamShopCommission teamShopCommission) {
    return teamShopCommissionMapper.insert(teamShopCommission);
  }

  @Override
  public int insertSelective(TeamShopCommission record) {
    return teamShopCommissionMapper.insertSelective(record);
  }

  @Override
  public TeamShopCommissionVO selectByPrimaryKey(String id) {
    return teamShopCommissionMapper.selectByPrimaryKey(id);
  }

  @Override
  public int updateByPrimaryKeySelective(TeamShopCommission record) {
    return teamShopCommissionMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  public int updateByPrimaryKey(TeamShopCommission record) {
    return teamShopCommissionMapper.updateByPrimaryKey(record);
  }

  @Override
  public int delete(String id) {
    return teamShopCommissionMapper.updateForArchive(id);
  }

  @Override
  public TeamShopCommission selectDefaultByshopId(String shopId) {
    return teamShopCommissionMapper.selectDefaultByshopId(shopId);
  }

  @Override
  public List<TeamShopCommission> selectNonDefaultByshopId(String shopId) {
    return teamShopCommissionMapper.selectNonDefaultByshopId(shopId);
  }

  /**
   * 删除战队默认设置
   */
  @Override
  public int deleteByDefaultByshopId(String shopId) {
    return teamShopCommissionMapper.deleteByDefaultByshopId(shopId);
  }

  @Override
  public List<TeamShopCommission> selectAllByshopId(String shopId) {
    return teamShopCommissionMapper.selectAllByshopId(shopId);
  }

  @Override
  public List<TeamShopCommissionVO> listNonDefaultByshopId(Pageable pager,
      Map<String, Object> params) {
    return teamShopCommissionMapper.ListNonDefaultByshopId(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCntNonDefault(Map<String, Object> params) {
    return teamShopCommissionMapper.selectCntNonDefault(params);
  }

  /**
   * 获取某个战队分组对应的周和月设置信息
   */
  @Override
  public Map getWeeksAndMonths(TeamShopCommission commission, String shopId) {
    HashMap map = new HashMap();
    // 自然周设置
    ArrayList<TeamShopCommissionDe> weeks = new ArrayList<TeamShopCommissionDe>();
    // 自然月设置
    ArrayList<TeamShopCommissionDe> months = new ArrayList<TeamShopCommissionDe>();

    // 查找对应的周和月的配置信息
    if (commission != null) {
      List<TeamShopCommissionDe> teamShopCommissionDes = teamShopCommissionDeMapper
          .selectByParentId(commission.getId());
      for (TeamShopCommissionDe de : teamShopCommissionDes) {
        if (de.getType() == TeamType.WEEK) {
          weeks.add(de);
        } else if (de.getType() == TeamType.MONTH) {
          months.add(de);
        }
      }
    }
    map.put("weeks", weeks);
    map.put("months", months);
    return map;
  }

}
