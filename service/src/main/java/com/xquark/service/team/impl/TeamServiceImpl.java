package com.xquark.service.team.impl;

import com.xquark.dal.mapper.TeamMapper;
import com.xquark.dal.model.Team;
import com.xquark.dal.vo.TeamRecordVO;
import com.xquark.dal.vo.TeamVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.team.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service("teamService")
public class TeamServiceImpl extends BaseServiceImpl implements TeamService {

  @Autowired
  TeamMapper teamMapper;

  @Override
  public int insert(Team team) {
    return teamMapper.insert(team);
  }

  @Override
  public int insertOrder(Team team) {
    return teamMapper.insert(team);
  }

  @Override
  public TeamVO load(String id) {
    return teamMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return teamMapper.updateForArchive(id);
  }

  @Override
  public int update(Team record) {
    return teamMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<TeamVO> list(Pageable pager, Map<String, Object> params) {
    return teamMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return teamMapper.selectCnt(params);
  }

  /**
   * 战队禁用，启用
   */
  @Override
  public int updateStatus(String id, String status) {
    return teamMapper.updateStatus(id, status);
  }

  /**
   * 战队结算记录列表
   */
  @Override
  public List<TeamRecordVO> listCommissionByShopId(String shopId, Map<String, Object> params,
      Pageable pageable) {
    return teamMapper.listCommissionByShopId(shopId, params, pageable);
  }

  /**
   * 战队结算记录
   */
  @Override
  public Long countCommissionByShopId(String shopId, Map<String, Object> params) {
    return teamMapper.countCommissionByShopId(shopId, params);
  }

  /**
   * 战队概况数据统计
   */
  @Override
  public Map getSummary(String shopId) {
// 缓存今天的各项值
    Map<String, Object> info = new HashMap<String, Object>();
    // 缓存一周的各项值
    Map<String, Object> nums = new HashMap<String, Object>();

    Map<String, Object> result = new HashMap<String, Object>();
    ArrayList dates = getWeekDay();
    // 本周已成功订单
    ArrayList weekOrder = (ArrayList) teamMapper.getWeekOrder(shopId, dates);
    HashMap weekValue = formatList(weekOrder);
    long total_team = teamMapper.countTotalTeam(shopId);
    BigDecimal lastMonth_amount = teamMapper.getLastMonthAmount(shopId);

    // 今日订单数，今日订单交易额,总战队数，上月总交易额
    info.put("today_num", weekValue.get("today_num"));
    info.put("today_amount", weekValue.get("today_amount"));
    info.put("total_team", total_team);
    info.put("lastMonth_amount", lastMonth_amount);

    // 近七天订单数，订单交易额
    nums.put("week_num", weekValue.get("week_num"));
    nums.put("week_amount", weekValue.get("week_amount"));

    // 各个战队交易额汇总，取前三名
    List teamAmounts = teamMapper.getTeamAmounts(shopId);
    // 战队总交易额
    BigDecimal totalAmount = teamMapper.getTotalAmount(shopId);
    // 其他类型交易额为总交易额减去前三名
    BigDecimal threeAmount = BigDecimal.ZERO;
    ArrayList teamNames = new ArrayList();
    for (int i = 0, n = teamAmounts.size(); i < n; i++) {
      Map teamMap = (Map) teamAmounts.get(i);
      BigDecimal amount = new BigDecimal("" + teamMap.get("value"));
      threeAmount = threeAmount.add(amount);
      teamNames.add(teamMap.get("name"));
    }
    Map other = new HashMap();
    other.put("name", "其他");
    other.put("value", totalAmount.subtract(threeAmount));
    teamAmounts.add(other);
    teamNames.add("其他");
    nums.put("teamAmounts", teamAmounts);
    nums.put("teamNames", teamNames);

    nums.put("weekDays", dates);
    result.put("info", info);
    result.put("nums", nums);
    return result;
  }

  /**
   * 获取一周内的所有日期
   */
  private static ArrayList getWeekDay() {
    ArrayList<String> list = new ArrayList<String>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Calendar calendar = Calendar.getInstance();
    while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
      calendar.add(Calendar.DATE, -1);
    }
    for (int i = 0; i < 7; i++) {
      list.add(sdf.format(calendar.getTime()));
      calendar.add(Calendar.DATE, 1);
    }
    return list;
  }

  /**
   * 将从数据库中取出的每天对应的各项值map数据结构做一次格式化 返回当天的值，如数据库没有则是0,同时返回周一到周日的值list(如果某天没有值，则为0)
   */
  private HashMap formatList(ArrayList<Map> list) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String today_num = "0";
    String today_amount = "0";
    ArrayList weeknumvalue = new ArrayList();
    ArrayList weekamountvalue = new ArrayList();
    // 今天
    String nowstr = sdf.format(new Date());
    HashMap result = new HashMap();
    // 得到一周内的每天的str字符串
    ArrayList<String> dates = getWeekDay();
    // 循环得到当天的值和周一到周天顺序每天的值
    for (String date : dates) {
      String temp_num = "0";
      String temp_amount = "0";
      for (Map value : list) {
        String s_date = (String) value.get("s_date");
        String c_num = "" + value.get("c_num");
        String c_amount = "" + value.get("c_amount");
        if (nowstr.equals(s_date)) {
          today_num = c_num;
          today_amount = c_amount;
        }
        if (date.equals(s_date)) {
          temp_num = c_num;
          temp_amount = c_amount;
          break;
        }
      }
      weeknumvalue.add(temp_num);
      weekamountvalue.add(temp_amount);
    }
    result.put("today_num", today_num);
    result.put("today_amount", today_amount);
    result.put("week_num", weeknumvalue);
    result.put("week_amount", weekamountvalue);
    return result;
  }

  /**
   * 查询用户在哪个战队中
   */
  @Override
  public TeamVO selectByUserId(String userId) {
    return teamMapper.selectByUserId(userId);
  }

  /**
   * 删除线下门店，同时删掉对应的战队
   */
  @Override
  public int deleteByStoreId(String storeId) {
    return teamMapper.deleteByStoreId(storeId);
  }
}
