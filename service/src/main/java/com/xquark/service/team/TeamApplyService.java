package com.xquark.service.team;

import com.xquark.dal.model.TeamApply;
import com.xquark.dal.vo.TeamApplyVO;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface TeamApplyService extends BaseEntityService<TeamApply> {

  TeamApply load(String id);

  int insert(TeamApply team);

  int deleteForArchive(String id);

  int update(TeamApply record);

  /**
   * 服务端分页查询数据
   */
  List<TeamApplyVO> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 战队禁用，启用
   */
  int updateStatus(String id, String status);

  /**
   * 根据userid查询意向用户信息
   */
  TeamApply selectByUserId(String userId);

}

