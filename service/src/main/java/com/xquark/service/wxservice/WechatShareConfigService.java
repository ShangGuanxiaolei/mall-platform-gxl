package com.xquark.service.wxservice;

import com.alibaba.fastjson.JSONObject;
import com.xquark.service.wxservice.config.WeChatConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.Random;

/**
 *
 * 微信分享配置服务类
 *
 * @author tanggb
 * @date 2019/02/13
 */
@Service
public class WechatShareConfigService {

    private String appId = WeChatConfig.appId;

    private String appSecret = WeChatConfig.secert;

    public String getAppId() {
        return appId;
    }


    public void setAppId(String appId) {
        this.appId = appId;
    }

    /**
     * 通过appId,appsecret获取access_token
     * @return
     */
    private String getAccessToken(){
        String token_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
        String request_url = token_url.replace("APPID", appId).replace("APPSECRET", appSecret);
        JSONObject jsonObject = sendGet(request_url);
        return jsonObject.getString("access_token");
    }

    /**
     * 通过access_token获取ticket
     * @return
     */
    public String getTicket(){
        String ticket_url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
        String request_url = ticket_url.replace("ACCESS_TOKEN", getAccessToken());
        JSONObject jsonObject = sendGet(request_url);
        return jsonObject.getString("ticket");
    }

    /**
     *获取时间戳，单位秒
     * @return
     */
    public String getTimestamp(){
        return String.valueOf(System.currentTimeMillis()/1000);
    }

//    /**
//     * 获取签名
//     * @return
//     * @throws NoSuchAlgorithmException
//     */
//    public String getSignature() throws NoSuchAlgorithmException {
//        String ciphertext = null;
//        String ticket = getTicket();
//        String nonceStr = getRandomStringByLength(32);
//        String timeStamp = getTimestamp();
//        String sKey = "jsapi_ticket="+ ticket + "&noncestr=" + nonceStr + "&timestamp=" + timeStamp + "&url=http://mp.weixin.qq.com?params=value";
//        MessageDigest md = MessageDigest.getInstance("SHA-1");
//        byte[] digest = md.digest(sKey.toString().getBytes());
//        ciphertext = byteToStr(digest);
//        return ciphertext.toLowerCase();
//    }

    /**
     * 获取一定长度的随机字符串
     * @param length 指定字符串长度
     * @return 一定长度的字符串
     */
    public String getRandomStringByLength(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 获得分享链接的签名。
     * @param ticket
     * @param nonceStr
     * @param timeStamp
     * @param url
     * @return
     * @throws Exception
     */
    public String getSignature(String ticket, String nonceStr, long timeStamp, String url) throws Exception {
        String sKey = "jsapi_ticket=" + ticket
                + "&noncestr=" + nonceStr + "&timestamp=" + timeStamp
                + "&url=" + url;
        System.out.println(sKey);
        return getSignature(sKey);
    }

    /**
     * 验证签名。
     *
     * @param sKey
     * @return
     */
    private String getSignature(String sKey) throws Exception {
        String ciphertext = null;
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] digest = md.digest(sKey.toString().getBytes());
        ciphertext = byteToStr(digest);
        return ciphertext.toLowerCase();
    }

    /**
     * 将字节数组转换为十六进制字符串
     *
     * @param byteArray
     * @return
     */
    private String byteToStr(byte[] byteArray) {
        String strDigest = "";
        for (int i = 0; i < byteArray.length; i++) {
            strDigest += byteToHexStr(byteArray[i]);
        }
        return strDigest;
    }

    /**
     * 将字节转换为十六进制字符串
     *
     * @param mByte
     * @return
     */
    private static String byteToHexStr(byte mByte) {
        char[] Digit = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        char[] tempArr = new char[2];
        tempArr[0] = Digit[(mByte >>> 4) & 0X0F];
        tempArr[1] = Digit[mByte & 0X0F];

        String s = new String(tempArr);
        return s;
    }

    /**
     *通过sendGet http请求数据
     * @param url 请求路径
     * @return JSONObject
     */
    private JSONObject sendGet(String url){

        CloseableHttpClient httpclient = HttpClients.createDefault();
        JSONObject jsonResult = null;
        HttpGet method = new HttpGet(url);
        try {
            CloseableHttpResponse result = httpclient.execute(method);
            if (result.getStatusLine().getStatusCode() == 200) {
                String str = "";
                try {
                    str = EntityUtils.toString(result.getEntity());
                    jsonResult = JSONObject.parseObject(str);
                } catch (Exception e) {
                    System.out.println("get请求提交失败:" + url);
                }
            }
        } catch (IOException e) {
            System.out.println("get请求提交失败:" + url);
        }
        return jsonResult;
    }
   public  JsConfigVo  getJsConfig(String url) throws  Exception{
       JsConfigVo jsConfigVo = new JsConfigVo();
       String  ticket=getTicket();
       String urla=url;
       String noncestr=getRandomStringByLength(32);
       String timestamp=getTimestamp();

       jsConfigVo.setAppId(appId);
       jsConfigVo.setNonceStr(noncestr);
       jsConfigVo.setTimestamp(timestamp);
       String signature = getSignature(ticket, noncestr, Long.parseLong(timestamp), urla);
       jsConfigVo.setSignature(signature);

       System.out.println(ticket);
       return  jsConfigVo;
   }




}
