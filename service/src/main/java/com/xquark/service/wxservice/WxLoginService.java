package com.xquark.service.wxservice;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.vo.BaseCustomerVO;
import com.xquark.dal.vo.WxVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.user.UserService;
import com.xquark.service.user.impl.WechatThirdUserConverter;
import com.xquark.service.wxservice.config.WeChatConfig;
import com.xquark.wechat.oauth.protocol.WechatUserBean;
import com.xquark.wechat.oauth.protocol.get_userinfo.GetUserinfoResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkState;

/**
 * Created by chp
 */
@Service
public class WxLoginService  extends BaseServiceImpl {
    private static final Logger logger= LoggerFactory.getLogger(WxLoginService.class);
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private CustomerProfileService customerProfileService;
    @Autowired
    private UserService userService;
    @Autowired
    private WechatThirdUserConverter wechatUserConverter;

    /**
     * 获取用户基本信息
     * @param code
     * @return
     */
    public WechatUserBean getUserInfo(String code)
    {
        JSONObject map  = getAccessToken(code);
        GetUserinfoResponse userInfo=null;
          String openId = map.getString("openid");
          String access_token = map.getString("access_token");
//        if(!isAccessedToken(openId,access_token))//token过期刷新token
//        {
//            Map map1=RefreshAccessToken(access_token);
//            access_token=map1.get("refresh_token")+"";
//        }
        if(access_token==null||access_token.equals("null")){

            throw  new BizException(GlobalErrorCode.AUTH_UNKNOWN,"用户code失效");
        }
        String url="https://api.weixin.qq.com/sns/userinfo?access_token=" + access_token +"&openid="+ openId;
        ResponseEntity<String> entity = restTemplate.getForEntity(url, String.class);
        if(null!=entity) {
            logger.debug("the entity is {}",entity.getBody());
            String body = entity.getBody();
            JSONObject json = JSON.parseObject(body);
            if(null == json.get("errcode")||"".equals(json.get("errcode")+"")) {//获取用户信息 成功
                userInfo = JSONObject.parseObject(entity.getBody(), GetUserinfoResponse.class);
                logger.info("the wechat userinfo is" + userInfo);
                 //设置appId
                userInfo.setAppId(WeChatConfig.appId);
//                 userInfo.setUnionid("feng");
            }
            //根据unionId去获取用户cpid, 判断用户是否存在
            logger.info("==== 微信用户信息获取成功, unionId: {} ====",userInfo.getUnionId());
//                userInfo.setUnionid(UUID.randomUUID().toString().replace("-",""));
               if(userInfo.getUnionId()==null){
                   throw  new BizException(GlobalErrorCode.INTERNAL_ERROR,"该公众号未绑定到开放平台");
               }
            try {
               boolean isUnionIdExists = customerProfileService.isWechatUnionExists(userInfo.getUnionId());
               if (!isUnionIdExists) {
                   if(userInfo.getNickName()!=null&&!userInfo.getNickName().equals("")){
                     userInfo.setNickname( new String(userInfo.getNickName().getBytes("ISO-8859-1"), "UTF-8"));
                   }
                    logger.info("username is  :{}",userInfo.getNickName());
                    logger.info("==== unionId不存在, 创建用户 ====");
                   userService.createUserFromThirdUserIfNotExists(wechatUserConverter,
                           userInfo, null, null);
               }
               List<BaseCustomerVO> customerProfiles = customerProfileService.listCustomerByUnionId(userInfo.getUnionId());

               logger.info("dna***  list value :{}",customerProfiles);

               checkState(CollectionUtils.isNotEmpty(customerProfiles), "用户创建不成功");
           }
           catch (Exception e){
                logger.error("dna***  WxLoginService Line.112",e);
//               logger.info(e.getMessage());
//                throw  new BizException(GlobalErrorCode.INTERNAL_ERROR,"用户创建失败");

           }

        }
        return userInfo;
    }

    /**
     * 获取微信授权码Access_token
     * @param code
     * @return
     */
    public JSONObject getAccessToken(String code)//获取access_token
    {
        logger.debug("code is:{}",code);
        String url="https://api.weixin.qq.com/sns/oauth2/access_token?appid="+ WeChatConfig.appId+"&secret="+WeChatConfig.secert+"&code="+code +"&grant_type=authorization_code";
        ResponseEntity<String> entity=null;
       try {
            entity = restTemplate.getForEntity(url, String.class);

          }catch (Exception e){
           logger.info("access_token获取失败");
           throw  new BizException(GlobalErrorCode.INTERNAL_ERROR,"access_token获取失败");

        }
        if (!entity.hasBody()){throw  new BizException(GlobalErrorCode.INTERNAL_ERROR,"用户code错误");}
        String body = entity.getBody();
        logger.debug("the response for access_token'body:{}",body);
        JSONObject json = JSONObject.parseObject(body);
         log.info("wechat return :"+json.toJSONString());
        return json;
    }


    /**
     * 获取微信授权码Access_token
     * @param code
     * @return
     */
    public Map getAccessTokenBy(String code)//获取access_token
    {
        logger.debug("code is:{}",code);
        String url="https://api.weixin.qq.com/sns/oauth2/access_token?appid="+ WeChatConfig.appId+"&secret="+WeChatConfig.secert+"&code="+code +"&grant_type=authorization_code";
        JSONObject json = sendGet(url);
        logger.debug("the response for access_token'body:{}",json.toString());
        String access_token = json.get("access_token")+"";
        String openId=json.get("openid")+"";
        HashMap<String, String> map = new HashMap<>();
        map.put("access_token",access_token);
        map.put("openId",openId);
        map.put("expires_in",json.get("expires_in")+"");
        map.put("unionid",json.get("unionid")+"");
        return  map;
    }


    /**
     *通过sendGet http请求数据
     * @param url 请求路径
     * @return JSONObject
     */
    private JSONObject sendGet(String url){

        CloseableHttpClient httpclient = HttpClients.createDefault();
        JSONObject jsonResult = null;
        HttpGet method = new HttpGet(url);
        try {
            CloseableHttpResponse result = httpclient.execute(method);
            if (result.getStatusLine().getStatusCode() == 200) {
                String str = "";
                try {
                    str = EntityUtils.toString(result.getEntity());
                    jsonResult = JSONObject.parseObject(str);
                } catch (Exception e) {
                    System.out.println("get请求失败:" + url);
                }
            }
        } catch (IOException e) {
            System.out.println("get请求败:" + url);
        }
        return jsonResult;
    }


    /**
     * 判断access_token是否过期
     * @param openId
     * @param access_token
     * @return
     */
    public boolean isAccessedToken(String openId,String access_token)//判断access_token是否有效
    {
        String url="https://api.weixin.qq.com/sns/auth?access_token=" + access_token +"&openid="+ openId;
        ResponseEntity<String> entity = restTemplate.getForEntity(url, String.class);
        String body = entity.getBody();
        Map map = JSONObject.parseObject(body, Map.class);
        if("ok".equals(map.get("errmsg")+"")) {
            return true;
        }
        return false;
    }

    /**
     * 刷新access_token有效期
     * @param access_token
     * @return
     */
    public Map RefreshAccessToken(String access_token)//刷新access_token
    {
        String url="https://api.weixin.qq.com/sns/oauth2/refresh_token?appid="+ WeChatConfig.appId+"&grant_type=refresh_token&refresh_token="+access_token;
        ResponseEntity<String> entity = restTemplate.getForEntity(url, String.class);
        String body = entity.getBody();
        JSONObject json = JSONObject.parseObject(body);
        HashMap<String, String> map = new HashMap<>();
        map.put("expires_in",json.get("expires_in")+"");
        map.put("refresh_token",json.get("refresh_token")+"");
        return map;
    }






    /**
     * 通过code获取unionid再获取cpId列表
     * @param map
     * @return
     */
    public List<WxVO> getCustomerId(Map map)
    {
        String unionId= (String) map.get("unionId");
        Map<String, Object> customerMap=null;

        if(StringUtils.isNotBlank(unionId))
        {
            return   Optional.ofNullable(customerProfileService.listCusbyUni(unionId)).orElse(null);
        }
        return null ;
    }



}
