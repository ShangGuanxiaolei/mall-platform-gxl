package com.xquark.service.wxservice;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/2/25
 * Time: 9:45
 * Description: 微信返回类
 */
public class JsConfigVo {

    private String appId;

    private String timestamp;

    private String nonceStr;

    private String signature;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
