package com.xquark.service.excel;

import com.xquark.helper.Generator;

/**
 * Created by wangxinhua. Date: 2018/8/22 Time: 下午1:49
 */
public class IndexGenerator implements Generator<Integer> {

  private int start;

  private int init;

  public IndexGenerator() {
    this(1);
  }

  public IndexGenerator(int start) {
    this.start = this.init = start;
  }

  @Override
  public Integer next() {
    return start++;
  }

  @Override
  public Integer[] limit(int times) {
    Integer[] ret = new Integer[times];
    for (int i = 0; i < times; i++) {
      ret[i] = next();
    }
    return ret;
  }

  @Override
  public Generator<Integer> clear() {
    this.start = this.init;
    return this;
  }
}
