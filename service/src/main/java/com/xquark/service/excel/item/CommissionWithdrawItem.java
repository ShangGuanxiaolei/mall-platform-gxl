package com.xquark.service.excel.item;

import static com.xquark.service.excel.ExcelGeneratorHelper.WITHDRAW_NON_ZH_INDEX_KEY;
import static com.xquark.service.excel.ExcelGeneratorHelper.WITHDRAW_ZH_INDEX_KEY;

import com.google.common.base.Optional;
import com.hds.xquark.dal.vo.CommissionWithdrawVO;
import com.xquark.dal.type.PlatformType;
import com.xquark.helper.Generator;
import com.xquark.service.excel.ExcelGeneratorHelper;
import com.xquark.service.excel.IndexAble;
import com.xquark.service.excel.IndexGenerator;
import java.math.BigDecimal;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by wangxinhua. Date: 2018/8/22 Time: 下午2:09 让积分导出数据支持索引值
 */
public class CommissionWithdrawItem extends CommissionWithdrawVO implements IndexAble<Integer> {

  // viviLife平台收取8%手续费
  private final static BigDecimal VIVI_PRECENT = BigDecimal.valueOf(0.92);

  @Override
  public BigDecimal getAmount() {
    BigDecimal amount = Optional.fromNullable(super.getAmount()).or(BigDecimal.ZERO);
    int source = getSource();
    // viivlife平台收取8%手续费
    if (source == PlatformType.V.getCode()) {
      amount = amount.multiply(VIVI_PRECENT);
    }
    return amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
  }

  @Override
  public Generator<Integer> getGenerator() {
    if (StringUtils.equals(this.getBankName(), "中国银行")) {
      return ExcelGeneratorHelper.getGenerator(WITHDRAW_ZH_INDEX_KEY, IndexGenerator.class);
    }
    return ExcelGeneratorHelper.getGenerator(WITHDRAW_NON_ZH_INDEX_KEY, IndexGenerator.class);
  }
}
