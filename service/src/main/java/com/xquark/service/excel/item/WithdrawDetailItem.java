package com.xquark.service.excel.item;

import com.hds.xquark.dal.vo.WithdrawDetailVO;
import com.xquark.helper.Generator;
import com.xquark.service.excel.ExcelGeneratorHelper;
import com.xquark.service.excel.IndexAble;
import com.xquark.service.excel.IndexGenerator;
import com.xquark.utils.DateUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import static com.xquark.service.excel.ExcelGeneratorHelper.WITHDRAW_DETAIL_KEY;

/**
 * @author luqing
 * @since 2019-05-31
 */
public class WithdrawDetailItem extends WithdrawDetailVO implements IndexAble<Integer> {
  @Override
  public Generator<Integer> getGenerator() {
    return ExcelGeneratorHelper.getGenerator(WITHDRAW_DETAIL_KEY, IndexGenerator.class);
  }

  public String getWithdrawDateFormat(){
    return DateFormatUtils.format(super.getWithdrawDate(),"yyyy/MM/dd HH:mm");
  }
}
