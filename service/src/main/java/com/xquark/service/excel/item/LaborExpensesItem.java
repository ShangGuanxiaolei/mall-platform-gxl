package com.xquark.service.excel.item;

import com.hds.xquark.dal.vo.LaborExpensesVO;
import com.xquark.helper.Generator;
import com.xquark.service.excel.ExcelGeneratorHelper;
import com.xquark.service.excel.IndexAble;
import com.xquark.service.excel.IndexGenerator;

import static com.xquark.service.excel.ExcelGeneratorHelper.LABOR_EXPENSES_KEY;

/**
 * @author luqing
 * @since 2019-06-06
 */
public class LaborExpensesItem extends LaborExpensesVO implements IndexAble<Integer> {
    @Override
    public Generator<Integer> getGenerator() {
        return ExcelGeneratorHelper.getGenerator(LABOR_EXPENSES_KEY, IndexGenerator.class);
    }
}
