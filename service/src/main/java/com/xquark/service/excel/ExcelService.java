package com.xquark.service.excel;

import com.xquark.utils.excel.SheetContent;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

public interface ExcelService {

  void export(String filePrefix, List<? extends SheetContent<?>> sheetWrappers,
      HttpServletResponse resp, boolean dateMark);

  @SuppressWarnings("rawtypes")
  public void export(String filePrefix, List objList, Class objClass, String sheetStr,
      String firstTitle, String[] secondTitle, String[] strBody, HttpServletResponse resp,
      boolean withTitle);
}
