package com.xquark.service.excel;

import static org.parboiled.common.Preconditions.checkNotNull;

import com.google.common.base.Preconditions;
import com.xquark.helper.Generator;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by wangxinhua. Date: 2018/8/22 Time: 下午1:53 帮助excel生成一些需要自生成的属性
 */
public class ExcelGeneratorHelper {

  private static final Logger LOGGER = LoggerFactory.getLogger(ExcelGeneratorHelper.class);

  public static String WITHDRAW_ZH_INDEX_KEY = "WITHDRAW_ZH";
  public static String WITHDRAW_NON_ZH_INDEX_KEY = "WITHDRAW_NON_ZH";
  public static String WITHDRAW_DETAIL_KEY = "WITHDRAW_DETAIL";
  public static String LABOR_EXPENSES_KEY = "LABOR_EXPENSES_KEY";


  private final static Map<String, Generator<?>> GENERATOR_MAP
      = new ConcurrentHashMap<>();

  /**
   * 获取生成器
   *
   * @param key 使用相同的key保证获取同一个生成器
   * @param <T> 生成器值泛型类型
   * @param <S> 生成器泛型类型
   * @return 根据key获取的生成器
   */
  public static <T, S extends Generator<T>> Generator<T> getGenerator(String key, Class<S> clazz) {
    @SuppressWarnings("unchecked")
    Generator<T> generator = (Generator<T>) GENERATOR_MAP.get(key);
    if (generator != null) {
      return generator;
    }
    try {
      generator = clazz.newInstance();
    } catch (InstantiationException | IllegalAccessException e) {
      // FIXME 生成器可能需要使用自定义构造函数创建
      LOGGER.error("反射创建生成器失败, 请检查是否有默认的无参构造函数", e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "反射创建生成器失败");
    }
    GENERATOR_MAP.put(key, generator);
    return generator;
  }

  /**
   * 重置key对应的生成器
   *
   * @param key 生成器键
   */
  public static void reset(String key) {
    Generator generator = GENERATOR_MAP.get(key);
    if (generator != null) {
      generator.clear();
    }
  }

}
