package com.xquark.service.excel.impl;

import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.excel.ExcelService;
import com.xquark.utils.ExcelUtils;
import com.xquark.utils.excel.SheetContent;
import com.xquark.utils.excel.SheetHeader;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.parboiled.common.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("excelService")
public class ExcelServiceImpl implements ExcelService {

  private Logger log = LoggerFactory.getLogger(ExcelServiceImpl.class);

  @Value("${bos.export.tempDir}")
  private String exportTempDir;

  @Override
  public void export(String filePrefix, List<? extends SheetContent<?>> sheetWrappers,
      HttpServletResponse resp, boolean dateMark) {
    String excelFolderPath = exportTempDir;
    if (!excelFolderPath.endsWith("/")) {
      excelFolderPath += File.separator;
    }
    File folderPath = new File(excelFolderPath);
    if (folderPath.exists() && folderPath.isDirectory()) {
      String fileName;
      if (dateMark) {
        fileName = filePrefix + "_" + curDate() + ".xls";
      } else {
        fileName = filePrefix + ".xls";
      }
      String excelFilePath = excelFolderPath + fileName;
        ExcelUtils
            .exportExcelByObject(sheetWrappers, excelFilePath);
      try {
        download(fileName, excelFilePath, resp);
      } catch (Exception e) {
        log.debug("文件下载异常：", e);
      }
    } else {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "临时文件夹不存在");
    }
  }

  @SuppressWarnings("rawtypes")
  @Override
  public void export(String filePrefix, List objList, Class objClass, String sheetStr,
      String firstTitle, String[] secondTitle, String[] strBody, HttpServletResponse resp,
      boolean withTitle) {
    List<SheetContent<Object>> singleList = ImmutableList.of(
        new SheetContent<Object>(filePrefix, objList, objClass, strBody,
            Collections.<SheetHeader>singletonList(SheetHeader
                .Builder.start(secondTitle).build())));
    export(filePrefix, singleList, resp, true);
  }

  public static String curDate() {
    Date date = new Date(System.currentTimeMillis());
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
        "yyyyMMddHHmmss");
    return simpleDateFormat.format(date);
  }

  public void download(String fileName, String filePath, HttpServletResponse resp)
      throws Exception {
    BufferedInputStream bis = null;
    BufferedOutputStream bos = null;
    try {
      resp.setHeader("Content-Disposition", "attachment; filename=\""
          + URLEncoder.encode(fileName, "UTF-8") + "\"");
      resp.setContentType("application/octet-stream; charset=UTF-8");
      bis = new BufferedInputStream(new FileInputStream(filePath));
      bos = new BufferedOutputStream(resp.getOutputStream());
      byte[] buff = new byte[2048];
      int bytesRead;
      while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
        bos.write(buff, 0, bytesRead);
      }
    } catch (Exception e) {
      log.debug("excel 下载出错", e);
    } finally {
      if (bis != null) {
        bis.close();
      }
      if (bos != null) {
        bos.close();
      }
      File tempFile = new File(filePath);
      if (tempFile != null) {
        tempFile.delete();
      }
    }
  }
}
