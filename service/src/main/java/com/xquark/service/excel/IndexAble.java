package com.xquark.service.excel;

import com.xquark.helper.Generator;

/**
 * Created by wangxinhua. Date: 2018/8/22 Time: 下午2:06 表示excel对象可以获取索引值
 */
public interface IndexAble<T> {

  Generator<T> getGenerator();

}
