package com.xquark.service.activityOrder;

        import java.util.List;
        import java.util.Map;

        import org.springframework.data.domain.Pageable;

        import com.xquark.dal.model.ActivityOrder;

public interface ActivityOrderService {

    int insertRandom(String activityId, String partnerType);

    int insertDirectByOrderNo(String activityId, String orderNo);

    int updateStatusConfirmed(String id);

    int updateStatusFinish(String id);

    int updateStatusCancel(String id);

    List<ActivityOrder> listByAdmin(Map<String, Object> params, Pageable page);

    int countByAdmin(Map<String, Object> params);

    ActivityOrder load(String id);
}
