package com.xquark.service.activityOrder.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.ActivityOrderMapper;
import com.xquark.dal.model.ActivityOrder;
import com.xquark.service.activityOrder.ActivityOrderService;

@Service("activityOrderService")
public class ActivityOrderServiceImpl implements ActivityOrderService {

  @Autowired
  private ActivityOrderMapper activityOrderMapper;

  @Override
  public int insertRandom(String activityId, String partnerType) {
    return activityOrderMapper.insertRandom(activityId, partnerType);
  }

  @Override
  public int insertDirectByOrderNo(String activityId, String orderNo) {
    return activityOrderMapper.insertDirectByOrderNo(activityId, orderNo);
  }

  @Override
  public int updateStatusConfirmed(String id) {
    return activityOrderMapper.updateStatusConfirmed(id);
  }

  @Override
  public int updateStatusFinish(String id) {
    return activityOrderMapper.updateStatusFinish(id);
  }

  @Override
  public int updateStatusCancel(String id) {
    return activityOrderMapper.updateStatusCancel(id);
  }

  @Override
  public List<ActivityOrder> listByAdmin(Map<String, Object> params, Pageable page) {
    return activityOrderMapper.listByAdmin(params, page);
  }

  @Override
  public int countByAdmin(Map<String, Object> params) {
    return activityOrderMapper.countByAdmin(params);
  }

  @Override
  public ActivityOrder load(String id) {
    return activityOrderMapper.selectByPrimaryKey(id);
  }

}
