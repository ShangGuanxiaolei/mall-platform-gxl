package com.xquark.service.activityOrder;

import java.util.List;

import com.xquark.dal.model.UserPayBankVO;
import com.xquark.service.address.AddressVO;
import com.xquark.service.address.AddressVOEX;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.pricing.CouponVO;
import com.xquark.service.pricing.vo.CartPricingResultVO;

public class ConfirmOrderVO {

    private List<CartItemVO> cartItems;

    private CartPricingResultVO prices;

    private AddressVOEX addressEX;

    private List<CouponVO> coupons;


    private List<UserPayBankVO> userAgreeBanks;

    public List<CartItemVO> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItemVO> cartItems) {
        this.cartItems = cartItems;
    }

    public CartPricingResultVO getPrices() {
        return prices;
    }

    public void setPrices(CartPricingResultVO prices) {
        this.prices = prices;
    }

    public AddressVOEX getAddressEX() {
        return addressEX;
    }

    public void setAddressEX(AddressVOEX addressEX) {
        this.addressEX = addressEX;
    }

    public List<UserPayBankVO> getUserAgreeBanks() {
        return userAgreeBanks;
    }

    public void setUserAgreeBanks(List<UserPayBankVO> userAgreeBanks) {
        this.userAgreeBanks = userAgreeBanks;
    }

    public List<CouponVO> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<CouponVO> coupons) {
        this.coupons = coupons;
    }


}
