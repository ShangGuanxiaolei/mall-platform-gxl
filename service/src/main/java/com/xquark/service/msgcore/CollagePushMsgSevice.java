package com.xquark.service.msgcore;

import com.xquark.dal.model.CollagePushMessage;

/**
 * push通道接口
 * @author tanggb
 *
 */
public interface CollagePushMsgSevice {

	/**
	 * 保存拼团信息
	 *
	 * @param collagePushMessage push消息实体类
	 * @return Boolean
	 */
	 Boolean sendMessage(CollagePushMessage collagePushMessage);
}
