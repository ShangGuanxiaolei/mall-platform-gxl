package com.xquark.service.msgcore;

import com.xquark.dal.model.CollageSmsMessage;

/**
 * 拼团短信
 * @author  tanggb
 */
public interface CollageSmsMsgService{

    /**
     *保存拼团短信消息
     * @param collageSmsMessage
     * @return int
     */
    boolean sendMessage(CollageSmsMessage collageSmsMessage);

}