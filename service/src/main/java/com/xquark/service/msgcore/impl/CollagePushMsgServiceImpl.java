package com.xquark.service.msgcore.impl;

import com.xquark.dal.mapper.CollagePushMessageMapper;
import com.xquark.dal.model.CollagePushMessage;
import com.xquark.service.msgcore.CollagePushMsgSevice;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author tanggb
 * Create in 14:58 2018/9/5
 */
@Service
public class CollagePushMsgServiceImpl implements CollagePushMsgSevice {

	@Autowired
	private CollagePushMessageMapper collageMessageMapper;


	@Override
	public Boolean sendMessage(CollagePushMessage collagePushMessage) {
		Boolean insert = collageMessageMapper.insert(collagePushMessage);
		return insert;
	}
}
