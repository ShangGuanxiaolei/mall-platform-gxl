package com.xquark.service.msgcore.impl;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.mapper.CollageSmsMessageMapper;
import com.xquark.dal.model.CollageSmsMessage;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.msgcore.CollageSmsMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;



/**
 * 短信实现类
 *
 * @author tanggb
 * Create in 14:58 2018/9/5
 */
@Service
public class CollageSmsMsgServiceImpl extends BaseServiceImpl implements CollageSmsMsgService {

    private final String LOCALHOST = "http://127.0.0.1:8080";

    @Autowired
    private CollageSmsMessageMapper collageSmsMessageMapper;

    @Autowired
    private RestOperations restTemplate;

    @Override
    public boolean sendMessage(CollageSmsMessage collageSmsMessage) {
        //1.保存短信消息
        collageSmsMessageMapper.insert(collageSmsMessage);
        //2.发送短信
        boolean b = restGet(collageSmsMessage.getIdRaw());
        return b;
    }

    /**
     * rest调用拼团短信
     * @param id
     * @return
     */
    private boolean restGet(long id){
        String url = LOCALHOST + "/sms/pieceSmsSend?id=" + id;
        String str = restTemplate.getForObject(url,String.class, new Object[]{});
        JSONObject json = JSONObject.parseObject(str);
        int code = json.getIntValue("errorCode");
        return json.getIntValue("errorCode") == 200;
    }

}

