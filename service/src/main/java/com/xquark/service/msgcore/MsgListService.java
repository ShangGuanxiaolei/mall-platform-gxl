package com.xquark.service.msgcore;

import com.xquark.dal.model.CollagePushMessage;
import com.xquark.dal.page.PageHelper;
import com.xquark.service.BaseService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author: zzl
 * @Date: 2018/9/6 16:51
 * @Version
 */
public interface MsgListService {


  /**
   * push消息列表查询
   * @param belognTo
   * @param pageNum
   * @param pageCount 每页显示数量
   * @return
   */
  PageHelper<CollagePushMessage> selectMsgList(Long belognTo,String TypeMsg, Integer pageNum, Integer pageCount);

  /**
   * push未读消息列表查询
   * @param belognTo
   * @param pageNum
   * @param pageCount 每页显示数量
   * @return List<CollagePushMessage>
   */
  List<CollagePushMessage> selectMsgUnReadList(String belognTo, Integer pageNum, Integer pageCount);

  /**
   * push消息状态修改
   * @param id
   * @return void
   *
   */
  Boolean updateMsgStatus(String id);

  /**
   *根据cpid查询push消息
   * @param cpid
   * @return
   */
  int selectMsgByCpId(Long cpid);


  Map<String,Object> typeList(Long cpid, String type);

  /**
   * 删除消息
   * @param listId
   * @return
   */
  boolean deleteMsg(List<Integer> listId);

}
