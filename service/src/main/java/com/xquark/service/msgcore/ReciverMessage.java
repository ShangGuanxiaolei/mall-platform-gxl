package com.xquark.service.msgcore;

import com.xquark.dal.model.CollagePushMessage;
import com.xquark.dal.model.CollageSmsMessage;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

/**
 * 统一接收消息实体类
 *
 * @author tanggb
 * Create in 21:02 2018/9/21
 */
@Service
public class ReciverMessage {
    /**
     * push消息实体类
     */
    private CollagePushMessage collagePushMessage;
    /**
     * Sms消息实体类
     */
    private CollageSmsMessage collageSmsMessage;
    /**
     * 消息通道类型
     */
    private String type;

    public CollagePushMessage getCollagePushMessage() {
        return collagePushMessage;
    }

    public void setCollagePushMessage(CollagePushMessage collagePushMessage) {
        this.collagePushMessage = collagePushMessage;
    }

    public CollageSmsMessage getCollageSmsMessage() {
        return collageSmsMessage;
    }

    public void setCollageSmsMessage(CollageSmsMessage collageSmsMessage) {
        this.collageSmsMessage = collageSmsMessage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
