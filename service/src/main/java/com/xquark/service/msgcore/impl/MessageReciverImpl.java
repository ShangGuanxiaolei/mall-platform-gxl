package com.xquark.service.msgcore.impl;

import com.xquark.service.bill.impl.bw.FieldConstants;
import com.xquark.service.msgcore.CollagePushMsgSevice;
import com.xquark.service.msgcore.CollageSmsMsgService;
import com.xquark.service.msgcore.MessageReciver;
import com.xquark.service.msgcore.ReciverMessage;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 消息接收接口实现类
 * @author tanggb
 *
 */
@Service
public class MessageReciverImpl implements MessageReciver {


    @Autowired
    private CollagePushMsgSevice collagePushSevice;

    @Autowired
    private CollageSmsMsgService collageSmsMsgService;


    @Override
    public boolean reciverMsg(ReciverMessage reciverMessage) {

        String type = reciverMessage.getType();
        //type没有指定通道，则为两个消息同时发送
        if (StringUtils.isEmpty(type)) {
            Boolean aPush = collagePushSevice.sendMessage(reciverMessage.getCollagePushMessage());
            if(aPush){
                boolean aSms = collageSmsMsgService.sendMessage(reciverMessage.getCollageSmsMessage());
                return aSms;
            }
            return false;
        }
        //只发送push消息
        if(type.equals(FieldConstants.PUSH_CHANNEL)){
            Boolean bPush = collagePushSevice.sendMessage(reciverMessage.getCollagePushMessage());
            return bPush;
        }
        //只发送短信消息
        if(type.equals(FieldConstants.SMS_CHANNEL)){
            boolean bSms = collageSmsMsgService.sendMessage(reciverMessage.getCollageSmsMessage());
            return bSms;
        }
        return false;
    }

}
