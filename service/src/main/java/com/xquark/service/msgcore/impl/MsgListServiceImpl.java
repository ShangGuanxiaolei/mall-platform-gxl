package com.xquark.service.msgcore.impl;

import com.xquark.dal.mapper.MsgListMapper;
import com.xquark.dal.model.CollagePushMessage;
import com.xquark.dal.page.PageHelper;
import com.xquark.service.msgcore.MsgListService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: zzl
 * @Date: 2018/9/6 16:52
 * @Version
 */
@Service("msgListService")
public class MsgListServiceImpl implements MsgListService {

  @Autowired
  private MsgListMapper msgListMapper;


  /**
   * push消息列表查询
   * @param belognTo 查看谁的信息
   * @param pageNum 每30条一页，查询第几页。之前查询的条数前端缓存起来，当页数为n时，一共显示n*30条信息
   * @return List<CollagePushMessage>
   */
  @Override
  public PageHelper<CollagePushMessage> selectMsgList(Long belognTo,String TypeMsg, Integer pageNum,Integer pageCount) {

    PageHelper<CollagePushMessage> helper = new PageHelper<>();
    helper.setPage(pageNum);
    helper.setPageSize(pageCount);
    Integer pageNumber = (pageNum-1)*pageCount;
    List<CollagePushMessage> collagePushMessages = msgListMapper.selectMsgList(belognTo,TypeMsg, pageNumber, pageCount);
    if(collagePushMessages != null){
      helper.setResult(collagePushMessages);
      return helper;
    }
    return null;
  }

  /**
   * push未读消息列表查询
   * @param belognTo
   * @param pageNum
   * @return List<CollagePushMessage>
   */
  @Override
  public List<CollagePushMessage> selectMsgUnReadList(String belognTo, Integer pageNum,Integer pageCount) {
    Integer pageNumber = (pageNum-1)*pageCount;
    return msgListMapper.selectMsgUnReadList(belognTo, pageNumber,pageCount);
  }

  /**
   * push消息状态修改
   * @param id
   * @return void
   *
   */
  @Override
  public Boolean updateMsgStatus(String id) {
    Boolean aBoolean = msgListMapper.updateMsgStatus(id);
    return aBoolean;
  }


    @Override
    public int selectMsgByCpId(Long cpid) {
        return msgListMapper.selectMsgByCpId(cpid);
    }

    @Override
    public Map<String ,Object> typeList(Long cpid, String type) {
        Map<String,Object> map=new HashMap<>();
        int count=msgListMapper.selectTypeCount(cpid,type);
        String content=msgListMapper.selectContent(cpid,type);
        map.put("count",count);
        map.put("content",content);
        return map;
    }

  @Override
  public boolean deleteMsg(List<Integer> listId) {
    boolean delete=true;
    try {
      for (int i = 0; i < listId.size(); i++) {
        delete = msgListMapper.deleteMsg(listId.get(i));
      }
    }catch (Exception e){
      e.printStackTrace();
    }
    return delete;
  }

}
