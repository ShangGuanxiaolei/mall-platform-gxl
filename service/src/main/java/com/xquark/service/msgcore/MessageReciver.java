package com.xquark.service.msgcore;


/**
 * 消息接收接口
 * @author tanggb
 *
 */
public interface MessageReciver {

    /**
     * 消息接收抽象类
     *
     * @param reciverMessage 内嵌type与实体类 type: 固定通道（push，sms）
     */
    boolean reciverMsg(ReciverMessage reciverMessage);

}
