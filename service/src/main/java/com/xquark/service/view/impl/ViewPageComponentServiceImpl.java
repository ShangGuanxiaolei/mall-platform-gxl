package com.xquark.service.view.impl;

import com.xquark.dal.mapper.ViewPageComponentMapper;
import com.xquark.dal.model.ViewPageComponent;
import com.xquark.service.view.ViewPageComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by chh on 16-10-17.
 */
@Service("viewPageComponentServiceImpl")
public class ViewPageComponentServiceImpl implements ViewPageComponentService {

  @Autowired
  private ViewPageComponentMapper viewPageComponentMapper;

  @Override
  public ViewPageComponent selectByPrimaryKey(String id) {
    return viewPageComponentMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(ViewPageComponent viewPageComponent) {
    return viewPageComponentMapper.insert(viewPageComponent);
  }

  @Override
  public int modifyViewPageComponent(ViewPageComponent viewPageComponent) {
    return viewPageComponentMapper.modifyViewPageComponent(viewPageComponent);
  }

  @Override
  public int delete(String id) {
    return viewPageComponentMapper.delete(id);
  }

  /**
   * 找到pageid对应的所有组件配置信息
   */
  @Override
  public List<ViewPageComponent> findByPageId(String pageId) {
    return viewPageComponentMapper.findByPageId(pageId);
  }
}
