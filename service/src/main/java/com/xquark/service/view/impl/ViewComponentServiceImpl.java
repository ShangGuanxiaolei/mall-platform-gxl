package com.xquark.service.view.impl;

import com.xquark.dal.mapper.ViewComponentMapper;
import com.xquark.dal.model.ViewComponent;
import com.xquark.service.view.ViewComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by chh on 16-10-17.
 */
@Service("viewComponentServiceImpl")
public class ViewComponentServiceImpl implements ViewComponentService {

  @Autowired
  private ViewComponentMapper viewComponentMapper;

  @Override
  public ViewComponent selectByPrimaryKey(String id) {
    return viewComponentMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(ViewComponent viewComponent) {
    return viewComponentMapper.insert(viewComponent);
  }

  @Override
  public int modifyViewComponent(ViewComponent viewComponent) {
    return viewComponentMapper.modifyViewComponent(viewComponent);
  }

  @Override
  public int delete(String id) {
    return viewComponentMapper.delete(id);
  }

  /**
   * 列表查询页面渲染组件
   */
  @Override
  public List<ViewComponent> list(Pageable page, String keyword) {
    return viewComponentMapper.list(page, keyword);
  }

  /**
   * 对分页页面渲染组件列表接口取总数
   */
  @Override
  public Long selectCnt(String keyword) {
    return viewComponentMapper.selectCnt(keyword);
  }
}
