package com.xquark.service.view;

import com.xquark.dal.model.ViewPage;
import com.xquark.dal.vo.ViewPageComponentVO;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by chh on 16-10-17. 用户自定义页面Service
 */
public interface ViewPageService {

  ViewPage selectByPrimaryKey(String id);

  int insert(ViewPage viewPage);

  int modifyViewPage(ViewPage viewPage);

  int delete(String id);

  /**
   * 列表查询自定义页面
   */
  List<ViewPage> list(Pageable page, String keyword);

  /**
   * 对分页自定义页面列表接口取总数
   */
  Long selectCnt(String keyword);

  /**
   * 列表查询自定义页面关联页面组件
   */
  List<ViewPageComponentVO> listComponent(Pageable page, String id);

  /**
   * 对分页自定义页面关联页面组件列表接口取总数
   */
  Long selectComponentCnt(String id);

}
