package com.xquark.service.view;

import com.xquark.dal.model.MarketingTweet;
import com.xquark.dal.model.ViewComponent;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by chh on 16-10-17. 页面渲染组件Service
 */
public interface ViewComponentService {

  ViewComponent selectByPrimaryKey(String id);

  int insert(ViewComponent viewComponent);

  int modifyViewComponent(ViewComponent viewComponent);

  int delete(String id);

  /**
   * 列表查询页面渲染组件
   */
  List<ViewComponent> list(Pageable page, String keyword);

  /**
   * 对分页页面渲染组件列表接口取总数
   */
  Long selectCnt(String keyword);

}
