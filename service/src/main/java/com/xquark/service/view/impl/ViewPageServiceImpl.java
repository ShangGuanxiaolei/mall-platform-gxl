package com.xquark.service.view.impl;

import com.xquark.dal.mapper.ViewPageMapper;
import com.xquark.dal.model.ViewPage;
import com.xquark.dal.vo.ViewPageComponentVO;
import com.xquark.service.view.ViewPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by chh on 16-10-17.
 */
@Service("viewPageServiceImpl")
public class ViewPageServiceImpl implements ViewPageService {

  @Autowired
  private ViewPageMapper viewPageMapper;

  @Override
  public ViewPage selectByPrimaryKey(String id) {
    return viewPageMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(ViewPage viewPage) {
    return viewPageMapper.insert(viewPage);
  }

  @Override
  public int modifyViewPage(ViewPage viewPage) {
    return viewPageMapper.modifyViewPage(viewPage);
  }

  @Override
  public int delete(String id) {
    return viewPageMapper.delete(id);
  }

  /**
   * 列表查询自定义页面
   */
  @Override
  public List<ViewPage> list(Pageable page, String keyword) {
    return viewPageMapper.list(page, keyword);
  }

  /**
   * 对分页自定义页面列表接口取总数
   */
  @Override
  public Long selectCnt(String keyword) {
    return viewPageMapper.selectCnt(keyword);
  }

  /**
   * 列表查询自定义页面关联页面组件
   */
  @Override
  public List<ViewPageComponentVO> listComponent(Pageable page, String id) {
    return viewPageMapper.listComponent(page, id);
  }

  /**
   * 对分页自定义页面关联页面组件列表接口取总数
   */
  @Override
  public Long selectComponentCnt(String id) {
    return viewPageMapper.selectComponentCnt(id);
  }
}
