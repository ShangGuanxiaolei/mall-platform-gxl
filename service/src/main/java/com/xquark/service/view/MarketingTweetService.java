package com.xquark.service.view;

import com.xquark.dal.model.MarketingTweet;
import com.xquark.dal.model.TweetImage;
import com.xquark.service.view.vo.MarketingTweetVO;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

/**
 * Created by chh on 16-10-17. 发现模板Service
 */
public interface MarketingTweetService {

  MarketingTweet selectByPrimaryKey(String id);

  int insert(MarketingTweet marketingTweet, List<TweetImage> imgs);

  int modifyMarketingTweet(MarketingTweet marketingTweet, List<TweetImage> imgs);

  int delete(String id);

  /**
   * 列表查询发现
   */
  List<MarketingTweet> list(Pageable page, String keyword);

  /**
   * 对分页发现列表接口取总数
   */
  Long selectCnt(String keyword);

  /**
   * 根据id查找对应记录
   */
  MarketingTweetVO load(String id);

  /**
   * app端发现列表查询发现
   */
  List<MarketingTweet> listApp(Pageable page);

  /**
   * 供客户端调用，获取首页品牌/专题
   */
  List<MarketingTweetVO> getHomeForApp();

}
