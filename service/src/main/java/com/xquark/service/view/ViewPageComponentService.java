package com.xquark.service.view;

import com.xquark.dal.model.ViewPageComponent;

import java.util.List;

/**
 * Created by chh on 16-10-17. 自定义页面所使用的组件Service
 */
public interface ViewPageComponentService {

  ViewPageComponent selectByPrimaryKey(String id);

  int insert(ViewPageComponent viewPageComponent);

  int modifyViewPageComponent(ViewPageComponent viewPageComponent);

  int delete(String id);

  /**
   * 找到pageid对应的所有组件配置信息
   */
  List<ViewPageComponent> findByPageId(String pageId);

}
