package com.xquark.service.view.impl;

import com.xquark.dal.mapper.MarketingTweetMapper;
import com.xquark.dal.mapper.TweetImageMapper;
import com.xquark.dal.model.MarketingTweet;
import com.xquark.dal.model.TweetImage;
import com.xquark.service.view.MarketingTweetService;
import com.xquark.service.view.vo.MarketingTweetVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by chh on 16-10-17.
 */
@Service("marketingTweetServiceImpl")
public class MarketingTweetServiceImpl implements MarketingTweetService {

  protected Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private MarketingTweetMapper marketingTweetMapper;

  @Autowired
  private TweetImageMapper tweetImageMapper;

  @Override
  public MarketingTweet selectByPrimaryKey(String id) {
    return marketingTweetMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(MarketingTweet marketingTweet, List<TweetImage> imgs) {
    int x = marketingTweetMapper.insert(marketingTweet);
    saveImgs(marketingTweet, imgs);
    return x;
  }

  @Override
  public int modifyMarketingTweet(MarketingTweet marketingTweet, List<TweetImage> imgs) {
    saveImgs(marketingTweet, imgs);
    return marketingTweetMapper.modifyMarketingTweet(marketingTweet);
  }

  @Override
  public int delete(String id) {
    return marketingTweetMapper.delete(id);
  }

  /**
   * 列表查询发现
   */
  @Override
  public List<MarketingTweet> list(Pageable page, String keyword) {
    return marketingTweetMapper.list(page, keyword);
  }

  /**
   * 对分页发现列表接口取总数
   */
  @Override
  public Long selectCnt(String keyword) {
    return marketingTweetMapper.selectCnt(keyword);
  }

  /**
   * 根据id查找对应记录
   */
  @Override
  public MarketingTweetVO load(String id) {
    MarketingTweet p = marketingTweetMapper.selectByPrimaryKey(id);
    if (p == null) {
      log.warn("您要查看的发现" + id + "不存在");
      return null;
    }

    final MarketingTweetVO vo = new MarketingTweetVO(p);
    // 图片
    final List<TweetImage> imgs = tweetImageMapper.selectByTweetId(vo.getId());
    for (final TweetImage i : imgs) {
      i.setImgUrl(i.getImg());
    }
    vo.setImgs(imgs);
    return vo;
  }

  /**
   * app端发现列表查询发现
   */
  @Override
  public List<MarketingTweet> listApp(Pageable page) {
    return marketingTweetMapper.listApp(page);
  }

  // update product img, do it like newspider, archive all and then insert all
  private void saveImgs(MarketingTweet marketingTweet,
      List<TweetImage> imgs) {
    if (imgs == null || marketingTweet == null) {
      return;
    }
    tweetImageMapper.updateForArchiveByParam(marketingTweet.getId());

    int idx = 0;
    for (final TweetImage img : imgs) {
      img.setTweetId(marketingTweet.getId());
      img.setImgOrder(idx++);
      tweetImageMapper.insert(img);
    }
  }

  /**
   * 供客户端调用，获取首页品牌/专题
   */
  @Override
  public List<MarketingTweetVO> getHomeForApp() {
    List<MarketingTweetVO> vos = new ArrayList<MarketingTweetVO>();
    List<MarketingTweet> tweets = marketingTweetMapper.getHomeForApp();
    for (MarketingTweet tweet : tweets) {
      final MarketingTweetVO vo = new MarketingTweetVO(tweet);
      // 图片
      final List<TweetImage> imgs = tweetImageMapper.selectByTweetId(vo.getId());
      for (final TweetImage i : imgs) {
        i.setImgUrl(i.getImg());
      }
      vo.setImgs(imgs);
      vos.add(vo);
    }
    return vos;
  }

}
