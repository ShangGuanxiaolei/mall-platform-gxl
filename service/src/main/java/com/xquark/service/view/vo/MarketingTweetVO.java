package com.xquark.service.view.vo;

import com.xquark.dal.model.MarketingTweet;
import com.xquark.dal.model.TweetImage;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * Created by root on 16-10-19.
 */
public class MarketingTweetVO extends MarketingTweet {


  private List<TweetImage> imgs;

  public List<TweetImage> getImgs() {
    return imgs;
  }

  public void setImgs(List<TweetImage> imgs) {
    this.imgs = imgs;
  }

  public MarketingTweetVO(MarketingTweet marketingTweet) {
    BeanUtils.copyProperties(marketingTweet, this);
  }
}
