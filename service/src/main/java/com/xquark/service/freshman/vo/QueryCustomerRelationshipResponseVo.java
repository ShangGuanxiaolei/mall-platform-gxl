package com.xquark.service.freshman.vo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/6/20
 * Time:16:35
 * Des:
 */
public class QueryCustomerRelationshipResponseVo {

    //    返回数据格式
//    {
//        "res_code": 11,
//         "res_msg": "弱关系"
//    }

    //    10：cpid不存在
//    11：弱关系
//    12：次强关系
//    13：强关系
    private int resCode;
    private String resMsg;
    private Long upline;

    public Long getUpline() {
        return upline;
    }

    public void setUpline(Long upline) {
        this.upline = upline;
    }

    public int getResCode() {
        return resCode;
    }

    public void setResCode(int resCode) {
        this.resCode = resCode;
    }

    public String getResMsg() {
        return resMsg;
    }

    public void setResMsg(String resMsg) {
        this.resMsg = resMsg;
    }

    @Override
    public String toString() {
        return "QueryCustomerRelationshipResponseVo{" +
                "resCode=" + resCode +
                ", resMsg='" + resMsg + '\'' +
                ", upline=" + upline +
                '}';
    }
}