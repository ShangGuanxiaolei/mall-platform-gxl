package com.xquark.service.freshman.impl;

import com.xquark.dal.mapper.FreshManProductMapper;
import com.xquark.dal.mapper.PromotionSkusMapper;
import com.xquark.dal.model.FreshManProductVo;
import com.xquark.service.freshman.FreshManProductService;
import com.xquark.service.freshman.pojo.FreshManProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @类名: FreshManProductServiceImpl
 * @描述: TODO .
 * @程序猿: LuXiaoLing
 * @日期: 2019/3/6 18:26
 * @版本号: V1.0 .
 */


@Service("freshManProductService")
public class FreshManProductServiceImpl implements FreshManProductService{
    @Autowired
    private FreshManProductMapper freshManProductMapper;
    @Autowired
    private PromotionSkusMapper promotionSkusMapper;
    /**
     * 功能描述: 根据skuid判断该商品是不是属于新人专区的商品
     * @author Luxiaoling
     * @date 2019/3/7 14:08
     * @param
     * @return  com.xquark.service.freshman.pojo.FreshManProduct
     */
    @Override
    public List<FreshManProductVo> selectByProductId(String productId) {
        return freshManProductMapper.selectByProductId(productId);
    }
    /**
     * 功能描述:查询新人专区商品配置的特殊邮费
     * @author Luxiaoling
     * @date 2019/3/9 14:49
     * @param
     * @return  java.math.BigDecimal
     */
    @Override
    public BigDecimal selectFreshmanLogisticsFee() {
        return freshManProductMapper.selectFreshmanLogisticsFee();
    }

    @Override
    public List<FreshManProductVo> selectBySkuId(String skuId) {
        return freshManProductMapper.selectBySkuId(skuId);
    }

    @Override
    public int countBySku(String skuCode, String productId) {
        return promotionSkusMapper.countBySku(skuCode,productId);
    }

}
