package com.xquark.service.freshman;

import com.xquark.dal.model.FreshManFlagVo;
import com.xquark.service.freshman.pojo.FreshManToast;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/11
 * Time:10:56
 * Des:新人弹框相关
 */
public interface FreshManFlagService {
    /**
     * 新人flag表中是否已经存在
     * @param cpId
     * @return
     */
    boolean isExistFlagData(Long cpId);

    /**
     * 插入一条数据
     * @return
     */
    boolean insertFlag4Toast(FreshManToast freshManToast);

    /**
     * 更新数据
     * @return
     */
    boolean updateFlag4Toast(FreshManToast freshManToast);

    /**
     * 插入一条数据
     * @return
     */
    boolean insert(FreshManFlagVo f);

    /**
     * 更新数据
     * @return
     */
    boolean update(FreshManFlagVo f);

    /**
     * 更新记录vip时间
     * @param f
     * @return
     */
    boolean updateVipTime(FreshManToast f);

    FreshManFlagVo selectFlag4Toast(Long cpId);
}