package com.xquark.service.freshman.vo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/6/25
 * Time:15:12
 * Des:
 */
public class RelationshipInfoVo {
    private Long cpId;
    private Long upLine;
    private int level; // 关系级别
    private String levelName; // 关系说明
    private String remark; // 说明关系来源

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }

    public Long getUpLine() {
        return upLine;
    }

    public void setUpLine(Long upLine) {
        this.upLine = upLine;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "RelationshipInfoVo{" +
                "cpId=" + cpId +
                ", upLine=" + upLine +
                ", level=" + level +
                ", levelName='" + levelName + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}