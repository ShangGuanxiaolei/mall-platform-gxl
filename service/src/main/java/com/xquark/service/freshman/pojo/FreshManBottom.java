package com.xquark.service.freshman.pojo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/6
 * Time:11:17
 * Des:
 */
public class FreshManBottom {
    private String title;
    private String imgUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}