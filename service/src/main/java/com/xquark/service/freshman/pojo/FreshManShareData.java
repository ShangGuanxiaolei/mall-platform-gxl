package com.xquark.service.freshman.pojo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/6
 * Time:11:29
 * Des：新人分享数据
 */
public class FreshManShareData {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}