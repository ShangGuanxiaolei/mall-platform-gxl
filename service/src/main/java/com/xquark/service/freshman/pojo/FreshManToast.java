package com.xquark.service.freshman.pojo;

import java.util.Date;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/11
 * Time:11:09
 * Des:新人弹框接口数据
 */
public class FreshManToast {
    public static  final Integer [] TYPE_ARR = new Integer [] {1,2,3,4,5}; //type包含类型
    private Long cpId;
    private Integer type; //1注册赠送德分弹窗，2首单完成弹窗，3消费满200弹窗，4成功升级VIP弹窗，5：满500弹框

    private Integer vipType; //0：无效状态，1:VIP套装升级，2：消费满500升级，3：拼团单升级
    private Date recordTime; //记录升级时间

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public Integer getVipType() {
        return vipType;
    }

    public void setVipType(Integer vipType) {
        this.vipType = vipType;
    }

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}