package com.xquark.service.freshman.pojo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/5
 * Time:13:49
 * Des:新人页面轮播图
 */
public class FreshManBannerData {
    private String imgUrl; //图片url
    private String redirectUrl; //跳转路径

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}