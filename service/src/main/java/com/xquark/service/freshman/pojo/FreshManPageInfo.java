package com.xquark.service.freshman.pojo;

import java.util.List;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/6
 * Time:11:00
 * Des:新人页面数据
 */
public class FreshManPageInfo {
    private ToastInfo toastInfo;
    private FreshManShareData shareInfo;
    private List<FreshManBannerData> bannerList;
    private FreshManZoneTitle zoneTitle;
    private List<FreshManProductTab> tabList;
    private FreshManBottom bottom;

    public FreshManZoneTitle getZoneTitle() {
        return zoneTitle;
    }

    public void setZoneTitle(FreshManZoneTitle zoneTitle) {
        this.zoneTitle = zoneTitle;
    }


    public ToastInfo getToastInfo() {
        return toastInfo;
    }

    public void setToastInfo(ToastInfo toastInfo) {
        this.toastInfo = toastInfo;
    }

    public FreshManShareData getShareInfo() {
        return shareInfo;
    }

    public void setShareInfo(FreshManShareData shareInfo) {
        this.shareInfo = shareInfo;
    }

    public List<FreshManBannerData> getBannerList() {
        return bannerList;
    }

    public void setBannerList(List<FreshManBannerData> bannerList) {
        this.bannerList = bannerList;
    }

    public List<FreshManProductTab> getTabList() {
        return tabList;
    }

    public void setTabList(List<FreshManProductTab> tabList) {
        this.tabList = tabList;
    }

    public FreshManBottom getBottom() {
        return bottom;
    }

    public void setBottom(FreshManBottom bottom) {
        this.bottom = bottom;
    }
}