package com.xquark.service.freshman.util;

import java.math.BigDecimal;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/12
 * Time:20:47
 * Des:处理价格相关精度
 */
public class BigDecimalUtil {

    /**
     * 四舍五入
     * @param b
     * @return
     */
    public static BigDecimal getRoundHalfUp(BigDecimal b){
        if( b == null)
            return b;
        return b.setScale(2,BigDecimal.ROUND_HALF_UP);
    }
}