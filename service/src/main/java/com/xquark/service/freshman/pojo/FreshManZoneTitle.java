package com.xquark.service.freshman.pojo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/7
 * Time:14:17
 * Des:精品区说明
 */
public class FreshManZoneTitle {
    private int limtBuy; //限购数量
    private String title;
    private String subTitle;

    public int getLimtBuy() {
        return limtBuy;
    }

    public void setLimtBuy(int limtBuy) {
        this.limtBuy = limtBuy;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }
}