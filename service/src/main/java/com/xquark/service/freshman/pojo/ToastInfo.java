package com.xquark.service.freshman.pojo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/
 * Time:11:48
 * Des:新人页面弹框
 */
public class ToastInfo {
    //RULE_FRESHMAN 新人规则
    //RULE_POINT 德分规则
    private int isFreshMan; //是否弹框 0：弹框，1：不弹框
    private String gainPoint;
    private String ruleFreshMan;
    private String rulePoint;
    private String ruleFreshManUrl; //新人规则图片地址
    private String rulePointUrl; //德分规则图片地址

    public String getRuleFreshManUrl() {
        return ruleFreshManUrl;
    }

    public void setRuleFreshManUrl(String ruleFreshManUrl) {
        this.ruleFreshManUrl = ruleFreshManUrl;
    }

    public String getRulePointUrl() {
        return rulePointUrl;
    }

    public void setRulePointUrl(String rulePointUrl) {
        this.rulePointUrl = rulePointUrl;
    }

    public String getRulePoint() {
        return rulePoint;
    }

    public void setRulePoint(String rulePoint) {
        this.rulePoint = rulePoint;
    }

    public String getRuleFreshMan() {
        return ruleFreshMan;
    }

    public void setRuleFreshMan(String ruleFreshMan) {
        this.ruleFreshMan = ruleFreshMan;
    }

    public int getIsFreshMan() {
        return isFreshMan;
    }

    public void setIsFreshMan(int isFreshMan) {
        this.isFreshMan = isFreshMan;
    }

    public String getGainPoint() {
        return gainPoint;
    }

    public void setGainPoint(String gainPoint) {
        this.gainPoint = gainPoint;
    }


}