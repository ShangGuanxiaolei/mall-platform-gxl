package com.xquark.service.freshman.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.model.CommissionTotal;
import com.hds.xquark.dal.model.PointTotal;
import com.hds.xquark.dal.type.PlatformType;
import com.hds.xquark.dal.type.Trancd;
import com.hds.xquark.service.point.CommissionServiceApi;
import com.hds.xquark.service.point.PointCommService;
import com.hds.xquark.service.point.PointServiceApi;
import com.hds.xquark.service.point.type.FunctionCodeType;
import com.xquark.dal.mapper.FirstOrderMapper;
import com.xquark.dal.mapper.FreshManAreatTabMapper;
import com.xquark.dal.mapper.FreshManBannerMapper;
import com.xquark.dal.mapper.FreshManConfigMapper;
import com.xquark.dal.mapper.FreshManModuleMapper;
import com.xquark.dal.mapper.FreshManProductMapper;
import com.xquark.dal.mapper.FreshManUpGradeMapper;
import com.xquark.dal.mapper.FreshmanRecommendMapper;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.mapper.UserMapper;
import com.xquark.dal.model.FreshManAreatTabVo;
import com.xquark.dal.model.FreshManBannerVo;
import com.xquark.dal.model.FreshManConfigVo;
import com.xquark.dal.model.FreshManFlagVo;
import com.xquark.dal.model.FreshManProductVo;
import com.xquark.dal.model.FreshManUpGradeVo;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.User;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.service.config.SystemConfigService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.freshman.FirstOrderService;
import com.xquark.service.freshman.FreshManFlagService;
import com.xquark.service.freshman.FreshManProductService;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.freshman.FreshmanRecommendService;
import com.xquark.service.freshman.pojo.FreshManBannerData;
import com.xquark.service.freshman.pojo.FreshManBottom;
import com.xquark.service.freshman.pojo.FreshManInfo;
import com.xquark.service.freshman.pojo.FreshManPageInfo;
import com.xquark.service.freshman.pojo.FreshManProduct;
import com.xquark.service.freshman.pojo.FreshManProductTab;
import com.xquark.service.freshman.pojo.FreshManShareData;
import com.xquark.service.freshman.pojo.FreshManToast;
import com.xquark.service.freshman.pojo.FreshManZoneTitle;
import com.xquark.service.freshman.pojo.ToastInfo;
import com.xquark.service.freshman.util.BigDecimalUtil;
import com.xquark.service.freshman.vo.QueryCustomerRelationshipResponseVo;
import com.xquark.service.orderHeader.OrderHeaderService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.user.UserService;
import com.xquark.utils.DateUtils;
import com.xquark.utils.http.PoolingHttpClients;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/5
 * Time:11:46
 * Des:新人相关
 */
@Service
public class FreshManServiceImpl implements FreshManService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FreshManServiceImpl.class);
    @Value("${balance.center.url}")
    private String balanceCenterHostURL;
    private static final String ADD_RELATIONSHIP_URL="/v1/api/addCustomerRelationship";
    private static final String QUERY_RELATIONSHIP_URL="/v1/api/queryCustomerRelationship";
    @Autowired
    private FreshManProductMapper freshManProductMapper;
    @Autowired
    private FreshManConfigMapper freshManConfigMapper;
    @Autowired
    private FreshManBannerMapper freshManBannerMapper;
    @Autowired
    private FreshManAreatTabMapper freshManAreatTabMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private FreshmanRecommendMapper freshmanRecommendMapper;
    @Autowired
    private FreshmanRecommendService freshmanRecommendService;
    @Autowired
    private UserService userService;


    private final PointServiceApi pointService;
    private final CommissionServiceApi commissionServiceApi;
    private final PointCommService pointCommService;

    @Autowired
    private FreshManFlagService freshManFlagService;
    @Autowired
    private SkuMapper skuMapper;
    @Autowired
    private CustomerProfileService customerProfileService;
    @Autowired
    private OrderHeaderService orderHeaderService;
    @Autowired
    private SystemConfigService systemConfigService;

    @Autowired
    public FreshManServiceImpl(PointContextInitialize pointContextInitialize) {
        this.pointService = pointContextInitialize.getPointServiceApi();
        this.commissionServiceApi = pointContextInitialize.getCommissionServiceApi();
        this.pointCommService = pointContextInitialize.getPointService();
    }
    @Autowired
    private FreshManModuleMapper freshManModuleMapper;
    @Autowired
    private FreshManUpGradeMapper freshManUpGradeMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private FreshManProductService freshManProductService;
    @Autowired
    private FirstOrderMapper firstOrderMapper;
    @Autowired
    private FirstOrderService firstOrderService;

    @Override
    public FreshManPageInfo selectFreshManPageInfo(Long cpId) {
        FreshManPageInfo result = new FreshManPageInfo();
        //弹框数据
        ToastInfo toastInfo = this.selectToastInfo(cpId);
        result.setToastInfo(toastInfo);

        //获取分享数据
        FreshManShareData freshManShareData = this.selectFreshManShareData();
        result.setShareInfo(freshManShareData);

        boolean isBanner = this.freshManModuleMapper.selectExistByStatusAndType(1, 1);
        //获取banner数据
        if(isBanner){
            List<FreshManBannerData> bannerData = this.getBannerData();
            result.setBannerList(bannerData);
        }

        boolean isTabs = this.freshManModuleMapper.selectExistByStatusAndType(1, 2);
        if(isTabs){
            //获取精品区数据
            FreshManZoneTitle zoneTitle = this.selectFreshManZoneTitle();
            result.setZoneTitle(zoneTitle);

            //获取商品数据
            List<FreshManProductTab> tabs = this.selectFreshManProductTabList(cpId);
            result.setTabList(tabs);
        }

        boolean isBottom = this.freshManModuleMapper.selectExistByStatusAndType(1, 3);
        //底部数据
        if(isBottom){
            FreshManBottom bottom = this.selectFreshManBottom();
            result.setBottom(bottom);
        }

        return result;
    }

    /**
     *
     * @param cpId
     * @return
     */
    @Override
    public ToastInfo selectToastInfo(Long cpId) {
        ToastInfo result = new ToastInfo();
        User user = this.userMapper.selectByCpId(cpId);
        if (user != null){
            result.setIsFreshMan(user.getRegisterPointFlag());
        }
        //注册送德分
        Long point = this.selectRegisterPoint().longValue();
        if(point == null)
            result.setGainPoint("100");
        else {
            result.setGainPoint(new StringBuffer().append(point.intValue()).toString());
        }
        //新人规则
        FreshManConfigVo ruleFreshman = this.freshManConfigMapper.selectByName("RULE_FRESHMAN");
        if(ruleFreshman != null){
            result.setRuleFreshMan(ruleFreshman.getValue());
        }
        //新人规则图片地址
        FreshManConfigVo ruleFreshmanUrl = this.freshManConfigMapper.selectByName("RULE_FRESHMAN_URL");
        if(ruleFreshmanUrl != null){
            result.setRuleFreshManUrl(ruleFreshmanUrl.getValue());
        }

        //德分规则
        FreshManConfigVo rulePoint = this.freshManConfigMapper.selectByName("RULE_POINT");
        if(rulePoint != null){
            result.setRulePoint(rulePoint.getValue());
        }

        //德分规则图片地址
        FreshManConfigVo rulePointUrl = this.freshManConfigMapper.selectByName("RULE_POINT_URL");
        if(rulePointUrl != null){
            result.setRulePointUrl(rulePointUrl.getValue());
        }

        return result;
    }

    /**
     * 商品某个分类及列表数据
     * @return
     */
    @Override
    public FreshManProductTab selectFreshManProductTab(Long cpId) {
        FreshManProductTab freshManProductTab = new FreshManProductTab();
        List<FreshManProduct> list = this.selectFreshManProductListByTabId(0,cpId);
        freshManProductTab.setGoods(list);
        return freshManProductTab;
    }

    /**
     * 获取商品列表数据
     * @return
     */
    @Override
    public List<FreshManProductTab> selectFreshManProductTabList(Long cpId) {
        List<FreshManProductTab> result = new ArrayList<>();
        //排序好的tabs
        List<FreshManAreatTabVo> tabS = this.selectFreshManAreatTabVo();
        //拼好各个tag的商品列表数据
        for (FreshManAreatTabVo f : tabS){
            if(f == null){
                continue;
            }
            FreshManProductTab freshManProductTab = new FreshManProductTab();
            List<FreshManProduct> goods = this.selectFreshManProductListByTabId(f.getTabId(),cpId);
            if(goods == null || goods.isEmpty()){
                continue;
            }
            freshManProductTab.setGoods(goods);
            freshManProductTab.setTabId(f.getTabId());
            freshManProductTab.setTitle(f.getName());
            result.add(freshManProductTab);
        }
        return result;
    }

    /**
     * 依据分类获取商品数据
     * @param tabId
     * @return
     */
    @Override
    public List<FreshManProduct> selectFreshManProductListByTabId(int tabId, Long cpId) {
        List<FreshManProduct> resultList = new ArrayList<>();
        List<FreshManProductVo> list = this.freshManProductMapper.selectListByTabId(tabId);

        //去重
        ArrayList<FreshManProductVo> collect = list.stream()
                .collect(Collectors.collectingAndThen(Collectors.toCollection(() ->
                        new TreeSet<>(Comparator.comparing(f
                                -> f.getProductId()))), ArrayList::new));

        for (FreshManProductVo f : collect){
            if(f != null){
                FreshManProduct freshManProduct = this.toFreshManProduct(f, cpId);
                if(Objects.isNull(freshManProduct))
                    continue;
                resultList.add(freshManProduct);
            }
        }
        return resultList;
    }

    /**
     * 获取底部数据
     * @return
     */
    @Override
    public FreshManBottom selectFreshManBottom() {
        FreshManBottom result = new FreshManBottom();
        FreshManConfigVo title = this.freshManConfigMapper.selectByName("ZONE_BRAND_TITLE");
        FreshManConfigVo url = this.freshManConfigMapper.selectByName("ZONE_BRAND_URL");
        if(title != null){
            result.setTitle(title.getValue());
        }
        if(url != null){
            result.setImgUrl(url.getValue());
        }
        return result;
    }

    /**
     * 获取新人相关数据
     * @return
     */
    @Override
    public List<FreshManInfo> selectFreshManInfoList(Long cpId) {
        List<FreshManInfo> resultList = new ArrayList<>();
        //获取商品相关数据
        FreshManInfo fFreshManProductTab = this.getFreshManProductTabData(cpId);
        resultList.add(fFreshManProductTab);
        //获取底部数据
        FreshManInfo fBottom = this.getBottomData();
        resultList.add(fBottom);
        return resultList;
    }

    /**
     * 获取底部数据
     * @return
     */
    private FreshManInfo getBottomData() {
        FreshManInfo freshManInfo = new FreshManInfo();
        freshManInfo.setType(3);//底部广告位
        FreshManBottom list = this.selectFreshManBottom();
        freshManInfo.setBottom(list);
        return freshManInfo;
    }

    /**
     * 商品tab相关拼数据
     * @return
     */
    private FreshManInfo getFreshManProductTabData(Long cpId) {
        FreshManInfo freshManInfo = new FreshManInfo();
        freshManInfo.setType(2);//新人专享
        List<FreshManProductTab> list = this.selectFreshManProductTabList(cpId);
        freshManInfo.setTabList(list);
        return freshManInfo;
    }

    /**
     * 轮播图相关拼数据
     * @return
     */
    private List<FreshManBannerData> getBannerData() {
        List<FreshManBannerData> list = this.selectFreshManBannerData();
        return list;
    }

    /**
     * 获取分享数据
     * @return
     */
    @Override
    public FreshManShareData selectFreshManShareData() {
        FreshManShareData result = new FreshManShareData();
        FreshManConfigVo freshManConfigVo = this.freshManConfigMapper.selectByName("ZONE_SHARE_TITLE");
        if(freshManConfigVo != null){
            result.setTitle(freshManConfigVo.getValue());
        }
        return result;
    }

    /**
     * 获取banner数据
     * @return
     */
    @Override
    public List<FreshManBannerData> selectFreshManBannerData() {
        List<FreshManBannerData> result = new ArrayList<>();
        List<FreshManBannerVo> list = this.freshManBannerMapper.selectBySortBanner(1);
        if(list == null || list.isEmpty()){
            return null;
        }
        for (FreshManBannerVo f : list){
            if(f != null){
                FreshManBannerData freshManBannerData = toFreshManBannerData(f);
                result.add(freshManBannerData);
            }
        }
        return result;
    }

    /**
     * 获取有效的新人页面tab
     * @return
     */
    @Override
    public List<FreshManAreatTabVo> selectFreshManAreatTabVo() {
        List<FreshManAreatTabVo> list = this.freshManAreatTabMapper.selectFreshManAreatTabVoByStatus(1);
        return list;
    }

    @Override
    public FreshManZoneTitle selectFreshManZoneTitle() {
        FreshManZoneTitle result = new FreshManZoneTitle();
        FreshManConfigVo limitVo = this.freshManConfigMapper.selectByName("ZONE_BUY_LIMIT");
        if(limitVo != null){
            result.setLimtBuy(Integer.parseInt(limitVo.getValue()));
        }
        FreshManConfigVo titleVo = this.freshManConfigMapper.selectByName("ZONE_TITLE");
        if(titleVo != null){
            result.setTitle(titleVo.getValue());
        }
//        FreshManConfigVo subTitleVo = this.freshManConfigMapper.selectByName("ZONE_SUB_TITLE");
//        if(subTitleVo != null){
//            String subTitle = subTitleVo.getValue();
//            if(StringUtil.isEmpty(subTitle))
//                result.setSubTitle("福利区包邮商品每人限购1件");
//            else {
//                result.setSubTitle(subTitle.replace("{0}"
//                        ,limitVo.getValue()));
//            }
//        }
        return result;
    }

    /**
     * 更新注册用户发送德分状态
     * @param cpId
     * @return
     */
    @Override
    public boolean updateFreshManPointStataus(Long cpId) {

        //0：用户确认德分，1：用户未确认德分
        return this.userMapper.updateRegisterPointFlag(cpId, 1);
    }

    /**
     * 新人注册发送德分
     * @param cpId
     * @return
     */
    @Override
    public boolean grantRegisterPoint(Long cpId) {
        //获取要发送的德分
        BigDecimal point = this.selectRegisterPoint();
        if(cpId == null){
            LOGGER.info(new StringBuffer().append("获取cpId为null，新人注册发送德分失败").toString());
            return false;
        }
        try {

            //注册防止多账号多次发送德分
            boolean b = this.userMapper.checkGrantPoint(cpId,Trancd.FRESHMAN.name());
            if(b){
                LOGGER.info(new StringBuffer()
                        .append("该用户已经发送过注册德分，对应的cpId是{")
                        .append(cpId).append("}")
                        .toString());
                return false;
            }

            this.pointService.grant(cpId
                    , "注册"
                    , Trancd.FRESHMAN
                    , PlatformType.E
                    ,point );

            LOGGER.info(new StringBuffer()
                    .append("新人注册发送德分成功，对应的cpId是{")
                    .append(cpId).append("}")
                    .append("发送的德分是{")
                    .append(point)
                    .append("}")
                    .toString());
            //更新flag表
            this.updateRegisterFlag(cpId);
        }catch (Exception e){
            LOGGER.error(new StringBuffer()
                    .append("新人注册发送德分失败，调用pointService.grant异常，对应的cpId是{")
                    .append(cpId).append("}")
                    .append("发送的德分是{")
                    .append(point)
                    .append("}")
                    .append("异常信息是{}")
                    .append(e.getMessage())
                    .toString());
        }
        return true;
    }

    private boolean updateRegisterFlag(Long cpId) {
        FreshManToast freshManToast = new FreshManToast();
        freshManToast.setType(1);
        freshManToast.setCpId(cpId);
        this.freshManFlagService.insertFlag4Toast(freshManToast);
        return true;
    }

    /**
     * 获取注册德分
     * @return
     */
    @Override
    public BigDecimal selectRegisterPoint() {
        FreshManUpGradeVo vo = this.freshManUpGradeMapper.selectByGrade("0");
        if (vo == null)
            return new BigDecimal("100");
        return new BigDecimal(vo.getPoint());
    }


    /**
     * 新人专区发放德分
     */
    @Override
    public void freshManGrantPoint(Order order){
        // 优化拆单逻辑
        String mainOrderId = order.getMainOrderId();
        if (StringUtils.isBlank(mainOrderId))
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "主订单 [ " + mainOrderId + " ] 不存在");

        User user = userService.selectUserByUserId(order.getBuyerId());
        boolean hasIdentity = customerProfileService.hasIdentity(user.getCpId());
        if(!hasIdentity){
            BigDecimal alreadyPay = freshmanRecommendService.countUserSpending(user.getCpId());
            //获取配置的金额和德分
            Map pointConfig = this.getPointConfig("1");
            String configAmountStr =(String)pointConfig.get("amount");
            String configPointtStr =(String)pointConfig.get("point");

            BigDecimal configAmount = new BigDecimal(configAmountStr);
            BigDecimal configPoint = new BigDecimal(configPointtStr);

            List<Order> orderList = orderMapper.selectByMainOrderId(mainOrderId);
            BigDecimal currentOrderHadPay = BigDecimal.ZERO;
            if (CollectionUtils.isNotEmpty(orderList)) {
                if (orderList.size() > 1) {
                    BigDecimal childOrderGoodsFee;
                    for (Order o : orderList) {
                        childOrderGoodsFee =
                                o.getGoodsFee().
                                        subtract(o.getDiscountFee()).
                                        add((o.getPaidPoint().add(o.getPaidPointPacket())).divide(new BigDecimal(10))).
                                        add(o.getPaidCommission());

                        currentOrderHadPay = currentOrderHadPay.add(childOrderGoodsFee);
                    }
                    childOrderGoodsFee = null; // help GC
                } else {
                    currentOrderHadPay=
                            order.getGoodsFee().
                                    subtract(order.getDiscountFee()).
                                    add((order.getPaidPoint().add(order.getPaidPointPacket())).divide(new BigDecimal(10))).
                                    add(order.getPaidCommission());

                }
            }

            //如果大于配置的金额 则发放配置的德分
            if(alreadyPay.compareTo(configAmount)==-1 && ((alreadyPay.add(currentOrderHadPay)).compareTo(configAmount)>=0)){
                try {

                    this.pointService.grant(user.getCpId()
                            , "德分奖励发放"
                            ,Trancd.FRESHMAN
                            ,com.hds.xquark.dal.type.PlatformType.E
                            ,configPoint );

                    LOGGER.info("cpId {} 满新人专区配置金额发放德分 {}, 业务Id: {}", user.getCpId(), configPoint, order.getOrderNo());

                }catch (Exception e){
                    LOGGER.error(new StringBuffer()
                            .append("满新人专区配置金额发放德分失败，调用pointService.grant异常，对应的cpId是{")
                            .append(user.getCpId()).append("}")
                            .append("发送的德分是{")
                            .append(configPoint)
                            .append("}")
                            .append("异常信息是{}")
                            .append(e.getMessage())
                            .toString());
                }
        }



        }
    }

    /**
     * 功能描述: 新人引导拼团订单消费满500升级vip
     * @author Luxiaoling
     * @date 2019/3/18 11:47
     * @param  order
     * @return  void
     */
    @Override
    public void freshManPieceOrderToVip(Order order) {
        BigDecimal orderHadPay=
                order.getGoodsFee().
                        subtract(order.getDiscountFee()).
                        add((order.getPaidPoint().add(order.getPaidPointPacket())).divide(new BigDecimal(10))).
                        add(order.getPaidCommission());
        User user = userService.selectUserByUserId(order.getBuyerId());
        boolean hasIdentity = customerProfileService.hasIdentity(user.getCpId());
        if(!hasIdentity){
            BigDecimal alreadyPay = freshmanRecommendService.countUserSpending(user.getCpId());
            Map pointConfig = this.getPointConfig("2");
            String configAmountStr =(String)pointConfig.get("amount");

            int bigger = alreadyPay.compareTo(new BigDecimal(configAmountStr));
            int less = (alreadyPay.subtract(orderHadPay)).compareTo(new BigDecimal(configAmountStr));
            //累计金额大于等于500  并且减去当前订单金额是小于500的
            if(bigger>=0 && less==-1){

                orderHeaderService.updateJoinTypeByOrderNo(3,order.getOrderNo());
                LOGGER.info("新人订单金额满500更新orderHeader表 joinType=3，OrderNo="+order.getOrderNo());
                //更新freshmanFlag 表
                FreshManFlagVo freshManFlagVo = freshManFlagService.selectFlag4Toast(user.getCpId());
                if(null==freshManFlagVo||(null!=freshManFlagVo&&freshManFlagVo.getVipFlag()==0) ){
                    FreshManToast freshManToast = new FreshManToast();
                    freshManToast.setType(4);
                    freshManToast.setVipType(2);
                    freshManToast.setCpId(Long.valueOf(user.getCpId()));
                    freshManToast.setRecordTime(DateUtils.addSeconds(new Date(), 1800));//加上半小时
                    freshManFlagService.updateFlag4Toast(freshManToast);
                }

            }
        }
    }

    /**
     * 功能描述: 拼团订单满* 元发放德分
     * @author Luxiaoling
     * @date 2019/3/18 12:38
     * @param  order
     * @return  void
     */
    @Override
    public void freshManPieceOrderGrantPoint(Order order) {
        User user = userService.selectUserByUserId(order.getBuyerId());
        boolean hasIdentity = customerProfileService.hasIdentity(user.getCpId());
        if(!hasIdentity){
            BigDecimal alreadyPay = freshmanRecommendService.countUserSpending(user.getCpId());
            //获取配置的金额和德分
            Map pointConfig = this.getPointConfig("1");
            String configAmountStr =(String)pointConfig.get("amount");
            String configPointtStr =(String)pointConfig.get("point");
            BigDecimal configAmount = new BigDecimal(configAmountStr);
            BigDecimal configPoint = new BigDecimal(configPointtStr);

            BigDecimal currentOrderHadPay=
                    order.getGoodsFee().
                            subtract(order.getDiscountFee()).
                            add((order.getPaidPoint().add(order.getPaidPointPacket())).divide(new BigDecimal(10))).
                            add(order.getPaidCommission());

            //如果大于配置的金额 则发放配置的德分
            if(alreadyPay.compareTo(configAmount)>=0 && ((alreadyPay.subtract(currentOrderHadPay)).compareTo(configAmount)<0)){
                try {

                    this.pointService.grant(user.getCpId()
                            , "德分奖励发放"
                            ,Trancd.FRESHMAN
                            ,com.hds.xquark.dal.type.PlatformType.E
                            ,configPoint );

                    FreshManToast freshManToast = new FreshManToast();
                    freshManToast.setType(3);
                    freshManToast.setCpId(Long.valueOf(user.getCpId()));
                    freshManToast.setVipType(0);
                    freshManFlagService.updateFlag4Toast(freshManToast);

                    LOGGER.info("cpId {} 满新人专区配置金额发放德分 {}, 业务Id: {}", user.getCpId(), configPoint, order.getOrderNo());

                }catch (Exception e){
                    LOGGER.error(new StringBuffer()
                            .append("满新人专区配置金额发放德分失败，调用pointService.grant异常，对应的cpId是{")
                            .append(user.getCpId()).append("}")
                            .append("发送的德分是{")
                            .append(configPoint)
                            .append("}")
                            .append("异常信息是{}")
                            .append(e.getMessage())
                            .toString());
                }
            }

        }
    }
    /**
     * 功能描述: 同意退款时 新人礼单需扣减上级的德分和收益
     * @author Luxiaoling
     * @date 2019/4/24 16:12
     * @param
     * @return  void
     */
    @Override
    public void frenshManDeductUplinePointAndCommission(Order order) {
        Integer joinType = orderHeaderService.selectJoinTypeByOrderNo(order.getOrderNo());
        if(5==joinType||6==joinType){
            User user = userService.selectUserByUserId(order.getBuyerId());
            Long fromCpId = orderHeaderService.loadFromCpId(order.getOrderNo());
            if(null!=fromCpId && 2000002L!=fromCpId && fromCpId!=user.getCpId()){
                try {
                    //扣减德分 先查询上级有几分
                    Optional<PointTotal> pointTotalOptional = pointService.loadTotal(fromCpId);
                    //上级汉薇可用德分
                    BigDecimal usableEcomm = pointTotalOptional.get().getUsableEcomm();
                    if(usableEcomm.compareTo(new BigDecimal(100))==-1){
                        //小于100 扣除所有
                        pointService.consume(fromCpId,
                                order.getOrderNo(),
                                Trancd.FRESHMAN_P,
                                com.hds.xquark.dal.type.PlatformType.E,
                                usableEcomm);
                        LOGGER.info("cpId {} 扣减德分 {}, 业务Id: {}", fromCpId, usableEcomm, order.getOrderNo());
                    }else{
                        //扣除上级的100德分
                        pointService.consume(fromCpId,
                                order.getOrderNo(),
                                Trancd.FRESHMAN_P,
                                com.hds.xquark.dal.type.PlatformType.E,
                                new BigDecimal(100));
                        LOGGER.info("cpId {} 扣减德分 {}, 业务Id: {}", fromCpId, 100, order.getOrderNo());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.error(new StringBuffer()
                            .append("扣減德分異常，对应的上级cpId是{")
                            .append(fromCpId).append("}")
                            .append("对应的当前单cpId是{")
                            .append(user.getCpId())
                            .append("异常信息是{}")
                            .append(e.getMessage())
                            .toString());
                }
                try {
                    //扣减积分
                    Optional<CommissionTotal> commissionTotalOptional = commissionServiceApi.loadTotal(fromCpId);
                    BigDecimal usableCommission = commissionTotalOptional.get().getUsableEcomm();
                    Boolean canDeductTotal = usableCommission.compareTo(new BigDecimal(2))==-1 ? true :false;
                    if(canDeductTotal){
                        //不足2积分 扣除所有
                        commissionServiceApi.modify(
                                fromCpId,
                                order.getOrderNo(),
                                Pair.of(FunctionCodeType.RETURN_COMMISSION, Trancd.hvns) ,
                                PlatformType.E,
                                usableCommission);
                        LOGGER.info("cpId {} 扣减积分 {}, 业务Id: {}", fromCpId, usableCommission, order.getOrderNo());

                    }else{
                        //扣除2积分
                        commissionServiceApi.modify(
                                fromCpId,
                                order.getOrderNo(),
                                Pair.of(FunctionCodeType.RETURN_COMMISSION, Trancd.hvns) ,
                                PlatformType.E,
                                new BigDecimal(2));
                        LOGGER.info("cpId {} 扣减积分 {}, 业务Id: {}", fromCpId, 2, order.getOrderNo());

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.error(new StringBuffer()
                            .append("扣減积分異常，对应的上级cpId是{")
                            .append(fromCpId).append("}")
                            .append("对应的当前单cpId是{")
                            .append(user.getCpId())
                            .append("异常信息是{}")
                            .append(e.getMessage())
                            .toString());
                }

            }

        }
    }

    /**
     * 功能描述:扣減德分
     * @author Luxiaoling
     * @date 2019/3/13 16:09
     * @param
     * @return
     */
    @Override
    public void freshManeductPoint(Order order){
        /**
         * 取消订单时，判断新人订单满200，
         * 如果满200并且扣完当前订单的金额不租200  需要扣除2000德分
         * 不满2000扣除所有德分
         */

        User user = userService.selectUserByUserId(order.getBuyerId());
        FreshManFlagVo freshManFlagVo = freshManFlagService.selectFlag4Toast(user.getCpId());
        //判断达到过配置金额 才扣减
        if(null!=freshManFlagVo&&freshManFlagVo.getReachFlag()==1 ){
            //新人已消费金额
            BigDecimal alreadyPay = freshmanRecommendService.countUserSpending(user.getCpId());
            //获取配置的金额和德分
            Map pointConfig = this.getPointConfig("1");
            String configAmountStr =(String)pointConfig.get("amount");
            String configPointtStr =(String)pointConfig.get("point");

            BigDecimal configAmount = new BigDecimal(configAmountStr);
            BigDecimal configPoint = new BigDecimal(configPointtStr);

            //应付金额 = goodfee - discountfee +（德分+红包分）/10 + 收益
            //BigDecimal currentOrderAmount = order.getTotalFee().subtract(order.getLogisticsFee());
            BigDecimal currentOrderHadPay=
                    order.getGoodsFee().
                            subtract(order.getDiscountFee()).
                            add((order.getPaidPoint().add(order.getPaidPointPacket())).divide(new BigDecimal(10))).
                            add(order.getPaidCommission());

            // 拼团中的订单不在累计消费金额内, 故不执行扣减操作
            String orderType = order.getOrderType().name();
            String orderStatus = order.getStatus().name();
            boolean flag;
            if (orderType.equals("PIECE") && orderStatus.equals("PAIDNOSTOCK")) {
                flag = false;
            } else {
                flag = true;
            }
            if(alreadyPay.compareTo(configAmount) >=0 && flag &&(alreadyPay.subtract(currentOrderHadPay)).compareTo(configAmount)==-1 ){
                try {
                    //扣减德分 先查询是否当前登入人有几分
                    Optional<PointTotal> pointTotalOptional = pointService.loadTotal(user.getCpId());
                    //购买人汉薇可用德分
                    BigDecimal usableEcomm = pointTotalOptional.get().getUsableEcomm();
                    if(usableEcomm.compareTo(configPoint)==-1){
                        //小于2000 扣除所有
                        pointService.consume(user.getCpId(),
                                "德分奖励扣回",
                                Trancd.FRESHMAN_P,
                                com.hds.xquark.dal.type.PlatformType.E,
                                usableEcomm);
                        LOGGER.info("cpId {} 扣减德分 {}, 业务Id: {}", user.getCpId(), usableEcomm, order.getOrderNo());
                    }else{
                        //扣除2000
                        pointService.consume(user.getCpId(),
                                "德分奖励扣回",
                                Trancd.FRESHMAN_P,
                                com.hds.xquark.dal.type.PlatformType.E,
                                configPoint);
                        LOGGER.info("cpId {} 扣减德分 {}, 业务Id: {}", user.getCpId(), configPoint, order.getOrderNo());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.error(new StringBuffer()
                            .append("扣減德分異常，对应的cpId是{")
                            .append(user.getCpId()).append("}")
                            .append("异常信息是{}")
                            .append(e.getMessage())
                            .toString());
                }
            }
        }

    }



    @Override
    public Map getPointConfig(String grade) {
        return this.freshManConfigMapper.selectPointConfig(grade);
    }

    /**
     * 获取sku信息
     * @param productId
     * @return
     */
    @Override
    public Sku selectSkuByProductId(String productId) {
        Sku sku = this.skuMapper.selectMinPriceSku4FreshMan(productId);
//        return this.skuMapper.selectMinPriceSkuByProductId(productId);
        return sku;
    }

    @Override
    public List<FreshManProductVo> selectFreshmanProductListById(String id) {
        return this.freshManProductMapper.selectByProductIdIgnoreStatus(id);
    }

    @Override
    public FreshManProductVo selectFreshmanProductById(String id) {
        List<FreshManProductVo> list = this.selectFreshmanProductListById(id);
        if(list != null && !list.isEmpty()){
            return list.get(0);
        }
        return null;
    }

    /**
     * 新课sku
     * @param productId
     * @return
     */
    @Override
    public List<Sku> selectFreshManSku(String productId) {
        return this.skuMapper.selectFreshManSku(productId);
    }

    /**
     * 新人商品限购
     * @param count
     * @param user
     */
    @Override
    public void freshmanLimit(int count,User user) {

        //当前登入人是否可以继续下新人单
        long userId = IdTypeHandler.decode(user.getId());
        boolean isOrderBuy=this.isBuyByOrder(userId);
        Integer canBuyOrderLimit=firstOrderMapper.selectOrderLimit();
        if(isOrderBuy==false) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "新人福利限购" + canBuyOrderLimit + "次哦");
        }
        //当前登入人还可以还可以购买的新人商品数量
        Integer canBuyCount = firstOrderService.countProductNumber(userId);
        if (canBuyCount <= 0) {
            canBuyCount = 0;
        }
        Integer findfreshmanValue = firstOrderMapper.findfreshmanValue();
        //校验购物车下单的 新人专区商品数量不能超过限购
        if (canBuyCount < count) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "新人商品限购" + findfreshmanValue + "件哦");
        }

    }

    @Override
    public Integer selectFreshmanOrder(Long cpId) {
        return freshmanRecommendMapper.selectFreshmanOrder(cpId);
    }

    @Override
    public boolean setFreshManCommission(Long cpId) {

        // 发送收益开关
        String value = this.systemConfigService.getValue("user_regist_send_commisssion_switch");
        // 关闭
        if("false".equals(value)) return false;

        if(Objects.isNull(cpId)) return false;
        // 是否发送过
        try {
            boolean b = this.userMapper.checkGrantCommission(cpId,Trancd.DEPOSIT_C.name());

            if (b) {
                LOGGER.info("用户" + cpId + "注册已经发送过收益");
                return false;
            }
            // 注册发送5收益，不可以提现
            BigDecimal point = new BigDecimal("5");
            pointCommService.grantCommissionNoWithdrawal(cpId, PlatformType.E, point, Trancd.DEPOSIT_C);
            userMapper.updateGrantCommission(cpId,Trancd.DEPOSIT_C.name());
            //commissionServiceApi.grant(cpId, "注册", Trancd.FRESHMAN, PlatformType.E, point);
            //userMapper.grantCommission4Buying(cpId, PlatformType.E.getCode(),point,Trancd.FRESHMAN.name() ,"注册");
            LOGGER.info("用户" + cpId + "注册已经成功发送5收益");
            return true;
        }catch (Exception e){
            LOGGER.error("用户" + cpId + "注册发送5收益出现异常，异常信息是" + e.getMessage());
        }
        return false;
    }

    // 该方法本期需求临时用的，待关系统一维护到结算中心后，不再使用
    @Override
    public boolean addCustomerRelationship(Long cpId, Long upLine, String type) {
        if(Objects.isNull(cpId) || Objects.isNull(upLine) || StringUtils.isBlank(type))
            return false;
        Map<String,Object> mapTag = new HashMap<>();

        HttpResponse response = null; //接口返回
        String entity = null;//接口返回值
        QueryCustomerRelationshipResponseVo result = new  QueryCustomerRelationshipResponseVo();
        mapTag.put("method", "addCustomerRelationship");
        mapTag.put("cpId", cpId);
        // 执行什么操作
        String typeExplain = this.explainType(type);
        mapTag.put("target", typeExplain);
        // url
        String repUrl =
                new StringBuffer().append(balanceCenterHostURL).append(ADD_RELATIONSHIP_URL)
                        .toString();
        // 参数,type:1为次强关系；2为弱关系；3为强关系
        ImmutableMap<String, Long> reParam = ImmutableMap.of("cpid",cpId
                ,"upline",upLine
                ,"type",Long.parseLong(type)
                ,"source",3L);
        mapTag.put("reqParameters", reParam);
        mapTag.put("reqURL", repUrl);
        LOGGER.info("绑定关系到结算中心的请求入参"+mapTag);
        try {
            response = PoolingHttpClients.postJsonResponse(repUrl, reParam);
        }catch (Exception e){
            LOGGER.error("绑定关系到结算中心发生异常" + JSONObject.toJSON(e.getMessage()) + mapTag);
            return false;
        }

        if(response == null){
            LOGGER.error("绑定关系到结算中心的返回null" + mapTag);
            return false;
        }

        try {
            entity = EntityUtils.toString(response.getEntity(), "utf-8");
        } catch (IOException e) {
            LOGGER.error("绑定关系到结算中心解析返回体数据io异常" + JSONObject.toJSON(e.getMessage()) + mapTag);
            return false;
        }

        // res_code：10：cpid不存在,11：弱关系,12：次强关系,13：强关系
        JSONObject json = JSON.parseObject(entity);
        mapTag.put("resStatus", response.getStatusLine());
        mapTag.put("resEntity", json);
        LOGGER.info("绑定关系到结算中心返回" + mapTag);
        // 10：记录成功,11：记录失败
        int res_code = json.getIntValue("res_code");
        if(res_code == 10){
            LOGGER.info("绑定关系到结算中心成功" + mapTag);
            return true;
        }
        LOGGER.info("绑定关系到结算中心失败" + mapTag);

        return false;
    }

    @Override
    public QueryCustomerRelationshipResponseVo queryCustomerRelationship(Long cpId) {
        if(Objects.isNull(cpId))
            return null;
        Map<String,Object> mapTag = new HashMap<>();

        HttpResponse response = null; //接口返回
        String entity = null;//接口返回值
        QueryCustomerRelationshipResponseVo result = new  QueryCustomerRelationshipResponseVo();
        mapTag.put("method", "queryCustomerRelationship");
        mapTag.put("cpId", cpId);

        // url
        String repUrl =
                new StringBuffer().append(balanceCenterHostURL).append(QUERY_RELATIONSHIP_URL)
                        .toString();
        // 参数
        ImmutableMap<String, Long> reParam = ImmutableMap.of("cpid",cpId);
        mapTag.put("reqParameters", reParam);
        mapTag.put("reqURL", repUrl);
        LOGGER.info("从结算中心查询用户关系的请求入参" + mapTag);

        try {
            response = PoolingHttpClients.postJsonResponse(repUrl, reParam);
        }catch (Exception e){
            LOGGER.error("从结算中心查询用户关系发生异常" + mapTag + JSONObject.toJSON(e.getMessage()));
            return null;
        }

        if(response == null){
            LOGGER.error("从结算中心查询用户关系返回null" + mapTag);
            return null;
        }

        try {
            entity = EntityUtils.toString(response.getEntity(), "utf-8");
        } catch (IOException e) {
            LOGGER.error("从结算中心查询用户关系解析返回体数据io异常" + JSONObject.toJSON(e.getMessage()));
            return null;
        }
        mapTag.put("repStatus", response.getStatusLine());
        // res_code：10：cpid不存在,11：弱关系,12：次强关系,13：强关系
        JSONObject json = JSON.parseObject(entity);
        mapTag.put("repEntity", json);
        LOGGER.info("从结算中心查询用户关系返回" + mapTag );

        int res_code = json.getIntValue("res_code");
        String res_msg = json.getString("res_msg");
        long upline = json.getLongValue("upline");

        result.setResCode(res_code);
        result.setResMsg(res_msg);
        result.setUpline(upline);
        return result;
    }

    /**
     * 1为次强关系；2为弱关系；3为强关系
     * @param type
     * @return
     */
    private String explainType(String type) {
        if(StringUtils.isBlank(type))
            return null;

        switch (type){
            case "1":
                return "绑次强关系";
            case "2":
                return "绑弱关系";
            case "3":
                return "绑强关系";
            default:
                return null;
        }
    }

    public FreshManBannerData toFreshManBannerData(FreshManBannerVo f) {
        FreshManBannerData freshManBannerData = new FreshManBannerData();
        freshManBannerData.setRedirectUrl(f.getRedirectUrl());
        freshManBannerData.setImgUrl(f.getImgUrl());
        return freshManBannerData;
    }

    public FreshManProduct toFreshManProduct(FreshManProductVo fpVo, Long cpId){

        FreshManProduct f = new  FreshManProduct();
        //价格、兑换价、兑换积分以sku为准
        Sku sku = this.selectSkuByProductId(fpVo.getProductId());
        if(Objects.isNull(sku))
            return null;
        f.setId(fpVo.getId());
        f.setPrice(BigDecimalUtil.getRoundHalfUp(sku.getPrice()));
        f.setStatus(fpVo.getStatus());

        //兑换价格
        BigDecimal conversionPrice = sku.getConversionPrice();
        BigDecimal subtractValue = sku.getPoint().add(sku.getServerAmt());

        //兑换积分
        f.setExchangePoint(BigDecimalUtil.getRoundHalfUp(sku.getDeductionDPoint()));
        //BigDecimal conversionPrice = sku.getPrice().subtract(sku.getDeductionDPoint().divide(new BigDecimal(10))).setScale(2, 1);
        f.setExchangePrice(BigDecimalUtil.getRoundHalfUp(conversionPrice));
        f.setImgUrl(fpVo.getImg());
        f.setProductName(fpVo.getProductName());
        f.setStock(fpVo.getStock());
        f.setProductId(fpVo.getProductId());
        f.setTabId(fpVo.getTabId());
        f.setSelfOperated(fpVo.getSelfOperated());
        f.setMarketPrice(BigDecimalUtil.getRoundHalfUp(sku.getOriginalPrice()));

        //会员价=专区配置的价格 - 立减
        BigDecimal memberPrice = sku.getPrice().subtract(sku.getPoint());
        //代理价=专区配置的价格 - 立减 - 服务费
        BigDecimal proxyPrice = sku.getPrice().subtract(sku.getPoint()).subtract(sku.getServerAmt());
        boolean isProxy = this.customerProfileService.isProxy(cpId);
        boolean isMember = this.customerProfileService.isMember(cpId);
        //是代理
        if (isProxy){
            f.setExchangePrice(conversionPrice.subtract(subtractValue).setScale(2, 1));
            f.setMemberPrice(memberPrice);
            f.setProxyPrice(proxyPrice);
        }
        //是会员
        if(!isProxy && isMember){
            f.setExchangePrice(conversionPrice.subtract(sku.getPoint()).setScale(2, 1));
            f.setMemberPrice(memberPrice);
        }
        return f;
    }


    /**
     * 根据订单数判断能否继续购买新人订单
     * @param userId
     * @return
     */
    public boolean isBuyByOrder(Long userId){
        Integer buyOrder=firstOrderMapper.mainOrderCount(userId);
        Integer orderLimit=firstOrderMapper.selectOrderLimit();
        boolean a=true;
        if(buyOrder>=orderLimit){
            a=false;
        }
        return a;
    }

}