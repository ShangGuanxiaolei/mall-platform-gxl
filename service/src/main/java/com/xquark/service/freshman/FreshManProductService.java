package com.xquark.service.freshman;

import com.xquark.dal.model.FreshManProductVo;

import java.math.BigDecimal;
import java.util.List;

/**
 * @类名: FreshManProductService
 * @描述: TODO .
 * @程序猿: Lucio
 * @日期: 2019/3/6 18:21
 * @版本号: V1.0 .
 */
public interface FreshManProductService {

    //根据productId查询该产品是否属于新客专区商品
    List<FreshManProductVo> selectByProductId(String productId);

    //查询新人专区配置的特殊邮费
    BigDecimal selectFreshmanLogisticsFee();

    //根据skuid 查询新人专区商品
    List<FreshManProductVo> selectBySkuId(String skuId);

    int countBySku(String skuCode,String productId);

}
