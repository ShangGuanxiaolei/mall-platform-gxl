package com.xquark.service.freshman.pojo;

import java.util.List;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/5
 * Time:13:56
 * Des:新人商品列表VO
 */
public class FreshManProductTab {

    private int tabId;
    private String title;
    private List<FreshManProduct> goods;

    public int getTabId() {
        return tabId;
    }

    public void setTabId(int tabId) {
        this.tabId = tabId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<FreshManProduct> getGoods() {
        return goods;
    }

    public void setGoods(List<FreshManProduct> goods) {
        this.goods = goods;
    }
}