package com.xquark.service.freshman;

import com.xquark.dal.model.FreshmanProduct;

/**
 * 验证新用户是否首单
 *
 * @author tanggb
 * @date 2019/03/05 13:43
 */
public interface FirstOrderService {
    //验证是否为首单购买商品
    Boolean firstOrder(Long cpId);
    //通过商品productId查询出新人专区商品的价格，兑换价格，交换德分
    FreshmanProduct freshmanProduct(Long productId);
    //统计剩余购买数
    Integer countProductNumber(Long userId);
    //用于支付完成页的首单校验
    Boolean firstOrderOfFinishPayPage(String buyerId, String mainOrderId);
    // 支付完成页, 校验首单之前是否有拼团订单
    Boolean countMainOrderByPiece(String buyerId, String mainOrderId);
    //统计购物车内新人专区商品总数
    Integer countCart(Long cpId);
    //检测新人专区库存
    boolean checkStock(Long productId);
}
