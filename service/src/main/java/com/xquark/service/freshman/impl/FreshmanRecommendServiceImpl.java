package com.xquark.service.freshman.impl;

import com.xquark.dal.mapper.FreshmanRecommendMapper;
import com.xquark.dal.mapper.FreshmanTrackMapper;
import com.xquark.dal.model.Product;
import com.xquark.service.freshman.FreshmanRecommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * 验证新用户是否首单
 *
 * @author tanggb
 * @date 2019/03/09 11:40
 */
@Service("FreshmanRecommendService")
public class FreshmanRecommendServiceImpl implements FreshmanRecommendService {

    @Autowired
    private FreshmanRecommendMapper freshmanRecommendMapper;
    @Autowired
    private FreshmanTrackMapper freshmanTrackMapper;

    @Override
    public BigDecimal countUserSpending(Long cpId) {
        //用户累计消费金额
        return freshmanTrackMapper.queryConsumedMoneyByCpid(cpId);
    }

    @Override
    public List<Product> findFreshmanProduct(BigDecimal price) {

        //通过价格来筛选价格相近，且大于该价格的10个商品
        return freshmanRecommendMapper.findFreshmanProduct(price);
    }

    @Override
    public List<Product> findFreshmanProductStrong(BigDecimal price) {
        //强推逻辑
        return freshmanRecommendMapper.findFreshmanProductByStrongPush(price);
    }

    @Override
    public Integer countByStrongPush() {

        return freshmanRecommendMapper.countByStrongPush();
    }

    @Override
    public Integer findVipType(Long cpId) {
        return freshmanRecommendMapper.findVipType(cpId);
    }

}

