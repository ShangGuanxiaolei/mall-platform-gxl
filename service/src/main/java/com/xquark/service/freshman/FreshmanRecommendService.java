package com.xquark.service.freshman;


import com.xquark.dal.model.Product;
import org.neo4j.cypher.internal.compiler.v2_1.ast.In;
import scala.Int;

import java.math.BigDecimal;
import java.util.List;

/**
 * 新人专区推荐商品
 *
 * @author tanggb
 * @date 2019/03/09 11:32
 */
public interface FreshmanRecommendService {

    //获取用户消费金额
    BigDecimal countUserSpending(Long cpId);

    List<Product> findFreshmanProduct(BigDecimal price);
    List<Product> findFreshmanProductStrong(BigDecimal price);
    Integer countByStrongPush();

    Integer findVipType(Long cpId);
}