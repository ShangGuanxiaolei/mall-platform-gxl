package com.xquark.service.freshman.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 新人推荐商品
 *
 * @author tanggb
 * @date 2019/03/09 17:18
 */
public class FreshManRecommendProduct {
    private Long id;
    private String name;
    private BigDecimal price;
    private BigDecimal exchangePrice;
    private BigDecimal deductionDPoint;
    private String img;  // 主图code
    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String imgUrl;
}