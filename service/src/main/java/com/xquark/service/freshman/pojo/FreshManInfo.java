package com.xquark.service.freshman.pojo;

import java.util.List;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/5
 * Time:14:22
 * Des:新人页返回数据
 */
public class FreshManInfo {
    private int type; // 1:banner 2：新人专享 3：底部品牌宣传
    private List<FreshManBannerData> bannerList;
    private List<FreshManProductTab> tabList;
    private FreshManBottom bottom;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<FreshManBannerData> getBannerList() {
        return bannerList;
    }

    public void setBannerList(List<FreshManBannerData> bannerList) {
        this.bannerList = bannerList;
    }

    public List<FreshManProductTab> getTabList() {
        return tabList;
    }

    public void setTabList(List<FreshManProductTab> tabList) {
        this.tabList = tabList;
    }

    public FreshManBottom getBottom() {
        return bottom;
    }

    public void setBottom(FreshManBottom bottom) {
        this.bottom = bottom;
    }
}