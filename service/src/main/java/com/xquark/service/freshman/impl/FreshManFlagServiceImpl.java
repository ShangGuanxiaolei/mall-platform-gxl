package com.xquark.service.freshman.impl;

import com.xquark.dal.mapper.FreshManFlagMapper;
import com.xquark.dal.model.FreshManFlagVo;
import com.xquark.service.freshman.FreshManFlagService;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.freshman.pojo.FreshManToast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/11
 * Time:10:57
 * Des:新人、用户升级弹框相关
 */
@Service
public class FreshManFlagServiceImpl implements FreshManFlagService {
    private Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private FreshManFlagMapper freshManFlagMapper;
    @Autowired
    private FreshManService freshManService;

    /**
     * 新人flag表中是否存在数据
     * @param cpId
     * @return
     */
    @Override
    public boolean isExistFlagData(Long cpId) {
        boolean b = this.freshManFlagMapper.selectIsExistByCpId(cpId);
        return b;
    }

    /**
     * 插入数据
     * @param freshManToast
     * @return
     */
    @Override
    public boolean insertFlag4Toast(FreshManToast freshManToast) {
        if(freshManToast == null){
            log.info("升级表flag状态时用户信息不能为空");
            return false;
        }
        Long cpId = freshManToast.getCpId();
        if(cpId == null){
            log.info("首次更新用户升级表flag状态，cpid不能为空");
            return false;
        }
        FreshManFlagVo freshManFlagVo = this.convertToFreshManFlagVo(cpId, freshManToast);
        boolean existFlagData = this.isExistFlagData(cpId);
        if(!existFlagData){
            freshManFlagVo.setCreatedAt(new Date());
            boolean b = this.freshManFlagMapper.insert(freshManFlagVo);
            if(b){
                log.info(new StringBuffer().append("新用户插入一条freshman_flag数据")
                        .append("对应的cpid是")
                        .append(cpId)
                        .append("{}对应数据是:")
                        .append(freshManFlagVo)
                        .toString());
            }
            return b;
        }
        return false;
    }

    /**
     *更新数据
     * @param freshManToast
     * @return
     */
    @Override
    public boolean updateFlag4Toast(FreshManToast freshManToast) {
        if(freshManToast == null){
            log.info("更新用户升级表flag状态时，用户信息不能为空");
        }
        Long cpId = freshManToast.getCpId();
        if(cpId == null){
            log.info("更新用户升级表flag状态，cpid不能为空");
            return false;
        }
        //type是1表示新人注册，需要更新一下user表中的是否新注册用户发送德分状态
        if(freshManToast.getType() != null && freshManToast.getType() == 1){
            this.freshManService.updateFreshManPointStataus(freshManToast.getCpId());
        }
        //是否存在
        boolean existFlagData = this.isExistFlagData(cpId);
        //不存在，则插入一条
        if(!existFlagData){
            return this.insertFlag4Toast(freshManToast);
        }

        //存在、则更新状态
        FreshManFlagVo freshManFlagVo = this.convertToFreshManFlagVo(cpId, freshManToast);
        boolean b = this.freshManFlagMapper.update(freshManFlagVo);
        return b;
    }

    @Override
    public FreshManFlagVo selectFlag4Toast(Long cpId) {
        return freshManFlagMapper.selectFlag4Toast(cpId);
    }

    @Override
    public boolean insert(FreshManFlagVo f) {
        if(f == null || f.getCpId() == null){
            return false;
        }
        try {
            return this.freshManFlagMapper.insert(f);
        }catch (Exception e){
            log.error(new StringBuffer()
            .append("cpid是：")
            .append(f.getCpId())
            .append("插入freshm_flag表数据失败")
            .append("异常信息是{}")
            .append(e.getMessage())
            .toString());
        }
        return false;
    }

    @Override
    public boolean update(FreshManFlagVo f) {
        if(f == null || f.getCpId() == null){
            return false;
        }
        try {
            boolean b = this.isExistFlagData(f.getCpId());
            if(b){
                //存在则更新
                return freshManFlagMapper.update(f);
            }
            return this.insert(f);
        }catch (Exception e){
            log.error(new StringBuffer()
                    .append("cpid是：")
                    .append(f.getCpId())
                    .append("更新freshm_flag表数据失败")
                    .append("异常信息是{}")
                    .append(e.getMessage())
                    .toString());
        }
        return false;
    }

    /**
     * 更新记录vip时间
     * @param f
     * @return
     */
    @Override
    public boolean updateVipTime(FreshManToast f) {
        if(f == null || f.getCpId() == null){
            return false;
        }
        FreshManFlagVo freshManFlagVo = new FreshManFlagVo();
        freshManFlagVo.setCpId(f.getCpId());
        //记录升级vip时间
        this.setVipUpTime(f, freshManFlagVo);
        // 记录升级VIP最早时间
        FreshManFlagVo beforeFreshman = freshManFlagMapper.selectFlag4Toast(f.getCpId());
        if (beforeFreshman != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, 1);
            Date beforeDate = beforeFreshman.getEffectAt() == null ? c.getTime() : beforeFreshman.getEffectAt();
            if (freshManFlagVo.getEffectAt().before(beforeDate)) {
                return this.update(freshManFlagVo);
            }
        }

        return false;
    }

    private FreshManFlagVo convertToFreshManFlagVo(Long cpId, FreshManToast f) {
        FreshManFlagVo freshManFlagVo = new FreshManFlagVo();
        freshManFlagVo.setCpId(cpId);
        this.checkFlagType(f, freshManFlagVo);
        //记录升级vip时间
        this.setVipUpTime(f, freshManFlagVo);
        return freshManFlagVo;
    }

    /**
     * 记录升级vip时间
     * @param f
     * @param freshManFlagVo
     */
    private boolean setVipUpTime(FreshManToast f, FreshManFlagVo freshManFlagVo) {
        if(f == null){
            return false;
        }
        if(f.getVipType() == null){
            return false;
        }
        freshManFlagVo.setVipType(f.getVipType());
        freshManFlagVo.setEffectAt(f.getRecordTime());
        return true;
    }

    /**
     * 1注册赠送德分弹窗，2首单完成弹窗，3消费满200弹窗，4成功升级VIP弹窗
     * 注意：若升级直接升级vip则其他的默认改为已经弹框
     * @param f
     * @param freshManFlagVo
     * @return
     */
    private boolean checkFlagType(FreshManToast f, FreshManFlagVo freshManFlagVo) {
        if(f == null)
            return false;

        if(f.getType() == null)
            return false;

        switch (f.getType()){
            case 1:
                freshManFlagVo.setRegisterFlag(1);
                freshManFlagVo.setRegisterFlagAt(new Date());
                break;
            case 2:
                freshManFlagVo.setRegisterFlag(1);
                freshManFlagVo.setFirstFlag(1);
                freshManFlagVo.setFirstFlagAt(new Date());
                break;
            case 3:
                freshManFlagVo.setRegisterFlag(1);
                freshManFlagVo.setFirstFlag(1);
                freshManFlagVo.setReachFlag(1);
                freshManFlagVo.setReachFlagAt(new Date());
                break;
            case 4:
                freshManFlagVo.setRegisterFlag(1);
                freshManFlagVo.setFirstFlag(1);
                freshManFlagVo.setReachFlag(1);
                freshManFlagVo.setVipFlag(1);
                freshManFlagVo.setVipFlagAt(new Date());
                break;
            case 5:
                freshManFlagVo.setRegisterFlag(1);
                freshManFlagVo.setFirstFlag(1);
                freshManFlagVo.setReachFlag(1);
                freshManFlagVo.setVipFlag(1);
                freshManFlagVo.setVipFlagAt(new Date());
                break;
            default:
                return false;
        }
        return true;
    }
}