package com.xquark.service.freshman;

import com.xquark.dal.model.*;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.freshman.pojo.FreshManBannerData;
import com.xquark.service.freshman.pojo.FreshManBottom;
import com.xquark.service.freshman.pojo.FreshManInfo;
import com.xquark.service.freshman.pojo.FreshManPageInfo;
import com.xquark.service.freshman.pojo.FreshManProduct;
import com.xquark.service.freshman.pojo.FreshManProductTab;
import com.xquark.service.freshman.pojo.FreshManShareData;
import com.xquark.service.freshman.pojo.FreshManZoneTitle;
import com.xquark.service.freshman.pojo.ToastInfo;
import com.xquark.service.freshman.vo.QueryCustomerRelationshipResponseVo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/5
 * Time:11:45
 * Des:
 */
public interface FreshManService {
    /**
     * 获取新人福利页面数据
     * @param cpId
     * @return
     */
    FreshManPageInfo selectFreshManPageInfo(Long cpId);

    /**
     * 获取弹框数据
     * @param cpId
     * @return
     */
    ToastInfo selectToastInfo(Long cpId);

    /**
     * 获取单个商品分类及列表数据
     * @return
     */
    FreshManProductTab selectFreshManProductTab(Long cpId);

    /**
     * 获取商品分类及列表数据
     * @return
     */
    List<FreshManProductTab> selectFreshManProductTabList(Long cpId);

    /**
     * 获取商品分类数据
     * @param tabId
     * @return
     */
    List<FreshManProduct> selectFreshManProductListByTabId(int tabId,Long cpId);

    /**
     * 获取底部数据
     * @return
     */
    FreshManBottom selectFreshManBottom();

    /**
     * 获取新人页面数据
     * @return
     */
    List<FreshManInfo> selectFreshManInfoList(Long cpId);

    /**
     * 获取新人页分享数据
     * @return
     */
    FreshManShareData selectFreshManShareData();

    /**
     * 获取banner数据
     * @return
     */
    List<FreshManBannerData> selectFreshManBannerData();

    /**
     * 获取有效的新人页面tab
     * @return
     */
    List<FreshManAreatTabVo> selectFreshManAreatTabVo();

    /**
     * 获取精品区数据
     * @return
     */
    FreshManZoneTitle selectFreshManZoneTitle();

    /**
     * 更新注册用户发送德分状态
     * @param cpId
     * @return
     */
    boolean updateFreshManPointStataus(Long cpId);

    /**
     * 新人注册发送德分
     * @param cpId
     * @return
     */
    boolean grantRegisterPoint(Long cpId);

    /**
     * 获取德分配置
     */
    Map getPointConfig(String grade);

    /**
     * 获取注册德分
     * @return
     */
    BigDecimal selectRegisterPoint();
    /**
     * 新人满*元 减德分
     */
    void freshManeductPoint(Order order);

    /**
     * 新人满*元 赠送德分
     */
    void freshManGrantPoint(Order order);

    void freshManPieceOrderToVip(Order order);

    void freshManPieceOrderGrantPoint(Order order);

    void frenshManDeductUplinePointAndCommission(Order order);

    /**
     * 获取sku信息
     * @param productId
     * @return
     */
    Sku selectSkuByProductId(String productId);

    List<FreshManProductVo> selectFreshmanProductListById(String id);

    FreshManProductVo selectFreshmanProductById(String id);

    /**
     * 新人商品sku
     * @param productId
     * @return
     */
    List<Sku> selectFreshManSku(String productId);

    /**
     * 新人商品限购
     * @param user
     */
    void freshmanLimit(int count,User user);

    Integer selectFreshmanOrder(Long cpId);

    /**
     * 新人注册发送收益（5）
     * @param cpId
     * @return
     */
    boolean setFreshManCommission(Long cpId);

    /**
     * 绑定关系到结算中心，统一维护到计算中心的功能上线之后需要统一维护，这是临时用的方法
     * @param cpId
     * @param upLine
     * @param type 1为次强关系；2为弱关系；3为强关系
     * @return
     */
    public boolean addCustomerRelationship(Long cpId, Long upLine,String type);

    /**
     * 从结算中心查询关系
     * @param cpId
     * @return
     */
    public QueryCustomerRelationshipResponseVo queryCustomerRelationship(Long cpId);
}