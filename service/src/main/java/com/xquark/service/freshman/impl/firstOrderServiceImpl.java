package com.xquark.service.freshman.impl;

import com.xquark.dal.mapper.FirstOrderMapper;
import com.xquark.dal.model.FreshmanProduct;
import com.xquark.service.freshman.FirstOrderService;
import com.xquark.service.freshman.pojo.FreshManProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 验证新用户是否首单
 *
 * @author tanggb
 * @date 2019/03/05 13:55
 */
@Service("FirstOrderService")
public class firstOrderServiceImpl implements FirstOrderService {

    @Autowired
    private FirstOrderMapper firstOrderMapper;

    @Override
    public Boolean firstOrder(Long cpId) {
        Boolean result = false;
        //通过cpId查询是否有下过订单且支付完成
        Long countOrder = firstOrderMapper.countOrder(cpId);
        if(countOrder < 1 ){
            result = true;
        }
        return result;
    }

    @Override
    public FreshmanProduct freshmanProduct(Long productId) {
        Integer num = firstOrderMapper.countFreshmanProduct(productId);
        List<FreshmanProduct> list = firstOrderMapper.findProductByProcductId(productId);
        if(list == null || list.isEmpty())
            return null;
        //兼容多规格商品默认取第一个
        FreshmanProduct freshmanProduct = list.get(0);
        if(freshmanProduct != null){
            freshmanProduct.setCountProduct(num);
        }
        //通过productId查询是否为新人专区商品
        return freshmanProduct;

    }

    @Override
    public Integer countProductNumber(Long userId) {
        Integer total;
        //查询出用户所有的购买商品的新人商品
        Integer countAmount = firstOrderMapper.countAmount(userId);
        //查询新人专区限购数
        Integer findfreshmanValue = firstOrderMapper.findfreshmanValue();
        total = findfreshmanValue - countAmount;
        return total;
    }

    @Override
    public Integer countCart(Long cpId) {
        //统计购物车内新人商品总数
        return firstOrderMapper.countCart(cpId);
    }

    @Override
    public boolean checkStock(Long productId) {
        boolean result = false;
        Integer checkStock = firstOrderMapper.checkStock(productId);
        if(checkStock > 0){
            result = true;
        }
        return result;
}

    @Override
    public Boolean firstOrderOfFinishPayPage(String buyerId, String mainOrderId) {
        Boolean result = false;
        //通过cpId查询主订单,是否有该笔订单之前的订单
        Long countOrder = firstOrderMapper.countMainOrder(buyerId, mainOrderId);
        if(countOrder < 1 ){
            result = true;
        }
        return result;
    }

    @Override
    public Boolean countMainOrderByPiece(String buyerId, String mainOrderId) {
        boolean result = false;
        Long countOrderByPiece = firstOrderMapper.countMainOrderByPiece(buyerId, mainOrderId);
        if(countOrderByPiece >= 1 ){
            result = true;
        }
        return result;
    }
}
