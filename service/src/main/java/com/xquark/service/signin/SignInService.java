package com.xquark.service.signin;

import com.xquark.aop.anno.IdEncode;
import com.xquark.aop.anno.NotNull;
import com.xquark.dal.model.SignIn;
import com.xquark.dal.model.SignInRule;
import com.xquark.dal.vo.SignInVO;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

/**
 * Created by wangxinhua on 17-8-17. DESC:
 */
public interface SignInService {

  List<SignInRule> listSignInRule(String order, Direction direction, Pageable pageable);

  List<SignInRule> listSignInRule();

  boolean save(SignIn signIn);

  boolean delete(String id);

  boolean update(SignIn signIn);

  SignInVO signIn(SignIn signIn);

  SignIn load(String id);

  /**
   * 查询出最后一次签到
   *
   * @param userId 用户id
   * @return SignIn 签到对象
   */
  SignIn loadLastModifiedByUser(String userId);

  /**
   * 查询用户签到周期中最后一个周期的第一次签到
   *
   * @param userId 用户id
   * @return 签到对象
   */
  SignIn loadLastCycleBegin(String userId);

  /**
   * 获取目前的签到规则 在{@link SignInService} 加载时初始化签到规则，如果后续更新了签到规则则需调用 {@code reLoadSignInRule} 刷新
   *
   * @return SignInRule 全局签到规则
   */
  SignInRule loadSignInRule();

  /**
   * 根据id获取签到规则 在{@link SignInService} 加载时初始化签到规则，如果后续更新了签到规则则需调用 {@code reLoadSignInRule} 刷新
   *
   * @return SignInRule id对应的签到规则
   */
  SignInRule loadSignInRule(String id);

  /**
   * 重新从数据库中刷新签到规则 若数据库中没有配置则使用默认配置
   */
  void reLoadSignInRule();

  /**
   * 删除签到规则
   *
   * @return true or false
   */
  boolean deleteSignInRule(String id);

  /**
   * 保存签到规则
   *
   * @param signInRule 签到规则对象
   * @return true or false
   */
  boolean saveSignInRule(@NotNull @IdEncode SignInRule signInRule);

  /**
   * 根据id是否为blank判断需要增加还是更新
   *
   * @param signInRule 签到规则对象
   * @return true or false
   */
  boolean saveOrUpdateRule(@NotNull SignInRule signInRule);

  /**
   * 获取签单生成器
   *
   * @return 签到生成器对象
   */
  SignInGenerator getGenerator();
}
