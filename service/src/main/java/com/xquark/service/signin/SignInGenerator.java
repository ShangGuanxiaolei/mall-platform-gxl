package com.xquark.service.signin;

import com.xquark.dal.status.LongOperation;
import com.xquark.helper.Generator;
import java.util.Arrays;

/**
 * Created by wangxinhua on 17-11-23. DESC: 签到生成器
 */
public class SignInGenerator implements Generator<Long> {

  /**
   * 初始值
   */
  private long init;

  /**
   * 当前值
   */
  private long curr;

  /**
   * 最大获取积分数, 默认为int最大值
   */
  private long max;

  /**
   * 运算方式, 默认为累加
   */
  private LongOperation operation;

  /**
   * 运算数
   */
  private int operationNumber;

  private SignInGenerator() {
  }

  public static class Builder {

    private long init = 1;

    private long max = Long.MAX_VALUE;

    private LongOperation operation = LongOperation.PLUS;

    private int operationNumber = 1;

    public Builder init(long init) {
      this.init = init;
      return this;
    }

    public Builder max(long max) {
      this.max = max;
      return this;
    }

    public Builder operation(LongOperation operation) {
      this.operation = operation;
      return this;
    }

    public Builder operationNumber(int number) {
      this.operationNumber = number;
      return this;
    }

    public SignInGenerator create() {
      SignInGenerator generator = new SignInGenerator();
      generator.curr = generator.init = this.init;
      generator.max = this.max;
      generator.operation = this.operation;
      generator.operationNumber = this.operationNumber;
      return generator;
    }

  }

  @Override
  public Long next() {
    if (curr >= max) {
      return max;
    }
    long curr = this.curr;
    this.curr = operation.apply(curr, (long) operationNumber);
    return curr;
  }

  @Override
  public Long[] limit(int times) {
    Long[] arr = new Long[times];
    int i = 0;
    while (i < times) {
      arr[i] = this.next();
      i++;
    }
    return arr;
  }

  @Override
  public Generator<Long> clear() {
    this.curr = this.init;
    return this;
  }

  public static Builder getBuilder() {
    return new Builder();
  }

  public long getCurr() {
    return curr;
  }

  public long getMax() {
    return max;
  }

  public LongOperation getOperation() {
    return operation;
  }

  public int getOperationNumber() {
    return operationNumber;
  }

  public static void main(String[] args) {
    Generator<Long> generator = SignInGenerator.getBuilder()
        .init(2)
        .operation(LongOperation.MULTIPLY)
        .operationNumber(3)
        .max(9)
        .create();
    System.out.println(Arrays.toString(generator.limit(7)));
    System.out.println(Arrays.toString(generator.clear().limit(7)));
  }
}
