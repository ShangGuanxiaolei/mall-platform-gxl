package com.xquark.service.signin;

import com.xquark.helper.Generator;
import com.xquark.utils.DateUtils;
import java.util.Date;

/**
 * Created by wangxinhua on 17-11-24. DESC:
 */
public class DayGenerator implements Generator<Date> {

  private Date init;

  private Date curr;

  public DayGenerator() {
    this.curr = this.init = DateUtils.getStartOfDay(new Date());
  }

  public DayGenerator(Date init) {
    this.curr = this.init = DateUtils.getStartOfDay(init);
  }

  @Override
  public Date next() {
    Date tmp = curr;
    curr = DateUtils.getNextDayStart(tmp);
    return tmp;
  }

  @Override
  public Date[] limit(int times) {
    Date[] result = new Date[times];
    int i = 0;
    while (i < times) {
      result[i] = next();
      i++;
    }
    return result;
  }

  @Override
  public Generator<Date> clear() {
    curr = init;
    return this;
  }
}
