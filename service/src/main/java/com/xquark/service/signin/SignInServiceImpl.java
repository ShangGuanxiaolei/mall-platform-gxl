package com.xquark.service.signin;

import com.xquark.aop.anno.IdEncode;
import com.xquark.aop.anno.NotNull;
import com.xquark.dal.mapper.SignInMapper;
import com.xquark.dal.mapper.SignInRuleMapper;
import com.xquark.dal.model.SignIn;
import com.xquark.dal.model.SignInRule;
import com.xquark.dal.model.User;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.LongOperation;
import com.xquark.dal.type.YundouOperationType;
import com.xquark.dal.vo.SignInVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.user.UserService;
import com.xquark.service.yundou.Result;
import com.xquark.service.yundou.YundouAmountService;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangxinhua on 17-8-17. DESC:
 */
@Service
public class SignInServiceImpl implements SignInService, InitializingBean {

  private static Logger logger = LoggerFactory.getLogger(SignInServiceImpl.class);

  private UserService userService;

  private SignInMapper signInMapper;

  private SignInRuleMapper signInRuleMapper;

  private YundouAmountService yundouAmountService;

  /* FIXME wangxinhua 更新规则后对于已经签到过的用户如何更新 */
  private volatile SignInRule signInRule;

  @Autowired
  public void setSignInMapper(SignInMapper signInMapper) {
    this.signInMapper = signInMapper;
  }

  @Autowired
  public void setYundouAmountService(YundouAmountService yundouAmountService) {
    this.yundouAmountService = yundouAmountService;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setSignInRuleMapper(SignInRuleMapper signInRuleMapper) {
    this.signInRuleMapper = signInRuleMapper;
  }

  @Override
  public List<SignInRule> listSignInRule(String order, Direction direction, Pageable pageable) {
    return signInRuleMapper.listSignInRule(order, direction, pageable);
  }

  @Override
  public List<SignInRule> listSignInRule() {
    return this.listSignInRule(null, null, null);
  }

  @Override
  public boolean save(SignIn signIn) {
    return signInMapper.insert(signIn) > 0;
  }

  @Override
  @Transactional
  public SignInVO signIn(SignIn signIn) {
    boolean insertResult = this.save(signIn);
    if (!insertResult) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "签到保存失败");
    }
    String id = String.valueOf(IdTypeHandler.encode(Long.parseLong(signIn.getId())));
    // 同步修改积分数量
    List<Result> modifyResult;
    try {
      modifyResult = yundouAmountService.modifyAmount(YundouOperationType.SIGN_IN, id);
    } catch (Exception e) {
      logger.error("用户 {} 签到后保存积分明细失败", signIn.getUserId(), e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "签到失败");
    }
    if (modifyResult == null || modifyResult.size() < 1) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "积分保存失败");
    }
    logger.info("用户 {} 第 {} 天签到完成, 连续签到 {}，明细: {}", signIn.getUserId(),
        signIn.getSignSum(), signIn.getSignCount(), modifyResult);

    Result result = modifyResult.get(0);

    String userId = result.getUserId();
    Long modifiedAmount = result.getAmount();
    User user = userService.load(userId);
    Long userAmount = user.getYundou();

    return new SignInVO(signIn, modifiedAmount, userAmount);
  }

  @Override
  public boolean delete(String id) {
    return signInMapper.deleteByPrimaryKey(id) > 0;
  }

  @Override
  public boolean update(SignIn signIn) {
    return signInMapper.updateByPrimaryKeySelective(signIn) > 0;
  }

  @Override
  public SignIn load(String id) {
    return signInMapper.selectByPrimaryKey(id);
  }

  @Override
  public SignIn loadLastModifiedByUser(String userId) {
    return signInMapper.selectLastModifiedByUser(userId);
  }

  @Override
  public SignIn loadLastCycleBegin(String userId) {
    return signInMapper.selectLastCycleBegin(userId);
  }

  @Override
  public SignInRule loadSignInRule() {
    return this.signInRule;
  }

  @Override
  public SignInRule loadSignInRule(String id) {
    return signInRuleMapper.selectByPrimaryKey(id);
  }

  @Override
  public void reLoadSignInRule() {
    SignInRule rule = signInRuleMapper.selectOne();
    if (rule == null) {
      rule = new SignInRule();
      rule.setInit(1L);
      rule.setOperation(LongOperation.PLUS);
      rule.setOperationNumber(1);
      rule.setMax(30L);
    }
    this.signInRule = rule;
  }

  @Override
  public boolean deleteSignInRule(String id) {
    return signInRuleMapper.deleteByPrimaryKey(id) > 0;
  }

  @Override
  public boolean saveSignInRule(@NotNull @IdEncode SignInRule signInRule) {
    return signInRuleMapper.insert(signInRule) > 0;
  }

  @Override
  public boolean saveOrUpdateRule(@NotNull SignInRule signInRule) {
    String id = signInRule.getId();
    boolean result;
    if (StringUtils.isBlank(id)) {
      result = signInRuleMapper.insert(signInRule) > 0;
    } else {
      result = signInRuleMapper.updateByPrimaryKeySelective(signInRule) > 0;
    }
    if (result) {
      this.reLoadSignInRule();
    }
    return result;
  }

  @Override
  public SignInGenerator getGenerator() {
    SignInRule rule = this.loadSignInRule();
    return SignInGenerator.getBuilder()
        .init(rule.getInit())
        .max(rule.getMax())
        .operation(rule.getOperation())
        .operationNumber(rule.getOperationNumber())
        .create();
  }

  @Override
  public void afterPropertiesSet() {
    reLoadSignInRule();
  }
}
