package com.xquark.service.cache;

import com.xquark.service.cache.constant.CacheKeyConstant;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;


/**
 * @author Jack Zhu
 * @since 2018/12/27
 */
public class GenerateKeyUtil {


    public static final String FIRST_PAGE_KEY = "PRODUCTS::categoryId::0::order::null::direction::null::includeBlackList::null::priceStart::null::priceEnd::null::0::0::20::null";
    private GenerateKeyUtil() {
    }

    public static String generateVipSuitById(String productId) {
        StringBuilder stringBuilder = new StringBuilder(CacheKeyConstant.PRODUCT_VIP);
        return String.format(stringBuilder.toString(), productId);
    }

    public static String getHotSearchKey(String id) {
        StringBuilder stringBuilder = new StringBuilder(CacheKeyConstant.HOT_SEARCH_KEYS);
        return String.format(stringBuilder.toString(), id);
    }

    public static String generateListFlashKey(Boolean isAvailable,
                                              String order, String direction, Pageable pageable) {
        StringBuilder stringBuilder = new StringBuilder(CacheKeyConstant.LIST_FLASH_KEY);
        return  String.format(stringBuilder.toString(), isAvailable, order, direction,
                pageable.getOffset(),
                pageable.getSort(),
                pageable.getPageNumber(),
                pageable.getPageSize());
    }


    public static String generateProductKey(String categoryId,
                                            String order,
                                            String direction,
                                            String includeBlackList,
                                            List<String> subCategoryIds,
                                            String priceStart,
                                            String priceEnd,
                                            Pageable pageable) {
        StringBuilder stringBuilder = new StringBuilder(CacheKeyConstant.PRODUCT_KEY);
        if (CollectionUtils.isNotEmpty(subCategoryIds)) {
            for (String subCategoryId : subCategoryIds) {
                stringBuilder.append("::").append(subCategoryId);
            }
        }
        String key = stringBuilder.toString();
        key = String.format(key,
                categoryId,
                order,
                direction,
                includeBlackList,
                priceStart,
                priceEnd,
                pageable.getOffset(),
                pageable.getPageNumber(),
                pageable.getPageSize(),
                pageable.getSort());
        return key;
    }
}
