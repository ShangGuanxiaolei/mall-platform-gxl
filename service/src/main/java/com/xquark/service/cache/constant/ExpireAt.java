package com.xquark.service.cache.constant;

/**
 * @author Jack Zhu
 * @since 2019/01/03
 */
public enum ExpireAt {
    /**
     * 五秒
     */
    FIVE,
    /**
     * 六十秒
     */
    SIXTH
}
