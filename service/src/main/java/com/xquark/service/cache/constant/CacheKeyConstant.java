package com.xquark.service.cache.constant;

/**
 * @author Jack Zhu
 * @since 2018/12/27
 */
public abstract class CacheKeyConstant {
    public static final String PRODUCT_KEY = "PRODUCTS::categoryId::%s::order::%s::direction::%s::includeBlackList::%s::priceStart::%s::priceEnd::%s::%s::%s::%s::%s";
    public static final String LIST_FLASH_KEY = "FLASH::isAvailable::true::order::#{order}::direction::#{direction}::pageable::#{pageable.offset}::#{pageable.sort}::#{pageable.pageNumber}::#{pageable.pageSize}";
    public static final String HOME_CAROUSELS = "/home/carousels";
    public static final String HOT_SEARCH_KEYS = "HOT_SEARCH_KEYS_id::#{id}";
    public static final String PRODUCT_TOP = "/home/getProductTop";
    public static final String PRODUCT_VIP = "PRODUCTS::VIP::#{id}";
    public static final String PRODUCT_SUITS_VIP = "VIP_PRODUCTS::#{productId}";
    public static final String LOAD_CATEGORY_BY_PRODUCTID = "LOAD_CATEGORY_BY_PRODUCTID::#{productId}";
    public static final String FLASHSALE_GROUP_KEY = "FLASH_GROUP::#{grandSaleId}";
}
