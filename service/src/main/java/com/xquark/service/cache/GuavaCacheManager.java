package com.xquark.service.cache;

import com.google.common.cache.CacheBuilder;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

/**
 * @author Jack Zhu
 * @since 2019/01/02
 */
@Component
public class GuavaCacheManager {

    //TODO 待优化
    private final ConcurrentMap<Object, Object> defaultCacheMap = CacheBuilder.newBuilder()
            .initialCapacity(16)
            .maximumSize(500)
            .concurrencyLevel(16)
            .expireAfterWrite(60, TimeUnit.SECONDS)
            .build()
            .asMap();

    private final ConcurrentMap<Object, Object> instantCacheMap = CacheBuilder.newBuilder()
            .initialCapacity(16)
            .maximumSize(100)
            .concurrencyLevel(8)
            .expireAfterWrite(5, TimeUnit.SECONDS)
            .build()
            .asMap();

    public Object get(Object key) {
        return defaultCacheMap.get(key);
    }

    public void putIfExpire(Object key, Object data) {
        if(Objects.nonNull(key)&&Objects.nonNull(data))
        defaultCacheMap.put(key, data);
    }

    public Object getByInstantMap(Object key) {
        return instantCacheMap.get(key);
    }

    public void putByInstantMap(Object key, Object data) {
        if(Objects.nonNull(key)&&Objects.nonNull(data))
        instantCacheMap.put(key, data);
    }

}
