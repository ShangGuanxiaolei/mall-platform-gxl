package com.xquark.service.polyapiOrder;

import com.xquark.dal.vo.PolyapiGoodInfoVO;
import com.xquark.dal.vo.PolyapiOrderItemVO;
import java.util.Date;
import java.util.List;

/**
 * User: kong Date: 2018/6/16. Time: 22:57 菠萝派订单
 */
public interface PolyapiOrderService {

  /**
   * 查询所有订单数量
   */
  String getNumTotalAllOrder();

  /**
   * 根据状态查询订单数量
   */
  String getNumTotalOrder(String status);

  /**
   * 查询订单
   */
  List<PolyapiOrderItemVO> getPolyapiOrderItem(Integer pageIndex, Integer pageSize,
      Date StartTime,Date EndTime,String OrderStatus);

  /**
   * 查询所有订单
   */
  List<PolyapiOrderItemVO> getPolyapiAllOrderItem(Integer pageIndex, Integer pageSize,
      Date StartTime,Date EndTime);

  /**
   * 根据订单号查找商品
   */
  List<PolyapiGoodInfoVO> getPolyapiGoodInfoByOrderNo(String orderNo);

  /**
   * 查找订单
   */
  List<PolyapiOrderItemVO> getPolyapiOrder(Integer pageIndex, Integer pageSize,Date StartTime,
      Date EndTime,String OrderStatus);

  /**
   * 查找所有订单
   */
  List<PolyapiOrderItemVO> getPolyapiAllOrder(Integer pageIndex, Integer pageSize,Date StartTime,
      Date EndTime);
}