package com.xquark.service.polyapiOrder.impl;

import com.xquark.dal.mapper.PolyapiOrderVOMapper;
import com.xquark.dal.vo.PolyBusinessOrderStatus;
import com.xquark.dal.vo.PolyapiGoodInfoVO;
import com.xquark.dal.vo.PolyapiOrderItemVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.polyapiOrder.PolyapiOrderService;
import com.xquark.service.systemRegion.SystemRegionService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: kong Date: 2018/6/16. Time: 23:00 菠萝派订单
 */
@Service
@Transactional
public class PolyapiOrderServiceImpl extends BaseServiceImpl implements PolyapiOrderService {

  @Autowired
  private PolyapiOrderVOMapper baseMapper;

  @Autowired
  private SystemRegionService systemRegionService;//新的地区

  @Override
  public String getNumTotalOrder(String status) {
    return baseMapper.getNumTotalOrder(status);
  }

  public String getNumTotalAllOrder() {
    return baseMapper.getNumTotalAllOrder();
  }

  private List<PolyapiOrderItemVO> list = new ArrayList<PolyapiOrderItemVO>();

  public List<PolyapiOrderItemVO> getPolyapiOrderItem(Integer pageIndex, Integer pageSize,
      Date StartTime,Date EndTime,String OrderStatus) {
    return baseMapper.getPolyapiOrderItem(pageIndex,pageSize,StartTime,EndTime,OrderStatus);
  }

  public List<PolyapiOrderItemVO> getPolyapiAllOrderItem(Integer pageIndex, Integer pageSize,
      Date StartTime,Date EndTime) {
    return baseMapper.getPolyapiAllOrderItem(pageIndex,pageSize,StartTime,EndTime);
  }

  @Override
  public List<PolyapiGoodInfoVO> getPolyapiGoodInfoByOrderNo(String orderNo) {
    return baseMapper.getPolyapiGoodInfoByOrderNo(orderNo);
  }

  public List<PolyapiOrderItemVO> getPolyapiOrder(Integer pageIndex, Integer pageSize,
      Date StartTime, Date EndTime, String OrderStatus) {
    try {
      list = baseMapper.getPolyapiOrderItem((pageIndex - 1) * pageSize,
          pageSize, StartTime, EndTime, OrderStatus);
      for (PolyapiOrderItemVO p : list) {
        p.setGoodInfos(baseMapper.getPolyapiGoodInfoByOrderNo(p.getOrderId()));
        p.setTradeStatus(PolyBusinessOrderStatus.getPolyStatusByOrderStatus(
            com.xquark.dal.status.OrderStatus.valueOf(p.getTradeStatus())).name());
        try {
          p.setArea(systemRegionService.load(p.getZip()).getName());
          p.setCity(systemRegionService.findParent(p.getZip()).getName());
          p.setProvince(systemRegionService.findParent(Long.toString(
              systemRegionService.findParent(p.getZip()).getId())).getName());
        }catch (Exception e){
          p.setArea("");
          p.setCity("");
          p.setProvince("");
          e.printStackTrace();
        }
      }
    } catch (NullPointerException e) {
      throw new NullPointerException("没有对应日期或地区的订单");
    }
    return list;
  }

  public List<PolyapiOrderItemVO> getPolyapiAllOrder(Integer pageIndex, Integer pageSize,
      Date StartTime,Date EndTime){
    try {
      list = baseMapper.getPolyapiAllOrderItem((pageIndex - 1) * pageSize,
          pageSize, StartTime, EndTime);
      for (PolyapiOrderItemVO p : list) {
        p.setGoodInfos(baseMapper.getPolyapiGoodInfoByOrderNo(p.getOrderId()));
        p.setTradeStatus(PolyBusinessOrderStatus.getPolyStatusByOrderStatus(
            com.xquark.dal.status.OrderStatus.valueOf(p.getTradeStatus())).name());
        try {
          p.setArea(systemRegionService.load(p.getZip()).getName());
          p.setCity(systemRegionService.findParent(p.getZip()).getName());
          p.setProvince(systemRegionService.findParent(Long.toString(
              systemRegionService.findParent(p.getZip()).getId())).getName());
        }catch (Exception e){
          p.setArea("");
          p.setCity("");
          p.setProvince("");
          e.printStackTrace();
        }
      }
    } catch (NullPointerException e) {
      throw new NullPointerException("没有对应日期或地区的订单");
    }
    return list;
  }
}
