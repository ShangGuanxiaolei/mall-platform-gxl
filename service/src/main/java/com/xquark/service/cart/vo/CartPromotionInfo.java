package com.xquark.service.cart.vo;

import com.xquark.utils.functional.Consumer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangxinhua on 17-11-3. DESC:
 */
public class CartPromotionInfo {

    /**
     * 是否参与限量秒杀
     */
    private boolean inFlashSale;

    /**
     * 是否参与满减
     */
    private boolean inFullCut;

    /**
     * 是否参与满件
     */
    private boolean inFullPieces;

    /**
     * 是否使用优惠券
     */
    private boolean inCoupon;

    public List<String> getPromotions() {
        List<String> result = new ArrayList<String>();
        if (inFlashSale) {
            result.add("限量秒杀");
        }
        if (inCoupon) {
            result.add("优惠券");
        }
        if (inFullCut) {
            result.add("满减");
        }
        if (inFullPieces) {
            result.add("满件");
        }
        return result;
    }

    /**
     * 提供一个外部回调修改内部状态
     * @param notifier 通知者
     */
    public CartPromotionInfo withState(Consumer<CartPromotionInfo> notifier) {
        if (notifier != null) {
            notifier.accept(this);
        }
        return this;
    }

    public void setInFlashSale(boolean inFlashSale) {
        this.inFlashSale = inFlashSale;
    }

    public void setInFullCut(boolean inFullCut) {
        this.inFullCut = inFullCut;
    }

    public void setInFullPieces(boolean inFullPieces) {
        this.inFullPieces = inFullPieces;
    }

    public void setInCoupon(boolean inCoupon) {
        this.inCoupon = inCoupon;
    }

    public boolean getInFlashSale() {
        return inFlashSale;
    }

    public boolean getInFullCut() {
        return inFullCut;
    }

    public boolean getInFullPieces() {
        return inFullPieces;
    }

    public boolean getInCoupon() {
        return inCoupon;
    }

    @Override
    public String toString() {
        return "CartPromotionInfo{" +
                "isFlashSale=" + inFlashSale +
                ", inFullCut=" + inFullCut +
                ", inFullPieces=" + inFullPieces +
                ", inCoupon=" + inCoupon +
                '}';
    }

}
