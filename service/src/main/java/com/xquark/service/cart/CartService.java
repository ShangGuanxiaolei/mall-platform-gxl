package com.xquark.service.cart;

import com.xquark.dal.model.CartItem;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.User;
import com.xquark.dal.type.PromotionType;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.product.vo.ProductVO;

import java.util.List;
import java.util.Set;

/**
 * @author ahlon
 */
public interface CartService {

  CartItem addToCart(String userId, String skuId, int amount, String shopOwnerId, String productId);

  CartItem addToCart(String userId, String skuId, int amount, String shopOwnerId, String productId,
      String realShopId);

  /**
   * 加载购物车项目
   */
  CartItem load(String id);

  /**
   * 按商品sku查找当前登录用户在购物车中的商品项
   */
  CartItem loadBySku(String skuId);

  /**
   * 获取当前用户购物车列表
   */
  List<CartItem> listByUserId(String userId);

  List<CartItemVO> listCartItems();

  List<CartItemVO> listCartItems(User user);

  List<CartItemVO> listCartItems(Set<String> skuIds);

  List<CartItemVO> listCartItems(String shopId);

  /**
   * 不在购物车的sku自动加入用户的购物车, 并且数量默认1
   *
   * @param autoInsert 购物车中不存在的sku，自动添加到购物车
   */
  List<CartItemVO> checkout(Set<String> skuIds);

  List<CartItemVO> checkout(Set<String> skuIds, String realShopId, PromotionType promotionType);

  /**
   * 适用于openapi
   */
  List<CartItemVO> checkout(Set<String> skuIds, User user);


  /**
   * 立即下单，不走购物车流程
   *
   * @param amount 购买数量
   */
  List<CartItemVO> checkout(Set<String> skuIds, int amount);

  List<CartItemVO> checkout(Set<String> skuIds, int amount, String realShopId, PromotionType promotionType);

  /**
   * 适用于openapi模式
   */
  List<CartItemVO> checkout(Set<String> skuIds, int amount, User user);

  List<CartItemVO> checkout(Set<String> skuIds, int amount, User user, String realShopId, PromotionType promotionType);

  /**
   * for webchat
   */
  List<CartItemVO> checkout(Set<String> skuIds, User user, String realShopId, PromotionType promotionType);

  List<CartItemVO> checkout(String shopId, PromotionType promotionType);

  List<CartItemVO> checkout(String shopId, User user, PromotionType promotionType);


  List<CartItemVO> checkout();

  boolean validate(List<String> skuIds);

  boolean validate(String shopId);

  CartItem saveOrUpdateCartItemAmount(String skuId, int amount);

  // 购物车CartItem项更新数量
  boolean updateAmount(String id, int amount);

  boolean updateAmount(String id, int amount, User user);

  boolean remove(String id);

  boolean remove(String id, String userId);

  boolean removeBySku(String skuId);

  void clear();

  void clear(List<String> skuIds);

  void clear(String shopId);

  Integer count();

  Integer count(String userId);

  CartItem saveOrUpdateCartItemAmount(String userId, String skuId, Integer amount);

  List<CartItemVO> checkout2(String userId);

  boolean updateCartUserId(String anonymouId);

  /**
   * 获取用户在某个商铺下的购物车内商品总数
   */
  Integer countByShopId(String userId, String shopId);

  PromotionType getCartPromotionType();

  PromotionType getCartPromotionType(List<CartItemVO> cartItem);

  void checkStadiumProductBuyLimit(Integer amount, Sku sku, ProductVO product);

}
