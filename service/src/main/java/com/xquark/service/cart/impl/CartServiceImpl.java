package com.xquark.service.cart.impl;

import com.google.common.collect.ImmutableSet;
import com.xquark.dal.IUser;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.StadiumType;
import com.xquark.dal.vo.FlashSalePromotionProductVO;
import com.xquark.dal.vo.PromotionSkuVo;
import com.xquark.service.cart.CartService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.product.ProductCombineService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.product.vo.SkuVO;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.promotion.PromotionSkusService;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.shop.ShopService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service("cartService")
public class CartServiceImpl extends BaseServiceImpl implements CartService {

  @Autowired
  private ProductService productService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private CartItemMapper cartItemMapper;

  @Autowired
  private SkuMapper skuMapper;

  @Autowired
  private ProductCombineService productCombineService;

  @Autowired
  private PromotionSkusService promotionSkusService;

  @Autowired
  private FlashSalePromotionProductService flashSaleService;

  @Autowired
  private FirstOrderMapper firstOrderMapper;

  @Autowired
  private PromotionBaseInfoService promotionBaseInfoService;

  @Autowired
  private PromotionFlashSaleMapper promotionFlashSaleMapper;

  @Autowired
  private OrderMapper orderMapper;

  private final static Set<PromotionType> SKIP_CHECK_PROMOTION_TYPE
          = ImmutableSet.of(PromotionType.FLASHSALE, PromotionType.RESERVE);

  @Override
  public CartItem load(String id) {
    return cartItemMapper.selectByPrimaryKey(id);
  }


  @Override
  public boolean updateCartUserId(String anonymouId) {
    User user = (User) this.getCurrentUser();
    int result = cartItemMapper.updateCartUserId(anonymouId, user.getId());
    return result > 0;
  }

  @Override
  public CartItem loadBySku(String skuId) {
    User user = (User) this.getCurrentUser();
    CartItem cartItem = cartItemMapper.selectByUserIdAndSku(user.getId(), skuId);
    return cartItem;
  }

  /**
   * 按sku访问购物车
   */
  @Override
  public List<CartItemVO> checkout(Set<String> skuIds) {
    User user = (User) getCurrentUser();
    return checkout(skuIds, user);
  }

  /**
   * 按sku访问购物车
   */
  @Override
  public List<CartItemVO> checkout(Set<String> skuIds, String realShopId, PromotionType promotionType) {
    User user = (User) getCurrentUser();
    return checkout(skuIds, user, realShopId, promotionType);
  }

  /**
   * 按sku访问购物车
   */
  @Override
  public List<CartItemVO> checkout(Set<String> skuIds, User user) {
    List<CartItemVO> cartItems = new ArrayList<CartItemVO>();
    for (String skuId : skuIds) {
      // 购物车中没有则创建，默认数量为1，有则为购物车中原有的数量
      CartItemVO item = checkup(user.getId(), skuId, true, false, false, null, null, null);
      cartItems.add(item);
    }
    return cartItems;
  }

  /**
   * 按sku访问购物车 for wechat
   */
  @Override
  public List<CartItemVO> checkout(Set<String> skuIds, User user, String realShopId, PromotionType promotionType) {
    List<CartItemVO> cartItems = new ArrayList<>();
    for (String skuId : skuIds) {
      // 购物车中没有则创建，默认数量为1，有则为购物车中原有的数量
      CartItemVO item = checkup(user.getId(), skuId, true, false, false, null, null, null,
          realShopId, promotionType);
      cartItems.add(item);
    }
    return cartItems;
  }

  /**
   * 直接下单，不走购物车流程
   */
  @Override
  public List<CartItemVO> checkout(Set<String> skuIds, int qty) {
    User user = (User) getCurrentUser();
    return checkout(skuIds, qty, user);
  }

  /**
   * 微商直接下单，不走购物车流程
   */
  @Override
  public List<CartItemVO> checkout(Set<String> skuIds, int qty, String realShopId, PromotionType promotionType) {
    User user = (User) getCurrentUser();
    return checkout(skuIds, qty, user, realShopId, promotionType);
  }

  /**
   * 直接下单，不走购物车流程
   */
  @Override
  public List<CartItemVO> checkout(Set<String> skuIds, int qty, User user) {
    List<CartItemVO> cartItems = new ArrayList<CartItemVO>();
    for (String skuId : skuIds) {
      // 购物车中没有则创建，默认数量为1，有则为购物车中原有的数量
      CartItemVO item = checkup(user.getId(), skuId, qty);
      cartItems.add(item);
    }
    return cartItems;
  }

  /**
   * 直接下单，不走购物车流程
   */
  @Override
  public List<CartItemVO> checkout(Set<String> skuIds, int qty, User user, String realShopId, PromotionType promotionType) {
    List<CartItemVO> cartItems = new ArrayList<CartItemVO>();
    for (String skuId : skuIds) {
      // 购物车中没有则创建，默认数量为1，有则为购物车中原有的数量
      CartItemVO item = checkup(user.getId(), skuId, qty, realShopId, promotionType);
      cartItems.add(item);
    }
    return cartItems;
  }


  /**
   * 按店铺访问购物车
   */
  @Override
  public List<CartItemVO> checkout(String shopId, PromotionType promotionType) {
    User user = (User) getCurrentUser();
    return checkout(shopId, user, promotionType);
  }

  /**
   * 按店铺访问购物车
   */
  @Override
  public List<CartItemVO> checkout(String shopId, User user, PromotionType promotionType) {
    List<CartItemVO> cartItems = new ArrayList<CartItemVO>();
    List<CartItem> list = cartItemMapper.selectByUserIdAndShopId(user.getId(), shopId);
    for (CartItem item : list) {
      String skuId = item.getSkuId();
      // 购物车中没有则创建，默认数量为1，有则为购物车中原有的数量
      CartItemVO itemVO = checkup(user.getId(), skuId, true, false, false, null, null, promotionType);
      cartItems.add(itemVO);
    }
    return cartItems;
  }


  /**
   * 按sku访问购物车
   */
  @Override
  public List<CartItemVO> checkout() {
    IUser user = getCurrentUser();
    List<CartItemVO> cartItems = new ArrayList<CartItemVO>();
    List<CartItem> list = cartItemMapper.selectByUserId(user.getId());
    for (CartItem item : list) {
      String skuId = item.getSkuId();
      CartItemVO itemVO = null;
      try {
        itemVO = checkup(user.getId(), skuId, false, false, false, null, null, null);
        cartItems.add(itemVO);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return cartItems;
  }

  @Override
  public List<CartItemVO> checkout2(String userId) {
    List<CartItemVO> cartItems = new ArrayList<CartItemVO>();

    // 获取用户的购物车
    List<CartItem> list = cartItemMapper.selectByUserId(userId);
    for (CartItem item : list) {
      String skuId = item.getSkuId();
      try {
        CartItemVO itemVO = checkup(userId, skuId, false, false, false, null, null, null);
        cartItems.add(itemVO);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return cartItems;
  }

  /**
   * 商品加入购物车
   */
  @Override
  public CartItem addToCart(String userId, String skuId, int amount, String shopOwnerId,
                            String productId) {
    Long flashSalePId = promotionFlashSaleMapper.selectActivePromotionId(productId);
    PromotionType type = flashSalePId == null ? null : PromotionType.FLASHSALE;
    return checkup(userId, skuId, true, true, true, amount, shopOwnerId, type);
  }

  /**
   * 商品加入购物车
   */
  @Override
  public CartItem addToCart(String userId, String skuId, int amount, String shopOwnerId,
                            String productId, String realShopId) {
    Long flashSalePId = promotionFlashSaleMapper.selectActivePromotionId(productId);
    PromotionType type = flashSalePId == null ? null : PromotionType.FLASHSALE;
    return checkup(userId, skuId, true, true, true, amount,
            shopOwnerId, productId, realShopId, type);
  }

  /**
   * 更新购物车中的商品
   */
  @Override
  public CartItem saveOrUpdateCartItemAmount(String skuId, int amount) {
    return saveOrUpdateCartItemAmount(getCurrentUser().getId(), skuId, amount);
  }

  @Override
  public CartItem saveOrUpdateCartItemAmount(String userId, String skuId,
                                             Integer amount) {
    return checkup(userId, skuId, true, true, false, amount, null, null);
  }

  @Override
  public boolean validate(List<String> skuIds) {
    User user = (User) getCurrentUser();
    for (String skuId : skuIds) {
      CartItemVO itemVO = checkup(user.getId(), skuId, false, false, false, null, null, null);
      if (itemVO == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "购物车项目不存在");
      }
    }
    return true;
  }

  public boolean validate(String shopId) {
    User user = (User) getCurrentUser();
    List<CartItem> list = cartItemMapper.selectByUserIdAndShopId(user.getId(), shopId);
    for (CartItem item : list) {
      checkup(user.getId(), item.getSkuId(), false, false, false, null, null, null);
    }
    return true;
  }

  /**
   * TODO 简化流程 三种使用场景，validate, checkout, add, update
   *
   * @param increment true为增量，false为全量
   * @param promotionType
   */
  private CartItemVO checkup(String userId, String skuId, boolean autoInsert, boolean autoUpdate,
                             boolean increment, Integer amount, String shopOwnerId, PromotionType promotionType) {
    amount = amount == null ? 1 : amount;
    if (amount <= 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品加入购物车中的数量应该为一个正整数");
    }

    // 检查商品sku是否存在
    // TODO 下面的sku的库存判断合一个sql语句
    Sku sku = productService.loadSku(skuId);
    if (sku == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品规格[id=" + skuId + "]不存在， 不能购买");
    }


    // 检查商品状态和库存
    ProductVO product = productService.load(sku.getProductId());

    //分会场商品按店铺购物车下单限购数量校验
    checkStadiumProductBuyLimit(amount, sku, product);

    if (promotionType != null && SKIP_CHECK_PROMOTION_TYPE.contains(promotionType)) {
      log.warn("sku {} 已参加活动 {}, 跳过下架及库存校验", skuId, promotionType);
    } else {
      // 检查商品sku库存
      if (sku.getAmount() <= sku.getSecureAmount()) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "温馨提示：[" + sku.getSpec() + "]商品已下架或售完，请改选其他商品");
      }

      if (product == null || !ProductStatus.ONSALE.equals(product.getStatus())) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "温馨提示：[" + product.getName() + "]商品已下架或售完，请改选其他商品");
      }
    }

    SkuVO skuVO = new SkuVO(sku, product.getMinYundouPrice(), product.getScaleOrUseGlobal());

    // TODO 将来增加的店铺状态检查
    Shop shop = shopService.loadRootShop();

    CartItem item = cartItemMapper.selectByUserIdAndSku(userId, skuId);
    if (item == null) {
      if (autoInsert) {
        item = new CartItem();
        item.setSkuId(skuId);
        item.setProductId(sku.getProductId());
        item.setShopId(shop.getId());
        item.setSellerId(shop.getOwnerId());
        item.setAmount(amount == null ? 1 : amount);
        item.setUserId(userId);
        item.setUnionId(shopOwnerId);
        cartItemMapper.insert(item);
      } else {
        return null;
      }
    } else {
      Integer realAmt = sku.getAmount();
      //折扣促销增加实际库存 要减去子sku的库存总和
      //根据skucode查询该sku的所有子sku的库存之和
      Integer countSubSku = skuMapper.countAmountBuySourceSkuCode(sku.getSkuCode());
      if(null!=countSubSku){
        realAmt=realAmt-countSubSku;
      }

      boolean isCombineSlave = productCombineService.selectIsCombinedSalve(skuId);
      if (isCombineSlave) {
        // 查询已经被组合到套装的sku数量
        Integer combineUsedAmt = productCombineService.sumBindedSlaveAmount(skuId);
        realAmt -= combineUsedAmt;
      }
      if (realAmt <= 0) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "温馨提示：[" + product.getName() + "]库存不足， 不能购买");
      }
      if (autoUpdate) {
        if (increment) {
          if (item.getAmount() + amount <= 0) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品数量不能减少到0或者0以下");
          }

          if (item.getAmount() + amount > realAmt) {
            // 超过sku库存数量，自动纠正购买数量
            item.setAmount(realAmt);
            cartItemMapper.updateByPrimaryKeySelective(item);
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "商品规格[" + sku.getSpec() + "]库存[" + realAmt + "]不足");
          }
          item.setAmount(item.getAmount() + amount);
          cartItemMapper.updateByPrimaryKeySelective(item);
        } else {
          if (amount > realAmt) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "购物车中商品[" + product.getName() + "], 规格[" + sku.getSpec() + "]购买" + amount + "件，库存只有"
                    + realAmt + "件，请调整购物车中的购买数量");
          }
          item.setAmount(amount);
          cartItemMapper.updateByPrimaryKeySelective(item);
        }
      } else {
        if (item.getAmount() > realAmt) {
          // 纠正购买数量为最大的sku库存数量
          item.setAmount(realAmt);
          cartItemMapper.updateByPrimaryKeySelective(item);
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "购物车中商品[" + product.getName() + "], 规格[" + sku.getSpec() + "]有" + item.getAmount()
                  + "件，库存只有" + realAmt + "件，请调整购物车中的购买数量");
        }
      }
    }
    // 校验套装商品库存
    productCombineService.checkAmount(sku);

    sku.setSpec(productService.findSkuSpec(sku, true));
    return new CartItemVO(item, skuVO, product, shop);
  }

  public void checkStadiumProductBuyLimit(Integer amount, Sku sku, ProductVO product) {
    if (sku.getPromotionType() != null) {
      StadiumType stadiumType = null;
      try {
        stadiumType = StadiumType.valueOf(sku.getPromotionType());
      } catch (Exception e) {
        log.error("类型转换失败,promotionType{}", sku.getPromotionType(), e);
      }
      StadiumType[] stadiumTypes = StadiumType.values();
      if (ArrayUtils.contains(stadiumTypes, stadiumType)) {
        Integer buyLimit = product.getBuyLimit();
        if (buyLimit == null || buyLimit == 0) {//限购为空或为0表示不限购
          return;
        }
        List<Integer> buyAmount = orderMapper.selectProductBuyAmount(getCurrentUser().getId(), product.getId());
        if (buyAmount != null && !buyAmount.isEmpty()) {
          IntSummaryStatistics collect = buyAmount.stream().collect(Collectors.summarizingInt(value -> value));
          long sum = collect.getSum();
          if (sum + amount > buyLimit) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, product.getName() + "限购" + buyLimit + "件");
          }
        } else {
          if (amount > buyLimit) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, product.getName() + "限购" + buyLimit + "件");
          }
        }
      }
    }
  }

  /**
   * TODO 简化流程 三种使用场景，validate, checkout, add, update
   *
   * @param increment true为增量，false为全量
   * @param promotionType
   */
  private CartItemVO checkup(String userId, String skuId, boolean autoInsert, boolean autoUpdate,
                             boolean increment, Integer amount, String shopOwnerId, String productId, String realShopId, PromotionType promotionType) {
    amount = amount == null ? 1 : amount;
    if (amount <= 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品加入购物车中的数量应该为一个正整数");
    }

    // 检查商品sku是否存在
    // TODO 下面的sku的库存判断合一个sql语句
    Sku sku = productService.loadSku(skuId);
    if (sku == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品规格[id=" + skuId + "]不存在， 不能购买");
    }

    ProductVO product = productService.load(sku.getProductId());

    //分会场商品购物车下单限购数量校验
    checkStadiumProductBuyLimit(amount, sku, product);

    if (promotionType != null && SKIP_CHECK_PROMOTION_TYPE.contains(promotionType)) {
      log.warn("sku {} 已参加活动 {}, 跳过下架及库存校验", skuId, promotionType);
    } else {
      // 检查商品sku库存
      if (sku.getAmount() <= sku.getSecureAmount() && !ProductSource.SF.name()
              .equals(sku.getSkuCodeResources())) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "温馨提示：[" + product.getName() + "]商品已经下架或已售罄，请改选其他商品");
      }

      // 检查商品状态和库存
      if (product == null || !ProductStatus.ONSALE.equals(product.getStatus())) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "温馨提示：[" + product.getName() + "]商品已下架或已售罄，请改选其他商品");
      }
    }

    SkuVO skuVO = new SkuVO(sku, product.getMinYundouPrice(), product.getScaleOrUseGlobal());

    // TODO 将来增加的店铺状态检查
    Shop shop = shopService.loadRootShop();

    CartItem item = cartItemMapper.selectByUserIdAndSkuShopId(userId, skuId, shop.getId());
    if (item == null) {
      if (autoInsert) {
        item = new CartItem();
        item.setSkuId(skuId);
        item.setProductId(sku.getProductId());
        item.setShopId(shop.getId());
        item.setSellerId(shop.getOwnerId());
        item.setAmount(amount == null ? 1 : amount);
        item.setUserId(userId);
        item.setUnionId(shopOwnerId);
        cartItemMapper.insert(item);
      } else {
        return null;
      }
    } else if (promotionType != null && SKIP_CHECK_PROMOTION_TYPE.contains(promotionType)) {
      log.warn("sku {} 已参加活动 {}, 跳过下架及库存校验", skuId, promotionType);
    }
    else {
      Integer realAmt = sku.getAmount();
      //折扣促销增加实际库存 要减去子sku的库存总和
      //根据skucode查询该sku的所有子sku的库存之和
      Integer countSubSku = skuMapper.countAmountBuySourceSkuCode(sku.getSkuCode());
      if(null!=countSubSku){
        realAmt=realAmt-countSubSku;
      }
      boolean isCombineSlave = productCombineService.selectIsCombinedSalve(skuId);
      if (isCombineSlave) {
        // 查询已经被组合到套装的sku数量
        Integer combineUsedAmt = productCombineService.sumBindedSlaveAmount(skuId);
        realAmt -= combineUsedAmt;
      }

      if (realAmt <= 0) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "温馨提示：[" + product.getName() + "]库存[" + realAmt + "]不足");
      }

      if (autoUpdate) {
        if (increment) {
          if (item.getAmount() + amount <= 0) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品数量不能减少到0或者0以下");
          }

          if (item.getAmount() + amount > realAmt) {
            // 超过sku库存数量，自动纠正购买数量
            item.setAmount(realAmt);
            cartItemMapper.updateByPrimaryKeySelective(item);
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "商品规格[" + sku.getSpec() + "]库存[" + realAmt + "]不足");
          }
          item.setAmount(item.getAmount() + amount);
          cartItemMapper.updateByPrimaryKeySelective(item);
        } else {
          if (amount > realAmt) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "购物车中商品[" + product.getName() + "], 规格[" + sku.getSpec() + "]购买" + amount + "件，库存只有"
                    + realAmt + "件，请调整购物车中的购买数量");
          }
          item.setAmount(amount);
          cartItemMapper.updateByPrimaryKeySelective(item);
        }
      } else {
        if (item.getAmount() > realAmt) {
          // 纠正购买数量为最大的sku库存数量
          item.setAmount(realAmt);
          cartItemMapper.updateByPrimaryKeySelective(item);
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              "购物车中商品[" + product.getName() + "], 规格[" + sku.getSpec() + "]有" + item.getAmount()
                  + "件，库存只有" + realAmt + "件，请调整购物车中的购买数量");
        }
      }
    }

    // 校验套装库存
    productCombineService.checkAmount(sku);
    sku.setSpec(productService.findSkuSpec(sku, true));
    return new CartItemVO(item, skuVO, product, shop);
  }

  /**
   * 立即下单，不走购物车流程
   *
   * @param qty 购买数量
   */
  private CartItemVO checkup(String userId, String skuId, Integer qty) {
    qty = qty == null ? 1 : qty;
    if (qty <= 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品加入购物车中的数量应该为一个正整数");
    }

    // 检查商品sku是否存在
    // TODO 下面的sku的库存判断合一个sql语句
    Sku sku = productService.loadSku(skuId);
    if (sku == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品规格[id=" + skuId + "]不存在， 不能购买");
    }

    // 检查商品sku库存
    if (sku.getAmount() <= 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "商品规格[" + sku.getSpec() + "]商品已下架或已售罄，请改选其他商品");
    }
    // 超过sku库存数量
    if (qty > sku.getAmount()) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "商品规格[" + sku.getSpec() + "]库存[" + sku.getAmount() + "]不足");
    }

    // 检查商品状态和库存
    ProductVO product = productService.load(sku.getProductId());
    if (product == null || !ProductStatus.ONSALE.equals(product.getStatus())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "温馨提示：[" + product.getName() + "]商品已下架或已售罄，请改选其他商品");
    }

    SkuVO skuVO = new SkuVO(sku, product.getMinYundouPrice(), product.getScaleOrUseGlobal());

    // TODO 将来增加的店铺状态检查
    Shop shop = shopService.loadRootShop();

    CartItem item = new CartItem();
    item.setSkuId(skuId);
    item.setProductId(sku.getProductId());
    item.setShopId(shop.getId());
    item.setSellerId(shop.getOwnerId());
    item.setAmount(qty);
    item.setUserId(userId);

    sku.setSpec(productService.findSkuSpec(sku, true));
    return new CartItemVO(item, skuVO, product, shop);
  }

  /**
   * 立即下单，不走购物车流程
   *
   * @param qty 购买数量
   * @param promotionType
   */
  private CartItemVO checkup(String userId, String skuId, Integer qty, String realShopId, PromotionType promotionType) {
    qty = qty == null ? 1 : qty;
    if (qty <= 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品加入购物车中的数量应该为一个正整数");
    }

    // 检查商品sku是否存在
    // TODO 下面的sku的库存判断合一个sql语句
    Sku sku = productService.loadSku(skuId);

    if (sku == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品规格[id=" + skuId + "]不存在， 不能购买");
    }

    Integer realAmt = sku.getAmount();
    //折扣促销增加实际库存 要减去子sku的库存总和
    //根据skucode查询该sku的所有子sku的库存之和
    Integer countSubSku = skuMapper.countAmountBuySourceSkuCode(sku.getSkuCode());
    if(null!=countSubSku){
      realAmt=realAmt-countSubSku;
    }

    boolean isCombineSlave = productCombineService.selectIsCombinedSalve(skuId);
    ProductVO product = productService.load(sku.getProductId());

    //分会场商品直接购买下单限购数量校验
    checkStadiumProductBuyLimit(qty, sku, product);

    if (isCombineSlave) {
      // 查询已经被组合到套装的sku数量
      Integer combineUsedAmt = productCombineService.sumBindedSlaveAmount(skuId);
      realAmt -= combineUsedAmt;
    }

    if (promotionType != null && SKIP_CHECK_PROMOTION_TYPE.contains(promotionType)) {
      log.warn("sku: ${} 参加了活动 {}，跳过库存及状态校验", skuId, promotionType);
    } else {
      if (realAmt <= 0) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "温馨提示：[" + product.getName() + "]库存不足， 不能购买");
      }

      if (realAmt <= sku.getSecureAmount() && !ProductSource.SF.name()
              .equals(sku.getSkuCodeResources())) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "温馨提示：[" + product.getName() + "]已经下架， 不能购买");
      }
      // 超过sku库存数量
      if (qty > realAmt) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "温馨提示：[" + product.getName() + "]库存[" + realAmt + "]不足");
      }

      if (realAmt <= sku.getSecureAmount()) {//安全库存校验
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "温馨提示：[" + product.getName() + "]库存[" + realAmt + "]不足");
      }

      // 检查商品状态和库存
      if (product == null || !ProductStatus.ONSALE.equals(product.getStatus())) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "温馨提示：[" + product.getName() + "]商品已下架或售完，请改选其他商品");
      }
    }

    // 校验套装库存
    productCombineService.checkAmount(sku);

    // TODO 将来增加的店铺状态检查
    Shop shop = shopService.loadRootShop();
    CartItem item = new CartItem();
    item.setSkuId(skuId);
    item.setProductId(sku.getProductId());
    item.setShopId(shop.getId());
    item.setSellerId(shop.getOwnerId());
    item.setAmount(qty);
    item.setUserId(userId);

    productCombineService.checkAmount(sku);

    //sku.setSpec(productService.findSkuSpec(sku, true));
    return new CartItemVO(item, sku, product, shop);
  }

  @Override
  public List<CartItemVO> listCartItems(User user) {
    List<CartItemVO> result = new ArrayList<CartItemVO>();
    List<CartItem> list = cartItemMapper.selectByUserId(user.getId());
    for (CartItem item : list) {
      Sku sku = productService.loadSku(item.getSkuId());
      if (sku == null) {
        continue;
      }
      ProductVO product = productService.load(sku.getProductId());
      if (product == null) {
        continue;
      }
      Shop shop = shopService.load(product.getShopId());
      if (shop == null) {
        continue;
      }
      result.add(new CartItemVO(item, sku, product, shop));
    }
    return result;
  }

  @Override
  public List<CartItemVO> listCartItems() {
    User user = (User) getCurrentUser();
    return listCartItems(user);
  }

  @Override
  public List<CartItemVO> listCartItems(Set<String> skuIds) {
    User user = (User) getCurrentUser();
    List<CartItemVO> result = new ArrayList<CartItemVO>();
    for (String skuId : skuIds) {
      CartItem cartItem = cartItemMapper.selectByUserIdAndSku(user.getId(), skuId);
      if (cartItem == null) {
        continue;
      }
      Sku sku = productService.loadSku(skuId);
      if (sku == null) {
        continue;
      }
      ProductVO product = productService.load(sku.getProductId());
      if (product == null) {
        continue;
      }
      Shop shop = shopService.load(product.getShopId());
      if (shop == null) {
        continue;
      }
      result.add(new CartItemVO(cartItem, sku, product, shop));
    }
    return result;
  }

  @Override
  public List<CartItemVO> listCartItems(String shopId) {
    User user = (User) getCurrentUser();
    List<CartItemVO> result = new ArrayList<CartItemVO>();
    List<CartItem> list = cartItemMapper.selectByUserIdAndShopId(user.getId(), shopId);
    for (CartItem item : list) {
      Sku sku = productService.loadSku(item.getSkuId());
      if (sku == null) {
        continue;
      }
      ProductVO product = productService.load(sku.getProductId());
      if (product == null) {
        continue;
      }
      //Shop shop = shopService.load(product.getShopId());
      Shop shop = shopService.load(shopId);
      if (shop == null) {
        continue;
      }
      result.add(new CartItemVO(item, sku, product, shop));
    }
    return result;
  }

  @Override
  public boolean remove(String id) {
    if (cartItemMapper.deleteByPrimaryKey(id) == 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该购物车项目[" + id + "]已经不存在");
    }

    return true;
  }

  @Override
  public boolean remove(String id, String userId) {
    int ok = cartItemMapper.deleteByUserIdAndId(id, userId);
    if (ok == 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该购物车项目[" + id + "]已经不存在");
    }

    return true;
  }


  @Override
  public boolean removeBySku(String skuId) {
    User user = (User) getCurrentUser();
    if (cartItemMapper.deleteByUserIdAndSkuId(user.getId(), skuId) == 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该购物车项目[skuId:" + skuId + "]已经不存在");
    }
    log.info("delete sku[" + skuId + "] in user[" + user.getId() + "] cart");
    return true;
  }

  @Override
  public void clear(List<String> skuIds) {
    User user = (User) getCurrentUser();
    for (String skuId : skuIds) {
      cartItemMapper.deleteByUserIdAndSkuId(user.getId(), skuId);
      log.info("delete sku[" + skuId + "] in user[" + user.getId() + "] cart");
    }
  }

  @Override
  public void clear(String shopId) {
    User user = (User) getCurrentUser();
    cartItemMapper.deleteByUserIdAndShopId(user.getId(), shopId);
    log.info("delete skus by user[" + user.getId() + "] in shop[" + shopId + "]");
  }

  @Override
  public void clear() {
    User user = (User) getCurrentUser();
    cartItemMapper.deleteByUserId(user.getId());
  }

  @Override
  public Integer count() {
    IUser user = null;
    try {
      user = getCurrentUser();
    } catch (Exception e) {
      log.error("", e);
    }
    if (user != null) {
      return cartItemMapper.selectCount(user.getId());
    } else {
      return 0;
    }
  }

  @Override
  public boolean updateAmount(String id, int amount) {
    User user = (User) getCurrentUser();
    return updateAmount(id, amount, user);
  }

  @Override
  public boolean updateAmount(String id, int amount, User user) {
    CartItem cartItem = cartItemMapper.selectByPrimaryKey(id);
    if (cartItem == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "购物车中该商品已经删除或者购买，请刷新页面重新操作");
    }
    return checkup(user.getId(), cartItem.getSkuId(), false, true, false, amount, null, null)
        != null;
  }


  @Override
  public Integer count(String userId) {
    return cartItemMapper.selectCount(userId);
  }

  @Override
  public List<CartItem> listByUserId(String userId) {
    return cartItemMapper.selectByUserId(userId);
  }

  @Override
  public Integer countByShopId(String userId, String shopId) {
    return cartItemMapper.countByShopId(userId, shopId);
  }

  @Override
  public PromotionType getCartPromotionType() {
    List<CartItemVO> cartItemVOList = checkout();
    return getCartPromotionType(cartItemVOList);
  }

  @Override
  public PromotionType getCartPromotionType(List<CartItemVO> cartItems) {
    PromotionType promotionType = PromotionType.NORMAL;
    for (CartItemVO cartItemVO : cartItems) {
      //1.先找大会
      List<PromotionSkuVo> promotionSkuVos = promotionSkusService.
              getPromotionSkus(cartItemVO.getSku().getSkuCode());
      if (CollectionUtils.isNotEmpty(promotionSkuVos)) {
          return PromotionType.MEETING;
      }
      //2.再找秒杀,一元专区
      FlashSalePromotionProductVO promotionProductVO = (FlashSalePromotionProductVO) flashSaleService
              .loadPromotionProductByProductId(cartItemVO.getProductId());
      if (promotionProductVO != null) {
        return promotionProductVO.getPromotionType();
      }
      //3.再找新人专区
      Integer freshmanProduct = firstOrderMapper.countFreshmanProduct(IdTypeHandler.decode(cartItemVO.getProductId()));
      if(freshmanProduct > 0){
        return PromotionType.FRESHMAN;
      }
      //4.再找折扣促销
      List<Promotion4ProductSalesVO> promotion4ProductSalesVOS = promotionBaseInfoService.selectExsitSalesPromotionByProductId(cartItemVO.getProductId());
      if (CollectionUtils.isNotEmpty(promotion4ProductSalesVOS)) {
        return PromotionType.PRODUCT_SALES;
      }
    }
    return promotionType;
  }
}
