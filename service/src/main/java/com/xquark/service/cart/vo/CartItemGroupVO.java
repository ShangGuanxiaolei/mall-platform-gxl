package com.xquark.service.cart.vo;

import java.util.List;

import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;

public class CartItemGroupVO {

  private Shop shop;

  private User seller;

  private List<CartItemVO> cartItems;

  public Shop getShop() {
    return shop;
  }

  public void setShop(Shop shop) {
    this.shop = shop;
  }

  public List<CartItemVO> getCartItems() {
    return cartItems;
  }

  public void setCartItems(List<CartItemVO> cartItems) {
    this.cartItems = cartItems;
  }

  public User getSeller() {
    return seller;
  }

  public void setSeller(User seller) {
    this.seller = seller;
  }

}
