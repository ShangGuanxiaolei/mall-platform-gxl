package com.xquark.service.cart.vo;

import java.util.List;

import com.xquark.service.pricing.vo.PricingResultVO;

public class CartVO {

  private List<CartItemVO> cartItems;

  private PricingResultVO prices;

  public CartVO() {
  }

  public CartVO(List<CartItemVO> cartItems, PricingResultVO prices) {
    this.cartItems = cartItems;
    this.prices = prices;
  }

  public List<CartItemVO> getCartItems() {
    return cartItems;
  }

  public void setCartItems(List<CartItemVO> cartItems) {
    this.cartItems = cartItems;
  }

  public PricingResultVO getPrices() {
    return prices;
  }

  public void setPrices(PricingResultVO prices) {
    this.prices = prices;
  }

}
