package com.xquark.service.cart.vo;

import com.xquark.dal.model.CartItem;
import com.xquark.dal.model.DynamicPricing;
import com.xquark.dal.model.DynamicPricingItem;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.User;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.product.vo.SkuVO;
import java.math.BigDecimal;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;

public class CartItemVO extends CartItem implements DynamicPricingItem {

  private static final long serialVersionUID = 4234562439721295804L;

  private ProductVO product;

  private Sku sku;

  private User user;

  private Shop shop;

  private CartPromotionInfo promotionInfo;

  //是否新人专区商品
  private boolean freshmanProduct;

  //是否促销折扣商品
  private boolean saleProduct;
  //折扣促销商品角标
  private String cornerTab;

  public String getCornerTab() {
    return cornerTab;
  }

  public void setCornerTab(String cornerTab) {
    this.cornerTab = cornerTab;
  }

  public boolean isSaleProduct() {
    return saleProduct;
  }

  public void setSaleProduct(boolean saleProduct) {
    this.saleProduct = saleProduct;
  }

  public boolean isFreshmanProduct() {
    return freshmanProduct;
  }

  public void setFreshmanProduct(boolean freshmanProduct) {
    this.freshmanProduct = freshmanProduct;
  }

  public CartItemVO() {
    super();
  }

  public CartItemVO(CartItem item, Sku sku, ProductVO product, Shop shop) {
    BeanUtils.copyProperties(item, this);
    this.sku = new SkuVO(sku, product.getMinYundouPrice(), product.getScaleOrUseGlobal());
    this.product = product;
    this.shop = shop;
  }

  public ProductVO getProduct() {
    return product;
  }

  public void setProduct(ProductVO product) {
    this.product = product;
  }

  public Sku getSku() {
    return sku;
  }

  public void setSku(Sku sku) {
    this.sku = sku;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getTitle() {
    if (product == null) {
      return "";
    }
    return  product.getName();
  }

  public Shop getShop() {
    return shop;
  }

  public void setShop(Shop shop) {
    this.shop = shop;
  }

  // 是否特价商品
  public Boolean getProductSP() {
    return product.getProductSP();
  }

  public CartPromotionInfo getPromotionInfo() {
    return promotionInfo;
  }

  public void setPromotionInfo(CartPromotionInfo promotionInfo) {
    this.promotionInfo = promotionInfo;
  }

  @Override
  public DynamicPricing getDynamicPricing() {
    return sku;
  }

  @Override
  public void setPrice(BigDecimal price) {
    // 没有价格字段
  }
}
