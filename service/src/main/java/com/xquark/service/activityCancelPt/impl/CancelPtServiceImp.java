package com.xquark.service.activityCancelPt.impl;

import com.xquark.dal.mapper.ActivityPtOutTImeMapper;
import com.xquark.dal.mapper.MainOrderMapper;
import com.xquark.dal.model.MainOrder;
import com.xquark.dal.status.PieceStatus;
import com.xquark.service.activityCancelPt.CancelPtService;
import com.xquark.service.activityPtOutTime.PtOrderCancelService;
import com.xquark.service.pricing.impl.pricing.impl.PgPromotionServiceAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Supplier;

/**
 * @author liuyandong
 * @date 2018/9/12 16:00
 */
@Service
public class CancelPtServiceImp implements CancelPtService {

  private final static Logger LOGGER = LoggerFactory.getLogger(CancelPtServiceImp.class);

  @Autowired private PtOrderCancelService ptOrderCancelService;
  @Autowired private PgPromotionServiceAdapter pgPromotionServiceAdapter;
  @Autowired private ActivityPtOutTImeMapper activityPtOutTImeMapper;
  @Autowired private MainOrderMapper mainOrderMapper;


  @Override
  public void cancelPt(String pCode, String tranCode) {
    cancelPtInner(() -> activityPtOutTImeMapper.selectOrderId(tranCode, pCode));
    // 库存不足/取消拼团  拼团失效取消拼团 FIXME 退款完成，修改团的状态
    try {
      pgPromotionServiceAdapter.batchUpdateTranAndMember(
          tranCode, PieceStatus.EXPIRE_CANCELED, PieceStatus.OPENED);
    } catch (Exception e) {
      LOGGER.error("拼团订单批量更新成员状态错误");
    }
  }

  @Override
  public void cancelPtDetail(String pCode, String detailCode) {
    cancelPtInner(() -> activityPtOutTImeMapper.selectOrderIdByDetailCode(detailCode, pCode));
    try {
      pgPromotionServiceAdapter.updateDetailMember(
          detailCode, PieceStatus.EXPIRE_CANCELED);
    } catch (Exception e) {
      LOGGER.error("拼团订单批量更新成员状态错误");
    }
  }

  private void cancelPtInner(Supplier<List<String>> orderSupp) {
    final List<String> list = orderSupp.get();
    // 查询拼团编码对应的订单头编号
    // 由头编号查id
    // 由头编号id查订单id
    if (list != null && list.size() > 0) {
      for (String orderNo : list) {
        try {
          MainOrder mainOrder = mainOrderMapper.selectByOrderNo(orderNo);
          if (mainOrder.getPaidAt() != null) { // 主订单支付时间不为空才去退款，防止回调异常
            // 取消订单，退款
            ptOrderCancelService.orderCancel(orderNo);
          }
        } catch (Exception e) {
          LOGGER.error("拼团订单 {} 执行退款操作失败", orderNo, e);
        }
      }
    }
  }








}
