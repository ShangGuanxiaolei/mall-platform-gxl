package com.xquark.service.activityCancelPt;

/**
 * @author liuyandong
 * @date 2018/9/12 15:37
 */
public interface CancelPtService {
    void cancelPt(String pCode,String groupCode);

    /**
     * 根据detailCode部分退款
     */
    void cancelPtDetail(String pCode, String detailCode);
}
