package com.xquark.service.carousel.impl;

import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.status.CarouselType;
import com.xquark.dal.type.TargetType;
import com.xquark.service.carousel.CarouselService;
import com.xquark.service.common.BaseServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("carouselService")
public class CarouselServiceImpl extends BaseServiceImpl implements CarouselService {

  @Autowired
  CarouselMapper carouselMapper;

  @Autowired
  CarouselImageMapper carouselImageMapper;

  @Autowired
  ProductMapper productMapper;

  @Autowired
  private IndexBannerMapper indexBannerMapper;

  @Autowired
  private CategoryMapper categoryMapper;

  private static final long PRODUCT_URI="hanwei://product/".length();
  private static final long CATEGORY_URI="hanwei://category/".length();

  @Override
  public int insert(Carousel carousel) {
    return carouselMapper.insert(carousel);
  }

  @Override
  public int insertOrder(Carousel carousel) {
    return carouselMapper.insert(carousel);
  }

  @Override
  public Carousel load(String id) {
    return carouselMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return carouselMapper.updateForArchive(id);
  }

  @Override
  public int update(Carousel record) {
    return carouselMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<Carousel> list(Pageable pager, Map<String, Object> params) {
    return carouselMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return carouselMapper.selectCnt(params);
  }

  /**
   * 查询是否已经有该轮播图类型的设置
   */
  @Override
  public Long selectByType(String shopId, String productId) {
    return carouselMapper.selectByType(shopId, productId);
  }

  /**
   * 供客户端调用，获取首页轮播图
   */
  @Override
  public Carousel getHomeForApp() {
    Carousel carousel = new Carousel();
    List<IndexBanner> indexBanners = indexBannerMapper.selectAppIndexBanners();
    if (CollectionUtils.isNotEmpty(indexBanners)) {
      ArrayList<CarouselImage> valueImages = new ArrayList<>();
      for (IndexBanner indexBanner : indexBanners) {
        indexBanner.setDropStatus(2);
        carousel.setTitle(indexBanner.getTitle());
        carousel.setShopId(String.valueOf(6102910));
        carousel.setDescription(indexBanner.getDescription());
        carousel.setArchive(indexBanner.getArchive());
        carousel.setType(CarouselType.HOME);
        carousel.setCreatedAt(indexBanner.getCreatedAt());
        carousel.setUpdatedAt(indexBanner.getUpdatedAt());

        CarouselImage carouselImage = new CarouselImage();
        carouselImage.setCarouselId(String.valueOf(1));
        carouselImage.setId(indexBanner.getId());
        carouselImage.setTitle(indexBanner.getTitle());
        carouselImage.setDescription(indexBanner.getDescription());
        carouselImage.setIdx(indexBanner.getSequence());
        carouselImage.setImg(indexBanner.getImgUrl());
        carouselImage.setImgUrl(indexBanner.getImgUrl());
        carouselImage.setTargetUrl(indexBanner.getJumpTypeLink());
        carouselImage.setCreatedAt(indexBanner.getCreatedAt());
        carouselImage.setUpdatedAt(indexBanner.getUpdatedAt());

        switch (indexBanner.getJumpType()) {
          case PRODUCT:
            String productCode = indexBanner.getJumpTypeLink().substring((int) PRODUCT_URI);
            if (StringUtils.isNotEmpty(productCode)) {
              Product product = productMapper.selectByPrimaryKey(productCode);
              carouselImage.setTargetName(product.getName());
              carouselImage.setTarget(productCode);
              carouselImage.setTargetType(TargetType.PRODUCT);
            }
            break;
          case SECOND_CATEGORY:
            String categoryId = indexBanner.getJumpTypeLink().substring((int) CATEGORY_URI);
            if (StringUtils.isNotBlank(categoryId)) {
              Category category = categoryMapper.loadParent(categoryId);
              if (null != category) {
                carouselImage.setTargetName(category.getName());
                carouselImage.setTarget(categoryId);
                carouselImage.setTargetType(TargetType.SECOND_CATEGORY);
              }
            }
            break;
          default:
            carouselImage.setTarget(indexBanner.getJumpTypeLink());
            carouselImage.setTargetType(TargetType.CUSTOMIZED);
            break;
        }
        valueImages.add(carouselImage);
      }

      carousel.setImages(valueImages);
    }
    return carousel;
  }

  private Carousel getCarouselsLocal() {
    Carousel carousel = carouselMapper.getHomeForApp();
    if (carousel != null) {
      List<CarouselImage> images = carouselImageMapper.getByCarouselId(carousel.getId());
      ArrayList valueImages = new ArrayList();
      // 已经删除的商品不在轮播图中显示
      for (CarouselImage image : images) {
        TargetType targetType = image.getTargetType();
        String target = image.getTarget();
        if (targetType == TargetType.PRODUCT) {
          Product product = productMapper.selectByPrimaryKey(target);
          if (product != null) {
            valueImages.add(image);
          }
        } else {
          valueImages.add(image);
        }
      }
      carousel.setImages(images);
    }
    return carousel;
  }

  /**
   * 供客户端调用，获取首页优惠券图片信息
   */
  @Override
  public Carousel getCouponForApp() {
    Carousel carousel = carouselMapper.getCouponForApp();
    if (carousel != null) {
      List<CarouselImage> images = carouselImageMapper.getByCarouselId(carousel.getId());
      carousel.setImages(images);
    }
    return carousel;
  }

  /**
   * 供客户端调用，获取首页优惠券图片信息
   */
  @Override
  public Carousel getHomeActivityForApp() {
    Carousel carousel = carouselMapper.getHomeActivityForApp();
    if (carousel != null) {
      List<CarouselImage> images = carouselImageMapper.getByCarouselId(carousel.getId());
      carousel.setImages(images);
    }
    return carousel;
  }

}
