package com.xquark.service.carousel;

import com.xquark.dal.model.Carousel;
import com.xquark.dal.model.CarouselImage;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface CarouselImageService extends BaseEntityService<CarouselImage> {

  CarouselImage load(String id);

  int insert(CarouselImage carousel);

  int deleteForArchive(String id);

  int update(CarouselImage record);

  /**
   * 服务端分页查询数据
   */
  List<CarouselImage> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 查询某种类型的优惠券的明细个数
   */
  Long countByCarouselId(String carouselId);

}

