package com.xquark.service.carousel.impl;

import com.xquark.dal.mapper.CarouselImageMapper;
import com.xquark.dal.mapper.CarouselMapper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.model.Carousel;
import com.xquark.dal.model.CarouselImage;
import com.xquark.dal.model.Product;
import com.xquark.dal.type.TargetType;
import com.xquark.service.carousel.CarouselImageService;
import com.xquark.service.carousel.CarouselService;
import com.xquark.service.common.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("carouselImageService")
public class CarouselImageServiceImpl extends BaseServiceImpl implements CarouselImageService {

  @Autowired
  CarouselImageMapper carouselImageMapper;

  @Autowired
  ProductMapper productMapper;

  @Override
  public int insert(CarouselImage carousel) {
    return carouselImageMapper.insert(carousel);
  }

  @Override
  public int insertOrder(CarouselImage carouselImage) {
    return carouselImageMapper.insert(carouselImage);
  }

  @Override
  public CarouselImage load(String id) {
    return carouselImageMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return carouselImageMapper.updateForArchive(id);
  }

  @Override
  public int update(CarouselImage record) {
    return carouselImageMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<CarouselImage> list(Pageable pager, Map<String, Object> params) {
    List<CarouselImage> images = carouselImageMapper.list(pager, params);
    for (CarouselImage image : images) {
      TargetType targetType = image.getTargetType();
      String target = image.getTarget();
      if (targetType == TargetType.PRODUCT) {
        Product product = productMapper.selectByPrimaryKey(target);
        if (product != null) {
          image.setTargetName(product.getName());
        }
      }
    }
    return images;
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return carouselImageMapper.selectCnt(params);
  }


  /**
   * 查询某种类型的优惠券的明细个数
   */
  @Override
  public Long countByCarouselId(String carouselId) {
    return carouselImageMapper.countByCarouselId(carouselId);
  }

}
