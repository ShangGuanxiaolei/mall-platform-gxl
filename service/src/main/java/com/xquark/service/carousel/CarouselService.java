package com.xquark.service.carousel;

import com.xquark.dal.model.Carousel;
import com.xquark.service.BaseEntityService;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;


public interface CarouselService extends BaseEntityService<Carousel> {

  Carousel load(String id);

  int insert(Carousel carousel);

  int deleteForArchive(String id);

  int update(Carousel record);

  /**
   * 服务端分页查询数据
   */
  List<Carousel> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 查询是否已经有该轮播图类型的设置
   */
  Long selectByType(String shopId, String productId);

  /**
   * 供客户端调用，获取首页轮播图
   */
  Carousel getHomeForApp();

  /**
   * 供客户端调用，获取首页优惠券图片信息
   */
  Carousel getCouponForApp();

  /**
   * 供客户端调用，获取首页优惠券图片信息
   */
  Carousel getHomeActivityForApp();

}

