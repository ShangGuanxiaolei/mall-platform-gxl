package com.xquark.service.auditRule.impl;

import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.AuditRuleType;
import com.xquark.dal.status.AuditType;
import com.xquark.service.auditRule.AuditRuleService;
import com.xquark.service.common.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.*;

/**
 * 经销商订单审核规则Service
 *
 * @author chh 2016-12-23
 */
@Service("auditRuleService")
public class AuditRuleServiceImpl extends BaseServiceImpl implements AuditRuleService {

  @Autowired
  private AuditRuleMapper auditRuleMapper;

  @Override
  public AuditRule selectByPrimaryKey(String id) {
    return auditRuleMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(AuditRule role) {
    return auditRuleMapper.insert(role);
  }

  @Override
  public int modify(AuditRule role) {
    return auditRuleMapper.modify(role);
  }

  @Override
  public int delete(String id) {
    return auditRuleMapper.delete(id);
  }

  /**
   * 列表查询页面
   */
  @Override
  public List<AuditRule> list(Pageable page, String keyword, String type) {
    return auditRuleMapper.list(page, keyword, type);
  }

  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long selectCnt(String keyword, String type) {
    return auditRuleMapper.selectCnt(keyword, type);
  }

  /**
   * 根据订单id查询该订单的审核类型
   */
  @Override
  public AuditRule selectByOrderId(String orderId) {
    AuditRule rule = auditRuleMapper.selectByOrderId(orderId);
    // 没有设置规则，则默认前后端均可审核
    if (rule == null) {
      rule = new AuditRule();
      rule.setAuditType(AuditType.ALL);
    }
    return rule;
  }

  /**
   * 判断根据买家，卖家类型查询是否已经存在记录
   */
  @Override
  public AuditRule selectByBuyerAndSeller(AgentType buyer, AgentType seller, String id,
      AuditRuleType type) {
    return auditRuleMapper.selectByBuyerAndSeller(buyer, seller, id, type);
  }

  /**
   * 代理审核时根据申请类型与上级类型查找审核规则
   */
  @Override
  public AuditRule selectByAgentType(AgentType type, AgentType parentType) {
    return auditRuleMapper.selectByAgentType(type, parentType);
  }

}
