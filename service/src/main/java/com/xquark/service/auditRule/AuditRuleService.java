package com.xquark.service.auditRule;

import com.xquark.dal.model.AuditRule;
import com.xquark.dal.model.CommissionRule;
import com.xquark.dal.model.Order;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.AuditRuleType;
import com.xquark.dal.status.AuditType;
import org.springframework.data.domain.Pageable;

import java.util.List;


/**
 * 经销商订单审核规则Service
 *
 * @author chh 2016-12-23
 */
public interface AuditRuleService {


  AuditRule selectByPrimaryKey(String id);

  int insert(AuditRule role);

  int modify(AuditRule role);

  int delete(String id);

  /**
   * 列表查询页面
   */
  List<AuditRule> list(Pageable page, String keyword, String type);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(String keyword, String type);

  /**
   * 根据订单id查询该订单的审核类型
   */
  AuditRule selectByOrderId(String orderId);

  /**
   * 判断根据买家，卖家类型查询是否已经存在记录
   */
  AuditRule selectByBuyerAndSeller(AgentType buyer, AgentType seller, String id,
      AuditRuleType type);

  /**
   * 代理审核时根据申请类型与上级类型查找审核规则
   */
  AuditRule selectByAgentType(AgentType type, AgentType parentType);

}
