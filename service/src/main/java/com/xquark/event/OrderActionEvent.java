package com.xquark.event;

import org.springframework.context.ApplicationEvent;

import com.xquark.dal.type.OrderActionType;

/**
 * @author ahlon
 */
public class OrderActionEvent extends ApplicationEvent {

  private static final long serialVersionUID = 8945352745268513303L;

  private OrderActionType type;

  public static int firstStep = 1;
  public static int secondStep = 2;
  public static int thirdStep = 3;

  public OrderActionEvent(OrderActionType type, Object source) {
    super(source);
    this.type = type;
  }

  public OrderActionType getType() {
    return type;
  }

  public void setType(OrderActionType type) {
    this.type = type;
  }

}
