package com.xquark.event;

import org.springframework.context.ApplicationEvent;

import com.xquark.service.sync.SyncEvent;

/**
 * @author stone
 */
public class SyncActionEvent extends ApplicationEvent {

  private static final long serialVersionUID = -8337145888983796236L;
  private SyncEvent type;

  public SyncActionEvent(SyncEvent type, Object source) {
    super(source);
    this.type = type;
  }

  public SyncEvent getType() {
    return type;
  }

  public void setType(SyncEvent type) {
    this.type = type;
  }

}
