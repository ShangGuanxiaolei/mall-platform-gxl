package com.xquark.event;

import org.springframework.context.ApplicationEvent;

import com.xquark.dal.type.PushMessageDeviceType;

public class MessageNotifyEvent extends ApplicationEvent {

  private static final long serialVersionUID = 7056585744944691134L;

  private String baiduTag;
  private String userId;
  private String url;
  private Integer type;
  private String relatId;
  private PushMessageDeviceType plantForm;

  public MessageNotifyEvent(Object message, String userId, String url,
      Integer type, String relatId) {
    super(message);
    this.userId = userId;
    this.url = url;
    this.type = type;
    this.relatId = relatId;
    this.plantForm = PushMessageDeviceType.ALL;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getBaiduTag() {
    return baiduTag;
  }

  public void setBaiduTag(String baiduTag) {
    this.baiduTag = baiduTag;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public String getRelatId() {
    return relatId;
  }

  public void setRelatId(String relatId) {
    this.relatId = relatId;
  }

  public PushMessageDeviceType getPlantForm() {
    return plantForm;
  }

  public void setPlantForm(PushMessageDeviceType plantForm) {
    this.plantForm = plantForm;
  }

}
