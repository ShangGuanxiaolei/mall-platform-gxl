package com.xquark.event;

import org.springframework.context.ApplicationEvent;

public class XQMessageNotifyEvent extends ApplicationEvent {

  private static final long serialVersionUID = 6406847943094420136L;

  private String baiduTag;
  private Integer type;
  private String relatId;
  private String url;
  private Object extDate;

  public XQMessageNotifyEvent(Object message, String baiduTag, String url, Integer type,
      String relatId) {
    super(message);
    this.baiduTag = baiduTag;
    this.type = type;
    this.url = url;
    this.relatId = relatId;
    this.extDate = null;
  }

  public String getBaiduTag() {
    return baiduTag;
  }

  public void setBaiduTag(String baiduTag) {
    this.baiduTag = baiduTag;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public String getRelatId() {
    return relatId;
  }

  public void setRelatId(String relatId) {
    this.relatId = relatId;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Object getExtDate() {
    return extDate;
  }

  public void setExtDate(Object extDate) {
    this.extDate = extDate;
  }
}
