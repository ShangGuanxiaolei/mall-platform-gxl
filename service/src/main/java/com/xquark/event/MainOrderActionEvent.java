package com.xquark.event;

import com.xquark.dal.type.MainOrderActionType;
import com.xquark.dal.type.OrderActionType;
import org.springframework.context.ApplicationEvent;

/**
 * @author liangfan
 */
public class MainOrderActionEvent extends ApplicationEvent {

    private static final long serialVersionUID = 8945352745268513304L;

    private MainOrderActionType type;

    public static int firstStep = 1;
    public static int secondStep = 2;


    public MainOrderActionEvent(MainOrderActionType type, Object source) {
        super(source);
        this.type = type;
    }

    public MainOrderActionType getType() {
        return type;
    }

    public void setType(MainOrderActionType type) {
        this.type = type;
    }

}
