package com.xquark.aop;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by wangxinhua on 17-11-16. DESC: AOP工具类
 */
public class AopHelper {

  private static final Logger logger = LoggerFactory.getLogger(AopHelper.class);

  /**
   * 获取方法参数
   *
   * @param joinPoint 切面对象
   * @return {@link List<MethodArgument>} 方法参数集合 或空集合
   */
  public static List<MethodArgument> getMethodArgs(JoinPoint joinPoint) {
    List<MethodArgument> arguments = null;
    try {
      arguments = MethodArgument.of(joinPoint);
    } catch (NoSuchMethodException e) {
      logger.error("无法在切面 {} 中找到对应方法", joinPoint, e);
    }
    return arguments == null ? Collections.<MethodArgument>emptyList() : arguments;
  }

  /**
   * 获取单个方法参数, 仅限只含有单个参数的方法
   *
   * @param joinPoint 切点对象
   * @return {@link MethodArgument} 方法参数
   * @throws NoSuchMethodException 方法不存在
   * @throws IllegalStateException 方法参数个数不正确
   */
  public static MethodArgument getMethodArg(JoinPoint joinPoint) {
    List<MethodArgument> arguments = getMethodArgs(joinPoint);
    if (arguments.size() == 0 || arguments.size() > 1) {
      throw new IllegalArgumentException(
          String.format("方法 %s 含有多个参数或没有参数", joinPoint.getSignature().getName()));
    }
    return arguments.get(0);
  }

  /**
   * 获取切点的指定注解
   *
   * @param joinPoint 切点对象
   * @param clazz 注解类型
   * @param <T> 注解类型泛型
   * @return T 注解对象
   */
  public static <T extends Annotation> T getMethodAnnotation(JoinPoint joinPoint,
      Class<T> clazz) {
    return getMethod(joinPoint).getAnnotation(clazz);
  }

  /**
   * 获取切点方法对象
   *
   * @param joinPoint 切点对象
   * @return {@link Method} 方法对象
   */
  public static Method getMethod(JoinPoint joinPoint) {
    MethodSignature signature = (MethodSignature) joinPoint.getSignature();
    Method method = signature.getMethod();
    // 获取实际实现类的方法而非接口声明的方法
    if (method.getDeclaringClass().isInterface()) {
      final String methodName = joinPoint.getSignature().getName();
      try {
        method = joinPoint.getTarget().getClass()
            .getDeclaredMethod(methodName, method.getParameterTypes());
      } catch (NoSuchMethodException e) {
        logger.error("未在子类中找到对应的方法 {}", method.getName());
      }
    }
    return method;
  }

  /**
   * 执行jointPoint的方法
   *
   * @param jp ProceedingJoinPoint
   * @return 执行结果, 若执行失败则返回空
   */
  public static Object proceedJP(ProceedingJoinPoint jp) {
    Object o = null;
    try {
      o = jp.proceed();
    } catch (Throwable throwable) {
      logger.error("方法 {} 运行失败", jp.getSignature().getName());
    }
    return o;
  }

}
