package com.xquark.aop;

import com.xquark.aop.anno.IdEncode;
import com.xquark.aop.anno.NotNull;
import com.xquark.dal.mybatis.IdTypeHandler;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 17-11-20. DESC:
 */
@Component
@Aspect
public class ServiceAOP {

  @Pointcut("execution(* com.xquark.service..*.*(..))")
  public void serviceLayer() {
  }

  //    @Pointcut("execution(* *(@com.xquark.aop.anno.IdEncode (*)))")
  @Pointcut("execution(* *(.., @com.xquark.aop.anno.IdEncode (*), ..))")
  public void withIdEncode() {
  }

  @Pointcut("execution(* *(.., @com.xquark.aop.anno.NotNull (*), ..))")
  public void withNotNullMethod() {
  }

  @Pointcut("execution(* *(.., @com.xquark.aop.anno.NotNull (*), ..))")
  public void withNotNullArg() {
  }

  @AfterReturning("serviceLayer() && withIdEncode()")
  public void idEncode(JoinPoint joinPoint) {
    List<MethodArgument> arguments = AopHelper.getMethodArgs(joinPoint);
    CollectionUtils.filter(arguments, new Predicate<MethodArgument>() {
      @Override
      public boolean evaluate(MethodArgument object) {
        return object.hasAnnotation(IdEncode.class);
      }
    });
    IdEncode idEncode = AopHelper.getMethodAnnotation(joinPoint, IdEncode.class);
    for (MethodArgument argument : arguments) {
      Object entity = argument.getValue();
      String field = idEncode.field();
      Object fieldValue;
      try {
        fieldValue = PropertyUtils.getProperty(entity, field);
        String encodedId;
        if (fieldValue instanceof String) {
          encodedId = IdTypeHandler.encode(Long.parseLong((String) fieldValue));
        } else {
          throw new RuntimeException(String.format("属性 %s 无法通过idHandler转换", fieldValue));
        }
        PropertyUtils.setProperty(entity, field, encodedId);
      } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
        throw new RuntimeException(String.format("id转换失败，对象 %s 没有 %s 属性", entity, field),
            e);
      }

    }
  }

  @Before("serviceLayer() && withNotNullArg()")
  public void nullCheck(JoinPoint joinPoint) throws NoSuchMethodException {
    MethodSignature signature = (MethodSignature) joinPoint.getSignature();
    for (MethodArgument argument : MethodArgument.of(joinPoint)) {
      if (argument.hasAnnotation(NotNull.class) && argument.getValue() == null) {
        throw new NullPointerException(String.format(
            "%s: argument \"%s\" (at position %d) cannot be null",
            signature.getMethod(), argument.getName(), argument.getIndex()));
      }
    }
  }

}
