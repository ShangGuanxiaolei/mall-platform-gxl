package com.xquark.aop.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.aspectj.lang.JoinPoint;

/**
 * Created by wangxinhua on 17-11-20. DESC: 使用该注解将bean中的id编码
 *
 * @see com.xquark.aop.ServiceAOP#idEncode(JoinPoint)
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IdEncode {

  String field() default "id";

}
