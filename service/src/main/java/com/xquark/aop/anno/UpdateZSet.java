package com.xquark.aop.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by wangxinhua on 17-11-17. DESC: 更新zSet
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface UpdateZSet {

  /**
   * zSet键
   */
  String key();

  /**
   * 在zSet中根据对象中该值查找，并删除
   */
  String searchBy() default "id";

  /**
   * 需要缓存的属性
   */
  String parameter() default "name";

  /**
   * 指定对应的mapper实现
   */
  Class dbService();

}
