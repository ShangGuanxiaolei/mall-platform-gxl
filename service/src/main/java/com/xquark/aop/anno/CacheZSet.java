package com.xquark.aop.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by wangxinhua on 17-11-16. DESC:
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CacheZSet {

  /**
   * 缓存的键名
   */
  String key();

  /**
   * 若指定了该值则只保存对象中该值
   */
  String parameter() default "";
}
