package com.xquark.aop;

import com.xquark.aop.anno.CacheZSet;
import com.xquark.aop.anno.NeedCache;
import com.xquark.aop.anno.RemoveZSet;
import com.xquark.aop.anno.UpdateZSet;
import com.xquark.cache.CacheUtils;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.service.CacheDBService;
import java.util.List;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 17-11-16. DESC:
 */
@Aspect
@Component
public class CacheAOP {

  private static final Logger logger = LoggerFactory.getLogger(CacheAOP.class);

  @Pointcut("execution(* com.xquark.service..* (..))")
  public void publicPointCut() {
  }

  /**
   * 方法正常结束后将指定值缓存到redis的zSet中
   *
   * @param joinPoint 方法切点对象
   * @return 方法原结果 true or fasle
   */
  @Around("publicPointCut() && @annotation(com.xquark.aop.anno.CacheZSet)")
  public Object cacheZSet(ProceedingJoinPoint joinPoint) {
    List<MethodArgument> args = AopHelper.getMethodArgs(joinPoint);
    String methodName = joinPoint.getSignature().getName();
    Boolean result = null;
    try {
      result = (Boolean) joinPoint.proceed();
    } catch (Throwable throwable) {
      logger.error("调用原始 {} 方法失败", methodName, throwable);
    }
    try {
      if (result != null && result) {
        CacheZSet cache = AopHelper.getMethodAnnotation(joinPoint, CacheZSet.class);
        assert cache != null;
        String key = cache.key();
        if (CollectionUtils.isEmpty(args)) {
          return Boolean.TRUE;
        }
        // FIXME 若有多个参数被标识为 NeedCache 则只会保存一个
        MethodArgument argument;
        if (args.size() > 1) {
          argument = CollectionUtils.find(args, new Predicate<MethodArgument>() {
            @Override
            public boolean evaluate(MethodArgument object) {
              return object.hasAnnotation(NeedCache.class);
            }
          });
          if (argument == null) {
            // 多参数中没有标识需要缓存的对象
            return Boolean.TRUE;
          }
        } else {
          argument = args.get(0);
        }
        Object bean = argument.getValue();
        String cacheParameter = cache.parameter();

        // 若指定了缓存值且对象中存在这个值则只缓存该值
        if (StringUtils.isNotBlank(cacheParameter)) {
          Object param = PropertyUtils.getProperty(bean, cacheParameter);
          if (param != null) {
            bean = param;
          }
        }
        // 非string类型转为JSON保存
        CacheUtils.zAdd(key, bean);
      }
    } catch (Exception e) {
      logger.error("缓存失败, 方法 {} ", methodName, e);
    }
    return result;
  }

  /**
   * 拦截 {@link RemoveZSet} 注解对该方法删除缓存
   *
   * @param joinPoint 切面对象
   * @return 方法原结果
   */
  @Around("publicPointCut() && @annotation(com.xquark.aop.anno.RemoveZSet)")
  public Object removeZSet(ProceedingJoinPoint joinPoint) {
    Boolean result = false;
    MethodArgument arg = AopHelper.getMethodArg(joinPoint);
    String methodName = joinPoint.getSignature().getName();
    try {
      result = (Boolean) joinPoint.proceed();
    } catch (Throwable throwable) {
      logger.error("调用原始 {} 方法失败", methodName, throwable);
    }
    if (result != null && result) {
      if (arg != null) {
        Object obj = arg.getValue();
        try {
          RemoveZSet removeZset = AopHelper
              .getMethodAnnotation(joinPoint, RemoveZSet.class);
          String key = removeZset.key();
          CacheUtils.zRemove(key, obj);
        } catch (Exception e) {
          logger.error("删除缓存 {} 失败, 方法: {}", obj, methodName);
        }
      }
    }
    return result;
  }


  /**
   * 拦截 {@link UpdateZSet} 注解对该方法同步缓存
   *
   * @param joinPoint 切面
   * @return 方法原结果
   */
  @Around("publicPointCut() && @annotation(com.xquark.aop.anno.UpdateZSet)")
  public Object updateZSet(ProceedingJoinPoint joinPoint) {
    UpdateZSet updateZSet = AopHelper.getMethodAnnotation(joinPoint, UpdateZSet.class);
    MethodArgument argument = AopHelper.getMethodArg(joinPoint);
    String methodName = joinPoint.getSignature().getName();

    assert updateZSet != null;
    if (argument == null) {
      return AopHelper.proceedJP(joinPoint);
    }

    Boolean proceedResult;
    String key = updateZSet.key();
    String searchBy = updateZSet.searchBy();
    String parameter = updateZSet.parameter();
    Class dbService = updateZSet.dbService();

    // 根据注解查找对应的数据读取类
    CacheDBService cacheDBService = (CacheDBService) SpringContextUtil.getBean(dbService);
    // 更新的bean
    Object targetBean = argument.getValue();

    String objKey;
    String oldValue = null;
    try {
      // 通过objKey找到数据库中旧的字段
      objKey = (String) PropertyUtils.getProperty(targetBean, searchBy);
      Object oldBean = cacheDBService.loadByKey(objKey);

      // 找出旧的值
      oldValue = (String) PropertyUtils.getProperty(oldBean, parameter);
    } catch (Exception e) {
      logger.error("查询缓存数据出错", e);
    } finally {
      // 缓存逻辑出错不影响原方法执行
      proceedResult = (Boolean) AopHelper.proceedJP(joinPoint);
    }
    if (proceedResult != null && proceedResult && oldValue != null) {
      try {
        targetBean = (targetBean instanceof String) ? targetBean
            : PropertyUtils.getProperty(targetBean, parameter);
        CacheUtils.zUpdate(key, oldValue, (String) targetBean);
      } catch (Exception e) {
        logger.error("更新缓存失败, 方法: {}", methodName);
      }
    }

    return proceedResult;
  }


}
